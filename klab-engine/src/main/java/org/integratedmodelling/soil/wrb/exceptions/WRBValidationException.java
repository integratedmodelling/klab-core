package org.integratedmodelling.soil.wrb.exceptions;

import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.soil.wrb.WRBIdentity;

public class WRBValidationException extends KlabValidationException {
	
	WRBIdentity identity;
	
	private static final long serialVersionUID = -5113192772008367504L;

	public WRBValidationException(WRBIdentity identity, String message) {
		super(message);
		this.identity = identity;
	}
	
	public String getExplanation() {
		/*
		 * TODO make this smart and link the form in error
		 */
		return getMessage();
	}

}
