package org.integratedmodelling.engine.authorities;

import org.integratedmodelling.api.metadata.ISearchResult;
import org.integratedmodelling.common.beans.authority.Authority;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.vocabulary.authority.BaseAuthority;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.soil.wrb.WRBIdentity;
import org.integratedmodelling.soil.wrb.WRBIdentity.WRBConcept;
import org.integratedmodelling.soil.wrb.WRBParser;
import org.integratedmodelling.soil.wrb.vocabulary.WRBVocabulary;

public class WRBAuthority extends BaseAuthority {

    public WRBAuthority(Authority definition) {
        super(definition);
    }

    @Override
    public ISearchResult<AuthorityConcept> search(String query, String authorityId) {
        return null;
    }

    public static WRBAuthority newInstance() {

        WRBVocabulary.useClasspathResources();
    	
        Authority ret = new Authority();

        ret.setName("SOIL");
        ret.setOverallDescription("SOIL authority endorses the World Reference Base (WRB) classification for soil.\n\n"
                + "<explain>\n\n"
                + "This authority is a joint effort between integratedmodelling.org, Italy's CREA () and FAO");
        ret.getAuthorities().add("SOIL.WRB");
        ret.getAuthorityDescriptions().add("");
        ret.setOntologyId("wrb");
        ret.setSearchable(false);
        ret.setWorldview("im");
        ret.setVersion("1.0");

        WRBVocabulary.get().defineOntology(ret);

        ret.getInitialProperties().add("hasWRBPrefixQualifier");
        ret.getInitialProperties().add("hasWRBSpecifier");

        ret.getCoreConcepts().put("SOIL.WRB", WRBVocabulary.RSG_BASE_IDENTITY);

        return new WRBAuthority(ret);
    }

    @Override
    public AuthorityConcept getConcept(String authority, String id) throws KlabValidationException {

        WRBIdentity wrb = WRBParser.parse(id);
        AuthorityConcept ret = wrb.getConceptDefinition();

        String restrictions = "";
        for (WRBConcept q : wrb.getQualifiers()) {

            String qrestrictions = "";
            if (q.getSpecifiers() != null) {
                for (String s : q.getSpecifiers()) {
                    ret.getConceptDefinition().add(WRBVocabulary.SPECIFIER_BASE_ATTRIBUTE + "," + s);
                    qrestrictions += (qrestrictions.isEmpty() ? "" : ",") + "hasWRBSpecifier=" + s;
                }
            }

            // qualifier restricted with its specifiers if any
            ret.getConceptDefinition().add(WRBVocabulary.QUALIFIER_BASE_ATTRIBUTE + "," + q.getShortId()
                    + (qrestrictions.isEmpty() ? "" : ("," + qrestrictions)) + ",rdfs:label=" + q.toString());

            restrictions += (restrictions.isEmpty() ? "" : ",") + "hasWRBPrefixQualifier=" + q.getShortId();
        }

        ret.getConceptDefinition().add(getBaseIdentity(authority) + "," + ret.getId()
                + (restrictions.isEmpty() ? "" : ("," + restrictions)));

        return ret;
    }

}
