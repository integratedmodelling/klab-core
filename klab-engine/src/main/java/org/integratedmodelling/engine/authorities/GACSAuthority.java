package org.integratedmodelling.engine.authorities;

import org.integratedmodelling.api.metadata.ISearchResult;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.beans.authority.Authority;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.authority.BaseAuthority;

public class GACSAuthority extends BaseAuthority {

    public GACSAuthority(Authority definition) {
        super(definition);
    }

    @Override
    public ISearchResult<AuthorityConcept> search(String query, String authorityId) {
        // TODO Auto-generated method stub
        return null;
    }

    public static GACSAuthority newInstance() {

        Authority ret = new Authority();

        // to become GACS I guess
        ret.setName("AGROVOC");
        ret.setOntologyId("agrovoc");
        ret.setOverallDescription("The AGROVOC vocabulary provides standard identifiers for many terms of common use in agriculture.\n\n"
                + "We do not provide a search facility in the application yet. Please connect to the AGROVOC search site at "
                + "<b>http://aims.fao.org/standards/agrovoc/functionalities/search</b> and use a form like\n\n"
                + "   <b>im.ecology:Population identified as \"1931\"  by AGROVOC</b>\n\n"
                + "in your annotations.");
        ret.getAuthorities().add("AGROVOC.SPECIES");
        ret.getAuthorities().add("AGROVOC.PROCESS");
        ret.getAuthorities().add("AGROVOC.CROP");
        ret.getAuthorityDescriptions().add("");
        ret.getAuthorityDescriptions().add("");
        ret.getAuthorityDescriptions().add("");
        ret.setSearchable(false); // for now
        ret.setWorldview("im");
        ret.setVersion("1.0");
        ret.getInitialConcepts().add(NS.CORE_IDENTITY_TRAIT + ",Crop," + NS.BASE_DECLARATION + "=true");
        ret.getInitialConcepts()
                .add(NS.CORE_IDENTITY_TRAIT + ",AgriculturalProcess," + NS.BASE_DECLARATION + "=true");
        ret.getInitialConcepts().add(NS.CORE_IDENTITY_TRAIT + ",Livestock," + NS.BASE_DECLARATION + "=true");

        ret.getCoreConcepts().put("AGROVOC.SPECIES", "Livestock");
        ret.getCoreConcepts().put("AGROVOC.PROCESS", "AgriculturalProcess");
        ret.getCoreConcepts().put("AGROVOC.CROP", "Crop");

        return new GACSAuthority(ret);
    }

    @Override
    public AuthorityConcept getConcept(String authority, String id) {

        String cid = null;

        // entirely TODO - for now just performs simple syntactical validation

        if (id.startsWith("http://aims.fao.org/aos/agrovoc/")) {
            cid = Path.getLast(id, '/');
        } else if (id.startsWith("c_")) {
            cid = id;
        }

        if (cid != null) {

            AuthorityConcept ret = new AuthorityConcept();

            ret.setId(cid);
            ret.getConceptDefinition().add(getBaseIdentity(authority) + "," + cid);
            ret.setLabel(id);
            ret.setDefinition(id);
            ret.setDescription(id);

            return ret;
        }

        return null;
    }
}
