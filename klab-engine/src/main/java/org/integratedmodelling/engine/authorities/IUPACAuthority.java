package org.integratedmodelling.engine.authorities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.metadata.ISearchResult;
import org.integratedmodelling.common.beans.authority.Authority;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.beans.authority.AuthorityQueryResponse;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.image.ImageUtil;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.authority.BaseAuthority;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.openscience.cdk.DefaultChemObjectBuilder;
import org.openscience.cdk.inchi.InChIGeneratorFactory;
import org.openscience.cdk.inchi.InChIToStructure;
import org.openscience.cdk.interfaces.IAtomContainer;
import org.openscience.cdk.layout.StructureDiagramGenerator;
import org.openscience.cdk.renderer.AtomContainerRenderer;
import org.openscience.cdk.renderer.RendererModel;
import org.openscience.cdk.renderer.font.AWTFontManager;
import org.openscience.cdk.renderer.generators.BasicAtomGenerator;
import org.openscience.cdk.renderer.generators.BasicSceneGenerator;
import org.openscience.cdk.renderer.generators.ExtendedAtomGenerator;
import org.openscience.cdk.renderer.generators.HighlightGenerator;
import org.openscience.cdk.renderer.generators.IGenerator;
import org.openscience.cdk.renderer.generators.RingGenerator;
import org.openscience.cdk.renderer.visitor.AWTDrawVisitor;

import net.sf.jniinchi.INCHI_KEY;
import net.sf.jniinchi.INCHI_KEY_STATUS;
import net.sf.jniinchi.INCHI_RET;
import net.sf.jniinchi.JniInchiException;
import net.sf.jniinchi.JniInchiOutputKey;
import net.sf.jniinchi.JniInchiWrapper;
import uk.ac.cam.ch.wwmm.opsin.NameToInchi;
import uk.ac.cam.ch.wwmm.opsin.NameToStructure;
import uk.ac.cam.ch.wwmm.opsin.NameToStructureConfig;
import uk.ac.cam.ch.wwmm.opsin.OpsinResult;
import uk.ac.cam.ch.wwmm.opsin.OpsinResult.OPSIN_RESULT_STATUS;

public class IUPACAuthority extends BaseAuthority {

    public IUPACAuthority(Authority definition) {
        super(definition);
    }

    @Override
    public ISearchResult<AuthorityConcept> search(String query, String authorityId) {
        
        AuthorityQueryResponse ret = new AuthorityQueryResponse();
               
        try {
            List<AuthorityConcept> result = searchOPSIN(query, this, authorityId);
            ret.getMatches().addAll(result);
            ret.setPage(-1);
            ret.setMoreMatches(false);
        } catch (KlabException e) {
            // log error and return no matches
            KLAB.error(e);
            ret.setError(e.getMessage());
        }
        
        return ret;
    }

    public static IUPACAuthority newInstance() {

        Authority ret = new Authority();

        ret.setName("IUPAC");
        ret.setOverallDescription("IUPAC endorses the use of the <b>InChI</b> identifiers for all organic and inorganic compounds.\n\n. Please search for the InChI "
                + "identifier for the compound you want to annotate (if the internal search is unsuccessful, Wikipedia lists them for each compound that has a page)"
                + " and use a form like\n\n   <b>im.core:Mass identified as \"1S/H3N/h1H3\" by IUPAC</b>\n\n"
                + "in your annotations.");
        ret.getAuthorities().add("IUPAC");
        ret.getAuthorityDescriptions().add("");
        ret.setOntologyId("iupac");
        ret.setSearchable(true);
        ret.setWorldview("im");
        ret.setVersion("1.0");
        
        ret.getInitialConcepts().add(NS.CORE_IDENTITY_TRAIT + ",Compound");
        ret.getCoreConcepts().put("IUPAC", "Compound");
        
        return new IUPACAuthority(ret);
    }

    @Override
    public AuthorityConcept getConcept(String authority, String id) {

        AuthorityConcept ret = null;
        String hash = getId(id);
        if (hash != null) {
            ret = new AuthorityConcept();
            ret.setId(hash.replaceAll("\\-", "_"));
            ret.setDescription(id);
            ret.setDefinition(id);
            ret.setLabel(id);
            ret.getConceptDefinition().add(getBaseIdentity(authority) + "," + hash.replaceAll("\\-", "_"));
        }
        return ret;
    }

    /*
     * OPSIN-specific methods.
     */

    public static List<AuthorityConcept> searchOPSIN(String query, IUPACAuthority authority, String view) throws KlabException {

        List<AuthorityConcept> ret = new ArrayList<>();

        NameToStructure nts = NameToStructure.getInstance();
        NameToStructureConfig ntsconfig = new NameToStructureConfig();
        ntsconfig.setAllowRadicals(true);
        OpsinResult result = nts.parseChemicalName(query, ntsconfig);

        if (result.getStatus() == OPSIN_RESULT_STATUS.SUCCESS) {
            
            String stdinchi = NameToInchi.convertResultToStdInChI(result);
            String stdinchikey = NameToInchi.convertResultToStdInChIKey(result);

            AuthorityConcept md = new AuthorityConcept();
            md.setId(stdinchikey.replaceAll("\\-", "_"));
            md.setDefinition(stdinchi);
            md.setLabel(result.getChemicalName());
            md.getConceptDefinition().add(authority.getBaseIdentity(view) + "," + stdinchikey.replaceAll("\\-", "_"));

            ret.add(md);
        }

        return ret;
    }

    /**
     * Get the IUPAC hash for the passed InChI string, or null if the string was not
     * recognized.
     * 
     * @param inchi
     * @return
     */
    public static String getId(String inchi) {

        String ret = inchi;
        INCHI_KEY_STATUS status = INCHI_KEY_STATUS.INVALID_LAYOUT;
        try {
            status = JniInchiWrapper.checkInchiKey(inchi);
        } catch (JniInchiException e) {
            // fall through with invalid status
        }

        if (status != INCHI_KEY_STATUS.VALID_STANDARD) {
            /*
             * try as a InChI - this should be the default situation
             */
            ret = null;
            try {
                JniInchiOutputKey output = JniInchiWrapper.getInchiKey("InChI=" + inchi);
                if (output.getReturnStatus() == INCHI_KEY.OK) {
                    ret = output.getKey();
                }
            } catch (JniInchiException e) {
                // fall through with ret = null
            }
        }
        return ret;
    }

    public static void makeImage(String inchi, File output) throws KlabException {

        BufferedImage image = null;
        try {

            InChIGeneratorFactory factory = InChIGeneratorFactory.getInstance();
            // Get InChIToStructure
            InChIToStructure intostruct = factory
                    .getInChIToStructure(inchi, DefaultChemObjectBuilder.getInstance());

            INCHI_RET ret = intostruct.getReturnStatus();
            if (ret != INCHI_RET.OKAY) {
                // Structure generation failed
                throw new KlabValidationException("Structure generation failed failed: " + ret.toString()
                        + " [" + intostruct.getMessage() + "]");
            }

            IAtomContainer mol = intostruct.getAtomContainer();
            StructureDiagramGenerator dptgen = new StructureDiagramGenerator();
            dptgen.setMolecule(mol);
            dptgen.generateCoordinates();

            // make generators
            List<IGenerator<IAtomContainer>> generators = new ArrayList<>();
            generators.add(new BasicSceneGenerator());
            // generators.add(new BasicBondGenerator());
            generators.add(new BasicSceneGenerator());
            generators.add(new RingGenerator());
            // generators.add(new AtomNumberGenerator());
            generators.add(new ExtendedAtomGenerator());
            generators.add(new HighlightGenerator());

            // setup the renderer
            AtomContainerRenderer renderer = new AtomContainerRenderer(generators, new AWTFontManager());
            RendererModel model = renderer.getRenderer2DModel();
            model.set(ExtendedAtomGenerator.CompactAtom.class, true);
            model.set(ExtendedAtomGenerator.AtomRadius.class, 5.0);
            model.set(ExtendedAtomGenerator.CompactShape.class, BasicAtomGenerator.Shape.OVAL);
            model.set(ExtendedAtomGenerator.KekuleStructure.class, true);
            model.set(ExtendedAtomGenerator.ShowAtomTypeNames.class, true);
            model.set(RingGenerator.BondWidth.class, 1.0);

            // get the image
            image = new BufferedImage(400, 400, BufferedImage.TYPE_4BYTE_ABGR);
            Graphics2D g = (Graphics2D) image.getGraphics();
            g.setColor(Color.WHITE);
            g.fill(new Rectangle2D.Double(0, 0, 400, 400));

            // paint
            // renderer.paint(molecule, new AWTDrawVisitor(g));
            renderer.paint(dptgen
                    .getMolecule(), new AWTDrawVisitor(g), new Rectangle2D.Double(0, 0, 400, 400), true);
            g.dispose();
        } catch (Exception e) {
            throw new KlabValidationException(e);
        }

        ImageUtil.saveImage(image, output.toString());
    }

    public static void main(String[] args) throws Exception {
//        for (AuthorityConcept result : searchOPSIN("(8R,9S,13S,14S,17S)-13-Methyl-6,7,8,9,11,12,14,15,16,17-decahydrocyclopenta[a]phenanthrene-3,17-diol")) {
//            System.out.println(result.getId());
//            System.out.println(result.getLabel());
////            makeImage(result.getString(IMetadata.DC_NAME), new File("diopro.png"));
//        }
    }

}
