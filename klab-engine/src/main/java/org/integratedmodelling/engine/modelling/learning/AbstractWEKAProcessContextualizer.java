package org.integratedmodelling.engine.modelling.learning;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.ILearningProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

import weka.classifiers.Classifier;
import weka.core.Instances;

/**
 * Use to derive any WEKA learning process contextualizer that can be trained through an
 * ARFF instance set. Have getClassifier() return a new classifier. Initialize() builds
 * the training set and trains the classifier, then computes the initialized model.
 * Transitions re-train updateable classifiers by adding any new archetypes or rebuild the
 * full model if the classifier isn't updateable. Models can be exported if supported.
 * Automatically documents the computation. Calling model, project and function parameters
 * are available to extending classes through the {@link #model}, {@link #project} and
 * {@link #parameters} protected members.
 * 
 * @author ferdinando.villa
 * @author ioannis.athanasiadis
 */
public abstract class AbstractWEKAProcessContextualizer
        implements ILearningProcessContextualizer {

    protected WEKALearningProcess learningProcess = null;
    boolean                       canDispose      = false;

    // these are available to children so they don't have to redefine setContext()
    protected IModel              model;
    protected IProject            project;
    protected Map<String, Object> parameters;
    // protected Instances instances;

    private File                  modelFile       = null;

    @Override
    public Pair<String, Collection<File>> getModel(String relativePath, File outputDirectory) {
        return learningProcess.saveModel();
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation contextSubject, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        canDispose = process.getScale().getTime() == null;
        this.learningProcess = createLearningProcess(monitor);
        this.learningProcess
                .initialize(process, contextSubject, resolutionContext, expectedInputs, expectedOutputs);

        if (modelFile != null) {
            /*
             * look for discretizer sidecar file
             */
            File discretizerFile = MiscUtilities.getSidecarFile(modelFile, WEKALearningProcess.DISCRETIZER_SUFFIX);
            if (!discretizerFile.exists()) {
                discretizerFile = null;
            }
            this.learningProcess.load(modelFile, discretizerFile);
        } else {
            this.learningProcess.train(ITransition.INITIALIZATION);
        }

        Map<String, IObservation> ret = new HashMap<>();
        ret.put(this.learningProcess.getOutputState().getMetadata()
                .getString(IMetadata.DC_LABEL), this.learningProcess.getOutputState());
        // Why do we run this again?
        this.learningProcess.runModel(ITransition.INITIALIZATION);

        return ret;
    }

    protected WEKALearningProcess createLearningProcess(IMonitor monitor) {
        return new WEKALearningProcess(this, monitor);
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException {

        Map<String, IObservation> ret = new HashMap<>();
        canDispose = transition.isLast();

        /*
         * TODO if there are new archetypes, re-train
         */
        if (!inputs.isEmpty()) {
            this.learningProcess.runModel(transition);
        }

        return ret;
    }

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance)
            throws KlabValidationException {

        this.model = model;
        this.project = project;
        this.parameters = parameters;

        if (parameters.containsKey("import")) {
            this.modelFile = new File(project.getLoadPath() + File.separator
                    + parameters.get("import").toString());
            if (!this.modelFile.exists()) {
                throw new KlabValidationException("no file " + parameters.get("import").toString()
                        + " exists");
            }
        }
    }

    /**
     * Creates and returns a new classifier instance
     * 
     * @param instances the instances that will be used for learning.
     * @return
     */
    protected abstract Classifier createNewClassifier(Instances instances);

    /**
     * Load the classifier from a file. The default will deserialize a binary file, the
     * norm in most of Weka.
     * 
     * @param classifier
     * @return
     */
    protected Classifier loadClassifier(File classifier) {

        Classifier result = null;

        try (FileInputStream fis = new FileInputStream(classifier);
                ObjectInputStream ois = new ObjectInputStream(fis)) {
            result = (Classifier) ois.readObject();
        } catch (Throwable e) {
            throw new KlabRuntimeException(e);
        }

        return result;
    }
}
