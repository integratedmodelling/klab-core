/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.datasources;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IValueResolver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.base.HashableObject;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.runtime.AbstractStateContextualizer;
import org.integratedmodelling.engine.modelling.random.DistributionValue;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A datasource that returns the same object no matter what.
 * 
 * @author Ferd
 *
 */
public class RandomDataSource extends HashableObject implements IDataSource {

    private DistributionValue _state    = null;
    private IMetadata         _metadata = new Metadata();
    private boolean           _integer  = false;

    class RandomActuator extends AbstractStateContextualizer implements IValueResolver {
        
        public RandomActuator(IMonitor monitor) {
            super(monitor);
        }

        public Map<String, Object> getValue() {
            Map<String, Object> ret = new HashMap<>();
            for (String s : getOutputKeys()) {
                double d = _state.draw();
                ret.put(s, _integer ? d : (int) Math.round(d));
            }
            return ret;
        }

        // @Override
        public String getDatasourceLabel() {
            return "[random from: " + _state.getName() + "]";
        }

        @Override
        public boolean isProbabilistic() {
            return false;
        }

        @Override
        public Map<String, Object> initialize(int index, Map<String, Object> inputs) throws KlabException {
            return getValue();
        }

        @Override
        public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
                throws KlabException {
            return getValue();
        }
        
        @Override
        public String getLabel() {
            return "randomize";
        }

    }

    public RandomDataSource(String distribution, double... parameters) {
        _state = new DistributionValue(distribution, parameters);
    }

    @Override
    public IStateContextualizer getContextualizer(IScale context, IObserver observer, IMonitor monitor)
            throws KlabException {
        return new RandomActuator(monitor);
    }

    @Override
    public IMetadata getMetadata() {
        return _metadata;
    }

    @Override
    public IScale getCoverage() {
        return new Scale();
    }

    @Override
    public String toString() {
        return _state.toString();
    }

    public void setIntegerOutput(boolean b) {
        _integer = b;
    }

    @Override
    public boolean isAvailable() {
        return true;
    }
    
    
}
