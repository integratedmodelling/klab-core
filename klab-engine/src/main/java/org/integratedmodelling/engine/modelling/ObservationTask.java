/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.agents.IObservationTask;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.engine.introspection.CallTracer;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation;
import org.integratedmodelling.exceptions.KlabException;

public class ObservationTask implements IObservationTask {
    // default generated id
    private static final long                serialVersionUID = 1L;
    protected final IActiveDirectObservation subject;
    protected final ITimeInstant             observationTime;
    private boolean                          valid            = true;
    private long                             startedAt;

    private IObservationGraphNode            parentNode       = null;
    protected final IObservationController   controller;

    public ObservationTask(IActiveDirectObservation subject, ITimeInstant observationTime,
            IObservationController controller) {
        this(subject, observationTime, null, controller);
    }

    public ObservationTask(IActiveDirectObservation subject, ITimeInstant observationTime,
            IObservationGraphNode parentNode, IObservationController controller) {
        this.subject = subject;
        this.observationTime = observationTime;
        this.parentNode = parentNode;
        this.controller = controller;
    }

    /**
     * provide temporal ordering according to each observation START time, and by END time secondarily. If the
     * time is the same, use dependency order.
     * 
     * FIXME - priority should be established using the schedule.
     */
    @Override
    public int compareTo(IObservationTask other) {
        int ctime = observationTime.compareTo(other.getObservationTime());
        return (ctime == 0 && getSubject() != null && other.getSubject() != null) 
                ? Integer.compare(((DirectObservation) getSubject())
                        .getPriorityOrder(), ((DirectObservation) other.getSubject()).getPriorityOrder())
                : ctime;
    }

    /**
     * TODO need to take the subject's state forward in time from its current state(s) to its state(s) as of
     * the next time step (this includes finding out what that time step is)
     */
    @Override
    public ITransition run() throws KlabException {
        CallTracer.indent("run()", this);
        // tell the agent (via the subjectObserver) to make the next state transition
        // NOTE: this should also set the agent to its new state (won't be done explicitly anywhere else)
        ITransition transition = subject.performTemporalTransitionFromCurrentState(controller);

        // add a furtherObservations item for the next time period, if the observer survives.
        if (subject.isActive() && transition.agentSurvives()) {
            IObservationTask subsequentTaskForThisAgent = new ObservationTask(subject, transition
                    .getTime().getEnd(), controller);
            transition.addFurtherObservationTask(subsequentTaskForThisAgent);
        }

        CallTracer.unIndent();
        return transition;
    }

    @Override
    public ITimeInstant getObservationTime() {
        return observationTime;
    }

    @Override
    public void startedWorkAt(long systemTimeMilliseconds) {
        startedAt = systemTimeMilliseconds;
    }

    @Override
    public long getStartTime() {
        return startedAt;
    }

    @Override
    public void setInvalid() {
        valid = false;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public IObservationGraphNode getParentNode() {
        return parentNode;
    }

    @Override
    public void setParentNode(IObservationGraphNode node) {
        this.parentNode = node;
    }

    @Override
    public IActiveDirectObservation getSubject() {
        return subject;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " for " + subject + " at " + observationTime;
    }
}
