/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabRuntimeException;

public class Process extends DirectObservation implements IActiveProcess, NetworkSerializable {
    
	String name;

	public Process(IObservableSemantics semantics, IModel model, IDirectObservation subject, IMonitor monitor) {
		/*
		 * TODO processes may have their own scale eventually.
		 */
		super(semantics, (IActiveDirectObservation) subject, subject.getScale(), subject.getContext(),  model.getNamespace(),
				NS.getDisplayName(semantics.getType()) + "_" + subject.getName(), monitor);
		
		setModel(model);
		
		this.contextSubject = subject;
		this.name = CamelCase.toLowerCase(getObservable().getSemantics().getLocalName(), '-');
		this.parentId = ((Observation) subject).getInternalId();
	}
	

	@Override
	public IModel getModel() {
		return this.model;
	}

	@Override
	public Map<IProperty, IState> getObjectStateCopy() {
		return new HashMap<IProperty, IState>();
	}

    @Override
	public String toString() {
		return "<process " + observable + ">";
	}


	@SuppressWarnings("unchecked")
	@Override
	public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {
		if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Process.class)) {
			throw new KlabRuntimeException("cannot serialize a Context to a " + desiredClass.getCanonicalName());
		}

		org.integratedmodelling.common.beans.Process ret = new org.integratedmodelling.common.beans.Process();
		super.serialize(ret);

		return (T) ret;
	}


    @Override
    public boolean affects(IConcept c) {
        // TODO Auto-generated method stub
        return false;
    }


    @Override
    public boolean creates(IConcept c) {
        // TODO Auto-generated method stub
        return false;
    }

}
