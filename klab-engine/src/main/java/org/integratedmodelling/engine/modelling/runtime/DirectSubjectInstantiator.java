package org.integratedmodelling.engine.modelling.runtime;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A subject instantiator that simply returns a list of subjects that was
 * resolved before.
 * 
 * @author fvilla
 *
 */
public class DirectSubjectInstantiator extends AbstractSubjectInstantiator {

	List<ISubject> subjects;

	public DirectSubjectInstantiator(List<ISubject> subjects, IMonitor monitor) {
		this.subjects = subjects;
		this.monitor = monitor;
	}

	@Override
	public Map<String, IObservation> createSubjects(IActiveSubject context, ITransition transition) throws KlabException {
		if (transition == ITransition.INITIALIZATION) {

			Map<String, IObservation> ret = new HashMap<>();

			for (ISubject subject : subjects) {
				((Subject)subject).setContextSubject(context);
				((Subject)context).addSubject(subject, KLAB.p(NS.PART_OF));
				ret.put(subject.getName(), subject);
			}

			return ret;
		}
		return null;
	}
	
	

}
