package org.integratedmodelling.engine.modelling.runtime;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.ILock;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Session;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.engine.ModelingEngine;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Engine-side implementation of Session, using the resolver and contextualizer to make the primary
 * observation.
 * 
 * @author ferdinando.villa
 *
 */
public class EngineSession extends Session {

    /**
     * @param user
     */
    public EngineSession(IUser user) {
        super(user);
    }

    @Override
    public ITask observe(Object observable, Object... options) throws KlabException {

        List<String> scenarios = new ArrayList<>();
        List<IExtent> forcings = new ArrayList<>();

        /*
         * TODO add error checking
         */
        for (Object o : MiscUtilities.flattenParameterList(options)) {
            if (o instanceof INamespace) {
                scenarios.add(((INamespace) o).getId());
            } else if (o instanceof String) {
                scenarios.add((String) o);
            } else if (o instanceof IExtent) {
                forcings.add((IExtent) o);
            }
        }

        IContext ctx = Context.createDeferred(observable, this, forcings);
        pushContext(ctx);
        if (scenarios != null && scenarios.size() > 0) {
            ctx = ctx.inScenario(scenarios.toArray(new String[scenarios.size()]));
        }

        return ((Context) ctx).observeAsynchronous();
    }

    @Override
    public boolean requestLock() {
        ILock lock = ((ModelingEngine) KLAB.ENGINE).getLock();
        if (lock.lock(getUser(), false)) {
            return true;
        }
        return false;
    }

    @Override
    public boolean releaseLock() {
        ILock lock = ((ModelingEngine) KLAB.ENGINE).getLock();
        if (lock.isLocked())
            if (lock.unlock(getUser())) {
                return true;
            }
        return false;
    }

    @Override
    public INetwork getNetwork() {
        /*
         * TODO this is wrong, although it won't do any damage the way things are currently set up. It should
         * be the view for the user, not the engine network.
         */
        return KLAB.ENGINE.getNetwork();
    }

}
