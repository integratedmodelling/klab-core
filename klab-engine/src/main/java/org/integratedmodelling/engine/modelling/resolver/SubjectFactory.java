/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.lang.reflect.Constructor;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.engine.modelling.runtime.Subject;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabInternalErrorException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Reimplemented to use the annotation-driven mechanism used for other things in Thinklab.
 * Just tag
 * the subject class to associate with a concept with the
 * annotation @SubjectType(NS.CONCEPT_TO_ASSOCIATE).
 * This will register the class with the Model Manager and the class will be associated
 * intelligently
 * (meaning, the reasoner will be used so derived concept will be associated to the class
 * that is
 * semantically closest).
 * TODO this could (should?) use the default ISemanticObject mechanism in Thinklab (works
 * exactly the same and adds
 * bi-directional extraction/injection of formal semantics from/to Java objects) but this
 * way it's
 * simpler and likely works out of the box.
 * 
 * @author Luke
 * @author Ferd
 */
public class SubjectFactory {

    /**
     * The main entry point.
     *
     * @param observable
     * @param namespace
     * @param scale
     * @param newSubjectId
     * @return a new subject of the appropriate class
     * @throws KlabException
     */
    public static Subject getSubjectByMetadata(IObservableSemantics observable, IContext context, INamespace namespace, IScale scale, String newSubjectId, IMonitor monitor)
            throws KlabException {

        Subject result;
        Constructor<?> constructor;

        Class<?> agentClass = KLAB.MMANAGER
                .getSubjectClass(observable.getType());
        if (agentClass == null) {
            agentClass = Subject.class;
        }

        try {
            constructor = agentClass
                    .getConstructor(IObservableSemantics.class, IScale.class, IContext.class, INamespace.class, String.class, IMonitor.class);
        } catch (Exception e) {
            throw new KlabInternalErrorException("No viable constructor found for Java class '"
                    + agentClass.getCanonicalName() + "' for agent type '"
                    + observable.getFormalName()
                    + "'");
        }

        try {
            result = (Subject) constructor
                    .newInstance(observable, scale, context, namespace, newSubjectId, monitor);
        } catch (Exception e) {
            throw new KlabRuntimeException("Unable to generate new instance of Java class '"
                    + agentClass.getCanonicalName() + "' for agent type '"
                    + observable.getFormalName()
                    + "'");
        }

        return result;

    }

}
