package org.integratedmodelling.engine.modelling.runtime;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.contextualization.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.IReifiableObject;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.exceptions.KlabException;

public abstract class ObjectSourceSubjectInstantiator extends AbstractSubjectInstantiator
		implements ISubjectInstantiator {

	protected abstract Collection<IReifiableObject> getObjects(IScale scale, ITransition transition);

	/**
	 * Create all subjects from an object source in a given scale. Only create them at
	 * initialization.
	 * 
	 * @param model
	 * @param objectSource
	 * @param scale
	 * @param contextSubject
	 * @param monitor
	 * @return new subjects
	 * @throws KlabException
	 */
	@Override
    public Map<String, IObservation> createSubjects(IActiveSubject context, ITransition transition) throws KlabException {

		Map<String, IObservation> ret = new HashMap<>();

		if (transition == ITransition.INITIALIZATION) {

			int i = 1;
			for (IReifiableObject o : getObjects(scale, transition)) {
				String name = CamelCase.toLowerCase(model.getObservable().getType().getLocalName(), '-') + "-" + i;
				IObservableSemantics observable = new ObservableSemantics(model.getObservable().getType());
				IScale oScale = o.getScale(scale);
				if (!oScale.isConsistent()) {
					monitor.warn("inconsistent or empty scale for new object " + name + " (id = " + o.getId()
							+ "): ignored");
					continue;
				}

				ISubject subject = ((Subject) context).newSubject(observable, oScale, name, KLAB.p(NS.PART_OF));

				for (Pair<String, IObserver> at : model.getAttributeObservers(false)) {
					Object att = o.getAttributeValue(at.getFirst(), at.getSecond());
					if (att != null) {
						// FIXME see what to do with dynamic states. ALSO should
						// enable expressions and 'on
						// definition' ....
						((Subject) subject).addState(new State(att, at.getSecond().getObservable(), subject, false, false));
					}
				}

				for (Pair<String, IProperty> at : model.getAttributeMetadata()) {
					if (at.getSecond().toString().equals(IMetadata.IM_NAME)) {
						Object att = o.getAttributeValue(at.getFirst(), null);
						if (att != null) {
							((Subject) subject).setId(att.toString());
						}
					} else {
						subject.getMetadata().put(at.getSecond().toString(), at.getFirst());
					}
				}

				((Subject) subject).setContextSubject(context);

				/*
				 * TODO model may have actions that we need to call and schedule
				 * for transitions
				 */
				ret.put(name, subject);
				i++;
			}

			monitor.info("created " + (i-1) + " " + model.getObservable().getType() + (i == 2 ? " subject" : " subjects"),
					Messages.INFOCLASS_MODEL);
		}
		return ret;
	}

}
