/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.resolver;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IDocumentation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IValueResolver;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.base.HashableObject;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.common.model.runtime.AbstractStateContextualizer;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Wraps a previously computed IState into a IModel that can be used to resolve
 * a dependency for other objects whose scale is overlapping, using a StateView.
 * 
 * @author Ferd
 * 
 */
public class StateViewModel implements IModel {

	IObservableSemantics _observable;
	IState _state;
	IScale _scale;
	IState view;
	IDataSource _datasource;
	INamespace _namespace;
	IObserver _observer;

	class StateDatasource extends HashableObject implements IDataSource {

		class StateDSActuator extends AbstractStateContextualizer implements IValueResolver {

			public StateDSActuator(IMonitor monitor) {
				super(monitor);
			}
			
			@Override
			public String toString() {
				return "<StateDSActuator " + _state.getObservable().getSemantics() + ">";
			}

			@Override
			public String getLabel() {
				return null;
			}

			@Override
			public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
					throws KlabException {
				return MapUtils.ofWithNull(getStateName(), getView().getValue(index));
			}

			@Override
			public boolean isProbabilistic() {
				return false;
			}

			@Override
			public Map<String, Object> initialize(int index, Map<String, Object> inputs) throws KlabException {
				return MapUtils.ofWithNull(getStateName(), getView().getValue(index));
			}

		}

		@Override
		public IMetadata getMetadata() {
			return _state.getMetadata();
		}

		@Override
		public IStateContextualizer getContextualizer(IScale context, IObserver contextObserver, IMonitor monitor)
				throws KlabException {
			return new StateDSActuator(monitor);
		}

		@Override
		public IScale getCoverage() {
			return _state.getScale();
		}

		@Override
		public String toString() {
			return "Rescaled observation of " + _state.getObservable().getSemantics().getType();
		}

		@Override
		public boolean isAvailable() {
			return true;
		}
	}
	

	public IState getView() {
		if (view == null) {
			view = States.getView(_state, _scale);
		}
		return view;
	}


	public StateViewModel(IState s, IScale scale, INamespace namespace) {
		_observable = s.getObservable().getSemantics();
		_state = s;
		_scale = scale;
		_datasource = new StateDatasource();
		_namespace = namespace;
		/*
		 * if we don't copy the observer, the dataflow will compile itself until
		 * the end of all heaps.
		 */
		_observer = ((KIMObserver) _state.getObserver()).copy();
	}

	@Override
	public List<IDependency> getDependencies() {
		return new ArrayList<>();
	}

	@Override
	public boolean hasActionsFor(IConcept observable, IConcept domainConcept) {
		return false;
	}

	@Override
	public IScale getCoverage(IMonitor monitor) {
		return _state.getScale();
	}

	@Override
	public List<IAction> getActions() {
		return new ArrayList<>();
	}

	@Override
	public IObservableSemantics getObservable() {
		return _observable;
	}

	@Override
	public String getId() {
		return ((State) _state).getInternalId();
	}

	@Override
	public boolean isPrivate() {
		return false;
	}

	@Override
	public boolean isInactive() {
		return false;
	}

	@Override
	public Collection<IAnnotation> getAnnotations() {
		return new ArrayList<>();
	}

	@Override
	public int getFirstLineNumber() {
		return 0;
	}

	@Override
	public int getLastLineNumber() {
		return 0;
	}

	@Override
	public INamespace getNamespace() {
		return _namespace;
	}

	@Override
	public String getName() {
		return "rescaled observation";
	}

	@Override
	public IMetadata getMetadata() {
		return _state.getMetadata();
	}

	@Override
	public List<IObservableSemantics> getObservables() {
		return Collections.singletonList(_observable);
	}

	@Override
	public IDataSource getDatasource(IMonitor monitor) {
		return _datasource;
	}

	@Override
	public IObjectSource getObjectSource(IMonitor monitor) {
		return null;
	}

	@Override
	public IObserver getObserver() {
		return _observer;
	}

	@Override
	public boolean isResolved() {
		return true;
	}

	@Override
	public String toString() {
		return "<StateViewModel " + _state.getObservable().getSemantics() + ">";
	}

	public boolean isView() {
		return false;
	}

	@Override
	public IConcept getType() {
		return _state.getType();
	}

	@Override
	public boolean isInstantiator() {
		return false;
	}

	@Override
	public int getErrorCount() {
		return 0;
	}

	@Override
	public int getWarningCount() {
		return 0;
	}

	@Override
	public boolean isFirstClass() {
		return true;
	}

	@Override
	public boolean isDeprecated() {
		return false;
	}

	@Override
	public Collection<IModelObject> getChildren() {
		return new ArrayList<>();
	}

	@Override
	public IModelObject getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object getInlineValue() {
		return null;
	}

	@Override
	public Collection<Pair<String, IObserver>> getAttributeObservers(boolean b) {
		return null;
	}

	@Override
	public Collection<Pair<String, IProperty>> getAttributeMetadata() {
		return null;
	}

	@Override
	public boolean isAvailable() {
		return true;
	}

	@Override
	public void setNamespace(INamespace namespace) {
		this._namespace = namespace;
	}

	@Override
	public IContextualizer getContextualizer(IResolutionScope scope, IProvenance.Artifact provenance, IMonitor monitor) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isReinterpreter() {
		return false;
	}

	@Override
	public String getNameFor(IObservableSemantics observable) {
		return observable.getFormalName();
	}


    @Override
    public IDocumentation getDocumentation() {
        // TODO Auto-generated method stub
        return null;
    }

}
