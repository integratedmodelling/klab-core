package org.integratedmodelling.engine.modelling.runtime;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAction.Trigger;
import org.integratedmodelling.api.modelling.IActiveEvent;
import org.integratedmodelling.api.modelling.IEventBus;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.interfaces.actuators.IDirectActuator;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.common.model.actuators.Actuator;

/**
 * One of these is tied to each context, and handles the communication of events
 * to registered event listeners.
 * 
 * @author ferdinando.villa
 *
 */
public class EventBus implements IEventBus {

    Context context;
    List<Pair<EventTrigger, DirectObservation>> listeners = new ArrayList<>();
    Set<IConcept> triggers = new HashSet<>();
    Set<IConcept> seen = new HashSet<>();
    
    // holds cached info
    class EventTrigger extends KIMAction.EventContext {
        
        EventTrigger(KIMAction.EventContext ctx) {
            super(ctx);
        }
        
    }
    
    public EventBus(Context context) {
        this.context = context;
    }

    @Override
    public void subscribe(IObservation observation, IActuator actuator) {
        // create record for observation and indices for concept
        for (IAction action : ((Actuator)actuator).getActions(Trigger.EVENT)) {
            for (IConcept c : action.getTriggeringEvents()) {
                addTrigger(c);
                listeners.add(new Pair<>(new EventTrigger(((KIMAction)action).getEventContext()), (DirectObservation)observation));
            }
        }
    }

    private void addTrigger(IConcept c) {
        if (seen.contains(c)) {
            return;
        }
        seen.add(c);
        for (IConcept co : c.getSemanticClosure()) {
            triggers.add(co);
        }
    }

    @Override
    public void dispatch(IActiveEvent event) {
        
        if (!triggers.contains(event.getObservable().getSemantics().getType())) {
            return;
        }
        for (Pair<EventTrigger, DirectObservation> listener : listeners) {
            if (listener.getSecond().isActive() && reactsTo(listener.getFirst(), event)) {
                ((IDirectActuator<?>)listener.getSecond().getActuator()).dispatch(event);
            }
        }
    }

    private boolean reactsTo(EventTrigger eventTrigger, IActiveEvent event) {
        // TODO make it smarter and faster
        // TODO support context
        return eventTrigger.event.is(event);
    }
}
