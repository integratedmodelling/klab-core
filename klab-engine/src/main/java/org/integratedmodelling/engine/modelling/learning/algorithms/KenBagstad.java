package org.integratedmodelling.engine.modelling.learning.algorithms;

import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.vocabulary.NS;

@Prototype(
        id = "ken.bagstad",
        args = {
                "# import",
                Prototype.TEXT,
                "# method",
                Prototype.TEXT
        },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class KenBagstad extends WekaBayesNet {

}
