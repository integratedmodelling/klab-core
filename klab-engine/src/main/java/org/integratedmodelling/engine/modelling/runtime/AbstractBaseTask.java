/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime;

import java.util.Date;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.utils.NameGenerator;

/**
 * @author ferdinando.villa
 *
 */
public abstract class AbstractBaseTask extends Thread implements ITask {

    protected volatile ITask.Status status = Status.RUNNING;
    protected String                taskId;
    protected IMonitor              monitor;
    protected String                description;
    protected ISession              session;
    protected long                  startTime;
    protected long                  endTime;
    protected ITask                 parent;

    protected AbstractBaseTask(IMonitor monitor) {
        this.session = monitor.getSession();
        this.taskId = NameGenerator.shortUUID();
        this.parent = monitor.getTask();
        this.monitor = ((Monitor) monitor).get(this);
        this.description = "";
        this.startTime = new Date().getTime();
    }

    @Override
    public final String getTaskId() {
        return this.taskId;
    }

    // tasks that have a parent should not generate independent logs or send anything to
    // the client.
    public ITask getParentTask() {
        return parent;
    }
    
    @Override
    public final String getDescription() {
        return this.description;
    }


    @Override
    public String getSecurityKey() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends IIdentity> T getParentIdentity(Class<? extends IIdentity> type) {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public final void interrupt() {
        // _monitor.stop(this);
        synchronized (this.status) {
            this.status = Status.INTERRUPTED;
        }
        this.endTime = new Date().getTime();
        this.monitor.send(Messages.TASK_INTERRUPTED);

    }

    protected Object finish(Object ret) {
        for (;;) {
            synchronized (this.status) {
                if (this.status == Status.FINISHED) {
                    break;
                }
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                }
            }
        }
        return ret;
    }

    @Override
    public final Status getStatus() {
        synchronized (this.status) {
            return this.status;
        }
    }

    @Override
    public final long getStartTime() {
        return this.startTime;
    }

    @Override
    public final long getEndTime() {
        return this.endTime;
    }

    /**
     * Set the status. Only the engine should do this.
     * 
     * @param status
     */
    public void setStatus(Status status) {
        this.status = status;
    }
}
