package org.integratedmodelling.engine.modelling.learning.algorithms;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.JMenuBar;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.beans.ModelArtifact;
import org.integratedmodelling.common.beans.responses.LocalExportResponse;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.modelling.learning.AbstractWEKAProcessContextualizer;
import org.integratedmodelling.engine.modelling.learning.WEKALearningProcess;
import org.integratedmodelling.engine.modelling.learning.WEKALearningProcess.Var;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation.ArtifactGenerator;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.riskwiz.io.Converter;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.net.BIFReader;
import weka.classifiers.bayes.net.EditableBayesNet;
import weka.classifiers.bayes.net.GUI;
import weka.core.Instances;
import weka.gui.GenericObjectEditor;
import weka.gui.LookAndFeel;

@Prototype(
        id = "weka.bayesnet",
        args = {
                "# import",
                Prototype.TEXT,
                "# method",
                Prototype.TEXT
        },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class WekaBayesNet extends AbstractWEKAProcessContextualizer {
    // TODO: Remove this. Shouldn't it be at WekaLearningProcess.instances?
    private Instances discretizedInstances;

    @Override
    protected Classifier createNewClassifier(Instances instances) {
        return new EditableBayesNet(instances);
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation contextSubject, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        Map<String, IObservation> ret = super.initialize(process, contextSubject, resolutionContext, expectedInputs, expectedOutputs, monitor);

        /*
         * add the learned model to the process
         */
        String artifactId = "Learned "
                + NS.getDisplayName(process.getModel().getObservables().get(1).getType())
                + " contextualizer";

        /**
         * TODO move this to the abstract contextualizer after virtualizing the save
         * method and adding GENIE as an output. edit() should also be virtual.
         */
        ((DirectObservation) process).addOutputModel(artifactId, new ArtifactGenerator() {

            @Override
            public LocalExportResponse generateArtifact() {
                LocalExportResponse ret = new LocalExportResponse();

                /**
                 * TODO this should be OK for all classifiers as long as we virtualize
                 * saving of the classifier (in this case on BIF)
                 */
                ret.setModel(true);

                File outfile = null;
                String bif = ((EditableBayesNet) learningProcess.getClassifier())
                        .toXMLBIF03();
                try {
                    outfile = File.createTempFile("bnet", ".bif");
                    FileUtils.writeStringToFile(outfile, bif);
                    ret.getFiles().add(outfile.toString());
                } catch (IOException e) {
                    throw new KlabRuntimeException(e);
                }

                for (File dfile : learningProcess.createDiscretizerSidecarFiles(outfile)) {
                    ret.getFiles().add(dfile.toString());
                }

                String modelStatement = "assess "
                        +  KIM.getDefinition(learningProcess.getOutputState().getObserver());

                int nobs = learningProcess.getPredictors().size();
                int no = 0;

                for (Var v : learningProcess.getPredictors()) {
                    if (no == 0) {
                        modelStatement += "\n\tobserving";
                    } else if (no < nobs) {
                        modelStatement += ",";
                    }
                    modelStatement += "\n\t\t(" + KIM.getDefinition(v.getObserver()) + ") as " + NS.EXPLANATORY_QUALITY_ROLE;
                    no++;
                }

                modelStatement += "\n\tusing weka.bayesnet(import = \"bn/"
                        + MiscUtilities.getFileName(outfile) + "\")\n;";

                ret.setModelStatement(modelStatement);
                ret.setRelativeExportPath("bn");
                return ret;
            }

            @Override
            public void edit() {
                String bif = ((EditableBayesNet) learningProcess.getClassifier())
                        .toXMLBIF03();
                try {
                    File outfile = File.createTempFile("bnet", ".bif");
                    FileUtils.writeStringToFile(outfile, bif);
                    // JavaUtils.exec(GUI.class, false, outfile.toString());
                    new Thread() {

                        @Override
                        public void run() {

                            LookAndFeel.setLookAndFeel();
                            JFrame jf = new JFrame("Bayes Network Editor");
                            final GUI g = new GUI();
                            JMenuBar menuBar = g.getMenuBar();
                            // remove the Exit thingy which will destroy the engine
                            menuBar.getMenu(0).remove(menuBar.getMenu(0).getItemCount() - 1);
                            // and the silly separator
                            menuBar.getMenu(0).remove(menuBar.getMenu(0).getItemCount() - 1);
                            try {
                                g.readBIFFromFile(outfile.toString());
                                jf.setJMenuBar(menuBar);
                                jf.getContentPane().add(g);
                                jf.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                                jf.setSize(800, 600);
                                jf.setVisible(true);
                                GenericObjectEditor.registerEditors();
                            } catch (Exception ex) {
                                monitor.error("error launching Bayesian network editor");
                            }
                        }
                    }.start();
                } catch (Exception e) {
                    throw new KlabRuntimeException(e);
                }
            }
        });

        monitor.send(new ModelArtifact(artifactId, "model", ((Observation) process)
                .getInternalId()));

        ((DirectObservation) process)
                .addOutputDataset("Training dataset (raw)", new ArtifactGenerator() {

                    @Override
                    public LocalExportResponse generateArtifact() {
                        LocalExportResponse ret = new LocalExportResponse();
                        ret.setModel(false);
                        try {
                            File outfile = File.createTempFile("bnet", ".arff");
                            learningProcess.saveData(outfile);
                            ret.getFiles().add(outfile.toString());
                        } catch (Exception e) {
                            throw new KlabRuntimeException(e);
                        }
                        return ret;
                    }

                    @Override
                    public void edit() {
                        // TODO Auto-generated method stub

                    }
                });

        monitor.send(new ModelArtifact("Training dataset (raw)", "dataset", ((Observation) process)
                .getInternalId()));

        ((DirectObservation) process)
                .addOutputDataset("GENIE network", new ArtifactGenerator() {

                    @Override
                    public LocalExportResponse generateArtifact() {
                        LocalExportResponse ret = new LocalExportResponse();
                        ret.setModel(false);
                        String bif = ((EditableBayesNet) learningProcess.getClassifier())
                                .toXMLBIF03();
                        try {
                            File outfile = File.createTempFile("bnet", ".xdsl");
                            File temp = File.createTempFile("bnss", ".bif");
                            FileUtils.writeStringToFile(temp, bif);
                            Converter.bifToGenie(temp.toString(), outfile.toString());
                            ret.getFiles().add(outfile.toString());
                        } catch (Exception e) {
                            throw new KlabRuntimeException(e);
                        }
                        return ret;
                    }

                    @Override
                    public void edit() {
                        // TODO Auto-generated method stub

                    }
                });

        monitor.send(new ModelArtifact("GENIE network", "dataset", ((Observation) process)
                .getInternalId()));

        if (discretizedInstances != null) {
            ((DirectObservation) process)
                    .addOutputDataset("Training dataset (discretized)", new ArtifactGenerator() {

                        @Override
                        public LocalExportResponse generateArtifact() {
                            LocalExportResponse ret = new LocalExportResponse();
                            ret.setModel(false);
                            try {
                                File outfile = File.createTempFile("bnet", ".arff");
                                learningProcess.saveData(outfile, discretizedInstances);
                                ret.getFiles().add(outfile.toString());
                            } catch (Exception e) {
                                throw new KlabRuntimeException(e);
                            }
                            return ret;
                        }

                        @Override
                        public void edit() {
                            // TODO Auto-generated method stub

                        }
                    });

            monitor.send(new ModelArtifact("Training dataset (discretized)", "dataset", ((Observation) process)
                    .getInternalId()));
        }

        return ret;
    }

    @Override
    protected WEKALearningProcess createLearningProcess(IMonitor monitor) {

        WEKALearningProcess wlp = new WEKALearningProcess(this, monitor);
        wlp.setNumericInputAllowed(false);
        return wlp;
    }

    @Override
    protected Classifier loadClassifier(File classifier) {
        Classifier ret = null;
        BIFReader bayesNet = new BIFReader();
        try {
            bayesNet.processFile(classifier.toString());
            ret = new EditableBayesNet(bayesNet);
            // hm
            learningProcess.getInstances().setClassIndex(0);
            ((EditableBayesNet)ret).setData(learningProcess.getInstances());
        } catch (Exception e) {
            throw new KlabRuntimeException(e);
        }
        return ret;
    }

}
