/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime.mediators;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IGrid.Cell;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.IShape.Type;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

import com.vividsolutions.jts.geom.Point;

public class GridToShape extends AbstractMediator implements IState.Mediator {

    private Iterable<Pair<Cell, Double>> cellCoverage;
    Aggregation                          aggregation;
    double                               total        = 0.0;
    IGrid                                originalGrid = null;

    /*
     * if the shape is a point, we just store this.
     */
    IShape                               point        = null;

    public GridToShape(IObservableSemantics observable, IExtent from, IExtent to, IConcept trait) {

        super(observable.getObserver(), trait);

        aggregation = MediationOperations.getAggregator(observable);
        originalGrid = ((ISpatialExtent) from).getGrid();
        IShape sh = ((ISpatialExtent) to).getShape();
        /*
         * clip the shape to the grid
         */
        try {
            sh = ((ShapeValue) sh).intersection(((Grid) originalGrid).getShape());
        } catch (KlabException e) {
            throw new KlabRuntimeException(e);
        }
        if (!sh.isEmpty()) {
            if (sh.getGeometryType() == Type.POINT) {
                this.point = sh;
            } else {
                try {
                    /*
                     * This returns the cells in the ORIGINAL grid that are covered by the
                     * shape, with a weight that expresses by how much.
                     */
                    this.cellCoverage = MediationOperations
                            .getCoveredCells(originalGrid, (IGeometricShape) sh, aggregation != Aggregation.SUM);
                } catch (KlabException e) {
                    throw new KlabRuntimeException(e);
                }

                if (cellCoverage != null && aggregation == Aggregation.SUM) {
                    /*
                     * compute stats for faster propagation
                     */
                    for (Pair<Cell, Double> tc : cellCoverage) {
                        total += tc.getSecond();
                    }
                }
            }
        }
    }

    @Override
    public Object mediateTo(Object value, int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Aggregation getAggregation() {
        return aggregation;
    }

    /*
     * in this one, the index is always 0 (non-Javadoc)
     * 
     * @see org.integratedmodelling.api.modelling.IState.Mediator#mediateFrom(int)
     */
    @Override
    public Object mediateFrom(IState originalState, IScale.Locator... otherLocators) {
        List<Pair<Integer, Double>> ret = new ArrayList<>();
        if (point != null) {
            Point pt = ((IGeometricShape) point).getGeometry().getCentroid();
            int ofs = originalGrid.getOffsetFromWorldCoordinates(pt.getX(), pt.getY());
            ret.add(new Pair<>(ofs, 1.0));
        } else {
            for (Pair<Cell, Double> zio : cellCoverage) {
                ret.add(new Pair<>(zio.getFirst().getOffsetInGrid(), zio.getSecond()));
            }
        }
        return ret;
    }

    @Override
    public List<Locator> getLocators(int index) {

        List<Locator> ret = new ArrayList<>();
        if (point != null) {

            Point pt = ((IGeometricShape) point).getGeometry().getCentroid();
            int ofs = originalGrid.getOffsetFromWorldCoordinates(pt.getX(), pt.getY());
            int[] xy = originalGrid.getXYOffsets(ofs);
            ret.add(SpaceLocator.get(xy[0], xy[1]));

        } else if (cellCoverage != null) {
            for (Pair<Cell, Double> zio : cellCoverage) {
                SpaceLocator loc = SpaceLocator.get(zio.getFirst().getX(), zio.getFirst().getY());
                loc.setWeight(zio.getSecond());
                ret.add(loc);
            }
        }
        return ret;
    }

}
