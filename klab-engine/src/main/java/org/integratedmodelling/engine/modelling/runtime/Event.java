package org.integratedmodelling.engine.modelling.runtime;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveEvent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.exceptions.KlabRuntimeException;

public class Event extends DirectObservation implements IActiveEvent, NetworkSerializable {

    protected Event(IObservableSemantics observable, IActiveDirectObservation contextObservation, IScale scale, IContext context, INamespace namespace, String name,
            IMonitor monitor) {
        super(observable, contextObservation, scale, context, namespace, name, monitor);
    }

    @Override
    public ISubject getContextObservation() {
        return (ISubject)super.getContextObservation();
    }

    @Override
    public Map<IProperty, IState> getObjectStateCopy() {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Event.class)) {
            throw new KlabRuntimeException("cannot serialize a Context to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Event ret = new org.integratedmodelling.common.beans.Event();
        super.serialize(ret);

        return (T) ret;
    }

}
