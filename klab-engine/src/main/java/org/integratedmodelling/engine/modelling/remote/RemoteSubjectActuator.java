/// *******************************************************************************
// * Copyright (C) 2007, 2015:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.engine.modelling.remote;
//
// import java.util.Map;
//
// import org.integratedmodelling.api.modelling.IDirectObservation;
// import org.integratedmodelling.api.modelling.IModel;
// import org.integratedmodelling.api.modelling.IObservable;
// import org.integratedmodelling.api.modelling.IObserver;
// import org.integratedmodelling.api.modelling.ISubject;
// import org.integratedmodelling.api.modelling.resolution.IResolutionContext;
// import org.integratedmodelling.api.modelling.scheduling.ITransition;
// import org.integratedmodelling.api.monitoring.IMonitor;
// import org.integratedmodelling.api.network.INode;
// import org.integratedmodelling.api.services.IPrototype;
// import org.integratedmodelling.common.client.NodeClient;
// import org.integratedmodelling.common.command.ServiceManager;
// import org.integratedmodelling.common.configuration.KLAB;
// import org.integratedmodelling.common.model.actuators.SubjectActuator;
// import org.integratedmodelling.common.network.EngineNetwork;
// import org.integratedmodelling.exceptions.ThinklabException;
//
// public class RemoteSubjectActuator extends SubjectActuator {
//
// ISubject subject;
// IDirectObservation context;
// IMonitor monitor;
// IPrototype prototype;
// Map<String, Object> parameters;
// private INode node;
//
// public RemoteSubjectActuator(IPrototype prototype, Map<String, Object> parameters) {
// this.prototype = prototype;
// this.parameters = parameters;
// this.node = ((EngineNetwork)KLAB.NETWORK).getNodeProviding(prototype.getId());
// }
//
// @Override
// public ISubject initialize(ISubject subject, IDirectObservation context, IResolutionContext
/// resolutionContext, IMonitor monitor)
// throws ThinklabException {
// this.subject = subject;
// this.monitor = monitor;
// this.context = context;
// return subject;
// }
//
// @Override
// public boolean processTransition(ITransition transition, IMonitor monitor)
// throws ThinklabException {
//
// Map<?, ?> result = ((NodeClient) node).call(ServiceManager.get()
// .getSubjectAccessorCall(prototype, subject, parameters, transition, monitor), Map.class);
//
// /*
// * TODO process result
// */
//
// return true;
//
// }
//
// @Override
// public void notifyExpectedOutput(IObservable observable, IObserver observer, String name) {
// // TODO Auto-generated method stub
//
// }
//
// @Override
// public void notifyModel(IModel model) {
// // TODO Auto-generated method stub
//
// }
//
// }
