package org.integratedmodelling.engine.modelling.runtime;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.base.HashableObject;
import org.integratedmodelling.exceptions.KlabException;

public abstract class AbstractSubjectInstantiator extends HashableObject implements ISubjectInstantiator {

    protected IScale               scale;
    protected IResolutionScope     scope;
    protected IActiveSubject       context;
    protected IModel               model;
    protected IMonitor             monitor;
    private boolean                canDispose;
    protected IProvenance.Artifact provenance;

    /**
     * Create all subjects from an object source in a given scale and context.
     * 
     * @param model
     * @param objectSource
     * @param scale
     * @param contextSubject
     * @param monitor
     * @return new subjects
     * @throws KlabException
     */
    public abstract Map<String, IObservation> createSubjects(IActiveSubject context, ITransition transition)
            throws KlabException;

    @Override
    public boolean canDispose() {
        return canDispose;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {
        this.provenance = provenance;
    }

    @Override
    public void initialize(IActiveSubject contextSubject, IResolutionScope context, IModel model, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        this.context = contextSubject;
        this.scope = context;
        this.scale = contextSubject.getScale();
        this.model = model;
        this.monitor = monitor;
    }

    @Override
    public Map<String, IObservation> createSubjects(IActiveSubject context, ITransition transition, Map<String, IState> inputs)
            throws KlabException {
        canDispose = transition == null ? !scale.isTemporallyDistributed() : transition.isLast();
        return createSubjects(context, transition);
    }

}
