/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveRelationship;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

public class Relationship extends DirectObservation implements IActiveRelationship, NetworkSerializable {

    ArrayList<IState> states     = new ArrayList<>();
    ISubject          source;
    ISubject          target;

    public Relationship(IStructure structure, IObservableSemantics observable, IScale scale, INamespace namespace,
            ISubject source, ISubject target, String name, IMonitor monitor) {
        super(observable, (IActiveDirectObservation) structure.getSubject(), scale, source.getContext(), namespace, name, monitor);
        this.source = source;
        this.target = target;
    }

    public void addState(IState state) {
        states.add(state);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Relationship && ((Relationship) obj).internalID.equals(internalID);
    }

    @Override
    public int hashCode() {
        return internalID.hashCode();
    }

    @Override
    public ISubject getContextObservation() {
        return (ISubject)super.getContextObservation();
    }

    @Override
    public ISubject getSource() {
        return source;
    }

    @Override
    public ISubject getTarget() {
        return target;
    }

    @Override
    public Map<IProperty, IState> getObjectStateCopy() {
        HashMap<IProperty, IState> result = new HashMap<>();
        for (IState state : states) {
            if (!(state instanceof IExtent)) {
                result.put(KLAB.p(NS.PART_OF), state);
            }
        }
        return result;
    }

    @Override
    public Collection<IState> getStates() {
        return states;
    }

    @Override
    public IState getStaticState(IObservableSemantics obs) throws KlabException {

        for (IState s : states) {
            if (obs.equals(s.getObservable().getSemantics())) {
                return s;
            }
        }

        if (!NS.isQuality(obs)) {
            throw new KlabValidationException("cannot create a state for a non-quality: "
                    + obs.getType());
        }

//        if (Observables.getInherentType(observable.getType()) == null) {
//            obs = ((ObservableSemantics) obs).withInherentType(this.getObservable().getSemantics().getType());
//        }

        IState ret = States.createStatic(obs, this);
        states.add(ret);
        return ret;
    }

    @Override
    public String toString() {
        return "R" + getObservable().getSemantics() + ": " + getName();
    }

    
    @Override
    public IState getState(IObservableSemantics obs, Object... data) throws KlabException {

        for (IState s : states) {
            if (obs.equals(s.getObservable().getSemantics())) {
                return s;
            }
        }

        if (!NS.isQuality(obs)) {
            throw new KlabValidationException("cannot create a state for a non-quality: "
                    + obs.getType());
        }

//        if (obs.getInherentType() == null) {
//            obs = ((Observable) obs).withInherentType(this.getObservable().getType());
//        }

        IState ret = States.create(obs, this);
        states.add(ret);
        return ret;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Relationship.class)) {
            throw new KlabRuntimeException("cannot serialize a Relationship to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Relationship ret = new org.integratedmodelling.common.beans.Relationship();

        super.serialize(ret);

        ret.setSource(((Subject)this.getSource()).getInternalId());
        ret.setTarget(((Subject)this.getTarget()).getInternalId());
        
        return (T) ret;
    }
}
