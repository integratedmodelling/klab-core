/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling.runtime.mediators;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.exceptions.KlabUnsupportedOperationException;

public class GridToGrid extends AbstractMediator {

    private Aggregation aggregation;
    IGrid gridFrom = null;
    IGrid gridTo = null;
    private int[] subgridOffsets;

    public GridToGrid(IObservableSemantics observable, IExtent from, IExtent to,
            IConcept dataReductionTrait) {

        super(observable.getObserver(), dataReductionTrait);

        this.aggregation = MediationOperations.getAggregator(observable);
        this.gridFrom = ((ISpatialExtent) from).getGrid();
        this.gridTo = ((ISpatialExtent) to).getGrid();
        // IGeometricShape shapeto = (IGeometricShape) ((ISpatialExtent) to).getShape();
        // IGeometricShape shapefrom = (IGeometricShape) ((ISpatialExtent)
        // from).getShape();

        int[] ofs = isSubgrid(gridTo, gridFrom);
        if (ofs != null) {
            this.subgridOffsets = ofs;
        } else {
            throw new KlabUnsupportedOperationException("grid to grid mediation with resampling is still unsupported");
        }
    }

    /**
     * If to is a conformant subgrid of from, return the offsets in it. Use state left in
     * grid during subgrid generation instead of computing conformance, so only works
     * properly with the engine's Grid implementations.
     * 
     * @param gridto
     * @param gridfrom
     * @return
     */
    private int[] isSubgrid(IGrid gridto, IGrid gridfrom) {
        if (gridto instanceof Grid && gridfrom instanceof Grid) {
            if (((Grid) gridto).getParentGridId() != null
                    && ((Grid) gridto).getParentGridId().equals(((Grid)gridfrom).getSignature())) {
                return ((Grid) gridto).getOffsetInParentGrid();
            }
        }
        return null;
    }

    @Override
    public Object mediateTo(Object value, int index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Aggregation getAggregation() {
        return aggregation;
    }


    /*
     * in this one, the index is always 0
     * (non-Javadoc)
     * @see org.integratedmodelling.api.modelling.IState.Mediator#mediateFrom(int)
     */
    @Override
    public Object mediateFrom(IState originalState, IScale.Locator... otherLocators) {
        List<Pair<Integer, Double>> ret = new ArrayList<>();
//        for (Pair<Cell, Double> zio : cellCoverage) {
//
//            /*
//             * TODO must use space locators and whatever else
//             */
//            ret.add(new Pair<Integer, Double>(zio.getFirst().getOffsetInGrid(), zio.getSecond()));
//        }
        return ret;
    }

    @Override
    public List<Locator> getLocators(int index) {

        List<Locator> ret = new ArrayList<>();
        if (subgridOffsets != null) {
            int[] xy = gridTo.getXYOffsets(index);
            SpaceLocator loc = SpaceLocator.get(xy[0] + subgridOffsets[0], xy[1] + subgridOffsets[1]);
            loc.setWeight(1.0);
            ret.add(loc);
        }
        return ret;
    }
}
