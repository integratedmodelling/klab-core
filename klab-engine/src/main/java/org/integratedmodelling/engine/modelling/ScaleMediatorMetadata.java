/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import org.integratedmodelling.common.metadata.Metadata;

public class ScaleMediatorMetadata extends Metadata {
    public enum ScaleMediatorTag {
        differentiable,
        differentiableArbitraryOrders,
        stepFunction,
        linear,
        planar,
        arbitraryDimensions,
        oneDimensional,
        twoDimensional,
        threeDimensional
    }

    /**
     * the main constructor. Tags the various scale interpolator features using the string version of the
     * ScaleMediatorTag ENUM.
     * 
     * calling ScaleMediatorMetadata.getBoolean() will return the intuitively appropriate result: Boolean.TRUE
     * for features that are present, or Boolean.FALSE for those that are not.
     * 
     * @param tags
     */
    public ScaleMediatorMetadata(ScaleMediatorTag... tags) {
        for (ScaleMediatorTag tag : tags) {
            put(tag.name(), Boolean.TRUE);
        }
    }

    /**
     * essentially the same as getBoolean(), but it returns a primitive boolean, and it takes a
     * ScaleMediatorTag so the caller doesn't have to convert it using .name() before calling. Just a simpler
     * interface.
     * 
     * @param key
     * @return true if 
     */
    public boolean is(ScaleMediatorTag key) {
        return (Boolean) get(key.name());
    }
}
