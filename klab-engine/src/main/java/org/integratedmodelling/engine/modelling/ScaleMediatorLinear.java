/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.modelling;

import java.util.Map;
import java.util.Map.Entry;

import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.engine.modelling.ScaleMediatorMetadata.ScaleMediatorTag;
import org.integratedmodelling.exceptions.KlabResourceNotFoundException;

/**
 * Linear translation can handle any arbitrary number of dimensions. Translations are done one dimension at a
 * time.
 * 
 * @author luke
 * 
 */
public class ScaleMediatorLinear extends ScaleMediator {

    protected static final IMetadata metadata = new ScaleMediatorMetadata(ScaleMediatorTag.linear,
                                                      ScaleMediatorTag.oneDimensional);

    public ScaleMediatorLinear(IActiveDirectObservation agent, ObservationController controller)
            throws KlabResourceNotFoundException {
        super(agent, controller);
    }

    @Override
    protected Map<IProperty, IState> generateTargetScale(SubjectObservation observation,
            IScale targetScale) {
        Map<IProperty, IState> result = null;

        Map<IProperty, IState> propertyStates = observation.agentStateNode.getAgentState().getStates();
        ISpatialExtent targetSpace = targetScale.getSpace();

        IProperty observedProperty;
        IState observedState;
        ISpatialExtent observedSpace;
        for (Entry<IProperty, IState> propertyState : propertyStates.entrySet()) {
            observedProperty = propertyState.getKey();
            observedState = propertyState.getValue();
            IState targetState = StateFactory.getEmptyClone(observedState, targetScale);

            /*
             *  TODO
             * 
             *  for (extent : space.extents)
             *  index(extent) by extent.envelope
             * 
             *  (probably do this for the target scale as well?)
             * 
             *  triangulate observed extents via indexed values
             *  (this is the intermediate representation)
             * 
             *  TODO is this a "triangulation" interpolator? (linear is 1-D equivalent)
             */
        }

        observation.outputStates.put(targetScale, result);
        return result;
    }

    @Override
    public void invalidate(ITimeInstant interruptTime) {
        // TODO Auto-generated method stub

    }

    @Override
    public void notify(IObservationGraphNode node) {
        // TODO Auto-generated method stub

    }
}
