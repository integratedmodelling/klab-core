/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.api.annotations.KlabBatchRunner;
import org.integratedmodelling.api.annotations.SubjectType;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.ILock;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.runtime.ITask.Status;
import org.integratedmodelling.api.services.IBatchRunner;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.beans.Notification;
import org.integratedmodelling.common.beans.Task;
import org.integratedmodelling.common.client.ModelingClient;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.network.Broadcaster;
import org.integratedmodelling.common.network.EngineNetwork;
import org.integratedmodelling.common.project.Component;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.project.Workspace;
import org.integratedmodelling.common.utils.Lock;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.engine.modelling.runtime.AbstractBaseTask;
import org.integratedmodelling.engine.modelling.runtime.Context;
import org.integratedmodelling.engine.modelling.runtime.EngineSession;
import org.integratedmodelling.engine.scripting.KimToolkit;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;

/**
 * A modeling engine is required to make any observation. This (and its client
 * counterpart, {@link ModelingClient} are the most likely to be used directly from Java
 * code as engine implementations.
 * 
 * @author ferdinando.villa
 *
 */
public class ModelingEngine extends Engine implements IModelingEngine {

    private static final String LAST_LOADED_PROJECTS_PROPERTY = "klab.localdeploy.lastused";

    private Broadcaster         advertiser;
    private String              communicationChannel          = "/engine/" + NameGenerator.shortUUID();
    private IMonitor            monitor                       = new EngineMonitor();
    private List<Listener>      listeners                     = new ArrayList<>();

    /**
     * The multicast notification bus where we can receive engine notifications and send
     * messages to client sessions.
     * 
     * @return the bus, or null if it was not started.
     */
    @Override
    public Broadcaster getNotificationBus() {
        return advertiser;
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    /*
     * prevent BS warnings from various libs.
     */
    static {
        System.setProperty("com.sun.media.jai.disableMediaLib", "true");
    }

    @Override
    public Collection<IPrototype> getFunctionPrototypes() {

        List<IPrototype> ret = ServiceManager.get().getFunctionPrototypes();

        Set<String> ids = new HashSet<>();
        for (IPrototype p : ret) {
            ids.add(p.getId());
        }

        /*
         * in the engine, also add all prototypes from the network. Nodes just report
         * their own although getFunctionPrototype() will also return remote component
         * functions.
         */
        for (IServer n : getNetwork().getNodes()) {
            for (IPrototype p : n.getFunctionPrototypes()) {
                /*
                 * don't override local prototypes with remote versions.
                 */
                if (!ids.contains(p.getId())) {
                    ret.add(p);
                }
            }
        }

        Collections.sort(ret, new Comparator<IPrototype>() {

            @Override
            public int compare(IPrototype o1, IPrototype o2) {
                return o1.getId().compareTo(o2.getId());
            }
        });
        return ret;

    }

    /**
     * Only use this constructor if the certificate is set later, or for the very limited
     * operation without one.
     * 
     * @throws KlabException
     */
    public ModelingEngine() throws KlabException {
        super();
        this.network = new EngineNetwork();
    }

    /**
     * The regular constructor, parsing a user certificate and connecting to the network
     * through the access point in it.
     * 
     * @param certificate
     * @throws KlabException
     */
    public ModelingEngine(File certificate) throws KlabException {
        super();
        this.network = new EngineNetwork(certificate);
    }

    /**
     * Start the multicast of our IP address and identity so that clients can discover us.
     * This is done automatically in servers.
     * 
     * @param applicationId
     * @param port
     */
    public void startAdvertising(String applicationId, int port) {
        KLAB.info("Starting multicast of IP on cluster " + applicationId + " for port " + port);
        this.advertiser = new Broadcaster(KLAB.ENGINE.getName(), applicationId, port);
    }

    @Override
    protected final void startup() throws KlabException {

        super.startup();

        registerAnnotation(SubjectType.class, new AnnotationHandler() {
            @SuppressWarnings("unchecked")
            @Override
            public void processAnnotatedClass(Annotation annotation, Class<?> cls) {
                String concept = ((SubjectType) annotation).value();
                if (ISubject.class.isAssignableFrom(cls)) {
                    KLAB.MMANAGER.registerSubjectClass(concept, (Class<? extends ISubject>) cls);
                }
            }
        });

        registerAnnotation(KlabBatchRunner.class, new AnnotationHandler() {
            @SuppressWarnings("unchecked")
            @Override
            public void processAnnotatedClass(Annotation annotation, Class<?> cls) {
                String id = ((KlabBatchRunner) annotation).id();
                if (IBatchRunner.class.isAssignableFrom(cls)) {
                    KLAB.registerRunnerClass(id, (Class<? extends IBatchRunner>) cls);
                }
            }
        });

        registerAnnotation(KimToolkit.class, new AnnotationHandler() {
            @SuppressWarnings("unchecked")
            @Override
            public void processAnnotatedClass(Annotation annotation, Class<?> cls) {
                KLAB.MMANAGER.registerKimImportClass(cls);
            }
        });

        scanPackage("org.integratedmodelling");

        this.lock = new Lock(".lck");

        KLAB.WORKSPACE = new Workspace(KLAB.CONFIG.getDataPath("workspace"), true);

        if (!((EngineNetwork) network).initialize()) {
            throw new KlabIOException("error initializing network. Check connectivity, online status, remote server status or certificate.");
        }

        /**
         * Create native authorities. Errors are pretty much fatal but for now allow to
         * proceed.
         */
        loadAuthorities();

        ((ProjectManager) KLAB.PMANAGER).deployComponents();

        /**
         * Ensure that local workspace projects override those deployed from network. This
         * shouldn't happen as the workspace is smart about it, but the network
         * automatically registers anything downloaded - FIXME check that.
         */
        ((ProjectManager) KLAB.PMANAGER).overrideDeployed(KLAB.WORKSPACE);

        KLAB.PMANAGER.load(true, KLAB.MFACTORY.getRootParsingContext());

        KLAB.info("loading component knowledge");

        /**
         * Initialize them
         */
        for (IComponent component : KLAB.PMANAGER.getComponents()) {
            ((Component) component).initialize(monitor);
        }

        for (IComponent component : KLAB.PMANAGER.getComponents()) {
            KLAB.PMANAGER.loadComponent(component, KLAB.MFACTORY.getRootParsingContext());
        }

        /*
         * clear configured projects so that any new local deployment is recorded as new.
         */
        KLAB.CONFIG.getProperties().setProperty(LAST_LOADED_PROJECTS_PROPERTY, "");
        KLAB.CONFIG.save();

    }

    /**
     * Entry point in Thinklab: call boot() before you do anything. Calling more than once
     * without calling shutdown() has no effect.
     * 
     * @throws KlabException
     */
    public static void boot() throws KlabException {
        if (KLAB.ENGINE == null) {
            KLAB.ENGINE = new ModelingEngine();
        }
        ((ModelingEngine) (KLAB.ENGINE)).startup();
    }

    @Override
    public void shutdown(int seconds) {

        if (lock.isLocked()) {
            lock.unlock(getUser());
        }

        if (advertiser != null) {
            advertiser.stop();
        }
        super.shutdown(seconds);
    }

    @SuppressWarnings("javadoc")
    public ILock getLock() {
        return lock;
    }

    @Override
    public String getName() {
        return KLAB.NAME;
    }

    @Override
    public String getUrl() {
        return network.getUrl();
    }

    @Override
    public boolean isRunning() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String submitObservation(IDirectObserver observer, boolean store) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<IObservationMetadata> queryObservations(String text, boolean localOnly) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<IModelMetadata> queryModels(IObservableSemantics observable, IResolutionScope context)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITask setupComponent(String componentId) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void removeObservations(Collection<String> observationNames) throws KlabException {
        // TODO Auto-generated method stub

    }

    @Override
    public IModelingEngine start() {
        try {
            extractKnowledge();
            startup();
        } catch (Exception e) {
            return null;
        }
        return this;
    }

    @Override
    public boolean stop() {
        shutdown(0);
        return true;
    }

    /**
     * an engine that sends notifications through the multicast bus rather than logging.
     */
    public static class EngineMonitor extends Monitor {

        /*
         * get the STOMP channel for the currently monitored context
         */
        String getChannel() {
            String ret = KLAB.ENGINE.getName();
            if (getSession() != null) {
                ret += "/session/" + getSession().getId();
            } else if (KLAB.ENGINE instanceof IModelingEngine) {
                ret = ((IModelingEngine) KLAB.ENGINE).getCommunicationChannel();
            }
            return ret;
        }

        @Override
        public void info(Object info, String infoClass) {
            if (getSession() == null || ((ModelingEngine) KLAB.ENGINE).getNotificationBus() == null) {
                KLAB.info(info);
            } else {
                Notification n = getNotification();
                n.setBody(info == null ? null : info.toString());
                n.setNotificationClass(infoClass);
                n.defineLevel(Level.INFO);
                ((ModelingEngine) KLAB.ENGINE).getNotificationBus().send(n, getChannel());
            }
        }

        @Override
        public void warn(Object o) {
            if (getSession() == null || ((ModelingEngine) KLAB.ENGINE).getNotificationBus() == null) {
                KLAB.warn(o);
            } else {
                Notification n = getNotification();
                n.setBody(payloadToString(o));
                n.defineLevel(Level.WARNING);
                ((ModelingEngine) KLAB.ENGINE).getNotificationBus().send(n, getChannel());
            }
        }

        @Override
        public void error(Object o) {

            if (getTask() != null) {
                ((AbstractBaseTask) getTask()).setStatus(Status.ERROR);
                send(o, Messages.TASK_FAILED);
            }

            if (getSession() == null || ((ModelingEngine) KLAB.ENGINE).getNotificationBus() == null) {
                KLAB.error(o);
            } else {
                Notification n = getNotification();
                n.setBody(payloadToString(o));
                n.defineLevel(Level.SEVERE);
                ((ModelingEngine) KLAB.ENGINE).getNotificationBus().send(n, getChannel());
            }
        }

        @Override
        public void debug(Object o) {
            if (KLAB.CONFIG.isDebug()) {
                if (getSession() == null || ((ModelingEngine) KLAB.ENGINE).getNotificationBus() == null) {
                    KLAB.debug(o);
                } else {
                    Notification n = getNotification();
                    n.setBody(payloadToString(o));
                    n.defineLevel(Level.FINE);
                    ((ModelingEngine) KLAB.ENGINE).getNotificationBus().send(n, getChannel());
                }
            }
        }

        private String payloadToString(Object o) {
            if (o instanceof Throwable) {
                // get it logged, too
                KLAB.error(o);
                Throwable t = (Throwable) o;
                o = ExceptionUtils.getMessage((Throwable) o);
                if (o == null || o.toString().isEmpty()) {
                    o = ExceptionUtils.getStackTrace(t);
                }
            }
            return o == null ? "(null)" : o.toString();
        }

        public void send(Object o, String notificationClass) {

            if (getSession() == null || ((ModelingEngine) KLAB.ENGINE).getNotificationBus() == null) {
                KLAB.info(o);
            } else {

                Notification n = getNotification();
                n.defineLevel(Level.INFO);

                if (o instanceof Throwable) {
                    n.setBody(ExceptionUtils.getFullStackTrace((Throwable) o));
                    n.setExceptionClass(o.getClass().getCanonicalName());
                    if (notificationClass != null) {
                        n.setNotificationClass(notificationClass);
                    }
                    n.defineLevel(Level.SEVERE);
                } else if (o instanceof ITask) {
                    n.setTask(KLAB.MFACTORY.adapt(o, Task.class));
                    n.setTaskId(((ITask) o).getTaskId());
                    n.setContextId(((ITask) o).getContext().getId());
                    n.setSessionId(((ITask) o).getContext().getSession().getId());
                } else if (o instanceof IContext) {
                    n.setContext(KLAB.MFACTORY.adapt(o, org.integratedmodelling.common.beans.Context.class));
                    n.setContextId(((IContext) o).getId());
                    n.setSessionId(((IContext) o).getSession().getId());
                } else {

                    if (o instanceof String) {
                        /*
                         * if we're sending a task message and the context has changed,
                         * send it along. If the context changes, it's always because of a
                         * task, so this will only happen here.
                         */
                        if (o.toString().startsWith(Messages.TASK_MESSAGE_PREFIX)) {
                            if (getTask().getParentTask() == null && ((Context) getContext()).hasDeltas()) {
                                n.setContext(KLAB.MFACTORY
                                        .adapt(getContext(), org.integratedmodelling.common.beans.Context.class));
                            }
                            if (o.toString().equals(Messages.TIME_TRANSITION)) {
                                /*
                                 * pass along the task with the transition
                                 */
                                n.setTask(KLAB.MFACTORY.adapt(getTask(), Task.class));
                                n.setTaskId(getTask().getTaskId());
                                n.setContextId(getContext().getId());
                                n.setSessionId(getContext().getSession().getId());
                            }
                        }
                        n.setNotificationClass(notificationClass == null ? o.toString() : notificationClass);
                        if (notificationClass != null) {
                            n.setBody(o.toString());
                        }

                    } else if (o instanceof IModelBean) {

                        /*
                         * wrap the bean into a notification to be pushed to receive() at
                         * the receiving end.
                         */
                        n.defineLevel(Level.CONFIG);
                        n.setBody(((ModelingEngine) KLAB.ENGINE).advertiser.toJSON((IModelBean) o));
                        n.setNotificationClass(o.getClass().getCanonicalName());
                    }
                }

                ((ModelingEngine) KLAB.ENGINE).getNotificationBus().send(n, getChannel());
            }
        }

        @Override
        public void send(Object o) {
            send(o, null);
        }

        public void setContext(Context context) {
            this.context = context;
        }
    }

    @Override
    public ISession createSession(IUser user) throws KlabException {
        return new EngineSession(user);
    }

    @Override
    public IUser getUser() {
        return ((EngineNetwork) network).getUser();
    }

    @Override
    public boolean provides(Object id) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    @Override
    public String getCommunicationChannel() {
        return communicationChannel;
    }

    public void registerClientProject(File location) {
        String prop = KLAB.CONFIG.getProperties().getProperty(LAST_LOADED_PROJECTS_PROPERTY, "");
        prop += (prop.isEmpty() ? "" : ",") + location;
        KLAB.CONFIG.getProperties().setProperty(LAST_LOADED_PROJECTS_PROPERTY, prop);
        KLAB.CONFIG.save();
    }

    public List<INamespace> loadLastUsedClientWorkspace() throws KlabException {

        // FIXME wrong - this should be loaded before clearing it in startup(), and the
        // resulting file array used here.

        String[] prop = KLAB.CONFIG.getProperties().getProperty(LAST_LOADED_PROJECTS_PROPERTY, "").split(",");
        for (String wfile : prop) {
            File file = new File(wfile);
            if (file.exists() && file.isDirectory()) {
                KLAB.PMANAGER.registerProject(file);
            }
            return KLAB.PMANAGER.load(false, KLAB.MFACTORY.getRootParsingContext());
        }
        return new ArrayList<>();
    }
}
