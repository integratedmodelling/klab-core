/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.scripting;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;

public class ScriptEngine {

    static IMonitor monitor;

    public static Object execute(File script) throws KlabException {
        try {
            return execute(FileUtils.readFileToString(script));
        } catch (IOException e) {
            throw new KlabIOException(e);
        }
    }

    public static Object execute(String script) throws KlabException {
        KIMScript expr = new KIMScript(script);
        return expr.eval(getEnvironment(), getMonitor());
    }
    
    public static Object execute(String script, Map<String, Object> parameters, IObservation context, IMonitor monitor) throws KlabException {
        KIMScript expr = new KIMScript(script, context);
        return expr.eval(parameters, monitor);
    }
    
    public static ITask executeAsynchronous(String script, Map<String, Object> parameters, IObservation context, IMonitor monitor) throws KlabException {
        KIMScript expr = new KIMScript(script, context);
        /*
         * TODO return a task that evals the script with its own monitor
         */
        return null;
    }

    private static IMonitor getMonitor() {
        if (monitor == null) {
            monitor = new ScriptMonitor();
        }
        return monitor;
    }

    private static Map<String, Object> getEnvironment() {
        Map<String, Object> ret = new HashMap<>();
        return ret;
    }

}
