/// *******************************************************************************
// * Copyright (C) 2007, 2015:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.engine.commandline;
//
// import java.awt.BorderLayout;
// import java.awt.Color;
// import java.awt.Container;
// import java.awt.Dimension;
// import java.awt.Font;
// import java.awt.GridLayout;
// import java.io.BufferedWriter;
// import java.io.File;
// import java.io.FileWriter;
// import java.io.IOException;
// import java.io.Reader;
// import java.util.List;
// import java.util.logging.Level;
//
// import javax.swing.BorderFactory;
// import javax.swing.JFrame;
// import javax.swing.JPanel;
//
// import org.apache.commons.io.FileUtils;
// import org.apache.commons.lang.StringEscapeUtils;
// import org.integratedmodelling.Version;
// import org.integratedmodelling.api.monitoring.IMonitor;
// import org.integratedmodelling.api.runtime.ISession;
// import org.integratedmodelling.api.services.IServiceCall;
// import org.integratedmodelling.common.command.ServiceManager;
// import org.integratedmodelling.common.configuration.KLAB;
// import org.integratedmodelling.engine.runtime.Session;
// import org.integratedmodelling.exceptions.ThinklabException;
//
// import bsh.util.JConsole;
//
/// **
// * A simple command-line driven interface, using the graphical BeanShell console.
// *
// * @author Ferdinando Villa
// */
// public class GraphicalShell {
//
// JConsole console = null;
//
// File historyFile = null;
// int task = 0;
//
// Font inputFont = new Font("Courier", Font.BOLD, 12);
// Font outputFont = new Font("Courier", Font.PLAIN, 12);
//
// public class InteractiveMonitor implements IMonitor {
//
// int nErrors = 0;
// Level logLevel = Level.INFO;
//
// InteractiveMonitor() {
// task++;
// }
//
// @Override
// public void warn(Object o) {
// GraphicalShell.this.warn(o.toString());
// }
//
// @Override
// public void info(Object info, String infoClass) {
// GraphicalShell.this.info(info.toString());
// }
//
// @Override
// public void error(Object o) {
// nErrors++;
// GraphicalShell.this.error(o.toString());
// }
//
// @Override
// public void debug(Object o) {
// }
//
// @Override
// public ISession getSession() {
// // TODO Auto-generated method stub
// return session;
// }
//
// }
//
// public class ConsoleSession extends Session {
//
// public ConsoleSession() throws ThinklabException {
// super();
// }
//
// // @Override
// // public PrintStream out() {
// // return console.getOut();
// // }
// //
// // @Override
// // public InputStream in() {
// // return console.getInputStream();
// // }
// }
//
// public class ConsolePanel extends JFrame {
//
// private static final long serialVersionUID = -1303258585100820402L;
//
// public ConsolePanel() {
// super("Thinklab console");
// Container content = getContentPane();
// content.setBackground(Color.lightGray);
// JPanel controlArea = new JPanel(new GridLayout(2, 1));
// content.add(controlArea, BorderLayout.EAST);
// console = new JConsole();
// console.setFont(outputFont);
// // Preferred height is irrelevant, since using WEST region
// console.setPreferredSize(new Dimension(600, 400));
// console.setBorder(BorderFactory.createLineBorder(Color.blue, 2));
// console.setBackground(Color.white);
// content.add(console, BorderLayout.WEST);
// pack();
// setVisible(true);
// }
// }
//
// public ISession session;
// public IMonitor monitor;
//
// private boolean error;
//
// public GraphicalShell() throws ThinklabException {
// this.session = new ConsoleSession();
// this.monitor = new InteractiveMonitor();
// historyFile = new File(KLAB.CONFIG.getScratchArea() + File.separator + ".history");
// }
//
// public void printStatusMessage() {
//
// console.println("k.LAB engine v" + Version.CURRENT);
// console.println("Workspace: " + KLAB.CONFIG.getDataPath());
// console.println();
//
// console.println("Enter \'help\' for a list of commands; \'exit\' quits");
// console.println();
// }
//
// public void info(String s) {
// console.setForeground(Color.blue);
// console.println(s);
// try {
// Thread.sleep(100);
// } catch (InterruptedException e) {
// }
// console.setForeground(Color.black);
// }
//
// public void warn(String s) {
// console.setForeground(Color.orange);
// console.println(s);
// try {
// Thread.sleep(100);
// } catch (InterruptedException e) {
// }
// console.setForeground(Color.black);
// }
//
// public void error(String s) {
// console.setForeground(Color.red);
// console.println(s);
// try {
// Thread.sleep(100);
// } catch (InterruptedException e) {
// }
// console.setForeground(Color.black);
// }
//
// /*
// * can't believe what this reader reads, and I'm sure there are things I did not
// * figure out, but this works, and I'm not spending one more second on this.
// */
// public static String readLine(Reader reader) throws Exception {
// String ret = "";
// char[] bytes = new char[1024];
// while (true) {
// reader.read(bytes);
// ret = StringEscapeUtils.unescapeJava(new String(bytes)).trim();
// if (!ret.isEmpty()) {
// break;
// }
// }
// return ret;
// }
//
// public void startConsole() throws Exception {
//
// ConsolePanel jpanels = new ConsolePanel();
//
// /*
// * read history if any
// */
// List<?> lines = null;
// try {
// lines = FileUtils.readLines(historyFile);
// } catch (IOException e) {
// // no problem
// }
//
// if (lines != null) {
// for (Object line : lines) {
// // console.addToHistory(line.toString());
// }
// }
//
// /* greet user */
// printStatusMessage();
//
// String input = "";
//
// /* define commands from user input */
// while (true) {
//
// console.print("> ");
// console.setFont(inputFont);
//
// input = readLine(console.getIn()).trim();
// console.setFont(outputFont);
//
// if ("exit".equals(input)) {
//
// console.println("shell terminated");
// System.exit(0);
// break;
//
// } else if (input.startsWith("!")) {
//
// String ss = input.substring(1);
// // for (int i = console.getHistory().size(); i > 0; i--) {
// // String s = console.getHistory().get(i - 1);
// // if (s.startsWith(ss)) {
// // console.println(s);
// // execute(s);
// // break;
// // }
// // }
//
// } else if (!("".equals(input)) && /* WTF? */!input.equals(";")) {
//
// execute(input);
//
// // TODO see if we want to exclude commands that created errors.
// if (/*!error*/true) {
// BufferedWriter bw = null;
// try {
// bw = new BufferedWriter(new FileWriter(historyFile, true));
// bw.write(input);
// bw.newLine();
// bw.flush();
// } catch (IOException ioe) {
// } finally {
// if (bw != null)
// try {
// bw.close();
// } catch (IOException ioe2) {
// }
// }
// }
// }
// }
// }
//
// private void execute(String input) {
//
// try {
// this.error = false;
//
// IServiceCall cmd = ServiceManager.get().parseCommandLine(input, monitor, session);
//
// if (cmd == null) {
// console.setForeground(Color.red);
// console.println(" unrecognized input: " + input);
// console.setForeground(Color.black);
// return;
// }
//
// Object result = cmd.execute();
//
// if (result != null) {
// console.println(result.toString());
// }
// // console.getOut().flush();
//
// } catch (Exception e) {
//
// e.printStackTrace();
// this.error = true;
// console.setForeground(Color.red);
// console.println(" " + e.getMessage());
// console.setForeground(Color.black);
// }
//
// /*
// * give it a little rest to help the output show entirely before the prompt
// * is printed again.
// */
// try {
// Thread.sleep(600);
// } catch (InterruptedException e) {
// }
// }
// }
