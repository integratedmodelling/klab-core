/// *******************************************************************************
// * Copyright (C) 2007, 2015:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.engine.commandline;
//
// import java.io.IOException;
// import java.util.Iterator;
// import java.util.Map;
//
// import org.integratedmodelling.Version;
// import org.integratedmodelling.api.monitoring.ILegacyMonitor;
// import org.integratedmodelling.api.runtime.ILegacySession;
// import org.integratedmodelling.api.services.IServiceCall;
// import org.integratedmodelling.common.command.ServiceManager;
// import org.integratedmodelling.common.configuration.KLAB;
// import org.integratedmodelling.common.utils.MiscUtilities;
// import org.integratedmodelling.engine.modelling.monitoring.Monitor;
// import org.integratedmodelling.engine.runtime.Session;
// import org.integratedmodelling.exceptions.ThinklabException;
//
// import jline.console.ConsoleReader;
//
/// **
// * A simple command-line driven interface. Just attach to a session, startConsole() and type 'help'.
// * @author Ferdinando Villa
// */
// public class Shell {
//
// public ILegacySession session;
// public ILegacyMonitor monitor;
// ConsoleReader console = null;
//
// public Shell(ILegacySession session) {
// this.session = session;
// }
//
// public Shell() {
// this.session = new Session();
// this.monitor = new Monitor(-1, session);
// }
//
// public void printStatusMessage() throws IOException {
//
// String buildInfo = "";
// if (!Version.VERSION_BUILD.equals("VERSION_BUILD")) {
// buildInfo = " build " + Version.VERSION_BUILD + " (" + Version.VERSION_BRANCH + " "
// + Version.VERSION_DATE + ")";
// }
// console.println("ThinkLab server v" + Version.CURRENT + buildInfo);
// // console.println("Workspace: " + Thinklab.get().getWorkspace());
// console.println("\n");
// console.println("Enter \'help\' for a list of commands; \'exit\' quits\n");
// console.println("\n");
// }
//
// public void startConsole() throws Exception {
//
// // Terminal.setupTerminal();
// this.console = new ConsoleReader();
//
// /* greet user */
// printStatusMessage();
//
// String input = "";
//
// /* define commands from user input */
// while (true) {
//
// input = console.readLine("> ");
//
// if (input == null)
// continue;
//
// if ("exit".equals(input)) {
// console.println("shell terminated\n");
// break;
// } else if (!("".equals(input))) {
//
// try {
//
// IServiceCall cmd = ServiceManager.get().parseCommandLine(input, monitor, session);
//
// if (cmd == null)
// return;
//
// outputResult(cmd.execute());
//
// } catch (ThinklabException e) {
// e.printStackTrace();
// console.println(" error: " + e.getMessage() + "\n");
// }
// }
// }
//
// }
//
// public static void runScript(String s, ILegacySession session) throws ThinklabException {
//
// for (String input : MiscUtilities.readFileIntoStrings(s)) {
//
// input = input.trim();
// if (input == null || input.isEmpty() || input.startsWith("#"))
// continue;
//
// if ("exit".equals(input)) {
// break;
// } else {
//
// try {
//
// IServiceCall cmd = ServiceManager.get()
// .parseCommandLine(input, new Monitor(-1, session), session);
//
// if (cmd == null)
// return;
//
// Object result = cmd.execute();
//
// if (result != null)
// KLAB.info(cmd + " -> " + result.toString());
//
// } catch (ThinklabException e) {
// KLAB.error("executing " + input, e);
// }
// }
// }
// }
//
// public void outputResult(Object ret) throws IOException {
//
// if (ret == null) {
// return;
// }
//
// if (ret instanceof Map) {
// for (Object o : ((Map<?, ?>) ret).keySet()) {
// console.println(o + " = " + ((Map<?, ?>) ret).get(o) + "\n");
// }
// } else if (ret instanceof Iterable) {
// for (Iterator<?> it = ((Iterable<?>) ret).iterator(); it.hasNext();) {
// console.println(it.next() + "\n");
// }
// } else {
// console.println(ret + "\n");
// }
// }
// }
