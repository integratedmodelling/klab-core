/*******************************************************************************
 * Copyright (C) 2007, 2014:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis;

import java.awt.image.RenderedImage;
import java.util.Collection;

import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;

import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IGridMask;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.GeotoolsVectorCoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.DummyActivationLayer;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterActivationLayer;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.opengis.feature.simple.SimpleFeature;

public class ThinklabRasterizer {

    /**
     * Convert the passed vector coverage into a raster coverage that adopts the passed
     * extent.
     * 
     * @param vCoverage
     * @param extent
     * @return a raster coverage
     */
    public static RasterCoverage rasterize(GeotoolsVectorCoverage vCoverage, String valueId, float noData, Grid extent, IObserver observer, String valueDefault, IMonitor monitor, boolean isWFS)
            throws KlabException {

        if (extent.getCRS() != null)
            vCoverage = (GeotoolsVectorCoverage) vCoverage.requireMatch(extent, observer, monitor, false);

        GridCoverage2D coverage = null;
        FeatureRasterizer rasterizer = new FeatureRasterizer(extent, noData, (valueId == null ? null
                : vCoverage.getAttributeDescriptor(valueId)), monitor);

        FeatureIterator<SimpleFeature> iterator = null;
        try {

            ReferencedEnvelope dataEnvelope = null;

            try {
                dataEnvelope = extent.getEnvelope().transform(vCoverage.getCoordinateReferenceSystem(), true);
            } catch (Exception e) {
                throw new KlabValidationException(e);
            }

            iterator = vCoverage.getFeatureIterator(extent.getEnvelope(), valueId);

            coverage = rasterizer.rasterize(vCoverage.getLayerName() + "_" + (valueId == null ? "" : valueId)
                    + "_raster", iterator, valueId, observer, valueDefault, dataEnvelope,
                    // FIXME this MAY work for WFS in the current implementation, but how
                    // the hell do I know.
                    Geospace.getCRSIdentifier(dataEnvelope.getCoordinateReferenceSystem(), true)
                            .equals("EPSG:4326")
                            && isWFS);

        } finally {
            if (iterator != null)
                iterator.close();
        }

        RasterCoverage ret = new RasterCoverage(vCoverage.getLayerName() + "_"
                + (valueId == null ? "" : valueId) + "_raster", coverage);

        ret.setRasterized(true);

        if (rasterizer.getClassification() != null) {
            ret.setClassMappings(rasterizer.getClassification());
        }

        return ret;
    }

    private static IGridMask rasterizeShape(ShapeValue shape, IGrid grid, int value) throws KlabException {

        if (grid.getXCells() * grid.getYCells() < 16) {
            return new DummyActivationLayer(grid);
        }

        RasterActivationLayer ret = (RasterActivationLayer) createMask(grid);
        GridCoverage2D coverage = null;

        ret.deactivate();
        FeatureRasterizer rasterizer = new FeatureRasterizer(grid, 0.0f, null, null);
        coverage = rasterizer.rasterize(shape, value);

        /*
         * turn coverage into mask
         */
        RenderedImage image = coverage.getRenderedImage();
        RandomIter itera = RandomIterFactory.create(image, null);

        for (int i = 0; i < grid.getCellCount(); i++) {

            int[] xy = grid.getXYOffsets(i);

            if (itera.getSampleDouble(xy[0], xy[1], 0) > 0.0) {
                ret.activate(xy[0], xy[1]);
            }
        }
        return ret;
    }

    public static IGridMask rasterizeShapes(Collection<ShapeValue> shapes, IGrid grid, int value)
            throws KlabException {

        if (grid.getXCells() * grid.getYCells() < 16) {
            return new DummyActivationLayer(grid);
        }

        RasterActivationLayer ret = (RasterActivationLayer) createMask(grid);
        GridCoverage2D coverage = null;

        ret.deactivate();
        FeatureRasterizer rasterizer = new FeatureRasterizer(grid, 0.0f, null, null);
        coverage = rasterizer.rasterize(shapes, value);

        /*
         * turn coverage into mask
         */
        RenderedImage image = coverage.getRenderedImage();
        RandomIter itera = RandomIterFactory.create(image, null);

        for (int i = 0; i < grid.getCellCount(); i++) {

            int[] xy = grid.getXYOffsets(i);

            if (itera.getSampleDouble(xy[0], xy[1], 0) > 0.0) {
                ret.activate(xy[0], xy[1]);
            }
        }
        return ret;
    }

    /**
     * Expensive but quick to write object density calculation.
     * 
     * @param shapes
     * @param grid
     * @param value
     * @return
     * @throws KlabException
     */
    public static int[] countShapes(Collection<ShapeValue> shapes, IGrid grid) throws KlabException {

        int[] ret = new int[grid.getCellCount()];

        if (grid.getXCells() * grid.getYCells() < 16) {
            return ret;
        }

        for (ShapeValue shape : shapes) {
            IGridMask mask = rasterizeShape(shape, grid, 1);
            for (int x = 0; x < ret.length; x++) {
                if (mask.isActive(x)) {
                    ret[x]++;
                }
            }
        }

        return ret;
    }

    public static IGridMask createMask(IGrid grid) {
        RasterActivationLayer ret = new RasterActivationLayer(grid.getXCells(), grid
                .getYCells(), false, grid);
        ret.setCRS(((Grid) grid).getCRS());
        return ret;
    }

    public static IGridMask createMask(Collection<ShapeValue> shape, IGrid grid) throws KlabException {
        return rasterizeShapes(shape, grid, 1);
    }

    public static IGridMask createMask(ShapeValue shape, IGrid grid) throws KlabException {
        return rasterizeShape(shape, grid, 1);
    }

    public static IGridMask addToMask(ShapeValue shape, IGridMask mask) throws KlabException {
        mask.or(rasterizeShape(shape, mask.getGrid(), 1));
        return mask;
    }

}
