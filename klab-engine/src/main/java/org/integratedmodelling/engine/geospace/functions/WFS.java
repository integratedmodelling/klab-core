/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.functions;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.space.IAreal;
import org.integratedmodelling.common.kim.expr.CodeExpression;
import org.integratedmodelling.common.utils.URLUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.datasources.WFSCoverageDataSource;
import org.integratedmodelling.exceptions.KlabException;

@Prototype(id = "wfs", args = {
        "# invert-coordinates",
        Prototype.BOOLEAN,
        "# urn",
        Prototype.TEXT,
        "# service",
        Prototype.TEXT,
        "# id",
        Prototype.TEXT,
        "# attr",
        Prototype.TEXT,
        "# filter",
        Prototype.TEXT,
        "# extract",
        Prototype.TEXT,
        "# value-type",
        Prototype.TEXT,
        "# default-value",
        Prototype.TEXT,
        "# table",
        Prototype.TEXT,
        "# urn",
        Prototype.TEXT,
        "# simplify-shapes",
        Prototype.BOOLEAN }, returnTypes = { NS.DATASOURCE, NS.SHAPE, NS.OBJECTSOURCE })
public class WFS extends CodeExpression implements IExpression, IAreal {

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws KlabException {

        String id = null;
        String service = null;

        if (parameters.containsKey("urn")) {

            /*
             * leave the service blank, this will signal to the datasource that we're
             * using a URN that we'll resolve when we read the data.
             */
            id = parameters.get("urn").toString();

            // Datarecord dr =
            // DataAssetResolver.getDatarecord(parameters.get("urn").toString(), monitor);
            // if (dr != null) {
            // service = dr.getAttribute(IDataAsset.KEY_URL);
            //
            // /*
            // * when lucky, this is the complete GS id.
            // */
            // id = dr.getAttribute(IDataAsset.KEY_PUBLISHED_DATAID);
            //
            // /*
            // * patch to avoid having to republish every individual file, or Luke having
            // * to do work.
            // *
            // * FIXME get into the collaboration code and clean this mess up once and for
            // all.
            // */
            // if (id == null) {
            // id = dr.getAttribute(IDataAsset.KEY_DATAID);
            //
            // if (dr.getAttribute(IDataAsset.KEY_NAMESPACE) != null
            // && !dr.getAttribute(IDataAsset.KEY_NAMESPACE).isEmpty()) {
            // /*
            // * FIXME all this needs to end.
            // * Jesus, will this ever be finished.
            // */
            // id = dr.getAttribute(IDataAsset.KEY_NAMESPACE) + ":"
            // + dr.getAttribute(IDataAsset.KEY_SERVER) + "_"
            // + dr.getAttribute(IDataAsset.KEY_NAMESPACE).replaceAll("-", "_") + "_" +
            // id;
            // }
            // } else {
            // if (dr.getAttribute(IDataAsset.KEY_NAMESPACE) != null
            // && !dr.getAttribute(IDataAsset.KEY_NAMESPACE).isEmpty()) {
            // id = dr.getAttribute(IDataAsset.KEY_NAMESPACE) + ":" + id;
            // }
            // }
            //
            // }

        } else {

            Object serv = parameters.get("service");
            if (serv instanceof String) {
                service = serv.toString();
            } else if (serv instanceof String[]) {
                String[] ss = (String[]) serv;
                for (String s : ss) {
                    if (URLUtils.ping(s)) {
                        service = s;
                        break;
                    }
                }
            }
            id = parameters.get("id").toString();
        }

        if (id == null)
            return null;

        String attribute = parameters.containsKey("attr") ? parameters.get("attr").toString()
                : null;
        String filter = parameters.containsKey("filter") ? parameters.get("filter").toString() : null;
        String valueType = parameters.containsKey("value-type") ? parameters.get("value-type").toString()
                : null;
        String valueDefault = parameters.containsKey("default-value") ? parameters.get("default-value")
                .toString() : null;
        String extract = parameters.containsKey("extract") ? parameters.get("extract").toString() : null;
        boolean simplify = parameters.containsKey("simplify-shapes")
                && parameters.get("simplify-shapes").toString().equals("true");

        if (attribute != null && attribute.equals(IModelMetadata.PRESENCE_ATTRIBUTE)) {
            attribute = null;
        }

        Boolean invertCoordinates = (Boolean) parameters.get("invert-coordinates");

        if (filter == null && extract != null) {
            filter = (extract.isEmpty() || extract.equals("true")) ? null : extract;
        }

        Object ret = new WFSCoverageDataSource(service, id, attribute, filter, valueType, valueDefault, invertCoordinates, simplify);

        if (extract != null) {

            /*
             * extract the first shape
             */
            ret = ((WFSCoverageDataSource) ret).extractFirst()
                    .transform(Geospace.getCRSFromID("urn:ogc:def:crs:EPSG:4326"));
        }

        return ret;
    }

}
