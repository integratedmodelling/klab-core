package org.integratedmodelling.engine.geospace;

import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.engine.geospace.coverage.ICoverage;
import org.integratedmodelling.exceptions.KlabException;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.AttributeDescriptor;

public interface GeotoolsVectorCoverage extends ICoverage {

	AttributeDescriptor getAttributeDescriptor(String valueId) throws KlabException;
	
	FeatureIterator<SimpleFeature> getFeatureIterator(ReferencedEnvelope envelope, String... attributes)
	            throws KlabException;

}
