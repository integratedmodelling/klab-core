package org.integratedmodelling.engine.geospace.testing;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IValueResolver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.base.HashableObject;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.runtime.AbstractStateContextualizer;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.extents.SpaceExtent;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * A datasource for raster spaces that produces a fake terrain.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(
        id = "gis.terrain",
        args = {
                "? max-altitude",
                Prototype.INT,
                "? detail",
                Prototype.INT,
                "? roughness",
                Prototype.FLOAT
        },
        returnTypes = { NS.DATASOURCE })
public class TerrainDatasource extends HashableObject implements IDataSource, IExpression {

    int       detailLevel = 5;
    double    roughness   = 0.37;
    int       maxAltitude = 3000;
    IMetadata metadata    = new Metadata();

    public TerrainDatasource() {
    }

    public TerrainDatasource(int maxAltitude, int detailLevel, double roughness) {
        this.detailLevel = detailLevel;
        this.roughness = roughness;
        this.maxAltitude = maxAltitude;
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws KlabException {

        int detailLevel = 5;
        int maxAltitude = 3000;
        double roughness = 0.37;

        if (parameters.containsKey("max-altitude")) {
            maxAltitude = ((Number) parameters.get("max-altitude")).intValue();
        }
        if (parameters.containsKey("detail")) {
            detailLevel = ((Number) parameters.get("detail")).intValue();
        }
        if (parameters.containsKey("roughness")) {
            roughness = ((Number) parameters.get("roughness")).doubleValue();
        }
        
        return new TerrainDatasource(maxAltitude, detailLevel, roughness);
    }

    @Override
    public IScale getCoverage() {
        return new Scale(SpaceExtent.WORLD());
    }

    @Override
    public boolean isAvailable() {
        return true;
    }

    @Override
    public IStateContextualizer getContextualizer(IScale context, IObserver observer, IMonitor monitor)
            throws KlabException {

        ISpatialExtent space = context.getSpace();
        if (space == null || space.getGrid() == null) {
            throw new KlabValidationException("the terrain datasource can only be used in spatial grid contexts");
        }
        return new TerrainActuator(space, monitor);
    }

    class TerrainActuator extends AbstractStateContextualizer implements IValueResolver {

        Terrain        terrain;
        ISpatialExtent space;

        public TerrainActuator(ISpatialExtent scale, IMonitor monitor) {
            super(monitor);
            this.space = scale;
            this.terrain = new Terrain(detailLevel, roughness);
        }

        
        @Override
        public String getLabel() {
            return "fractal terrain generator";
        }
        
        public double processState(int stateIndex, ITransition transition) {

            int[] xy = space.getGrid().getXYOffsets(stateIndex);
            double altitude = terrain
                    .getAltitude((double) xy[0] / (double) space.getGrid().getXCells(), (double) xy[1]
                            / (double) space.getGrid().getYCells());

            return altitude * maxAltitude;
        }

        @Override
        public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {

            if (parameters.containsKey("max-altitude")) {
                maxAltitude = ((Number) parameters.get("max-altitude")).intValue();
            }
            if (parameters.containsKey("detail")) {
                detailLevel = ((Number) parameters.get("detail")).intValue();
            }
            if (parameters.containsKey("roughness")) {
                roughness = ((Number) parameters.get("roughness")).doubleValue();
            }
        }

        @Override
        public Map<String, Object> initialize(int index, Map<String, Object> inputs) throws KlabException {
            Map<String, Object> ret = new HashMap<>();
            ret.put(getStateName(), processState(index, ITransition.INITIALIZATION));
            return ret;
        }

        @Override
        public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
                throws KlabException {
            Map<String, Object> ret = new HashMap<>();
            ret.put(getStateName(), processState(index, transition));
            return ret;
        }

        @Override
        public boolean isProbabilistic() {
            return false;
        }

    }

}
