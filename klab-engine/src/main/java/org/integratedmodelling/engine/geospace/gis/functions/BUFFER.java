/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.engine.geospace.gis.functions;
//
//import java.util.Map;
//
//import org.integratedmodelling.api.knowledge.IConcept;
//import org.integratedmodelling.api.knowledge.IExpression;
//import org.integratedmodelling.api.project.IProject;
//import org.integratedmodelling.api.services.annotations.Prototype;
//import org.integratedmodelling.common.vocabulary.NS;
//import org.integratedmodelling.engine.geospace.gis.operators.RBufferAccessor;
//import org.integratedmodelling.exceptions.ThinklabException;
//
///**
// * Raster buffering. TODO should automatically switch to vector buffering if
// * the context is vector.
// * 
// * @author Ferd
// *
// */
//@Prototype(
//        id = "gis.buffer",
//        args = { "distance", Prototype.FLOAT, "? use-data-as-distance", Prototype.BOOLEAN },
//        returnTypes = { NS.GLOBAL_STATE_CONTEXTUALIZER })
//public class BUFFER implements IExpression {
//
//    IProject project;
//
//    @Override
//    public Object eval(Map<String, Object> parameters, IConcept... context) throws ThinklabException {
//
//        int method = RBufferAccessor.FIXED_DISTANCE;
//
//        double distance = 10;
//        if (parameters.containsKey("distance")) {
//            distance = Double.parseDouble(parameters.get("distance").toString());
//        }
//
//        if (parameters.containsKey("use-data-as-distance")) {
//            if (parameters.get("use-data-as-distance") instanceof Boolean
//                    && (Boolean) parameters.get("use-data-as-distance")) {
//                method = RBufferAccessor.USE_CELL_VALUE_AS_BUFFER_DISTANCE;
//            }
//        }
//
//        return new RBufferAccessor(distance, method);
//    }
//
//    @Override
//    public void setProjectContext(IProject project) {
//        this.project = project;
//    }
//
// }
