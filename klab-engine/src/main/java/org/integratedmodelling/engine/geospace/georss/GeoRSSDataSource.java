/*******************************************************************************
 *  Copyright (C) 2014, 2015:
 *  
 *    - Ioannis N. Athanasiadis <ioannis@athanasiadis.info>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *    You should have received a copy of the Affero General Public License
 *    along with this program; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *    The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.georss;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.kim.KIMFunctionCall;
import org.integratedmodelling.engine.geospace.datasources.VectorCoverageDataSource;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabUnsupportedOperationException;

/**
 * 
 * @author Ioannis N. Athanasiadis <ioannis@athanasiadis.info>
 * @since 2014
 */
public class GeoRSSDataSource extends VectorCoverageDataSource {

    URL             service;
    private boolean initialized  = false;
    private String  node         = "";
    private String  defaultvalue = "";
    private String  extract      = "";
    private String  limit        = "";

    class GeoRSSFunctionCall extends KIMFunctionCall implements IExpression {

        public GeoRSSFunctionCall(String id, Map<String, Object> parameters, INamespace namespace) {
            super(id, parameters, namespace);
        }

        @Override
        public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
                throws KlabException {

            String url = parameters.containsKey("url") ? parameters.get("url").toString() : "";

            if (!url.equalsIgnoreCase(url.toString()))
                throw new KlabException("Something is wrong with GeoRSSFunctionCall.eval() method");

            String node = parameters.containsKey("attr") ? parameters.get("attr").toString() : "";
            String defaultvalue = parameters.containsKey("default-value")
                    ? parameters.get("default-value").toString() : "";
            String extract = parameters.containsKey("extract") ? parameters.get("extract").toString() : "";
            String limit = parameters.containsKey("limit") ? parameters.get("limit").toString() : "";

            return new GeoRSSDataSource(service.toString(), node, defaultvalue, extract, limit);
        }
    }

    public GeoRSSDataSource(String url, String node) throws KlabUnsupportedOperationException {
        try {
            service = new URL(url);
        } catch (MalformedURLException e) {
            throw new KlabUnsupportedOperationException("Cannot read an RSS file from" + url);
        }
        try {
            service.openConnection().connect();
        } catch (IOException e) {
            throw new KlabUnsupportedOperationException("Cannot read an RSS file from" + url);
        }
    }

    public GeoRSSDataSource(String url, String node, String defaultvalue, String extract, String limit)
            throws KlabUnsupportedOperationException {
        this(url, node);
        if (node == null || defaultvalue == null || extract == null || limit == null)
            throw new KlabUnsupportedOperationException("GeoRSS parameters cannot be null" + url);
        this.node = node;
        this.defaultvalue = defaultvalue;
        this.extract = extract;
        this.limit = limit;
    }

    // This must ensure that coverage contains a valid vector coverage.

    @Override
    protected void initialize() throws KlabException {
        if (!initialized) {
            this.coverage = new GeoRSSCoverage(service);
            initialized = true;
        }
    }

    // @Override
    // public List<IReifiableObject> getObjects(IScale scale) {
    //
    // IExtent space = scale.getSpace();
    // if (!(space instanceof SpaceExtent)) {
    // throw new ThinklabRuntimeException(
    // "cannot extract objects from a vector coverage in a non-spatial context");
    // }
    //
    // ReferencedEnvelope env = ((SpaceExtent) space).getEnvelope();
    //
    // ArrayList<IReifiableObject> ret = new ArrayList<IReifiableObject>();
    //
    // FeatureIterator<SimpleFeature> fit = null;
    // try {
    // initialize();
    // GeoRSSCoverage gcov = (GeoRSSCoverage) coverage;
    // fit = gcov.getFeatures().features();
    //
    //
    // while (fit.hasNext()) {
    // ret.add(new ShapeObject(fit.next()));
    // }
    //
    // } catch (ThinklabException e) {
    // throw new ThinklabRuntimeException(e);
    // } finally {
    // if (fit != null) {
    // fit.close();
    // }
    // }
    //
    // return ret;
    // }

    @Override
    public String toString() {
        return "GeoRSS [" + service + "]";
    }

    @Override
    public boolean isAvailable() {
        try {
            service.openConnection().connect();
        } catch (IOException e) {
            return false;
        }
        ;
        return true;
    }

    @Override
    public IFunctionCall getDatasourceCallFor(String attribute, INamespace namespace) {
        Map<String, Object> parms = new HashMap<>();
        parms.put("attr", attribute);
        return new GeoRSSFunctionCall("georss", parms, namespace);
    }

}
