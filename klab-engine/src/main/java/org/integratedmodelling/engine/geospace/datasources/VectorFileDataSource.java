/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.datasources;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.kim.KIMFunctionCall;
import org.integratedmodelling.engine.geospace.coverage.CoverageFactory;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabUnsupportedOperationException;

public class VectorFileDataSource extends VectorCoverageDataSource {

    boolean initialized = false;
    File    _file;
    String  _filter     = null;
    String  _attribute  = null;
    String  _tableUrl   = null;

    class VectorFunctionCall extends KIMFunctionCall implements IExpression {

        public VectorFunctionCall(String id, Map<String, Object> parameters, INamespace namespace) {
            super(id, parameters, namespace);
        }

        @Override
        public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
                throws KlabException {
            return new VectorFileDataSource(_id, _file.toString(), _filter, parameters.get("attr")
                    .toString(), _tableUrl, parameters.containsKey("simplify-shapes")
                            && parameters.get("simplify-shapes").toString().equals("true"));
        }
    }

    public VectorFileDataSource(String id, String file, String filter, String attr, String tableUrl,
            boolean simplifyShapes)
            throws KlabUnsupportedOperationException {

        _id = id;
        _filter = filter;
        _attribute = attr;
        _tableUrl = tableUrl;
        this.simplifyShapes = simplifyShapes;

        File f = new File(file);
        if (f.exists()) {
            _file = f;
        }

        if (!_file.exists()) {
            throw new KlabUnsupportedOperationException("file " + file
                    + " cannot be read or does not have a supported raster format");
        }
    }

    @Override
    protected void initialize() throws KlabException {

        if (!initialized) {
            this.coverage = CoverageFactory.readVector(_file, _id, _attribute, _filter);
            initialized = true;
        }
    }

    @Override
    public IFunctionCall getDatasourceCallFor(String attribute, INamespace namespace) {
        Map<String, Object> parms = new HashMap<>();
        parms.put("attr", attribute);
        return new VectorFunctionCall("wfs", parms, namespace);
    }

    @Override
    public boolean isAvailable() {
        return _file.exists() && _file.canRead();
    }
}
