/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.datasources;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMFunctionCall;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.engine.geospace.coverage.CoverageFactory;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;

public class WFSCoverageDataSource extends VectorCoverageDataSource {

    class WFSFunctionCall extends KIMFunctionCall implements IExpression {

        public WFSFunctionCall(String id, Map<String, Object> parameters, INamespace namespace) {
            super(id, parameters, namespace);
        }

        @Override
        public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
                throws KlabException {
            return new WFSCoverageDataSource(server, _id, parameters
                    .get("attr").toString(), filter, null, null, invertCoordinates, parameters
                            .containsKey("simplify-shapes")
                            && parameters.get("simplify-shapes").toString().equals("true"));
        }
    }

    /**
     * WFS service URL
     */
    String     server;

    /**
     * attribute containing the data we want. If no attribute, it's 0/1 for
     * presence/absence.
     */
    String     attr;

    /**
     * CQL expression to filter features if requested
     */
    String     filter;

    Properties properties  = new Properties();
    boolean    initialized = false;
    String     authentication;

    /**
     * 
     * @param service the WFS service URL. Cannot be null.
     * @param id the coverage ID.
     * @param attribute
     * @param filter
     * @param valueType
     * @param valueDefault
     */
    public WFSCoverageDataSource(String service, String id, String attribute, String filter,
            String valueType, String valueDefault, Boolean invertCoordinates, boolean simplifyShapes) {

        this.server = service;
        this._id = id;
        this.attr = attribute;
        this.filter = filter;
        this.invertCoordinates = invertCoordinates;
        this.simplifyShapes = simplifyShapes;
    }

    private boolean checkURN() {
        if (this.server == null) {
            this.authentication = ResourceFactory.getUrnAuthorization(_id);
            if (this.authentication != null) {
                this.server = ResourceFactory.getNodeURLForUrn(_id)
                        + API.GET_RESOURCE.replace("{service}", "wfs").replace("{urn}", _id)
                        + ".rewrite";
            } else {
                return false;
            }
        }

        return true;
    }

    @Override
    public void initialize() throws KlabException {

        if (!initialized) {

            checkURN();

            initialized = true;
            URL url;
            try {
                url = new URL(server);
            } catch (MalformedURLException e) {
                throw new KlabIOException(e);
            }

            try {
                this.coverage = CoverageFactory.readVector(url, _id, attr, filter, authentication);
                // if (invertCoordinates == null) {
                // /*
                // * default for WGS84 is to invert; asking for explicit inversion means
                // * don't invert.
                // */
                // invertCoordinates = this.coverage.getCoordinateReferenceSystem()
                // .equals(Geospace.get().getDefaultCRS());
                // }
            } catch (KlabException e) {
                if (_errors == 0) {
                    IMonitor monitor = _monitor;
                    if (monitor == null) {
                        monitor = KLAB.ENGINE.getMonitor();
                    }
                    monitor.error("vector data source " + _id + " is inaccessible");
                    _errors++;
                }
                throw e;
            }
            initialized = true;
        }
    }

    @Override
    public String toString() {
        return "wfs [" + _id + "]";
    }

    @Override
    public IFunctionCall getDatasourceCallFor(String attribute, INamespace namespace) {
        Map<String, Object> parms = new HashMap<>();
        parms.put("attr", attribute);
        return new WFSFunctionCall("wfs", parms, namespace);
    }

    @Override
    public boolean isAvailable() {
        // TODO Auto-generated method stub
        return true;
    }

}
