/*******************************************************************************
 * Copyright (C) 2007, 2014:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.coverage.vector;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.wfs.WFSDataStoreFactory;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.common.vocabulary.KlabUrn;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;

public class WFSCoverage extends AbstractVectorCoverage {

    public static final int TIMEOUT     = 100000;
    public static final int BUFFER_SIZE = 512;

    DataStore store = null;

    public WFSCoverage(URL service, String id, String valueField, String filter, String authentication)
            throws KlabException {
        super(service, id, valueField, filter, authentication);
    }

    @Override
    protected DataStore getDataStore() throws KlabException {

        if (store == null) {

            Integer wfsTimeout = TIMEOUT;
            Integer wfsBufsize = BUFFER_SIZE;

            // if we don't do this, it will take the first layer in WFS
            coverageId = layerName;
            Map<Object, Object> connectionParameters = new HashMap<Object, Object>();

            /*
             * If we're using a URN: set USERNAME and (bogus) PASSWORD and see which
             * headers get sent to the proxy. If any, we can intercept using the service
             * handler before processing in the GET controller. It should get an
             * Authorization: header with "Basic " + encoded USERNAME:PASSWORD
             * (RFC2045-MIME variant of Base64).
             */
            if (authentication != null) {

                /*
                 * For docs: The GS instance must be configured to return its own proxy
                 * address using http://<knode URL>/get/wfs/<knode
                 * name>:system:geoserver:<access.key> GS access key should be obtained
                 * when the GS is configured. This will allow internal WFS to obtain its
                 * configuration without breaking security.
                 */
                connectionParameters.put(WFSDataStoreFactory.USERNAME.key, authentication);
                connectionParameters.put(WFSDataStoreFactory.PASSWORD.key, "holacabronazo");
                KlabUrn urn = new KlabUrn(layerName);
                
                /*
                 * AARGH just tell me that this won't change again.
                 */
                String namespace = (urn.getOriginator() + "." + urn.getNamespace()).replace(".", "-");
                String layerId = (urn.getNodeName() +  "." + urn.getOriginator() + "." + urn.getNamespace() + "." + urn.getResourceId()).replace(".", "_");
                coverageId = namespace + ":" + layerId;

            }

            connectionParameters.put(WFSDataStoreFactory.URL.key, sourceUrl
                    + "?REQUEST=getCapabilities&VERSION=1.1.0");
            connectionParameters.put(WFSDataStoreFactory.TIMEOUT.key, wfsTimeout);
            connectionParameters.put(WFSDataStoreFactory.BUFFER_SIZE.key, wfsBufsize);
            // Use GET (hopefully) so we don't have to go crazy when proxying.
            connectionParameters.put(WFSDataStoreFactory.PROTOCOL.key, Boolean.FALSE);

            if (!NetUtilities.urlResponds(this.sourceUrl)) {
                throw new KlabIOException("connection to WFS host failed for layer " + this.coverageId);
            }

            try {
                store = DataStoreFinder.getDataStore(connectionParameters);
            } catch (IOException e) {
                throw new KlabIOException(e);
            }
        }

        return store;
    }

    @Override
    protected void setDataStore(DataStore store) {
        this.store = store;
    }

}
