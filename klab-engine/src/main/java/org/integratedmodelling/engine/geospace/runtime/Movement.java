package org.integratedmodelling.engine.geospace.runtime;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.engine.modelling.runtime.Process;

public class Movement extends Process {

    public Movement(IObservableSemantics semantics, IModel model, IDirectObservation subject,
            IMonitor monitor) {
        super(semantics, model, subject, monitor);
    }

}
