/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.coverage;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import org.geotools.coverage.GridSampleDimension;
import org.geotools.coverage.grid.GridCoverage2D;
import org.geotools.gce.arcgrid.ArcGridReader;
import org.geotools.gce.geotiff.GeoTiffReader;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.geospace.coverage.raster.RasterCoverage;
import org.integratedmodelling.engine.geospace.coverage.vector.VectorCoverage;
import org.integratedmodelling.engine.geospace.coverage.vector.WFSCoverage;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabValidationException;

import jline.internal.Nullable;

/**
 * Use this to create instances of ICoverage from scratch or from raster or vector files
 * and services.
 * 
 * @author Ferd
 *
 */
public class CoverageFactory {

    final static String[] supportedRasterExtensions = { "tif", "tiff" };

    final static String[] supportedVectorExtensions = { "shp" };

    public static final String CRS_PROPERTY              = "crs";
    public static final String FIELD_NAMES_PROPERTY      = "field.names";
    public static final String PROTOTYPE_PROPERTY_PREFIX = "field.prototype";
    public static final String GEOMETRY_TYPE_PROPERTY    = "field.names";

    /**
     * Read the source and set properties, but do not render any image or waste any more
     * memory than necessary at this stage. Load the data using loadImage, possibly after
     * setting different crop, projection and no-data values.
     * 
     * @param url
     * @param properties
     * @throws KlabException
     */
    public synchronized static ICoverage readRaster(String url, Properties properties)
            throws KlabException {

        GridCoverage2D coverage = null;

        if (url.endsWith(".tif") || url.endsWith(".tiff")) {

            try {

                GeoTiffReader reader = new GeoTiffReader(url, Geospace.get().getGeotoolsHints());

                coverage = reader.read(null);

            } catch (Exception e) {
                throw new KlabValidationException(e);
            }

        } else if (url.toString().endsWith(".adf")) {

            try {

                ArcGridReader reader = new ArcGridReader(url, null);
                coverage = reader.read(null);

            } catch (Exception e) {
                throw new KlabValidationException(e);
            }

        }

        if (coverage == null) {
            throw new KlabIOException("read error loading coverage from " + url);
        }

        /*
         * TODO enable handling of multi-band coverages WITHIN RasterCoverage. Could be
         * used for distributions, too.
         */
        GridSampleDimension[] sdims = coverage.getSampleDimensions();
        return new RasterCoverage(url.toString(), coverage, sdims[0], sdims.length == 1);
    }

    public static boolean supportsFormat(String ext) {

        return Arrays.binarySearch(supportedRasterExtensions, ext) >= 0
                || Arrays.binarySearch(supportedVectorExtensions, ext) >= 0;
    }

    public static ICoverage makeCoverage(Grid ext, Map<Collection<Integer>, Double> data)
            throws KlabException {

        double[] dataset = new double[ext.getYCells() * ext.getXCells()];

        for (Collection<Integer> o : data.keySet()) {

            Iterator<Integer> it = o.iterator();

            int y = it.next();
            int x = it.next();
            double d = data.get(o);

            dataset[(y * ext.getXCells()) + x] = d;
        }

        RasterCoverage ret = new RasterCoverage("", ext, dataset);

        return ret;
    }

    /**
     * Read a WFS unless the URL is a file
     * 
     * @param url
     * @param id
     * @param attr
     * @param filter
     * @return coverage
     * @throws KlabException
     */
    public static ICoverage readVector(URL url, String id, String attr, String filter, /*@Nullable*/ String authentication)
            throws KlabException {

        ICoverage ret = null;
        if (url.getProtocol().startsWith("file")) {
            ret = new VectorCoverage(url, id, attr, filter);
        } else {
            ret = new WFSCoverage(url, id, attr, filter, authentication);
        }

        return ret;
    }

    /**
     * Read a vector coverage from a file
     * 
     * @param file
     * @param id
     * @param attr
     * @param filter
     * @return coverage
     * @throws KlabException
     */
    public static VectorCoverage readVector(File file, String id, String attr, String filter)
            throws KlabException {
        try {
            return new VectorCoverage(file.toURI().toURL(), id, attr, filter);
        } catch (MalformedURLException e) {
            throw new KlabValidationException(e);
        }
    }

}
