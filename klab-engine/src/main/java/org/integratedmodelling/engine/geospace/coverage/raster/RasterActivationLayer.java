/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.coverage.raster;

import java.util.BitSet;

import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IGridMask;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.engine.geospace.extents.Grid;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

/**
 * A support class that is coupled with a raster layer and tells us whether the
 * cell at x,y belongs to the raster shape. Basically a mask, used by the raster 
 * conceptual model and by the raster path to determine the order of iteration. 
 * 
 * Uses X,Y indexing - not row, column.
 * 
 * @author Ferdinando Villa
 *
 */
public class RasterActivationLayer extends BitSet implements IGridMask {

    private static final long         serialVersionUID = 2831346054544907423L;
//    private int                       active;
//    private boolean                   initialized      = false;

    // nothing for now
    Object                            gaps             = null;
    private CoordinateReferenceSystem crs;
    private IGrid                      grid;

    @Override
    public String toString() {
        return "[raster-activation-layer (" + cardinality() + "/" + (grid.getXCells() * grid.getYCells()) + ")]";
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#intersect(org.integratedmodelling.geospace.coverage.RasterActivationLayer)
     */
    @Override
    public void intersect(IGridMask other) throws KlabValidationException {
        this.and((RasterActivationLayer) other);
//        active = this.cardinality();
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#or(org.integratedmodelling.geospace.coverage.IGridMask)
     */
    @Override
    public void or(IGridMask other) throws KlabValidationException {
        this.or(other);
//        active = this.cardinality();
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#getCell(int)
     */
    @Override
    public Pair<Integer, Integer> getCell(int index) {
        int[] xy = grid.getXYOffsets(index);
        return new Pair<Integer, Integer>(xy[0], xy[1]);
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#isActive(int, int)
     */
    @Override
    public boolean isActive(int x, int y) {
//        if (!initialized) {
//            initialize();
//        }
        return get(grid.getOffset(x, y));
    }
    
    /**
     * If associated grid has a shape, deactivate anything outside of it.
     */
    public void createMask() {

        if (((Grid)grid).getShape() != null) {
            for (int i = 0; i < size(); i++) {
                double[] ll = grid.getCoordinates(i);
                if (!((Grid)grid).getShape().containsCoordinates(ll[0], ll[1])) {
                    set(i, false);
                }
            }
        }
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#activate(int, int)
     */
    @Override
    public void activate(int x, int y) {
//        if (!isActive(x, y)) {
//            active++;
//        }
        set(grid.getOffset(x, y), true);
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#deactivate(int, int)
     */
    @Override
    public void deactivate(int x, int y) {
//        if (isActive(x, y)) {
//            active--;
//        }
        set(grid.getOffset(x, y), false);
    }

    public RasterActivationLayer(int x, int y, IGrid grid) {
        super(x * y);
//        active = grid.getCellCount();
        this.grid = grid;
        // set all bits to true
        and(this);
    }
    
    public RasterActivationLayer(int x, int y, boolean setActive, IGrid grid) {
        super(x * y);
//        active = 0;
        this.grid = grid;

        // set all bits to true
        if (setActive) {
            activate();
        }
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#totalActiveCells()
     */
    @Override
    public int totalActiveCells() {
        return cardinality();
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#nextActiveOffset(int)
     */
    @Override
    public int nextActiveOffset(int fromOffset) {
        return nextSetBit(fromOffset);
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#nextActiveCell(int, int)
     */
    @Override
    public int[] nextActiveCell(int fromX, int fromY) {

        int ofs = nextSetBit(grid.getOffset(fromX, fromY));

        if (ofs == -1)
            return null;

        return grid.getXYOffsets(ofs);
    }

    /* (non-Javadoc)
     * @see org.integratedmodelling.geospace.coverage.IGridMask#nextActiveCell(int)
     */
    @Override
    public Pair<Integer, Integer> nextActiveCell(int fromOffset) {

        int ofs = nextSetBit(fromOffset);

        if (ofs == -1)
            return null;

        int[] xy = grid.getXYOffsets(ofs);
        return new Pair<Integer, Integer>(xy[0], xy[1]);
    }

    public void setCRS(CoordinateReferenceSystem crs) {
        this.crs = crs;
    }

    public CoordinateReferenceSystem getCoordinateReferenceSystem() {
        return this.crs;
    }

    @Override
    public IGrid getGrid() {
        return this.grid;
    }

    @Override
    public boolean isActive(int linearIndex) {
        int[] xy = grid.getXYOffsets(linearIndex);
        return isActive(xy[0], xy[1]);
    }

    @Override
    public void invert() {
        for (int i = 0; i < this.size(); i++) {
            flip(i);
        }
    }

    @Override
    public void deactivate() {
        this.clear();
    }

    @Override
    public void activate() {
        set(0, size());
    }
}
