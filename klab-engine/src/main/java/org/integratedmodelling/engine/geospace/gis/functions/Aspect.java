/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis.functions;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.vocabulary.GeoNS;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.gis.SextanteOperations;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

import es.unex.sextante.core.OutputFactory;
import es.unex.sextante.core.OutputObjectsSet;
import es.unex.sextante.core.ParametersSet;
import es.unex.sextante.dataObjects.IRasterLayer;
import es.unex.sextante.geotools.GTOutputFactory;
import es.unex.sextante.morphometry.aspect.AspectAlgorithm;
import es.unex.sextante.outputs.Output;

@Prototype(
        id = "gis.aspect",
        args = { "# method", "burgess|bauer|tarboton|heerdegen|zevenberger|haralick|maximum-slope" },
        returnTypes = { NS.PROCESS_CONTEXTUALIZER })
public class Aspect implements IProcessContextualizer {

    public final static int METHOD_MAXIMUM_SLOPE = AspectAlgorithm.METHOD_MAXIMUM_SLOPE;
    public final static int METHOD_TARBOTON      = AspectAlgorithm.METHOD_TARBOTON;
    public final static int METHOD_BURGESS       = AspectAlgorithm.METHOD_BURGESS;
    public final static int METHOD_BAUER         = AspectAlgorithm.METHOD_BAUER;
    public final static int METHOD_HEERDEGEN     = AspectAlgorithm.METHOD_HEERDEGEN;
    public final static int METHOD_ZEVENBERGEN   = AspectAlgorithm.METHOD_ZEVENBERGEN;
    public final static int METHOD_HARALICK      = AspectAlgorithm.METHOD_HARALICK;

    public final static int UNITS_RADIANS    = AspectAlgorithm.UNITS_RADIANS;
    public final static int UNITS_DEGREES    = AspectAlgorithm.UNITS_DEGREES;
    public final static int UNITS_PERCENTAGE = AspectAlgorithm.UNITS_PERCENTAGE;

    IProject project;
    int      method     = METHOD_HARALICK;
    boolean  canDispose = false;

    @Override
    public boolean canDispose() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) {

        this.project = project;
        if (parameters.containsKey("method")) {

            String m = parameters.get("method").toString().toLowerCase();
            if (m.equals("burgess")) {
                method = METHOD_BURGESS;
            } else if (m.equals("bauer")) {
                method = METHOD_BAUER;
            } else if (m.equals("tarboton")) {
                method = METHOD_TARBOTON;
            } else if (m.equals("heerdegen")) {
                method = METHOD_HEERDEGEN;
            } else if (m.equals("zevenbergen")) {
                method = METHOD_ZEVENBERGEN;
            } else if (m.equals("haralick")) {
                method = METHOD_HARALICK;
            } else if (m.equals("maximum-slope")) {
                method = METHOD_MAXIMUM_SLOPE;
            }
        }

    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation context, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException {

        canDispose = !context.getScale().isTemporallyDistributed();
        GeoNS.synchronize();

        Map<String, IObservation> ret = new HashMap<>();

        AspectAlgorithm alg = new AspectAlgorithm();
        ParametersSet parms = alg.getParameters();
        IState elevation = null;

        /*
         * TODO we should also check that the model does not expect anything but aspect and the core
         * process.
         */
        String oName = null;
        IObservableSemantics output = null;
        for (String n : expectedOutputs.keySet()) {
            if (expectedOutputs.get(n).is(GeoNS.ASPECT)) {
                oName = n;
                output = expectedOutputs.get(n);
            }
        }

        if (output == null) {
            throw new KlabValidationException("aspect GIS computation: model has no usable concept in outputs");
        }

        for (IState st : context.getStates()) {
            if (st.getObservable().getSemantics().is(GeoNS.ELEVATION)) {
                elevation = st;
            }
        }

        if (elevation == null) {
            throw new KlabValidationException("aspect GIS computation: DEM not found in observed dependencies");
        }

        /*
         * units based on the output observer 
         */
        int unit = AspectAlgorithm.UNITS_DEGREES;

        IRasterLayer dem = SextanteOperations.getInputAsRaster(elevation);

        try {
            parms.getParameter(AspectAlgorithm.UNITS).setParameterValue(new Integer(unit));
            parms.getParameter(AspectAlgorithm.METHOD).setParameterValue(new Integer(method));
            parms.getParameter(AspectAlgorithm.DEM).setParameterValue(dem);

            OutputFactory outputFactory = new GTOutputFactory();
            alg.execute(SextanteOperations.getTaskMonitor(monitor), outputFactory);

            OutputObjectsSet outputs = alg.getOutputObjects();
            Output computed = outputs.getOutput(AspectAlgorithm.ASPECT);
            IState aspect = null;

            /*
             * result is only dynamic if elevation is.
             */
            if (elevation.isTemporallyDistributed()) {
                aspect = SextanteOperations.getStateFromRaster(output, context, (IRasterLayer) computed
                        .getOutputObject());
            } else {
                aspect = SextanteOperations.getStaticStateFromRaster(output, context, (IRasterLayer) computed
                        .getOutputObject());
            }

            ret.put(oName, aspect);

        } catch (Exception e) {
            monitor.error(e);
        }

        return ret;
    }

    @Override
    public Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException {
        canDispose = transition.isLast();
        // TODO replay if the DEM has changed.
        return null;
    }

}
