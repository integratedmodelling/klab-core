/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.functions;

import java.io.File;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.api.space.IAreal;
import org.integratedmodelling.common.kim.expr.CodeExpression;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.geospace.datasources.VectorFileDataSource;
import org.integratedmodelling.exceptions.KlabException;

@Prototype(
        id = "vector",
        args = {
                "file",
                Prototype.TEXT,
                "# id",
                Prototype.TEXT,
                "# filter",
                Prototype.TEXT,
                "# attr",
                Prototype.TEXT,
                "# table",
                Prototype.TEXT,
                "# key",
                Prototype.TEXT,
                "# simplify-shapes",
                Prototype.BOOLEAN},
        returnTypes = { NS.DATASOURCE, NS.OBJECTSOURCE })
public class VECTOR extends CodeExpression implements IExpression, IAreal {

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws KlabException {

        File input = new File(getProject().getLoadPath() + File.separator
                + parameters.get("file").toString());

        if (!input.exists()) {
            input = new File(parameters.get("file").toString());
        }

        if (input.exists()) {

            String id = parameters.containsKey("id") ? parameters.get("id").toString() : MiscUtilities
                    .getFileBaseName(input.toString());

            String attr = parameters.containsKey("attr") ? parameters.get("attr").toString() : null;
            String filter = parameters.containsKey("id") ? parameters.get("id").toString() : null;
            String tableUrl = parameters.containsKey("table") ? parameters.get("table").toString() : null;
            boolean simplify = parameters.containsKey("simplify-shapes")
                    && parameters.get("simplify-shapes").toString().equals("true");
            
            if (attr != null && attr.equals(IModelMetadata.PRESENCE_ATTRIBUTE)) {
                attr = null;
            }

            return new VectorFileDataSource(id, input.toString(), filter, attr, tableUrl, simplify);
        }
        return null;
    }

}
