/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.datasources;

import java.util.Properties;

import org.integratedmodelling.api.network.API;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.engine.geospace.coverage.ICoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.AbstractRasterCoverage;
import org.integratedmodelling.engine.geospace.coverage.raster.WCSCoverage;
import org.integratedmodelling.exceptions.KlabException;

public class WCSGridDataSource extends RegularRasterGridDataSource {

    private Properties properties = new Properties();
    private String     service;
    private String     authentication;

    public WCSGridDataSource(String service, String id, double[] noData, boolean isAvailable, int band)
            throws KlabException {

        this.service = service;
        this.id = id;
        this.isAvailable = isAvailable;
        this.band = band;

        if (this.service != null) {
            properties.put(WCSCoverage.WCS_SERVICE_PROPERTY, this.service);
        }

        for (double d : noData) {
            if (!Double.isNaN(d)) {
                String s = properties.getProperty(AbstractRasterCoverage.NODATA_PROPERTY, "");
                if (s.length() > 0)
                    s += ",";
                s += d;
                properties.put(AbstractRasterCoverage.NODATA_PROPERTY, s);
            }
        }
    }

    @Override
    public String toString() {
        return "wcs [" + this.id + "]";
    }

    @Override
    protected ICoverage readData() throws KlabException {

        isAvailable = checkURN();
        if (isAvailable && this.coverage == null) {
            this.coverage = new WCSCoverage(id, properties, monitor, authentication, band);
        }
        return this.coverage;
    }

    private boolean checkURN() {
        if (this.service == null) {
            this.authentication = ResourceFactory.getUrnAuthorization(id);
            if (this.authentication != null) {
                this.service = ResourceFactory.getNodeURLForUrn(id)
                        + API.GET_RESOURCE.replace("{service}", "wcs").replace("{urn}", id)
                        + ".rewrite";
                properties.put(WCSCoverage.WCS_SERVICE_PROPERTY, this.service);
            } else {
                return false;
            }
        }

        return true;
    }

    @Override
    public boolean isAvailable() {
        // TODO add check of network credentials
        return isAvailable;
    }

}
