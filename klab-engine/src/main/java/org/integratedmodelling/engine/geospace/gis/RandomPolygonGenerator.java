/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.geospace.gis;

import java.util.Random;

import org.geotools.geometry.jts.ReferencedEnvelope;
import org.integratedmodelling.engine.geospace.literals.ShapeValue;

import com.vividsolutions.jts.algorithm.ConvexHull;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;

public class RandomPolygonGenerator {

    private static final double CLOUD_MIN = 150;
    private static final double CLOUD_W   = 200;

    public static ShapeValue randomPolygon(ReferencedEnvelope envelope, int resolutionMeters,
            int maxLinearSize) {

        Random rand = new Random();
        final Coordinate[] cloud = new Coordinate[10];

        /*
         * size and current resolution of box. Ensure that passed parameters are
         * consistent with enclosing box.
         */

        /*
         * determine bounds where to create points based on given resolution and
         * linear size. Use a normal distribution around coords with 1/16 (configure)
         * radius as standard dev.
         */

        /*
         * create random points in box and make polygon shape from their convex hull. Clip to 
         * envelope if required. Check distance and overlap and discard if required. For each
         * accepted polygon compute area and stop when target is covered.
         */
        for (int i = 0; i < cloud.length; i++) {
            cloud[i] = new Coordinate(CLOUD_MIN + CLOUD_W * rand.nextDouble(), CLOUD_MIN + CLOUD_W
                    * rand.nextDouble());
        }

        ConvexHull hull = new ConvexHull(cloud, new GeometryFactory());
        return new ShapeValue((hull.getConvexHull()), envelope.getCoordinateReferenceSystem());
    }

}
