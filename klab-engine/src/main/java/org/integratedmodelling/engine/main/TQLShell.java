/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.main;
//package org.integratedmodelling.thinklab.main;
//
//import java.awt.GraphicsEnvironment;
//import java.io.File;
//import java.util.ArrayList;
//
//import org.integratedmodelling.thinklab.Thinklab;
//import org.integratedmodelling.thinklab.api.runtime.ISession;
//import org.integratedmodelling.thinklab.commandline.GraphicalTQLShell;
//import org.integratedmodelling.thinklab.commandline.Shell;
//import org.integratedmodelling.thinklab.common.utils.MiscUtilities;
//import org.integratedmodelling.thinklab.literals.BooleanValue;
//
///**
// * A simple command-line driven knowledge manager. Just run and type 'help'.
// * @author Ferdinando Villa
// */
//public class TQLShell {
//
//    private static final String GRAPHICAL_SHELL_PROPERTY = "commandline.graphical.shell";
//
//    private ArrayList<String> scriptFiles = new ArrayList<String>();
//
//    public ISession session;
//
//    protected void initApplication(String[] args) {
//
//        //		scriptFiles.addAll(LocalConfiguration.getProfileScripts());
//
//        for (String s : args) {
//
//            // FIXME?
//            // temporary fix - the first arg in cl is the main class name, but
//            // that doesn't apply when running through the JVM API so just 
//            // starting from 1 would require some inelegant CL hacks.
//            if (s.startsWith("org.") && !new File(s).exists())
//                continue;
//
//            scriptFiles.add(s);
//        }
//    }
//
//    public void startApplication() throws Exception {
//
//        boolean isGraphical = BooleanValue.parseBoolean(Thinklab.get().getProperties()
//                .getProperty(GRAPHICAL_SHELL_PROPERTY, "true"));
//
//        if (isGraphical && GraphicsEnvironment.isHeadless())
//            isGraphical = false;
//
//        boolean hasScript = false;
//        ISession session = null;
//        for (String s : scriptFiles) {
//
//            if (!hasScript && MiscUtilities.getFileName(s).startsWith("."))
//                hasScript = true;
//
//            Shell.runScript(s, session);
//        }
//
//        if (hasScript)
//            return;
//
//        if (isGraphical) {
//            GraphicalTQLShell shell = new GraphicalTQLShell();
//            shell.startConsole();
//        } else {
//            Shell shell = new Shell();
//            shell.startConsole();
//        }
//    }
//
//    public static void main(String[] args) throws Exception {
//
//        Thinklab.boot();
//        Runtime.getRuntime().addShutdownHook(new Thread() {
//            @Override
//            public void run() {
//                Thinklab.shutdown();
//            }
//        });
//
//        /*
//         * the shell reads project from $HOME/thinklab/projects by default.
//         */
//        Thinklab.get().registerProjectDirectory(Thinklab.get().getProjectPath());
//
//        TQLShell shell = new TQLShell();
//        shell.initApplication(args);
//        shell.startApplication();
//        Thinklab.shutdown();
//    }
// }
