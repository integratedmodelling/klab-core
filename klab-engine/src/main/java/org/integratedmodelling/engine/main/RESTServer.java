/// *******************************************************************************
// * Copyright (C) 2007, 2015:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.engine.main;
//
// import java.io.File;
// import java.util.ArrayList;
// import java.util.List;
//
// import org.integratedmodelling.api.monitoring.ILegacyNotification;
// import org.integratedmodelling.api.runtime.ILegacySession;
// import org.integratedmodelling.common.configuration.Configuration;
// import org.integratedmodelling.common.configuration.KLAB;
// import org.integratedmodelling.common.utils.ClassUtils;
// import org.integratedmodelling.engine.KLABEngine;
// import org.integratedmodelling.engine.rest.RESTManager;
// import org.integratedmodelling.exceptions.ThinklabException;
// import org.integratedmodelling.exceptions.ThinklabRuntimeException;
//
/// **
// * A simple command-line driven knowledge manager. Just run and type 'help'.
// * @author Ferdinando Villa
// */
// public class RESTServer {
//
// public ILegacySession session;
//
// public void startServer(String context, int port) throws Exception {
//
// RESTManager.get().start(context, port);
//
// if (KLAB.CONFIG.getNotificationLevel() == ILegacyNotification.DEBUG) {
// CommandLineShell shell = new CommandLineShell();
// shell.initApplication(new String[] {});
// shell.startApplication();
// }
//
// while (true) {
// try {
// Thread.sleep(1000);
// } catch (InterruptedException e) {
// break;
// }
// }
// }
//
// public static void main(String[] args) throws Exception {
//
// boolean clear = false;
// String componentPath = null;
//
// KLAB.CONFIG = new Configuration(true);
//
// String ept = System.getenv(KLAB.THINKLAB_REST_PORT_PROPERTY);
// if (ept == null)
// ept = "8182";
//
// String ctx = System.getenv(KLAB.THINKLAB_REST_CONTEXT_PROPERTY);
// if (ctx == null)
// ctx = "rest";
//
// /*
// * TODO process command line options
// */
// for (int i = 0; i < args.length; i++) {
// String arg = args[i];
// if ("--clear".equals(arg)) {
// clear = true;
// } else if ("--componentPath".equals(arg)) {
// componentPath = args[++i];
// } else {
// KLAB.error("command line argument " + arg + " not understood");
// }
// }
//
// /*
// * Just try (and create if not there) the default component path if none was
// * supplied.
// */
// if (componentPath == null) {
// componentPath = KLAB.CONFIG.getDataPath() + File.separator + "components";
// new File(componentPath).mkdirs();
// }
//
// ClassUtils.addJarFilesToThreadClasspath(harvestComponentJars(componentPath));
//
// final int port = Integer.parseInt(ept);
//
// Runtime.getRuntime().addShutdownHook(new Thread() {
// @Override
// public void run() {
// try {
// RESTManager.get().stop(port);
// } catch (ThinklabException e) {
// throw new ThinklabRuntimeException(e);
// }
// KLABEngine.shutdown();
// }
// });
//
// KLABEngine.boot();
//
// if (clear) {
// // Thinklab.logger().info("--clear option found: clearing kbox thinklab");
// // Thinklab.get().dropKbox("thinklab");
// }
//
// RESTServer shell = new RESTServer();
// shell.startServer(ctx, port);
// }
//
// private static List<File> harvestComponentJars(String cpath) {
// ArrayList<File> ret = new ArrayList<>();
// for (String f : cpath.split(";")) {
// File file = new File(f);
// if (!file.exists() || !file.canRead()) {
// KLAB.warn("component path " + file + " does not exist or is unreadable");
// continue;
// }
// addJarsRecursively(file, ret);
// }
// for (File f : ret) {
// KLAB.info("loading component " + f);
// }
// return ret;
// }
//
// private static void addJarsRecursively(File file, ArrayList<File> ret) {
// if (file.isFile() && file.toString().endsWith(".jar")) {
// ret.add(file);
// } else if (file.isDirectory()) {
// for (File f : file.listFiles()) {
// addJarsRecursively(f, ret);
// }
// }
// }
// }
