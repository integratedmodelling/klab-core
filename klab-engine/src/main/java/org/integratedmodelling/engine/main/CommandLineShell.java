/// *******************************************************************************
// * Copyright (C) 2007, 2015:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.engine.main;
//
// import java.awt.GraphicsEnvironment;
// import java.io.File;
// import java.util.ArrayList;
//
// import org.integratedmodelling.api.runtime.ILegacySession;
// import org.integratedmodelling.common.configuration.KLAB;
// import org.integratedmodelling.common.utils.MiscUtilities;
// import org.integratedmodelling.engine.KLABEngine;
// import org.integratedmodelling.engine.commandline.GraphicalShell;
// import org.integratedmodelling.engine.commandline.Shell;
// import org.integratedmodelling.engine.introspection.CallTracer;
// import org.integratedmodelling.engine.introspection.DataRecorder;
// import org.integratedmodelling.engine.runtime.Session;
//
/// **
// * A simple command-line driven knowledge manager. Just run and type 'help'.
// * @author Ferdinando Villa
// */
// public class CommandLineShell {
//
// private static final String GRAPHICAL_SHELL_PROPERTY = "commandline.graphical.shell";
//
// private ArrayList<String> scriptFiles = new ArrayList<String>();
//
// public ILegacySession session;
// GraphicalShell _gshell = null;
//
// protected void initApplication(String[] args) {
//
// // scriptFiles.addAll(LocalConfiguration.getProfileScripts());
//
// for (String s : args) {
//
// // FIXME?
// // temporary fix - the first arg in cl is the main class name, but
// // that doesn't apply when running through the JVM API so just
// // starting from 1 would require some inelegant CL hacks.
// if (s.startsWith("org.") && !new File(s).exists())
// continue;
//
// scriptFiles.add(s);
// }
// }
//
// public void startApplication() throws Exception {
//
// boolean isGraphical = Boolean.parseBoolean(KLAB.CONFIG.getProperties()
// .getProperty(GRAPHICAL_SHELL_PROPERTY, "true"));
//
// if (isGraphical && GraphicsEnvironment.isHeadless())
// isGraphical = false;
//
// boolean hasScript = false;
// ILegacySession session = null;
// for (String s : scriptFiles) {
//
// if (session == null)
// session = new Session();
//
// if (!hasScript && MiscUtilities.getFileName(s).startsWith("."))
// hasScript = true;
//
// Shell.runScript(s, session);
// }
//
// if (hasScript)
// return;
//
// if (isGraphical) {
// GraphicalShell shell = new GraphicalShell();
// DataRecorder.get().setShell(shell);
// CallTracer.enable();
// shell.startConsole();
// } else {
// Shell shell = new Shell();
// shell.startConsole();
// }
// }
//
// public static void main(String[] args) throws Exception {
//
// KLABEngine.boot();
// Runtime.getRuntime().addShutdownHook(new Thread() {
// @Override
// public void run() {
// KLABEngine.shutdown();
// }
// });
//
// CommandLineShell shell = new CommandLineShell();
// shell.initApplication(args);
// shell.startApplication();
// KLABEngine.shutdown();
// }
// }
