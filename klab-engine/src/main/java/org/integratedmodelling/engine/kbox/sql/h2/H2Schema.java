/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.kbox.sql.h2;

import java.sql.Types;

import org.integratedmodelling.engine.kbox.sql.h2.H2Kbox.Schema;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Need:
 *  a literal schema (no fields)
 *  a collection schema (with order option and a schema for the collected)
 *  a map schema
 *  a compound schema (map field->schema)
 *  
 * @author Ferd
 */
@Deprecated
public abstract class H2Schema implements Schema {

    public static final String FIELD_PKEY = "OID";
    public static final String FIELD_FKEY = "FID";

    protected String           fieldName;
    protected int              sqlType;
    protected Class<?>         cls;

    public H2Schema(Class<?> cls) {
        this.cls = cls;
    }

    @Override
    public String getCreateSQL() {
        return null;
    }

    public H2Schema named(String name) {
        this.fieldName = name;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof H2Schema && compareFields((H2Schema) o);
    }

    private boolean compareFields(H2Schema o) {
        // TODO Auto-generated method stub
        return false;
    }

    protected String sanitizeName(String name) {
        // TODO Auto-generated method stub
        return name;
    }

    @Override
    public String getFieldName() {
        return fieldName;
    }

    @Override
    public String getFieldType() {
        switch (sqlType) {
        case Types.INTEGER:
            return "INTEGER";
        case Types.DOUBLE:
            return "DOUBLE";
        case Types.BIGINT:
            return "BIGINT";
        case Types.VARCHAR:
            return "VARCHAR(1024)";
        }
        throw new KlabRuntimeException("can't attribute SQL type for id " + sqlType);
    }
}
