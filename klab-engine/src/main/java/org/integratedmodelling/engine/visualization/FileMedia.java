/// *******************************************************************************
// * Copyright (C) 2007, 2015:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.engine.visualization;
//
// import java.io.File;
//
// import org.integratedmodelling.api.modelling.visualization.ILegend;
// import org.integratedmodelling.api.modelling.visualization.IMedia;
// import org.integratedmodelling.api.modelling.visualization.IViewport;
// import org.springframework.http.MediaType;
//
/// **
// * Simple wrapper for a file. If this is returned by a REST command, the file is automatically
// * dispatched for download in the servlet response.
// *
// * @author Ferd
// *
// */
// public class FileMedia implements IMedia {
//
// public File file;
// public MediaType type;
//
// public FileMedia(File file, MediaType type) {
// this.file = file;
// this.type = type;
// }
//
// // @Override
// // public MimeType getMIMEType() {
// // try {
// // return new MimeType(type.toString());
// // } catch (MimeTypeParseException e) {
// // throw new KlabRuntimeException(e);
// // }
// // }
//
// @Override
// public ILegend getLegend() {
// return null;
// }
//
// @Override
// public IMedia scale(IViewport viewport) {
// return null;
// }
// }
