package org.integratedmodelling.engine.visualization;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Point2D;
import java.util.Collection;

import javax.swing.JFrame;
import javax.swing.JPanel;

import com.vividsolutions.jts.awt.PointTransformation;
import com.vividsolutions.jts.awt.ShapeWriter;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

/**
 * Simple interactive tool to visually check JTS geometries. Use as
 * <pre>
 * GeometryViewer.view(g1, g2, ...);
 * </pre>
 * 
 * NOTE: works only with WGS84 geometries.
 * 
 * @author ferdinando.villa
 *
 */
public class GeometryViewer extends JPanel {

    Geometry[] geometries;
   
    static int xSize = 700;
    static int ySize = 700;
    
    double xMin = Double.NaN, xMax = Double.NaN, yMin = Double.NaN, yMax = Double.NaN;
    
    private static final long serialVersionUID = 4645561761074484585L;

    public GeometryViewer(Geometry[] geometries) {
        this.geometries = geometries;
        
        for (Geometry g : geometries) {
            Geometry env = g.getEnvelope();
            for (Coordinate c : env.getCoordinates()) {
                if (Double.isNaN(xMin) || c.x < xMin) {
                    xMin = c.x;
                }
                if (Double.isNaN(xMax) || c.x > xMax) {
                    xMax = c.x;
                }
                if (Double.isNaN(yMin) || c.y < yMin) {
                    yMin = c.y;
                }
                if (Double.isNaN(yMax) || c.y > yMax) {
                    yMax = c.y;
                }
            }
        }
    
    }

    @Override
    public void paint(Graphics g) {

        ShapeWriter sw = new ShapeWriter(new PointTransformation() {
            @Override
            public void transform(Coordinate src, Point2D dest) {
                int px = (int)(xSize * ((src.x - xMin)/(xMax - xMin)));
                int py = ySize - (int)(ySize * ((src.y - yMin)/(yMax - yMin))) - 1;
                dest.setLocation(px, py);
            }
        });
        for (Geometry geometry : geometries) {
            Shape shape = sw.toShape(geometry);
            ((Graphics2D) g).draw(shape);
        }
    }

    public static void view(String title, Collection<? extends Geometry> geometries) {
        view(title, geometries.toArray(new Geometry[geometries.size()]));
    }
    
    public static void view(String title, Geometry...geometries) {
        JFrame f = new JFrame();
        f.setTitle(title);
        f.getContentPane().add(new GeometryViewer(geometries));
        f.setSize(xSize, ySize);
        f.setVisible(true);
    }
}
