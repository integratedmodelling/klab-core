/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.time.literals;

import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.KlabException;

public class PeriodValueUnboundedEnd extends PeriodValue {

    public PeriodValueUnboundedEnd(long startTime) {
        super(startTime, Long.MAX_VALUE);
    }

    @Override
    public String toString() {
        return "Unbounded time period starting at " + getStart();
    }

    @Override
    public Object clone() {
        return new PeriodValueUnboundedEnd(getValue().getStartMillis());
    }

    @Override
    public boolean contains(ITimeInstant time) {
        return (time.compareTo(getStart()) >= 0);
    }

    @Override
    public boolean contains(long millisInstant) {
        return getStart().getMillis() <= millisInstant;
    }

    @Override
    public boolean endsBefore(ITimeInstant instant) {
        return false;
    }

    @Override
    public boolean endsBefore(ITemporalExtent other) {
        return false;
    }

    @Override
    public boolean overlaps(ITemporalExtent other) {
        if (other instanceof PeriodValueUnboundedEnd) {
            return true; // all unbounded periods eventually overlap prior to infinity
        }
        return contains(other.getEnd());
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws KlabException {
        // TODO Auto-generated method stub
        return super.merge(extent, force);
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws KlabException {
        // TODO Auto-generated method stub
        return super.checkCoverage(obj);
    }

    @Override
    public long getValueCount() {
        // TODO Auto-generated method stub
        return super.getValueCount();
    }

    @Override
    public boolean isTemporallyDistributed() {
        // TODO Auto-generated method stub
        return super.isTemporallyDistributed();
    }

    @Override
    public long getMultiplicity() {
        // TODO Auto-generated method stub
        return super.getMultiplicity();
    }

    @Override
    public ITemporalExtent intersection(IExtent other) throws KlabException {
        // TODO Auto-generated method stub
        return super.intersection(other);
    }

    @Override
    public ITemporalExtent union(IExtent other) throws KlabException {
        ITimeInstant otherStart = ((ITemporalExtent) other).getStart();
        long start = otherStart.compareTo(getStart()) < 0 ? otherStart.getMillis() : getValue()
                .getStartMillis();
        return new PeriodValueUnboundedEnd(start);
    }

    @Override
    public boolean contains(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return super.contains(o);
    }

    @Override
    public boolean overlaps(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return super.overlaps(o);
    }

    @Override
    public boolean intersects(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return super.intersects(o);
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws KlabException {
        // TODO Auto-generated method stub
        return super.union(other);
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws KlabException {
        // TODO Auto-generated method stub
        return super.intersection(other);
    }

}
