/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.time;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * @author Ferd, Luke Will have non-static methods to persist temporal states and the
 *         like.
 */
public class Time {

    private static Time _this = null;

    public static Time get() {

        if (_this == null) {
            _this = new Time();
        }
        return _this;
    }

    /**
     * Called upon to persist a temporally distributed, non-spatial state to a sensible
     * format. Should probably be a CSV file with data in the first column.
     *
     * @param state
     * @param file
     * @throws KlabException
     */
    public void persistState(IState state, File file, Iterable<IScale.Locator> locators)
            throws KlabException {

        if (state.getScale().getTime() == null || state.getScale().getTime()
                .getMultiplicity() != state.getScale().getMultiplicity()) {
            throw new KlabValidationException("cannot export state to CSV: scale distribution is not only temporal");
        }

        try {

            FileWriter writer = new FileWriter(file);

            writer.append("Time," + state.getObservable().getSemantics().getType().getLocalName());
            if (state.getObserver() instanceof IMeasuringObserver) {
                writer.append(" " + ((IMeasuringObserver) state.getObserver()).getUnit());
            } else if (state.getObserver() instanceof IValuingObserver) {
                writer.append(" " + ((IValuingObserver) state.getObserver()).getCurrency());
            }

            for (int n = 0; n < state.getScale().getTime().getMultiplicity() /* FIXME */ - 2; n++) {
                writer.append("\n" + state.getScale().getTime().getExtent(n).getStart() + ","
                        + States.get(state, n));
            }

            writer.flush();
            writer.close();

        } catch (IOException e) {
            throw new KlabIOException(e);
        }
    }
}
