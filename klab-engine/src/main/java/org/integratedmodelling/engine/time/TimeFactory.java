/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.time;

import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.engine.time.extents.RegularTemporalGrid;
import org.integratedmodelling.engine.time.extents.TemporalLocation;
import org.integratedmodelling.engine.time.literals.DurationValue;
import org.integratedmodelling.engine.time.literals.TimeValue;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Build time topologies.
 *
 * @author Ferdinando
 *
 */
public class TimeFactory {

    /**
     * Turn a string into a the list representation of the corresponding
     * time topology. If the string contains a slash, that
     * is assumed to separate extent from resolution and the result will be a
     * regular time grid. Otherwise, the result will be a time record observation.
     *
     * The extent part (possibly the whole string) can be any time value such
     * as a year , a month-year, a day-month-year or a ISO date, or a number with
     * units expressing a duration, such as 1s, 1h, 2year or so. If it's a duration,
     * we assume the extent starts now with the appropriate resolution.
     *
     * The resolution (if any) must be a duration and it should divide the
     * extent evenly and into at least one time "cell". All calculations are
     * done in milliseconds.
     *
     * @param s
     * @return parsed extent
     * @throws KlabValidationException
     */
    public static ITemporalExtent parseTimeTopology(String s) throws KlabException {

        String ext = null;
        String res = null;

        if (s.contains("/")) {
            String[] ss = s.split("/");
            ext = ss[0];
            res = ss[1];
        } else {
            ext = s;
        }

        DurationValue step = null;
        if (res != null) {
            step = new DurationValue(res);
        }

        ITimeInstant start = null;
        ITimeInstant end = null;

        if (Character.isLetter(ext.charAt(ext.length() - 1))) {
            /*
             * extent is a duration, start it now
             */
            DurationValue duration = new DurationValue(ext);
            Pair<ITimeInstant, ITimeInstant> pd = duration.localize();
            start = pd.getFirst();
            end = pd.getSecond();

            if (res == null && duration.getOriginalQuantity() > 1) {
                res = (1 + duration.getOriginalUnit());
                step = new DurationValue(res);
            }

        } else {

            String exd = null;
            if (ext.contains("#")) {
                String[] zo = ext.split("#");
                ext = zo[0];
                exd = zo[1];
            }
            /*
             * extent is a date or a range thereof, extent is one time the implied
             * resolution.
             */
            start = new TimeValue(ext);
            end = exd == null ? ((TimeValue) start).getEndOfImpliedExtent() : new TimeValue(exd);
        }

        ITemporalExtent ret = null;
        if (res == null) {
            // FIXME assuming 1ms duration if it's a location. This will need some
            // consideration and the string should be able to indicate resolution as per
            // previous implementations.
            ret = new TemporalLocation(start, step == null ? 1 : step.getMilliseconds());
        } else {
            ret = new RegularTemporalGrid(start, end, step.getMilliseconds());
        }

        return ret;
    }

}
