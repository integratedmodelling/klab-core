/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.time.extents;

import java.util.Iterator;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeDuration;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.knowledge.Extent;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.time.literals.PeriodValue;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Extent class for a single temporal location.
 * TODO this should be defined from a period as well as a timevalue, because it is exactly
 * what it represents (according to resolution). In the present implementation resolutions
 * are fixed, but I need to make them arbitrary and connect TimeGrid with this using the
 * full extent value.
 *
 * @author Ferdinando Villa
 *
 */
public class TemporalLocation extends Extent implements ITemporalExtent {

    private final ITimeInstant location;
    private final long         impliedDuration;
    private int scaleRank;
    
    @Override
    public int getScaleRank() {
        return scaleRank;
    }

    public TemporalLocation(ITimeInstant location, long impliedDuration) {
        this.location = location;
        this.impliedDuration = impliedDuration;
    }

    @Override
    public Object getValue(int offset) {
        return null;
    }

    @Override
    public int[] getDimensionSizes() {
        return new int[] { (int) getMultiplicity() };
    }
    
    @Override
    public IState as(IObserver observer) {
        return this;
    }


    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        return new int[] { linearOffset };
    }

    @Override
    public long getValueCount() {
        return 1;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return false;
    }

    @Override
    public long getMultiplicity() {
        return getValueCount();
    }

    @Override
    public ITemporalExtent intersection(IExtent other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IExtent union(IExtent other) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean contains(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean overlaps(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean intersects(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITimePeriod collapse() {
        return new PeriodValue(location.getMillis() - impliedDuration, location.getMillis());
    }

    @Override
    public ITemporalExtent getExtent(int stateIndex) {
        // TODO Auto-generated method stub
        return stateIndex == 0 ? this : null;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        // TODO Auto-generated method stub
        return false;
    }

    // TimeValue value;
    //
    // public TemporalLocationExtent(TimeValue value) {
    // this.value = value;
    // }
    //
    // @Override
    // public IValue getFullExtentValue() {
    // return value;
    // }
    //
    // @Override
    // public Object getValue(int granule) {
    // return value;
    // }
    //
    // public int getValueCount() {
    // return 1;
    // }
    //
    // public TimeValue getTimeValue() {
    // return value;
    // }
    //
    // public String toString() {
    // return value.toString();
    // }
    //
    // @Override
    // public IDatasourceTransformation getDatasourceTransformation(
    // IConcept mainObservable, IExtent extent) {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public IExtent getExtent(int granule) {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public IExtent and(IExtent extent) throws ThinklabException {
    //
    // if (! (extent instanceof TemporalLocationExtent)) {
    // throw new
    // ThinklabContextualizationException("temporal extents are incompatible: heterogeneous time models used
    // in observations");
    // }
    //
    // TimeValue v1 = getTimeValue();
    // TimeValue v2 = ((TemporalLocationExtent)extent).getTimeValue();
    // TimeValue max = null;
    //
    // if (v1.isIdentical(v2)) {
    // max = v1;
    // } else if (v1.comparable(v2) || v2.comparable(v1)){
    // max = v1.mostPrecise(v2);
    // }
    //
    // if (max == null)
    // throw new ThinklabContextualizationException("temporal extents " + v1 + " and " + v2 +
    // " are incompatible");
    //
    // return new TemporalLocationExtent(max);
    // }
    //
    // @Override
    // public Polylist conceptualize() throws ThinklabException {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public IExtent constrain(IExtent extent) throws ThinklabException {
    // // we don't have a grain, so this should be OK
    // return and(extent);
    // }
    //
    // @Override
    // public String getSignature() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public boolean contains(ITopologicallyComparable o)
    // throws ThinklabException {
    // // TODO Auto-generated method stub
    // return false;
    // }
    //
    // @Override
    // public boolean intersects(ITopologicallyComparable o)
    // throws ThinklabException {
    // // TODO Auto-generated method stub
    // return false;
    // }
    //
    // @Override
    // public boolean overlaps(ITopologicallyComparable o)
    // throws ThinklabException {
    // // TODO Auto-generated method stub
    // return false;
    // }
    //
    // @Override
    // public IExtent or(IExtent myExtent) {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public IExtent getAggregatedExtent() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public getStateLocators(int index) {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public boolean checkDomainDiscontinuity() throws ThinklabException {
    // // TODO Auto-generated method stub
    // return false;
    // }
    //
    // @Override
    // public IExtent intersection(IExtent extent) throws ThinklabException {
    // // TODO Auto-generated method stub
    // return extent;
    // }
    //
    // @Override
    // public IExtent force(IExtent extent) throws ThinklabException {
    // // TODO Auto-generated method stub
    // return extent;
    // }
    //
    // @Override
    // public IExtent union(IExtent extent) throws ThinklabException {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public IConcept getValueType() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public void setValue(int index, Object o) {
    // // TODO Auto-generated method stub
    //
    // }
    //
    // @Override
    // public Object getRawData() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public double[] getDataAsDoubles() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public double getDoubleValue(int index) {
    // // TODO Auto-generated method stub
    // return 0;
    // }
    //
    // @Override
    // public Metadata getMetadata() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public IConcept getObservableClass() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public IContext getObservationContext() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public Restriction getConstraint(String operator) throws ThinklabException {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public IState aggregate(IConcept concept) throws ThinklabException {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public AggregationParameters getAggregationParameters(IConcept concept,
    // Unit unit) {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public boolean isSpatiallyDistributed() {
    // // TODO Auto-generated method stub
    // return false;
    // }
    //
    // @Override
    // public boolean isTemporallyDistributed() {
    // // TODO Auto-generated method stub
    // return false;
    // }
    //
    // @Override
    // public boolean isCovered(int granule) {
    // // we have no gaps
    // return true;
    // }

    @Override
    public IConcept getDomainConcept() {
        return KLAB.c(NS.TIME_DOMAIN);
    }

    @Override
    public IObserver getObserver() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isTemporal() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isSpatial() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public boolean isConsistent() {
        // TODO Auto-generated method stub
        return false;
    }

//    @Override
//    public IProperty getCoverageProperty() {
//        // TODO Auto-generated method stub
//        return null;
//    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getCoveredExtent() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public ITimeInstant getStart() {
        return location;
    }

    @Override
    public ITimeInstant getEnd() {
        return location;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    // @Override
    // public BitSet getMask() {
    // // TODO Auto-generated method stub
    // return null;
    // }

    @Override
    public ITemporalExtent getExtent() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimeDuration getStep() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Iterator<?> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int locate(Locator locator) {
        if (locator == null)
            return 0;
        // TODO locate transition or time period/instant

        return 0;
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservableSemantics observable, IConcept trait) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public void addChangeListener(ChangeListener listener) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public ITransition getTransition(int i) {
        // TODO Auto-generated method stub
        return null;
    }

}
