/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.time.literals;

import static javax.measure.unit.SI.MILLI;
import static javax.measure.unit.SI.SECOND;

import javax.measure.converter.UnitConverter;
import javax.measure.unit.Unit;

import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.time.ITimeDuration;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.time.TemporalPrecision;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

/**
 * A parsed literal representing a time duration. Linked to the DurationValue OWL class to be used as an
 * extended literal for it. Maximum resolution is milliseconds. Can be initialized
 * by a string expressing number with units; unit must express time and the syntax is the one
 * supported by the implementation of IUnit.
 *
 * @author Ferdinando Villa
 * FIXME make it wrap something real
 */
public class DurationValue implements ITimeDuration, IParseable {

    String literal      = null;
    int    precision    = TemporalPrecision.MILLISECOND;
    int    origQuantity = 1;
    String origUnit     = "";
    long   value;

    public DurationValue() throws KlabException {
        this((Long) null);
    }

    public DurationValue(Long milliseconds) {
        value = milliseconds;
    }

    // public DurationValue(IConcept concept, Long milliseconds) {
    // super(concept, milliseconds);
    // }
    //
    // public DurationValue(IConcept c) throws ThinklabException {
    // super(c, 0l);
    // }

    public DurationValue(String s) throws KlabValidationException {
        parse(s);
    }

    public void parse(String s) {

        /*
         * insert a space to break strings like 1s or 2year
         */
        int brk = -1;
        if (!s.contains(" ")) {
            for (int i = 0; i < s.length(); i++) {
                if (!Character.isDigit(s.charAt(i))) {
                    brk = i;
                    origUnit = s.substring(i);
                    break;
                }
            }
        } else {
            origUnit = s.substring(s.indexOf(" ") + 1);
        }

        if (brk > 0) {
            s = s.substring(0, brk) + " " + s.substring(brk);
        }

        if (Character.isDigit(s.charAt(0))) {
            this.origQuantity = MiscUtilities.readIntegerFromString(s);
        }

        try {
            precision = TemporalPrecision.getPrecisionFromUnit(s);
            literal = s;
            /* oh do I like this */
            UnitConverter duration = Unit.valueOf(origUnit).getConverterTo(MILLI(SECOND));
            value = (long) duration.convert(origQuantity);
        } catch (Exception e) {
            throw new KlabRuntimeException(e);
        }
    }

    public void wrap(Object o) {
        value = ((Number) o).longValue();
    }

    public boolean isNumber() {
        return false;
    }

    public boolean isText() {
        return false;
    }

    public boolean isBoolean() {
        return false;
    }

    public boolean isClass() {
        return false;
    }

    // @Override
    // public boolean isObject() {
    // return false;
    // }
    //
    // @Override
    // public boolean isLiteral() {
    // return true;
    // }

    @Override
    public String toString() {
        return literal == null ? (value + " ms") : literal;
    }

    @Override
    public Object clone() {
        return new DurationValue(value);
    }

    @Override
    public long getMilliseconds() {
        return value;
    }

    @Override
    public Pair<ITimeInstant, ITimeInstant> localize() {

        DateTime date = new DateTime();
        TimeValue start = null, end = null;
        long val = value;

        switch (precision) {

        case TemporalPrecision.MILLISECOND:
            start = new TimeValue(date);
            end = new TimeValue(date.plus(val));
            break;
        case TemporalPrecision.SECOND:
            val = value / DateTimeConstants.MILLIS_PER_SECOND;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), date.getMinuteOfHour(), date.getSecondOfMinute(), 0));
            end = new TimeValue(start.getTimeData().plusSeconds((int) val));
            break;
        case TemporalPrecision.MINUTE:
            val = value / DateTimeConstants.MILLIS_PER_MINUTE;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), date.getMinuteOfHour(), 0, 0));
            end = new TimeValue(start.getTimeData().plusMinutes((int) val));
            break;
        case TemporalPrecision.HOUR:
            val = value / DateTimeConstants.MILLIS_PER_HOUR;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    date.getHourOfDay(), 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusHours((int) val));
            break;
        case TemporalPrecision.DAY:
            val = value / DateTimeConstants.MILLIS_PER_DAY;
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), date.getDayOfMonth(),
                    0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusDays((int) val));
            break;
        case TemporalPrecision.MONTH:
            start = new TimeValue(new DateTime(date.getYear(), date.getMonthOfYear(), 1, 0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusMonths(origQuantity));
            break;
        case TemporalPrecision.YEAR:
            start = new TimeValue(new DateTime(date.getYear(), 1, 1, 0, 0, 0, 0));
            end = new TimeValue(start.getTimeData().plusYears(origQuantity));
            break;
        }

        return new Pair<ITimeInstant, ITimeInstant>(start, end);
    }

    public int getOriginalQuantity() {
        return origQuantity;
    }

    public String getOriginalUnit() {
        return origUnit;
    }

    @Override
    public String asText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int compareTo(ITimeDuration other) {
        long otherValue = other.getMilliseconds();
        if (value == otherValue) {
            return 0;
        }
        if (value < otherValue) {
            return -1;
        }
        return 1;
    }
}
