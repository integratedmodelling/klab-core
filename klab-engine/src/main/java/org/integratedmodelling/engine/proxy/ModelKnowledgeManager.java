/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine.proxy;

import org.integratedmodelling.api.factories.IKnowledgeManager;
import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IKnowledgeIndex;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.ui.IBookmarkManager;
import org.integratedmodelling.common.configuration.KLAB;

/*
 * Just a delegate knowledge manager that uses the singleton for 
 * all operations. This allows us to use dependency injection in external
 * libraries that only depend on the API.
 * 
 * Use KnowledgeManager, not Thinklab, as Thinklab may not be fully 
 * instantiated by the time this is needed.
 * 
 * FIXME check if needed - this is probably completely obsolete, not to mention pointless.
 * 
 * @author Ferd
 *
 */
@SuppressWarnings("javadoc")
public class ModelKnowledgeManager implements IKnowledgeManager {

    @Override
    public IProperty getProperty(String prop) {
        return KLAB.KM.getProperty(prop);
    }

    @Override
    public IConcept getConcept(String prop) {
        return KLAB.KM.getConcept(prop);
    }

    @Override
    public IKnowledge getKnowledge(String k) {
        IKnowledge ret = getConcept(k);
        if (ret == null) {
            ret = getProperty(k);
        }
        return ret;
    }

    @Override
    public IConcept getRootConcept() {
        return KLAB.KM.getRootConcept();
    }

    @Override
    public INamespace getCoreNamespace(String ns) {
        return KLAB.KM.getCoreNamespace(ns);
    }

//    @Override
//    public IAuthority getAuthority(String id) {
//        return KLAB.KM.getAuthority(id);
//    }

    @Override
    public IOntology requireOntology(String id) {
        return KLAB.KM.requireOntology(id);
    }

    @Override
    public IKnowledgeIndex getIndex() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IBookmarkManager getBookmarkManager() {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public IAuthority<?> getAuthorityFor(IConcept concept) {
        return KLAB.KM.getAuthorityFor(concept);
    }



}
