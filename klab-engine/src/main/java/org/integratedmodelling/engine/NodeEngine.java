/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.engine;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.network.NodeNetwork;
import org.integratedmodelling.common.project.Workspace;
import org.integratedmodelling.exceptions.KlabConfigurationException;
import org.integratedmodelling.exceptions.KlabException;

public class NodeEngine extends Engine implements IServer {

	private IMonitor monitor = new NodeMonitor();

	@Override
	public IMonitor getMonitor() {
		return monitor;
	}

	/*
	 * prevent BS warnings from various libs.
	 */
	static {
		System.setProperty("com.sun.media.jai.disableMediaLib", "true");
	}

	public NodeEngine() throws KlabException {

		super();
		this.network = new NodeNetwork();
	}

	// /*
	// * (non-Javadoc)
	// *
	// * @see
	// *
	// org.integratedmodelling.thinklab.plugin.IThinklabPlugin#getClassLoader()
	// */
	// @Override
	// public ClassLoader getClassLoader() {
	// return this.getClass().getClassLoader();
	// }

	@Override
	protected final void startup() throws KlabException {

		super.startup();
		scanPackage("org.integratedmodelling");

		if (!((NodeNetwork) network).initialize()) {
			throw new KlabConfigurationException("node initialization failed: check configuration");
		}

		loadAuthorities();
		
		KLAB.WORKSPACE = new Workspace(KLAB.CONFIG.getDataPath("workspace"), true);
		for (File pdir : KLAB.WORKSPACE.getProjectLocations()) {
			KLAB.PMANAGER.registerProject(pdir);
		}

	}

	/**
	 * Entry point in Thinklab: call boot() before you do anything. Calling more
	 * than once without calling shutdown() has no effect.
	 * 
	 * @throws KlabException
	 */
	public static void boot() throws KlabException {
		if (KLAB.ENGINE == null) {
			KLAB.ENGINE = new NodeEngine();
		}
		((NodeEngine) (KLAB.ENGINE)).startup();
	}

	@Override
	public long getBootTime() {
		return bootTime;
	}

	@Override
	public String getName() {
		return KLAB.NAME;
	}

	@Override
	public String getUrl() {
		return network.getUrl();
	}

	@Override
	public boolean isRunning() {
		return true;
	}

	//
	// @Override
	// public IPrototype getFunctionPrototype(String id) {
	// // TODO Auto-generated method stub
	// return null;
	// }

	@Override
	public String submitObservation(IDirectObserver observer, boolean store) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<IObservationMetadata> queryObservations(String text, boolean localOnly) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<IModelMetadata> queryModels(IObservableSemantics observable, IResolutionScope context) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ITask setupComponent(String componentId) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void removeObservations(Collection<String> observationNames) throws KlabException {
		// TODO Auto-generated method stub

	}

	@Override
	public String getId() {
		return KLAB.NAME;
	}

	@Override
	public boolean isActive() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getKey() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return null;
	}

	public class NodeMonitor extends Monitor {

		@Override
		public void info(Object info, String infoClass) {
			KLAB.info((infoClass == null ? "" : ("{" + infoClass + "} ")) + info);
		}

		@Override
		public void warn(Object o) {
			KLAB.warn(o);
		}

		@Override
		public void error(Object o) {
			KLAB.error(o);
		}

		@Override
		public void debug(Object o) {
			KLAB.debug(o);
		}

		@Override
		public void send(Object o) {
			// TODO Auto-generated method stub

		}

	}

	@Override
	public Authentication getAuthentication() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean provides(Object id) {
		return getResourceConfiguration().getSynchronizedProjectIds().contains(id)
				|| getResourceConfiguration().getComponentIds().contains(id);
	}

}
