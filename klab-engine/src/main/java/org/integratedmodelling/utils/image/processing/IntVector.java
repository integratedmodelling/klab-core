/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.utils.image.processing;

import java.io.Serializable;

/**
 * A fast dynamic array of primitive integers
 *
 * @author <A href=http://www.ctr.columbia.edu/~dzhong>Di Zhong (Columbia University)</a>
 */
public class IntVector extends Object implements Serializable {

    /**
     * The actual integer array where one can get values
     */
    public int[] v = null;

    private int inc;
    private int size;
    private int n;

    /**
     * @param i_inc step size of buffer increasement
     * @param i_size initial buffer size
     */
    public IntVector(int i_inc, int i_size) {
        inc = i_inc;
        size = i_size;
        n = 0;
        v = new int[size];
    }

    public IntVector() {
        size = 80;
        inc = 200;
        n = 0;
        v = new int[size];
    }

    /**
     * Number of integers in the array
     */
    public int number() {
        return n;
    }

    /**
     * Add an integer to the array
     */
    public void add(int value) {

        if (n >= size) {
            int[] tmp = new int[size + inc];
            System.arraycopy(v, 0, tmp, 0, size);
            size += inc;
            v = tmp;
        }
        v[n++] = value;

    }
}
