package org.integratedmodelling.thinklab.toolkits

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.api.modelling.IDirectObservation
import org.integratedmodelling.api.modelling.IState
import org.integratedmodelling.api.monitoring.IMonitor
import org.integratedmodelling.api.monitoring.Messages
import org.integratedmodelling.common.kim.ModelFactory
import org.integratedmodelling.common.vocabulary.GeoNS
import org.integratedmodelling.common.vocabulary.ObservableSemantics
import org.integratedmodelling.common.vocabulary.Observables
import org.integratedmodelling.engine.geospace.gis.SextanteOperations
import org.integratedmodelling.engine.scripting.KimToolkit
import org.integratedmodelling.exceptions.KlabRuntimeException
import org.integratedmodelling.thinklab.actions.DefaultAction
import org.integratedmodelling.thinklab.script.DirectObservation
import org.integratedmodelling.thinklab.script.State

@KimToolkit
class GIS {

    /**
     * Viewshed of passed observation in the context. Observe elevation if 
     * required. Context must exist and be a raster; we can rescale here and
     * keep the viewshed if necessary. Cache it for future reference.
     * 
     * @param point
     * @return
     */
    static def getViewshed(DirectObservation point) {

        if (point.obs.getScale().getSpace() == null) {
            throw new KlabRuntimeException("cannot create a viewshed for an observation that's not spatial", DefaultAction.getArtifact(point.binding));
        }
        IDirectObservation ctx = point.obs.getContextObservation();
        if (ctx == null || ctx.getScale().getSpace() == null) {
            throw new KlabRuntimeException("cannot create a viewshed for an observation that's not in a spatial context", DefaultAction.getArtifact(point.binding));
        }
        IMonitor monitor = point.getMonitor();
        IConcept visregion = Observables.declareObservable(ctx.getObservable().getSemantics().getType(), Collections.singleton(GeoNS.VISIBLE_TRAIT));
        IConcept viewshedObservable = Observables.makePresence(visregion);
        IState ret = ((org.integratedmodelling.engine.modelling.runtime.DirectObservation)ctx).getExistingState(viewshedObservable, point.obs);
        if (ret == null) {
            IState elevation = ((org.integratedmodelling.engine.modelling.runtime.DirectObservation)ctx).getExistingState(GeoNS.ELEVATION);
            ObservableSemantics observable = new ObservableSemantics(ModelFactory
                    .presenceObserver(viewshedObservable), point.obs, "visible-" + ctx.name);
            monitor.info("computing viewshed of " + point.name, Messages.INFOCLASS_MODEL);
            ret = SextanteOperations.getRasterViewshed(elevation, ctx, point.unwrap(), observable, monitor);
        }

        return new State(ret, point.binding);
    }
}
