package org.integratedmodelling.thinklab.toolkits

import org.integratedmodelling.api.metadata.IReport
import org.integratedmodelling.engine.scripting.KimToolkit
import org.integratedmodelling.thinklab.script.Concept
import org.integratedmodelling.thinklab.script.Observation

@KimToolkit
class REPORT {

    IReport report;
    Observation observation;

    static REPORT get(Observation observation) {
        if (observation == null) {
            return new REPORT(observation: observation, report: null);
        }
        return new REPORT(observation: observation, report: observation.obs.getContext().getReport());
    }

    def append(Object[] args) {
        if (report != null) {
            if (args != null) {
                for (Object o : args) {
                    report.write(o.toString());
                }
            }
        }
    }

    def print(String s) {
        append(s);
    }

    def println(String s) {
        append(s + "\n");
    }

    def section(String id) {
        if (report != null) {
            report.setSection(id);
        }
    }

    def separator(Object[] args) {
        if (report != null) {
            report.writeln("\n\n----\n\n");
        }
    }

    def paragraph(Object[] args) {
        if (report != null) {
            if (args != null) {
                for (Object o : args) {
                    report.writeln("\n" + o + "\n\n");
                }
            }
        }
    }

    def cite(String[] args) {
        if (report != null) {
            return report.reference(args);
        }
    }

    def describe(Object[] observations) {
        if (report != null && observations != null) {
            for (Object o : observations) {
                report.describe(getKnowledge(o));
            }
        }
    }
    
    def getKnowledge(Object o) {
        if (o instanceof Observation) {
            return o.obs;
        }
        if (o instanceof Concept) {
            return o.concept;
        }
        return o;
    }
    
}
