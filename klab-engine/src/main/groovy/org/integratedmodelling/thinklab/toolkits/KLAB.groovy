package org.integratedmodelling.thinklab.toolkits

import org.integratedmodelling.api.engine.IModelingEngine
import org.integratedmodelling.engine.scripting.KimToolkit
import org.integratedmodelling.thinklab.script.User

@KimToolkit
class KLAB {

    public User getUser() {
        return new User(user: ((IModelingEngine)org.integratedmodelling.common.configuration.KLAB.ENGINE).getUser());
    }
    
}
