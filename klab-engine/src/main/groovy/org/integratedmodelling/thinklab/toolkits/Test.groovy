package org.integratedmodelling.thinklab.toolkits

import javax.measure.unit.Unit

import org.jscience.physics.amount.Amount

class Test {

    static main(args) {
    
        ExpandoMetaClass.enableGlobally();
        
        Number.metaClass.getProperty = { String symbol ->
            Amount.valueOf(delegate, Unit.valueOf(symbol))
        }
        Amount.metaClass.multiply = { Number factor -> delegate.times(factor) }
        Number.metaClass.multiply = { Amount amount -> amount.times(delegate) }
        Number.metaClass.div = { Amount amount -> amount.inverse().times(delegate) }
        Amount.metaClass.div = { Number factor -> delegate.divide(factor) }
        Amount.metaClass.div = { Amount factor -> delegate.divide(factor) }
        Amount.metaClass.power = { Number factor -> delegate.pow(factor) }
        Amount.metaClass.negative = { -> delegate.opposite() }
    
        // arithmetics: multiply, divide, addition, substraction, power
        println( 18.4.kg * 2 )
        println( 1800000.kg / 3 )
        println( 1.kg * 2 + 3.kg / 4 )
        println( 3.cm + 12.m * 3 - 1.km )
        println( 1.5.h + 33.s - 12.min )
        println( 30.m**2 - 100.ft**2 )
        
        // opposite and comparison
        println( -3.h )
        println( 3.h < 4.h )
//        Amount.metaClass.static.valueOf = { Number number, String unit -> Amount.valueOf(number, Unit.valueOf(unit)) }
//        Amount.metaClass.multiply = { Number factor -> delegate.times(factor) }
//        Number.metaClass.multiply = { Amount amount -> amount.times(delegate) }
//        Amount.metaClass.plus = { Number factor -> delegate.plus(factor) }
////        Number.metaClass.plus = { Amount amount -> amount.plus(delegate) }
////        Amount.metaClass.minus = { Number factor -> delegate.minus(factor) }
////        Number.metaClass.minus = { Amount amount -> amount.minus(delegate) }
//        Number.metaClass.div = { Amount amount -> amount.inverse().times(delegate) }
//        Amount.metaClass.div = { Number factor -> delegate.divide(factor) }
//        Amount.metaClass.div = { Amount factor -> delegate.divide(factor) }
//        Amount.metaClass.power = { Number factor -> delegate.pow(factor) }
//        Amount.metaClass.negative = { -> delegate.opposite() }
//
//        // for unit conversions
//        Amount.metaClass.to = { Amount amount -> delegate.to(amount.unit) }
//        Amount.metaClass.to = { String unit -> delegate.to(Unit.valueOf(unit)) }
        
        
        println((19 + 23).kg)
        
    }

}
