package org.integratedmodelling.thinklab.toolkits

import org.integratedmodelling.engine.scripting.KimToolkit

@KimToolkit
class OSM {

    /**
     * Return a tagProvider that will find an OSM line feature and provide
     * its metadata and extents.
     * 
     * @param args
     * @return
     */
    def line(Object[] args) {
        
    }
    
    def area(Object[] args) {
        
    }
    
    def find(Object[] args) {
        
    }
    
    def point(Object[] args) {
        
    }
}
