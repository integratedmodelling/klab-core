package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.common.vocabulary.NS
import org.integratedmodelling.exceptions.KlabRuntimeException

class ProcessInfo extends ObservationInfo {
    
	ProcessInfo(Concept concept, Object... args) {
        super(concept, args);
        if (!NS.isProcess(concept.concept)) {
			throw new KlabRuntimeException("cannot use a non-process concept to create a process")
		}
	}
}
