package org.integratedmodelling.thinklab.script;

import org.integratedmodelling.api.auth.IUser;

public class User {
    
    IUser user;
    
    def getName() {
        return user.getFirstName() + " " + user.getLastName();
    }
    
    def getEmail() {
        return user.getEmailAddress();
    }
}
