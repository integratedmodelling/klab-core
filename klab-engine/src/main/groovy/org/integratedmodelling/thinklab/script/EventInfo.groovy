package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.common.vocabulary.NS
import org.integratedmodelling.exceptions.KlabRuntimeException

class EventInfo extends ObservationInfo {

   EventInfo(Concept concept, Object... args) {
       super(concept, args);
       if (!NS.isEvent(concept.concept)) {
			throw new KlabRuntimeException("cannot use a non-event concept to create an event")
		}
	}
	
}
