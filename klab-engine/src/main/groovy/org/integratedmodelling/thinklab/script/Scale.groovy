package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.modelling.IExtent
import org.integratedmodelling.api.modelling.IScale
import org.integratedmodelling.api.modelling.scheduling.ITransition
import org.integratedmodelling.api.space.ISpatialExtent
import org.integratedmodelling.api.time.ITemporalExtent

/**
 * Works as a looper for the index over the current transition.
 * Simply do 
 * 
 * <pre>
 * for (n in scale) { 
 *      value = elevation[n\]; 
 * }
 * </pre>
 * or use scale.each { } semantics.
 * 
 * @author Ferd
 *
 */
class Scale {

    IScale scale;
    Binding binding;

    Scale(IScale scale, Binding binding) {
        this.scale = scale;
        this.binding = binding;
    }
    
    Iterator iterator() {
        ITransition t = null;
        if (binding.hasVariable('_transition')) {
            t = (ITransition)binding.getVariable('_transition');
        }
        return this.scale.getIndex(t).iterator();
    }
    
    def getSpace() {
        return scale.getSpace() == null ? null : new Space(scale.getSpace(), binding);
    }

    def getTime() {
        return scale.getTime() == null ? null : new Time(scale.getTime(), binding);
    }

    def getExtent(Concept o) {
        IExtent ext = scale.getExtent(o.concept);
        if (ext == null) {
            return null;
        }
        if (ext instanceof ISpatialExtent) {
            return new Space(ext, binding);
        }
        if (ext instanceof ITemporalExtent) {
            return new Time(ext, binding);
        }
        return new Extent(ext, binding);
    }
    
    def or(Object e) {
        return scale.union(e);
    }
    
    def and(Object e) {
        return scale.intersection(e);
    }
    
    def getMultiplicity() {
        return scale.getMultiplicity();
    }

}
