package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.knowledge.IObservation
import org.integratedmodelling.api.space.ISpatialExtent
import org.integratedmodelling.engine.geospace.extents.SpaceExtent

class Space extends Extent {

	Space(ISpatialExtent space, Binding binding) {
        super(space, binding);
	}
    
    def getGrid() {
        return new Grid(((ISpatialExtent)extent).getGrid(), binding);
    }
	
    def plus(other) {
        return new Space(((SpaceExtent)extent).joinBoundaries(coerceToExtent(other)), binding);
    }
    
    def multiply(other) {
        // TODO mask
        return this;
    }
    
    def distance(Object o) {
        return ((SpaceExtent)extent).distanceTo(coerceToExtent(o));
    }

	def getArea() {
		return ((SpaceExtent)extent).getArea();
	}
	    
    ISpatialExtent coerceToExtent(Object o) {
        if (o instanceof Space) {
            return ((Space)o).extent;
        }
        if (o instanceof Observation) {
            return ((Observation)o).obs.getScale().getSpace();
        }
        if (o instanceof IObservation) {
            return ((IObservation)o).getScale().getSpace();
        }
        if (o instanceof ISpatialExtent) {
            return o;
        }
        return null;
    }

}
