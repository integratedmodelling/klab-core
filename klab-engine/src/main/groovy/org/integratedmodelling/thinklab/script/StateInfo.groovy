package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.api.knowledge.IObservation
import org.integratedmodelling.api.modelling.IObservableSemantics
import org.integratedmodelling.common.vocabulary.ObservableSemantics
import org.integratedmodelling.common.vocabulary.Roles

class StateInfo {

	IObservableSemantics observable;
	Object value;
	String name;
	IConcept dataReductionTrait;
    
	StateInfo(IObservableSemantics observable, Object value) {
		this.observable = observable;
		this.value = value;
        this.name = observable.getFormalName();
	}
    
    Object getValue() {
        return value;
    }
		
	def named(String n) {
		this.name = n;
		this;
	}
    
    def getObservable(IObservation observation) {
        if (observation == null) {
            return observable;
        }
        def ret = observable;
        IConcept withRole = Roles.assignRoles(observable.getType(), observation);
        if (!(withRole.equals(observable.getType()))) {
            ret = new ObservableSemantics((ObservableSemantics)observable);
            ((ObservableSemantics)ret).setType(withRole);
        }
        return ret;
    }
    
    /**
     * Used to specify a data reduction trait when an object is tagged through
     * an existing state.
     * 
     * @param trt
     * @return
     */
    def reduce(Concept trt) {
        this.dataReductionTrait = trt.concept;
    }
	
}
