package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.api.modelling.INumericObserver
import org.integratedmodelling.api.modelling.IPresenceObserver
import org.integratedmodelling.api.modelling.IScale
import org.integratedmodelling.api.modelling.IState
import org.integratedmodelling.api.modelling.scheduling.ITransition
import org.integratedmodelling.common.states.States
import org.integratedmodelling.engine.visualization.VisualizationFactory
import org.integratedmodelling.exceptions.KlabRuntimeException
import org.integratedmodelling.thinklab.actions.DefaultAction

class State extends Observation {

    // only used to tag a state for reduction when transformed
    IConcept dataReduction = null;
    ITransition timePointer = ITransition.INITIALIZATION;

    State(IState obs, Binding binding) {
        super(obs, binding);
    }

    String toString() {
        return obs.toString();
    }

    /**
     * Return the state with the appropriate type, aggregating as necessary. If we
     * have a time pointer, use that.
     * 
     * @param cls
     * @return
     */
    Object asType(Class cls) {

        Number nret = null;

        setTimePointer();

        if (cls == Number) {
            return aggregateNumbers();
        } else if (cls == Double || cls == double) {
            nret = aggregateNumbers();
            return nret ? nret.doubleValue() : (double)0.0;
        } else if (cls == Long || cls == long) {
            nret = aggregateNumbers();
            return nret ? nret.longValue() : (long)0;
        } else if (cls == Integer || cls == int) {
            nret = aggregateNumbers();
            return nret ? nret.intValue() : 0;
        } else if (cls == Float || cls == float) {
            nret = aggregateNumbers();
            return nret ? nret.floatValue() : (float)0.0;
        } else if (cls == Boolean || cls == boolean) {
            Object zio = aggregateMajority(Boolean);
            return (zio instanceof Boolean) ? (Boolean)zio : false;
        } else if (cls == IConcept) {
            Object zio = aggregateMajority(IConcept);
            return (zio instanceof IConcept) ? (IConcept)zio : null;
        } else if (cls == double[]) {
            return getDoubleData();
        } else if (cls == long[]) {
            //			return (long)aggregate();
        } else if (cls == int[]) {
            //			return (int)aggregate();
        } else if (cls == float[]) {
            //			return (float)aggregate();
        } else if (cls == boolean[]) {
            // TODO
        } else if (cls == IConcept[]) {
            // TODO
        }
        return null;
    }

    double[] getDoubleData() {
        VisualizationFactory.get().getStateDataAsNumbers((IState)obs,
                Collections.singleton(timePointer));
    }

    /**
     * Assign to state as a whole using state << value. If we are in a 
     * transition handler, assign to the value AFTER the current transition and
     * set the state's time pointer to the transition just seen.
     * 
     * @param value
     * @return
     */
    def leftShift(Object value) {
        setTimePointer();
        States.set(((IState)obs), value, timePointer);
    }


    def setTimePointer() {
        ITransition tr = getTransition();
        if (tr != null) {
            if (timePointer == null || tr.getTimeIndex() > timePointer.getTimeIndex()) {
//                if (((org.integratedmodelling.common.states.State)obs).isCopyStateAtTransitions()) {
//                    States.copyStateToNextTransition((IState)obs, timePointer);
//                }
                timePointer = tr;
            }
        }
    }

    /**
     * Support for state[n]
     * 
     * @param offset
     * @return
     */
    def getAt(int offset) {
        States.get((IState)obs, offset, getTransition());
    }

    def at(Observation o) {
        return new State(States.getView((IState)obs, o.obs, dataReduction), binding);
    }

    /**
     * Support for state[n] = value
     * 
     * @param offset
     * @param value
     * @return
     */
    def putAt(int offset, Object value) {
        States.set(((IState)obs), value, offset);
    }

    /**
     * Support for (state * observation) -> returning state view
     * 
     * @param o
     * @return
     */
    def multiply(Object o) {
        if (o instanceof Observation) {
            return new State(States.getView(obs, ((Observation)o).obs), binding);
        }
        if (o instanceof Scale) {
            return new State(States.getView(obs, ((Scale)o).scale), binding);
        }
        if (o instanceof IScale) {
            return new State(States.getView(obs, (IScale)o), binding);
        }
    }

    def div(Object o) {
        /*
         * must be a data reduction trait 
         * TODO implement (in aggregation strategy)
         */
        if (!(o instanceof Concept)) {
            throw new KlabRuntimeException("the division operator on a state can only be used with a data reduction trait");
        }
        State ret = new State(obs, binding);
        ret.dataReduction = ((Concept)o).concept;
        return ret;
    }

    /**
     * Iterable offsets over the current transition: foreach (n : state.offsets) {}
     * 
     * @return
     */
    def getOffsets() {
        def locs = new IScale.Locator[1];
        locs[0] = getTransition();
        return obs.getScale().getIndex(locs);
    }

    /**
     * TODO revise; use native States functions.
     * @return
     */

    Number aggregateNumbers() {

        double t = Double.NaN;
        if (((IState)obs).getObserver() instanceof INumericObserver) {
            double[] d = VisualizationFactory.getStateDataAsNumbers((IState)obs, Collections.singleton(timePointer));
            int n = 0;
            for (double v in d) {
                if (!Double.isNaN(v)) {
                    if (Double.isNaN(t)) {
                        t = v;
                    } else {
                        t += v;
                    }
                    n ++
                }
            }

            // FIXME reintegrate
            //            if (n > 0 && !((IState)obs).getObserver().isExtensive()) {
            t = t/(double)n;
            //            }
        }
        return t;
    }

    Object aggregateMajority(Class cls) {
        Object ret = null;
        return ret;
    }

    def getSum() {
        double t = Double.NaN;
        if (((IState)obs).getObserver() instanceof INumericObserver) {
            double[] d = VisualizationFactory.getStateDataAsNumbers((IState)obs,
                    Collections.singleton(timePointer));
            int n = 0;
            for (double v in d) {
                if (!Double.isNaN(v)) {
                    if (Double.isNaN(t)) {
                        t = v;
                    } else {
                        t += v;
                    }
                    n ++
                }
            }
        }
        return t;
    }

    /*
     * TODO smarten up - compute stats once per transition and store them.
     */
    def getAvg() {
        double t = Double.NaN;
        if (((IState)obs).getObserver() instanceof INumericObserver) {
            double[] d = VisualizationFactory.getStateDataAsNumbers((IState)obs,
                    Collections.singleton(timePointer));
            int n = 0;
            for (double v in d) {
                if (!Double.isNaN(v)) {
                    if (Double.isNaN(t)) {
                        t = v;
                    } else {
                        t += v;
                    }
                    n ++
                }
            }

            if (n > 0) {
                t = t/(double)n;
            }
        }
        return t;
    }

    def getMin() {
        double t = Double.NaN;
        if (((IState)obs).getObserver() instanceof INumericObserver) {
            double[] d = VisualizationFactory.getStateDataAsNumbers((IState)obs,
                    Collections.singleton(timePointer));
            int n = 0;
            for (double v in d) {
                if (!Double.isNaN(v)) {
                    if (Double.isNaN(t) || t > v) {
                        t = v;
                    }
                    n ++
                }
            }
        }
        return t;
    }

    def getMax() {
        double t = Double.NaN;
        if (((IState)obs).getObserver() instanceof INumericObserver) {
            double[] d = VisualizationFactory.getStateDataAsNumbers((IState)obs,
                    Collections.singleton(timePointer));
            int n = 0;
            for (double v in d) {
                if (!Double.isNaN(v)) {
                    if (Double.isNaN(t) || t < v) {
                        t = v;
                    }
                    n ++
                }
            }
        }
        return t;
    }

    def covers(Observation observation) {
        return getCoverageOf(observation) > 0;
    }

    def getCoverageOf(Observation observation) {

        // TODO return the proportion of coverage that intersects an ACTIVE state
        // of ours in the current transition and at matching other extents
        if (((IState)obs).getObserver() instanceof IPresenceObserver) {

            IState view = States.getView((IState)obs, observation.obs);
            // TODO this shouldn't be necessary
            boolean inView = (Boolean) view.getValue(0);
            if (!inView) {
                // to fool the optimizer; no worries for the useless code.
                inView = true;
            }
            double percentInView = 1.0;
            if (inView && view.getMetadata().contains(IState.Mediator.SPACE_TOTAL_VALUES)) {
                percentInView = (double) view.getMetadata().getInt(IState.Mediator.SPACE_VALUE_SUM) / (double) view.getMetadata().getInt(IState.Mediator.SPACE_TOTAL_VALUES);
            }
            return percentInView;

        } else {
            // TODO
            throw new KlabRuntimeException("getCoverageOf is implemented only for boolean states at the moment", DefaultAction.getArtifact(binding));
        }

        return 1.0;
    }
}
