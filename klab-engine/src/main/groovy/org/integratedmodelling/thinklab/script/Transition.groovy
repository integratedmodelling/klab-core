package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.modelling.scheduling.ITransition

class Transition {

    ITransition transition;

    Transition(ITransition transition) {
        this.transition = transition;
    }

    def getStart() {
        return new Date(transition.getTime().getStart());
    }

    def getEnd() {
        return new Date(transition.getTime().getEnd());
    }

    Object asType(Class cls) {
        if (cls == Date) {
            def mid = transition.getTime().getStart().getMillis()  + (transition.getTime().getEnd().getMillis() - transition.getTime().getStart().getMillis())/2;
            return new Date(mid as Long);
        }
        return null;
    }

    public String toString() {
        return transition.toString();
    }

    def minus(Object o) {

        if (o instanceof Integer) {
            ITransition t = transition;
            for (i in 0..((Integer)o)) {
                t = t.previous();
            }
            return new Transition(t);
        }
        return null;
    }
    
    def getDays() {
        return ((org.integratedmodelling.common.model.runtime.Transition)transition).getDays();
    }

    def getMinutes() {
        return ((org.integratedmodelling.common.model.runtime.Transition)transition).getMinutes();
    }

    def getSeconds() {
        return ((org.integratedmodelling.common.model.runtime.Transition)transition).getSeconds();
    }

    def getWeeks() {
        return ((org.integratedmodelling.common.model.runtime.Transition)transition).getWeeks();
    }

    def getHours() {
        return ((org.integratedmodelling.common.model.runtime.Transition)transition).getHours();
    }

    def getYears() {
        return ((org.integratedmodelling.common.model.runtime.Transition)transition).getYears();
    }

    
}
