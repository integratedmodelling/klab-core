package org.integratedmodelling.thinklab.script;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IExtent

/**
 * Proxy for an object containing metadata and potentially extents that 
 * can be applied to a direct observation to modify or further specify it.
 * 
 * @author ferdinando.villa
 *
 */
public abstract class TagProvider {
    
    IMetadata metadata = null;
    List<IExtent> extents = new ArrayList<>();

    protected void addExtent(IExtent extent) {
        extents.add(extent);
    }
        
    /**
     * Produce the metadata in the context of the observation and if 
     * 
     * @param target
     * @return
     */
    abstract IMetadata resolve(Observation target);

}
