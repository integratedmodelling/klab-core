package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.time.ITemporalExtent

class Time extends Extent {
	
	Time(ITemporalExtent time, Binding binding) {
    	super(time, binding);
	}

}
