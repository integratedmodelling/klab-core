package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.common.vocabulary.NS
import org.integratedmodelling.exceptions.KlabRuntimeException

class SubjectInfo extends ObservationInfo {

    SubjectInfo(Concept concept, Object... args) {
        super(concept, args);
        if (!NS.isThing(concept.concept)) {
            throw new KlabRuntimeException("cannot use a non-subject concept to create a subject")
        }
    }
}
