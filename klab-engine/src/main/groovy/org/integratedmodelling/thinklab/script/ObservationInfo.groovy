package org.integratedmodelling.thinklab.script

import org.integratedmodelling.api.modelling.IScale
import org.integratedmodelling.common.utils.CamelCase
import org.integratedmodelling.common.utils.NameGenerator
import org.integratedmodelling.thinklab.actions.Utils

class ObservationInfo {

    String name;
    Concept concept;
    Object[] args;

    public ObservationInfo(Concept concept, Object... args) {
        this.concept = concept;
        this.args = args;
    }

    public IScale makeScale() {
        return Utils.extractScale(args);
    }

    def named(String n) {
        this.name = n;
        return this;
    }

    def getName() {
        if (name == null) {
            name = CamelCase.toLowerCamelCase(concept.concept.getLocalName(), '_' as char) + "_" + NameGenerator.shortUUID();
        }
        return name;
    }
    
}
