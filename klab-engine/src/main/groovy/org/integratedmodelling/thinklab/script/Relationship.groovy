package org.integratedmodelling.thinklab.script;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IRelationship;

import groovy.lang.Binding;

public class Relationship extends DirectObservation {

    public Relationship(IDirectObservation obs, Binding binding) {
        super(obs, binding);
    }

    public DirectObservation getSource() {
        return new DirectObservation(((IRelationship)obs).getSource(), binding);
    }
    
    public DirectObservation getTarget() {
        return new DirectObservation(((IRelationship)obs).getSource(), binding);
    }
}
