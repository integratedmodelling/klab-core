package org.integratedmodelling.thinklab.actions.filter;

public enum SpaceRel {
    NEAR,
    FAR,
    // distance
    WITHIN,
    BEYOND,
    INSIDE,
    OUTSIDE,
    // this means: outside of the convex hull between ourselves and the set of named features
    ACROSS
}
