package org.integratedmodelling.thinklab.actions.filter

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.api.knowledge.IObservation
import org.integratedmodelling.api.modelling.IDirectObservation
import org.integratedmodelling.common.vocabulary.NS
import org.integratedmodelling.common.vocabulary.Roles

/**
 * Selector is at the root of a filter chain, return by select() and family on 
 * a direct observation. Pick one or more observations or use the wrapped collection.
 * 
 * @author Ferd
 *
 */
class RoledObservationSet extends ObservationSet {

    IConcept role;
    @Deprecated
    IObservation roleContext;

    @Override
    protected Object process(Object result) {

        List<Object> ret = new ArrayList<>();
        if (role) {
            for (IObservation obs : ((org.integratedmodelling.engine.modelling.runtime.DirectObservation)(context.unwrap())).retrieveAll(recursive)) {
                if (roleContext == null) {
                    if (Roles.getRoles(obs.getObservable().getSemantics().getType()).contains(role)) {
                        ret.add(wrapIfNecessary(obs));
                    }
                } else {
                    if (obs instanceof IDirectObservation && NS.contains(role, ((IDirectObservation)roleContext).getRolesFor(obs))) {
                        ret.add(wrapIfNecessary(obs));
                    }
                }
            }
        }
        return ret;
    }
}
