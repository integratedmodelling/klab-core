package org.integratedmodelling.thinklab.actions.interfaces;

import java.util.Collection;

import org.integratedmodelling.api.modelling.IExtent;

/**
 * Anything extentual can produce one or more extents. Used to milk
 * scale from objects passed when creating observations.
 * 
 * @author Ferd
 *
 */
public interface Extentual {

    Collection<IExtent> getExtents();
}
