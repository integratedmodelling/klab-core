package org.integratedmodelling.thinklab.actions.filter

import org.integratedmodelling.api.knowledge.IConcept
import org.integratedmodelling.api.knowledge.IObservation
import org.integratedmodelling.api.modelling.IDirectObservation
import org.integratedmodelling.common.utils.StringUtils
import org.integratedmodelling.exceptions.KlabRuntimeException
import org.integratedmodelling.thinklab.script.Concept
import org.integratedmodelling.thinklab.script.Observation
import org.integratedmodelling.thinklab.script.State

/**
 * Selector is at the root of a filter chain, return by select() and family on 
 * a direct observation. Pick one or more observations or use the wrapped collection.
 * 
 * @author Ferd
 *
 */
class ObservationSet extends AbstractObservationSet {

    int nItems = 1;
    Object[] args;
    String[] match;
    boolean recursive;
    boolean children;

    @Override
    protected Object process(Object result) {

        List<Object> ret = new ArrayList<>();

        for (Object obs : ((org.integratedmodelling.engine.modelling.runtime.DirectObservation)(context.unwrap())).retrieve(new ArrayList<IObservation>(), recursive, children, processArgs())) {
            if (match != null && obs instanceof IDirectObservation) {
                boolean go = false;
                for (String m : match) {
                      if (StringUtils.matchWildcards(((IDirectObservation)obs).getName(), m)) {
                          go = true;
                          break;
                      }
                }
                if (!go) { 
                    continue;
                }    
            }
            ret.add(wrapIfNecessary(obs));
        }
        return ret;
    }
    
    def asBoolean() {
        resolve();
        if (adaptee == null || (adaptee instanceof Collection && ((Collection)adaptee).size() == 0)) {
            return false;
        }
        return true;
    }
    
    def resolveToObservationCollection() {
        resolve();
        def ret = new ArrayList<IObservation>();
        if (adaptee != null) {
            if (adaptee instanceof Observation) {
                ret.add(((Observation)adaptee).obs);
            } else if (adaptee instanceof Collection) {
                for (object in adaptee) {
                    if (object instanceof Observation) {
                        ret.add(((Observation)object).obs);
                    } else if (object instanceof IObservation) {
                        ret.add(object);
                    }
                }
            }
        }
        return ret;
    }
    
    Object asType(Class cls) {
        if (Number.isAssignableFrom(cls) || IConcept.isAssignableFrom(cls) || Concept.isAssignableFrom(cls)) {
            Object o = pick(1);
            if (!(o instanceof State)) {
                throw new KlabRuntimeException("result of select() can only be cast to scalar values when the result is a single state");
            }
            return ((State)o).asType(cls);
        }
        return super.asType(cls);
    }
    
    def processArgs() {
        List<Object> ret = new ArrayList<>();
        for (Object o : args) {
            if (o instanceof Collection) {
                for (Object oo : ((Collection<?>)o)) {
                    ret.add(oo instanceof Concept ? ((Concept)oo).concept : oo);
                }
            } else {
                ret.add(o instanceof Concept ? ((Concept)o).concept : o);
            }
        }
        return ret.toArray();
    }
    
    /**
     * Use simple wildcard matching 
     * @param match
     * @return
     */
    def like(String ... match) {
        this.match = match;
        return this;
    }
}
