package org.integratedmodelling.thinklab.provenance

import org.integratedmodelling.api.provenance.IProvenance
import org.integratedmodelling.thinklab.script.Concept

class Action {
    
    Binding binding;
    IProvenance.Action action;
    
    def getAgent() {
    }
    
    def getCause() {
    }
 
    def trace(Object ... args) {
        // TODO 
    }
    
    def getMetadata() {
        return action.getMetadata();
    }
    
    public String toString() {
        return action.toString();
    }   
}
