package org.integratedmodelling.thinklab.provenance

import org.integratedmodelling.api.provenance.IProvenance
import org.integratedmodelling.thinklab.actions.DefaultAction
import org.integratedmodelling.thinklab.script.Concept

class Artifact {
    
    Binding binding;
    IProvenance.Artifact artifact;
    
    def getObservable() {
        return new Concept(artifact.observable, binding);
    }
    
	def getObservation() {
		if (artifact.getObservation() != null) {
			return DefaultAction._wrapIfNecessary(artifact.getObservation(), binding);
		}
        return null;
	}
	
    def getMetadata() {
        return artifact.getMetadata();
    }
 
    def trace(Concept concept) {
		IProvenance.Artifact ret = artifact.trace(concept.concept, concept.roleContext);
		if (ret != null) {
			return new Artifact(artifact: ret, binding: binding);
		}
		return null;
	}
    
    def collect(Concept concept) {
        Set<Artifact> ret = new HashSet<>();
        for (IProvenance.Artifact a : artifact.collect(concept.concept, concept.roleContext)) {
            ret.add(new Artifact(artifact: a, binding: binding));
        }
        return ret;
    }
    
    public String toString() {
        return artifact.toString();
    }   
    
    def getModel() {
        return artifact.getModel();
    }
    
    def getTime() {
        return artifact.getTime();
    }
    
    def getSpace() {
        return artifact.getSpace();
    }
}
