package org.integratedmodelling.lang;

public enum IAggregation {
    AGGREGATE,
    AVERAGE
}
