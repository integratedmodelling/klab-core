package org.integratedmodelling.api.services;

import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Implement this to provide batch runners for -run execution. They must
 * have no-argument constructors.
 * 
 * @author ferdinando.villa
 *
 */
public interface IBatchRunner {

    /**
     * Passed all the arguments after the -run <id> on the command line
     * @param parameters
     * @throws KlabValidationException
     */
    void setContext(Object ... parameters) throws KlabValidationException;
    
    /**
     * Called once after setContext to run the configured strategy. Should
     * return 0 on normal termination; system will (normally) exit after
     * that and return that return value.
     * 
     * @return
     * @throws KlabException
     */
    int run() throws KlabException;
}
