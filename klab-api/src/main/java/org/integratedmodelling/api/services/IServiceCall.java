/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.services;

import java.io.File;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.client.Interactive;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A request for a service. It is created by the service manager after parsing a command line
 * instruction, REST request, function call or anything else compatible. A command references an existing
 * prototype that has been used to validate the request.
 * 
 * @author Ferd
 *
 */
public interface IServiceCall {

    /**
     * Get the prototype this command has been matched to. If a ICommand exist, there
     * must be a prototype, so this is never null.
     * 
     * @return prototype for call
     */
    IPrototype getPrototype();

    /**
     * Get the command or the option value identified by the passed ID. It will have the
     * appropriate type according to prototype.
     * 
     * @param argumentId
     * @return argument value
     */
    Object get(String argumentId);

    /**
     * True only when the (optional) argument is passed. If it's not a BOOLEAN, true
     * for this one will correspond to a non-null value of get().
     * 
     * @param argumentId
     * @return true if arg or option is there
     */
    boolean has(String argumentId);

    /**
     * A command is a task, so it gets a task id with which it can be recognized, 
     * interrupted by a monitor, or anything else. 
     * TODO probably not used
     * @return task ID. 
     */
    long getTaskID();

    /**
     * Commands are issues in a session and can return it. 
     * 
     * @return the session 
     */
    ISession getSession();

    /**
     * Execute the command, creating a new instance of the executor and
     * calling the method annotated with the corresponding @Execute annotation.
     * 
     * @return the result (may be null).
     * @throws KlabException
     */
    Object execute() throws KlabException;

    /**
     * If the command has a subcommand, return it; otherwise return null.
     * 
     * @return subcommand
     */
    String getSubcommand();

    /**
     * Return the user if we have it - either because it was set explicitly or because we
     * have it from the session.
     * TODO remove - take from session
     * @return user
     */
    IUser getUser();

    /**
     * Convenience method to retrieve a string parameter or null if not passed.
     * 
     * @param string
     * @return argument as string
     */
    String getString(String string);

    /**
     * Convenience method to retrieve a string optional parameter or passed default if not passed.
     * 
     * @param string
     * @param def 
     * @return argument as string with default value
     */
    String getString(String string, String def);

    /**
     * Return the set of groups the requester belongs to. Use instead of
     * getUser().getGroups() when the service does not require user authentication but
     * may be passed groups anyway. Should never return null.
     * TODO remove
     * @return roles
     */
    Set<String> getRequesterRoles();

    /**
     * Return the set of roles the requester belongs to. Use instead of
     * getUser().getRoles() when the service does not require user authentication but
     * may be passed roles anyway. Should never return null.
     * TODO remove
     * @return groups
     */
    Set<String> getRequesterGroups();

    /**
     * If this call came with a file payload (upload), return the temporary file that
     * was generated from it.
     * TODO remove
     * @return payload
     */
    File getFile();

    /**
     * ID of the user who triggered the request, directly or indirectly (we should endeavor to
     * send it around reliably for logging purposes). If the username is unknown, return "anonymous", 
     * not null.
     * TODO remove
     * @return username
     */
    String getUsername();

    /**
     * If true, the caller has our engine authorization key (either because it got it from us or has
     * our certificate).
     * TODO remove
     * @return poh
     */
    boolean hasEngineAuthorization();

    /**
     * If not null, the caller is an engine and has communicated its authorization key.
     * TODO remove
     * @return poh
     */
    String getCallerAuthorizationKey();

    /**
     * True if the call comes from a known engine. False if coming from a personal engine, an unknown one, or a
     * naked network call.
     * TODO remove
     * @return poh
     */
    boolean isFromKnownEngine();

    /**
     * If the call is being used in a user-interaction context, this will return
     * a non-null object that can be used to ask questions.
     * 
     * @return interactive UI. 
     */
    Interactive getInteractiveUI();

}
