package org.integratedmodelling.api.services.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * When added to a @Prototype, ensures it's not processed as a service. Used in clients
 * for command line commands that share the classpath with server classes.
 * 
 * TODO move outside API.
 * 
 * @author ferdinando.villa
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface CLIPrototype {

}
