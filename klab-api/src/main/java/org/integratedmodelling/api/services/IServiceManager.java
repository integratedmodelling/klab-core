/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.services;

import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Singleton that collects prototype declarations and helps with parsing of command lines and
 * function calls. Prototypes are for anything callable - commands, language functions etc.
 * 
 * TODO Command is not the best name for these now.
 * 
 * @author ferdinando.villa
 *
 */
public interface IServiceManager {

    /**
     * 
     * @param command
     * @param executor
     * @return processed prototype
     */
    IPrototype processPrototypeDeclaration(Prototype command, Class<?> executor);

    /**
     * 
     * @param id
     * @return named prototype or null
     */
    IPrototype getPrototype(String id);

    /**
     * Parses a command line into a service call.
     * @param line
     * @param monitor
     * @param session
     * @return parsed call
     * @throws KlabException
     */
    IServiceCall parseCommandLine(String line, IMonitor monitor, ISession session) throws KlabException;

    /**
     * 
     * @param id
     * @return prototype for function name
     */
    IPrototype getFunctionPrototype(String id);

    /**
     * True if service prototype is published by a network node. Since 0.9.7, if a service is
     * available remotely it will also always be available locally.
     * 
     * @param id
     * @return true if remotely available
     */
    boolean isServiceAvailableRemotely(String id);

}
