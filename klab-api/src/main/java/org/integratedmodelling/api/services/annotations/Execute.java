/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.services.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.integratedmodelling.api.monitoring.Messages;

/**
 * Use in a command's executor to identify methods that respond to 
 * a command (or subcommand if specified). The method must take a
 * ICommand and return an Object.
 *
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Execute {

    /**
     * The subcommand ID. Default executor if not specified.
     * @return the command
     */
    String command() default "";

    /**
     * Description for the subcommand. Only used in actual subcommands.
     * @return description
     */
    String description() default "";

    /**
     * Allows to specify which arguments are required for this specific subcommand. If not
     * given, all non-optional arguments are required.
     * 
     * @return required arguments
     */
    String[]requires() default {};

    /**
     * If this is true, the service will spawn a task and return the task
     * description immediately as its result. The final result will be communicated
     * with the {@link Messages#TASK_FINISHED} message. As a default, the service
     * will block until execution is finished.
     * 
     * @return true if asynchronous
     */
    boolean asynchronous() default false;
}
