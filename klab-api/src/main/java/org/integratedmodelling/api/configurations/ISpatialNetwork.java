package org.integratedmodelling.api.configurations;

import org.integratedmodelling.api.modelling.IDirectObservation;

public interface ISpatialNetwork extends INetworkConfiguration {

    ITrajectory route(IDirectObservation from, IDirectObservation to);

}
