/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.lang;

import java.io.InputStream;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Something that can turn an input stream into Java objects representing a
 * namespace. 
 *  
 * @author Ferd
 *
 */
public interface IModelParser {

    /**
     * Parse an input stream in a given namespace. Must not specify a namespace programmatically.
     * The result is a clone of the input namespace, but contains only those model objects and 
     * axioms that were declared in the input stream - i.e. a delta compared to the existing
     * namespace (or all of it if it didn't exist originally).
     * 
     * @param input source language to be parsed into model objects to add to the namespace. It should not
     *        be legal to declare a namespace in it.
     * @param namespace a namespace ID that may identify a known namespace or not.
     * @param monitor 
     * @return a namespace containing whatever NEW model objects and axioms have been parsed from the input.
     * @throws KlabException 
     */
    INamespace parseInNamespace(InputStream input, String namespace, IMonitor monitor)
            throws KlabException;

    /**
     * Main entry point into the parser. Turns a resource into a Namespace bean, from which point on it's the job
     * of the implementation to use it. A IResolver is used to connect resource names to input streams, find namespace
     * sources across dependency chains, and handling errors and messages fed to it by the parser.
     * @param namespace 
     * 
     * @param resource should identify a resolvable URI that the passed resolver can handle (second parameter in resolve).
     * @param monitor 
     * @return a finished namespace to be turned into usable model objects by the implementation.
     * @throws KlabException
     */
    INamespace parse(String namespace, String resource, IMonitor monitor) throws KlabException;

}
