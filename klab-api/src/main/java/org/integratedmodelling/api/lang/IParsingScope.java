/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.lang;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.exceptions.KlabException;

/**
 * The parsing context for the k.IM language. One is passed to any parsing call and used
 * to keep track of context and influence the resulting beand.
 * 
 * @author Ferd
 *
 */
public interface IParsingScope {

    enum Type {
        ROOT,
        NAMESPACE,
        MODEL,
        CONCEPT,
        PROPERTY,
        OBSERVATION,
        PROJECT,
        FUNCTIONCALL,
        METADATA,
        MODEL_DOCUMENTATION,
        NAMESPACE_DOCUMENTATION,
        TRAIT_LIST,
        CHILD_LIST,
        PARENT_LIST,
        EXPOSED_TRAIT_LIST,
        ADOPTED_EXPOSED_TRAIT_LIST,
        INHERITED_TRAIT_LIST,
        ROLE_LIST,
        ROLE_RESTRICTED_OBSERVABLE_LIST,
        EQUIVALENT_CONCEPT_LIST,
        USES_RELATIONSHIP,
        HAS_RELATIONSHIP,
        CONTAINS_RELATIONSHIP,
        MODEL_OBSERVABLE,
        ANNOTATION,
        OBSERVER_OBSERVABLE,
        OBJECT_OBSERVABLE,
        OBSERVER_CONDITIONAL,
        OBSERVER,
        // the declaration inside an observable
        OBSERVABLE_DECLARATION,
        OBSERVER_CONDITIONAL_EXPRESSION,
        NUMERIC_DISCRETIZATION,
        PROPORTION_DISCRETIZATION,
        CLASSIFICATION,
        CLASSIFIER,
        CLASSIFIER_CONCEPT,
        // function call as main observed object in a model.
        OBSERVED_FUNCTIONCALL,
        DEPENDENCY,
        UNIT,
        DEPENDENCY_OBSERVABLE,
        COMPARISON_OBSERVABLE,
        TABLE_CLASSIFIER,
        // the observer that describes a literal state for a direct observation
        OBSERVATION_STATE_OBSERVER,
        // function call in a context transition action or contextualization
        COVERAGE_FUNCTION_CALL,
        // 'using' accessor call
        ACTUATOR_FUNCTION_CALL,
        // generic literal value or generation function
        VALUE,
        LIST_LITERAL,
        CONTEXTUALIZATION_ACTION,
        REIFICATION_FUNCTION_CALL,
        DISTRIBUTION_DEPENDENCY_CONDITION,
        LOOKUP_TABLE,
        OBSERVER_ATTRIBUTE,
        PROPERTY_DOMAIN,
        PROPERTY_RANGE,
        TRAIT_DESCRIBED_QUALITY,
        QUALITY_DESCRIBED_CONFIGURATION,
        CONFERRED_TRAIT_LIST,
        AFFECTED_QUALITIES_LIST,
        TARGET_TRAIT_LIST,
        CURRENCY,
        CURRENCY_CONCEPT,
        ROLE_TARGET_OBSERVABLE_LIST,
        CONTEXTUALIZED_CONCEPT,
        CONTEXTUALIZED_ROLE,
        CONTEXTUALIZED_RESOLUTION,
        // context for an event declaration as trigger for a contextual action
        EVENT_CONTEXTUALIZATION,
        IMPLIES_RELATIONSHIP,
        ROLE_DESCRIPTION_LIST,
        PROJECT_DEFAULT_KNOWLEDGE
    }

    /**
     * Log a warning at the specified line and enable any monitors to track it later.
     * 
     * @param message
     * @param line
     */
    void warning(String message, int line);

    /**
     * Log an informational message at the specified line and enable any monitors to track
     * it later.
     * 
     * @param message
     * @param line
     */
    void info(String message, int line);

    /**
     * Log an error at the specified line and enable any monitors to track it later.
     * 
     * @param message
     * @param line
     */
    void error(String message, int line);

    /**
     * Check if the context is within the scope of the given context type.
     * 
     * @param type
     * @return true if within scope.
     */
    boolean isInScope(Type type);

    /**
     * The project from the current scope, or null.
     * 
     * @return project in scope
     */
    IProject getProject();

    /**
     * Get the namespace from the current scope, or null.
     * 
     * @return namespace in scope.
     */
    INamespace getNamespace();

    /**
     * Timestamp of resource in scope, or 0.
     * 
     * @return the time of the resource in scope.
     */
    long getTimestamp();

    /**
     * Get a sub-context within a specified sub-scope.
     * 
     * @param type
     * @return a new context in the requested sub-scope.
     */
    IParsingScope get(Type type);

    /**
     * Get a context in project scope below ours.
     * 
     * @param project
     * @return new context in project scope.
     */
    IParsingScope get(IProject project);

    /**
     * This one opens the resource, and if a namespace ID is not set by an upstream
     * context, defined the namespace ID based on the resource name. Implementations must
     * ensure that the resource is closed when this context is popped from the stack.
     * 
     * @param resource
     * @return context in resource scope
     * @throws KlabException
     */
    IParsingScope forResource(Object resource) throws KlabException;

    /**
     * Define the namespace ID that will be parsed downstream. This will disable detection
     * of namespace ID from the resource that will follow.
     * 
     * @param namespaceId
     * @return context in namespace scope with forced ID.
     */
    IParsingScope forNamespace(String namespaceId);

    /**
     * True if this context has parsed or is parsing said namespace. Used to avoid
     * needless recursion in project loading.
     * 
     * @param namespaceId
     * @return true if namespace ID has been already seen in context.
     */
    boolean hasSeen(String namespaceId);

    /**
     * Return the number of errors seen in the context.
     * 
     * @return number of error in scope.
     */
    int getErrorCount();

    /**
     * Report the number of warnings seen in the context.
     * 
     * @return number of warnings in scope.
     */
    int getWarningCount();

    /**
     * Return a copy of this scope with a specified monitor.
     * 
     * @param monitor
     * @return
     */
    IParsingScope withMonitor(IMonitor monitor);

}
