package org.integratedmodelling.api.monitoring;

import org.integratedmodelling.api.modelling.IModelBean;

/**
 * TODO still to be completely defined - the methods exposed are not the important ones.
 * @author ferdinando.villa
 *
 */
public interface INotificationBus {

    /**
     * Just access our configured object mapper, for external access when we
     * want to wrap an object into a notification.
     * @param beanJSON 
     * @param cls 
     * @return the deserialized object
     */
    <T extends IModelBean> T fromJSON(String beanJSON, Class<? extends IModelBean> cls);

    /**
     * Just access our configured object mapper, for external access when we
     * want to wrap an object into a notification.
     * 
     * @param bean
     * @return the stringified object
     */
    String toJSON(IModelBean bean);

}
