/*******************************************************************************
 * Copyright (C) 2007, 2014:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.monitoring;

/**
 * Collects the message types sent around through {@link IMonitor send} and
 * {@link IMonitor info}.
 * 
 * @author ferdinando.villa
 *
 */
public interface Messages {

    public static final String TASK_MESSAGE_PREFIX              = "TASK_";
    public static final String ENGINE_MESSAGE_PREFIX            = "ENGINE_";
    public static final String RESOURCE_MESSAGE_PREFIX          = "RESOURCE_";
    public static final String PROVENANCE_MESSAGE_PREFIX        = "PROVENANCE_";

    /**
     * messages that redefine task status. They all start with TASK_MESSAGE_PREFIX, which
     * triggers reevaluation of context state for communication to clients.
     */
    public static final String TASK_STARTED                     = "TASK_STARTED";
    public static final String TASK_FAILED                      = "TASK_FAILED";
    public static final String TASK_FINISHED                    = "TASK_FINISHED";
    public static final String TASK_INTERRUPTED                 = "TASK_INTERRUPTED";
    public static final String TIME_TRANSITION                  = "TASK_TRANSITION";

    /**
     * These are not used at the moment.
     */
    public static final String ENGINE_BOOTING                   = "ENGINE_BOOTING";
    public static final String ENGINE_AVAILABLE                 = "ENGINE_AVAILABLE";
    public static final String ENGINE_STOPPED                   = "ENGINE_STOPPED";
    public static final String ENGINE_EXCEPTION                 = "ENGINE_EXCEPTION";
    public static final String ENGINE_STATUS                    = "ENGINE_STATUS";

    /*
     * --------------------------------------------------------------------- Predefined
     * classes for info messages. Used only for display.
     * ---------------------------------------------------------------------
     */

    public static final String INFOCLASS_NEW_RESOURCE_AVAILABLE = "INFO_RESOURCE_NEW_AVAILABLE";
    public static final String INFOCLASS_RESOURCE_MODIFIED      = "INFO_RESOURCE_MODIFIED";
    public static final String INFOCLASS_DOWNLOAD               = "INFO_DOWNLOAD";
    public static final String INFOCLASS_UPLOAD                 = "INFO_UPLOAD";
    public static final String INFOCLASS_COMPLETED              = "INFO_COMPLETED";
    public static final String INFOCLASS_NETWORK                = "INFO_NETWORK";
    public static final String INFOCLASS_COMPONENT              = "INFO_COMPONENT";
    public static final String INFOCLASS_MODEL                  = "INFO_MODEL";
    public static final String INFOCLASS_TIME                   = "INFO_TIME";
    public static final String INFOCLASS_LOCK                   = "INFO_LOCK";
    public static final String INFOCLASS_HAPPY                  = "INFO_HAPPY";
    public static final String INFOCLASS_SAD                    = "INFO_SAD";
    public static final String INFOCLASS_USER_OWN               = "INFO_USER_OWN";
    public static final String INFOCLASS_USER_FOREIGN           = "INFO_USER_FOREIGN";
    public static final String INFOCLASS_STOP                   = "INFO_STOP";

    /*
     * graph types
     */
    public static final String GRAPH_PROVENANCE                 = "GRAPH_PROVENANCE";
    public static final String GRAPH_DATAFLOW                   = "GRAPH_DATAFLOW";
    public static final String GRAPH_RESOLUTION                 = "GRAPH_RESOLUTION";
    public static final String GRAPH_STRUCTURE                  = "GRAPH_STRUCTURE";

}
