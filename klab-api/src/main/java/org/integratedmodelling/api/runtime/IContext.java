/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.runtime;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IReport;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IEventBus;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.exceptions.KlabException;

/**
 * The context holds the state of an ongoing observation process. Each observation in it
 * has a path that can be used to identify it. Contexts are initialized with a
 * ISubjectObserver, and each further observation made "in" them becomes part of an
 * observation tree starting at the subject observed by that, and gets a unique path with
 * which it can be referred to.
 * 
 * TODO - improve and streamline. The context is the "world" all observations are part
 * of, and contains semantics for the individuals (IObservation and IObservable) 
 * 
 * @author ferdinando.villa
 * 
 */
public interface IContext extends IIdentity {

    /**
     * Each context has an ID that's unique within a session. The id serves as
     * authentication token in REST calls concerning this context.
     * 
     * @return the unique context's ID.
     */
    String getId();

    /**
     * True if the context has been created without any concrete observation in it. It may
     * contain abstracts (space, time etc) or not at this stage, but no root object.
     * 
     * @return whether the context is empty.
     */
    boolean isEmpty();

    /**
     * True if context is not empty and all states have been fully contextualized; this
     * means that either there is no time scale, or if so, run() was called to complete
     * observation in time and has finished. It will also return true if the schedule was
     * interrupted from the monitor or by internal error conditions.
     * 
     * Monitor functions should be used to understand whether the execution had errors or
     * was interrupted before accessing states and objects.
     * 
     * @return whether no more observations can be made.
     */
    boolean isFinished();

    /**
     * True if run() has been called and isFinished() returns false.
     * 
     * @return whether temporal actions are being computed.
     */
    boolean isRunning();

    /**
     * Return a context that is focused on the passed observation, which becomes the
     * subject for the next observe(). The original context is not modified.
     * 
     * @param observation
     * @return a context where the passed observation is the focus for future observe()
     * calls.
     */
    IContext focus(ISubject observation);
    
    /**
     * Return a context that will substitute the passed extents into the current scale,
     * producing observations in a modified scale. Normally only used from within the
     * Groovy API.
     * 
     * @param extents
     * @return
     */
    IContext rescale(Collection<IExtent> extents);

    /**
     * Return a proxy to this context that will make all the observations called on it
     * affected by the passed scenarios. It will reset any scenarios previously defined
     * and not modify the context it is called upon.
     * 
     * @param scenarios
     * @return a new IContext that wraps the original object, but whose model choices will
     * be affected by the scenarios. The states created will be in the same subject.
     */
    IContext inScenario(String... scenarios);

    /**
     * Run all temporal transitions (or do nothing if there is no time in the scale). Call
     * only on the root context or be punished. Can only be called once. Blocks until all
     * transitions have ran or the root subject has died. Most applications except scripts
     * will want to use the asynchronous version, as this may run for very long times.
     * After this runs, isFinished() will return true. Any errors and events will be
     * communicated to the monitor in the context.
     * 
     * @return the finalized context, which is very likely the same one this was called
     * on.
     * 
     * @throws KlabException
     */
    ITask run() throws KlabException;

    /**
     * Observe the passed observable, which may be a semantic object or simply the string
     * identifier of an object. The focus for the observation (context of the resulting
     * observation) is the main subject or whatever was focused on using focus().
     * 
     * @param observable
     * @return a task that is performing the observation. Call finish() on it to retrieve
     * the context in a synchronous environment.
     * @throws KlabException
     */
    ITask observe(Object observable) throws KlabException;

    /**
     * Get the coverage after the last observation on the context.
     * 
     * @return the current coverage.
     */
    ICoverage getCoverage();

    /**
     * The root subject.
     * 
     * @return the root subject. Null if the first task is still executing.
     */
    ISubject getSubject();

    /**
     * Return all the tasks that have been run in this context, in order of execution.
     * 
     * @return the list of tasks, earliest first.
     */
    List<ITask> getTasks();

    /**
     * Get the path of the passed observation in this context, or null if the observation
     * is not part of this context.
     * 
     * @param observation
     * @return the path for the passed observation.
     */
    String getPathFor(IObservation observation);

    /**
     * Return the subject or state identified by the passed path. Paths are returned by
     * getPathFor(). Used to identify observations in an observation context.
     * 
     * @param path
     * @return the observation corresponding to the path, or null if none is found.
     */
    IObservation get(String path);
    
    /**
     * Persist to passed file (may be a directory, according to implementation). A new
     * read-only IContext should be able to be reconstructed from the resulting file.
     * 
     * @param file
     * @param path persist the passed observation or the whole context if path is null or
     * "/".
     * @param options
     * @throws KlabException
     */
    void persist(File file, String path, Object... options) throws KlabException;

    /**
     * Contexts exist in a session and they must be able to return it.
     * 
     * @return the session this context is part of.
     */
    ISession getSession();

    /**
     * Local time of creation.
     * @return time of creation in milliseconds since epoch.
     */
    long getCreationTime();

    /**
     * Temporal contexts keep track of the last temporal transition they've seen.
     * Non-temporal contexts or temporal contexts before {@link #run()} will return null.
     * 
     * @return the last temporal transition seen
     */
    ITransition getLastTemporalTransition();

    /**
     * Return the current provenance graph. This grows as new observations are made.
     * 
     * @return the provenance
     */
    IProvenance getProvenance();

    /**
     * Return the resolution scope for this context, so that new observations can be made. If the
     * context is inactive (i.e. not part of an active observation session, such as in a client 
     * implementation or a read-only context) the result will be null. 
     * 
     * @return the resolution scope, or null.
     */
    IResolutionScope getScope();
    
    /**
     * Return all the observations made during the last {@link #observe(Object)} call that
     * are directly linked to the root subject ({@link #getSubject()}). Will return an
     * empty collection if called more than once without observing anything in between
     * {@link #observe(Object)} calls.
     * 
     * @return all new observations.
     */
    Collection<IObservation> getNewObservations();

    /**
     * Get the report for the context. 
     * 
     * @return
     */
    IReport getReport();

    /**
     * Event bus. Each context has one, never null.
     * 
     * @return
     */
    IEventBus getEventBus();
    
}
