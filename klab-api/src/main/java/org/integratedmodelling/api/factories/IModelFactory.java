/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.factories;

import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IParsingScope;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.monitoring.IKnowledgeLifecycleListener;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IGridMask;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Encapsulates all functions that make modeling possible, like the creation of
 * observations, accessors, storage and execution of runtime functions. A model factory is
 * always available as Env.MFACTORY; according to implementation, it may create dummy
 * objects or functional ones.
 * 
 * @author Ferd
 *
 */
public interface IModelFactory {

    /**
     * Add a listener to intercept generation of k.IM knowledge.
     * 
     * @param listener
     */
    void addKnowledgeLifecycleListener(IKnowledgeLifecycleListener listener);

    /**
     * Get a top-level context for parsing a model tree.
     * 
     * @return the parsing context for a new parse operation.
     */
    IParsingScope getRootParsingContext();

    /**
     * Call the passed function using whatever mechanism for code resolution is available
     * in the runtime.
     * 
     * @param function
     * @param monitor
     * @param model
     * @param context
     * @return the result of calling the passed function call
     * @throws KlabException
     */
    Object callFunction(IFunctionCall function, IMonitor monitor, IModel model, IConcept... context)
            throws KlabException;

    /**
     * Create the storage for a state of this observer in this scale, using a backing
     * dataset if passed. If state is dynamic, prepare for storage of timeslices.
     * 
     * @param observer
     * @param scale
     * @param dataset
     * @param isDynamic
     * @param isProbabilistic
     * @return a suitable storage for the passed observer and scale.
     */
    IStorage<?> createStorage(IObserver observer, IScale scale, IDataset dataset, boolean isDynamic, boolean isProbabilistic);

    /**
     * 
     * @param observer
     * @param context
     * @param contextObservation
     * @param monitor
     * @return a subject capable of being contextualized
     * @throws KlabException
     */
    IActiveDirectObservation createSubject(IDirectObserver observer, IContext context, IActiveDirectObservation contextObservation, IMonitor monitor)
            throws KlabException;

    /**
     * Create the favorite implementation of IScale based on the extent definition in the
     * passed functions.
     * 
     * @param monitor
     * 
     * @param definition
     * @return a new scale.
     */
    IScale createScale(Collection<IFunctionCall> definition, IMonitor monitor);

    /**
     * Create a scale out of any objects, turning each into extents the way they are
     * supposed to in the implementation. Could be passed extents, WKT strings, function
     * calls, time locations or intervals, etc. If any of the objects can't be turned into
     * an extent, throw a runtime exception.
     * 
     * @param monitor
     * @param objects
     * @return a new scale
     */
    IScale createScale(IMonitor monitor, Object... objects);

    /**
     * Create a new scale identical to the passed one in an implementation compatible with
     * the factory's scope.
     * 
     * @param monitor
     * @param scale
     * @return
     * @throws KlabException 
     */
    IScale copyScale(IScale scale);

    
    /**
     * Create an actuator for the passed object based on its definition; if actions are
     * defined, wrap any result into an action actuator. The passed function call may be
     * null - it will be invoked anyway if the observer has actions. If the function call
     * returns a contextualizer, wrap it into a contextualizing actuator.
     * 
     * @param call
     * @param observer
     * @param monitor
     * @return an actuator from the passed call
     * @throws KlabException
     */
    IContextualizer getContextualizer(IFunctionCall call, IObservingObject observer, IResolutionScope scope, IProvenance.Artifact provenance, IMonitor monitor)
            throws KlabException;

    /**
     * Call the function and return the created datasource.
     * 
     * @param datasource
     * @param monitor
     * @return a new datasource
     * @throws KlabException
     */
    IDataSource createDatasource(IFunctionCall datasource, IMonitor monitor) throws KlabException;

    /**
     * Call the function and return the created object.
     * 
     * @param objectsource
     * @param monitor
     * 
     * @param datasource
     * @return a new objectsource
     * @throws KlabException
     */
    IObjectSource createObjectsource(IFunctionCall objectsource, IMonitor monitor) throws KlabException;

    /**
     * Create a constant data source that will return the passed literal no matter what.
     * 
     * @param inlineValue
     * @return a constant datasource that produces the passed object.
     */
    IDataSource createConstantDataSource(Object inlineValue);

    /**
     * Create and return a spatial index for this object. Will only be used in model
     * engines so safe to return null in a client.
     * 
     * @param space
     * @return a spatial index for the passed space
     */
    ISpatialIndex getSpatialIndex(ISpatialExtent space);

    /**
     * Generic factory to turn beans into objects and objects into beans. Will typically
     * defer to instance methods. Objects that need to be different at client/server ends
     * can be intercepted in a specialized factory.
     * 
     * @param toAdapt
     * @param desiredClass
     * @return the adapted object
     */
    <T> T adapt(Object toAdapt, Class<? extends T> desiredClass);

    /**
     * Remove any persistent artifacts related to this namespace. Called at project
     * undeploy before the NS is physically removed.
     * 
     * @param namespaceId
     *            the namespace to clean.
     * @throws KlabException
     */
    void cleanNamespaceArtifacts(String namespaceId) throws KlabException;

    /**
     * Ensure that a passed extent (which may be implemented in any way) has an
     * implementation that fits the purposes of the running engine.
     * 
     * @param forcing
     * @return
     * @throws KlabException
     */
    IExtent sanitizeExtent(IExtent forcing) throws KlabException;

    /**
     * Return the class annotated with a particular service call without creating the
     * object. Only works at the engine side, where it counts.
     * 
     * @param contextualizerCall
     * @return
     */
    Class<?> getContextualizerClass(IFunctionCall contextualizerCall);

    public int[] countShapes(Collection<IShape> shape, IGrid grid) throws KlabException;

    public IGridMask createMask(Collection<IShape> shape, IGrid grid) throws KlabException;
    
    public IGridMask createMask(IShape shape, IGrid grid) throws KlabException;

    public IGridMask addToMask(IShape shape, IGridMask mask) throws KlabException;
}
