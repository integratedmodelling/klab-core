/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.factories;

import java.io.File;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.lang.IParsingScope;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IProjectLifecycleListener;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.project.IDependencyGraph;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabResourceNotFoundException;

/**
 * The server side of project management. Projects define namespaces, containing knowledge
 * - either ontologies or model objects, plus any support resources necessary.
 * 
 * @author Ferd
 *
 */
public interface IProjectManager {

//    /**
//     * If we have loaded a worldview, return its identifier. One or more project may
//     * identify themselves as part of it, and two projects with different worldviews are
//     * not allowed in the manager.
//     * 
//     * @return the currently loaded worldview, or null.
//     */
//    String getWorldview();

    /**
     * Add a listener so project activity can be monitored.
     * 
     * @param listener
     */
    public void addListener(IProjectLifecycleListener listener);

    /**
     * Return a specific project if registered, or null if not found. Do not throw an
     * exception. The project may be loaded or unloaded.
     * 
     * @param projectId
     * @return the project, or null
     */
    public IProject getProject(String projectId);

    /**
     * Return all the projects registered with the manager. They may or may not be loaded.
     * 
     * @return all projects
     */
    public Collection<IProject> getProjects();

    /**
     * Return all the components deployed to the manager.
     * 
     * @return all components
     */
    public Collection<IComponent> getComponents();

    /**
     * Return a specific component if registered, or null if not found. This should not
     * return a component that exists but is not in our component repository (i.e. has
     * been synchronized from an external source). If so, this function should return
     * null.
     * 
     * @param componentId
     * @return the component, or null.
     */
    public IComponent getComponent(String componentId);

    /**
     * If we have the passed component in the repository, return it after ensuring it's
     * been initialized successfully; otherwise try to find it on the network, deploy it,
     * initialize it and return it. Throw an exception if the component was not found or
     * could not be initialized.
     * 
     * @param componentId
     * @return a usable component.
     * @throws KlabResourceNotFoundException if component could not be found or
     * initialized
     * @throws KlabException if anything else went wrong
     */
    public IComponent getDeployedComponent(String componentId) throws KlabException;

    /**
     * Load every project registered in order of dependency, minimizing unnecessary
     * unloading unless the force option is true.
     * 
     * Return all the namespaces that were read anew - do not return those that were
     * loaded and had untouched sources or were read-only.
     * @param forceReload
     * @param context
     * @return all namespaces that have modified content
     * 
     * @throws KlabException
     */
    List<INamespace> load(boolean forceReload, IParsingScope context) throws KlabException;

    /**
     * Deploy a project contained in a given archive or directory. Projects deployed
     * through this one end up in the main area of the workspace, overriding any versions
     * of the same that exist in the network synchronized storage.
     * 
     * @param pluginId
     * @param resourceId
     * @param monitor
     * @return the project (registered, not loaded) after successful deployment
     * @throws KlabException
     */
    IProject deployProject(String pluginId, String resourceId, IMonitor monitor) throws KlabException;

    /**
     * Synonymous of unload followed by unregister.
     * 
     * @param projectId
     * @throws KlabException
     */
    void undeployProject(String projectId) throws KlabException;

    /**
     * Register project contained in the passed directory. If the directory does not
     * contain a project, throw an exception. If the project is already registered, do
     * nothing. Do not load the project.
     * 
     * @param projectDir
     * @return the project after registration.
     * @throws KlabException
     */
    IProject registerProject(File projectDir) throws KlabException;

    /**
     * Load a project, ensuring all its prerequisites are also loaded. This should work as
     * refresh() as well, but not throw an exception if the project isn't loaded. Return
     * the list of namespaces that were actually read - i.e., do not return namespaces
     * that were already loaded and needed no refresh because their source files had not
     * changed.
     * 
     * @param projectId
     * @param scope the context for parsing
     * @return all the namespaces created by the loading
     * @throws KlabException
     */
    List<INamespace> loadProject(String projectId, IParsingScope scope) throws KlabException;

    /**
     * Register component contained in the passed directory. If the directory does not
     * contain a component, throw an exception. If the component is already registered, do
     * nothing. Load binary assets but not knowledge (call
     * {@link #loadComponent(IComponent, IParsingScope)} for that).
     * 
     * @param projectDir
     * @return the component after registration.
     * @throws KlabException
     */
    IComponent registerComponent(File projectDir) throws KlabException;

    /**
     * Load the knowledge in a previously successfully initialized component. Will throw
     * an exception if the component is not active. Otherwise this works like loadProject,
     * except components cannot be unloaded, and any attempt to load the same component
     * more than once will be ignored.
     * 
     * The order of operation should be: <pre> deployComponents() or
     * registerComponent(...); // loads any binary assets from components loadProjects();
     * // load basic knowledge that components may need foreach component
     * loadComponent(component); // load knowledge once both base knowledge and binary
     * assets are there </pre>
     * 
     * @param component
     * @param scope
     * @return the namespaces defined in the component.
     * @throws KlabException
     */
    List<INamespace> loadComponent(IComponent component, IParsingScope scope) throws KlabException;

    /**
     * Unregister the project, unloading it (and those that depend on it) if loaded. After
     * this is called, load(id) will throw an exception.
     * 
     * @param projectId
     * @throws KlabException
     */
    void unregisterProject(String projectId) throws KlabException;

    /**
     * The project manager should not have a watched project directory by default -
     * projects directories can be registered individually. If this is given, it should
     * register all the projects in it.
     * 
     * @param projectDirectory
     * @throws KlabException
     */
    void registerProjectDirectory(File projectDirectory) throws KlabException;

    /**
     * Unload the project. Should count references to the project and leave its
     * definitions in the knowledge base unless no other loaded projects reference it.
     * 
     * @param projectId
     * @throws KlabException
     */
    void unloadProject(String projectId) throws KlabException;

    /**
     * Get the up to date dependency graph. Should never be used in a circumstance when
     * load() could be called concurrently.
     * 
     * @return the graph of dependencies
     */
    IDependencyGraph getDependencyGraph();

    /**
     * Reload only the passed namespace.
     * 
     * @param ns
     * @throws KlabException
     */
    INamespace reloadNamespace(INamespace ns) throws KlabException;

    /**
     * true if load() has been called at least once. If this returns false, the model
     * manager is probably unaware of what the project space contains.
     * 
     * @return true if load was called during the lifetime of this manager
     */
    boolean hasBeenLoaded();

    /**
     * Clear all projects in the workspace and remove their namespaces from the model
     * environment and the kbox.
     * 
     * @param clearDeployed also clear remotely synchronized projects
     * @throws KlabException
     */
    void clearWorkspace(boolean clearDeployed) throws KlabException;

}
