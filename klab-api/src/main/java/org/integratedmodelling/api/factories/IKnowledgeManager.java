/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.factories;

import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IKnowledgeIndex;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.ui.IBookmarkManager;

/**
 * Handles creation, management, storage and introspection on semantic objects.
 * 
 * @author Ferd
 *
 */
public interface IKnowledgeManager {

    /**
     * Find and return the named concept. If not found, return null.
     * @param conceptId 
     * @return the concept with passed id, or null.
     */
    IConcept getConcept(String conceptId);

    /**
     * Find and return the passed property. If not found, return null.
     * 
     * @param propertyId
     * @return the property with passed id, or null
     */
    IProperty getProperty(String propertyId);

    /**
     * Find and return a concept or property with this id. If not found, return null.
     * 
     * @param fullId
     * @return any knowledge (concept or property) with passed id, or null.
     */
    IKnowledge getKnowledge(String fullId);

    /**
     * Return the concept at the root of all the concept hierarchy.
     * 
     * @return the root concept (usually owl:Thing)
     */
    IConcept getRootConcept();

    /**
     * This may be overkill, but for now we distinguish some "upper" abstract knowledge from the
     * namespaces returned by the model manager, which contain user knowledge and model content.
     * This one is expected to return namespaces corresponding to all the core ontologies installed
     * outside the modeling and project subsystems. Typically that will correspond to upper ontologies
     * that users do not necessarily need to see, hence the distinction.
     * 
     * @param ns the namespace (ontology name).
     * @return A namespace or null if not found.
     */
    INamespace getCoreNamespace(String ns);

//    /**
//     * Return the authority corresponding to the passed ID, or null if it was not installed or authorized.
//     * 
//     * @param id
//     * @return the authority named, or null.
//     */
//    IAuthority getAuthority(String id);
    
    /**
     * Return the authority that generated the passed concept, if any, or
     * null.
     * 
     * @param concept
     * @return
     */
    IAuthority<?> getAuthorityFor(IConcept concept);

    /**
     * Return or create the passed ontology.
     * 
     * @param id
     * @return an ontology, possibly newly created.
     */
    IOntology requireOntology(String id);

    /**
     * Return the knowledge index. Server-based engine implementations may return
     * null, as the index is used only by interactive users.
     * 
     * @return the index, creating if necessary.
     */
    IKnowledgeIndex getIndex();

    /**
     * Knowledge managers (at least at the client side) also keep track of user 
     * bookmarks by exposing a bookmark manager.
     * 
     * @return the bookmark manager
     */
    IBookmarkManager getBookmarkManager();

}
