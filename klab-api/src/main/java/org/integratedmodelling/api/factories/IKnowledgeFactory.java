/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.factories;

import java.io.File;
import java.net.URL;
import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.exceptions.KlabException;

/**
 * @author ferdinando.villa An extended knowledge manager that can operate on the
 *         knowledge base.
 */
public interface IKnowledgeFactory extends IKnowledgeManager {

    /**
     * Supposed to check if the ontology we're trying to import is already in the
     * repository, and only load it if it's not there or if there is an older version.
     * Versioning is not formally defined at this stage, so it's likely to only translate
     * to "load if not present" for the time being.
     * 
     * @param url
     *            the ontology URL, either local or web based.
     * @param name
     *            the name by which we want the ontology to be known, or null if we don't
     *            care.
     * @return the ID of the ontology if it has been loaded or refreshed, null otherwise
     * @throws KlabException
     *             if anything goes wrong
     */
    public abstract IOntology refreshOntology(URL url, String name) throws KlabException;

    /**
     * Release the passed ontology and delete from repository. Nothing should happen if
     * name is not found.
     * 
     * @param s
     *            name of ontology to release.
     * @return whether the ontology was actually released.
     */
    public abstract boolean releaseOntology(String s);

    /**
     * Empty the repository.
     */
    public abstract void releaseAllOntologies();

    /**
     * retrieve the named ontology as an Ontology object.
     * 
     * @param ontName
     *            the name of the ontology
     * @return the Ontology object.
     */
    public abstract IOntology getOntology(String ontName);

    /**
     * retrieve a Collection of all ontologies in repository
     * 
     * @param includeInternal
     *            if true, the result will contain the core knowledge.
     * @return collection of requested ontologies.
     */
    public abstract Collection<IOntology> getOntologies(boolean includeInternal);

    /**
     * Create a new ontology with given ID, using defaults for the rest of the URI.
     * Complain if the ontology exists.
     * 
     * @param id
     * @param ontologyPrefix
     * @return the ontology
     * @throws KlabException
     */
    public abstract IOntology createOntology(String id, String ontologyPrefix) throws KlabException;

    /**
     * Return all concepts whose direct and only parent is the root concept (e.g.
     * owl:Thing).
     * 
     * @return all root concepts
     */
    public abstract Collection<IConcept> getRootConcepts();

    /**
     * Get a collection of all the known concepts. Not sure it should stay.
     * 
     * @return all known concepts
     */
    public abstract Collection<IConcept> getConcepts();

    /**
     * Export given ontology as an OWL file. Should return the original file if the
     * ontology was read from a local file, or create a temporary file if the ontology was
     * created through the API.
     * 
     * @param ontologyId
     * @return a file containing the ontology
     * @throws KlabException
     */
    public abstract File exportOntology(String ontologyId) throws KlabException;

    /**
     * Get the owl:Nothing concept or anything we use to say 'something that cannot
     * exist'.
     * 
     * @return the owl:Nothing concept
     */
    public abstract IConcept getNothing();

    /**
     * Get the property corresponding to the passed relationship concept, if any. If the
     * concept is not a relationships, return null.
     * 
     * @param knowledge
     * @return
     */
    public abstract IProperty getProperty(IConcept knowledge);

}
