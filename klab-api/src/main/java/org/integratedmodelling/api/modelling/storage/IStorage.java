/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.storage;

import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Each IState has one of these, which contains the actual data. The IObserver produce the
 * proper storage for each observation. IStorage can be marshalled through web services so
 * that there is no ambiguity about the data type in a state. Semantically it behaves like 
 * a linear array, with added functionalities. States will reinterpret the offsets according
 * to the scale.
 * 
 * @author ferdinando.villa
 * @param <T> 
 *
 */
public interface IStorage<T> {

    /**
     * Get the n-th object
     * @param index
     * @return value at index
     */
    T get(int index);

    /**
     * Set the value at given index. Improper values are a runtime exception.
     * 
     * @param index
     * @param value
     */
    void set(int index, Object value);

    /**
     * Bulk set of raw data in specified position.
     * @param data
     * @param locators
     */
    void set(Object data, Locator... locators);

    /**
     * Number of total objects.
     * 
     * @return total count of states
     */
    int size();

    /**
     * Dynamic storage is for data that won't change after initialization is complete, i.e.
     * will not change in time. This is more a "dynamic" than "read only" status
     * 
     * Distinguishing dynamic storage is important because if styatic, no space will be
     * wasted for history in backing datasets and model output (the time dimension won't
     * be added to the corresponding variables). Space occupation for temporal/spatial
     * datasets can be very high.
     * 
     * By default, all inputs of a model will be static unless the corresponding states 
     * are tagged writable by a contextualizer during initialize() or the model has 
     * change/integrate actions that depend on time. Outputs are writable by default;
     * contextualizers should create read-only states when appropriate.
     * 
     * @return true if dynamic
     */
    boolean isDynamic();

    /**
     * Get a string encoding the data content in a particular slice of the context. From this, we must
     * be able to reconstruct its contents using setBytes() in the same context. Used to pass states across
     * web services.
     * 
     * We want this to be the 
     * smallest string possible as long as it's fast to encode/decode - the current
     * implementation produces and consumes float LZ4-compressed data encoded as base64.
     * @param locators any locator that 
     * @return encoded bytes at locator
     * @throws KlabException 
     */
    String getEncodedBytes(Locator... locators) throws KlabException;

    /**
     * Decode a string returned by getEncodedBytes() and set own state accordingly. Assumes
     * the state is already correctly set with observer and scale. Must also understand the
     * data type from the string itself. If state is dynamic, this only sets the timeslice
     * in live storage.
     * 
     * @param encodedBytes
     * @param locators 
     */
    void setBytes(String encodedBytes, Locator... locators);

    /**
     * Return the Java class of the data item. According to usage it may
     * different so it's not necessarily T.class (e.g. it may be a POD, 
     * a distribution or a fuzzy value).
     * 
     * @return data class
     */
    Class<?> getDataClass();

    /**
     * This one is called before any time transition that affects the state is computed. Meant
     * to allow disk-backed states to flush their content to disk in a synchronous and
     * efficient way.
     * 
     * @param incomingTransition
     */
    void flush(ITransition incomingTransition);

    /**
     * Minimum value of numeric equivalent of content across the whole storage, or
     * Double.NaN if not applicable or no-data across.
     * 
     * @return min value if any
     */
    double getMin();

    /**
     * Maximum value of numeric equivalent of content across the whole storage, or
     * Double.NaN if not applicable or no-data across.
     * 
     * @return max value if any
     */
    double getMax();

    /*
     * TODO this shouldn't really be exposed in the API.
     */
    void setChanged(boolean b);

    /**
     * True if anything has changed the values since the last setChanged(false).
     * 
     * @return true if modified since last checked
     */
    boolean hasChanged();

    /**
     * If we have a SCALAR value that describes the latest version of the data, return it. Used only for
     * display of non-distributed states. Return null if the latest value is distributed or we can't 
     * assess the value or the latest time updated.
     * 
     * @return latest value as scalar
     */
    Object getLatestAggregatedValue();

    /**
     * If true, this stores probability distributions.
     * 
     * @return
     */
    boolean isProbabilistic();

}
