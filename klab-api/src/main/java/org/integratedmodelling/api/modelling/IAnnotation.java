/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Map;

import org.integratedmodelling.api.services.IPrototype;

/**
 * Annotation (the Java kind, i.e. @ something). It's a function call syntactically, so
 * it gets the same interface, minus the call part.
 * 
 * @author Ferd
 *
 */
public interface IAnnotation extends ILanguageObject {

    /*
     * known annotations (used in parser)
     */
    /**
     * 
     */
    String DEPRECATED = "deprecated";
    
    /**
     * 
     */
    String EXPORT     = "export";

    /**
     * name given to a single parameter passed by itself, outside of a named list.
     */
    String DEFAULT_PARAMETER_NAME = "value";

    /**
     * @return the annotation id
     */
    public String getId();

    /**
     * @return the prototype for the annotation. Currently null - annotations are
     * free-form.
     */
    public IPrototype getPrototype();

    /**
     * The parameters passed to the call.
     * 
     * @return parameter map
     */
    public Map<String, Object> getParameters();

    /**
     * The namespace this lives in
     * @return namespace
     */
    public INamespace getNamespace();

}
