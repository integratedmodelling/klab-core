/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.visualization;

import java.awt.Color;
import java.awt.Image;
import java.awt.image.IndexColorModel;

/**
 * Colormaps are generated in visualizations. 
 * 
 * @author ferdinando.villa
 *
 */
public interface IColormap {

    /**
     * 
     * @return the number of different colors
     */
    int getColorCount();

    /**
     * 
     * @return t the color model
     */
    IndexColorModel getColorModel();

    /**
     * 
     * @return true if the zero is transparent (not included in the colormap)
     */
    boolean hasTransparentZero();

    /**
     * 
     * @param index
     * @return the color at the specified index
     */
    Color getColor(int index);

    /**
     * @param width
     * @param height
     * @return an image of the color map
     */
    Image getImage(int width, int height);

}
