package org.integratedmodelling.api.modelling;

/**
 * Extent dimensions understood in this implementation.
 * 
 * @author ferdinando.villa
 *
 */
public enum ExtentDimension {

    PUNTAL,
    LINEAL,
    AREAL,
    VOLUMETRIC,
    TEMPORAL,
    CONCEPTUAL
}
