/*******************************************************************************
 * Copyright (C) 2007, 2014:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.storage;

import java.io.File;

import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A dataset is storage for observations that share a scale. It can be accessed using
 * standard Thinklab observables and produces IStorage objects for states that can be used
 * for I/O.
 * 
 * @author Ferdinando Villa
 */
public interface IDataset {

    /**
     * Scale is uniform across the dataset, as understood for a dataset in common
     * practice. A dataset must be able to produce the scale it represents.
     * 
     * @return the scale
     */
    IScale getScale();

    /**
     * Ensure we can get GC's without losing data. We should know where. Return a location
     * URI it can be restored from. Should be capable of taking null for a location,
     * creating its own storage in a default area.
     * 
     * @param location
     * @return persisted file
     * 
     * @throws KlabException
     */
    File persist(String location) throws KlabException;

    /**
     * Get a storage object to do I/O for the observable. If full time history is desired,
     * pass true for the second argument, otherwise the variable won't be temporal. If the
     * scale has no time, the second parameter is irrelevant.
     * 
     * @param observable
     * @param dynamic
     * @param probabilistic
     * @return storage for observable
     */
    IStorage<?> getStorage(IObservableSemantics observable, boolean dynamic, boolean probabilistic);

}
