/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.lang.IMetadataHolder;

/**
 * A direct observation - subject, event or process.
 * 
 * @author Ferd
 *
 */
public interface IDirectObservation extends IObservation, IMetadataHolder {

    /**
     * Direct observations are stated in a namespace. This is necessary
     * so they can be resolved properly (using possibly customized 
     * resolution criteria). Normally this will be the namespace of
     * the observer that stated them, or the namespace of the parent
     * subject when generated outside.
     * 
     * @return the resolution namespace
     */
    INamespace getNamespace();

    /**
     * All direct observations have a name. The name is a "personal" ID and 
     * will be shown to users. It does not include the namespace. E.g. a lake's
     * name is its geographical name. The name is identifying the subject - so
     * no homonimy is supported for now, as subjects are created in maps indexed
     * by name.
     * 
     * @return the fully qualified name of the observation.
     */
    String getName();

    /**
     * Return any states linked to this observation at its current location in space/time.
     * The subject should also store the properties that link the states to it.
     *
     * @return any states observed in this context.
     */
    Collection<IState> getStates();

    /**
     * Return any roles for the passed observation within this. Roles are 
     * stated in models for specific dependencies or inferred by reasoning on implied roles adopted by the
     * main observable. If an observation is passed, any roles attributed by the
     * user during a scenario session are also checked.
     * 
     * @param observable
     * @return
     */
    Collection<IConcept> getRolesFor(ISemantic observation);
    

    /**
     * Selection functions for children.
     * 
     * @param args
     * @return
     */
    Collection<IObservation> select(Object... args);

    /**
     * Selection function for siblings.
     * 
     * @param args
     * @return
     */
    Collection<IObservation> siblings(Object... args);

    /**
     * Selection function for children at all levels.
     * 
     * @param args
     * @return
     */
    Collection<IObservation> selectAll(Object... args);

}
