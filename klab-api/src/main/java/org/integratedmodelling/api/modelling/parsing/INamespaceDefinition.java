/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.api.modelling.parsing;
//
// import java.io.File;
//
// import org.integratedmodelling.api.knowledge.IAxiom;
// import org.integratedmodelling.api.knowledge.IConcept;
// import org.integratedmodelling.api.knowledge.IOntology;
// import org.integratedmodelling.api.lang.IModelResolver;
// import org.integratedmodelling.api.metadata.IMetadata;
// import org.integratedmodelling.api.modelling.IFunctionCall;
// import org.integratedmodelling.api.modelling.ILanguageObject;
// import org.integratedmodelling.api.modelling.INamespace;
// import org.integratedmodelling.api.project.IProject;
//
// public interface INamespaceDefinition extends ILanguageObject, INamespace {
//
// /**
// * Set the namespace ID. Only for resolvers and such.
// *
// * @param id
// */
// void setId(String id);
//
// /**
// * Add an axiom for the namespace's tbox.
// *
// * @param axiom
// */
// void addAxiom(IAxiom axiom);
//
// /**
// * Set the resource name that this namespace was created from.
// *
// * @param resourceUrl
// */
// void setResourceUrl(String resourceUrl);
//
// /**
// * Timestamp of last modification - tied to the resource or creation date if
// * ephemeral.
// *
// * @param timestamp
// */
// void setTimeStamp(long timestamp);
//
// /**
// * Notification of imported namespace.
// *
// * @param namespace
// */
// void addImportedNamespace(String namespace);
//
// /**
// * Notification of model object read within this namespace.
// *
// * @param modelObject
// */
// void addModelObject(IModelObjectDefinition modelObject);
//
// /**
// * Set project we belong to.
// *
// * @param project
// */
// void setProject(IProject project);
//
// /**
// * Set from namespace header specs if a language was defined. The language used for
// * expressions is a namespace-scoped definition, i.e., no mixing of expression
// * languages within the same namespace.
// *
// *
// * @param language
// */
// void setExpressionLanguage(String language);
//
// /**
// * Add functions returning an extent outside of which the model should not be applied. If this
// * is done more than once for an extent of the same type, the extents should be
// * merged. Exceptions may result from the merging.
// *
// * @param o
// */
// void addCoveredExtent(IFunctionCall o, IModelResolver resolver, int lineNumber);
//
// /**
// * Add a namespace ID that will be used to resolve dependencies. Default when none
// * is added is to use all visible namespaces.
// *
// * @param tns
// */
// void addLookupNamespace(String tns);
//
// /**
// * Define the resolution criteria for the namespace.
// *
// * @param metadata
// */
// void setResolutionCriteria(IMetadata metadata);
//
// /**
// * Add a namespace ID that will be used to find evidence for training. Default is
// * to use all visible namespaces, which is bad. TODO may want to make it mandatory
// * for training to happen.
// *
// * @param tns
// */
// public void addTrainingNamespace(String tns);
//
// /**
// * Set the ontology manually. Done when the ontology is created externally to the
// * namespace.
// *
// * @param iOntology
// */
// void setOntology(IOntology iOntology);
//
// /**
// * Set whether this namespace is a scenario or not.
// *
// * @param isScenario
// */
// void setScenario(boolean isScenario);
//
// /**
// * Add the named namespace to the list of those that should not be
// * used for resolution when this is active.
// *
// * @param dn
// */
// void addDisjointNamespace(String dn);
//
// /**
// * flush all the axioms accumulated so far and update the ontology.
// */
// void synchronizeKnowledge(IModelResolver resolver);
//
// boolean isInternal();
//
// void setInternal(boolean b);
//
// void setLocalFile(File file);
//
// /*
// * a private namespace is one where each model in it is private.
// */
// void setPrivate(boolean isPrivate);
//
// /*
// * an inactive namespace is parsed but not used.
// */
// void setInactive(boolean isInactive);
//
// /**
// * Namespace may have metadata.
// *
// * @param processMetadata
// */
// void setMetadata(IMetadataDefinition processMetadata);
//
// void setDescription(String docstring);
//
// /*
// * FIXME remove when error management is 100% finalized and harmonized in client and engine. Only used
// * in the parser to avoid having to create an Error object using the resolver.
// */
// @Deprecated
// void addError(Throwable e, int lineNumber);
//
// void setDomain(IConcept concept);
//
// }
