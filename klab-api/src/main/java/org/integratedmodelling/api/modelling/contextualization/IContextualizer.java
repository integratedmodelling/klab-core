/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.contextualization;

import java.util.Map;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Contextualizers are the workhorses of model execution; each of the fundamental
 * observable types (state, process, event, subject) have a specialized contextualizer
 * that can be implemented and provided through a function by prefixing it with a
 * {@link Prototype} annotation. The contextualizers on remote engine can be provided as
 * services to remote clients if the prototype is tagged public and is part of a
 * IComponent.
 * 
 * @author ferdinando.villa
 *
 */
public abstract interface IContextualizer {

    /**
     * If a contextualizer service is enabled to run on a remote engine, make it a
     * RemoteContextualizer and define the method to return whether the personal engine
     * should attempt to run on a remote machine. If this returns true and the network
     * does not have a node that will run this for the current user, canRunLocally() will
     * be called and if that returns true, the service will be run locally.
     * 
     * @author Ferd
     *
     */
    public static interface Remote {

        /**
         * 
         * @param user
         * @return true if remote accessor should be used by this user
         */
        boolean useRemote(IUser user);

        /**
         * Only called if useRemote returned true but no network node provides the
         * service.
         * 
         * @return if we have the option of running locally
         */
        boolean canRunLocally();
    }

    /**
     * Return true when this contextualizer has finished its cycle and can be disposed of.
     * This usually means that the last transaction has been processed. Used only in
     * remote services, which are left in the user session unless this returns true.
     * 
     * @return true if we can dispose of the contextualizer now.
     */
    boolean canDispose();

    /**
     * Called just after object construction to pass any parameters from the function call
     * that instantiated this and the project that the function was in.
     * 
     * @param parameters
     * @param model
     *            the model that is calling this contextualizer, if any. May be null.
     * @param project
     * @throws KlabValidationException
     */
    void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance)
            throws KlabValidationException;
}
