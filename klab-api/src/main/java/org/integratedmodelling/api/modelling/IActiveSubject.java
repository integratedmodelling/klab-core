/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.exceptions.KlabException;

/**
 * The in-memory version of a ISubject. We can count on any ISubject being an IActiveSubject while
 * it's being contextualized. 
 * 
 * @author Ferd
 *
 */
public interface IActiveSubject extends ISubject, IActiveDirectObservation {

    /**
     * Call after initialize() to complete covering the context, i.e. run any time transitions. 
     * After this has run, the subject is immutable.
     * @throws KlabException 
     */
    void contextualize() throws KlabException;

    /**
     * Adds a computed state, determining the property to link with using the semantics. Throw an exception if
     * adding the state would result in semantic inconsistency.
     * 
     * @param s
     * @throws KlabException
     */
    void addState(IState s) throws KlabException;

    /**
     * Subjects during contextualization may have a backing dataset for access to history. This may
     * return null.
     * 
     * @return a dataset that backs the states in this subject, if any.
     */
    IDataset getBackingDataset();

    /**
     * Create a new event inherently to this subject, with the passed scale. The event will be resolved after
     * creation.
     * 
     * @param observable
     * @param scale
     * @param name 
     * @return a new event matching the inputs
     * @throws KlabException
     */
    IEvent newEvent(IObservableSemantics observable, IScale scale, String name)
            throws KlabException;

    /**
     * Create a new subject in this subject's structure, linking it to this subject with a relationship that
     * observes the passed property. Both the subject and the relationship will be resolved after 
     * creation. 
     * 
     * If the passed property is null, link it with the standard "inherentTo" relationship (note that this 
     * does not imply containment). Use of an appropriate relationship should be encouraged.
     * 
     * @param observable
     * @param scale
     * @param name 
     * @param relationship 
     * @return a new subject matching the inputs
     * @throws KlabException
     */
    ISubject newSubject(IObservableSemantics observable, IScale scale, String name, IProperty relationship)
            throws KlabException;

    /**
     * Create a new process inherently to this subject, with the passed scale. The process will be resolved after
     * creation.
     * 
     * @param observable
     * @param scale
     * @param name 
     * @return a new process matching the inputs.
     * @throws KlabException
     */
    IProcess newProcess(IObservableSemantics observable, IScale scale, String name)
            throws KlabException;

    /**
     * Connect two subject using a new relationship of the passed type and scale and return the relationship. Return can be null
     * or inactive. Scale may be null (will use the subject's if so).
     * 
     * @param source
     * @param destination
     * @param relationshipType
     * @param relationshipScale 
     * @return
     * @throws KlabException
     */
    IRelationship connect(IActiveSubject source, IActiveSubject destination, IConcept relationshipType, IScale relationshipScale)
            throws KlabException;


}
