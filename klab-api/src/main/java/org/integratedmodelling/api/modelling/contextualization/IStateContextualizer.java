/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.contextualization;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A contextualizer that computes a quality, operating on a single value at a
 * time. Essentially a multi-valued function. It may create additional states as
 * side effect, which it will have to take care of.
 * 
 * 
 * @author ferdinando.villa
 *
 */
public interface IStateContextualizer extends IContextualizer {

	/**
	 * Called once at the beginning. The state for the observable will be
	 * created unless the output map contains one for the passed name. Any
	 * "side effect" states may also be returned. Other observations will be
	 * also added (subjects, events) but it's not expected that quality models
	 * produce direct observations.
	 * 
	 * @param name
	 * @param observer
	 * @param contextSubject
	 * @param context
	 * @param expectedInputs
	 * @param expectedOutputs
	 * @param isLastInChain if true, this is the contextualizer that provides data directly
	 * 		   				to the state. If false, data checks should not be activated as
	 * 						the returned data will be further processed.
	 * @param monitor
	 * @return new or changed observations
	 * @throws KlabException
	 */
	Map<String, IObservation> define(String name, IObserver observer, IActiveDirectObservation contextObservation,
			IResolutionScope context, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs,
			boolean isLastInChain, IMonitor monitor) throws KlabException;

	/**
	 * Called at the beginning with the process this contextualizer will be
	 * handling. As this is expected to produce a single quality, a state for
	 * the output will be created externally. The output for the passed name
	 * should be the single values at the passed index.
	 * 
	 * The name passed is the ID of the primary output, corresponding to the
	 * observable. All other expected outputs are in addition to that.
	 * 
	 * @param index
	 * @param inputs
	 * @return initial values at index
	 * 
	 * @throws KlabException
	 */
	Map<String, Object> initialize(int index, Map<String, Object> inputs) throws KlabException;

	/**
	 * Called at each computation with the input map to produce the output map.
	 * Inputs and outputs will honor the semantics passed at initialization. The
	 * transition will be null if we're obtaining initial values. The output map
	 * must contain the value for the primary state name passed at
	 * initialization.
	 * 
	 * @param index
	 * 
	 * @param transition
	 * @param inputs
	 * @return new or changed values
	 * @throws KlabException
	 */
	Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs) throws KlabException;

	/**
	 * Return true if the state produced is a probability distribution.
	 * 
	 * @return
	 */
	boolean isProbabilistic();
	
    /**
     * If true, this contextualizer will produce a constant value over the context.
     * @return
     */
    boolean isConstant();

}
