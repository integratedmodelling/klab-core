/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.contextualization;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveRelationship;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * @author Ferd
 *
 */
public interface IRelationshipContextualizer extends IContextualizer {

    /**
     * Called at the beginning with the process this contextualizer will be handling. The
     * process contains its scale and context subject. The initializer has the option of
     * modifying the process or changing it altogether and returning a new one, which may
     * have a different scale as long as it's contained within the original one.
     * 
     * The expected input and outputs corresponds to states that will be passed to
     * compute() or are expected back from it.
     * 
     * @param relationship
     * @param contextSubject
     * @param context
     * @param expectedInputs
     * @param expectedOutputs
     * @param monitor
     * 
     * @param process
     * @return new or changed observations
     * @throws KlabException
     */
    Map<String, IObservation> initialize(IActiveRelationship relationship, IActiveSubject sourceSubject, IActiveSubject targetSubject, IActiveSubject contextSubject, IResolutionScope context, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException;

    /**
     * React to the passed transition by computing the necessary states for the subject
     * passed at initialization. Transition will be null if we're initializing the
     * subject.
     * 
     * @param transition
     * @param inputs
     * @return new or changed observations
     * @throws KlabException
     */
    Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException;
}
