package org.integratedmodelling.api.modelling.visualization;

import java.util.List;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.collections.Pair;

/**
 * Abstract model for a client context viewer.
 * 
 * @author ferdinando.villa
 *
 */
public interface IViewer {

    public static enum DrawMode {
        PAN,
        POINT,
        LINE,
        POLYGON
    }
    
    /**
     * Supported types of view. We just keep the state of
     * what is currently shown to the user and in which
     * view.
     * 
     * @author ferdinando.villa
     *
     */
    public enum Display {
        /**
         * Geographical map
         */
        MAP,
        /**
         * Timeseries plot of temporal states or point display from maps
         */
        TIMEPLOT,
        /**
         * Timeline of events, possibly with superimposed timeplots
         */
        TIMELINE,
        /**
         * Sankey flow diagram for flow networks
         */
        FLOWS,
        /**
         * Number display
         */
        DATAGRID,
        /**
         * Provenance graph for context
         */
        PROVENANCE,
        /**
         * Subject structure with other subjects and their relationships
         */
        STRUCTURE
    }

    /**
     * Set current view to a new view of the passed context if not already the
     * same, and redisplay.
     * 
     * @param context
     */
    void show(IContext context);

    /**
     * Expose all pertinent displays to observation, redisplaying if any of
     * the currently active ones is affected.
     * 
     * @param observation must be in context
     */
    void show(IObservation observation);
    
    /**
     * Show in a specified display.
     * 
     * @param observation
     * @param display
     */
    void show(IObservation observation, Display display);

    /**
     * Remove observation from all displays it's in, redisplaying if any of
     * the currently active ones is affected.
     * 
     * @param observation must be in context
     */
    void hide(IObservation observation);

    /**
     * Make the observation passed the focus of the display.
     * 
     * @param observation must be in context
     */
    void focus(IDirectObservation observation);
    /**
     * Get the current display type.
     * @return current display
     */
    Display getCurrentDisplay();

    /**
     * Get the view corresponding to the passed display, or null
     * if that display is not there.
     * 
     * @param display
     * @return the view for this display, or null
     */
    IView getView(Display display);

    /**
     * Get the overall view, containing the union of all the observations
     * displayed in all the available displays.
     * 
     * @return the overall view, never null.
     */
    IView getView();

    /**
     * Get a display label for the passed observation.
     * 
     * @param observation
     * @return a label suitable for UI display.
     */
    String getLabel(IObservation observation);

    /**
     * Choose a specific display and show it. If not feasible for the
     * context, do nothing. Return the current display after the call.
     * 
     * @param display
     * @return the current display after the call
     */
    Display setDisplay(Display display);

    /**
     * If the current display has alternative views, cycle to the
     * next available, wrapping after the last.
     */
    void cycleView();

    /**
     * Reset the current view to "normal" overall coverage and look after
     * user navigation.
     */
    void resetView();

    /**
     * If we're looking at a spatial context and we have a stack of maps, return
     * the one that's in front of the viewer. Used in GUIs to define the target 
     * of an export or info-tool operation.
     * 
     * @return the topmost spatial state, or null
     */
    IState getVisibleSpatialState();

    /**
     * Get all visible spatial states (maps), bottom layer first.
     * 
     * @return all visible spatial states. Empy list if the current display is not spatial.
     */
    List<IState> getVisibleSpatialStates();

    /**
     * Viewers are session-specific.
     * @return the session we're linked to.
     */
    ISession getSession();

    /**
     * Set the opacity of observation view. 
     * 
     * @param observation
     * @param opacity from 0 (invisible) to 1 (fully opaque).
     */
    void setOpacity(IObservation observation, double opacity);

    /**
     * If the viewer is capable of creating temporal or spatial contexts for
     * new or existing observations, activate/deactivate edit mode. Either 
     * edit the topmost observation if possible and present, or a new one.
     * If the viewer is incapable of editing, return false at the request to
     * activate.
     * 
     * @param on request active or inactive editing state
     * @return state of edit mode after request
     */
    boolean setContextEditMode(boolean on);

    /**
     * Same as the other, but drop the UI directly into a map drawing mode of the
     * specified type.
     * 
     * @param mode
     * @return
     */
    void setContextEditMode(DrawMode mode);
    
    /**
     * Clear everything, remove context, and reset to default empty view.
     */
    void clear();

    /**
     * Remove all visible states from view.
     */
    void clearAllStates();

    /**
     * Locate all managed views into a given sub-context (usually a temporal
     * location, but the managed locators will depend on implementation).
     *  
     * @param locators zero or more locators. No locators = show full context.
     */
    void locate(Locator... locators);

    /**
     * Distributed states have a colormap and an histogram, according to locations and
     * type.
     * 
     * @param state
     * @return a histogram, or null if not applicable.
     */
    Pair<IColormap, IHistogram> getStateSummary(IState state);



}
