/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Set;

import org.integratedmodelling.api.knowledge.IObservation;

/**
 * The IStructure is the state of a IDirectObservation, and is a graph where the nodes are IDirectObservations
 * and the edges are IRelationships. Each subject starts with an empty structure where there is one node (the subject
 * itself) and no relationships. At any time, a contextualizer can modify the structure.
 * 
 * @author Ferd
 *
 */
public interface IStructure {

    /**
     * Get the subject that this is the structure of. The subject is also included in the 
     * result of getObservations(). 
     * 
     * @return the subject this refers to
     */
    ISubject getSubject();

    /**
     * All the observations linked in this structure. They're all subjects.
     * 
     * @return the observations linked
     */
    Set<IObservation> getObservations();

    /**
     * All the links, i.e. observations of a relationship.
     * 
     * @return all relationships
     */
    Set<IRelationship> getRelationships();

    /**
     * Get the outgoing relationships from a subject
     * @param observation
     * @return outgoing relationships
     */
    Set<IRelationship> getOutgoingRelationships(IObservation observation);

    /**
     * Get the relationships going into a subject
     * @param observation
     * @return the incoming relationships
     */
    Set<IRelationship> getIncomingRelationships(IObservation observation);

    /**
    * Like the other link(), use when control over the relationship used is needed.
    *
    * @param from
    * @param to
    * @param relationship
    */
    void link(ISubject from, ISubject to, IRelationship relationship);

    /**
    * Remove the observation and every relationship it's involved into.
    *
    * @param observation
    */
    void removeObservation(IObservation observation);

}
