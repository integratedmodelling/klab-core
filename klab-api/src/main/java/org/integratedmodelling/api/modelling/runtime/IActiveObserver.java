/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * A IObserver that can produce the necessary IActuators so that states may be
 * computed and mediations done. These are the most important functions for
 * correct operation of dataflows.
 * 
 * @author Ferd
 *
 */
public interface IActiveObserver extends IObserver {

	/**
	 * Return the actuator that will mediate to this observer from the passed
	 * one. If the mediation is unnecessary return null. Only throw an exception
	 * if the observer passed is incompatible.
	 * 
	 * @param observer
	 * @param monitor
	 *            report errors and warnings here
	 * @return a mediator if needed to reinterpret the passed observer, or null
	 * @throws KlabException
	 */
	IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException;

	/**
	 * A data processor is a contextualizer that will be used to process raw data from
	 * datasources. It can be used for enforcing data sanity or to preprocess data that
	 * won't come ready (e.g. to turn categories into concepts). If no processing is
	 * wanted, return null.
	 * 
	 * This will also be called on observers that declare to not need further resolution
	 * by returning false at the {@link #needsResolution()} call. If a processor is
	 * returned, it will produce the data before any other contextualizer is invoked.
	 * 
	 * @param monitor
	 * @return the interpreting accessor
	 * @throws KlabException 
	 */
	IStateContextualizer getDataProcessor(IMonitor monitor) throws KlabException;

}
