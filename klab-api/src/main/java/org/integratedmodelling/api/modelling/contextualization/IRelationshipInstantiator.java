/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.contextualization;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.exceptions.KlabException;

/**
 * The instantiator for relationships only creates one relationship at a time (or none), between
 * two explicit subjects. It's not called by user action but only by models within the subjects
 * that decide the network structure.
 * 
 * @author ferdinando.villa
 *
 */
public interface IRelationshipInstantiator extends IDirectInstantiator {

    /**
     * Called at transitions AND at initialization (with transition ==
     * {@link ITransition#INITIALIZATION}). The new relationship will be resolved and
     * contextualized. The relationship contextualizer may decide that the relationship
     * should be split into more relationships. When the relationship does not exist
     * this method will legitimately return null.
     * 
     * @param transition
     * @param inputs
     * @return new or changed observations
     * @throws KlabException
     */
    Map<String, IObservation> createRelationships(IActiveDirectObservation context, ITransition transition, Map<String, IState> inputs)
            throws KlabException;
}
