package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.runtime.IContext;

/**
 * The event bus handling event communication within a {@link IContext}.
 * @author ferdinando.villa
 *
 */
public interface IEventBus {

    /**
     * Register a listener observation.
     * 
     * @param observation
     * @param concept
     * @param actuator
     */
    void subscribe(IObservation observation, IActuator actuator);

    /**
     * Dispatch any event after resolution. Will call the event handler of any listener
     * at the appropriate transition.
     * 
     * @param event
     */
    void dispatch(IActiveEvent event);

}
