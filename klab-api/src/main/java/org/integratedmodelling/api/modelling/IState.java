/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.Pair;

/**
 * An IState is the result of observing a quality. IStates only exist in the context of an
 * ISubject and are the target of relationships defined by data properties. As such an
 * IState classifies as a literal semantic object.
 * 
 * Because ISubjects are endurant physical objects that may exist in time/space or other
 * abstract regions (an {@link IExtent} in Thinklab) whose adoption implies a distribution
 * of observed states, the IState is a complex literal that has as many states as the
 * cartesian product of all the IExtents owned by the ISubject that contains it. The
 * demote() operation will therefore return collections or atomic objects according to the
 * extents adopted. Specialized access methods return the event index of the ISchedule
 * that represents the ISubject's view of the extents.
 * 
 * @author Ferdinando
 */
public interface IState extends IMetadataHolder, IObservation {

    /**
     * Listener that we can install with {@link #addChangeListener} to be notified of
     * changes.
     * 
     * @author ferd
     *
     */
    interface ChangeListener {

        /**
         * Called every time a new value is set.
         * 
         * @param offset
         * @param value
         */
        void changed(int offset, Object value);

        /**
         * Called once before a new transition is executed or at the end of
         * initialization.
         * 
         * @param transaction
         */
        void transitionDone(ITransition transaction);
    }

    /**
     * Mediators are created by extents and are used to implement views of a state that
     * mediate values to another scale.
     * 
     * A mediator should be aware that the extents it mediates may have changed (it can
     * use States.hasChanged() to inspect that) and be able to readjust if necessary. This
     * will properly handle moving agents.
     * 
     * FIXME all mediators should be change listeners for the mediated state, and
     * rearrange the mediation strategy at each change as needed - we should subscribe
     * them automatically.
     * 
     * @author ferdinando.villa
     *
     */
    interface Mediator {

        /**
         * Aggregation mode. The default is AVERAGE for intensive properties and
         * non-physical properties or SUM for extensive properties, but data reduction
         * traits in the target may modify it (e.g. we may want the MAX if we tag the
         * final observer with im:Maximum). MAJORITY will be the default for qualitative
         * and semi-qualitative observers; at some point we may want to add fuzzy
         * membership and other more sophisticated strategies for probabilistic observers.
         * 
         * @author ferdinando.villa
         *
         */
        enum Aggregation {
            NONE,
            SUM,
            AVERAGE,
            MIN,
            MAX,
            MAJORITY,
            MAXIMUM_LIKELIHOOD
        }

        /**
         * These keys MAY be available after each mediation in the state's metadata. Their
         * meaning may differ according to the observer.
         * 
         * @author ferdinando.villa
         *
         */
        public final static String SPACE_MIN_VALUE          = "Mediator.SPACE_MIN_VALUE";
        public final static String SPACE_MAX_VALUE          = "Mediator.SPACE_MAX_VALUE";
        public final static String SPACE_VALUE_SUM          = "Mediator.SPACE_VALUE_SUM";
        public final static String SPACE_VALUE_DISTRIBUTION = "Mediator.SPACE_VALUE_DISTRIBUTION";
        public final static String SPACE_TOTAL_VALUES       = "Mediator.SPACE_TOTAL_VALUES";
        public final static String SPACE_CONFIDENCE         = "Mediator.SPACE_CONFIDENCE";
        public final static String SPACE_ERROR              = "Mediator.SPACE_ERROR";
        public final static String TIME_MIN_VALUE           = "Mediator.TIME_MIN_VALUE";
        public final static String TIME_MAX_VALUE           = "Mediator.TIME_MAX_VALUE";
        public final static String TIME_VALUE_SUM           = "Mediator.TIME_VALUE_SUM";
        public final static String TIME_VALUE_DISTRIBUTION  = "Mediator.TIME_VALUE_DISTRIBUTION";
        public final static String TIME_TOTAL_VALUES        = "Mediator.TIME_TOTAL_VALUES";
        public final static String TIME_CONFIDENCE          = "Mediator.TIME_CONFIDENCE";
        public final static String TIME_ERROR               = "Mediator.TIME_ERROR";

        /**
         * The kind of aggregation that the mediation implies.
         * 
         * @return aggregation type
         */
        Aggregation getAggregation();

        /**
         * Apply the locators to the original state, adding whatever other locators the
         * mediation strategy implies. Return the aggregated value implied by the
         * strategy.
         * 
         * @param originalState
         * @param otherLocators
         *
         * @return a mediated object
         */
        Object mediateFrom(IState originalState, IScale.Locator... otherLocators);

        /**
         * Apply the passed value to our scale and return the result.
         * 
         * @param value
         * @param index
         * @return a mediated object
         */
        Object mediateTo(Object value, int index);

        /**
         * Get all the locators that will map the original state's scale to the passed
         * index in the mediated scale. Weights should be assigned according to coverage
         * and aggregation strategy.
         * 
         * @param index
         * @return the locators needed to mediate
         */
        List<IScale.Locator> getLocators(int index);

        /**
         * Reduce the passed collection of pairs (value, weight) to one value according to
         * aggregation strategy.
         * 
         * @param toReduce
         * @param metadata
         *            a map to fill with any relevant statistics related to the
         *            aggregation (errors, uncertainty, boundaries, distributions, truth
         *            values etc) using the keys above.
         * 
         * @return the reduced value
         */
        Object reduce(Collection<Pair<Object, Double>> toReduce, IMetadata metadata);
    }

    /**
     * Return the total number of values determined by the extents owned by the owning
     * ISubject.
     * 
     * @return the value count. Should be 1 or more.
     */
    long getValueCount();

    /**
     * States are created by observers and will store them to provide a link to the
     * observation semantics.
     * 
     * @return the observer. Never null.
     */
    IObserver getObserver();

    /**
     * True if the owning ISubject has an observation of space with more than one state
     * value.
     * 
     * @return true if distributed in space
     */
    boolean isSpatiallyDistributed();

    /**
     * True if the owning ISubject has an observation of time with more than one state
     * value.
     * 
     * @return true if distributed in time.
     */
    boolean isTemporallyDistributed();

    /**
     * True if the owning ISubject has ANY implementation of time.
     * 
     * @return if time is known
     */
    boolean isTemporal();

    /**
     * True if the owning ISubject has ANY implementation of space.
     * 
     * @return if space is known
     */
    boolean isSpatial();

    /**
     * Return the spatial extent or null.
     * 
     * @return the observation of space
     */
    ISpatialExtent getSpace();

    /**
     * Return the temporal extent or null.
     * 
     * @return the observation of time
     */
    ITemporalExtent getTime();

    /**
     * Get a value at the passed offset from the scale.
     * 
     * @param index
     * @return the value at the passed offset
     */
    Object getValue(int index);

    /**
     * Get an iterator for the states over the dimension specified in the passed index.
     * Index is retrieved from the scale using the {@link IScale#getIndex} function.
     * 
     * @param index
     * @return an iterator of values following the passed index
     */
    Iterator<?> iterator(Index index);

    /**
     * Get the storage object that backs the dataset.
     * 
     * @return the storage
     */
    IStorage<?> getStorage();

    /**
     * True if the state has the same value overall despite the scale.
     * 
     * @return true if constant
     */
    boolean isConstant();

    /**
     * True if the state is expected to change in time.
     * 
     * @return true if dynamic
     */
    boolean isDynamic();

    /**
     * Add a listener to notify when any value is changed.
     * 
     * @param listener
     */
    void addChangeListener(ChangeListener listener);

    /**
     * Return either the original state or a wrapper that will allow get/set of values 
     * in a specified observation semantics.
     *  
     * @param observer
     * @return
     */
    IState as(IObserver observer);
    
    
}
