/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IKnowledge;

/**
 * A valuing observer defines its state by applying subjective value criteria (using a currency that may be or 
 * not be economic) to the object of another observer.
 * 
 * @author Ferd
 *
 */
public interface IValuingObserver extends INumericObserver, IMediatingObserver, IValueMediatingObserver {

    /**
     * Get the currency of the valuation. If "abstract", it should just specify a ranking scale and
     * potentially a concept to describe the value.
     * 
     * @return the currency
     */
    ICurrency getCurrency();

    /**
     * @return the unit of distribution of the count, potentially null
     */
    IUnit getUnit();
    
    /**
     * The main (or the first in case of pairwise comparison) observable being
     * valued.
     * 
     * @return the main observable valued
     */
    IKnowledge getValuedObservable();

    /**
     * The comparison observable being valued, or null if the valuing is not pairwise.
     * 
     * @return the comparison observable, if any
     */
    IKnowledge getComparisonObservable();

    /**
     * If true, our value is the relative assessment of two alternatives, and getComparisonObservable 
     * returns non-null.
     * 
     * @return true if pairwise
     */
    boolean isPairwise();

}
