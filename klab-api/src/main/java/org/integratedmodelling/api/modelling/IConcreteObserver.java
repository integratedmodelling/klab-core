package org.integratedmodelling.api.modelling;

import java.util.Collection;

import org.integratedmodelling.exceptions.KlabException;

/**
 * A concrete observer is able to lookup one or more objects and use them
 * to derive a quality. In this case a concept has not been given, so the
 * observer must define a concrete concept on its own.
 * 
 * This interface should technically extend IObserver, but it's used as a 
 * tag interface to avoid (legal, but uglier) semantic collinearities.
 * 
 * @author Ferd
 *
 */
public interface IConcreteObserver {

    /**
     * Return the stated concrete objects to use in deriving
     * the quality we represent. The observers that create the
     * objects should be returned instead of the objects themselves; the
     * resolver will do the rest.
     * 
     * @return a set of observers we are referencing to compute our state.
     * @throws KlabException 
     */
    Collection<IDirectObserver> getConcreteObjects() throws KlabException;

}
