/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Collection;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;

/**
 * Actions may be connected to accessors to operate after their values have been computed. 
 * In state accessors, actions may change, integrate, reinterpret observable values, or compute
 * the value of the observable in computing observers. In
 * subject accessors, they may modify the state or context of the subject. Actions are 
 * triggered by transitions; a set of domain concepts identifies the specific transitions
 * that the action is sensitive to.
 * 
 * @author Ferd
 *
 */
public interface IAction {

    /**
     * @author Ferd
     *
     */
    enum Type {
        SET,
        CHANGE,
        INTEGRATE,
        DO,
        MOVE,
        DESTROY
    };
    
    /**
     * Trigger for action
     * @author Ferd
     *
     */
    enum Trigger {
        /**
         * Definition: before resolution, after calling any external contextualizers (using...). For
         * instantiators, 'self' is the context of the new instances.
         */
        DEFINITION,
        /**
         * State initialization is called after all the context has been initialized (with individual
         * on definition actions) in a state model. Self is the state itself.
         */
        STATE_INITIALIZATION,
        /**
         * Instantiation: before resolution of EACH new instance from an instantiator. Not accepted
         * within contextualizers. 'self' is the new instance, 'context' their context on which the
         * instantiator was called.
         */
        INSTANTIATION,
        /**
         * Instantiation: after resolution of EACH new instance from an instantiator. Not accepted
         * within contextualizers. 'self' is the new instance, 'context' their context on which the
         * instantiator was called.
         */
        RESOLUTION,
        /**
         * Termination: just after 'move away' or deactivate(). Cannot change the outcome of deactivation but
         * object can still "do" things within the action.
         */
        TERMINATION,
        /**
         * Triggered by events (types returned by getTriggeredEvents())
         */
        EVENT,
        /**
         * Triggered by temporal transitions.
         */
        TRANSITION
    }

    /**
     * The type of action.
     * 
     * @return action type
     */
    Type getType();

    /**
     * The trigger for the action.
     * 
     * @return
     */
    Trigger getTrigger();
    
    /**
     * Return the array of domain concepts that select the transitions upon which 
     * this action will be run. If the return value is empty, the action
     * is expected to run at initialization only. Concepts may be extents (usually
     * time) and/or events.
     * 
     * @return domain concepts for the action's scope. Never null, possibly empty.
     */
    Set<IConcept> getDomain();

    /**
     * If the action has a target state, return its formal name in the model. 
     * 
     * @return state targeted
     */
    String getTargetStateId();

    /**
     * Get the expression that will execute the action when evaluated in context.
     * 
     * @return main action expression
     */
    IExpression getAction();

    /**
     * Get the condition for the action to take place. May be null and may be an ICondition.
     * 
     * @return action condition, or null
     */
    IExpression getCondition();
    
    /**
     * The types of events that trigger the action, or an empty collection if trigger
     * is not Trigger.EVENT.
     * @return
     */
    Collection<IConcept> getTriggeringEvents();

}
