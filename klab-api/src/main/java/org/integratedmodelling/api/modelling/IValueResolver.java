package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;

/**
 * A IValueResolver tags a {@link IStateContextualizer} that is capable of 
 * <em>resolving</em> the observable, i.e. generate data autonomously without
 * any need for further resolution. 
 * 
 * If an observer uses one of these as its contextualizer, resolution of the 
 * observer stops there.
 * 
 * @author Ferd
 *
 */
public interface IValueResolver {

}
