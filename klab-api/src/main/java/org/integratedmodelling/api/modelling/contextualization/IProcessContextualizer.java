/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.contextualization;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * @author Ferd
 *
 */
public interface IProcessContextualizer extends IContextualizer {

    /**
     * Called at the beginning with the process this contextualizer will be 
     * handling. The process contains its scale and context subject. Return
     * the observations made.
     * 
     * @param process
     * @param contextSubject 
     * @param resolutionContext 
     * @param expectedInputs 
     * @param expectedOutputs 
     * @param monitor 
     * @return all observations made during initialization
     * @throws KlabException 
     */
    Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation contextSubject, IResolutionScope resolutionContext, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException;

    /**
     * Compute any observations that change in this transition and return it. Only observations
     * that have changed should be returned. The input map contains any states that have changed
     * compared to the previous transition. Anything else can be obtained from the context based
     * on the info passed to initialize().
     * 
     * @param transition
     * @param inputs
     * @return All observation that have changed or were created during this transition.
     * @throws KlabException 
     */
    Map<String, IObservation> compute(ITransition transition, Map<String, IState> inputs)
            throws KlabException;
}
