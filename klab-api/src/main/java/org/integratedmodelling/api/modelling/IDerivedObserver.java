/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.List;

import org.integratedmodelling.api.modelling.contextualization.IContextualizer;

/**
 * Interface for an observer that can find indirect ways to resolve its
 * observable if the direct way is not resolved. E.g., presence or distance that
 * can observe the objects and compute the quality, or a ratio observer that can
 * compute the ratio from the implied constraint.
 * 
 * @author ferdinando.villa
 *
 */
public interface IDerivedObserver extends IObserver {

	/**
	 * Called only when the original concept cannot be resolved. Should return
	 * any other observable that makes it possible to produce the observation,
	 * for example the cities in an observation of distance from city.
	 * 
	 * @return other observables that provide an alternative way of observing
	 *         the observable.
	 */
	List<IObservableSemantics> getAlternativeObservables();

	/**
	 * If the resolution of one of the observables returned by
	 * {@link #getAlternativeObservables()} has succeeded (i.e. returned a
	 * coverage whose {@link ICoverage#isRelevant()} returns true), this will
	 * receive the correspondent contextualizer and the corresponding coverage.
	 * It can then tell the resolver to stop or continue.
	 * 
	 * It is then the observer's job to use it to produce the necessary
	 * observations. Note that the own observer's contextualizer will only be
	 * called if the overall coverage is sufficient (i.e. 
	 * {@link ICoverage#isComplete()} returns true).
	 * 
	 * @param contextualizer
	 * @param coverage
	 * @return coverage if this contextualizer is enough to continue, empty if we want the resolver
	 * 			    to try more of the observables returned by {@link #getAlternativeObservables()}
	 * 				if any, and/or try to find a model that provides another contextualizer if
	 * 			    no more observables are there. 
	 */
	ICoverage acceptAlternativeContextualizer(IContextualizer contextualizer, ICoverage coverage);
}
