/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Collection;

import org.integratedmodelling.api.modelling.IScale.Locator;

/**
 * ISubject is the equivalent of an individual in the Thinklab modeling platform. It
 * represents the observation of an "object", i.e. the individual that results from the
 * observation of an endurant object capable of existing autonomously.
 *
 * At the most general level, the ISubject is the "agent" of most modeling frameworks, and
 * may have behavior specified as actions to be linked to any context transition. The
 * allowed behaviors depend on its semantics and are translated in the API using the enums
 * in this class (see <a
 * href="https://integratedmodelling.org/confluence/display/LSD/Direct+observables"/>).
 *
 * A ISubject is a semantic object whose literal properties point to {@link IState}
 * (observations of qualities that require a ISubject to exist) and whose object
 * properties point to other ISubjects. Being the result of observation it will normally
 * be located in time/space extents (themselves IStates), and methods are provided to
 * facilitate introspection of extents, other states, and other subjects that belong to
 * the subject.
 *
 * Initial states of ISubjects are created by an {@link IDirectObserver} and made
 * conformant with the knowledge base by the act of observation. Other subjects and
 * qualities can be observed in the context of an ISubject, producing valid new
 * observations. Because Thinklab commits to OWL's open world assumption, it is possible
 * to observe things using a ISubject as the context; the operation creates a new ISubject
 * with different semantics.
 *
 * An ISubject is the result of observation, therefore it's immutable and valid during its
 * full lifetime. It is not possible to create a partially specified ISubject - the
 * ISubjectObserver is provided as a proxy to control the observation process in a
 * fine-grained way without exposing the partially specified ISubject.
 *
 * @author Ferd
 *
 */
public interface ISubject extends IDirectObservation {

    /**
     * Type of agent, determining the allowed behaviors.
     * 
     * @author fvilla
     *
     */
    public static enum AgentType {
        REACTIVE,
        INTERACTIVE,
        DELIBERATIVE
    }

    /**
     * Type of relationship with the context, influencing allowed behaviors.
     * 
     * @author fvilla
     *
     */
    public static enum ContextRelationship {
        REQUIREMENT,
        CONTAINMENT,
        AFFECTING
    }

    /**
     * Get the state of the subject, i.e. the graph of its relationships with every direct
     * observation made in its context, in the specified scale context (normally a time
     * locator or null for initial).
     * @param locators
     * 
     * @return the subject's structure. Locators may be passed to specify the structure in
     * a particular extent(s).
     */
    IStructure getStructure(Locator... locators);

    /**
     * Return any subjects linked to this subject. These should be stored in an explicit
     * object structure, a graph of which this function only returns the nodes.
     *
     * @return all the subject observed in this' context.
     */
    Collection<ISubject> getSubjects();

    /**
     * Get all the processes that were resolved for this subject.
     * @return all processes
     */
    Collection<IProcess> getProcesses();

    /**
     * Get all the events that were resolved in this subject at the time of calling.
     * 
     * @return all events
     */
    Collection<IEvent> getEvents();

}
