/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.visualization;

import java.io.File;

import org.integratedmodelling.exceptions.KlabException;

/**
 * A visualization is specific of a state in a context and is produced from them by the
 * visualization factory. 
 * 
 * @author Ferd
 *
 */
public interface IStateVisualization {

    /*
     * values for flags
     */
    static public final int OPAQUE_BACKGROUND      = 0x0001;
    static public final int EARTH_IMAGE_BACKGROUND = 0x0002;
    static public final int USE_SMOOTHING          = 0x0004;

    /**
     * Return the most appropriate media type for the state we represent. Pass flags if 
     * any special type of visualization is desired.
     * 
     * @param directory
     * @param viewportWidth
     * @param viewportHeight
     * @param flags 
     * @return requested media
     * @throws KlabException
     */
    public IMedia getMedia(File directory, int viewportWidth, int viewportHeight, int flags)
            throws KlabException;

    /**
     * Return a media file of the passed type. Throw an exception if the type is inappropriate
     * for the context.
     * 
     * @param directory
     * @param mediaType pass one of the values in IMedia.
     * @param viewportWidth
     * @param viewportHeight
     * @param flags 
     * @return requested media
     * @throws KlabException
     */
    public IMedia getMedia(File directory, int mediaType, int viewportWidth, int viewportHeight, int flags)
            throws KlabException;

    /**
     * Return an index (0-255 max) corresponding to the object passed in the visualization created
     * for our state.
     * 
     * @param o
     * @return index for value
     */
    public byte getVisualizedValue(Object o);

    /**
     * Called only after getMedia has been called once. Should return the same media that getMedia returned, with
     * the new viewport. It should be used so that the largest media size is requested in getMedia and only smaller
     * ones get rescaled requests.
     * @param thumbDir 
     * @param mediaType 
     * @param viewportWidth 
     * @param viewportHeight 
     * @param flags 
     * @return rescaled media
     * 
     */
    public IMedia getRescaledMedia(File thumbDir, int mediaType, int viewportWidth, int viewportHeight, int flags);

}
