/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.api.modelling.parsing;
//
// import org.integratedmodelling.api.knowledge.IExpression;
// import org.integratedmodelling.api.modelling.IDataSource;
// import org.integratedmodelling.api.modelling.IFunctionCall;
// import org.integratedmodelling.api.modelling.IKnowledgeObject;
// import org.integratedmodelling.api.modelling.IModel;
//
// public interface IModelDefinition extends IObservingObjectDefinition, IModel {
//
// /**
// * Set the observer for the model. If {@link addConditional} is used, this one
// * should not be called.
// *
// * @param observer
// */
// public void setObserver(IObserverDefinition observer);
//
// /**
// * This one adds a model to work in a switcher statement. Expression may be null.
// * If this one is used, {@link setObserver} should not be used as the observer will be
// * reset to a conditional one with the models linked to it.
// *
// * @param observer
// * @param expression
// */
// public void addConditional(IModelDefinition observer, IExpression expression);
//
// /**
// * Set the datasource.
// *
// * @param datasource
// */
// public void setDataSource(IDataSource datasource);
//
// /**
// *
// * @param observationConcept
// * @param formalName
// */
// public void addObservable(String observationConceptName, String formalName);
//
// /**
// *
// * @param observationModel
// * @param formalName
// */
// public void addObservable(IModel observationModel, String formalName);
//
// /**
// *
// * @param function
// * @param formalName
// */
// public void addObservable(IFunctionCall function, String formalName);
//
// /**
// *
// * @param observable
// * @param formalName
// */
// public void addInlineObservable(Object observable, String formalName);
//
// /**
// * Called on models that need an ID but have not been given one and there is
// * no obvious way of figuring one out. The final ID will go through the resolver
// * to be disambiguated, so it may be different from the return value.
// * @return
// */
// public String createId();
//
// /**
// * When this one is called, the model must prepare itself for interpreting objects coming from
// * the object generator (function, concept or model) as annotated agents of the passed concept.
// * Translation parameters for 'grey' object attributes will be passed later using
// * {@link addAttributeTranslator}.
// *
// * @param concept
// * @param objectGenerator
// */
// public void setObjectReificationData(IKnowledgeObject concept, Object objectGenerator);
//
// /**
// * Add a translation rule for an attribute of the incoming concept. The passed property may
// * be null.
// *
// * @param attributeId
// * @param observer
// * @param property
// */
// public void addAttributeTranslator(String attributeId, IModel observer, IKnowledgeObject property);
//
// /**
// * Called when the model is meant to simply wrap this observer under the given concept.
// *
// * @param observableConcept
// */
// public void wrap(IObserverDefinition observer);
//
// /*
// * an inactive namespace is parsed but not used.
// */
// @Override
// void setInactive(boolean isInactive);
//
// /**
// * Called by parser to notify that this model is within a dependency.
// *
// * @param b
// */
// public void setDependencyModel(boolean b);
//
// /**
// * Called by parser when model is interpreting the observer and doesn't add a new concept.
// * @param b
// */
// void setInterpreter(boolean b);
//
// }
