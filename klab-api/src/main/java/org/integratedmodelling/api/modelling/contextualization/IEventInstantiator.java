/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.contextualization;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Because events are atomic and happen at transitions, the contextualizer for
 * events must create them directly. The context subject is passed at
 * initialization, so it should be consulted at event creation and any
 * consequences the events may have on the subject should be evaluated at
 * transitions.
 * 
 * @author ferdinando.villa
 *
 */
public abstract interface IEventInstantiator extends IDirectInstantiator {

	/**
	 * Called at transitions AND at initialization (with transition ==
	 * {@link ITransition#INITIALIZATION}). Any new subject will be resolved and
	 * contextualized as the simulation progresses.
	 * 
	 * @param transition
	 * @param inputs
	 * @return new or changed observations
	 * @throws KlabException
	 */
	Map<String, IObservation> createEvents(ITransition transition, Map<String, IState> inputs) throws KlabException;

}
