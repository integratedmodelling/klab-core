/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import java.util.List;

import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IActiveEvent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;

/**
 * An actuator actuates contextualization, i.e., drives zero or more
 * {@link IContextualizer} that compute the state of any observable they're linked to and
 * holds any k.IM actions specified by the user to react to events and time transitions.
 * 
 * @author Ferdinando
 */
public abstract interface IActuator {

    /**
     * All actuators must have a name. Their name may be translated to a formal name in a
     * dependency or mediation link.
     * 
     * @return actuator name
     */
    String getName();

    /**
     * Actuators may have a sequence of IActions connected to them. If so, they are
     * triggered if the current computation context is the result of the transitions the
     * action responds to.
     * 
     * @return actions if any, or empty list
     */
    List<IAction> getActions();

    /**
     * If true, the actuator has actions that will only be executed at temporal
     * transitions.
     * 
     * @return true if there are temporal actions
     */
    boolean isTemporal();

    /**
     * If true, this actuator has actions triggered by explicit events.
     * 
     * @return
     */
    boolean hasEventHandlers();

    /**
     * Actuators come from models.
     * 
     * @return
     */
    IModel getModel();


}
