package org.integratedmodelling.api.modelling.contextualization;

import java.io.File;
import java.util.Collection;

import org.integratedmodelling.collections.Pair;

/**
 * A learning process produces a model and a training dataset along with everything else
 * the process does.
 * 
 * TODO
 * 
 * @author ferd
 *
 */
public interface ILearningProcessContextualizer extends IProcessContextualizer {

    /**
     * Produce the model definition as a string and, if applicable, any files required to
     * save and run the trained model. This should be called after initialize(), which
     * ends with the training. Files will be produced in the directory indicated and are
     * referenced using the passed relative path.
     * 
     * Returns null if the model cannot be saved for future reuse. If so, the learning
     * process behaves like a regular one.
     * 
     * @return
     */
    Pair<String, Collection<File>> getModel(String relativePath, File outputDirectory);

}
