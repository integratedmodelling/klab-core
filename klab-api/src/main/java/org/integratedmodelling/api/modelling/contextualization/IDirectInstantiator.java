package org.integratedmodelling.api.modelling.contextualization;

import java.util.Map;

import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Tag interface for any contextualizer that instantiates objects -either events
 * or subjects. These are returned by {@link IObjectSource#getInstantiator()} or
 * linked to k.IM 'using' operators.
 * 
 * @author fvilla
 *
 */
public abstract interface IDirectInstantiator extends IContextualizer {

	/**
	 * Called at the beginning. Passing a subject (never null) as the context
	 * for the subjects. Do not create anything here:
	 * {@link #createSubjects(ITransition, Map)} will be called also at
	 * initialization.
	 * 
	 * @param contextSubject
	 * @param context
	 * @param model
	 * @param expectedInputs
	 * @param expectedOutputs
	 * @param monitor
	 * 
	 * @param subject
	 * @param process
	 * @return new or changed observations
	 * @throws KlabException
	 */
	void initialize(IActiveSubject contextSubject, IResolutionScope scope, IModel callingModel,
			Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
			throws KlabException;

}
