package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.runtime.IActuator;

public interface IActiveObservation extends IObservation {

    /**
     * Active observations that have any action associated will have an actuator.
     * 
     * @return the associated actuator, or null.
     */
    IActuator getActuator();
    
}
