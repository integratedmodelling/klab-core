package org.integratedmodelling.api.modelling.contextualization;

/**
 * Tag interface whose sole purpose is to distinguish contextualizers
 * used for validation, so they can be executed after any k.IM actions
 * instead of before them.
 * 
 * @author Ferd
 *
 */
public interface IStateValidator extends IStateContextualizer {

}
