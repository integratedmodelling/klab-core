package org.integratedmodelling.api.modelling.visualization;

import java.awt.Image;

public interface IHistogram {

    long getNoDataCount();

    int[] getBins();

    double[] getNumericBoundaries();

    boolean isEmpty();

    String[] getValueDescriptions();

    double getAggregatedMean();

    double getAggregatedTotal();

    /**
     * More or less meaningful description of data and range, suitable for display (will contain
     * category names, so may be longer than a label).
     * @return an overall description
     */
    String getDescription();

    /**
     * Create an displayable image of the histogram.
     * 
     * @param width
     * @param height
     * @return a Java image of specified size
     */
    Image getImage(int width, int height);

}
