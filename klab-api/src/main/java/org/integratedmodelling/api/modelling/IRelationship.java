/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

/**
 * A IRelationship is the observation of a directed link between two IDirectObservation. It is
 * itself a direct observation, and as such can have qualities (e.g. flow strength) and its own scale. 
 * 
 * The main observable of a relationship is a property, never a concept.
 * 
 * @author Ferd
 *
 */
public interface IRelationship extends IDirectObservation {
    
    /**
     * Relationships always inhere to a subject. Never null.
     * 
     * @return the subject we belong to
     */
    @Override
    ISubject getContextObservation();

    /**
     * Relationships are directed by default. The source of the relationship cannot be null.
     * @return the subject we start at
     */
    ISubject getSource();

    /**
     * Relationships are directed by default. The target of the relationship cannot be null.
     * @return the subject we end at
     */
    ISubject getTarget();

}
