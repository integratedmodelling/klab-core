/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.contextualization;

import java.util.Map;

import org.integratedmodelling.exceptions.KlabException;

/*
 * Switching accessor, used by a ConditionalAccessor to allow the VM to determine which
 * observer in a switch should be given control to.
 * 
 * TODO this doesn't need to be a IStateAccessor - all it needs from it is setValue, 
 * but I'm not sure what happens in the compiler if I do it now.
 */
public interface ISwitchingContextualizer extends IStateContextualizer {

    /**
     * Return the number of conditional observers we're handling.
     * @return number of conditionals
     */
    int getConditionsCount();

    /**
     * True if the condition with the given index is null - meaning we're just
     * asking to stop at the first non-nodata result.
     * 
     * @param condition
     * @return true if condition at position is catch-all
     */
    boolean isNullCondition(int condition);

    /**
     * Compute the condition indexed by the passed index and return its 
     * result, which must be a boolean.
     * 
     * @param condition
     * @param inputs
     * @return value of condition 
     * @throws KlabException 
     */
    boolean getConditionValue(int condition, Map<String, Object> inputs) throws KlabException;

    /**
     * Notify the accessor that will be executed for the given condition index.
     * @param actuator
     * @param conditionIndex
     */
    void notifyConditionalContextualizer(IStateContextualizer actuator, int conditionIndex);
}
