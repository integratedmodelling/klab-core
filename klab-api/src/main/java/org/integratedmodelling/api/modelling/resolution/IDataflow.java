/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.resolution;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.exceptions.KlabException;

/**
 * The result of compiling a {@link IResolution} into a sequence of processing steps. Running the dataflow
 * will compute initialized states for the subject it refers to. 
 * 
 * Little more than a tag interface for now. Should be fleshed out when possible. Ideally a IDataflow could
 * bridge to any scientific workflow system or other virtual machine, and be translated into
 * procedural specifications for any runtime.
 * 
 * @author Ferd
 *
 */
public interface IDataflow  {
    
    /**
     * The nodes of a dataflow
     * @author ferdinando.villa
     *
     */
    interface ProcessingStep {
        
    }
    
    /**
     * The edges of the dataflow.
     * 
     * @author ferdinando.villa
     *
     */
    interface Datapath {
        
        ProcessingStep getSourceStep();

        ProcessingStep getTargetStep();
        
    }
    
    /**
     * Return the highest number of direct observations computed along
     * the dependency tree of this dataflow.
     * 
     * @return
     */
    int getDepth();
    
    /**
     * Return the level of dependency of the passed observation.
     * 
     * @param observation
     * @return
     */
    int getDepth(IDirectObservation observation);
    
    /**
     * Run the dataflow for a transition.
     * 
     * @param transition
     * @return true if all went well
     * @throws KlabException 
     */
    boolean run(ITransition transition) throws KlabException;

}
