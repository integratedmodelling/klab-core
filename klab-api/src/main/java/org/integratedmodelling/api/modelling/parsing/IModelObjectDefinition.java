/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.api.modelling.parsing;
//
// import org.integratedmodelling.api.lang.IModelResolver;
// import org.integratedmodelling.api.modelling.IAnnotation;
// import org.integratedmodelling.api.modelling.ILanguageObject;
// import org.integratedmodelling.api.modelling.IModelObject;
//
// public abstract interface IModelObjectDefinition extends ILanguageObject, IModelObject {
//
// public void setId(String id);
//
// public void setNamespace(INamespaceDefinition namespace);
//
// public void setMetadata(IMetadataDefinition metadata);
//
// /**
// * private knowledge is only known within its namespace.
// * @param b
// */
// void setPrivate(boolean b);
//
// /*
// * an inactive object is parsed but not used for anything.
// */
// void setInactive(boolean isInactive);
//
// // /**
// // * Add an error for later reporting. Implementations may decide to throw the passed exception. Return
// // the
// // * same error passed, so we can chain it efficiently.
// // *
// // * @param error
// // * @param lineNumber
// // */
// // Throwable addError(Throwable error, int lineNumber);
//
// /**
// * Initialize the object as soon as its definition is complete. Called by the model parser.
// *
// * @param resolver
// */
// public void initialize(IModelResolver resolver);
//
// /**
// * Add annotations. May be called 0+ times.
// *
// * @param processAnnotation
// */
// public void addAnnotation(IAnnotation processAnnotation);
//
// }
