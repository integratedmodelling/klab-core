/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import java.util.Collection;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A direct observation - subject, event or process - that is alive and operating in a context.
 * 
 * @author Ferd
 * @author Luke
 *
 */
public interface IActiveDirectObservation extends IDirectObservation, IActiveObservation {

    /**
     * Return the resolved coverage of the passed concept, i.e. the coverage resulting
     * from any previous observation of it made in this context. If no previous observations
     * were made, return null.
     * 
     * @param concept
     * @return
     */
    ICoverage getResolvedCoverage(ISemantic concept);

    /**
     * Active observations exist in a context and they must be able to create it. The context
     * is the knowledge environment for an observation tree.
     * 
     * @return
     */
    @Override
    IContext getContext();

    /**
     * This is where an agent determines whether or not two agent-states cause a collision. 
     * It is left to each agent (i.e. processing is done twice for every pair of overlapping
     * agent-states) to decide whether a collision happens, and why, and when, and what is the result. This is
     * because an agent might have a subjective idea of what types of collisions might happen, might have
     * domain-specific knowledge or detection algorithms, etc.
     *
     * This should always return the FIRST collision in the overlapping time period if there are more than
     * one, because the two overlapping agents' results (collisions) will be compared and the LATER one will
     * be discarded.
     *
     * @param myAgentState
     * @param otherAgentState
     * @return a collision, or null
     */
    ICollision detectCollision(IObservationGraphNode myAgentState, IObservationGraphNode otherAgentState);

    /**
     * This is where an agent will determine whether or not its agent-state will be affected
     * by the collision. If it answers true, then the agent-state will be partitioned into an
     * un-affected pre-collision partition, and a new observation task will be created for the collision time
     * which takes the collision into consideration. The post-collision agent-state may or may not be changed
     * from the original, and its duration may be the same or different.
     *
     * All causal relationships which depended on this agent-state AFTER the collision time will be
     * invalidated and so must be re-observed. This implies that the agent must re-create any causal links
     * (and observation tasks) which are a result of the new post-collision agent-state.
     *
     * @param agentState
     * @param collision
     * @return true if collision is relevant for agent state
     */
    boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision);

    
    /**
     * Activate or deactivate an observation, returning its previous state. Active means
     * "alive" for agents. When an observation is deactivated, the framework may decide to
     * get rid of it entirely. All observations are born active by default.
     * 
     * @param state
     * @return
     */
    boolean setActive(boolean state);
    
    /**
     * Current active status. 
     * 
     * @return
     */
    boolean isActive();
    
    /**
     *  
     * @param timePeriod
     * @return transition
     */
    ITransition reEvaluateStates(ITimePeriod timePeriod);

    /**
     * Perform the current transition and move the temporal context to the next.
     * 
     * @return the next temporal transition to execute
     * @throws KlabException 
     */
    ITransition performTemporalTransitionFromCurrentState(IObservationController controller) throws KlabException;

    /**
     * Return the linked states as a map keyed by the properties that are represented
     * by each state. Only return the IObjectState states, because that is how
     * these results are used in calling code. (abstract states
     * like topologies are not relevant to callers)
     * @return state map
     */
    public Map<IProperty, IState> getObjectStateCopy();

    /**
     * An active observation has a model - an exception is a ISubject which may not have one, and can
     * return null.
     * 
     * @return the model that is handling this observation.
     */
    public IModel getModel();

    /**
     * Request a state for the passed observable, which must be a quality. The state is created
     * if not already present in the subject. Optional "data" (of any kind) may be passed and it
     * should be an error to provide them when the state already exists. If it does not and data
     * are passed, the state should be created from them, appropriately. For example, if a single
     * number is passed, the state should be created as a constant value. If data are not passed,
     * an empty (no-data) state capable of expressing values over the full scale should be returned.
     *
     * @param observable 
     * @param data 
    
     * @return a new or existing state with the specified content, if any.
     * 
     * @throws KlabException 
     */
    IState getState(IObservableSemantics observable, Object... data) throws KlabException;

    /**
     * Request a state for the passed observable, which must be a quality. The state is created
     * if not already present in the subject. This state will not change in time, so it can only
     * be written at initialization. It's important to use this one at initialize() when appropriate,
     * or much space will be wasted at runtime.
     * 
     * @param observable
     * @return a static, new or existing state for the observable.
     * @throws KlabException 
     */
    IState getStaticState(IObservableSemantics observable) throws KlabException;

    /**
     * Add an event handler action to this subject and its parts or children, according to semantics.
     * This is only called for actions that are explicitly linked to semantic events.
     * 
     * @param action
     */
    void addEventHandler(IAction action);

    /**
     * Return any observation made by actions, optionally resetting the collection to empty.
     * 
     * @param reset
     * @return
     */
    Collection<IObservation> getActionGeneratedObservations(boolean reset);


}
