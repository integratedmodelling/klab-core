/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.resolution;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A  SubjectResolver can resolve a context to the model strategy that observe the target set in the 
 * context. The resolve() operation returns the total extent coverage that was resolved. 
 * 
 * @author Ferd
 * @author Luke
 * 
 */
public interface ISubjectResolver {

    /**
     * @param subject
     * @param monitor
     * @return coverage resulting from resolution
     * @throws KlabException
     */
    ICoverage resolve(IActiveDirectObservation subject, IMonitor monitor) throws KlabException;
    
    /**
     * Resolve an abstract concept to its context-dependent concrete subclass in the passed
     * subject's context. Because the concept is universal in its scope, coverage is always
     * full.
     * 
     * @param abstractConcept
     * @param subject
     * @return
     * @throws KlabException
     */
    IConcept resolve(IConcept abstractConcept, IActiveDirectObservation subject) throws KlabException;

    /**
     * @param observable
     * @param monitor
     * @param contextSubject
     * @return coverage resulting from resolution
     * @throws KlabException
     */
    ICoverage resolve(IObservableSemantics observable, IMonitor monitor, IActiveSubject contextSubject)
            throws KlabException;

}
