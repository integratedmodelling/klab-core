/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.exceptions.KlabException;

/**
 * An observer is the observation semantics of an observable quality. It is
 * responsible for interpreting the states that will describe the observable
 * indirectly.
 * 
 * Observers may generate a concept other than the stated observable to allow
 * more intuitive specification semantics. In all cases, Thinklab ensures that
 * the semantics of the observation concept is consistent and complete enough
 * for validation of any further use in semantic mediation.
 * 
 */
public abstract interface IObserver extends IObservingObject {

	/**
	 * Returns the core type that defines the observation this observer is
	 * making and how it matches the final observable. The type should be
	 * specialized enough to allow correct reasoning and matching; for example,
	 * for measurements it should specialize to include the physical dimensions
	 * of the observer (e.g. LengthMeasurement) so that we can lookup compatible
	 * observations. It will end up in the IObservable returned by
	 * getObservable().
	 * 
	 * @return the observation type
	 */
	IConcept getObservationType();

	/**
	 * Return the model we're part of. Tried to avoid this forever, but
	 * visualization made it necessary.
	 * 
	 * @return the model we're in
	 */
	IModel getModel();

	/**
	 * If true, observer produces an extensive value over the passed extent, one
	 * that varies with the extents of computation. A true return value will
	 * cause different aggregation than the default averaging when mediating to
	 * different scales.
	 * 
	 * @return
	 */
	boolean isExtensive(IConcept extent);

	/**
	 * Contextualizers returned by observers are always state contextualizers.
	 * 
	 * @see IObservingObject#getContextualizer(IResolutionScope, IMonitor)
	 */
	@Override
	IStateContextualizer getContextualizer(IResolutionScope scope, IProvenance.Artifact provenance, IMonitor monitor) throws KlabException;

	/**
	 * This one is fundamental for proper operation: if it returns false, the
	 * observer is expected to need a source of states for its observable (data
	 * or other model). If it returns true, the observer is treated as capable
	 * of computing its data based only on its dependencies, and no resolution
	 * of the observable is attempted. Should return true if there is a
	 * datasource or a contextualizer capable of producing vales autonomously
	 * (equivalent to a 'set to' vs. a 'change to' action in k.IM).
	 * 
	 * @return true if a computation with an accessor is sufficient to make the
	 *         observation
	 */
	boolean needsResolution();

}
