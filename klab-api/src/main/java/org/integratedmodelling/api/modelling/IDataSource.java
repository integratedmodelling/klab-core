/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * IDatasource is an object that represents states for observers to interpret. Datasources
 * incarnate the objects of semantic annotation - datasets - which are matched to Scales
 * and if the match succeeds, are capable of producing an IAccessor that will give access
 * to the states under the viewpoint of that Scale.
 *
 * A IDataSource is nothing in Thinklab without a matching IObserver that will provide
 * it with observation semantics; observers are specified within models "of" the datasource.
 * 
 * @author  Ferd
 */
public interface IDataSource extends IMetadataHolder {

    /**
     * Extents covered by the data. If the datasource is constant or has no extents, return 
     * an empty scale (not null).
     * 
     * @return the coverage of the data source.
     */
    public IScale getCoverage();

    /**
     * Yes if source can be expected to be usable. For example an online source may 
     * not respond. Called at resolution so that models that use offline datasources
     * won't make the cut instead of breaking a model at runtime.
     * 
     * @return true if usable.
     */
    public boolean isAvailable();

    /**
     * Get a contextualizer to compute observation states in the passed context. 
     * 
     * @param context
     * @param observer
     * @param monitor
     * @return the contextualizer that will compute values in the passed scale and with the
     *         passed semantics. The contextualizer is then run by the observer's actuator.
     * @throws KlabException
     */
    IStateContextualizer getContextualizer(IScale context, IObserver observer, IMonitor monitor)
            throws KlabException;

}
