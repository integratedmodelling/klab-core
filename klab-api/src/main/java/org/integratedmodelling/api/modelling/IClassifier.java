/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

/**
 * An object that can be matched to another.
 * 
 * @author Ferd
 *
 */
public interface IClassifier {

    /**
     * True if passed object matches the conditions of the classifier.
     * 
     * @param o
     * @return True if passed object matches the conditions of the classifier
     */
    public boolean classify(Object o);

    /**
     * True if this classifier matches everything.
     * 
     * @return True if this classifier matches everything
     */
    boolean isUniversal();

    /**
     * True if this classifier only matches null (unknown).
     * 
     * @return True if this classifier only matches null
     */
    boolean isNil();

    /**
     * True if this is an interval classifier.
     * 
     * @return True if this is an interval classifier
     */
    boolean isInterval();


    /**
     * Classifiers may be used as a value; this one should return the most appropriate 
     * value translation of the classifier, i.e. the matched object if it's matching a
     * single one, or possibly a random object among the choices if it's in OR.
    
     * @return the value this classifier resolves to.
     */
    public Object asValue();
}
