/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;

/**
 * The k.IM language end of any stated knowledge. Usually the result of a stated concept declaration, in the appropriate
 * contexts it may also be set from known models or network-available observations.
 * 
 * @author Ferd
 *
 */
public interface IKnowledgeObject extends IModelObject, ISemantic {

    /**
     * 
     * @return true if this represents a concept.
     */
    boolean isConcept();

    /**
     * 
     * @return true if this represents a property
     */
    boolean isProperty();

    /**
     * Knowledge can be also referenced by referring to a model for it, using its fully qualified name or local ID. In that
     * case, getModel() will return the model and getKnowledge() will return its observable.
     * 
     * @return the model if this object states knowledge through one
     */
    IModel getModel();

    /**
     * If a fully qualified name is specified in the declaration and it does not correspond to a 
     * known model, a direct observation with that name will be looked for on the network. If that
     * is found, the observable will be set from it and this will return the resolved object.
     * 
     * @return the direct observer if the object states knowledge through one.
     */
    IDirectObserver getDirectObserver();

    /**
     * True if this was parsed from a 'negative' observable (e.g. 'no Precipitation'). Does not
     * affect the concept returned by getKnowledge(), which will contain the negativity.
     * 
     * @return true if this comes with a negation.
     */
    boolean isNegated();

    /**
     * Get the knowledge as a concept if we are a concept, null if not.
     * 
     * @return the concept we are
     */
    IConcept getConcept();

    /**
     * Get the knowledge as a concept if we are a concept, null if not.
     * 
     * @return the property we are
     */
    IProperty getProperty();

    /**
     * The 'of' part if present, representing the inherent type for the main concept, which must be
     * a quality or a trait.
     * 
     * @return the inherent type in our statement
     */
    IKnowledge getInherentType();

    /**
     * If any, the 'within' contexts, which must all be subject concepts, in order of
     * declaration. If no within spec has been given, return an empty list, not null.
     * 
     * @return the context type if any
     */
    IKnowledge getContextType();

    /**
     * If a 'down to' specification has been given and parsed correctly, this will return the
     * level of detail to which this concept has been referred to (0 or larger, only >0 being
     * useful). This only applies to classes and traits; it's an error to refer to level of detail
     * for any other concept.
     *  
     * @return the level of detail if supplied
     */
    IKnowledge getDetailLevel();

    /**
     * true if this object was declared but the declaration has not resulted into usable knowledge. This can
     * also be forced by specifying 'is nothing'.
     * 
     * @return true if this is not knowledge.
     */
    boolean isNothing();
    
    /**
     * True only if the concept is only an alias for another entirely built from
     * worldview concepts and has no children.
     * 
     * @return
     */
    boolean isWorldviewCompatible();

}
