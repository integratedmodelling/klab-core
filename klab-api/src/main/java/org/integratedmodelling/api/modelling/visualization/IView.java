package org.integratedmodelling.api.modelling.visualization;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.visualization.IViewer.Display;

/**
 * A IView represents a specific display of a context, containing zero or more
 * {@link IObservation}. A {@link IViewer} handles
 * one or more views (corresponding to different {@link Display} values) and can
 * return it on request. 
 * 
 * From the API point of view, IView is read-only and can be used to assess whether
 * a particular observation is visible.
 * 
 * @author ferdinando.villa
 *
 */
public interface IView {

    /**
     * Check if the observation is visible in this view.
     * 
     * @param observation
     * @return true if observation is displayed.
     */
    boolean isShown(IObservation observation);

    /**
     * If the view is focused on a specific time, return the
     * correspondent locator.
     * 
     * @return a time locator if applicable, or null.
     */
    IScale.Locator getTime();

}
