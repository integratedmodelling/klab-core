/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IParseable;

/**
 * @author ferdinando.villa
 *
 */
public interface IUnit extends IValueMediator, IParseable {
    
	/**
	 * True if a unit describing the passed extent is found at the 
	 * denominator.
	 * 
	 * @param extent
	 * @return
	 */
    boolean isDensity(IConcept extent);

    /**
     * 
     * @return true if its over time
     */
    boolean isRate();
    
    /**
     * 
     * @return if rate, get the time unit
     */
    public abstract IUnit getTimeExtentUnit();

    /**
     * True if this unit is a density over a spatial length.
     * 
     * @return true if density over 1d space
     */
    public abstract boolean isLengthDensity();

    /**
     *  
     * @return if 1d density, return space unit
     */
    public abstract IUnit getLengthExtentUnit();

    public abstract boolean isArealDensity();

    /**
     * If the unit represents an areal density, return the area term with 
     * inverted exponents - e.g. if we are something per square meter, return
     * square meters. If not an areal density, return null.
     * 
     * @return unit of areal space if we're an areal density 
     */
    public abstract IUnit getArealExtentUnit();

    public abstract boolean isVolumeDensity();

    public abstract IUnit getVolumeExtentUnit();

    public abstract boolean isUnitless();

    /**
     * True if this unit is a density over the kind of space represented by the passed extent,
     * which may be null (in which case, obviously, the result is false). Accounts automatically
     * for the dimensionality of the space represented.
     * 
     * @param scale
     * @return true if spatial density
     */
    public abstract boolean isSpatialDensity(IExtent scale);
}
