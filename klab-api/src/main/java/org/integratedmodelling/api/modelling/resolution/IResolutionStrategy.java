///*******************************************************************************
// *  Copyright (C) 2007, 2015:
// *  
// *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
// *    - integratedmodelling.org
// *    - any other authors listed in @author annotations
// *
// *    All rights reserved. This file is part of the k.LAB software suite,
// *    meant to enable modular, collaborative, integrated 
// *    development of interoperable data and model components. For
// *    details, see http://integratedmodelling.org.
// *    
// *    This program is free software; you can redistribute it and/or
// *    modify it under the terms of the Affero General Public License 
// *    Version 3 or any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but without any warranty; without even the implied warranty of
// *    merchantability or fitness for a particular purpose.  See the
// *    Affero General Public License for more details.
// *  
// *     You should have received a copy of the Affero General Public License
// *     along with this program; if not, write to the Free Software
// *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// *     The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
//package org.integratedmodelling.api.modelling.resolution;
//
//import org.integratedmodelling.api.modelling.ICoverage;
//import org.integratedmodelling.api.modelling.IModel;
//import org.integratedmodelling.api.modelling.ISubject;
//import org.integratedmodelling.api.modelling.resolution.IResolutionStrategy.Step;
//import org.integratedmodelling.api.modelling.scheduling.ITransition;
//import org.integratedmodelling.api.provenance.IProvenance;
//import org.integratedmodelling.exceptions.KlabException;
//
///**
// * A resolution strategy is available for a {@link ISubject} after calling the correspondent
// * {@code resolve} function {@link ISubjectResolver}. The strategy is composed of a series of steps corresponding
// * to the dependency groups identified for the subject. Each step has a {@link IProvenance} and a {@link IDataflow}
// * associated, which are usually only non-trivial for quality resolution strategies.
// *
// *
// * @author Ferd
// *
// */
//public interface IResolutionStrategy extends Iterable<Step> {
//
//    /**
//     * Steps in a resolution strategy. Each step is called in sequence at initialization. 
//     * @author Ferd
//     *
//     */
//    public interface Step {
//
//        /**
//         * If the step resolves a process or subject, return the models that provide
//         * relevant accessors and object sources. May be null.
//         * 
//         * @return the model that will make the requested direct observation
//         */
//        public IModel getModel();
//
//        /**
//         * The provenance graph for the passed step, detailing all choices made by Thinklab to observe each
//         * concept. May be null.
//         *
//         * @param step
//         * @return the provenance graph
//         */
//        IResolution getResolutionGraph();
//
//        /**
//         * The workflow for the passed step, detailing all the processing steps computed to
//         * create the initial observation. A workflow is compiled from a provenance graph.
//         * May be null, and may be not null even for process or subject steps, in which case
//         * the workflow should be run first.
//         *
//         * @param step
//         * @return the dataflow
//         */
//        IDataflow getWorkflow();
//
//    }
//
//    /**
//     * Number of steps in this strategy.
//     *
//     * @return the number of steps
//     */
//    int getStepCount();
//
//    /**
//     * Get the n-th step.
//     * @param i
//     * @return the n-th step 
//     */
//    Step getStep(int i);
//
//    /**
//     * Execute all steps for the passed transition. The null transition is 
//     * initialization.
//     * 
//     * @param transition
//     * @param scope to set any resolved observations for reuse in further resolution.
//     * 
//     * @return true if the strategy succeeded
//     * @throws KlabException 
//     */
//    boolean execute(ITransition transition, IResolutionScope scope) throws KlabException;
//
//    /**
//     * Get the total coverage that this strategy was able to resolve. Never null and implicitly 
//     * sufficient, as no strategy is generated unless there is enough coverage to be worth observing. 
//     * 
//     * @return the coverage
//     */
//    ICoverage getCoverage();
//
//}
