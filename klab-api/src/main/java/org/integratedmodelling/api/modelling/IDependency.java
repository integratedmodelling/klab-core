/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;

/**
 * Dependency objects can be associated with observing objects {@link IObservingObject} and
 * state the observations that need to be made in order for their observation to be
 * computable.
 * 
 * @author Ferd
 *
 */
public interface IDependency extends ILanguageObject, ISemantic {

    /**
     * The object that we need to observe to satisfy this dependency.
     * @return the observable
     */
    IObservableSemantics getObservable();

    /**
     * Whether we can do without this dependency or not.
     * @return whether dependency is optional.
     */
    boolean isOptional();

    /**
     * The property that expresses the counterpart of this dependency in the conceptual model for the
     * observable of the embedding model. May be null. If not, the dependency resolution strategy must
     * honor the restrictions of the property in the context of each matched observable for the embedding
     * model.
     * 
     * @return a property
     */
    IProperty getProperty();

    /**
     * If this does not return null, the observations resulting from resolving the dependency must
     * be reinterpreted as the trait returned, i.e., their observables must be redefined by inheriting
     * the trait.
     * 
     * Largely superseded by roles and may be deprecated soon.
     * 
     * @return a trait to reinterpret the result as
     */
    IConcept reinterpretAs();

    /**
     * Return whatever object was passed to contextualize the dependency (the 'at' part of the
     * specification). It can be anything that generates context - either extents, models, or a function
     * call that creates that.
     * 
     * @return the context model - not used at the moment
     */
    Object getContextModel();

    /**
     * If this is true, the dependency will refer to 'each' subject of the type returned by
     * getDistributionContext(), which will need to be observed before the dependency can be
     * computed. It may also have a 'where' clause to filter the subjects.
     *  
     * @return as said - unimplemented for now
     */
    boolean isDistributing();
    
    /**
     * A generic dependency forces the resolution behavior used for abstract observables on non-abstract
     * ones. Under abstract resolution, all known subtypes of the observable will be resolved; if instantiators
     * for the base type are also found, they will be used only if the subtypes do not cover the context.
     * 
     * Generic dependencies are only allowed for countable observables (subjects and events).
     * 
     * @return
     */
    boolean isGeneric();

    /**
     * This will only return non-null in distributing dependencies, and will be applied to the subjects 
     * in the distribution context to select the ones that match.
     * 
     * @return as said - unimplemented for now
     */
    IExpression getWhereCondition();

    /**
     * This is non-null in distributing dependencies, and returns the direct observable for the
     * objects that make up the distribution context.
     * 
     * @return as said - unimplemented for now
     */
    IConcept getDistributionContext();

    /**
     * All dependencies have a formal name, either given by the user or assigned automatically. It is an error
     * to have non-unique formal names in any observing object.
     *
     * @return the name of the dependency. Never null.
     * 
     */
    String getFormalName();

}
