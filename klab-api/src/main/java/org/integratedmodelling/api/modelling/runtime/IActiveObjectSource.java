/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.modelling.runtime;

import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Object source that can produce state actuators to compute the dereification
 * of object properties.
 * 
 * @author Ferd
 *
 */
public interface IActiveObjectSource extends IObjectSource {
	/**
	 * Returns a state accessor for the dereified quality corresponding to the
	 * passed attribute and observer.
	 * 
	 * @param attribute
	 * @param observer
	 * @param context
	 * @param monitor
	 * @return a state actuator that will dereify an attribute of the objects
	 *         according to the passed observer and scale.
	 * @throws KlabException
	 */
	IStateContextualizer getContextualizer(String attribute, IObserver observer, IScale context, IMonitor monitor)
			throws KlabException;

}
