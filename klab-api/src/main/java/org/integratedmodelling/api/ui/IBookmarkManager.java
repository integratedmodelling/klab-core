package org.integratedmodelling.api.ui;

import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.exceptions.KlabIOException;

/**
 * The bookmark manager allows keeping track of bookmarks and
 * is linked to the source code for those stated through 
 * code annotations.
 * 
 * @author ferdinando.villa
 *
 */
public interface IBookmarkManager {

    /**
     * Check if the passed bookmark comes from code or is a permanent one
     * saved by the user.
     * 
     * @param bookmark
     * @return true if bookmark is permanent.
     */
    boolean isPermanent(IBookmark bookmark);

    /**
     * Remove the passed bookmark.
     * 
     * @param bookmark
     * @throws KlabIOException
     */
    void deleteBookmark(IBookmark bookmark) throws KlabIOException;

    /**
     * Add the passed bookmark and persist storage so that it will be
     * there next time.
     * 
     * @param bookmark
     */
    void addPersistentBookmark(IBookmark bookmark);

    /**
     * Bookmark the passed model object with the passed id (potentially a slash-separated
     * path for folders) and description. Will create a bookmark and call {@link #addPersistentBookmark(IBookmark)} 
     * with it.
     * 
     * @param modelObject
     * @param name
     * @param description
     * @return the bookmark just added
     */
    IBookmark bookmark(IModelObject modelObject, String name, String description);

}
