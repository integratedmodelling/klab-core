/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.ui;

import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IModel;

/**
 * Points to a model object available somewhere in the model tree or to an external
 * resource. Bookmarks may be persistent across sessions or be generated on the fly,
 * either by users or by the code, to provide stand-ins for objects of any kind.
 * 
 * Bookmarks play several roles, particularly in the k.LAB IDE, where they are used
 * as the vehicle for all drag/drop operations, including those involving objects
 * created on the fly.
 * 
 * The type specified by {@link #getType()} is not necessarily indicative of which of the
 * get- functions will return the object represented: bookmarks of any type may be
 * representing a URN as well as the finished object, so the get-(object) should be
 * checked for null and if so, {@link #getUrn()} should be called and URN resolution
 * should proceed as prescribed.
 * 
 * This API is still tentative and incomplete, although operational.
 * 
 * FIXME unify with PaletteItem and remove. Serialize to JSON.
 * 
 * @author Ferd
 *
 */
public interface IBookmark {

    /**
     * Type of bookmark. Returned by {@link IBookmark#getType()}.
     * @author Ferd
     *
     */
    public static enum Type {
        /**
         * Bookmark represents a folder of bookmarks
         */
        FOLDER,
        /**
         * Bookmark represents a concept
         */
        CONCEPT,
        /**
         * Bookmark represents a model
         */
        MODEL,
        /**
         * Bookmark represents a subject observation
         */
        SUBJECT,
        /**
         * Bookmark represents a generic URN
         */
        RESOURCE
    }

    /**
     * Return the model corresponding to this bookmark, or null if not a model or not
     * existing in workspace.
     * 
     * @return a model
     */
    IModel getModel();

    /**
     * Return the concept corresponding to this bookmark, or null if not a concept or not
     * existing in workspace.
     * 
     * @return a model
     */
    IConcept getConcept();

    /**
     * Return the URN corresponding to this bookmark, or null if not a resource.
     * 
     * @return a model
     */
    String getUrn();

    /**
     * Return the user-specified name of the bookmark, if any. Slashes represent folder
     * paths.
     * 
     * @return user-specified name for bookmark. May be null for system-generated ones.
     */
    String getName();

    /**
     * Return the type of bookmark this is.
     * 
     * @return type of bookmark
     */
    Type getType();

    /**
     * A folder bookmark has children.
     * 
     * @return all children
     */
    Collection<IBookmark> getChildren();

    /**
     * Return the parent bookmark, if any.
     * @return parent bookmark, or null.
     */
    IBookmark getParent();
    
    /**
     * Get a string signature that can fully identify itself and the bookmarked
     * object.
     * 
     * @return
     */
    String getSignature();

}
