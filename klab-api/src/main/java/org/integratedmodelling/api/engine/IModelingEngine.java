/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any other authors listed
 * in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable modular, collaborative,
 * integrated development of interoperable data and model components. For details, see
 * http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the Affero
 * General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any warranty; without even the
 * implied warranty of merchantability or fitness for a particular purpose. See the Affero General Public
 * License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this program; if not, write
 * to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. The license
 * is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.engine;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.monitoring.INotificationBus;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A modeling engine is an engine that makes observations in session.
 *
 * @author ferdinando.villa
 *
 */
public interface IModelingEngine extends IEngine, IIdentity {

    /**
     * Add listeners to be notified of sessions as they are opened and closed.
     * 
     * @author Ferd
     *
     */
    public interface Listener {

        /**
         * Passed session was just opened.
         * 
         * @param session
         */
        void sessionOpened(ISession session);

        /**
         * Passed session was just closed. Don't do anything with it and do not store it.
         * 
         * @param session
         */
        void sessionClosed(ISession session);

        /**
         * Engine was just locked by current session user.
         * 
         * @param workspaceCleared
         */
        void engineLocked();

        /**
         * engine lock was just released.
         */
        void engineUnlocked();

        /**
         * Engine user was authenticated.
         * 
         * @param user
         */
        void engineUserAuthenticated(IUser user);
    }

    /**
     * Start the engine if it wasn't running. Return false if it was running and nothing needed to be done.
     * 
     * @return the engine itself (to enable fluent idioms) if the engine was successfully started, or null.
     */
    IModelingEngine start();

    /**
     * Stop the engine if it was running. Return false if it was not running.
     * 
     * @return true if the engine was successfully stopped.
     */
    boolean stop();

    /**
     * Observations happen in sessions, which are opened by users. User privileges are intersected with those
     * of the user owning the engine.
     * 
     * @param user
     * @return a session dedicated to a user.
     * 
     * @throws KlabException in case of error (e.g. user authorization, engine is not running, etc.)
     */
    ISession createSession(IUser user) throws KlabException;

    /**
     * All modeling engines run as a certified user, which we can retrieve. Sessions can be open for other
     * users as well, according to
     * 
     * @return the user that owns the engine.
     */
    IUser getUser();

    /**
     * Add a listener to be notified of sessions.
     * 
     * @param listener
     */
    void addListener(Listener listener);

    /**
     * Modeling engines have a notification bus for communication of session state and engine status.
     * 
     * @return the notification bus.
     */
    INotificationBus getNotificationBus();

    /**
     * Get the channel for the notification bus over which this engine is communicating global notifications.
     * 
     * @return a channel name to subscribe to.
     */
    String getCommunicationChannel();

}
