/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.engine;

import java.io.File;
import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.configuration.IResourceConfiguration.StaticResource;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A basic k.LAB engine. One is always available through KLAB.ENGINE. May be a proper engine or
 * a client connected to a networked engine. 
 * 
 * Subinterfaces add functions for modeling. A {@link IServer} is a networked engine; anything capable 
 * of making observations is a {@link IModelingEngine}.
 * 
 * @author ferdinando.villa
 */
public interface IEngine {
    

    /**
     * All engines have a name. Only network node engines must have a unique and
     * sensible name, the others may have descriptive names with no requirement
     * of being unique. The requirement of uniqueness is enforced in INode.getId().
     * 
     * @return the name of the engine.
     */
    String getName();

    /**
     * Return the URL the server responds on.
     * 
     * @return the URL to communicate with the engine.
     */
    String getUrl();

    /**
     * Use to quickly check if the engine is running or not.
     * 
     * @return whether startup has been successful.
     */
    boolean isRunning();

    /**
     * True if this engine provides the named project or component, or service to the active user.
     * 
     * @param id the name of a project or a component. Projects and components must all
     *        have distinct names. To check if a service is provided, pass a {@link StaticResource}
     *        instead.
     * @return true if the engine provides what named.
     */
    boolean provides(Object id);

    /**
     * Submit a locally created direct observer for use and optionally storage in this engine.
     * 
     * @param observer
     * @param store
     * @return the fully qualified name of the submitted observer.
     * @throws KlabException
     */
    String submitObservation(IDirectObserver observer, boolean store) throws KlabException;

    /**
     * Return descriptors for all observations that match a textual query. The modeling engine
     * should also interrogate all authorized nodes on the network.
     * 
     * @param text
     * @param localOnly if true, only lookup those in local kbox; otherwise also look in all nodes that
     *        authorize us to search.
     * @return a list of potentially matching metadata, from which the observations can be
     *         observed. If ranking, the best matches should come first.
     * @throws KlabException
     */
    public List<IObservationMetadata> queryObservations(String text, boolean localOnly)
            throws KlabException;

    /**
     * Retrieve a direct observer by fully qualified name.
     * 
     * @param observationid
     * @param nodeId 
     * @return a direct observer, or null if retrieval was unsuccessful.
     * @throws KlabException
     */
    public IDirectObserver retrieveObservation(String observationid, String nodeId)
            throws KlabException;

    /**
     * Return descriptors for models that match the passed info. In modeling engines, it
     * will also consult the network.
     *  
     * @param observable
     * @param context
     * @return metadata for all matching models
     * @throws KlabException
     */
    public List<IModelMetadata> queryModels(IObservableSemantics observable, IResolutionScope context)
            throws KlabException;

    /**
     * Start the task of setting up a component using its setup conventions; return the task that
     * is accomplishing that. Throw exceptions if unauthorized or errors happened launching setup.
     * 
     * @param componentId
     * @return a task that is computing the setup operation in the engine.
     * @throws KlabException 
     */
    public ITask setupComponent(String componentId) throws KlabException;

    /**
     * Import observations from local file, return resulting list of objects.
     * 
     * FIXME improve result semantics. Should probably be just a list of ObservationMetadata.
     * 
     * @param file
     * @return something that should be clearer.
     * @throws KlabException
     */
    List<IObservationMetadata> importObservations(File file) throws KlabException;

    /**
     * Remove all observations with passed fully qualified names.
     * 
     * @param observationNames
     * @throws KlabException
     */
    void removeObservations(Collection<String> observationNames) throws KlabException;

    /**
     * Get the root monitor for the engine. Session, context and task monitors are generated from
     * it.
     * 
     * @return the root monitor. 
     */
    IMonitor getMonitor();

    /**
     * Return the object that gives access to the engine's shared resources and
     * their permissions.
     * 
     * @return the local resource configuration.
     */
    IResourceConfiguration getResourceConfiguration();

    /**
     * Return the function prototype for the given function ID, or null if not registered.
     * 
     * @param id
     * @return the function prototype, or null
     */
    IPrototype getFunctionPrototype(String id);

    /**
     * Get all function prototypes we want the world to know we have. In modeling engines,
     * this translates into all functions the language can use. In nodes, only the functions
     * published by components and projects we can share.
     * 
     * @return all function prototypes we publish.
     */
    Collection<IPrototype> getFunctionPrototypes();
    
    /**
     * Get the network we're connected to.
     * 
     * @return the network
     */
    INetwork getNetwork();

    /**
     * Boot time
     * @return the local boot time
     */
    long getBootTime();

    /**
     * Shutdown the engine nicely.
     * @param delaySeconds number of seconds to wait before shutdown.
     * 
     */
    void shutdown(int delaySeconds);

    /**
     * In modeling engines and nodes, scans a package (from the main codebase or a component) for 
     * declarations of k.LAB components, services or other extensions.
     * 
     * @param packageId
     * @return a list of pairs containing each the annotation and the annotated class found.
     * @throws KlabException
     */
    List<Pair<Annotation, Class<?>>> scanPackage(String packageId) throws KlabException;

	
	/**
     * Environmental variable that may be substituted in the script that launches an
     * embedded server.
     */
    public static final String ENV_THINKLAB_MAX_MEMORY = "THINKLAB_MAX_MEMORY";
    public static final String ENV_THINKLAB_JRE        = "THINKLAB_JRE";
    public static final String VERSION_INFO_STRING     = "THINKLAB_VERSION_INFO";

}
