package org.integratedmodelling.api.engine;

import org.integratedmodelling.api.auth.IUser;

/**
 * Gives access to a locking status. Every modeling engine has a lock.
 * 
 * @author ferdinando.villa
 *
 */
public interface ILock {

    /**
     * Check the locking status.
     * @return true if locked.
     */
    boolean isLocked();

    /**
     * Lock. Will fail if user is unauthorized or lock was already acquired by
     * another user, unless force is true and user has rights to force.
     * 
     * @param user 
     * @param force
     * @return true if lock was successful.
     */
    boolean lock(IUser user, boolean force);

    /**
     * Release a lock.
     * @param user the user releasing the lock. Must be same user who locked.
     * @return true if lock was successfully released.
     */
    boolean unlock(IUser user);

    /**
     * Null unless isLocked() returned true.
     * 
     * @return the user who is locking the engine, or null if nobody is.
     */
    IUser getUser();

}
