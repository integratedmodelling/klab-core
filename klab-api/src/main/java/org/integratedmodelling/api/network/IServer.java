/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any other authors listed
 * in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable modular, collaborative,
 * integrated development of interoperable data and model components. For details, see
 * http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the Affero
 * General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any warranty; without even the
 * implied warranty of merchantability or fitness for a particular purpose. See the Affero General Public
 * License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this program; if not, write
 * to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. The license
 * is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.network;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.engine.IEngine;

/**
 * A INode is a connected Thinklab server in a INetwork. All the nodes in a network have an identical INetwork
 * object that gives access to all nodes. Each server that contains a INetwork will have at least the "self"
 * INode in it.
 * 
 * @author ferdinando.villa
 *
 */
public interface IServer extends IEngine, IIdentity {

    /**
     * @author Ferd
     *
     */
    public static enum Authentication {
        /**
         * No authentication is provided by this node.
         */
        NONE,

        /**
         * This node can authenticate a user by referring to another authoritative node.
         */
        INDIRECT,

        /**
         * This node manages a user directory and can authenticate users directly.
         */
        DIRECT
    }

    /**
     * The server ID is unique within the INetwork. It should be the same as IEngine.getName() but with the
     * uniqueness constraint enforced.
     * 
     * @return node id
     */
    String getId();

    /**
     * Describes the way we can authenticate users.
     * 
     * @return node authentication type
     */
    Authentication getAuthentication();

    /**
     * Nodes should be polled for active status and transparently added/dropped as their status changes.
     * 
     * @return true if node is giving recent signs of life
     */
    boolean isActive();

    /**
     * The auth key that we must know in order to use this node. Communicated by the server itself upon
     * authentication.
     * 
     * @return authorization key, specific of authentication context.
     */
    String getKey();

    /**
     * Optional description from certificate. Please return an empty string, not null, if there's no
     * description.
     * 
     * @return node description
     */
    String getDescription();

}
