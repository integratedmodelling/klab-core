package org.integratedmodelling.api.network;

/**
 * A node network has an authorization key it can use to authorize operations coming from
 * other nodes that know it by sharing certificates.
 * 
 * @author ferdinando.villa
 *
 */
public interface INodeNetwork extends INetwork {

    /**
     * The authorization key from our server certificate.
     * 
     * @return the auth key. Never null.
     */
    public String getKey();
}
