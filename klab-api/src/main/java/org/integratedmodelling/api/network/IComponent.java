/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.network;

import java.io.File;
import java.util.Collection;

import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IPrototype;

/**
 * A Component is a project that can include binary assets and is usually packages as a 
 * jar file in predefined locations. Components are unpacked before being loaded.
 * 
 * Currently there is no signing mechanism but one will be soon provided to ensure that
 * components are legitimate.
 * 
 * @author ferdinando.villa
 *
 */
public interface IComponent extends IProject {

    /**
     * Return every binary asset included in the project.
     * 
     * @return all loadable binary files.
     */
    Collection<File> getBinaryAssets();
    
    /**
     * The API, as a set of prototypes for all the services provided by this component.
     * 
     * @return all prototypes provided
     */
    Collection<IPrototype> getAPI();

    /**
     * Called to establish if the component has been properly initialized and is ready to be used.
     * 
     * @return true if usable.
     */
    boolean isActive();

}
