package org.integratedmodelling.api.network;

import org.integratedmodelling.api.auth.IUser;

/**
 * An engine network can identify the user that owns the engine and the primary node with which it connects to
 * the network.
 * 
 * @author ferdinando.villa
 *
 */
public interface IEngineNetwork extends INetwork {

    /**
     * Get the primary node that we use to connect. Can only be null if the {@link INetwork#isOnline()}
     * returns false.
     * 
     * @return the primary node, or null if offline.
     */
    IServer getPrimaryNode();

    /**
     * Return the user we run under. Worst case scenario it can be the anonymous user where (@link
     * IUser#isAnonymous()} returns true.
     * 
     * @return the user. Never null.
     */
    IUser getUser();
}
