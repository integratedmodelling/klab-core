/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.knowledge;

import java.util.List;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.metadata.ISearchResult;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * An authority processes an ID into an identity. It must have a unique name and correspond to
 * the "official" view of a controlled vocabulary or repository of unique IDs that correspond
 * to unambiguous identities. The k.LAB counterpart is a trait concept that can be used to
 * identify any fundamental observable.
 * 
 * @author Ferd
 *
 */
public interface IAuthority<T> {

    /**
     * Simple and unique string. Will be shown to users and selected by them. All uppercase is the convention.
     * 
     * @return the authority name
     */
    String getAuthorityId();

    /**
     * Return a trait corresponding to the identifier recognized by the authority. Should validate
     * the ID and throw an exception if it's not recognized.
     * 
     * @param id
     * @param authorityId TODO
     * @return the identity corresponding to the passed id string
     * @throws KlabValidationException
     */
    IConcept getIdentity(String id, String authorityId) throws KlabValidationException;
    
    /**
     * Return metadata for a given textual query. Use DC ontology identifiers in the metadata as
     * specified in {@link IMetadata}.
     * 
     * @param query
     * @return metadata for all matching identities, using dublin core tags.
     */
    ISearchResult<T> search(String query, String authorityId);

    /**
     * Provide a longish description of what this authority does.
     * 
     * @return a readable description of what the authority handles.
     */
    String getDescription();

    /**
     * Provide a longish description of what each specific sub-authority does.
     * 
     * @return a readable description of what the authority handles.
     */
    String getDescription(String authorityId);

    
    /**
     * True if search() is going to be returning anything useful. If this returns false, no
     * search will ever be attempted.
     * 
     * @return true if search is enabled
     */
    boolean canSearch();

    /**
     * Return all subauthorities, if any. If these exist, the authority ID cannot be
     * used directly. Each subauthority handles one specific abstract identity.
     * 
     * @return
     */
    List<String> getAuthorityIds();
    
    /**
     * Passed the concept being aliased to the identity passed. If this is OK return null; otherwise
     * return an error message to be displayed to the user. Do not throw exceptions.
     * 
     * @param knowledge
     * @param id 
     * @return something weird
     */
    String validateIdentifiedConcept(IConcept knowledge, String id);
}
