/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.knowledge;

import java.util.Collection;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.IContext;

/**
 * The product of contextualization. Observations are continuants whose type is one of the
 * observation types; the type of the observed object is {@link IObservable#getType()} on
 * the observable returned by {@link #getObservable()}.
 * 
 * 
 * 
 * @author Ferd
 */
public interface IObservation extends IIndividual {

    /**
     * Return the observable, including the main type and the observation type.
     * 
     * @return the observation's observable
     */
    IObservable getObservable();

    /**
     * Return the scale seen by this object, merging all the extents declared for the
     * subject in the observation context.
     *
     * @return the observation's scale
     */
    IScale getScale();

    /**
     * Observation may have been made in the context of another direct observation. This
     * will always return non-null in indirect observations, and may return null in direct
     * ones when they represent the "root" context.
     * 
     * @return the context for the observation, if any.
     */
    IDirectObservation getContextObservation();

    /**
     * The overall "world" that this observation is part of, with the root subject at the
     * top and all informations about resolution and provenance.
     * 
     * @return
     */
    IContext getContext();

    /**
     * The subject observation that contextualized this observation. This is not the same
     * as the context observation: it allows recording different viewpoints on
     * observations that are contextual to the same observable - e.g. qualities of the
     * same subject seen by different child subjects in it. If null, this was made by the
     * "root subject" that represents the session user. Later we may create a subject to
     * represent the session user.
     * 
     * @return the subject that provides the viewpoint for this observation, or null if
     *  this was a user-made observation.
     */
    ISubject getObservingSubject();
    
//    
//    /**
//     * 
//     * @return
//     */
//    Collection<IConcept> getRoles(IObservation context);
//    
//       
//    /**
//     * 
//     * @param role
//     * @param context
//     * @return
//     */
//    boolean hasRole(IConcept role, IObservation context);

}
