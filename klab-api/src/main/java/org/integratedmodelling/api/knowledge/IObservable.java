package org.integratedmodelling.api.knowledge;

import java.util.Collection;

import org.integratedmodelling.api.modelling.IObservableSemantics;

/**
 * An IObservable is the individual observed by an IObservation. When it exists
 * it's guaranteed to be immutable, semantically consistent and fully contextualized.
 * It can answer questions about its role in its context.
 *  
 * @author Ferd
 *
 */
public interface IObservable extends IIndividual {

	/**
	 * 
	 * @return
	 */
	IObservableSemantics getSemantics();
	
	/**
	 * 
	 * @return
	 */
	IObservation getObservation();
	
	/**
	 * 
	 * @return
	 */
	IObservable getContext();


	/**
	 * Being a compatible observable means that this isa obs and for each
	 * trait T in this, obs inherits a trait that isa T.
	 * 
	 * @param context
	 * @return
	 */
	boolean isCompatible(ISemantic obs);

	
	/**
	 * 
	 * @param trait
	 * @return
	 */
	boolean hasTrait(IConcept trait);
	
	/**
	 * 
	 * @param attribute
	 * @return
	 */
	boolean hasAttribute(IConcept attribute);
	
	/**
	 * 
	 * @param realm
	 * @return
	 */
	boolean hasRealm(IConcept realm);
	
	/**
	 * 
	 * @param identity
	 * @return
	 */
	boolean hasIdentity(IConcept identity);
}
