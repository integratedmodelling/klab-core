package org.integratedmodelling.api.knowledge;

import java.util.Collection;

import org.integratedmodelling.api.lang.IMetadataHolder;

/**
 * The individual (instance). In k.LAB, the important individuals are observations and
 * observables.
 * 
 * @author Ferd
 *
 */
public interface IIndividual extends ISemantic, IMetadataHolder {
	
	Collection<IIndividual> getIndividuals(IProperty property);

	Collection<Object> getData(IProperty property);
	
	Collection<IProperty> getObjectRelationships();

	Collection<IProperty> getDataRelationships();
	
	boolean is(ISemantic type);
}
