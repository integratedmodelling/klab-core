package org.integratedmodelling.api.knowledge;

import java.util.Collection;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A knowledge manager is expected to manage a knowledge index over the knowledge
 * it manages.
 * 
 * @author Ferd
 *
 */
public interface IKnowledgeIndex {

    /** 
     * Clear the index. Do not remove the indexed objects from the
     * list.
     */
    void clear();

    /**
     * Reindex all the knowledge we added with {@link #index(Object)}. Do not clear
     * anything that was not related to a previous version of the reindexed objects.
     */
    void reindex();

    /**
     * Schedule whatever is passed for indexing, looking up an adapter according
     * to what the object is. If no adapter can be found, ignore silently.
     * 
     * Should expect duplicate submissions and be able to overwrite and schedule
     * reindexing only if things that have changed.
     * 
     * Doesn't do anything to the actual index unless reindex() is called.
     * 
     * @param object
     */
    void index(Object object);

    /**
     * Run a query and return only metadata for the objects that match. Ideally
     * a lazy collection. Metadata can later be passed to retrieveObject() to
     * recreate the object indexed, again using adapters.
     * 
     * Will be used incrementally, so this should be very fast and return
     * the essential metadata only.
     * 
     * @param query
     * @return a collection of metadata object describing the results, best first.
     * @throws KlabException
     */
    Collection<IMetadata> search(String query) throws KlabException;
    
    /**
     * Remove all indexed objects belonging to passed namespace.
     * 
     * @param namespaceId
     */
    void deleteNamespace(String namespaceId);
}
