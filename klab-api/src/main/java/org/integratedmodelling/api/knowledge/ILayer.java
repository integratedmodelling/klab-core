package org.integratedmodelling.api.knowledge;

/**
 * Layers are collections of objects that share a (possibly independent) topology and
 * implement operations on it. Mostly intended for spatial implementations, although that
 * may be more general. Spatial subjects have a layer collection linked to concepts,
 * either for observations (automatically notified to the layer) or for realms, in which
 * case the layer represents realm-specific configurations and supports models in the
 * realm. Layers also get notified of other layers in the subject, so they can register
 * with them, enabling vertical transport or other operations.
 * 
 * @author Ferd
 *
 */
public interface ILayer {

    /**
     * Return the domain concept for this layer, usually space but not necessarily. May be
     * null.
     * 
     * @return
     */
    public IConcept getDomainConcept();

    /**
     * 
     * @return
     */
    public IConcept getConcept();

    /**
     * Any observation that is about this layer (e.g. processes in a realm or subjects
     * that are observations of the concept returned by {@link #getConcept()} are notified
     * after resolution.
     * 
     * @param observation
     */
    public void addObservation(IObservation observation);

    /**
     * 
     * @param layer
     */
    public void syncWith(ILayer layer);

}
