package org.integratedmodelling.api.data;

public interface IProbabilityDistribution {

    double[] getData();

    double[] getRanges();

    double getMean();

    double getUncertainty();

}
