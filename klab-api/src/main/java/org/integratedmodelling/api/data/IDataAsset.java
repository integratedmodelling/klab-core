/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.data;

import java.net.URL;
import java.util.Collection;

import org.integratedmodelling.api.configuration.IServiceConfiguration;

/**
 * Wraps a local resource under an object that can produce a URL and few other data when
 * an indirect resource service uses a URN that points to this.
 * 
 * In k.LAB, these are resolved from URNs by URNResolver and consumed by
 * IndirectResourceService (both interfaces not exposed in the API) with the final purpose
 * of providing resources to be used through the /get endpoint.
 * 
 * The API only exposes the information required to make use of the data; no other info is
 * required, and implementations can add all the bookkeeping info they wish in the
 * implementation.
 * 
 * @author Ferd
 *
 */
public interface IDataAsset {

    /**
     * Username of creator, or null. If this matches the current user, the groups are not
     * checked and access is allowed.
     * 
     * @return username of creator
     */
    String getCreatorUsername();

    /**
     * Groups that this resource is restricted to. No groups means public.
     * 
     * @return the allowed groups
     */
    Collection<String> getAllowedGroups();

    /**
     * Return a URL for the resource that will produce the resource according to the needs
     * of the passed service type.
     * 
     * FIXME this should be removed and the service pointed to by getServiceKey should be
     * enough to access any URLs we need.
     * 
     * @param serviceType
     * @return a URL or null if the service is unknown.
     */
    URL getResourceUrl(String serviceType);

    /**
     * The service key that identifies the particular service to direct actual requests
     * to. This makes getResourceUrl() unnecessary (the URL should be obtainable, with
     * knowledge of the implementation, through getNamespace() and getResourceId()) but we
     * leave it for legacy support.
     * 
     * @return the service key that resolves the service with
     *         {@link IServiceConfiguration}
     */
    String getServiceKey();

    /**
     * An ID for the resource, such as file name or layer name.
     * 
     * @return the resource ID
     */
    String getResourceId();

    /**
     * A namespace for the resource, if applicable.
     * 
     * @return namespace ID or null.
     */
    String getResourceNamespace();

    /**
     * All the service types that this resource can be asked by.
     * 
     * @return the applicable type constants, matching the services that can handle it.
     */
    Collection<String> getResourceTypes();
}
