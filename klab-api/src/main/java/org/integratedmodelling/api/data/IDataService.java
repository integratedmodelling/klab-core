package org.integratedmodelling.api.data;

import java.util.List;

import org.integratedmodelling.api.configuration.IServiceConfiguration;
import org.integratedmodelling.api.network.API;

/**
 * Initial API for services capable of providing resources through the /get endpoint. The
 * service key in a {@link IDataAsset} links to one of these, resolved by the installed
 * {@link IServiceConfiguration}. Some services that need configuration or access beyond
 * resource URNs can also be talked to using a {@link API#GET_RESOURCE} call that uses a
 * URN in the form
 * 
 * <pre>
 * &lt;node-id&gt;:service:&lt;service-type&gt:&lt;service-key&gt
 * </pre>
 * 
 * A URL using such a pattern can be configured as the proxy address for the software
 * implementing the service, so that calls from external clients can be proxied through
 * the k.node without exposing the server.
 * 
 * @author ferdinando.villa
 *
 */
public interface IDataService {

    /**
     * Service name that can be presented to users in case a choice of services is
     * available. Must be unique in a node.
     * 
     * @return the service name.
     */
    String getName();

    /**
     * @return the URL for the service. May be null if the service is not network-based.
     */
    String getUrl();

    /**
     * Class of service provider, such as "geoserver". Should refer to the specific
     * implementation providing a service, not the service itself (so 'geoserver', not
     * 'wcs'). Used for cross-validation in those URNs that are used for internal
     * communication with the server and are not exposed to users.
     * 
     * @return the service class, one word suitable for use in a URN.
     */
    String getType();

    /**
     * The primary key to access the service. Not seen by users but needed to enable
     * internal communication with servers. The IDataAsset pointed to by a resource URN
     * should return this. Knowing the key potentially enables service configuration
     * through the web, so it should never be exposed outside of the node-server circuit.
     * 
     * @return the service key.
     */
    String getKey();

    /**
     * If the service requires credentials to use it, return them here (typical case is
     * username/password, but anything the server digests is fine). The handler of the
     * service type (selected by the service name in {@link API#GET_RESOURCE} call) will
     * use these as necessary.
     * 
     * @return credentials if needed, null if not.
     */
    List<String> getAuthenticationCredentials();

}
