package org.integratedmodelling.api.data;

public interface IExport {

    static public enum Format {
        RASTER_MAP, OBJECT_COLLECTION_FILE, STATE_COLLECTION_FILE, SQL, RDF, CSV, SCIENTIFIC_DATASET, FILE_COLLECTION, VIDEO, OWL, GRAPH, PDF, EVERYTHING, NETCDF, SITE, KBOX
    }

    static public enum Aggregation {
        TOTAL, AVERAGE
    }

}
