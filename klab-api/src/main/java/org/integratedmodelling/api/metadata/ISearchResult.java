package org.integratedmodelling.api.metadata;

import java.util.List;

/**
 * Attempt to generalize the least generalizable of problems.
 * 
 * @author ferdinando.villa
 *
 * @param <T>
 */
public interface ISearchResult<T> {

    /**
     * Get the matching objects. Possibly empty, never null.
     * 
     * @return
     */
    List<T> getMatches();

    /**
     * Returns -1 if the result is not paged, i.e. all possible matches are contained
     * here.
     * 
     * @return
     */
    int getPage();

    /**
     * Call if getPage() != -1 to know whether there are other pages beyond this one.
     * 
     * @return
     */
    boolean isMoreMatches();

}
