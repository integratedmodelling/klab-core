/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.metadata;

import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.monitoring.IMonitor;

/**
 * This is implemented by remote query services when observations can be queried
 * and returned but we want to check their description before creating an actual
 * observer, so that potentially expensive operations can be delayed. Analogous
 * to {@link IModelMetadata} for direct observations.
 * 
 * @author ferdinando.villa
 *
 */
public interface IObservationMetadata {

	/**
	 * Observable. Parsed on demand please.
	 * 
	 * @return the observable concept
	 */
	IConcept getConcept();

	/**
	 * Individual observer ID (without namespace)
	 * 
	 * @return the observer ID
	 */
	String getId();

	/**
	 * Fully qualified name of observer.
	 * 
	 * @return the fully qualified name
	 */
	String getName();

	/**
	 * Name of node holding the observer. If local engine, the name will be the
	 * locking user name, also available as Env.NAME in both client and local
	 * server.
	 * 
	 * @return the node this comes from
	 */
	String getNodeId();

	/**
	 * Create and return a direct observer corresponding to the metadata.
	 * 
	 * @param monitor
	 * @return a new subject observer based on these metadata.
	 */
	IDirectObserver getSubjectObserver(IMonitor monitor);

	
	/**
	 * Attributes of any kind
	 * @return
	 */
	Map<String, Object> getAttributes();

}
