package org.integratedmodelling.api.metadata;

import java.util.Map;

public interface IDocumentable {
    
	/**
	 * Return the Markdown documentation of the implementing object for
	 * the requested section, using a documentation template if available.
	 * 
	 * If no content should be provided for the section, return null.
	 * 
	 * @param section
	 * @return
	 */
	String document(String section);
	
	/**
	 * Return the appropriate template for this object.
	 * 
	 * @return
	 */
	IDocumentation getTemplate();
	
	/**
	 * Return all the parameters set for the object. If no parameters
	 * are set, return an empty map, not null.
	 * 
	 * @return
	 */
    Map<String, Object> getParameters();
    
    /**
     * Set a parameter in the map that getParameters() will
     * return. Parameters will be used in the template.
     * 
     * @param key
     * @param value
     */
    void setParameter(String key, Object value);
    

}
