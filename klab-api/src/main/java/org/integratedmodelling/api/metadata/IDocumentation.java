package org.integratedmodelling.api.metadata;

import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAction.Trigger;

/**
 * A documentation object is the translation of the 'with documentation' metadata. It
 * consists of a set of templates, associated to tags or action types, each composed of a
 * list of calls to the reporting system. Each structure can be translated into the Groovy
 * language that will do the actual reporting during contextualization.
 * 
 * Documentation implementations are specific to an action language and the calling system
 * must ensure they are chosen correctly.
 * 
 * @author ferdinando.villa
 *
 */
public interface IDocumentation {

    /**
     * Each template is a list of sections, each of which gets ultimately translated in
     * calls to the reporting system. Such calls can be direct (using @<call>() format),
     * indirect (using the GString template system) or be [] expressions in the template
     * language, preprocessed for @ calls and inserted in the action code as they are.
     * 
     * @author ferdinando.villa
     *
     */
    public static interface Template {

        /**
         * Each section is a template element of a single type.
         * 
         * @author ferdinando.villa
         *
         */
        public static interface Section {

            public static enum Type {
                
                /**
                 * string reported as-is, inheriting any templating facilities from the
                 * host action language.
                 */
                TEMPLATE_STRING,
                
                /**
                 * Action code, referenced in brackets in the documentation text, and inserted
                 * as-is in action code after documentation-specific preprocessing and before
                 * action preprocessing.
                 */
                ACTION_CODE,
                
                /**
                 * Call to the reporting system, referenced using annotation language (@) and
                 * translated into the correspondent call in the action implementation.
                 */
                REPORT_CALL
            }

            /**
             * Type of the section, determining the type of translation made.
             * 
             * @return
             */
            Type getType();

            /**
             * Body of the section, ready for inclusion in action language.
             * 
             * @return
             */
            String getCode();
            
        }

        /**
         * Return all the sections in declaration order.
         * 
         * @return
         */
        List<Section> getSections();

        /**
         * Translate the list of section into actionable instructions in the supported
         * action language.
         * 
         * @param actionLanguage
         * @return
         */
        String getActionCode();

    }

    /**
     * Get the template corresponding to the passed action type, if any.
     * 
     * @param actionType
     * @return
     */
    Template get(IAction.Trigger actionType);

    /**
     * Get the template corresponding to the passed tag, if any.
     * 
     * @param tag
     * @return
     */
    Template get(String tag);
    
    /**
     * Return all non-action tags defined. For those, the corresponding
     * templates will most likely contain simply text.
     * 
     * @return
     */
    Collection<String> getTags();

    /**
     * Return all the action triggers defined.
     * 
     * @return
     */
    Collection<Trigger> getTriggers();
    
    /**
     * Check that this returns an empty list before using; if not, report
     * errors.
     * 
     * @return
     */
    List<String> getErrors();


}
