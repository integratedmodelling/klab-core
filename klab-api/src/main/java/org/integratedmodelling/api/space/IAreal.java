package org.integratedmodelling.api.space;

import org.integratedmodelling.api.modelling.IDimensional;

/**
 * Tag interface that will assign 2-d spatial nature to a class, so that 
 * inferences can be made when checking units or other types of use.
 * 
 * @author ferdinando.villa
 *
 */
public interface IAreal extends IDimensional {
    static int getDimensionCount() {
        return 2;
    }
}
