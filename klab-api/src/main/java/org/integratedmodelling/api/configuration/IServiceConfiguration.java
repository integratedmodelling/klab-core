package org.integratedmodelling.api.configuration;

import org.integratedmodelling.api.data.IDataService;

/**
 * Service configuration. In most cases a simple wrapper over some bean directory, but could be
 * more complex.
 * 
 * @author ferdinando.villa
 *
 */
public interface IServiceConfiguration {

    /**
     * Return the service corresponding to the key.
     * 
     * @param key
     * @return the data service corresponding to the key, or null.
     */
    IDataService getDataService(String key);

    /**
     * If a default service is supported, return it. This is meant to accommodate 
     * resource descriptors for systems that are not equipped to handle configurable
     * services.
     * 
     * @return the default data service, or null.
     */
    IDataService getDefaultService();
}
