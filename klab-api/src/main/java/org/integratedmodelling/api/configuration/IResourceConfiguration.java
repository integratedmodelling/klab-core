package org.integratedmodelling.api.configuration;

import java.util.Collection;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Configuration of available resources for an engine and access to the correspondent
 * authorizations.
 * 
 * @author ferdinando.villa
 *
 */
public interface IResourceConfiguration {

    /*
     * TODO will need to be made a string because of the need to extend services
     * unfortunately.
     * TODO look at typesafe enum pattern
     */
    public enum StaticResource {
        OBSERVATION_SUBMIT,
        OBSERVATION_QUERY,
        MODEL_QUERY,
        EXPLORER,
        ADMINISTRATION,
        ENGINE_LOCK,
        WCS, // TODO remove, add it externally
        WFS  // TODO remove, add it externally
    }

    /**
     * @return the list of components this engine is handling.
     */
    Collection<String> getComponentIds();

    /**
     * @return the set of shared projects this engine is handling.
     */
    Collection<String> getSharedProjectIds();

    /**
     * @return the set of synchronized projects this engine is handling.
     */
    Collection<String> getSynchronizedProjectIds();
    
    
    /**
     * Static resources provided (services etc)
     * @return the set of resources provided
     */
    Collection<StaticResource> getStaticResources();

    /**
     * @param user
     * @param clientAddress
     * @return true if authorized
     */
    boolean isAuthorized(IUser user, String clientAddress);

    /**
     * @param username
     * @param groups
     * @param clientAddress
     * @return true if authorized
     */
    boolean isAuthorized(String username, Set<String> groups, String clientAddress);

    /**
     * @param project
     * @param user
     * @param clientAddress
     * @return true if authorized
     */
    boolean isAuthorized(IProject project, IUser user, String clientAddress);

    /**
     * @param project
     * @param username
     * @param groups
     * @param clientAddress
     * @return true if authorized
     */
    boolean isAuthorized(IProject project, String username, Set<String> groups, String clientAddress);

    /**
     * @param component
     * @param user
     * @param clientAddress
     * @return true if authorized
     */
    boolean isAuthorized(IComponent component, IUser user, String clientAddress);

    /**
     * @param component
     * @param username
     * @param groups
     * @param clientAddress
     * @return true if authorized.
     */
    boolean isAuthorized(IComponent component, String username, Set<String> groups, String clientAddress);

    /**
     * 
     * @param resource
     * @param user
     * @param clientAddress
     * @return true if given resource is authorized
     */
    boolean isAuthorized(StaticResource resource, IUser user, String clientAddress);

    /**
     * 
     * @param resource
     * @param username
     * @param groups
     * @param clientAddress
     * @return true if given resource is authorized
     */
    boolean isAuthorized(StaticResource resource, String username, Set<String> groups, String clientAddress);

    /**
     * If the access property define an administration key, return it - it will enable external
     * admin operations without session.
     * @return the administration key or null.
     */
    String getAdministrationKey();

    /**
     * Ensure that any resource defined through remote URLs is up to date. Return true if
     * anything was updated.
     * @throws KlabException 
     * 
     */
    boolean updateRemoteProjects() throws KlabException;

    
}
