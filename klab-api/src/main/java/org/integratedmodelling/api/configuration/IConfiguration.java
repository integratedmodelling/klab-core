/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.configuration;

import java.io.File;
import java.util.Properties;

import org.integratedmodelling.collections.OS;

/**
 * This interface tags objects that have configuration properties and need
 * workspace areas, such as sessions, project and plugins. Objects implementing
 * this interface can have installation directories and must have workspaces,
 * temp areas and scratch areas.
 * 
 * @author Ferd
 * 
 */
public interface IConfiguration {

	public static final String KLAB_CLIENT_PROJECTS = "klab.client.workspace";

	/**
	 * 
	 */
	public static final String KLAB_OFFLINE = "klab.offline";

	/**
	 * 
	 */
	public static final String KLAB_USE_CONTEXT_QUALITIES = "klab.use.context.qualities";

	/**
	 * 
	 */
	public static final String KLAB_EXPORT_PATH = "klab.export.path";

	/**
	 * 
	 */
	public static final String KLAB_REASONING = "klab.reasoning";

	/**
	 * 
	 */
	public static final String KLAB_CLIENT_DEBUG = "thinklab.client.debug";

	/**
	 * 
	 */
	public static final String CERTFILE_PROPERTY = "klab.certificate";

	/**
	 * 
	 */
	public static final String KLAB_SOURCE_DISTRIBUTION = "thinklab.source.distribution";

	/**
	 * 
	 */
	public static final String KLAB_CONNECTION_TIMEOUT = "klab.connection.timeout";

	// use these subspaces within dataDir, for consistency.
	/**
	 * 
	 */
	String SUBSPACE_KNOWLEDGE = "knowledge";
	/**
	 * 
	 */
	String SUBSPACE_SOURCE = "src";
	/**
	 * 
	 */
	String SUBSPACE_LIB = "lib";
	/**
	 * 
	 */
	String SUBSPACE_WORKSPACE = "workspace";
	/**
	 * 
	 */
	String SUBSPACE_INDEX = "index";

	/*
	 * these properties can be set to define what states to store during
	 * contextualization when the defaults are inadequate. They're mostly
	 * unsupported at this time.
	 */
	/**
	 * 
	 */
	public static final String KLAB_STORE_RAW_DATA = "klab.store.raw";
	/**
	 * 
	 */
	public static final String KLAB_STORE_INTERMEDIATE_DATA = "klab.store.intermediate";
	/**
	 * 
	 */
	public static final String KLAB_STORE_CONDITIONAL_DATA = "klab.store.conditional";
	/**
	 * 
	 */
	public static final String KLAB_STORE_MEDIATED_DATA = "klab.store.mediated";

	/**
	 * Minutes after which a session times out. Default 60.
	 */
	public static final String KLAB_SESSION_TIMEOUT_MINUTES = "klab.session.timeout";

	/**
	 * Absolute path of work directory. Overrides the default which is
	 * ${user.home}/THINKLAB_WORK_DIRECTORY
	 */
	public static final String KLAB_DATA_DIRECTORY = "klab.data.directory";

	/**
	 * Name of work directory relative to ${user.home}. Ignored if
	 * THINKLAB_DATA_DIRECTORY_PROPERTY is specified.
	 */
	public static final String KLAB_WORK_DIRECTORY = "klab.work.directory";

	/**
	 * Points to a comma-separated list of directories where components are
	 * loaded from their Maven development tree.
	 */
	public static final String KLAB_LOCAL_COMPONENTS = "klab.local.components";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_CERTIFICATE = "klab.engine.certificate";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_DATADIR = "klab.engine.datadir";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_DEBUG_PORT = "klab.engine.debugPort";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_USE_DEBUG = "klab.engine.useDebug";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_KLAB_DEBUG = "klab.engine.klabDebug";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_USE_DEVELOPER_NETWORK = "klab.engine.useDeveloperNetwork";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_USE_LOCAL_INSTALLATION = "klab.engine.useLocalInstallation";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_SHUTDOWN_ON_EXIT = "klab.engine.shutdownOnExit";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_UPGRADE_AUTOMATICALLY = "klab.engine.upgradeAutomatically";

	/**
	 * 
	 */
	public static final String KLAB_ENGINE_LAUNCH_AUTOMATICALLY = "klab.engine.launchAutomatically";

	/**
	 * If true, instructs some calls to spoof the _dev URLs of development
	 * network nodes to look like the official one (without the suffix returned
	 * by {@link #getDeveloperNetworkURLPostfix()}). Used when external services
	 * are configured with the official node URL and the modified URL gets in
	 * the way.
	 */
	public static final String KLAB_SPOOF_DEV_URL = "klab.engine.spoofdevurl";

	/**
	 * Configurable objects should have properties that can be persisted across
	 * invocations.
	 * 
	 * @return the properties.
	 */
	Properties getProperties();

	/**
	 * the data path is the root of the other system-oriented paths, which are
	 * assumed not to be relevant to the user. Used by both client and server.
	 * 
	 * @return the work directory.
	 */
	File getDataPath();

	/**
	 * A scratch area is for places where stuff that's not going to be
	 * understandable to the user will get written. It should remain there
	 * across sessions.
	 * 
	 * @return a valid, writable scratch path.
	 */
	File getScratchArea();

	/**
	 * A scratch area is for places where stuff that's not going to be
	 * understandable to the user will get written. It should remain there
	 * across sessions. Use this one for specific subdirectories.
	 * 
	 * @param subArea
	 * 
	 * @return a valid, writable scratch path.
	 */
	File getScratchArea(String subArea);

	/**
	 * A temp area is like a scratch path but the object implementing this
	 * configuration should ensure that it's removed either at the end of a
	 * session or at the beginning of a new one, ideally at both points.
	 * 
	 * @param subArea
	 * @return a temporary area that will be removed after exit.
	 */
	File getTempArea(String subArea);

	/**
	 * Persist the properties after a change.
	 */
	void save();

	/**
	 * The OS we run on.
	 * 
	 * @return the OS
	 */
	OS getOS();

	/**
	 * True if debugging mode has been enabled.
	 * 
	 * @return whether debugging is active
	 */
	boolean isDebug();

	/**
	 * True if offline mode is enabled. Means we're not connecting to the
	 * k-network, not that we're actually offline.
	 * 
	 * @return whether we are offline or not.
	 */
	boolean isOffline();

	/**
	 * A subdir of the main work directory for this engine, created if
	 * necessary.
	 * 
	 * @param string
	 * @return an existing directory inside the data path.
	 */
	File getDataPath(String string);

	/**
	 * Only the name of the main work directory, expected to be found under
	 * $HOME in personal engines and clients.
	 * 
	 * @return the name (not the path) of the main work directory.
	 */
	String getWorkDirectoryName();

	/**
	 * If true, an OWL reasoner will be connected to the OWL model and kept
	 * updated. This will cause much more sensible operation, may be
	 * counterindicated on slow machines with less memory.
	 * 
	 * @return
	 */
	boolean useReasoner();

	/**
	 * Default export path if any.
	 * 
	 * @return
	 */
	File getDefaultExportPath();

	/**
	 * If true, user asked to be logged on to the developer network.
	 * 
	 * @return
	 */
	boolean useDeveloperNetwork();

	/**
	 * Get the postfix to append to a URL for a development network primary
	 * server.
	 * 
	 * @return
	 */
	String getDeveloperNetworkURLPostfix();

	/**
	 * If true, qualities from the context are used to resolve qualities for compatible
	 * direct observations in it. If not, qualities are resolved using normal model lookup.
	 * The advantage is speed, at the expense of proper resolution.
	 * 
	 * On by default.
	 * 
	 * @return
	 */
	boolean useContextQualities();

}
