package org.integratedmodelling.api.auth;

/**
 * Any identity known to the IM semantic web. Since 0.9.11 identities are arranged in
 * a parent/child hierarchy through exposing their parent token, which is only null
 * in top-level identities, i.e. IServer. Identity objects are passed to the
 * API in lieu of their raw tokens, to give quick access to the identity's metadata
 * and their lineage.
 * 
 * Identities also correspond to roles for Spring security in the kmodeler and klab-community
 * projects.
 * 
 * Identities for now have the following parent/child relationships:
 * 
 * <pre>
 * 	IIdentity (abstract)
 * 		IServer
 * 			IUser (authenticated by IServer, directly or indirectly)
 * 				INetworkSession
 *              	IEngine (has a IUser (automatically promoted to IEngineUser) but can authenticate others as IEngineUser)
 *              		IEngineUser
 * 							ISession
 * 				     			IContext
 * 									ITask
 * </pre>
 * 
 * @author Ferd
 *
 */
public abstract interface IIdentity {
	
	static enum Type {
		SERVER,
		IM_USER,
		NETWORK_SESSION,
        ENGINE,
        ENGINE_USER,
		MODEL_SESSION,
		CONTEXT,
		TASK
	}
		
	/**
	 * Authorization token retrieved upon authentication. Assumed to expire at
	 * some sensible point in time, if stored it should be validated before use
	 * and refreshed if necessary.
	 * 
	 * @return a token to use as authentication when dealing with the engine.
	 */
	String getSecurityKey();
	
	/**
	 * Get the parent identity of the passed type.
	 * 
	 * @return the desired identity or null.
	 */
	<T extends IIdentity> T getParentIdentity(Class<? extends IIdentity> type);
}
