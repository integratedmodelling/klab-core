/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.api.auth;

import java.util.Date;
import java.util.Set;

/**
 * Describes a user in any authenticated session. Implementations can provide
 * their own validation processing. There are two implementations: one for the
 * server (which has life-or-death rights over user records) and one for the
 * client (where users can only be defined through certificates or be
 * anonymous).
 * 
 * @author Ferd
 *
 */
public interface IUser extends IIdentity {

	/**
	 * Status of a user wrt. the network. Starts at UNKNOWN.
	 */
	public enum Status {
		/**
		 * User is authenticated locally but not online, either for lack of
		 * authentication or lack of network connection/
		 */
		OFFLINE,
		/**
		 * User is authenticated and online with the network.
		 */
		ONLINE,
		/**
		 * User has not been submitted for authentication yet.
		 */
		UNKNOWN
	}

	/**
	 * points to the public key filename in the global Configuration properties.
	 */
	public static final String THINKLAB_PUBLIC_KEY_PROPERTY = "thinklab.public.key";

	/**
	 * ID for an anonymous user. Unsurprising.
	 */
	public static final String ANONYMOUS_USER_ID = "anonymous";

	/*
	 * Keys for user properties in certificates or for set operations.
	 */
	public static final String REALNAME = "realname";
	public static final String FIRSTNAME = "firstname";
	public static final String EMAIL = "email";
	public static final String LASTNAME = "lastname";
	public static final String INITIALS = "initials";
	public static final String PHONE = "phone";
	public static final String ADDRESS = "address";
	public static final String AFFILIATION = "affiliation";
	public static final String JOBTITLE = "jobtitle";
	public static final String COMMENTS = "comments";
	public static final String SKEY = "key";
	public static final String USER = "user";
	public static final String ROLES = "roles";
	public static final String GROUPS = "groups";
	public static final String MODULES = "modules";
	public static final String EXPIRY = "expiry";
	public static final String SERVER = "primary.server";

	public static final String ROLE_ADMINISTRATOR = "ADMIN";
	public static final String GROUP_ADMINISTRATORS = "ADMINISTRATORS";
	public static final String GROUP_DEVELOPERS = "DEVELOPERS";
	public static final String GROUP_IM = "IM";

	/**
	 * Never null, may be ANONYMOUS_USER_ID when isAnonymous() returns true.
	 * 
	 * @return the username
	 */
	String getUsername();

	/**
	 * List roles for this user. So far, only role of relevance is ADMIN - roles
	 * are just strings, stored at server side and defined at authenticate().
	 * Roles do not imply assets but are checked for privileged operations.
	 * 
	 * @return a set of role names
	 */
	Set<String> getRoles();

	/**
	 * List groups the user belongs to. At client side, only non-empty after
	 * authenticate() is successful.
	 * 
	 * @return a set of group names
	 */
	Set<String> getGroups();

	/**
	 * True after the empty constructor is used. In this situation, isOnline()
	 * is false, the assets are empty (not null) and the username is
	 * "anonymous".
	 * 
	 * @return whether the user is anonymous and unauthenticated.
	 */
	boolean isAnonymous();

	/**
	 * Primary server URL, harvested from the group set at the server side and
	 * stored in the certificate. One of three bits of data that the certificate
	 * contains along with username and email (plus optionally basic anagraphic
	 * data for pretty-printing at client side). This one can be null (in
	 * anonymous and unprivileged users). If null, everything works but the
	 * whole system is essentially a self-contained sandbox.
	 * 
	 * @return the URL of the primary server for the user.
	 */
	String getServerURL();

	/**
	 * Never null - users cannot be created at server side without an email
	 * address.
	 * 
	 * @return the user's email address.
	 */
	String getEmailAddress();

	/**
	 * May be null if user has been created in non-standard ways.
	 * 
	 * @return user stated first name.
	 */
	String getFirstName();

	/**
	 * May be null if user has been created in non-standard ways.
	 * 
	 * @return user stated last name.
	 */
	String getLastName();

	/**
	 * OK, Anglo-saxons, have it your way. At least it can be null.
	 * 
	 * @return user middle initials, if any.
	 */
	String getInitials();

	/**
	 * Input by user at registration, possibly null.
	 * 
	 * @return user affiliation, if any.
	 */
	String getAffiliation();

	/**
	 * Return whatever further comments were entered by user at registration.
	 * 
	 * @return user-stated comments.
	 */
	String getComment();

	/**
	 * Date of last login for user. Should be kept up to date at server side
	 * when authorizing, and correspond to authentication time at client side.
	 * 
	 * @return date of last login.
	 */
	Date getLastLogin();

	/**
	 * False if the user is successfully certified (has a valid certificate) at
	 * the client side, but either the authentication with the primary server
	 * didn't work for any reason or the Thinklab is not accessible.
	 *
	 * @return online status of user.
	 */
	Status getOnlineStatus();

}
