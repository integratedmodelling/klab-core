package org.integratedmodelling.collections;

import java.util.Collection;
import java.util.Set;

public abstract class ImmutableSet<T> implements Set<T> {

    @Override
    public boolean add(T e) {
        throw new UnsupportedOperationException("cannot modify immutable set");
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        throw new UnsupportedOperationException("cannot modify immutable set");
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException("cannot modify immutable set");
    }


    @Override
    public boolean remove(Object o) {
        throw new UnsupportedOperationException("cannot modify immutable set");
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        throw new UnsupportedOperationException("cannot modify immutable set");
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException("cannot modify immutable set");
    }


    @Override
    public Object[] toArray() {
        Object[] ret = new Object[size()];
        int i = 0;
        for (Object o : this) {
            ret[i++] = o;
        }
        return ret;
    }

    @SuppressWarnings({ "unchecked", "hiding" })
    @Override
    public <T> T[] toArray(T[] a) {
        int i = 0;
        for (Object o : this) {
            a[i++] = (T)o;
        }
        return a;
    }

}
