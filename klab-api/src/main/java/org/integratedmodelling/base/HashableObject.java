/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.base;

/**
 * Derive objects from this to implement a global ID assignment that is guaranteed to
 * work in a hash and support equality checking. Works across threads - the thread-local
 * version is ThreadHashableObject.
 * 
 * Note: equals() will only return true in case of identity, i.e. the two objects are the
 * same object. So don't use it for anything that is treated like a literal.
 * 
 * @author Ferd
 *
 */
public class HashableObject {

    static long __ID = 0L;

    // Returns the current thread's unique ID, assigning it if necessary
    private synchronized static long nextId() {
        return __ID++;
    }

    protected Long __id = nextId();

    public int hashCode() {
        return __id.hashCode();
    }

    public boolean equals(Object o) {
        return o instanceof HashableObject && __id == ((HashableObject) o).__id;
    }
    
    /**
     * Use when you want another object to be equal to the original one. With care,
     * of course.
     * 
     * @param o
     */
    public void copyId(HashableObject o) {
       __id = o.__id; 
    }
}
