k.LAB component quickstart archetype
=========================================

Summary
-------
The project is a Maven archetype for a generic k.LAB component that makes
contributions in the form of observation contextualizers.

Generated project characteristics
-------------------------
* Component class and annotation generated automatically
* An example contextualizer is generated for each supported
  observable type and prototype annotations are provided to
  illustrate how k.IM bindings are created.

Installation
------------

All org.integratedmodelling artifacts are in Maven central, so there
is in principle no need to install anything as Maven should automatically 
find the archetype.

If for any reason you need to install the archetype in your local repository, execute:

```bash
    git clone https://bitbucket.org/ariesteam/klab-component-quickstart.git
    cd klab-component-quickstart
    mvn clean install
```


Create a project
----------------

```bash
    mvn archetype:generate \
        -DarchetypeGroupId=org.integratedmodelling \
        -DarchetypeArtifactId=klab-component-quickstart \
        -DarchetypeVersion=0.9.9 \
        -DgroupId=my.groupid \
        -DartifactId=my-artifactId \
        -Dversion=my.version \
        -DcomponentId=my.componentId
```

Run the project
----------------

```bash
	mvn install
```

Test
-------------------

Move the JAR file generated in target/ to the components/ directory of an existing
k.LAB node or engine, and check that the services provided are available in the
server capabilities.

Creating a new project in Eclipse
----------------------------------

* Create new Maven project and select the archetype (search for 'klab').

If installing locally:

* Import archetype URI by `Import ... > Projects from Git > Clone URI`
* Install the archetype in local repository with `mvn install`
* Go to `Preferences > Maven > Archetypes` and `Add Local Catalog`
* Select the catalog from file (`archetype-catalog.xml`) 
