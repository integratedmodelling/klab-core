
## Tuning

On linux, the default multicast buffer (used for client/engine communication) is often too small and this can lead to performance issues. If a notice to that extent appears in the klab.log file, it is advisable to add the following lines to /etc/sysctl.conf 

kernel.sem = 250 32000 100 128
kernel.shmall = 2097152
kernel.shmmax = 2147483648
kernel.shmmni = 4096
fs.file-max = 65536
vm.swappiness = 0
vm.vfs_cache_pressure = 50
net.core.rmem_max = 16777216
net.core.wmem_max = 16777216
net.ipv4.tcp_rmem = 4096 87380 16777216
net.ipv4.tcp_wmem = 4096 65536 16777216
net.ipv4.tcp_no_metrics_save = 1
fs.file-max = 100000
fs.inotify.max_user_watches = 100000


then run 'sudo sysctl -p' to update the setting. Be VERY careful with the file and always make a backup - a wrong character can make the system non-functional.

The above are meant for a wired connection with good performance and more than 512 MB of RAM. See http://www.ubuntugeek.com/performance-tuning-with-system-control-sysctl-in-ubuntu.html for more information and settings for different environments.
