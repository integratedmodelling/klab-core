package org.integratedmodelling.kmodeler.viewer;

import org.integratedmodelling.api.network.API;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

/**
 * Websockets configuration for the web-based viewer.
 * @author ferdinando.villa
 *
 */
@Configuration
@EnableWebSocketMessageBroker
public class ViewConfiguration extends AbstractWebSocketMessageBrokerConfigurer {

    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        // messages from us to JS use the channel /r/<sessionId>
        config.enableSimpleBroker("/session");
        // messages from JS to us are sent to "/klab/viewer" and have ViewerNotification payload
        config.setApplicationDestinationPrefixes("/klab");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint(API.VIEWER_ENDPOINT).withSockJS();
    }

}
