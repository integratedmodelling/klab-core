package org.integratedmodelling.kmodeler.components;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.beans.requests.SessionRequest;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Session;
import org.integratedmodelling.engine.modelling.runtime.EngineSession;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.kserver.controller.KServerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author ferdinando.villa
 */
@Service
public class SessionManager {

//    @Autowired
//    IUserRegistry                     userRegistry;

    @Autowired
    KServerController                serverController;

    /**
     * check on the status of sessions every this many seconds
     */
    private static final long        SESSION_REAPER_PERIOD_SECONDS = 60;

    /**
     * remove a session when this timeout has been reached. Overridden by configuration
     * property.
     */
    private static long              SESSION_TIMEOUT_MINUTES       = 60;

    private ScheduledExecutorService reaperExecutor;
    private Future<?>                reaperFuture;

    Map<String, List<Session>>       sessionsByUser                = new HashMap<>();
    Map<String, Session>             sessions                      = new HashMap<>();

    /**
     * @param authToken
     * @return a preexisting session with this token
     */
    public Session getReopenableSessionFor(String authToken) {
        synchronized (sessions) {
            if (sessionsByUser.containsKey(authToken)) {
                for (Session s : sessionsByUser.get(authToken)) {
                    if (s.isReopenable()) {
                        return s;
                    }
                }
            }
        }
        return null;
    }

    /**
     * @param sessionId
     * @return the session corresponding to this id.
     */
    public Session getSession(String sessionId) {
        synchronized (sessions) {
            return sessions.get(sessionId);
        }
    }

    @SuppressWarnings("javadoc")
    public Session getSession(Collection<String> sessionIds) {
        if (sessionIds == null) {
            return null;
        }
        synchronized (sessions) {
            for (String s : sessionIds) {
                Session ret = sessions.get(s);
                if (ret != null) {
                    return ret;
                }
            }
        }
        return null;
    }

    /**
     * Return a new session. If request is exclusive, we have already ascertained that the
     * user has permission.
     * 
     * @param request
     * @param user
     * @return a new session conforming to the request
     * @throws KlabException
     */
    public Session getNewSessionForRequest(SessionRequest request, IUser user)
            throws KlabException {

        Session ret = new EngineSession(user);

        synchronized (sessions) {
            serverController.getUserRegistry().register(user);
            sessions.put(ret.getId(), ret);
            List<Session> ls = sessionsByUser.get(user.getSecurityKey());
            if (ls == null) {
                ls = new ArrayList<>();
            }
            ls.add(ret);
            sessionsByUser.put(user.getSecurityKey(), ls);
        }

        ret.setLocalFilesystem(request.getClientSignature() != null
                && serverController.getLocalClientSignature().equals(request.getClientSignature()));

        KLAB.info("session " + ret.getId() + " created for " + user.getUsername() + " with "
                + (request.isRequestingExclusive() ? "full write" : "read-only") + " privileges");

        return ret;
    }

    /**
     * TODO session reaper - must be started and stopped properly 0. increment stats in
     * each session 1. reap sessions with configurable timeout (set to 1h?) 2. timeout for
     * locked sessions may be different - but they NEED to expire otherwise they never
     * will 3. warn if there is more than one exclusive session and collect general
     * statistics
     * 
     * @param sessionId
     */
    public void deleteSession(String sessionId) {
        // TODO remove from list as well as map
        KLAB.info("removing session " + sessionId);
        synchronized (sessions) {
        }
    }

    /**
     * 
     */
    public void startPolling() {

        if (KLAB.CONFIG.getProperties().containsKey(IConfiguration.KLAB_SESSION_TIMEOUT_MINUTES)) {
            SESSION_TIMEOUT_MINUTES = Long.parseLong(KLAB.CONFIG.getProperties()
                    .getProperty(IConfiguration.KLAB_SESSION_TIMEOUT_MINUTES));
        }

        if (SESSION_TIMEOUT_MINUTES == 0) {
            KLAB.info("session timeout disabled in configuration");
            return;
        }

        KLAB.info("session timeout is " + SESSION_TIMEOUT_MINUTES + " minutes, checked every "
                + SESSION_REAPER_PERIOD_SECONDS + " seconds");

        if (reaperFuture != null) {
            reaperFuture.cancel(false);
            reaperExecutor.shutdown();
        }

        reaperExecutor = Executors.newScheduledThreadPool(24);
        reaperFuture = reaperExecutor
                .scheduleWithFixedDelay(new SessionReaperService(), 0, SESSION_REAPER_PERIOD_SECONDS, TimeUnit.SECONDS);
    }

    /**
     * Stop the polling.
     */
    public void pausePolling() {

        if (reaperFuture != null) {
            reaperFuture.cancel(false);
            reaperExecutor.shutdown();
            reaperFuture = null;
        }
    }

    class SessionReaperService implements Runnable {

        @Override
        public void run() {

            long time = new Date().getTime();
            ArrayList<String> toReap = new ArrayList<>();
            synchronized (sessions) {
                for (String key : sessions.keySet()) {
                    Session session = sessions.get(key);
                    if ((time - session.getLastChecked()) > SESSION_TIMEOUT_MINUTES * 60L * 1000L) {
                        toReap.add(key);
                    }
                }
            }
            for (String key : toReap) {
                deleteSession(key);
            }
        }
    }

    @SuppressWarnings("javadoc")
    public IContext getContext(List<String> list) {
        for (String s : list) {
            IContext c = getContext(s);
            if (c != null) {
                return c;
            }
        }
        return null;
    }

    /**
     * Get a context from whatever session has it. FIXME use some catalog so that we don't
     * have to run long searches when we have 1000 sessions, although that's unlikely.
     * 
     * @param id context id.
     * @return the context, or null if not found.
     */
    public IContext getContext(String id) {

        synchronized (sessions) {
            for (Session s : sessions.values()) {
                IContext ctx = s.getContext(id);
                if (ctx != null) {
                    return ctx;
                }
            }
        }
        return null;
    }

}
