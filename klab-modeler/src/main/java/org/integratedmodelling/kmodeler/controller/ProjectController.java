package org.integratedmodelling.kmodeler.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.common.beans.Project;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabResourceNotFoundException;
import org.integratedmodelling.kmodeler.components.SessionManager;
import org.integratedmodelling.kserver.controller.AbstractProjectController;
import org.integratedmodelling.kserver.controller.KServerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ferdinando.villa
 */
@RestController
public class ProjectController extends AbstractProjectController {

	@Autowired
	SessionManager sessionManager;

	/*
	 * we only let one user clear the workspace once.
	 */
	private String workspaceClearedBy;

	/**
	 * @param forced
	 * @param headers
	 *            requires session authentication
	 * @param request
	 * @return ids of all namespaces that were modified
	 * @throws KlabException
	 */
	@RequestMapping(value = API.RELOAD, method = RequestMethod.POST)
	public @ResponseBody List<String> load(
			@RequestParam(value = "forced", required = false, defaultValue = "false") boolean forced,
			@RequestHeader HttpHeaders headers, HttpServletRequest request) throws KlabException {

		ISession session = sessionManager.getSession(headers.get(API.AUTHENTICATION_HEADER));
		if (session == null || !session.isExclusive()) {
			throw new KlabAuthorizationException("projects can only be loaded within an exclusive session");
		}

		/*
		 * FIXME this must be done within a Task
		 */
		List<String> ret = new ArrayList<>();
		for (INamespace ns : KLAB.PMANAGER.load(forced, KLAB.MFACTORY.getRootParsingContext())) {
			ret.add(ns.getId());
		}

		return ret;
	}

	/**
	 * @param id
	 * @param file
	 * @param headers
	 *            requires session-level authentication
	 * @param request
	 * @return the project descriptor after deployment
	 * @throws KlabException
	 */
	@RequestMapping(value = API.DEPLOY, method = RequestMethod.POST)
	public @ResponseBody Project deployProject(@PathVariable String id, @RequestParam("file") MultipartFile file,
			@RequestHeader HttpHeaders headers, HttpServletRequest request) throws KlabException {

		ISession session = sessionManager.getSession(headers.get(API.AUTHENTICATION_HEADER));
		if (session == null || !session.isExclusive()) {
			/**
			 * TODO see below for what to do. ALSO send error to session
			 * monitor.
			 */
			throw new KlabAuthorizationException("projects can only be loaded within an exclusive session");
		}

		File outfile = null;

		boolean reloadOthers = KLAB.PMANAGER.getProject(id) != null;

		if (file.isEmpty()) {
			throw new KlabIOException("empty content for deploy request: " + id);
		}
		try {
			// use a shortuuid to avoid the "prefix too short" problem with
			// project names like "im"
			outfile = File.createTempFile(NameGenerator.shortUUID(), "zip");
			byte[] bytes = file.getBytes();
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(outfile));
			stream.write(bytes);
			stream.close();
		} catch (Exception e) {
			throw new KlabIOException(e);
		}

		KLAB.info("deploy archive received for project " + id);
		IProject project = KLAB.PMANAGER.deployProject(id, outfile.toString(),
				((Monitor) KLAB.ENGINE.getMonitor()).get(session));

		if (project != null) {
			/*
			 * reload the entire thing unless the project is one we had not seen
			 * before. This is because we cannot easily know the
			 * interdependencies, particularly regarding composed concepts with
			 * traits from disparate projects.
			 */
			if (reloadOthers) {
				KLAB.PMANAGER.load(true, KLAB.MFACTORY.getRootParsingContext());
			} else {
				KLAB.PMANAGER.loadProject(project.getId(), KLAB.MFACTORY.getRootParsingContext());
			}
		}

		return KLAB.MFACTORY.adapt(KLAB.PMANAGER.getProject(id), Project.class);
	}

	/**
	 * Undeploy project.
	 * 
	 * @param id
	 * @param headers
	 * @throws KlabException
	 */
	@RequestMapping(value = API.UNDEPLOY, method = RequestMethod.DELETE)
	public void undeployProject(@PathVariable String id, @RequestHeader HttpHeaders headers) throws KlabException {
		ISession session = sessionManager.getSession(headers.get(API.AUTHENTICATION_HEADER));
		if (session == null || !session.isExclusive()) {
			/**
			 * TODO see below for what to do. ALSO send error to session
			 * monitor.
			 */
			throw new KlabAuthorizationException("projects can only be loaded within an exclusive session");
		}
		KLAB.PMANAGER.undeployProject(id);
	}

	/**
	 * Handle an update request - redefine or create a namespace by rewriting
	 * its full contents.
	 * 
	 * @param project
	 * @param id
	 * @param request
	 * @param kimContents
	 * @throws Exception
	 */
	@RequestMapping(value = API.UPDATE_NAMESPACE, method = RequestMethod.PUT)
	public void updateNamespace(@PathVariable String project, @PathVariable String id, HttpServletRequest request,
			@RequestBody String kimContents) throws Exception {

		ISession session = sessionManager.getSession(request.getHeader(API.AUTHENTICATION_HEADER));
		if (session == null || !session.isExclusive()) {
			/*
			 * TODO this must check if engine has been locked by same user as
			 * session.
			 */
			throw new KlabAuthorizationException("projects can only be loaded within an exclusive session");
		}

		IProject proj = KLAB.PMANAGER.getProject(project);
		if (proj == null) {
			throw new KlabResourceNotFoundException(
					"project " + project + " for namespace " + id + " not found: cannot update");
		}

		/*
		 * TODO write and reload ns
		 */
		File outfile = null;
		Collection<INamespace> dependents = null;
		INamespace namespace = KLAB.MMANAGER.getNamespace(id);
		if (namespace != null) {
			outfile = namespace.getLocalFile();
			dependents = KLAB.PMANAGER.getDependencyGraph().getDependents(namespace);
		} else {
			outfile = proj.findResourceForNamespace(id, true);
		}

		FileUtils.writeStringToFile(outfile, kimContents);

		if (dependents != null) {
			for (INamespace ns : dependents) {
				KLAB.PMANAGER.reloadNamespace(ns);
			}
		}

		KLAB.PMANAGER.loadProject(project, KLAB.MFACTORY.getRootParsingContext());
	}

	/**
	 * @param id
	 * @param request
	 * @throws KlabException
	 */
	@RequestMapping(value = API.DELETE_NAMESPACE, method = RequestMethod.DELETE)
	public void deleteNamespace(@PathVariable String id, HttpServletRequest request) throws KlabException {

		ISession session = sessionManager.getSession(request.getHeader(API.AUTHENTICATION_HEADER));
		if (session == null || !session.isExclusive()) {
			throw new KlabAuthorizationException("projects can only be loaded within an exclusive session");
		}

		INamespace namespace = KLAB.MMANAGER.getNamespace(id);
		IProject project = namespace == null ? null : namespace.getProject();
		if (project == null) {
			KLAB.warn("non-existent namespace " + id + " in deletion request");
			return;
		}

		((org.integratedmodelling.common.project.Project) project).deleteNamespace(id);
		KLAB.PMANAGER.loadProject(project.getId(), KLAB.MFACTORY.getRootParsingContext());

	}

	/**
	 * Clear the workspace. Should be called in preparation for a modeling
	 * session.
	 * 
	 * @param headers
	 * @param request
	 * @throws KlabException
	 */
	@RequestMapping(value = API.CLEAR_WORKSPACE, method = RequestMethod.DELETE)
	public void clearWorkspace(@RequestHeader HttpHeaders headers, HttpServletRequest request) throws KlabException {

		ISession session = sessionManager.getSession(request.getHeader(API.AUTHENTICATION_HEADER));
		if (session != null && !session.isExclusive()) {
			throw new KlabAuthorizationException("projects can only be loaded within an exclusive session");
		}
		if (session == null) {
			if (!(KServerController.isLocalIp(request) || KServerController.isAdmin(headers))) {
				throw new KlabAuthorizationException("no authorization to clear workspace");
			}
		}

		if (this.workspaceClearedBy == null) {
			this.workspaceClearedBy = session == null ? "admin" : session.getUser().getUsername();
			KLAB.PMANAGER.clearWorkspace(false);
		}
	}

}
