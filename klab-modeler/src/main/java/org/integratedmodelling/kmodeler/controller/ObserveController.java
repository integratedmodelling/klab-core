package org.integratedmodelling.kmodeler.controller;

import javax.servlet.http.HttpServletRequest;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.Task;
import org.integratedmodelling.common.beans.requests.ObservationRequest;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.engine.modelling.runtime.Context;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.kmodeler.components.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ferdinando.villa
 *
 */
@RestController
public class ObserveController {

	@Autowired
	SessionManager sessionManager;

	/**
	 * Main entry point for observation: requires session authentication and
	 * observes the main observable.
	 * 
	 * @param request
	 * @param headers
	 * @param servletRequest
	 * @return the running task adapted to a context bean.
	 * @throws KlabException
	 */
	@RequestMapping(value = API.OBSERVE, method = RequestMethod.POST)
	public @ResponseBody Task observe(@RequestBody ObservationRequest request, @RequestHeader HttpHeaders headers,
			HttpServletRequest servletRequest) throws KlabException {

		ISession session = sessionManager.getSession(headers.get(API.AUTHENTICATION_HEADER));
		if (session == null) {
			throw new KlabAuthorizationException("observe: session authentication failed");
		}

		ITask task = session.observe(request.parseObservable(), request.getScenarios(), request.adaptForcings());

		return KLAB.MFACTORY.adapt(task, Task.class);
	}

	/**
	 * Observation made on a context. Requires context authentication.
	 * 
	 * @param request
	 * @param headers
	 * @param servletRequest
	 * @return the running task adapted to a Task bean.
	 * @throws KlabException
	 */
	@RequestMapping(value = API.OBSERVE_IN_CONTEXT, method = RequestMethod.POST)
	public @ResponseBody Task observeWithin(@RequestBody ObservationRequest request, @RequestHeader HttpHeaders headers,
			HttpServletRequest servletRequest) throws KlabException {

		IContext context = sessionManager.getContext(headers.get(API.AUTHENTICATION_HEADER));
		if (context == null) {
			throw new KlabAuthorizationException("context authentication failed");
		}

		/*
		 * FIXME should use a secondary context, currently creates problems with
		 * subject instantiation.
		 */
		((Context) context).resetScenarios();

		if (request.getScenarios().size() > 0) {
			context = context.inScenario(request.getScenarios().toArray(new String[request.getScenarios().size()]));
		}
		
		/*
		 * create roles when explicitly set by users.
		 */
		if (request.getUserScenario() != null) {
		    for (Pair<IObservation, IConcept> ra : request.getUserScenario().findRoleAttributions(context)) {
		        ((Observation)ra.getFirst()).addRole(ra.getSecond());
		    }
		}
		
		if (request.getFocusObservation() != null) {
		    IObservation focus = ((Context)context).findWithId(request.getFocusObservation());
		    context = context.focus((ISubject) focus);
		}

		ITask task = context.observe(request.parseObservable());

		return KLAB.MFACTORY.adapt(task, Task.class);
	}

	/**
	 * Run the temporal transitions in the authenticating context.
	 * 
	 * @param headers
	 * @return the running task adapted to a Task bean
	 * @throws KlabException
	 */
	@RequestMapping(value = API.OBSERVE_RUN, method = RequestMethod.GET)
	public @ResponseBody Task runContext(@RequestHeader HttpHeaders headers) throws KlabException {

		IContext context = sessionManager.getContext(headers.get(API.AUTHENTICATION_HEADER));
		if (context == null) {
			throw new KlabAuthorizationException("context authentication failed");
		}

		ITask task = context.run();
		return KLAB.MFACTORY.adapt(task, Task.class);
	}
}
