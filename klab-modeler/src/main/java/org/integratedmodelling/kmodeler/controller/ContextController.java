package org.integratedmodelling.kmodeler.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.beans.Task;
import org.integratedmodelling.common.beans.generic.Graph;
import org.integratedmodelling.common.beans.requests.LocalExportRequest;
import org.integratedmodelling.common.beans.responses.FileResource;
import org.integratedmodelling.common.beans.responses.LocalExportResponse;
import org.integratedmodelling.common.beans.responses.StateData;
import org.integratedmodelling.common.beans.responses.StateSummary;
import org.integratedmodelling.common.beans.responses.ValueSummary;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.AbstractContext;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.visualization.ColorMap;
import org.integratedmodelling.common.visualization.Histogram;
import org.integratedmodelling.common.visualization.Viewport;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox2;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation;
import org.integratedmodelling.engine.modelling.runtime.DirectObservation.ArtifactGenerator;
import org.integratedmodelling.engine.visualization.VisualizationFactory;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kmodeler.components.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Implements endpoints that retrieve information from an active context. Different from
 * others, many of these will also accept media types like images or text (hopefully, one
 * day, also html, geojson and others). Spatial states could also respond to wfs/wcs or
 * wms requests, which may come later (e.g. through deegree or other).
 *
 * @author ferdinando.villa
 */
@RestController
public class ContextController {

    @Autowired
    SessionManager sessionManager;

    /**
     * Store observation from context in local kbox.
     * 
     * @param observationId
     * @param headers
     * @return
     * @throws KlabException
     */
    @RequestMapping(value = API.CONTEXT_STORE_OBSERVATION, method = RequestMethod.GET)
    public FileResource storeObservation(@PathVariable("observation") String observationId, @RequestHeader HttpHeaders headers)
            throws KlabException {

        IContext context = sessionManager
                .getContext(headers.get(API.AUTHENTICATION_HEADER));
        if (context == null) {
            throw new KlabAuthorizationException("context authentication failed");
        }

        IObservation obs = ((AbstractContext) context).findWithId(observationId);

        if (obs != null) {
            long id = ObservationKbox2.get().store(obs);
            FileResource ret = new FileResource();
            String urn = ObservationKbox2.get()
                    .getUrnForStoredObservation(id, context.getSession().getUser());
            ret.setUrn(urn);
            return ret;
        }

        throw new KlabValidationException("cannot locate observation from ID in passed context");
    }

    /**
     * @return the full file path requested for verification.
     * @throws KlabException
     */
    @RequestMapping(value = API.CONTEXT_EXPORT_DATA_LOCAL, method = RequestMethod.POST)
    public LocalExportResponse exportContextData(@RequestBody LocalExportRequest request, @RequestHeader HttpHeaders headers, HttpServletResponse response)
            throws KlabException {

        IContext context = sessionManager
                .getContext(headers.get(API.AUTHENTICATION_HEADER));
        if (context == null) {
            throw new KlabAuthorizationException("context authentication failed");
        }

        LocalExportResponse ret = null;

        if (request.getObservationId().startsWith("A|")) {

            /*
             * requesting model artifact - special treatment
             */
            String[] oreqs = request.getObservationId().split("\\|");
            String artifactId = oreqs[1];
            String observationId = oreqs[2];
            boolean isModel = oreqs[3].equals("model");

            IObservation observation = ((AbstractContext) context).findWithId(observationId);

            if (observation instanceof DirectObservation) {
                ret = ((DirectObservation) observation).buildArtifact(artifactId, isModel);
            }

        } else {

            ret = new LocalExportResponse();

            List<Object> opts = new ArrayList<>();
            opts.add(request.getAggregation());
            opts.add(request.getFormat());
            if (request.getLocators() != null) {
                opts.add(Scale.parseLocators(request.getLocators()));
            }

            context.persist(request.getOutputFile(), request.getObservationId(), opts
                    .toArray());

            ret.getFiles().add(request.getOutputFile().toString());
        }

        return ret;
    }

    /**
     * @return the full file path requested for verification.
     * @throws KlabException
     */
    @RequestMapping(value = API.CONTEXT_EDIT_LOCAL, method = RequestMethod.POST)
    public void editContextArtifact(@RequestBody LocalExportRequest request, @RequestHeader HttpHeaders headers, HttpServletResponse response)
            throws KlabException {

        IContext context = sessionManager
                .getContext(headers.get(API.AUTHENTICATION_HEADER));
        if (context == null) {
            throw new KlabAuthorizationException("context authentication failed");
        }

        /*
         * requesting model artifact - special treatment
         */
        String[] oreqs = request.getObservationId().split("\\|");
        String artifactId = oreqs[1];
        String observationId = oreqs[2];
        boolean isModel = oreqs[3].equals("model");

        IObservation observation = ((AbstractContext) context).findWithId(observationId);

        if (observation instanceof DirectObservation) {
            ArtifactGenerator handler = ((DirectObservation) observation)
                    .getArtifactHandler(artifactId, isModel);

            /**
             * launch editor or do nothing
             */
            if (handler != null) {
                handler.edit();
            }
        }
    }

    @RequestMapping(
            value = API.CONTEXT_GET_REPORT,
            method = RequestMethod.GET,
            produces = MediaType.TEXT_HTML_VALUE)
    public String getReport(@PathVariable(value = "context") String contextId) throws KlabException {

        IContext context = sessionManager.getContext(contextId);
        if (context == null) {
            throw new KlabAuthorizationException("context resolution failed");
        }

        return context.getReport().asHTML();
    }

    /**
     * FIXME should be a DELETE endpoint and use task authentication with no parameters.
     */
    @RequestMapping(
            value = API.CONTEXT_ABORT_TASK,
            method = RequestMethod.GET)
    public Task abortTask(@PathVariable("context") String contextId, @PathVariable("task") String taskId)
            throws KlabAuthorizationException {

        IContext context = sessionManager.getContext(contextId);
        if (context == null) {
            throw new KlabAuthorizationException("context resolution failed");
        }

        ITask task = ((AbstractContext) context).getTask(taskId);
        if (task != null) {
            KLAB.info("task " + task + " is being aborted");
            task.interrupt();
        }

        return task == null ? null : KLAB.MFACTORY.adapt(task, Task.class);
    }

    /**
     * @param contextId
     * @param observation
     * @return data from context
     * @throws KlabException
     */
    @RequestMapping(
            value = API.CONTEXT_GET_DATA,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    @ResponseBody
    public void getContextData(@PathVariable(
            value = "context") String contextId, @PathVariable String observation, @RequestParam String index, HttpServletResponse response)
            throws KlabException {

        IContext context = sessionManager.getContext(contextId);
        if (context == null) {
            throw new KlabAuthorizationException("context resolution failed");
        }

        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);

        File out = null;
        try {
            out = File.createTempFile("out", "tif");
            context.persist(out, observation);
        } catch (IOException e) {
            throw new KlabIOException(e);
        }

        try (InputStream in = new FileInputStream(out)) {
            IOUtils.copy(in, response.getOutputStream());
        } catch (Exception e) {
            throw new KlabIOException(e);
        }
    }

    /**
     * @param contextId
     * @param observation
     * @param index
     * @param viewport
     * @param servletRequest
     * @param response
     * @throws KlabException
     * @throws KlabAuthorizationException
     * @throws IOException
     */
    @RequestMapping(
            value = API.CONTEXT_GET_MEDIA,
            method = RequestMethod.GET,
            produces = MediaType.IMAGE_PNG_VALUE)
    public void getContextImage(@PathVariable(
            value = "context") String contextId, @PathVariable String observation, @RequestParam String index, @RequestParam String viewport, HttpServletRequest servletRequest, HttpServletResponse response)
            throws KlabException, IOException {

        IContext context = sessionManager.getContext(contextId);
        if (context == null) {
            throw new KlabAuthorizationException("context resolution failed");
        }

        /*
         * FIXME there must be a way to get this from the extension in the URL.
         */
        String contentType = response.getContentType();
        if (contentType == null) {
            contentType = MediaType.IMAGE_PNG_VALUE;
        }
        IObservation obs = ((AbstractContext) context).findWithId(observation);
        Viewport vport = Viewport.fromString(viewport);
        Locator[] locators = Scale.parseLocators(index);
        IMedia media = VisualizationFactory.get().getMedia(obs, obs.getScale()
                .getIndex(locators), vport, contentType, null);

        try (InputStream in = media.getStream()) {
            IOUtils.copy(in, response.getOutputStream());
        }
    }

    /**
     * Returns the current provenance graph for the named context.
     * 
     * @param contextId
     * @param observation
     * @param index
     * @return summary
     * @throws KlabException
     */
    @RequestMapping(value = API.CONTEXT_GET_PROVENANCE, method = RequestMethod.GET)
    public @ResponseBody Graph getContextProvenance(@PathVariable(
            value = "context") String contextId)
            throws KlabException {

        IContext context = sessionManager.getContext(contextId);
        if (context == null) {
            throw new KlabAuthorizationException("context resolution failed");
        }
        return KLAB.MFACTORY.adapt(context.getProvenance(), Graph.class);
    }

    /**
     * Returns a descriptor for the aggregated distribution of value in a state at a given
     * index (usually only a temporal index).
     * 
     * @param contextId
     * @param observation
     * @param index
     * @return summary
     * @throws KlabException
     */
    @RequestMapping(value = API.CONTEXT_GET_SUMMARY, method = RequestMethod.GET)
    public @ResponseBody StateSummary getContextSummary(@PathVariable(
            value = "context") String contextId, @PathVariable String observation, @RequestParam String index)
            throws KlabException {

        IContext context = sessionManager.getContext(contextId);
        if (context == null) {
            throw new KlabAuthorizationException("context resolution failed");
        }
        IObservation obs = ((AbstractContext) context).findWithId(observation);
        Locator[] locators = Scale.parseLocators(index);

        if ((locators == null || locators.length == 0) && obs.getScale().isTemporallyDistributed()
                && obs.getScale().isSpatiallyDistributed()) {
            // assume user wants initial state map
            locators = new Locator[] { ITransition.INITIALIZATION };
        }

        if (obs instanceof IState) {
            StateSummary ret = new StateSummary();
            IScale.Index ind = obs.getScale().getIndex(locators);
            Histogram histogram = VisualizationFactory
                    .getHistogram((IState) obs, ind, 10);
            ColorMap colormap = (ColorMap) VisualizationFactory
                    .getColormap((IState) obs, ind);
            if (histogram != null) {
                ret.setHistogram(KLAB.MFACTORY
                        .adapt(histogram, org.integratedmodelling.common.beans.Histogram.class));
            }
            if (colormap != null) {
                ret.setColormap(KLAB.MFACTORY
                        .adapt(colormap, org.integratedmodelling.common.beans.Colormap.class));
            }
            return ret;
        }
        return null;
    }

    @RequestMapping(value = API.CONTEXT_GET_VALUES, method = RequestMethod.GET)
    public @ResponseBody StateData getContextValues(@PathVariable(
            value = "context") String contextId, @PathVariable String observation)
            throws KlabException {

        IContext context = sessionManager.getContext(contextId);
        if (context == null) {
            throw new KlabAuthorizationException("context resolution failed");
        }
        IObservation obs = ((AbstractContext) context).findWithId(observation);

        if (obs instanceof IState) {

            StateData ret = new StateData();
            IState state = (IState) obs;

            String label = NS.getDisplayName(state.getObservable().getType()) + " of "
                    + state.getContextObservation().getName();
            if (state.getObserver() instanceof IMeasuringObserver) {
                label += " " + ((IMeasuringObserver) state.getObserver()).getUnit();
            } else if (state.getObserver() instanceof IValuingObserver) {
                label += " " + ((IValuingObserver) state.getObserver()).getCurrency();
            }

            long[] times = new long[(int) state.getScale().getTime().getMultiplicity()];
            double[] vdata = new double[(int) state.getScale().getTime()
                    .getMultiplicity()];

            for (int n = 0; n < state.getScale().getTime().getMultiplicity(); n++) {
                times[n] = state.getScale().getTime().getExtent(n).getStart().getMillis();
                vdata[n] = States.getDouble(state, n);
            }

            ret.setLabel(label);
            ret.setTimes(times);
            ret.setVdata(vdata);

            return ret;

        }

        return null;
    }

    /**
     * Describe a single state value at a given offset.
     * 
     * @param observation
     * @param stateIndex
     * @param headers
     * @return value
     * @throws KlabException
     */
    @RequestMapping(value = API.CONTEXT_GET_VALUE, method = RequestMethod.GET)
    public @ResponseBody ValueSummary getContextValue(@PathVariable(
            value = "context") String contextId, @PathVariable String observation, @RequestParam(
                    name = "state-offset") long stateIndex, @RequestHeader HttpHeaders headers)
            throws KlabException {

        IContext context = sessionManager.getContext(contextId);
        if (context == null) {
            throw new KlabAuthorizationException("context resolution failed");
        }
        IObservation obs = ((AbstractContext) context).findWithId(observation);
        if (obs instanceof IState) {
            return VisualizationFactory.get().describeValue((IState) obs, ((IState) obs)
                    .getValue((int) stateIndex));
        }
        return null;
    }
}
