package org.integratedmodelling.kmodeler.controller;

import javax.servlet.http.HttpServletRequest;

import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.kmodeler.components.SessionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Endpoints and directories related to the establishment of the IM network.
 * 
 * @author Ferd
 *
 */
@RestController
public class NetworkController {

    @Autowired
    SessionManager sessionManager;
    
    /**
     * Engine version of GET_NETWORK: just return the network as is. 
     * Unsecured for now.
     * 
     * TODO secure with session authentication and filter nodes
     * 
     * @param servletRequest
     * @return the network
     * @throws KlabAuthorizationException
     */
    @RequestMapping(value = API.GET_NETWORK, method = RequestMethod.GET)
    public @ResponseBody Network getNetwork(HttpServletRequest servletRequest)
            throws KlabAuthorizationException {
        return KLAB.MFACTORY.adapt(KLAB.ENGINE.getNetwork(), Network.class);
    }

    @RequestMapping(value = API.SET_NETWORK_STATUS, method = RequestMethod.GET)
    public void setNetworkStatus(HttpServletRequest servletRequest, @PathVariable String status, @RequestHeader HttpHeaders headers)
            throws KlabAuthorizationException {

        ISession session = sessionManager
                .getSession(headers.get(API.AUTHENTICATION_HEADER));
        if (session == null) {
            throw new KlabAuthorizationException("set network status: session authentication failed");
        }

        KLAB.ENGINE.getNetwork().activate(status.equals("online"));
        
    }

}
