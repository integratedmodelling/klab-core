/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.kmodeler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.services.IBatchRunner;
import org.integratedmodelling.common.configuration.Configuration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.configuration.KLAB.BootMode;
import org.integratedmodelling.engine.ModelingEngine;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kmodeler.components.SessionManager;
import org.integratedmodelling.kserver.controller.KServerController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author ferdinando.villa
 *
 */
@SpringBootApplication
@ComponentScan(basePackages = {
        "org.integratedmodelling.kmodeler",
        "org.integratedmodelling.error.kserver",    // link in the error controller
        "org.integratedmodelling.auth.indirect",     // link indirect auth for /get
        "org.integratedmodelling.common.configuration.services",    // service conf
        "org.integratedmodelling.common.data",          // links in the basic urn resolver
        "org.integratedmodelling.kserver" })
public class Application {

    /**
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws Exception {

        if (!KServerController.setup(BootMode.MODELER)) {
            KLAB.error("modeling engine initialization failed. Exiting.");
            System.exit(1);
        }
        
        List<String> batchOpts = new ArrayList<>();
        IBatchRunner runner = null;
        for (int i = 0; i < args.length; i++) {
            if (runner != null) {
                batchOpts.add(args[i]);
            } else if (args[i].equals("-run")) {
                String rid = args[++i];
                Class<? extends IBatchRunner> cls = KLAB.getBatchRunner(rid);
                if (cls == null) {
                    throw new KlabValidationException("no batch runner corresponds to identifier " + rid); 
                }
                runner = cls.newInstance();
            }
        }

        if (runner != null) {
            ((ModelingEngine) KLAB.ENGINE).loadLastUsedClientWorkspace();
            runner.setContext(batchOpts.toArray());
            runner.run();
            return;
        } 
        
        final ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        final KServerController controller = context.getBean(KServerController.class);

        /*
         * print a nice visible startup banner - stdout not log
         */
        System.out.println();
        System.out.println("****************************************************************************");
        System.out.println("* k.LAB engine " + KLAB.ENGINE.getName() + " v" + Version.CURRENT + " ready at " + KLAB.ENGINE.getUrl());
        System.out.println("* Engine workspace set to " + KLAB.WORKSPACE.getDefaultFileLocation());
        System.out.println("****************************************************************************");

        /*
         * these are the inline ones or any deployed in components/, although that's not the
         * typical thing to do with a modeling engine.
         */
//        ((ProjectManager)KLAB.PMANAGER).deployComponents();
        
        if (controller != null) {
            // make ourselves discoverable on the local network
            ((ModelingEngine) KLAB.ENGINE)
                    .startAdvertising(Configuration.MODELER_APPLICATION_ID, controller.getPort());
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                SessionManager sessionManager = context.getBean(SessionManager.class);
                if (sessionManager != null) {
                    sessionManager.pausePolling();
                }
                if (controller != null) {
                    KLAB.ENGINE.shutdown(0);
                }
            }
        });
        
    }

}
