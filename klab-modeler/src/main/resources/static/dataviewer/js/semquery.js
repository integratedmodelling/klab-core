/**
 * HTML support for semantic query results.
 *
 * Main functions:
 *      describeConcepts([concepts], "divId");
 *      describeSubjects([subjects], "divId");
 *
 * Each concept is a map like
 *
 *     c1 = {
        name: 'geography:Elevation',
        type: 'Q',
        description: 'The geographical elevation above sea level, as described by a digital	elevation model.',
        definition: 'The relative height of the Earth terrain within a terrestrial Earth region.',
        tryIn: ['im.example.locations:tanzania-grr'],
        seeAlso: ['geography:Slope', 'geography:Aspect', 'geography:BathymetricDepth'],
        domain: 'Geography'
    };
 *
 * Each subject is a map like:
 *
 *     s1 = {
        name: 'im.example.locations:tanzania-grr',
        id: 'tanzania-grr',
        description: 'The Great Ruaha river region in Tanzania',
        type: { // this is a concept's map like c1
            name: 'earth:Region',
            type: 'S',
            description: 'A section of the Earth realm delimited in volumetric space, including the surface ' +
            'and the atmospheric bottom layer. It applies to both land and sea - anywhere on Earth - and ' +
            'should be qualified as necessary when used.',
            definition: 'Any geo-located, delimited body of earth surface in the natural domain.'
        }
    }
 *
 * Created by Ferd on 11/22/2015.
 */

function getIcon(type) {
    var ret = '';
    var tooltip = '';
    switch (type) {
        case 'T':
            ret = 't_blue.png';
            tooltip = 'Trais represent attributes, realms, identities and roles. They are defined\n' +
                'independently from observables and can be added to them as needed. Using traits\n' +
                'helps keep ontologies small and understandable';
            break;
        case 'P':
            ret = 'p_gold.png';
            tooltip = 'Processes unfold in time, modifying qualities and have the ability to\n' +
                'create or destroy subjects and events. Most dynamic models are observations\n' +
                'of processes.';
            break;
        case 'E':
            ret = 'e_yellow.png';
            tooltip = 'Events are time-bound; seen at a finer temporal scale, they would be processes,\n' +
                'but the scale of the worldview adopted makes them recognizable as\n' +
                'countable, atomic entities. They create one-off change in qualities,\n' +
                'and have the ability to create and destroy subjects';
            break;
        case 'S':
            ret = 's_orange.png';
            tooltip = 'Subjects are countable objects that are usually physical. Groups of subjects\n' +
                'can themselves be seen as subjects. Subjects are the context for qualities, events,\n' +
                'processes, and other subjects. Each subject can be seen as a system.';
            break;
        case 'Q':
            ret = 'q_green.png';
            tooltip = 'Qualities are what we commonly observe as "data": they can’t exist alone and are always\n' +
                'observed indirectly, by referring them to a reference quantity (e.g. 10 m of elevation).\n' +
                'Qualities define the states of subjects and adopt the scale that describes their context\n' +
                'subject’s view of the world. They can be modified by processes and events.';
            break;
    }
    return $('<img/>', { 'data-toggle': "tooltip", title: tooltip, src: 'img/' + ret,  width: '16px', height: '16px', style: 'vertical-align: bottom;'});
}

function describeConcept(conceptMap) {

    var rows = [];

    rows.push($('<div/>', {class: 'row'}).append(
        $('<div/>', {class: 'col-xs-1'}).append(
            $('<div/>', {class: 'img-thumbnail'}).append(
                getIcon(conceptMap.type)
            )
        ),
        $('<div/>', {class: 'col-xs-8', style: 'font-size: 16px;'}).append(
            conceptMap.name
        ),
        $('<p/>'),
        $('<div/>', {class: 'col-xs-1'}).append(
            $('<button/>', {type: 'button', class: 'btn btn-xs btn-link'}).
            append('Observe').
            click(function() {
                send({cmd: 'observe', observable: conceptMap.name});
            })
        ),
        $('<div/>', {class: 'col-xs-1'}).append(
            $('<button/>', {type: 'button', class: 'btn btn-xs btn-link'}).
            append('Bookmark').
            click(function() {
                send({cmd: 'bookmark', observable: conceptMap.name});
            })
        ),
        $('<div/>', {class: 'col-xs-1'}).append(
            $('<button/>', {type: 'button', class: 'btn btn-xs btn-link'}).
            append('Inspect').
            click(function() {
                send({cmd: 'inspect', observable: conceptMap.name});
            })
        )
    ));

    if (conceptMap.domain) {
        rows.push(
            $('<div/>', {class: 'row'}).append(
                $('<div/>', {style: 'font-size: 10px;'}).
                append("<strong>Domain: </strong>").
                append($('<button/>', {type: 'button', class: 'btn btn-xs btn-link', style: 'font-size: 10px; margin-bottom: 2px;'}).
                    append(conceptMap.domain).click(function() {
                        send({cmd: 'explore-domain', domain: conceptMap.domain});
                    })
                )
            )
        );
    }

    if (conceptMap.definition) {
        rows.push(
            $('<div/>', {class: 'row'}).append(
                $('<div/>', {style: 'font-size: 10px; margin-top: -2px; margin-bottom: 2px;'}).
                append("<strong>Defined as: </strong>" + conceptMap.definition)
            )
        );
    }

    rows.push(
        $('<div/>', {class: 'row'}).append(
            $('<div/>', {class: 'well'}).append(conceptMap.description)
        )
    );

    if (conceptMap.tryIn && conceptMap.tryIn.length > 0) {
        var buttons = [];
        for (var i = 0; i < conceptMap.tryIn.length && i < 12; i++) {
            buttons.push(
                $('<button/>', {type: 'button', class: 'btn btn-xs btn-link', style: 'font-size: 11px;'}).
                append(conceptMap.tryIn[i]).click(function() {
                    alert("TRY IN " + conceptMap.tryIn[i]);
                }));
        }

        rows.push(
            $('<div/>', {class: 'row'}).append(
                $('<div/>', {style: 'font-size: 11px;'}).
                append("<strong>Try in: </strong>").append(buttons)
            )
        )
    }

    if (conceptMap.seeAlso && conceptMap.seeAlso.length > 0) {

        var buttons = [];
        for (var i = 0; i < conceptMap.seeAlso.length; i++) {
            buttons.push(
                $('<button/>', {type: 'button', class: 'btn btn-xs btn-link', style: 'font-size: 11px;'}).
                append(conceptMap.seeAlso[i]).click(function() {
                    alert("SEE ALSO " + conceptMap.seeAlso[i]);
                }));
        }

        rows.push(
            $('<div/>', {class: 'row'}).append(
                $('<div/>', {style: 'font-size: 11px;'}).
                append("<strong>See also: </strong>").append(buttons)
            )
        )
    }

    rows.push($('<p/>', {style: 'margin-bottom: 18px;'}));

    return $('<div/>', {class: "container"}).append(rows);
}

function describeConcepts(concepts, divId) {
    for (var i = 0; i < concepts.length; i++) {
        describeConcept(concepts[i]).appendTo( $("#"+divId) );
    }
}