/**
 * Created by Ferd on 10/27/2015.
 */

/**
 * TODO add parameters
 */
function drawFlows(rows) {

    var data = new google.visualization.DataTable();
    data.addColumn('string', 'From');
    data.addColumn('string', 'To');
    data.addColumn('number', 'Weight');
    data.addRows(rows);

    // Sets chart options.
    var options = {
        width: $(window).width() - 20,
        height: $(window).height() - 80,
    };

    // Instantiates and draws our chart, passing in some options.
    var chart = new google.visualization.Sankey(document.getElementById('flowmap'));
    chart.draw(data, options);
}