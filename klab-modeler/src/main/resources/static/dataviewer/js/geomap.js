/**
 * Created by Ferd on 10/27/2015.
 */

// the Openlayers map widget
var map;

// center of map, passed by URL or defaulting to somewhere nice.
var center;

// vector layer for drawing upon
var drawVector;

// drawVector layer for the current context and its edit toolbar
var showVector;
var editToolbar;

// marker layer
var markerLayer;

// base map layers for orientation and browsing
var baseLayers = [];

// image layers added by showMap and their order of addition
var layers = {};
var layerOrder = [];

// features added one by one to showLayer
var features = {};
// markers added one by one to markerLayer
var markers = {};

// projections - we use Google but talk in lat/lon
var latlon = new OpenLayers.Projection("EPSG:4326");
var goglon = new OpenLayers.Projection("EPSG:900913");

// index of currently visible layer in baseLayers
var layerIdx = 0;

// available styles for polygons
var redOutline;
var greyFill;

// wkt format
var wkt = new OpenLayers.Format.WKT();

function getVectorLayer() {
	if (showVector == null) {
		showVector = new OpenLayers.Layer.Vector("Context" /*
															 * , NO!
															 * {numZoomLevels:
															 * 22, isBaseLayer:
															 * false}
															 */);
		// Ensure that text and geometry are rendered in the same container
		// (fixes overlapping of text re: GWT).
		// showVector.setTextRootRendererToVectorRootRenderer(); ??? I think
		// this is the code:
		showVector.renderer.textRoot = showVector.renderer.vectorRoot;
		map.addLayer(showVector);
	}
	return showVector;
}

function getMarkersLayer() {
	if (markerLayer == null) {
		markerLayer = new OpenLayers.Layer.Markers("Markers");
		map.addLayer(markerLayer);
	}
	return markerLayer;
}

function drawMode(enabled) {

	if (enabled) {

		if (drawVector == null) {
			drawVector = new OpenLayers.Layer.Vector("Edit", {
				numZoomLevels : 22,
				isBaseLayer : false
			});
			editToolbar = new OpenLayers.Control.EditingToolbar(drawVector);

			drawVector.events.register("featureadded", drawVector, function(e) {
				var geometry = e.feature.geometry;
				geometry.transform(goglon, latlon);
				// why it adds plus signs without a space is beyond me.
				send("shape-added", wkt.extractGeometry(geometry));
			})
		}

		map.addLayer(drawVector);
		map.addControl(editToolbar);

	} else if (drawVector != null) {
		map.removeLayer(drawVector);
		map.removeControl(editToolbar);
	}
}

function compareArrays(a1, a2) {
	if (a1.length != a2.length) {
		return false;
	}
	for (var i = 0; i < a1.length; i++) {
		if (a1[i] != a2[i]) {
			return false;
		}
	}
	return true;
}

function showMap(urls, ids, minx, miny, maxx, maxy, viewx, viewy) {

	if (!(urls.constructor === Array)) {
		urls = [ urls ];
		ids = [ ids ];
	}

	var isSame = compareArrays(ids, layerOrder);
	if (!isSame) {
		removeAllMaps();
	}

	var zindex = 3000;
	var bounds = new OpenLayers.Bounds(minx, miny, maxx, maxy);
	bounds = bounds.transform(latlon, goglon);
	for (var i = 0; i < urls.length; i++) {
		if (isSame) {
			layers[ids[i]].url = urls[i];
			layers[ids[i]].redraw();
		} else {
			var image = new OpenLayers.Layer.Image("I" + ids[i], urls[i],
					bounds, new OpenLayers.Size(viewx, viewy), {
						numZoomLevels : 22,
						maxResolution : 156543.0339,
						displayOutsideMaxExtent : true,
						isBaseLayer : false
					});
			image.zIndex = zindex + i;
			layers[ids[i]] = image;
			layerOrder.push(ids[i]);
			map.addLayer(image);
		}
	}
}

function showContext(minx, miny, maxx, maxy, shape) {

	removeAllMaps();
	if (showVector != null) {
		showVector.removeAllFeatures();
	}
	if (markerLayer != null) {
		markerLayer.clearMarkers();
	}
	features = {};
	markers = {};

	var bounds = new OpenLayers.Bounds(minx, miny, maxx, maxy);
	bounds = bounds.transform(latlon, goglon);
	map.zoomToExtent(bounds, true);
	if (shape != null && shape != "__NULL__") {
		addFeature(shape, null, "/", "#ff0000");
	}
}

function addFeature(shape, name, id, color, fillColor, opacity) {
//	var isLine = shape.startsWith("LINE");
	if (!((id + "_0") in features)) {
		var ff = [];
		var feats = wkt.read(shape);
		if (feats.constructor == Array) {
			for (var i = 0; i < feats.length; i++) {
				ff.push(feats[i]);
			}
		} else {
			ff.push(feats);
		}
		for (var i = 0; i < ff.length; i++) {
			ff[i].geometry.transform(latlon, goglon);
			ff[i].style = newStyle(color, fillColor, opacity, name);
			features[id + "_" + i] = ff[i];
		}
	}
	showFeature(id);
}

function newStyle(color, fillColor, opacity, text, image) {

	// hostias
	var ret = OpenLayers.Util.extend({},
			OpenLayers.Feature.Vector.style['default'])

	if (typeof image === 'undefined') {
		image = null;
	}

	if (typeof text === 'undefined') {
		text = null;
	}

	if (typeof opacity === 'undefined') {
		opacity = 0.0;
	}

	if (typeof fillColor === 'undefined') {
		fillColor = null;
	}

	if (image != null) {
		ret.externalGraphic = image;
	}
	ret.pointRadius = 4;
	ret.strokeWidth = fillColor == null ? 1 : 0;
	ret.fillOpacity = opacity;
	ret.strokeOpacity = 1.0;
	if (fillColor != null) {
		ret.fillColor = fillColor;
	}
	ret.strokeColor = color;

	if (text != null) {
		ret.label = text;
		ret.fontFamily = "arial";
		ret.fontSize = "7pt";
	}

	return ret;
}

function hideFeature(id) {
	for (var i = 0;; i++) {
		if (features[id + "_" + i] == null) {
			break;
		}
		getVectorLayer().removeFeatures([ features[id + "_" + i] ]);
	}
	delete features[id];
}

function showFeature(id) {
	var feas = [];
	for (var i = 0;; i++) {
		if (features[id + "_" + i] == null) {
			break;
		}
		feas.push(features[id + "_" + i]);
	}
	if (feas.length > 0) {
		getVectorLayer().addFeatures(feas);
		getVectorLayer().zIndex = 4000;
		getVectorLayer().refresh();
	}
}

function removeAllMaps() {
	$.each(layers, function(k, v) {
		map.removeLayer(v);
	});
	layers = {};
	layerOrder = [];
}

function removeMap(id) {
	if (id in layers) {
		map.removeLayer(layers[id]);
		delete layers[id];
		var index = layerOrder.indexOf(id);
		if (index > -1) {
			layerOrder.splice(index, 1);
		}
	}
}

function addMarker(name, id, x, y) {

	if (!(id in markers)) {
		var lonlat = new OpenLayers.LonLat(x, y);
		lonlat.transform(latlon, goglon);
		var marker = new OpenLayers.Marker(lonlat);
		markers[id] = marker;
	}
	showMarker(id);
}

function showMarker(id) {
	if (id in markers) {
		getMarkersLayer().addMarker(markers[id]);
		getMarkersLayer().setZIndex(4001);
	}
}

function hideMarker(id) {
	if (id in markers) {
		getMarkersLayer().removeMarker(markers[id]);
	}
}

function setOpacity(layerId, opacity) {
	var layer = layers[layerId];
	if (layer != null) {
		layer.setOpacity(opacity);
	}
}

function resetMap() {
	removeAllMaps();
	if (markerLayer != null) {
		markerLayer.clearMarkers();
	}
	if (showVector != null) {
		showVector.removeAllFeatures();
	}
	features = {};
	markers = {};
	map.setCenter(center, 5);
}

function cycleMap() {
	layerIdx = (layerIdx + 1) % baseLayers.length;
	for (var i = 0; i < baseLayers.length; i++) {
		if (i == layerIdx) {
			baseLayers[i].visibility = true;
			baseLayers[i].isBaseLayer = true;
			map.setBaseLayer(baseLayers[i]);
		} else {
			baseLayers[i].visibility = false;
			baseLayers[i].isBaseLayer = false;
		}
	}
}

function allowDropOnMap(ev) {
	ev.preventDefault();
}

function drag(ev) {
	ev.dataTransfer.setData("text", ev.target.id);
}

function dropOnMap(ev) {
	ev.preventDefault();
	var data = ev.dataTransfer.getData("text");
	if (data != null && (data.substring(0, 3) == "@PI" || data.substring(0, 3) == "@BM")) {
		var key = "";
		if (ev.ctrlKey) {
			key = "ctrl";
		} else if (ev.shiftKey) {
			key = "shift";
		} else if (ev.altKey) {
			key = "alt";
		}
		var lonlat = map.getLonLatFromViewPortPx(new OpenLayers.Pixel(ev.clientX,
				ev.clientY));
		lonlat.transform(goglon, latlon);
		var extent = map.getExtent();
		extent.transform(goglon, latlon);
		send((data.substring(0,2) == "@B" ? "object-drop" : "tool-drop"), data.substring(4), lonlat.lat, lonlat.lon, extent.left, extent.right, extent.bottom, extent.top, key);
	}
}

function redrawMap() {
	map.updateSize();
}

function sendBoundaries() {
	var extent = map.getExtent();
	extent.transform(goglon, latlon);
	send("on-view-change", extent.left, extent.right, extent.bottom, extent.top);
}

function initMap(id) {

	var latC = getUrlParameter('lat', '48.9');
	var lonC = getUrlParameter('lon', '10.2');
	center = new OpenLayers.LonLat(parseFloat(lonC), parseFloat(latC))
			.transform(latlon, goglon);

	redOutline = new OpenLayers.Style();
	redOutline.pointRadius = 4;
	redOutline.strokeWidth = 1;
	redOutline.strokeColor = "#ff0000";
	redOutline.strokeDashstyle = "dash";

	map = new OpenLayers.Map(id, {
		allOverlays : true
	});

	// map.addControl(new OpenLayers.Control.LayerSwitcher());

	// the SATELLITE layer has all 22 zoom level, so we add it first to
	// become the internal base layer that determines the zoom levels of the
	// map.
	var gsat = new OpenLayers.Layer.Google("Google Satellite", {
		type : google.maps.MapTypeId.SATELLITE,
		numZoomLevels : 22
	});
	var gphy = new OpenLayers.Layer.Google("Google Physical", {
		type : google.maps.MapTypeId.TERRAIN,
		visibility : false
	});
	var gmap = new OpenLayers.Layer.Google("Google Streets", // the default
	{
		numZoomLevels : 20,
		visibility : false
	});
	var ghyb = new OpenLayers.Layer.Google("Google Hybrid", {
		type : google.maps.MapTypeId.HYBRID,
		numZoomLevels : 22,
		visibility : false
	});

	baseLayers = [ gsat, gphy, gmap, ghyb ];
	map.addLayers(baseLayers);

	/*
	 * set handler for click on map
	 */
	map.events.register("click", map, function(e) {
		var lonlat = map.getLonLatFromViewPortPx(e.xy);
		lonlat.transform(goglon, latlon);
		send("on-click", lonlat.lat, lonlat.lon);
	});
	
	map.events.register("moveend", map, sendBoundaries);
	map.events.register("zoomend", map, sendBoundaries);

	// /*
	// * hande drop on map from outside - basically add world coordinates and
	// * echo payload.
	// */
	// map.events.register("drop", map, function(e) {
	//
	// });
	map.setCenter(center, 5);
	
	// ensure resize of map after windows resize
	window.onresize = function() {
		setTimeout(function() {
			map.updateSize();
			var size = map.getCurrentSize();
		}, 200);
	}

}
