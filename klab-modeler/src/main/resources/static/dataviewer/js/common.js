/**
 * Created by ferdinando.villa on 10/30/2015.
 */
var chartsInited = false;

function getUrlParameter(sParam, defv) {

    if (typeof defv === 'undefined') {
        defv = null;
    }
    

    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }

    return defv;
}
