/**
 * Implements the duplex communication that controls the web-based dataviewer.
 *
 * Created by Ferd on 10/28/2015.
 * Modified to use websockets on 1/26/2016.
 */
var stompClient = null;
var sessionId = getUrlParameter('sessionId');

function send() {
	var message = {};
	message.arguments = [];
	message.sessionId = sessionId;
	var i;
	for (i = 0; i < arguments.length; i++) {
		if (i == 0) {
			message.command = arguments[i];
		} else {
			message.arguments.push(arguments[i]);
		}
	}
	stompClient.send("/klab/viewer", {}, JSON.stringify(message));
}

function setupCommunication() {
    var socket = new SockJS('/kmodeler/viewer');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function(frame) {
        stompClient.subscribe('/session/' + sessionId, function(command){
            var cmd = JSON.parse(command.body);
            execute(cmd);
        });
    });
}

function switchView(view) {
    switch (view) {
        case "flow":
            $("#geomap").hide();
            $("#flowmap").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            $("#timeplot").hide();
            $("#timeline").hide();
            $("#semquery").hide();
            break;
        case "map":
            $("#geomap").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            $("#flowmap").hide();
            $("#timeplot").hide();
            $("#timeline").hide();
            $("#semquery").hide();
            break;
        case "plot":
            $("#geomap").hide();
            $("#flowmap").hide();
            $("#timeplot").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            $("#timeline").hide();
            $("#semquery").hide();
            break;
        case "timeline":
            $("#geomap").hide();
            $("#flowmap").hide();
            $("#timeplot").hide();
            $("#timeline").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            $("#semquery").hide();
            break;
        case "query":
            $("#geomap").hide();
            $("#flowmap").hide();
            $("#timeplot").hide();
            $("#timeline").hide();
            $("#semquery").show("slow").css({"top":"0","bottom":"0","left":"0","right":"0"});
            break;
    }
}

function execute(message) {

    switch (message.command) {
        case "set-context":
            showContext(message.arguments[0], message.arguments[1], message.arguments[2], message.arguments[3], message.arguments[4]);
            break;
        case "cycle-maps":
            cycleMap();
            break;
        case "show-map":
            showMap(
                message.arguments[0], // URL array
                message.arguments[1], // ID array
                message.arguments[2], // minX
                message.arguments[3], // minY
                message.arguments[4], // maxX
                message.arguments[5], // maxY
                message.arguments[6], // image X size
                message.arguments[7]  // image Y size
            );
            break;
        case "draw-mode":
            drawMode(message.arguments[0] == "on");
            break;
        case "remove-map":
            removeMap(message.arguments[0]);
            break;
        case "remove-maps":
            removeAllMaps();
            break;
        case "set-opacity":
            setOpacity(message.arguments[0], message.arguments[1]);
            break;
        case "reset-map":
            resetMap();
            break;
        case "draw-flows":
            // TODO params
            drawFlows(message.arguments[0]);
            break;
        case "draw-plots":
        	// TODO add data labels etc.; support transects; may use x other than time
            drawPlots(
            		message.arguments[0],  // variable name
            		message.arguments[1],  // time array (millis)
            		message.arguments[2]); // data array (numeric)
            break;
        case "draw-timeline":
            // TODO params
            drawTimeline();
            break;
        case "switch-view":
            switchView(message.arguments[0]);
            break;
        case "add-feature":
            addFeature(
                message.arguments[0], // shape wkt
                message.arguments[1], // name
                message.arguments[2], // id
                (message.arguments.length > 3 ? message.arguments[3] : "#ff0000"), // color
                (message.arguments.length > 4 ? message.arguments[4] : null), // fillColor
                (message.arguments.length > 5 ? message.arguments[5] : 1.0) // opacity
            );
            break;
        case "add-marker":
            addMarker(
            		message.arguments[0], // name 
            		message.arguments[1], // id
            		message.arguments[2], // x
            		message.arguments[3]); // y
            break;
        case "show-marker":
            showMarker(message.arguments[0]);
            break;
        case "hide-marker":
            hideMarker(message.arguments[0]);
            break;
        case "show-feature":
            showFeature(message.arguments[0]);
            break;
        case "hide-feature":
            hideFeature(message.arguments[0]);
            break;
        case "show-concepts":
            describeConcepts(message.arguments[0], "semquery");
            switchView("query");
            break;
        case "show-subjects":
            describeSubjects(message.arguments[0], "semquery");
            switchView("query");
            break;
        case "redraw-map":
        	redrawMap();
        	break;
    }
}
