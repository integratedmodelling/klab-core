/**
 * Created by Ferd on 10/27/2015.
 */

function drawPlots(varname, time, numdata) {
	
	var table = new google.visualization.DataTable();
	table.addColumn('datetime', 'Time');
	table.addColumn('number', varname);
	
	for (var i = 0; i < time.length; i++) {

		/*
		 * workaround for something that should not happen.
		 */
		if (time[i] <= 0) {
			continue;
		}
		
		/*
		 * NaN arrives as string through JSON.
		 */
		var ddata = numdata[i];
		if (typeof ddata === 'string') {
			ddata = Number.NaN;
		}
		table.addRow([new Date(time[i]), ddata])
	}
	
	var options = {
		curveType : 'function',
		legend : {
			position : 'bottom'
		}
	};

	var chart = new google.visualization.LineChart(document
			.getElementById('timeplot'));

	chart.draw(table, options);
}