import * as types from '../mutation-types'

const state = {
  token: null,
  username: null
}

const getters = {
  isAuthenticated: function (state) { return state.token !== null },
  getUsername: function (state) { return state.username }
}

const actions = {
  authorize: function ({commit, state}, userProfile) {
    //  const savedCartItems = [...state.added]
    commit(types.AUTHORIZE_USER)
    //  shop.buyProducts(
    //    products,
    //     () => commit(types.CHECKOUT_SUCCESS),
    //    () => commit(types.CHECKOUT_FAILURE, { savedCartItems })
    //  )
  }
}

const mutations = {
  [types.AUTHORIZE_USER] (state, userProfile) {
    // rollback to the cart saved before sending the request
    state.token = userProfile.authToken
    state.username = userProfile.username
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
