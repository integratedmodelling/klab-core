const _url = 'http://127.0.0.1:8151/portal'

export var endpoints = {
  AUTHENTICATION_USERNAME: '/authentication',
  AUTHENTICATION_CERTIFICATE: '/authentication/cert-file',
  AUTHENTICATION_REGISTER: '/profile' /* with POST - TODO I see the point but maybe change to more expressive name? */
}

export const url = function (endpoint) {
  // TODO pass and substitute parameters
  return _url + endpoint
}
