const POSITION = 'bottom right'

export function serverError (response, context) {
  if (response.error && response.exception) {
    context.$notify({
      type: 'error',
      group: 'global',
      title: 'Server error',
      text: (response.message ? response.message : 'No explanation available'),
      position: POSITION
    })
    return true
  }
  return false
}
