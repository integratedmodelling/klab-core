package org.integratedmodelling.auth.indirect;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.common.auth.User;
import org.integratedmodelling.common.auth.UserAuthorizationProvider;
import org.integratedmodelling.common.beans.requests.AuthorizationRequest;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

/**
 * Indirect authorization provider that delegates authentication to one or more other
 * providers (using the API endpoints) and keeps a catalog of authorization tokens.
 * 
 * If a specific user in the user.properties file, remote authorization is skipped and
 * authentication proceeds directly if certified. FIXME this will need double-checking for
 * security - for now it's useful if offline.
 * 
 * Predefines the core IM authenticator(s) if none is given in application config.
 * 
 * In a Spring environment, just scan this package to provide indirect authorization.
 * 
 * @author ferdinando.villa
 *
 */
@Component
public class IndirectAuthorizationProvider implements UserAuthorizationProvider {

    Map<String, IUser> authorizations = new HashMap<>();

    @Value("${authentication.endpoints:https://integratedmodelling.org/collaboration}")
    String[] authEndpoints;

    HashMap<String, IUser> authenticated = new HashMap<>();

    @Override
    public AuthorizationResponse authenticateUser(String username, String password)
            throws KlabAuthorizationException {

        RestTemplate template = new RestTemplate();
        
        AuthorizationRequest request = new AuthorizationRequest();
        request.setUsername(username);
        request.setPassword(password);
        
        for (String authEndpoint : authEndpoints) {
            try {
                KLAB.info("delegating authentication to " + authEndpoint
                        + API.AUTHENTICATION);
                AuthorizationResponse ret = template.postForObject(authEndpoint
                        + API.AUTHENTICATION, request, AuthorizationResponse.class);
                authorize(ret);
                return ret;
            } catch (Throwable e) {
                KLAB.warn(e);
            }
        }
        throw new KlabAuthorizationException("authorization failed");
    }

    @Override
    public AuthorizationResponse authenticateUser(String certificate) throws KlabAuthorizationException {

        /*
         * TODO endpoint in modeling engine must be the primary server ONLY.
         */

        RestTemplate template = new RestTemplate();
        for (String authEndpoint : authEndpoints) {
            try {
                KLAB.info("delegating certificate authentication to " + authEndpoint
                        + API.AUTHENTICATION_CERT_FILE);
                AuthorizationResponse ret = template.postForObject(authEndpoint
                        + API.AUTHENTICATION_CERT_FILE, certificate, AuthorizationResponse.class);
                authorize(ret);
                return ret;
            } catch (Throwable e) {
                KLAB.warn(e);
            }
        }
        throw new KlabAuthorizationException("authorization failed");
    }

    private void authorize(AuthorizationResponse ret) {
        IUser user = new User(ret);
        authenticated.put(user.getSecurityKey(), user);
    }

    @Override
    public IUser getAuthenticatedUser(String token) throws KlabAuthorizationException {
        return authorizations.get(token);
    }

    @Override
    public ConnectionAuthorization getAuthorization(String token) {
        IUser user = authenticated.get(token);
        if (user == null) {
            return null;
        }
        return new ConnectionAuthorization(user);
    }

    /**
     * Set the endpoints used for authorization. They will be tried in sequence.
     * 
     * @param urls
     */
    public void setAuthorizationEndpoints(String... urls) {
        this.authEndpoints = urls;
    }

    @Override
    public boolean isAuthorizationDirect() {
        return false;
    }

}
