/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.errormanagement;

import org.integratedmodelling.api.errormanagement.ICompileNotification;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.configuration.KLAB;

public abstract class CompileNotification implements ICompileNotification {

    int    line      = 0;
    String namespace;
    String message;
    long   timestamp = System.currentTimeMillis();

    protected CompileNotification(String namespace, String message, int line) {
        this.line = line;
        this.message = message;
        this.namespace = namespace;
    }

    protected CompileNotification(INamespace namespace, String message, int line) {
        this.line = line;
        this.message = message;
        this.namespace = namespace == null ? "" : namespace.getId();
    }

    @Override
    public int getLineNumber() {
        return line;
    }

    @Override
    public INamespace getNamespace() {
        return namespace == null ? null : KLAB.MMANAGER.getNamespace(namespace);
    }

    @Override
    public String getMessage() {
        return message;
    }

}
