package org.integratedmodelling.common.monitoring;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.logging.Level;

import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.errormanagement.ICompileInfo;
import org.integratedmodelling.api.errormanagement.ICompileNotification;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.beans.Notification;
import org.integratedmodelling.common.beans.generic.Graph;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.errormanagement.CompileError;
import org.integratedmodelling.common.errormanagement.CompileInfo;
import org.integratedmodelling.common.errormanagement.CompileWarning;

/**
 * Base class for anything that can collect a set of timestamped notifications.
 * 
 * @author Ferd
 *
 */
public class Notifiable {

    protected Deque<ICompileNotification> compileNotifications = new ArrayDeque<>();

    private IMonitor runtimeMonitor;

    protected void addNotification(ICompileNotification notification) {
        synchronized (compileNotifications) {
            compileNotifications.push(notification);
        }
    }

    protected void addMonitor(IMonitor monitor) {
        this.runtimeMonitor = monitor;
    }

    /**
     * Accept a notification and process it to keep state actualized. Default 
     * implementation simply creates compile notifications from it; redefine
     * to implement state change for specific messages.
     * 
     * @param notification
     */
    public void acceptNotification(Notification notification) {

        if (notification.getBody() != null) {
            if (notification.loggingLevel() == Level.INFO) {
                addNotification(new CompileInfo(notification.getBody(), notification.getNotificationClass()));
                if (runtimeMonitor != null) {
                    runtimeMonitor.info(notification, notification.getNotificationClass());
                }
            } else if (notification.loggingLevel() == Level.WARNING) {
                addNotification(new CompileWarning(notification.getBody()));
                if (runtimeMonitor != null) {
                    runtimeMonitor.warn(notification);
                }
            } else if (notification.loggingLevel() == Level.SEVERE) {
                addNotification(new CompileError(notification.getBody()));
                if (runtimeMonitor != null) {
                    runtimeMonitor.error(notification);
                }
            } else if (notification.loggingLevel() == Level.FINE) {
                if (runtimeMonitor != null) {
                    runtimeMonitor.debug(notification);
                }
            } else if (notification.loggingLevel() == Level.CONFIG) {
                IModelBean bean = null;
                try {
                    @SuppressWarnings("unchecked")
                    Class<? extends IModelBean> breceiver = (Class<? extends IModelBean>) Class
                            .forName(notification.getNotificationClass());
                    bean = ((IModelingEngine) KLAB.ENGINE).getNotificationBus()
                            .fromJSON(notification.getBody(), breceiver);
                    receive(bean);
                } catch (Throwable e) {
                    KLAB.warn(e);
                    return;
                }
            }
        }
    }

    protected void receive(IModelBean object) {
    }

    /**
     * Return all notifications, earliest first.
     * @return all notifications from earliest to latest.
     */
    public List<ICompileNotification> getNotifications() {
        synchronized (compileNotifications) {
            return new ArrayList<>(compileNotifications);
        }
    }

    /**
     * Return all notifications, earliest first.
     * @return all errors seen in this context
     */
    public List<ICompileError> getErrors() {
        List<ICompileError> ret = new ArrayList<>();
        synchronized (compileNotifications) {
            for (ICompileNotification n : compileNotifications) {
                if (n instanceof ICompileError) {
                    ret.add((ICompileError) n);
                }
            }
        }
        return ret;
    }

    /**
     * Return all notifications, earliest first.
     * @return all warnings seen in this context
     */
    public List<ICompileWarning> getWarnings() {
        List<ICompileWarning> ret = new ArrayList<>();
        synchronized (compileNotifications) {
            for (ICompileNotification n : compileNotifications) {
                if (n instanceof ICompileWarning) {
                    ret.add((ICompileWarning) n);
                }
            }
        }
        return ret;
    }

    /**
     * Return all notifications, earliest first.
     * @return all info seen in this context
     */
    public List<ICompileInfo> getInfo() {
        List<ICompileInfo> ret = new ArrayList<>();
        synchronized (compileNotifications) {
            for (ICompileNotification n : compileNotifications) {
                if (n instanceof ICompileInfo) {
                    ret.add((ICompileInfo) n);
                }
            }
        }
        return ret;
    }

}
