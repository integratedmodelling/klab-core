/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.vocabulary.authority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.common.beans.authority.Authority;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.exceptions.KlabValidationException;

public class AuthorityFactory {

    HashMap<String, IAuthority<AuthorityConcept>> _authorities = new HashMap<>();
    private static AuthorityFactory               _this;

    public static AuthorityFactory get() {
        if (_this == null) {
            _this = new AuthorityFactory();
        }
        return _this;
    }

    public void registerAuthority(IAuthority<AuthorityConcept> authority) {
        
        if (authority != null) {
            _authorities.put(authority.getAuthorityId(), authority);
        }
    }

    /**
     * True if an authority named authorityId exists
     * @param authorityId
     * @return
     */
    public IAuthority<AuthorityConcept> getAuthorityNamed(String authorityId) {
        return _authorities.get(authorityId);
    }
    
    /**
     * Return the given authority if existing. Only throws an exception if request is for
     * a specific non-existing view in an existing authority. Otherwise will return null
     * if no authority with the ID exists, or the authority itself.
     * 
     * @param id
     * @return
     * @throws KlabValidationException
     */
    public IAuthority<AuthorityConcept> getAuthorityView(String id) throws KlabValidationException {

        String rid = id;
        if (id.contains(".")) {
            rid = Path.getFirst(id, ".");
        }

        IAuthority<AuthorityConcept> ret = _authorities.get(rid);
        if (ret != null) {
            if (!ret.getAuthorityIds().contains(id)) {
                throw new KlabValidationException("authority " + ret.getAuthorityId()
                        + " does not provide requested view " + id + ": available views are "
                        + StringUtils.join(ret.getAuthorityIds(), ", "));
            }
        }

        return ret;
    }

    public Collection<String> getAuthorityIds() {
        return _authorities.keySet();
    }

    public Collection<String> getAuthorityViewIds() {
        List<String> ret = new ArrayList<>();
        for (IAuthority<?> auth : _authorities.values()) {
            for (String id : auth.getAuthorityIds()) {
                ret.add(id);
            }
        }
        Collections.sort(ret);
        return ret;
    }

    
    /**
     * Add a client for a remote authority unless a native one was provided at
     * initialization.
     * 
     * @param pr
     */
    public void addRemoteAuthority(Authority pr) {
        if (!_authorities.containsKey(pr.getName())) {

            // filter authorities that don't belong to current worldview and warn
            if (KLAB.getWorldview() != null && pr.getWorldview() != null
                    && !KLAB.getWorldview().equals(pr.getWorldview())) {
                KLAB.warn("remote authority " + pr.getName()
                        + " is incompatible with current worldview: are you using the right certificate?");
                return;
            }
            _authorities.put(pr.getName(), new ClientAuthority(pr));
        }
    }

    public Collection<IAuthority<AuthorityConcept>> getAuthorities() {
        return _authorities.values();
    }

}
