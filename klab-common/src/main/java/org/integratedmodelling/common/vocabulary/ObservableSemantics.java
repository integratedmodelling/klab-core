/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.common.kim.KIMScope;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.lang.Axiom;

/**
 * Works as a delegate for a non-observed concept (e.g. ElevationSeaLevel) but provides
 * the info that describe the semantics of its observable and observation. On request it
 * can incarnate the observable in a passed context.
 * 
 * @author Ferd
 * 
 */
public class ObservableSemantics
        implements IObservableSemantics, NetworkSerializable, NetworkDeserializable {

    IConcept                                          mainType           = null;
    IConcept                                          obsType            = null;
    String                                            formalName         = null;
    Action                                            action             = Action.EXPLAIN;

    // model and observer should be stored with the model, and restored at
    // initializeFromStorage().
    IModel                                            model              = null;
    IObserver                                         observer           = null;
    IConcept                                          semantics          = null;

    // in dependencies, we may request a local concept as the final output ('C as
    // <observer>'). If
    // so, we keep it here without changing the stated observer type.
    IConcept                                          localConcept;

    /*
     * TODO whether we want to instantiate or explain direct objects. For now this is just
     * a gimmick to solve some difficult situations in resolution, and the "actual" info
     * is in the IResolutionContext, but it should probably be moved here and removed from
     * there, as it's very hard to predict all cases and new observers may need different
     * strategies.
     */
    boolean                                           isInstantiator     = false;

    /**
     * this is set when deserialized, and must be linked to a real subject after the
     * observation hierarchy is completed.
     * observing subject makes two observables of the same thing reflect different POVs.
     */
    String                                            observingSubjectId = null;

    private boolean                                   forceProbabilistic;
    private boolean                                   isAbstract         = false;

    /*
     * roles are cached to avoid having to retrieve them too many times.
     */
    Collection<Triple<IConcept, IConcept, IProperty>> roles              = null;

    // we keep a catalog of delegate concept -> its observable. Faster and easier than
    // doing everything with actual OWL restrictions.
    static Map<IKnowledge, IObservableSemantics>      _catalog           = new HashMap<>();

    // for the DB
    public ObservableSemantics() {
    }

    // public Observable(Map<?, ?> map) {
    //
    //
    // }

    public ObservableSemantics(IConcept observedConcept, IConcept observationType,
            String name) {

        mainType = observedConcept;
        obsType = observationType;
        formalName = name;
    }

    public void setLocalConcept(IConcept localConcept) {
        this.localConcept = localConcept;
    }

    public ObservableSemantics(IConcept observedConcept, IConcept observationType,
            IConcept subjectType,
            String name, boolean reification) {
        this.mainType = observedConcept;
        this.obsType = observationType;
        this.formalName = name;
        this.action = reification ? Action.INSTANTIATE : Action.EXPLAIN;
    }

    public ObservableSemantics(IObserver observer, IDirectObservation observingSubject,
            String name) {
        this.mainType = observer.getObservable().getType();
        this.obsType = observer.getObservable().getObservationType();
        if (observingSubject != null) {
            this.observingSubjectId = ((Observation)observingSubject).getInternalId();
            // FIXME - confusing, unsafe and ugly.
            ((ObservableSemantics) observer
                    .getObservable()).observingSubjectId = ((Observation)observingSubject).getInternalId();
        }

        this.observer = observer;
        this.formalName = name;
        this.action = Action.EXPLAIN;
    }

    /**
     * Passed a delegate observation type stated in a model's specification, which is used
     * to reference the observed types of the passed model. This stores the observable in
     * the catalog.
     * 
     * @param concept
     * @param model
     * @param name
     */
    public ObservableSemantics(IConcept concept, IModel model, String name) {

        this(model, name);

        /*
         * make the concept a subclass of mainType if it's not already...
         */
        if (mainType != null && !concept.is(mainType) && concept.getOntology() != null) {
            concept.getOntology()
                    .define(Collections.singleton(Axiom
                            .SubClass(mainType.toString(), concept.toString())));
        }

        /*
         * ...then make it the main type...
         */
        mainType = concept;

        /*
         * ...and remember the observable for this delegate so we don't make mistakes
         * attributing it to an incompatible observation type.
         */
        _catalog.put(concept, this);
    }

    /**
     * Create from an initialized observer (promoted to model). Used to define the
     * delegate observation type for models where the observable is anonymous, e.g. "model
     * 100 as measure Elevation in m".
     * 
     * @param model
     * @param name
     * 
     */
    public ObservableSemantics(IModel model, String name) {

        if (model.getObservable() != null) {
            mainType = model.getObservable().getType();
            obsType = model.getObservable().getObservationType();
        } else if (model.getObserver()
                .getObservable() != null /* happens only in error */) {
            mainType = model.getObserver().getObservable().getType();
            obsType = model.getObserver().getObservable().getObservationType();
        }
        this.observer = model.getObserver();
        this.model = model;
        this.formalName = name == null
                ? (CamelCase.toLowerCase(NS.getDisplayName(mainType), '-')) : name;
        this.action = model.isInstantiator() ? Action.INSTANTIATE : Action.EXPLAIN;
    }

    /**
     * Constructor that will set the observation type to the most general for the passed
     * observed concept.
     * 
     * @param concept
     */
    public ObservableSemantics(IConcept concept) {
        if (concept != null) {
            mainType = concept;
            obsType = NS.isObject(concept) ? KLAB.c(NS.DIRECT_OBSERVATION)
                    : KLAB.c(NS.INDIRECT_OBSERVATION);
            formalName = CamelCase.toLowerCase(NS.getDisplayName(concept), '-');
        }
    }

    public ObservableSemantics(ObservableSemantics o) {

        if (o == null) {
            formalName = "error";
            return;
        }

        mainType = o.mainType;
        obsType = o.obsType;
        formalName = o.formalName;
        observer = o.observer;
        model = o.model;
        semantics = o.semantics;
        observingSubjectId = o.observingSubjectId;
        isInstantiator = o.isInstantiator;
        localConcept = o.localConcept;
        if (o.roles != null) {
            roles = new ArrayList<>();
            roles.addAll(o.roles);
        }
    }

    public void setObservingSubject(IDirectObservation subject) {
        observingSubjectId = ((Observation)subject).getInternalId();
    }

    public static IObservableSemantics getObservable(IConcept delegate) {
        return _catalog.get(delegate);
    }

    public boolean isInstantiator() {
        return isInstantiator;
    }

    // util to keep any semantic dependencies and their roles in this observable between
    // calls.
    Collection<Triple<IConcept, IConcept, IProperty>> getRoles() {
        if (roles == null) {
            roles = NS.getSemanticDependencies(mainType);
        }
        return roles;
    }

    @Override
    public IConcept getType() {
        return mainType;
    }

    /**
     * Main type ignoring the local concept if any.
     * 
     * @return
     */
    public IConcept getLocalType() {
        return localConcept;
    }

    public void setFormalName(String name) {
        formalName = name;
    }

    @Override
    public String toString() {
        /*
         * must serialize back to the represented concept, otherwise storage of
         * observables won't work properly.
         */
        return "[" + (obsType == null ? "NULL" : obsType.getLocalName()) + // DirectObservation
                " of " + // of
                (mainType == null ? "NULL" : mainType.toString()) +
                "]";
    }

    @Override
    public String getLocalName() {
        return mainType.getLocalName();
    }

    @Override
    public boolean is(IObservableSemantics concept) {

        boolean conceptOK = mainType.is(concept.getType());
        boolean obsOK = obsType.is(concept.getObservationType());
        return conceptOK && obsOK;
    }

    @Override
    public boolean is(IKnowledge concept) {
        return mainType == null ? false : mainType.is(concept);
    }

    /**
     * 
     * @param observationType
     * @param observedType
     * @return true if subsumption can be asserted
     */
    @Override
    public boolean is(IConcept observationType, IConcept observedType) {

        boolean conceptOK = mainType.is(observedType);
        boolean obsOK = obsType.is(observationType);
        return conceptOK && obsOK;
    }

    /**
     * 
     * @param observationType
     * @param observedType
     * @param inherentToType
     * @return true if subsumption can be asserted
     */
    @Override
    public boolean is(IConcept observationType, IConcept observedType, IConcept inherentToType) {

        boolean conceptOK = mainType.is(observedType);
        boolean obsOK = obsType.is(observationType);
        return conceptOK && obsOK; // && inhOK;
    }

    /**
     * 
     * @param observationType
     * @param observedType
     * @param inherentToType
     * @return true if subsumption can be asserted
     */
    @Override
    public boolean is(IConcept observationType, IConcept observedType, IConcept inherentToType, IConcept traitType) {

        boolean conceptOK = mainType.is(observedType);
        boolean obsOK = obsType.is(observationType);
        return conceptOK && obsOK;
    }

    public String getConceptSpace() {
        return mainType.getConceptSpace();
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public IObserver getObserver() {
        return observer;
    }

    @Override
    public IConcept getObservationType() {
        return obsType;
    }

    @Override
    public String getFormalName() {
        return formalName;
    }

    @Override
    public int hashCode() {
        return getSignature().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ObservableSemantics
                && getSignature().equals(((ObservableSemantics) o).getSignature());
    }

    String _signature = null;

    private String getSignature() {
        if (_signature == null) {
            _signature = mainType.getDefinition() + "|" + obsType;
            if (observingSubjectId != null) {
                _signature += "|" + observingSubjectId;
            }
        }
        return _signature;
    }

    public void setModel(IModel iModel) {
        model = iModel;
        if (iModel != null) {
            observer = iModel.getObserver();
        }
    }

    public void setObservationType(IConcept c) {
        obsType = c;
    }

    public void setObservedType(IConcept c) {
        mainType = c;
    }

    public IObservableSemantics setInstantiator(boolean b) {
        isInstantiator = b;
        return this;
    }

    public void setType(IConcept type) {
        mainType = type;
    }

    public void setType(IKnowledgeObject type, KIMScope context) {
        mainType = type.getType();
        if (this.formalName == null && mainType != null) {
            formalName = CamelCase.toLowerCase(NS.getDisplayName(this.mainType), '-');
        }
    }

    public ObservableSemantics withType(IConcept type) {
        ObservableSemantics ret = new ObservableSemantics(this);
        ret.setType(type);
        return ret;
    }

    public void setObserver(IObserver observer) {
        this.observer = observer;
    }

    @Override
    public Action getAction() {
        return action;
    }

    public String getObservingSubjectId() {
        return observingSubjectId;
    }

    public ObservableSemantics withInterpretingTrait(IConcept interpretAs)
            throws KlabValidationException {
        if (interpretAs != null) {
            return withType(Observables
                    .declareObservable(getType(), Collections
                            .singleton(interpretAs), null, null));
        }
        return this;
    }

    public boolean equalsWithoutInherency(IObservableSemantics observable) {
        return getType().equals(observable.getType())
                &&
                getObservationType().equals(observable.getObservationType())
                &&
                // jesus
                ((observingSubjectId == null
                        && ((ObservableSemantics) observable).observingSubjectId == null)
                        || (observingSubjectId != null
                                && ((ObservableSemantics) observable).observingSubjectId != null
                                && observingSubjectId
                                        .equals(((ObservableSemantics) observable).observingSubjectId)));
    }

    public String getExportFileName() {

        String ret = CamelCase.toLowerCase(NS.getDisplayName(getType()), '_');
        if (observingSubjectId != null) {
            ret = observingSubjectId + "_" + ret;
        }
        return ret;
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observable)) {
            throw new KlabRuntimeException("cannot deserialize an Observable from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observable bean = (org.integratedmodelling.common.beans.Observable) object;

        mainType = Knowledge.parse(bean.getType());
        if (bean.getLocalType() != null) {
            localConcept = Knowledge.parse(bean.getLocalType());
        }
        formalName = bean.getName();
        observingSubjectId = bean.getObservingSubject();

        if (bean.getObservationType() != null) {
            obsType = KLAB.c(bean.getObservationType());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass
                .isAssignableFrom(org.integratedmodelling.common.beans.Observable.class)) {
            throw new KlabRuntimeException("cannot serialize an Observable to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observable ret = new org.integratedmodelling.common.beans.Observable();
        if (mainType != null)
            ret.setType(mainType.getDefinition());
        if (obsType != null)
            ret.setObservationType(obsType.toString());
        if (localConcept != null) {
            ret.setLocalType(localConcept.getDefinition());
        }
        if (formalName != null)
            ret.setName(formalName);
        if (observingSubjectId != null) {
            ret.setObservingSubject(observingSubjectId);
        }

        return (T) ret;
    }

    public IObservableSemantics forceProbabilistic() {
        ObservableSemantics ret = new ObservableSemantics(this);
        ret.forceProbabilistic = true;
        return ret;
    }

    public boolean isProbabilistic() {
        return forceProbabilistic;
    }

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setAbstract(boolean b) {
        isAbstract = b;
    }

    public IObservableSemantics copy() {
        ObservableSemantics ret = new ObservableSemantics(this);
        if (ret.observer != null) {
            ret.observer = ((KIMObserver)ret.observer).copy();
        }
        return ret;
    }

}
