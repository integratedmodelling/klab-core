/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRankingObserver;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Concept;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabInternalErrorException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.lang.Axiom;

/**
 * One and only holder of semantic concept IDs. These are all expected to exist in the
 * default core knowledge base, known to both clients and servers.
 * 
 * @author Ferd
 */
public class NS {

    /**
     * Sometimes we want these without referring to actual concepts.
     * 
     * @author Ferd
     */
    public static enum CoreType {
        SUBJECT,
        PROCESS,
        EVENT,
        QUALITY,
        TRAIT,
        RELATIONSHIP
    };

    private static Set<String> nothings                               = new HashSet<>();

    /*
     * these are only available after NS.synchronize() has returned true.
     */
    public static IConcept     DATA_REDUCTION_TRAIT;
    public static IConcept     AVERAGE_TRAIT;
    public static IConcept     MINIMUM_TRAIT;
    public static IConcept     MAXIMUM_TRAIT;
    public static IConcept     LENGTH;
    public static IConcept     VOLUME;
    public static IConcept     DISTANCE;
    public static IConcept     AREA;
    public static IConcept     FLOW;

    /*
     * traits to categorize inputs and outputs
     */
    public static IConcept     HOURLY_TRAIT                           = null;
    public static IConcept     DAILY_TRAIT                            = null;
    public static IConcept     WEEKLY_TRAIT                           = null;
    public static IConcept     MONTHLY_TRAIT                          = null;
    public static IConcept     YEARLY_TRAIT                           = null;

    /*
     * traits and roles to support internal operations
     */
    public static IConcept     VISIBLE_TRAIT                          = null;
    public static IConcept     ARCHETYPE_ROLE                         = null;
    public static IConcept     EXPLANATORY_QUALITY_ROLE               = null;
    public static IConcept     LEARNED_QUALITY_ROLE                   = null;
    public static IConcept     LEARNING_PROCESS_ROLE                  = null;

    // domain concepts for known extents
    public static final String SPACE_DOMAIN                           = "observation:Space";
    public static final String TIME_DOMAIN                            = "observation:Time";

    // core properties
    public static final String IS_ABSTRACT                            = "observation:isAbstract";
    public static final String BASE_DECLARATION                       = "observation:baseDeclaration";
    public static final String ORDER_PROPERTY                         = "observation:orderingRank";
    public static final String HAS_REALM_PROPERTY                     = "observation:hasRealm";
    public static final String HAS_IDENTITY_PROPERTY                  = "observation:hasIdentity";
    public static final String HAS_ATTRIBUTE_PROPERTY                 = "observation:hasAttribute";
    public static final String HAS_CONTEXT_PROPERTY                   = "observation:hasContext";
    public static final String HAS_SUBJECTIVE_TRAIT_PROPERTY          = "observation:hasSubjectiveTrait";
    public static final String IS_SUBJECTIVE                          = "observation:isSubjectiveTrait";
    public static final String IS_INHERENT_TO_PROPERTY                = "observation:isInherentTo";
    public static final String HAS_ROLE_PROPERTY                      = "observation:hasRole";
    public static final String EXPOSES_TRAIT_PROPERTY                 = "observation:exposesTrait";
    public static final String DENIABILITY_PROPERTY                   = "observation:isDeniable";
    public static final String IMPLIES_OBSERVABLE_PROPERTY            = "observation:impliesObservable";
    public static final String IMPLIES_ROLE_PROPERTY                  = "observation:impliesRole";
    public static final String APPLIES_TO_PROPERTY                    = "observation:appliesTo";
    public static final String IMPLIES_SOURCE_PROPERTY                = "observation:impliesSource";
    public static final String IMPLIES_DESTINATION_PROPERTY           = "observation:impliesDestination";
    public static final String CONFERS_TRAIT_PROPERTY                 = "observation:confersTrait";
    public static final String DESCRIBES_QUALITY_PROPERTY             = "observation:describesQuality";
    public static final String LIMITED_BY_PROPERTY                    = "observation:limitedBy";
    public static final String REPRESENTED_BY_PROPERTY                = "observation:representedBy";
    public static final String IS_TYPE_DELEGATE                       = "observation:isTypeDelegate";

    // core observation ontology
    public static final String OBSERVATION                            = "observation:Observation";
    public static final String DIRECT_OBSERVATION                     = "observation:DirectObservation";
    public static final String INDIRECT_OBSERVATION                   = "observation:IndirectObservation";
    public static final String CLASSIFICATION                         = "observation:Classification";
    public static final String MEASUREMENT                            = "observation:Measurement";
    public static final String QUANTIFICATION                         = "observation:Quantification";
    public static final String RANKING                                = "observation:Ranking";
    public static final String COUNT_OBSERVATION                      = "observation:CountObservation";
    public static final String PERCENTAGE_OBSERVATION                 = "observation:PercentageObservation";
    public static final String PROPORTION_OBSERVATION                 = "observation:ProportionObservation";
    public static final String RATIO_OBSERVATION                      = "observation:RatioObservation";
    public static final String DISTANCE_OBSERVATION                   = "observation:DistanceMeasurement";
    public static final String VALUE_OBSERVATION                      = "observation:Valuation";
    public static final String PROBABILITY_OBSERVATION                = "observation:ProbabilityObservation";
    public static final String UNCERTAINTY_OBSERVATION                = "observation:UncertaintyObservation";
    public static final String PRESENCE_OBSERVATION                   = "observation:PresenceObservation";

    // annotation property that specifies the base SI unit for a physical property
    public static final String SI_UNIT_PROPERTY                       = "observation:unit";

    // im ontology annotations affecting the ranking system. Used as key in
    // maps, so
    // they don't depend on the ontology being in the system.
    public static final String LEXICAL_SCOPE                          = "im:lexical-scope";
    public static final String TRAIT_CONCORDANCE                      = "im:trait-concordance";
    public static final String SEMANTIC_DISTANCE                      = "im:semantic-concordance";
    public static final String SCALE_COVERAGE                         = "im:scale-coverage";
    public static final String SCALE_SPECIFICITY                      = "im:scale-specificity";
    public static final String INHERENCY                              = "im:inherency";
    public static final String EVIDENCE                               = "im:evidence";
    public static final String NETWORK_REMOTENESS                     = "im:network-remoteness";
    public static final String SCALE_COHERENCY                        = "im:scale-coherency";
    public static final String SUBJECTIVE_CONCORDANCE                 = "im:subjective-concordance";

    // only annotation used for subjective ranking in the default behavior
    public static final String RELIABILITY                            = "im:reliability";

    /*
     * model objects for function return types and the like
     */
    public static final String INTEGER                                = "klab:ShortInteger";
    public static final String FLOAT                                  = "klab:ShortFloat";
    public static final String TEXT                                   = "klab:Text";
    public static final String LONG                                   = "klab:LongInteger";
    public static final String DOUBLE                                 = "klab:LongFloat";
    public static final String BOOLEAN                                = "klab:Boolean";
    public static final String NUMBER                                 = "klab:Number";
    public static final String SHAPE                                  = "klab:Shape";

    public static final String STATE_CONTEXTUALIZER                   = "klab:StateContextualizer";
    public static final String SUBJECT_CONTEXTUALIZER                 = "klab:SubjectContextualizer";
    public static final String PROCESS_CONTEXTUALIZER                 = "klab:ProcessContextualizer";
    public static final String EVENT_INSTANTIATOR                     = "klab:EventInstantiator";
    public static final String SUBJECT_INSTANTIATOR                   = "klab:SubjectInstantiator";
    public static final String EVENT_CONTEXTUALIZER                   = "klab:EventContextualizer";
    public static final String RELATIONSHIP_INSTANTIATOR              = "klab:RelationshipInstantiator";
    public static final String FUNCTIONAL_RELATIONSHIP_CONTEXTUALIZER = "klab:FunctionalRelationshipContextualizer";
    public static final String STRUCTURAL_RELATIONSHIP_CONTEXTUALIZER = "klab:StructuralRelationshipContextualizer";
    public static final String DATASOURCE                             = "klab:DataSource";
    public static final String OBJECTSOURCE                           = "klab:ObjectSource";
    public static final String LOOKUP_TABLE                           = "klab:LookupTable";

    /*
     * annotation properties supporting k.LAB functions
     */

    public static final String CONCEPT_DEFINITION_PROPERTY            = "klab:conceptDefinition";
    public static final String LOCAL_ALIAS_PROPERTY                   = "klab:localAlias";
    public static final String DISPLAY_LABEL_PROPERTY                 = "klab:displayLabel";
    public static final String AUTHORITY_ID_PROPERTY                  = "klab:authorityId";
    public static final String UNTRANSFORMED_CONCEPT_PROPERTY         = "klab:untransformedConceptId";
    public static final String ORIGINAL_TRAIT                         = "klab:originalTrait";

    /**
     * Annotation contains the ID of the property (in same ontology) that will be used to
     * create restrictions to adopt the trait carrying the annotation.
     */
    public static final String TRAIT_RESTRICTING_PROPERTY             = "klab:restrictingProperty";

    /*
     * the core properties we use internally to establish observation semantics
     */
    /**
     * The property that links an observation to its observable.
     */
    public static final String CONTEXTUALIZES                         = "observation:contextualizes";
    public static final String INHERENT_IN                            = "observation:isInherentTo";
    public static final String OBSERVED_INTO                          = "observation:hasContext";
    public static final String PART_OF                                = "observation:isPartOf";
    public static final String CONSTITUENT_OF                         = "observation:isConstituentOf";
    public static final String STRUCTURING_PROPERTY                   = "observation:structuringObjectProperty";
    public static final String MAXIMUM_TRAIT_CONCEPT                  = "IM.MAXIMUM_TRAIT";
    public static final String DEPENDS_ON_PROPERTY                    = "observation:dependsOn";
    public static final String RELATES_TO_PROPERTY                    = "observation:relatesTo";
    public static final String CONTAINS_PART_PROPERTY                 = "observation:containsPart";
    public static final String CONTAINS_PART_SPATIALLY_PROPERTY       = "observation:containsPartSpatially";
    public static final String OBSERVES_PROPERTY                      = "observation:observes";

    /**
     * The ontology for all the core concepts (which depends only on BFO).
     */
    public static final String CORE_ONTOLOGY                          = "observation";

    /**
     * Only class that subsumes both observables and observations. It's bfo:entity in
     * label.
     */
    public static final String CORE_PARTICULAR                        = "bfo:BFO_0000001";

    /**
     * Subsumes traits, domains and configurations. BFO does not still address universals,
     * so we provide it in observation.
     */
    public static final String CORE_UNIVERSAL                         = "observation:universal";

    /**
     * the root domain for the ontologies. For many reasons it becomes very difficult to
     * keep it in the imcore namespace, so we use the most general abstract in DOLCE.
     */
    public static final String CORE_DOMAIN                            = "observation:Domain";

    // core concepts to map with the language keywords
    public static final String CORE_OBSERVABLE                        = "observation:Observable";
    public static final String CORE_OBSERVATION                       = "observation:Observation";
    public static final String CORE_OBJECT                            = "observation:DirectObservable";
    public static final String CORE_PROCESS                           = "observation:Process";
    public static final String CORE_QUALITY                           = "observation:Quality";
    public static final String CORE_EVENT                             = "observation:Event";
    public static final String CORE_TRAIT                             = "observation:Trait";
    public static final String CORE_IDENTITY_TRAIT                    = "observation:Identity";
    // public static final String SUBJECTIVE_TRAIT = "observation:SubjectiveTrait";
    public static final String CORE_QUANTITY                          = "observation:QuantifiableQuality";
    public static final String CORE_NUMERIC_QUANTITY                  = "observation:ContinuousNumericallyQuantifiableQuality";
    public static final String CORE_ASSERTED_QUALITY                  = "observation:AssertedQuality";
    public static final String CORE_SUBJECT                           = "observation:Subject";
    public static final String CORE_PHYSICAL_OBJECT                   = "observation:PhysicalObject";
    public static final String CORE_PHYSICAL_PROPERTY                 = "observation:PhysicalProperty";
    public static final String CORE_EXTENSIVE_PHYSICAL_PROPERTY       = "observation:ExtensivePhysicalProperty";
    public static final String CORE_INTENSIVE_PHYSICAL_PROPERTY       = "observation:IntensivePhysicalProperty";
    public static final String CORE_ENERGY                            = "observation:Energy";
    public static final String CORE_ENTROPY                           = "observation:Entropy";
    public static final String CORE_LENGTH                            = "observation:Length";
    public static final String CORE_MASS                              = "observation:Mass";
    public static final String CORE_PROBABILITY                       = "observation:Probability";
    public static final String CORE_RELATIVE_QUANTITY                 = "observation:RelativeQuantity";
    public static final String CORE_VOLUME                            = "observation:Volume";
    public static final String CORE_WEIGHT                            = "observation:Weight";
    public static final String CORE_DURATION                          = "observation:Duration";
    public static final String CORE_MONETARY_VALUE                    = "observation:MonetaryValue";
    public static final String CORE_PREFERENCE_VALUE                  = "observation:PreferenceValue";
    public static final String CORE_ACCELERATION                      = "observation:Acceleration";
    public static final String CORE_AREA                              = "observation:Area";
    public static final String CORE_DENSITY                           = "observation:Density";
    public static final String CORE_ELECTRIC_POTENTIAL                = "observation:ElectricPotential";
    public static final String CORE_CHARGE                            = "observation:Charge";
    public static final String CORE_RESISTANCE                        = "observation:Resistance";
    public static final String CORE_RESISTIVITY                       = "observation:Resistivity";
    public static final String CORE_PRESSURE                          = "observation:Pressure";
    public static final String CORE_ANGLE                             = "observation:Angle";
    public static final String CORE_ASSESSMENT                        = "observation:Assessment";
    public static final String CORE_SPEED                             = "observation:Speed";
    public static final String CORE_TEMPERATURE                       = "observation:Temperature";
    public static final String CORE_VISCOSITY                         = "observation:Viscosity";
    public static final String CORE_AGENT                             = "observation:Agent";
    public static final String CORE_PATTERN                           = "observation:Configuration";
    public static final String CORE_RELATIONSHIP                      = "observation:Relationship";
    public static final String CORE_FUNCTIONAL_RELATIONSHIP           = "observation:FunctionalRelationship";
    public static final String CORE_STRUCTURAL_RELATIONSHIP           = "observation:StructuralRelationship";
    public static final String TYPE                                   = "observation:Type";
    public static final String ORDERING                               = "observation:Ordering";
    public static final String CORE_REALM_TRAIT                       = "observation:Realm";
    public static final String ATTRIBUTE_TRAIT                        = "observation:Attribute";
    public static final String ROLE_TRAIT                             = "observation:Role";
    public static final String CORE_COUNT                             = "observation:Numerosity";
    public static final String CORE_PROPORTION                        = "observation:Proportion";
    public static final String CORE_RATIO                             = "observation:Ratio";
    public static final String CORE_PRESENCE                          = "observation:Presence";
    public static final String CORE_VALUE                             = "observation:Value";
    public static final String CORE_DISTANCE                          = "observation:Distance";
    public static final String BASE_AGENT                             = "observation:Agent";
    public static final String REACTIVE_AGENT                         = "observation:ReactiveAgent";
    public static final String DELIBERATIVE_AGENT                     = "observation:DeliberativeAgent";
    public static final String INTERACTIVE_AGENT                      = "observation:InteractiveAgent";
    public static final String CORE_UNCERTAINTY                       = "observation:Uncertainty";
    public static final String OBSERVABILITY_TRAIT                    = "observation:Observability";
    public static final String ABSENCE_TRAIT                          = "observation:Absence";
    public static final String EXTENT                                 = "observation:Extent";

    /*
     * IM properties for visualization and ranking
     */
    public static final String NUMERIC_ENCODING_PROPERTY              = "im:numeric-encoding";
    public static final String COLORMAP_PROPERTY                      = "im:colormap";
    public static final String LOWER_BOUND_PROPERTY                   = "im:lower-bound";
    public static final String UPPER_BOUND_PROPERTY                   = "im:upper-bound";
    public static final String COLOR_PROPERTY                         = "im:color";
    public static final String OPENNESS_PROPERTY                      = "im:openness";
    public static final String RELIABILITY_PROPERTY                   = "im:reliability";
    public static final String PEER_REVIEW_PROPERTY                   = "im:peer-review";
    public static final String EFFICIENCY_PROPERTY                    = "im:efficiency";
    public static final String TESTING_PROPERTY                       = "im:testing";
    public static final String STATUS_PROPERTY                        = "im:status";
    public static final String INTEGRATION_PROPERTY                   = "im:integration";
    public static final String DESIGN_PROPERTY                        = "im:design";

    /*
     * internal use
     */
    public static final String INTERNAL_DATA_LABEL_PROPERTY           = "internal:data-label";
    public static final String AVERAGE_TRAIT_CONCEPT                  = "IM.AVERAGE_TRAIT";
    public static final String MINIMUM_TRAIT_CONCEPT                  = "IM.MINIMUM_TRAIT";
    public static final String HOURLY_TRAIT_CONCEPT                   = "IM.HOURLY_TRAIT";
    public static final String DAILY_TRAIT_CONCEPT                    = "IM.DAILY_TRAIT";
    public static final String MONTHLY_TRAIT_CONCEPT                  = "IM.MONTHLY_TRAIT";
    public static final String WEEKLY_TRAIT_CONCEPT                   = "IM.WEEKLY_TRAIT";
    public static final String YEARLY_TRAIT_CONCEPT                   = "IM.YEARLY_TRAIT";
    public static final String VISIBLE_TRAIT_CONCEPT                  = "IM.VISIBLE_TRAIT";
    public static final String ARCHETYPE_ROLE_CONCEPT                 = "IM.ARCHETYPE";
    public static final String EXPLANATORY_QUALITY_ROLE_CONCEPT       = "IM.EXPLANATORY_QUALITY";
    public static final String LEARNED_QUALITY_ROLE_CONCEPT           = "IM.LEARNED_QUALITY";
    public static final String LEARNING_PROCESS_ROLE_CONCEPT          = "IM.LEARNING_PROCESS";

    public static boolean isDerived(IKnowledge k) {
        return k instanceof IConcept && !((IConcept) k).getDefinition().equals(k.toString());
    }

    public static void registerNothingConcept(String s) {
        nothings.add(s);
    }

    // flags for concept recognition w/o inference - set in flagscomputed after
    // first
    // check, when the response is written in flags.
    private static final int ABSTRACT_FLAG                = 0x000001;
    private static final int STRUCTURAL_RELATIONSHIP_FLAG = 0x000002;
    private static final int PROCESS_FLAG                 = 0x000004;
    private static final int QUALITY_FLAG                 = 0x000008;
    private static final int ATTRIBUTE_FLAG               = 0x000010;
    private static final int ROLE_FLAG                    = 0x000020;
    private static final int REALM_FLAG                   = 0x000040;
    private static final int IDENTITY_FLAG                = 0x000080;
    private static final int TRAIT_FLAG                   = 0x000100;
    private static final int SUBJECT_FLAG                 = 0x000200;
    private static final int EVENT_FLAG                   = 0x000400;
    private static final int FUNCTIONAL_RELATIONSHIP_FLAG = 0x000800;
    private static final int TYPE_FLAG                    = 0x001000;
    private static final int PARTICULAR_FLAG              = 0x002000;
    private static final int CONFIGURATION_FLAG           = 0x004000;
    private static final int DOMAIN_FLAG                  = 0x008000;
    private static final int RELATIONSHIP_FLAG            = 0x010000;
    private static final int OBSERVABILITY_FLAG           = 0x020000;

    private static boolean checkFlags(IKnowledge k, int flag, String concept) {

        if (((ProjectManager) KLAB.PMANAGER).isParsing()) {
            /*
             * disable caching during parsing, when concepts are being defined.
             */
            return k.is(KLAB.c(concept));
        }

        if ((((Knowledge) k).flagscomputed & flag) == 0) {
            boolean ret = k.is(KLAB.c(concept));
            ((Knowledge) k).flagscomputed |= flag;
            if (ret) {
                ((Knowledge) k).flags |= flag;
            }
            return ret;
        }
        return (((Knowledge) k).flags & flag) != 0;
    }

    /**
     * True if this is an observable particular, but not an observation.
     * 
     * @param concept
     * @return
     */
    public static boolean isParticular(ISemantic concept) {
        return concept.getType() instanceof IConcept
                && checkFlags(concept.getType(), PARTICULAR_FLAG, CORE_PARTICULAR)
                && !concept.getType().is(KLAB.c(CORE_OBSERVATION));
    }

    public static boolean isClass(ISemantic o) {
        return checkFlags(o.getType(), TYPE_FLAG, TYPE);
    }

    public static boolean isOrdering(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept instanceof IConcept && concept.is(KLAB.c(ORDERING));
    }

    public static boolean isObservable(ISemantic o) {
        IKnowledge concept = o.getType();
        return concept.is(KLAB.c(CORE_OBSERVABLE));
    }

    /**
     * true for all objects - i.e., subjects, processes and events.
     * 
     * @param o
     * @return true if object
     */
    public static boolean isObject(ISemantic o) {
        return isThing(o) || isProcess(o) || isEvent(o);
    }

    public static boolean isFunctionalRelationship(ISemantic o) {
        return checkFlags(o.getType(), FUNCTIONAL_RELATIONSHIP_FLAG, CORE_FUNCTIONAL_RELATIONSHIP);
    }

    public static boolean isStructuralRelationship(ISemantic o) {
        return checkFlags(o.getType(), STRUCTURAL_RELATIONSHIP_FLAG, CORE_STRUCTURAL_RELATIONSHIP);
    }

    public static boolean isRelationship(ISemantic o) {
        return checkFlags(o.getType(), RELATIONSHIP_FLAG, CORE_RELATIONSHIP);
    }

    public static boolean isThing(ISemantic o) {
        return checkFlags(o.getType(), SUBJECT_FLAG, CORE_SUBJECT);
    }

    public static boolean isQuality(ISemantic o) {
        return checkFlags(o.getType(), QUALITY_FLAG, CORE_QUALITY);
    }

    public static boolean isCountable(ISemantic o) {
        return isThing(o) || isEvent(o) || isRelationship(o);
    }

    public static boolean isObservability(ISemantic o) {
        return checkFlags(o.getType(), OBSERVABILITY_FLAG, OBSERVABILITY_TRAIT);
    }

    public static boolean isProcess(ISemantic o) {
        return checkFlags(o.getType(), PROCESS_FLAG, CORE_PROCESS);
    }

    public static boolean isTrait(ISemantic o) {
        return checkFlags(o.getType(), TRAIT_FLAG, CORE_TRAIT);
    }

    public static boolean isAttribute(ISemantic o) {
        return checkFlags(o.getType(), ATTRIBUTE_FLAG, ATTRIBUTE_TRAIT);
    }

    public static boolean isRealm(ISemantic o) {
        return checkFlags(o.getType(), REALM_FLAG, CORE_REALM_TRAIT);
    }

    public static boolean isIdentity(ISemantic o) {
        return checkFlags(o.getType(), IDENTITY_FLAG, CORE_IDENTITY_TRAIT);
    }

    public static boolean isRole(ISemantic o) {
        return checkFlags(o.getType(), ROLE_FLAG, ROLE_TRAIT);
    }

    public static boolean isEvent(ISemantic o) {
        return checkFlags(o.getType(), EVENT_FLAG, CORE_EVENT);
    }

    public static boolean isDomain(ISemantic o) {
        return checkFlags(o.getType(), DOMAIN_FLAG, CORE_DOMAIN);
    }

    public static boolean isConfiguration(ISemantic o) {
        return checkFlags(o.getType(), CONFIGURATION_FLAG, CORE_PATTERN);
    }

    public static boolean isNothing(ISemantic o) {
        return nothings.contains(o.getType().toString());
    }

    /**
     * True if the passed knowledge has been explicitly declared in k.IM.
     * 
     * @param o
     * @return true if declared
     */
    public static boolean isDeclared(ISemantic o) {
        IKnowledge observable = o.getType();
        INamespace ns = KLAB.MMANAGER.getNamespace(observable.getConceptSpace());
        return ns != null && ns.getModelObject(observable.getLocalName()) != null;
    }

    /**
     * True if the passed knowledge is a direct observable, i.e., an object (see @{link
     * isObject}) or a relationship.
     * 
     * @param o
     * @return true if directly observable
     */
    public static boolean isDirect(ISemantic o) {
        IKnowledge observable = o.getType();
        return isObject(observable) || isRelationship(observable);
    }

    /**
     * Given a trait class (which must be a base trait) return all the concrete types this
     * trait can assume, using superclasses appropriately, and ordering them if the trait
     * is an ordering.
     * 
     * @param trait
     * @return trait space for trait
     */
    public static Collection<IConcept> getTraitSpace(IConcept trait) {

        List<IConcept> ret = new ArrayList<>();

        /*
         * the trait space is the set of non-base children of the closest superclass that
         * has any.
         */
        IConcept tr = trait;
        while (tr != null && ret.isEmpty()) {

            Set<IConcept> ch = getNonBaseChildren(tr);
            if (ch.size() > 0) {
                ret.addAll(ch);
            }
            tr = getBaseParentTrait(tr);
        }

        if (trait.is(KLAB.c(NS.ORDERING))) {
            Collections.sort(ret, new Comparator<IConcept>() {

                @Override
                public int compare(IConcept arg0, IConcept arg1) {
                    Integer o0 = arg0.getMetadata().getInt(NS.ORDER_PROPERTY, 0);
                    Integer o1 = arg1.getMetadata().getInt(NS.ORDER_PROPERTY, 0);
                    return o0.compareTo(o1);
                }
            });
        }

        return ret;

    }

    public static IConcept getBaseParentTrait(IConcept tr) {

        String orig = tr.getMetadata().getString(NS.ORIGINAL_TRAIT);
        if (orig != null) {
            tr = KLAB.c(orig);
        }
        
        /*
         * there should only be one of these or none.
         */
        if (tr.getMetadata().get(NS.BASE_DECLARATION) != null) {
            return tr;
        }

        for (IConcept c : tr.getAllParents()) {
            IConcept r = getBaseParentTrait(c);
            if (r != null) {
                return r;
            }
        }
        return null;
    }

    /**
     * True if concept was declared in k.IM at root level. These serve as the "official"
     * least general "family" of concepts for several purposes - e.g. for traits, only
     * these are seen as "general" enough to be used in an "exposes" statement.
     * 
     * 
     * @param tr
     * @return
     */
    public static boolean isBaseDeclaration(IConcept tr) {
        return tr.getMetadata().get(NS.BASE_DECLARATION) != null;
    }

    private static Set<IConcept> getNonBaseChildren(IConcept tr) {

        Set<IConcept> ret = new HashSet<>();
        for (IConcept c : tr.getChildren()) {
            if (c.getMetadata().get(NS.BASE_DECLARATION) == null) {
                ret.add(c);
            }
        }
        return ret;
    }

    // static IConcept getBackingConcept(IKnowledge knowledge) {
    // if (knowledge instanceof IConcept) {
    // return (IConcept) knowledge;
    // }
    // // TODO return the actual backing concept
    // return KLAB.c(CORE_OBJECT);
    // }

    /**
     * Return the most specific core observable that subsumes this object (e.g. event,
     * subject, length, trait etc) or null if it's not an observable or trait. If the
     * concept is a trait and not an observable, the core trait type will be returned.
     * Otherwise the core observable is returned (even if it has traits). If this is not a
     * core particular this will return null.
     * 
     * @param observable
     * @return core type
     */
    public static IKnowledge getCoreType(ISemantic observable) {

        String toCheck = isObservable(observable) ? CORE_OBSERVABLE : CORE_UNIVERSAL;

        IKnowledge k = observable.getType();
        if (k.is(KLAB.p(STRUCTURING_PROPERTY))) {
            return k;
        }

        if (k.is(KLAB.c(toCheck))) {
            for (IConcept c : getLeastGeneral(((IConcept) k).getAllParents())) {
                if (c.is(KLAB.c(toCheck))) {
                    return getLeastGeneralFrom(c, CORE_ONTOLOGY, KLAB.c(toCheck));
                }
            }
        }

        return null;
    }

    /**
     * Return the domain, if any, that a given object belongs to. For now, that is simply
     * the (unique) domain assigned to the closest namespace. Later we may have different
     * domains in a namespace.
     * 
     * @param observable
     * @return domain of passed knowledge
     */
    public static IConcept getDomain(ISemantic observable) {

        IKnowledge k = observable.getType();
        INamespace ns = KLAB.MMANAGER.getNamespace(k.getConceptSpace());
        if (ns != null && ns.getDomain() != null) {
            return ns.getDomain();
        }
        if (k instanceof IConcept) {
            for (IConcept c : ((IConcept) k).getParents()) {
                IConcept d = getDomain(c);
                if (d != null) {
                    return d;
                }
            }
        } else if (k instanceof IProperty) {
            for (IProperty c : ((IProperty) k).getParents()) {
                IConcept d = getDomain(c);
                if (d != null) {
                    return d;
                }
            }
        }
        return null;
    }

    /**
     * Get the least general concept in the passed concept's hierarchy that belongs to the
     * named concept space. Will return the first parent found in it.
     * 
     * @param c
     * @param ontology
     * @return least general
     */
    public static IConcept getLeastGeneralFrom(IConcept c, String ontology) {

        if (c.getConceptSpace().equals(ontology)) {
            return c;
        }

        for (IConcept p : c.getParents()) {
            IConcept ret = getLeastGeneralFrom(p, ontology);
            if (ret != null) {
                return ret;
            }
        }

        return null;
    }

    /**
     * Get the least general concept in the passed concept's hierarchy that belongs to the
     * named concept space and is the passed base concept. Will return the first
     * non-derived parent found in it.
     * 
     * @param c
     * @param ontology
     * @return least general
     */
    public static IConcept getLeastGeneralFrom(IConcept c, String ontology, IConcept root) {

        if (c.equals(root)) {
            return c;
        }

        if (c.getConceptSpace().equals(ontology) && c.is(root) && !isDerived(c)) {
            return c;
        }

        for (IConcept p : c.getParents()) {
            IConcept ret = getLeastGeneralFrom(p, ontology, root);
            if (ret != null) {
                return ret;
            }
        }

        return null;
    }

    /**
     * True if concept is a trait and not an observable. If isBaseTrait is true, also
     * check that the concept is a base trait class - an abstract one that can produce
     * concrete concepts to classify things with.
     * 
     * @param cc
     * @param isBaseTrait
     * @return true if trait
     */
    public static boolean isTrait(IKnowledge cc, boolean isBaseTrait) {
        boolean ret = isTrait(cc);
        if (ret && isBaseTrait) {
            ret = cc.getMetadata().get(NS.BASE_DECLARATION) != null;
        }
        return ret;
    }

    /**
     * Return all concepts that the passed concept is and are observables.
     * 
     * @param subject
     * @return closure
     */
    public static Set<IKnowledge> getObservableClosure(IKnowledge subject) {

        Set<IKnowledge> ret = new HashSet<>();
        ret.add(subject);

        if (subject instanceof IConcept) {
            for (IConcept c : ((IConcept) subject).getAllParents()) {
                if (isObservable(c)) {
                    ret.add(c);
                }
            }
        } else if (subject instanceof IProperty) {
            for (IProperty c : ((IProperty) subject).getAllParents()) {
                if (isObservable(c)) {
                    ret.add(c);
                }
            }
        }
        return ret;
    }

    /**
     * Finds a metadata field in the inheritance chain of a concept.
     * {@link IConcept#getMetadata()} only retrieves metadata for the concept it's called
     * on.
     * 
     * @param concept
     * @param field
     * @return
     */
    public static String getMetadata(IConcept concept, String field) {
        String ret = concept.getMetadata().getString(field);
        if (ret != null) {
            return ret;
        }
        for (IConcept c : concept.getParents()) {
            ret = getMetadata(c, field);
            if (ret != null) {
                return ret;
            }
        }
        return null;
    }

    /**
     * Collect all concrete disjoint children of a class, stopping the recursive search at
     * the first concrete child. Used when observing dependencies with the "every"
     * qualifier.
     * 
     * @param concept
     * @return concrete disjoint children
     */
    public static Collection<IKnowledge> getConcreteDisjointChildren(IKnowledge concept) {

        HashSet<IKnowledge> ret = new HashSet<>();
        collectConcreteChildren(concept, ret);
        return ensureDisjoint(ret);

    }

    /**
     * Remove all concepts in the passed collection that are not disjoint with each other.
     * TODO unimplemented - needs functionality in IConcept, or better, needs full
     * reasoner integration.
     * 
     * @param ret
     * @return disjoint concepts
     */
    public static Collection<IKnowledge> ensureDisjoint(Collection<IKnowledge> ret) {

        ArrayList<IKnowledge> rr = new ArrayList<>();
        rr.addAll(ret);
        HashSet<IConcept> toRemove = new HashSet<>();
        for (int i = 0; i < rr.size(); i++) {
            for (int j = 0; j < rr.size(); j++) {
                // if (i != j && !rr.get(i).isDisjointWith(rr.get(j))) {
                // toRemove.add(rr.get(i));
                // toRemove.add(rr.get(j));
                // }
            }
        }

        ret.removeAll(toRemove);

        return ret;
    }

    private static void collectConcreteChildren(IKnowledge concept, HashSet<IKnowledge> ret) {

        if (!concept.isAbstract()) {
            ret.add(concept);
        } else {
            if (concept instanceof IConcept) {
                for (IConcept c : ((IConcept) concept).getChildren()) {
                    collectConcreteChildren(c, ret);
                }
            } else {
                for (IProperty c : ((IProperty) concept).getChildren()) {
                    collectConcreteChildren(c, ret);
                }
            }
        }
    }

    public static boolean isIdentity(IConcept c) {
        return c.is(KLAB.c(NS.CORE_IDENTITY_TRAIT));
    }

    public static boolean isSubjective(IObservableSemantics obs) {
        for (IConcept trait : Traits.getTraits(obs.getType())) {
            if (isSubjective(trait)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSubjective(IConcept concept) {
        IConcept base = getBaseParentTrait(concept);
        if (base != null) {
            return base.getMetadata().contains(IS_SUBJECTIVE);
        }
        return false;
    }

    /**
     * TODO use reasoner for better resolution of physical properties etc.
     * 
     * @param observable
     * @return observation type
     */
    public static IConcept getObservationTypeFor(IConcept observable) {

        if (observable.is(KLAB.c(CORE_DISTANCE))) {
            return KLAB.c(DISTANCE_OBSERVATION);
        } else if (observable.is(KLAB.c(CORE_PHYSICAL_PROPERTY))) {
            return KLAB.c(MEASUREMENT);
        } else if (isClass(observable) || isTrait(observable)) {
            return KLAB.c(CLASSIFICATION);
        } else if (observable.is(KLAB.c(CORE_PREFERENCE_VALUE))
                || observable.is(KLAB.c(CORE_MONETARY_VALUE))) {
            return KLAB.c(VALUE_OBSERVATION);
        } else if (observable.is(KLAB.c(CORE_RATIO))) {
            return KLAB.c(RATIO_OBSERVATION);
        } else if (observable.is(KLAB.c(CORE_PRESENCE))) {
            return KLAB.c(PRESENCE_OBSERVATION);
        } else if (observable.is(KLAB.c(CORE_PROBABILITY))) {
            return KLAB.c(PROBABILITY_OBSERVATION);
        } else if (observable.is(KLAB.c(CORE_UNCERTAINTY))) {
            return KLAB.c(UNCERTAINTY_OBSERVATION);
        } else if (observable.is(KLAB.c(CORE_COUNT))) {
            return KLAB.c(COUNT_OBSERVATION);
        } else if (observable.is(KLAB.c(CORE_NUMERIC_QUANTITY))) {
            return KLAB.c(RANKING);
        } else if (isDirect(observable)) {
            return KLAB.c(DIRECT_OBSERVATION);
        }

        return KLAB.c(INDIRECT_OBSERVATION);
    }

    /**
     * Find or create the property that would link an observable to a concept. FIXME of
     * course must use the semantics and a reasoner more than it does, and return null or
     * throw exceptions if anything inconsistent is attempted. When that is done, remove
     * checks in the actual observing functions that call this one.
     * 
     * @param observable
     * @param receiver
     * @param namespace
     * @return property to link receiver to observable
     * @throws KlabException
     */
    public static IProperty getPropertyFor(Object observable, IObservableSemantics receiver, INamespace namespace)
            throws KlabException {

        IProperty property = null;

        IConcept c = null;
        if (observable instanceof IConcept)
            c = (IConcept) observable;
        else if (observable instanceof IObservableSemantics)
            c = ((IObservableSemantics) observable).getType();
        else if (observable instanceof IModel)
            c = ((IModel) observable).getObservable().getType();
        else if (observable instanceof ISubject)
            c = ((ISubject) observable).getObservable().getSemantics().getType();
        else if (observable instanceof IProcess)
            c = ((IProcess) observable).getObservable().getSemantics().getType();
        // else if (observable instanceof ISemanticObject<?>)
        // c = (IConcept) ((ISemanticObject<?>) observable).getDirectType();
        else if (observable instanceof IState)
            c = ((IState) observable).getObservable().getSemantics().getType();

        if (c != null && receiver.getType() != null) {

            /*
             * FIXME: if a model is taking subjects (say a railway) but discretizes it to
             * an observation it will MAKE the observation a quality. The attribute is for
             * the OBSERVATION, not for the observable. For now adjust things looking at
             * the model first, but we need proper treatment of the semantics.
             */
            boolean isQuality = isQuality(c);
            boolean isQualitySpace = isClass(c);

            if (observable instanceof IModel) {
                isQuality = ((IModel) observable).getObserver() != null;
                if (isQuality) {
                    isQualitySpace = !isNumericTransformation(((IModel) observable).getObserver());
                }
            } else if (observable instanceof IState) {
                isQuality = true;
                /*
                 * FIXME should also check isQualitySpace, but if we get here we have
                 * already created or found the property before, so it's already there and
                 * the check after this will succeed without further intelligence.
                 */
            }

            /*
             * no range to speak of if we have a dataproperty, so just look for hasxxx and
             * return it if there.
             */
            if (isQuality && namespace.getOntology().getProperty("has" + c.getLocalName()) != null) {
                return namespace.getOntology().getProperty("has" + c.getLocalName());
            }

            /*
             * TODO should put them all away and raise an exception if there is ambiguity.
             */
            for (IProperty p : receiver.getType().getAllProperties()) {

                if (receiver.getType().getPropertyRange(p).contains(c)) {
                    property = p;
                    break;
                }
            }

            if (property /* still */ == null) {

                /*
                 * TODO/FIXME: using only object properties.
                 */
                boolean isObject = true; // !(isQuality || isQualitySpace);

                ArrayList<IAxiom> axioms = new ArrayList<>();

                axioms.add(isObject ? Axiom.ObjectPropertyAssertion("has" + c.getLocalName())
                        : Axiom.DataPropertyAssertion("has" + c.getLocalName()));
                axioms.add(isObject ? Axiom.ObjectPropertyRange("has" + c.getLocalName(), c.toString())
                        : Axiom.DataPropertyRange("has" + c.getLocalName(), c.toString()));
                axioms.add(isObject
                        ? Axiom.ObjectPropertyDomain("has" + c.getLocalName(), receiver.toString())
                        : Axiom.DataPropertyDomain("has" + c.getLocalName(), receiver.toString()));

                namespace.getOntology().define(axioms);

                property = namespace.getOntology().getProperty("has" + c.getLocalName());

                // TODO turn to debug or remove
                // Env.logger
                // .info((property.isObjectProperty() ? "object" : "data") + "
                // property "
                // + property
                // + " created for " + c);

            } else {

                // // TODO turn to debug or remove
                // Env.logger
                // .info((property.isObjectProperty() ? "object" : "data") + "
                // property "
                // + property
                // + " found for " + c);

            }
        } else {

            /*
             * huh?
             */
            throw new KlabInternalErrorException("internal error: adding dependency on something that is not semantic: "
                    + observable);
        }

        return property;
    }

    /*
     * return true if this observed doesn't change the semantics of the ultimate observer.
     */
    public static boolean isNumericTransformation(IObserver observer) {

        if (observer instanceof IRankingObserver || observer instanceof IMeasuringObserver
                || observer instanceof IValuingObserver) {
            return true;
        }

        if (observer instanceof IConditionalObserver) {
            boolean ret = true;
            for (Pair<IModel, IExpression> o : ((IConditionalObserver) observer).getModels()) {
                if (!isNumericTransformation(o.getFirst().getObserver())) {
                    ret = false;
                    break;
                }
            }
            return ret;
        }

        return false;
    }

    public static boolean synchronize() {

        if (LENGTH == null) {
            try {
                LENGTH = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.LENGTH"), IConcept.class);
                AREA = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.AREA"), IConcept.class);
                DISTANCE = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.DISTANCE"), IConcept.class);
                VOLUME = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.VOLUME"), IConcept.class);
                MINIMUM_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.MINIMUM_TRAIT"), IConcept.class);
                MAXIMUM_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.MAXIMUM_TRAIT"), IConcept.class);
                AVERAGE_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.AVERAGE_TRAIT"), IConcept.class);
                DATA_REDUCTION_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge("IM.DATA_REDUCTION_TRAIT"), IConcept.class);
                FLOW = checkClass(KLAB.MMANAGER.getExportedKnowledge("IM.FLOW"), IConcept.class);
                HOURLY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(HOURLY_TRAIT_CONCEPT), IConcept.class);
                DAILY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(DAILY_TRAIT_CONCEPT), IConcept.class);
                WEEKLY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(WEEKLY_TRAIT_CONCEPT), IConcept.class);
                MONTHLY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(MONTHLY_TRAIT_CONCEPT), IConcept.class);
                YEARLY_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(YEARLY_TRAIT_CONCEPT), IConcept.class);
                VISIBLE_TRAIT = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(VISIBLE_TRAIT_CONCEPT), IConcept.class);
                ARCHETYPE_ROLE = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(ARCHETYPE_ROLE_CONCEPT), IConcept.class);
                EXPLANATORY_QUALITY_ROLE = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(EXPLANATORY_QUALITY_ROLE_CONCEPT), IConcept.class);
                LEARNED_QUALITY_ROLE = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(LEARNED_QUALITY_ROLE_CONCEPT), IConcept.class);
                LEARNING_PROCESS_ROLE = checkClass(KLAB.MMANAGER
                        .getExportedKnowledge(LEARNING_PROCESS_ROLE_CONCEPT), IConcept.class);
            } catch (KlabException e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check that passed object is of passed class (and not null); throw the necessary
     * exceptions if not so, and return the casted object if so.
     * 
     * @param object
     * @param cls
     * @return cast object
     * @throws KlabException
     */
    @SuppressWarnings("unchecked")
    public static <T> T checkClass(Object object, Class<? extends T> cls) throws KlabException {
        if (object == null) {
            throw new KlabValidationException("required content of type " + cls.getCanonicalName()
                    + " is null");
        }
        if (!cls.isInstance(object)) {
            throw new KlabValidationException("wrong type for " + object + ": " + cls.getCanonicalName()
                    + " required, "
                    + object.getClass().getCanonicalName() + " passed");
        }
        return (T) object;
    }

    public static void dumpOntologies(File outDir) throws KlabException {

        for (INamespace ns : KLAB.MMANAGER.getNamespaces()) {
            IOntology o = ns.getOntology();
            if (o != null) {
                o.write(new File(outDir + File.separator + o.getConceptSpace() + ".owl"), true);
            }
        }
    }

    /**
     * Arrange a set of concepts into the collection of the most specific members of each
     * concept hierarchy therein.
     * 
     * @param cc
     * @return least general
     */
    public static Collection<IConcept> getLeastGeneral(Collection<IConcept> cc) {

        Set<IConcept> ret = new HashSet<>();
        for (IConcept c : cc) {
            List<IConcept> ccs = new ArrayList<>(ret);
            boolean set = false;
            for (IConcept kn : ccs) {
                if (c.is(kn)) {
                    ret.remove(kn);
                    ret.add(c);
                    set = true;
                } else if (kn.is(c)) {
                    set = true;
                }
            }
            if (!set) {
                ret.add(c);
            }
        }
        return ret;
    }

    /**
     * Arrange a set of concepts into the collection of the most specific members of each
     * concept hierarchy therein. Return one concept or null.
     * 
     * @param cc
     * @return least general
     */
    public static IConcept getLeastGeneralConcept(Collection<IConcept> cc) {
        Collection<IConcept> z = getLeastGeneral(cc);
        return z.size() > 0 ? z.iterator().next() : null;
    }

    public static IConcept getLeastGeneralCommonConcept(IConcept concept1, IConcept c) {
        return concept1.getLeastGeneralCommonConcept(c);
    }

    public static IConcept getLeastGeneralCommonConcept(Collection<IConcept> cc) {

        IConcept ret = null;
        Iterator<IConcept> ii = cc.iterator();

        if (ii.hasNext()) {

            ret = ii.next();

            if (ret != null)
                while (ii.hasNext()) {
                    ret = ret.getLeastGeneralCommonConcept(ii.next());
                    if (ret == null)
                        break;
                }
        }

        return ret;
    }

    /**
     * Return all independent observables that have a stated role within the context of
     * the passed observable, which must be a direct observable. The result may differ if
     * a reasoner is connected. If an actuator is passed, check if it wants to rearrange
     * the dependencies based on the context before anything happens.
     * 
     * @param observable
     * @return triples containing the target concept, the role it is restricting, and the
     *         property that carries the restriction.
     */
    public static Collection<Triple<IConcept, IConcept, IProperty>> getSemanticDependencies(ISemantic observable) {

        Map<IConcept, IProperty> cc = new HashMap<>();
        List<Triple<IConcept, IConcept, IProperty>> ret = new ArrayList<>();

        IKnowledge k = observable.getType();
        if (k instanceof Concept) {
            for (Pair<IConcept, IProperty> cp : ((Concept) k).getObjectRestrictions()) {
                if (isObservable(cp.getFirst()) && !cp.getFirst().isAbstract()) {
                    cc.put(cp.getFirst(), cp.getSecond());
                }
            }
        }

        for (IConcept fc : getLeastGeneral(cc.keySet())) {
            Collection<IConcept> restricted;
            try {
                restricted = getLeastGeneral(fc.getPropertyRange(cc.get(fc)));
                if (restricted.size() > 0) {
                    IConcept rest = restricted.iterator().next();
                    if (NS.isRole(rest)) {
                        ret.add(new Triple<>(fc, rest, cc.get(fc)));
                    }
                }
            } catch (KlabException e) {
            }
        }

        return ret;
    }

    /**
     * Return the order of the concepts subsumed by the passed one, which must be an
     * ordering trait.
     * 
     * @param ordering
     * @param level
     * @return ordering for type
     */
    public static List<IConcept> getOrdering(IConcept ordering, /* @Nullable */ String level) {

        List<IConcept> ret = new ArrayList<>();

        return ret;
    }

    public static List<String> dumpConcepts(String namespace) {

        List<String> ret = new ArrayList<>();

        for (INamespace ns : KLAB.MMANAGER.getNamespaces()) {
            if (namespace != null && !namespace.equals(ns.getId())) {
                continue;
            }

            for (IModelObject o : ns.getModelObjects()) {
                dumpKnowledge(o, ret, 0);
            }

        }
        return ret;
    }

    private static void dumpKnowledge(IModelObject o, List<String> ret, int indent) {

        if (o instanceof IKnowledgeObject) {

            if (ret.isEmpty()) {
                ret.add("NAMESPACE,ID,ID (hierarchy),DOMAIN,CORE TYPE,DESCRIPTION");
            }

            IConcept domain = ((IKnowledgeObject) o).getType().getDomain();
            IKnowledge coretp = NS.getCoreType((IKnowledgeObject) o);
            IKnowledge type = ((IKnowledgeObject) o).getType();

            String id = o.getId();
            String ns = o.getNamespace().getId();
            String in = StringUtils.repeat("*", indent) + o.getId();
            String dm = domain == null ? "N/A" : domain.toString();
            String ct = coretp == null ? "N/A" : coretp.getLocalName();
            String cm = type.getMetadata().getString(IMetadata.DC_COMMENT);

            ret.add(ns + "," + id + "," + in + "," + dm + "," + ct + "," + cm);

            for (IModelObject oo : o.getChildren()) {
                dumpKnowledge(oo, ret, indent + 1);
            }
        }
    }

    public static List<IAnnotation> getAnnotations(IModelObject o, String string) {
        List<IAnnotation> ret = new ArrayList<>();
        for (IAnnotation a : o.getAnnotations()) {
            if (a.getId().equals(string)) {
                ret.add(a);
            }
        }
        return ret;
    }

    /**
     * Return a list of n levels (tagged as an ordering) guaranteed to exist, in the local
     * namespace.
     * 
     * @param maxBins
     * @return levels
     */
    public static List<IConcept> getLevels(int maxBins) {

        List<IConcept> ret = new ArrayList<>();
        INamespace ns = KLAB.MMANAGER.getLocalNamespace();
        IConcept parent = NS.getUserOrdering();
        for (int i = 0; i < maxBins; i++) {
            String lname = "UL" + i;
            IConcept level = KLAB.KM.getConcept(ns.getId() + ":" + lname);
            if (level == null) {
                ArrayList<IAxiom> ax = new ArrayList<>();
                ax.add(Axiom.ClassAssertion(lname));
                ax.add(Axiom.SubClass(parent.getLocalName(), lname));
                ax.add(Axiom.AnnotationAssertion(lname, NS.ORDER_PROPERTY, "" + i));
                ns.getOntology().define(ax);
                level = ns.getOntology().getConcept(lname);
            }
            ret.add(level);
        }

        return ret;
    }

    /**
     * Return a stable abstract ordering from the local namespace.
     * 
     * @return stable ordering
     */
    public static IConcept getUserOrdering() {
        String cname = "UL";
        INamespace ns = KLAB.MMANAGER.getLocalNamespace();
        IConcept ret = KLAB.KM.getConcept(ns.getId() + ":" + cname);
        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cname));
            ax.add(Axiom.SubClass(NS.ORDERING, cname));
            ax.add(Axiom.AnnotationAssertion(cname, NS.IS_ABSTRACT, "true"));
            ns.getOntology().define(ax);
            ret = ns.getOntology().getConcept(cname);
        }
        return ret;
    }

    public static boolean isUniversal(ISemantic k) {
        IKnowledge concept = k.getType();
        return concept.is(KLAB.c(CORE_UNIVERSAL));
    }

    /**
     * Check if a concept is any of the passed candidates.
     * 
     * @param type
     * @param candidates
     * @return
     */
    public static boolean isContained(IKnowledge type, Collection<IConcept> candidates) {
        for (IConcept c : candidates) {
            if (type.is(c)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check if any of the passed candidates is a concept.
     * 
     * @param type
     * @param candidates
     * @return
     */
    public static boolean contains(IKnowledge type, Collection<IConcept> candidates) {
        for (IConcept c : candidates) {
            if (c.is(type)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the best display name for a concept.
     * 
     * @param t
     * @return
     */
    public static String getDisplayName(IKnowledge t) {

        String ret = t.getMetadata().getString(NS.DISPLAY_LABEL_PROPERTY);

        if (ret == null) {
            ret = t.getMetadata().getString(IMetadata.DC_LABEL);
        }
        if (ret == null) {
            ret = t.getLocalName();
        }
        if (ret.startsWith("i")) {
            ret = ret.substring(1);
        }
        return ret;
    }

    public static boolean isRelativeQuality(IKnowledge knowledge) {
        return knowledge.is(KLAB.c(NS.CORE_RELATIVE_QUANTITY));
    }

    /*
     * we add these as we need
     */
    public static boolean isRelationship(Collection<IConcept> concepts) {

        if (concepts.size() == 0) {
            return false;
        }

        for (IConcept c : concepts) {
            if (!isRelationship(c)) {
                return false;
            }
        }
        return true;
    }

    public static boolean isThing(Collection<IConcept> concepts) {

        if (concepts.size() == 0) {
            return false;
        }

        for (IConcept c : concepts) {
            if (!isThing(c)) {
                return false;
            }
        }
        return true;
    }
}
