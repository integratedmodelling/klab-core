/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.metadata.IReport;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.Filter;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.knowledge.Definition;
import org.integratedmodelling.common.owl.Concept;
import org.integratedmodelling.common.owl.OWL;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.reporting.Report;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.LogicalConnector;

/**
 * General utility filters and listers of concepts that will return top-level concepts for
 * many useful browsing operations. By default it only includes concepts from the core
 * ontologies (defaulting at im.*) and skips generated concepts.
 * 
 * @author Ferd
 */
public class Observables {

    public static enum Qualities {
        MEASUREMENT,
        PRESENCE,
        RANK,
        PROPORTION,
        TYPE,
        RATIO,
        DISTANCE,
        PROBABILITY,
        UNCERTAINTY,
        COUNT,
        VALUE,
        OCCURRENCE,
        // the next is a guest as it does not describe a derived quality but a process
        ASSESSMENT
    }

    String  _mainOntology           = "im";
    String  _coreOntology           = "im.core";
    String  _mainNamespacePrefix    = "im.";
    boolean _skipGenerated          = true;
    boolean _includeNonCoreConcepts = false;

    /**
     * Get the context ('within') for the passed quality or trait. If the passed concept
     * is an attribute, configuration, class or realm, the context is the one specified
     * for the quality/configuration under "describes". For identities and processes,
     * context is found in 'applies to'. If the context is not specified but there is an
     * inherent concept, the context of the inherent, if any, is returned.
     * 
     * @param concept
     * @return
     */
    public static IConcept getContextType(IKnowledge concept) {
        return getContextType(concept, true);
    }

    /**
     * Return a concept that is inherent or contextual to the passed context one. Will
     * choose the semantics appropriately for the specific context and observables
     * requested - e.g. a quality contextual to a region will restrict the context, a
     * quality inherent to an agent in a region will use inherency etc.
     * 
     * @param observable
     * @param context
     * @return
     */
    public static IConcept contextualizeTo(IConcept observable, IConcept context) throws KlabException {

        IConcept ret = observable;

        IConcept inherent = Observables.getInherentType(observable);
        // IConcept ctx = Observables.getContextType(observable);

        if (inherent != null) {
            /*
             * inherencies are always explicit; only contextualize if the inherency is
             * different.
             */
            IConcept cinh = getInherentType(context);
            if (cinh != null && inherent.is(cinh)) {
                return ret;
            }
        }

        ret = declareObservable(observable, null, null, context);

        return ret;
    }

    private static IConcept getContextType(IKnowledge concept, boolean recurse) {

        if (!(concept instanceof IConcept)) {
            return null;
        }
        IConcept ret = null;
        if (recurse && (NS.isAttribute(concept) || NS.isRealm(concept) || NS.isClass(concept)
                || NS.isConfiguration(concept))) {
            IConcept described = getDescribedQuality((IConcept) concept);
            ret = described == null ? null : getContextType(described, false);
        } else if (recurse && (NS.isIdentity(concept) || NS.isProcess(concept))) {
            Collection<IConcept> described = getApplicableObservables(concept);
            ret = described.size() > 0 ? NS.getLeastGeneralCommonConcept(described) : null;
        } else {

            Collection<IConcept> cls = OWL
                    .getRestrictedClasses((IConcept) concept, KLAB.p(NS.HAS_CONTEXT_PROPERTY));
            ret = cls.isEmpty() ? null : cls.iterator().next();
        }

        if (ret == null && NS.isObservable(concept)) {
            IConcept inherent = getInherentType(concept);
            if (inherent != null) {
                ret = getContextType(inherent, false);
            }
        }

        return ret;
    }

    public static IConcept getByType(IKnowledge concept) {
        if (!(concept instanceof IConcept)) {
            return null;
        }
        Collection<IConcept> cls = OWL
                .getRestrictedClasses((IConcept) concept, KLAB.p(NS.REPRESENTED_BY_PROPERTY));
        return cls.isEmpty() ? null : cls.iterator().next();
    }

    /**
     * Get the possible children of a concept when such concept has a limitedTo closure
     * (specified with 'down to').
     * 
     * @param concept
     * @return
     */
    public static Collection<IConcept> getRestrictedClosure(IConcept concept) {
        return OWL.getRestrictedClasses(concept, KLAB.p(NS.LIMITED_BY_PROPERTY));
    }

    public static IConcept getInherentType(IKnowledge concept) {
        if (!(concept instanceof IConcept)) {
            return null;
        }
        Collection<IConcept> cls = OWL
                .getRestrictedClasses((IConcept) concept, KLAB.p(NS.IS_INHERENT_TO_PROPERTY));
        return cls.isEmpty() ? null : cls.iterator().next();
    }

    /**
     * Get all other roles implied by this one. These must be concrete when the role is
     * used in the main observable for a model, which must produce or use them.
     * 
     * @param role
     * @return
     */
    public static Collection<IConcept> getRelationships(IConcept role) {
        return OWL.getRestrictedClasses(role, KLAB.p(NS.RELATES_TO_PROPERTY), true);
    }

    public static Collection<IConcept> getDependencies(IConcept role) {
        return OWL.getRestrictedClasses(role, KLAB.p(NS.DEPENDS_ON_PROPERTY), true);
    }

    public static Collection<IConcept> getParts(IConcept role, boolean spatial) {
        return OWL.getRestrictedClasses(role, KLAB
                .p(spatial ? NS.CONTAINS_PART_SPATIALLY_PROPERTY : NS.CONTAINS_PART_PROPERTY), true);
    }

    /**
     * Return the core observable, i.e., the most generic concrete parent that was
     * asserted directly (no traits) in a concrete domain.
     * 
     * @param observable
     * @return
     */
    public static IConcept getCoreObservable(IConcept observable) {

        try {
            Definition definition = Definition.parse(observable.getDefinition());
            return definition.getCoreType();
        } catch (KlabValidationException e) {
        }
        return null;
    }

    /**
     * True if o1 and o2 are observables from recognized domains, have compatible context
     * and inherency, o1 is o2, and o1 adopts all the traits and roles that o2 adopts.
     * 
     * @param o1
     * @param o2
     * @return
     */
    public static boolean isCompatible(IConcept o1, IConcept o2) {
        return isCompatible(o1, o2, 0);
    }

    /**
     * If passed to {@link #isCompatible(IConcept, IConcept, int)}, different realms will
     * not determine incompatibility.
     */
    static public final int ACCEPT_REALM_DIFFERENCES      = 0x01;
    /**
     * If passed to {@link #isCompatible(IConcept, IConcept, int)}, only types that have
     * the exact same core type will be accepted.
     */
    static public final int REQUIRE_SAME_CORE_TYPE        = 0x02;
    /**
     * If passed to {@link #isCompatible(IConcept, IConcept, int)}, types with roles that
     * are more general of the roles in the first concept will be accepted.
     */
    static public final int USE_ROLE_PARENT_CLOSURE       = 0x04;
    /**
     * If passed to {@link #isCompatible(IConcept, IConcept, int)}, types with traits that
     * are more general of the traits in the first concept will be accepted.
     */
    static public final int USE_TRAIT_PARENT_CLOSURE      = 0x08;

    /**
     * If passed to
     * {@link #declareObservable(IConcept, Collection, IConcept, IConcept, Collection, IConcept, IConcept, IOntology, int)}
     * causes acceptance of subjective traits for observables.
     */
    static public final int ACCEPT_SUBJECTIVE_OBSERVABLES = 0x10;

    /**
     * Compatibility from the point of view of observation. True if o1 and o2 are
     * observables from recognized domains, have compatible context and inherency, o1 is
     * o2, and o1 adopts all the traits and roles that o2 adopts.
     * 
     * @param o1
     * @param o2
     * @return
     */
    public static boolean isCompatible(IConcept o1, IConcept o2, int flags) {

        boolean mustBeSameCoreType = (flags & REQUIRE_SAME_CORE_TYPE) != 0;
        boolean useRoleParentClosure = (flags & USE_ROLE_PARENT_CLOSURE) != 0;
        boolean acceptRealmDifferences = (flags & ACCEPT_REALM_DIFFERENCES) != 0;

        // TODO unsupported
        boolean useTraitParentClosure = (flags & USE_TRAIT_PARENT_CLOSURE) != 0;

        if ((!NS.isObservable(o1) || !NS.isObservable(o2))
                && !(NS.isConfiguration(o1) && NS.isConfiguration(o2))) {
            return false;
        }

        IConcept core1 = getCoreObservable(o1);
        IConcept core2 = getCoreObservable(o2);

        if (core1 == null || core2 == null || !(mustBeSameCoreType ? core1.equals(core2) : core1.is(core2))) {
            return false;
        }

        IConcept cc1 = getContextType(o1);
        IConcept cc2 = getContextType(o2);

        if ((cc1 == null && cc1 != null) || (cc1 != null && cc2 == null)) {
            return false;
        }
        if (cc1 != null && cc1 != null) {
            if (!isCompatible(cc1, cc2, ACCEPT_REALM_DIFFERENCES)) {
                return false;
            }
        }

        IConcept ic1 = getInherentType(o1);
        IConcept ic2 = getInherentType(o2);

        if ((ic1 == null && ic1 != null) || (ic1 != null && ic2 == null)) {
            return false;
        }
        if (ic1 != null && ic1 != null) {
            if (!isCompatible(ic1, ic2)) {
                return false;
            }
        }

        for (IConcept t : Traits.getTraits(o2)) {
            boolean ok = Traits.hasTrait(o1, t);
            if (!ok && useTraitParentClosure) {
                ok = Traits.hasParentTrait(o1, t);
            }
            if (!ok) {
                return false;
            }
        }

        for (IConcept t : Roles.getRoles(o2)) {
            boolean ok = Roles.hasRole(o1, t);
            if (!ok && useRoleParentClosure) {
                ok = Roles.hasParentRole(o1, t);
            }
            if (!ok) {
                return false;
            }
        }

        return true;
    }

    public static Collection<IConcept> getCompatibleWith(IConcept observable) {
        Set<IConcept> ret = new HashSet<>();
        for (IConcept c : KLAB.REASONER.getSemanticClosure(observable)) {
            if (KLAB.REASONER.isSatisfiable(c) && isCompatible(c, observable)) {
                ret.add(c);
            }
        }
        return ret;
    }

    /**
     * Semantic distance increases with distance along inheritance chains for both main
     * observables and common traits/roles. Varies between 0 and 100, with points above 50
     * reserved from transformed concepts ('by'), in which case D - 50 is the semantic
     * distance to the untransformed concept.
     * 
     * @param o1
     * @param o2
     * @return
     */
    public static int getSemanticDistance(IConcept o1, IConcept o2) {
        // TODO
        if (o1.equals(o2)) {
            return 0;
        }
        return 50;
    }

    /**
     * True if observable has a stated context (mandatory for qualities) and context
     * isCompatible with the stated context for observable.
     * 
     * @param observable
     * @param context
     * @return
     */
    public static boolean isContextCompatible(IConcept observable, IConcept context) {
        IConcept ctx = getContextType(observable);
        return ctx != null && isCompatible(context, ctx);
    }

    /*
     * Reporting functions
     */

    /**
     * Create an extensive, nicely formatted, recursive report on an observable.
     * 
     * @param k
     * @return
     */
    public static IReport describe(IKnowledge k) {

        IReport ret = new Report();
        if (k instanceof IConcept) {
            ret.writeln("# Description of " + k + "\n");
            describeConcept((IConcept) k, ret, 0);
        }
        return ret;
    }

    public static IReport describe(INamespace ontology) {

        IReport ret = new Report();
        for (IModelObject o : ontology.getModelObjects()) {
            if (o instanceof IKnowledgeObject) {
                IConcept c = ((IKnowledgeObject) o).getConcept();
                if (c != null) {
                    ret.writeln("# Description of " + c + "\n");
                    describeConcept(c, ret, 0);
                    ret.writeln("\n");
                }
            } else if (o instanceof IModel) {

                IModel m = (IModel) o;

                ret.writeln("# Description of model" + o + "\n");

                int i = 0;
                for (IObservableSemantics obs : m.getObservables()) {
                    ret.writeln("## Output observable (#" + (i++) + ")\n");
                    describeConcept(obs.getType(), ret, 0);
                }
                i = 0;
                for (IDependency dep : m.getDependencies()) {
                    ret.writeln("## Input observable (#" + (i++) + ")\n");
                    describeConcept(dep.getObservable().getType(), ret, 0);
                }
                if (m.getObserver() != null) {
                    for (IDependency dep : m.getObserver().getDependencies()) {
                        ret.writeln("## Input observable (#" + (i++) + ")\n");
                        describeConcept(dep.getObservable().getType(), ret, 0);
                    }
                }
                ret.writeln("\n");
            }
        }
        return ret;
    }

    private static void describeConcept(IConcept k, IReport report, int indent) {

        if (k == null) {
            return;
        }

        String filler = StringUtils.spaces(indent);

        report.writeln(filler + "* **" + k + "**");
        report.writeln(filler + "* k.LAB definition: " + k.getDefinition());
        IConcept domain = k.getDomain();
        report.writeln(filler + "* domain: " + domain);
        IConcept coreType = (IConcept) NS.getCoreType(k);
        report.writeln(filler + "* core type: " + coreType);

        boolean isObservable = NS.isObservable(k);
        boolean isUniversal = NS.isUniversal(k);

        Collection<IConcept> applicables = Observables.getApplicableObservables(k);
        IConcept describes = Observables.getDescribedQuality(k);

        if (isObservable) {

            report.writeln(filler + "* " + (k.isAbstract() ? "abstract" : "concrete") + " observable");
            IConcept baseType = Observables.getCoreObservable(k);
            report.writeln(filler + "* root observable: " + baseType);

            Collection<IConcept> identities = Traits.getIdentities(k);
            Collection<IConcept> realms = Traits.getRealms(k);
            Collection<IConcept> attributes = Traits.getAttributes(k);
            Collection<IConcept> roles = Roles.getRoles(k);

            if (identities.isEmpty()) {
                report.writeln(filler + "* no identities");
            } else {
                report.writeln(filler + "* identities:");
                for (IConcept i : identities) {
                    describeConcept(i, report, indent + 4);
                }
            }

            if (realms.isEmpty()) {
                report.writeln(filler + "* no realms");
            } else {
                report.writeln(filler + "* realms:");
                for (IConcept i : realms) {
                    describeConcept(i, report, indent + 4);
                }
            }

            if (attributes.isEmpty()) {
                report.writeln(filler + "* no attributes");
            } else {
                report.writeln(filler + "* attributes:");
                for (IConcept i : attributes) {
                    describeConcept(i, report, indent + 4);
                }
            }

            IConcept context = Observables.getContextType(k);

            if (context == null) {
                report.writeln(filler + "* no context");
            } else {
                report.writeln(filler + "* context:");
                describeConcept(context, report, indent + 4);
            }

            IConcept inherent = Observables.getInherentType(k);

            if (inherent == null) {
                report.writeln(filler + "* no inherency");
            } else {
                report.writeln(filler + "* inherent to:");
                describeConcept(inherent, report, indent + 4);
            }

            if (roles.isEmpty()) {
                report.writeln(filler + "* no roles");
            } else {
                report.writeln(filler + "* roles:");
                for (IConcept i : roles) {
                    describeConcept(i, report, indent + 4);
                }
            }

            if (NS.isClass(k)) {
                Collection<IConcept> exposed = Traits.getExposedTraits(k);
                if (exposed.isEmpty()) {
                    report.writeln(filler + "* no exposed traits");
                } else {
                    report.writeln(filler + "* exposed traits:");
                    for (IConcept i : exposed) {
                        describeConcept(i, report, indent + 4);
                    }
                }
            }

            IConcept byType = Observables.getByType(k);

            if (byType != null) {
                report.writeln(filler + "* interpreted as:");
                describeConcept(byType, report, indent + 4);
            }

            Collection<IConcept> downto = Observables.getRestrictedClosure(k);
            if (!downto.isEmpty()) {
                report.writeln(filler + "* restricted to: " + StringUtils.joinCollection(downto, '|'));
            }

        }

        if (isUniversal) {
            report.writeln(filler + "* " + (k.isAbstract() ? "abstract" : "concrete") + " universal");
        }

        if (!applicables.isEmpty()) {
            report.writeln(filler + "* applies to:");
            for (IConcept i : applicables) {
                describeConcept(i, report, indent + 4);
            }
        }

        if (describes != null && !k.equals(describes)) {
            report.writeln(filler + "* describes:");
            describeConcept(describes, report, indent + 4);
        }

    }

    /*
     * Filter-related methods. Require an instance because there are configurable options.
     * 
     * @param b
     */

    public void includeNonCoreConcepts(boolean b) {
        _includeNonCoreConcepts = b;
    }

    public void skipGenerated(boolean b) {
        _skipGenerated = b;
    }

    /**
     * TODO this should better take a regexp, but I have exactly three seconds left and
     * I'm not going to need it anyway.
     * 
     * @param main
     * @param core
     * @param prefix
     */
    public void setCoreOntologies(String main, String core, String prefix) {
        _mainOntology = main;
        _coreOntology = core;
        _mainNamespacePrefix = prefix;
    }

    abstract class ObservableFilter implements Filter<IKnowledge> {

        @Override
        public final boolean match(IKnowledge o) {

            boolean ok = true;
            if (!_includeNonCoreConcepts && (_mainOntology != null && _mainNamespacePrefix != null)) {
                ok = o.getConceptSpace().equals(_mainOntology)
                        || o.getConceptSpace().startsWith(_mainNamespacePrefix);
            }
            if (ok && _skipGenerated) {
                ok = !NS.isDerived(o);
            }
            return ok ? select(o) : false;
        }

        public abstract boolean select(IKnowledge o);

    }

    private Collection<IKnowledge> filter(Filter<IKnowledge> filter) {
        Collection<IKnowledge> ret = new ArrayList<>();
        for (IOntology o : (KLAB.KM).getOntologies(false)) {
            for (IConcept c : ((Ontology) o).getTopConcepts(true)) {
                if (filter.match(c))
                    ret.add(c);
            }
        }
        return ret;
    }

    public Collection<IKnowledge> getAllFundamentalQualities() {
        ArrayList<IKnowledge> ret = new ArrayList<>();
        IOntology o = (KLAB.KM).getOntology(_coreOntology);
        if (o != null) {
            for (IConcept c : ((Ontology) o).getTopConcepts(true)) {
                if (NS.isQuality(c)) {
                    ret.add(c);
                }
            }
        }
        return ret;
    }

    public Collection<IKnowledge> getAllPhysicalProperties() {
        return filter(new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return o.is(KLAB.c(NS.CORE_PHYSICAL_PROPERTY));
            }
        });
    }

    public Collection<IKnowledge> getAllFundamentalIdentities() {
        return filter(getAllFundamentalIdentitiesFilter());
    }

    public Collection<IKnowledge> getAllFundamentalSubjects() {
        return filter(getAllFundamentalSubjectsFilter());
    }

    public Collection<IKnowledge> getAllFundamentalAttributes() {
        return filter(getAllFundamentalAttributesFilter());
    }

    public Collection<IKnowledge> getAllFundamentalRealms() {
        return filter(getAllFundamentalRealmsFilter());
    }

    public Collection<IKnowledge> getAllRootProcesses() {
        return filter(getAllRootProcessesFilter());
    }

    public Collection<IKnowledge> getAllRootEvents() {
        return filter(getAllRootEventsFilter());
    }

    public Filter<IKnowledge> getAllFundamentalQualitiesFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return NS.isQuality(o);
            }
        };
    }

    public Filter<IKnowledge> getAllFundamentalSubjectsFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return NS.isThing(o);
            }
        };
    }

    public Filter<IKnowledge> getAllFundamentalAttributesFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return o.is(KLAB.c(NS.ATTRIBUTE_TRAIT)) && !NS.isObservable(o);
            }
        };
    }

    public Filter<IKnowledge> getAllFundamentalIdentitiesFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return o.is(KLAB.c(NS.CORE_IDENTITY_TRAIT)) && !NS.isObservable(o);
            }
        };
    }

    public Filter<IKnowledge> getAllFundamentalRealmsFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return o.is(KLAB.c(NS.CORE_REALM_TRAIT)) && !NS.isObservable(o);
            }
        };
    }

    public Filter<IKnowledge> getAllRootProcessesFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return NS.isProcess(o);
            }
        };
    }

    public Filter<IKnowledge> getAllRootEventsFilter() {
        return new ObservableFilter() {
            @Override
            public boolean select(IKnowledge o) {
                return NS.isEvent(o);
            }
        };
    }

    public Filter<IKnowledge> getAllUserObservableEvents(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    public Filter<IKnowledge> getAllUserObservableProcesses(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    public Filter<IKnowledge> getAllUserObservableQualities(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    public Filter<IKnowledge> getAllUserObservableSubjects(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    public Filter<IKnowledge> getAllUserObservableTraits(IContext context) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Create the concept that adopts all the passed traits, checking that concrete traits
     * are adopted by concrete concepts. Optionally also add the inherent concept ('of')
     * and the context concept ('within'). Do not add concepts that are already expressed
     * and raise exceptions if any concepts in the same hierarchy are not subsumed by the
     * intended traits. The new concept is created in the same ontology that contains
     * main.
     * 
     * @param main
     * @param traits
     * @param context
     * @param inherent
     * @return
     * @throws KlabValidationException
     */
    public static IConcept declareObservable(IConcept main, Collection<IConcept> traits)
            throws KlabValidationException {
        return declareObservable(main, traits, null, null, null, null, null, KLAB.REASONER.getOntology());
    }

    /**
     * Create the concept that adopts all the passed traits, checking that concrete traits
     * are adopted by concrete concepts. Optionally also add the inherent concept ('of')
     * and the context concept ('within'). Do not add concepts that are already expressed
     * and raise exceptions if any concepts in the same hierarchy are not subsumed by the
     * intended traits. The new concept is created in the same ontology that contains
     * main.
     * 
     * @param main
     * @param traits
     * @param context
     * @param inherent
     * @return
     * @throws KlabValidationException
     */
    public static IConcept declareObservable(IConcept main, Collection<IConcept> traits, IConcept context, IConcept inherent)
            throws KlabValidationException {
        return declareObservable(main, traits, context, inherent, null, null, null, KLAB.REASONER
                .getOntology());
    }

    /**
     * Create the concept that adopts all the passed traits, checking that concrete traits
     * are adopted by concrete concepts. Optionally also add the inherent concept ('of'),
     * the context concept ('within') and any roles ('with role'). Do not add concepts
     * that are already expressed and raise exceptions if any concepts in the same
     * hierarchy are not subsumed by the intended traits. Can be called without modifiers
     * (just returns main unmodified). TODO may want a parameter to decide whether we want
     * to store the ontology with the definition. Used to be like this but for now we put
     * all concepts in the overall ontology in the reasoner and not store it.
     * 
     * @param main
     * @param traits
     * @param context
     * @param inherent
     * @param ontology
     * @return
     * @throws KlabValidationException
     */
    public static IConcept declareObservable(IConcept main, Collection<IConcept> traits, IConcept context, IConcept inherent, Collection<IConcept> roles, IConcept byTrait, IConcept downTo, IOntology ontology)
            throws KlabValidationException {
        return declareObservable(main, traits, context, inherent, roles, byTrait, downTo, ontology, 0);
    }

    /**
     * Create the concept that adopts all the passed traits, checking that concrete traits
     * are adopted by concrete concepts. Optionally also add the inherent concept ('of'),
     * the context concept ('within') and any roles ('with role'). Do not add concepts
     * that are already expressed and raise exceptions if any concepts in the same
     * hierarchy are not subsumed by the intended traits. Can be called without modifiers
     * (just returns main unmodified). TODO may want a parameter to decide whether we want
     * to store the ontology with the definition. Used to be like this but for now we put
     * all concepts in the overall ontology in the reasoner and not store it.
     * 
     * @param main
     * @param traits
     * @param context
     * @param inherent
     * @param ontology
     * @return
     * @throws KlabValidationException
     */
    public static IConcept declareObservable(IConcept main, Collection<IConcept> traits, IConcept context, IConcept inherent, Collection<IConcept> roles, IConcept byTrait, IConcept downTo, IOntology ontology, int flags)
            throws KlabValidationException {

        /*
         * correctly support trival case so we can use this without checking.
         */
        if ((traits == null || traits.isEmpty()) && context == null && inherent == null
                && (roles == null || roles.isEmpty()) && byTrait == null && downTo == null) {
            return main;
        }

        Set<IConcept> identities = new HashSet<>();
        Set<IConcept> attributes = new HashSet<>();
        Set<IConcept> realms = new HashSet<>();

        /*
         * definition so that we can reconstruct the concept when we want.
         */
        String mainDefinition = ((Concept) main).getAssertedDefinition();

        /*
         * to ensure traits are not conflicting
         */
        Set<IConcept> baseTraits = new HashSet<>();

        /*
         * to ensure we know if we concretized any abstract traits so we can properly
         * compute our abstract status.
         */
        Set<IConcept> abstractTraitBases = new HashSet<>();

        IConcept ret = null;
        ArrayList<String> tids = new ArrayList<>();
        // ArrayList<IConcept> keep = new ArrayList<IConcept>();

        /*
         * preload any base traits we already have. If any of them is abstract, take
         * notice so we can see if they are all concretized later.
         */
        for (IConcept c : Traits.getTraits(main)) {
            IConcept base = NS.getBaseParentTrait(c);
            baseTraits.add(base);
            if (c.isAbstract()) {
                abstractTraitBases.add(base);
            }
        }

        /*
         * name and display label for the finished concept
         */
        String cId = "";
        String cDs = "";

        /*
         * we also add a non-by, non-down-to concept (untrasformed) if absent, so that we
         * can reconstruct an observable without transformations but with all traits and
         * roles if required. This is returned by getUntransformedObservable().
         */
        String uId = "";

        /*
         * final abstract status for result
         */
        String traitDefinition = "";

        if (traits != null && traits.size() > 0) {

            for (IConcept t : traits) {

                if (t.equals(main)) {
                    continue;
                }

                if (Traits.getTraits(main).contains(t)) {
                    throw new KlabValidationException("concept " + NS.getDisplayName(main)
                            + " already adopts trait " + NS.getDisplayName(t));
                }

                if (t.is(KLAB.c(NS.CORE_IDENTITY_TRAIT))) {
                    identities.add(t);
                } else if (t.is(KLAB.c(NS.CORE_REALM_TRAIT))) {
                    realms.add(t);
                } else if (!NS.isSubjective(t)) {
                    attributes.add(t);
                } else if ((flags & ACCEPT_SUBJECTIVE_OBSERVABLES) == 0) {
                    throw new KlabValidationException("an observable concept can only inherit subjective traits like "
                            + NS.getDisplayName(t) + " in a dependency or a private model");
                }

                IConcept base = NS.getBaseParentTrait(t);

                if (base == null) {
                    throw new KlabValidationException("trait " + t
                            + " cannot be understood as coming from a known declaration");
                }

                if (!baseTraits.add(base)) {
                    throw new KlabValidationException("cannot add trait " + NS.getDisplayName(t)
                            + " to concept " + main
                            + " as it already adopts a trait of type " + NS.getDisplayName(base));
                }

                if (t.isAbstract()) {
                    abstractTraitBases.add(base);
                } else {
                    abstractTraitBases.remove(base);
                }

                traitDefinition += (traitDefinition.isEmpty() ? "" : ",") + t.getDefinition();

                tids.add(NS.getDisplayName(t));

            }

        }

        /*
         * FIXME using the display name to build an ID is wrong and forces us to use
         * display names that are legal for concept names. The two should work
         * independently.
         */
        if (tids.size() > 0) {
            Collections.sort(tids);
            for (String s : tids) {
                cId += cleanInternalId(s);
                cDs += cleanInternalId(s);
                uId += cleanInternalId(s);
            }
        }

        boolean makeAbstract = false;
        if (main.isAbstract()) {
            /*
             * main remains abstract unless it's identified by an identity or realm, or
             * it's made inherent to a non-abstract subject.
             */
            makeAbstract = identities.size() == 0 && realms.size() == 0
                    && !(inherent != null && !inherent.isAbstract());
        }

        /*
         * it goes back to abstract if it's inheriting an abstract trait.
         */
        if (!makeAbstract && abstractTraitBases.size() > 0) {
            makeAbstract = true;
        }

        /*
         * add the main identity to the ID after all traits and before any context
         */
        cId += cleanInternalId(main.getLocalName());
        cDs += cleanInternalId(main.getLocalName());
        uId += cleanInternalId(main.getLocalName());

        /*
         * handle context and inherency
         */
        String inherentDefinition = "";
        if (inherent != null) {
            IConcept other = getInherentType(main);
            if (other != null && !isCompatible(inherent, other)) {
                throw new KlabValidationException("cannot add inherent type " + NS.getDisplayName(inherent)
                        + " to concept " + NS.getDisplayName(main)
                        + " as it already has an incompatible inherency: "
                        + NS.getDisplayName(other));
            }
            cId += "Of" + cleanInternalId(inherent.getLocalName());
            cDs += "Of" + cleanInternalId(inherent.getLocalName());
            uId += "Of" + cleanInternalId(inherent.getLocalName());
            inherentDefinition = inherent.getDefinition();
        }

        String contextDefinition = "";
        if (context != null) {
            IConcept other = getContextType(main);
            if (other != null && !isCompatible(context, other)) {
                throw new KlabValidationException("cannot add context " + NS.getDisplayName(context)
                        + " to concept " + NS.getDisplayName(main)
                        + " as it already has an incompatible context: " + NS.getDisplayName(other));
            }
            cId += "In" + cleanInternalId(context.getLocalName());
            cDs += "In" + cleanInternalId(context.getLocalName());
            uId += "In" + cleanInternalId(context.getLocalName());
            contextDefinition = context.getDefinition();
        }

        String byDefinition = "";
        String downToDefinition = "";
        Set<IConcept> allowedDetail = new HashSet<>();

        if (byTrait != null) {

            if (!NS.isTrait(byTrait)) {
                throw new KlabValidationException("the concept in a 'by' clause must be a base abstract trait");
            }

            /*
             * TODO trait must be a base trait and abstract.
             */
            if (!NS.isBaseDeclaration(byTrait) || !byTrait.isAbstract()) {
                throw new KlabValidationException("traits used in a 'by' clause must be abstract and declared at root level");
            }
            cId += "By" + cleanInternalId(byTrait.getLocalName());
            cDs += "By" + cleanInternalId(byTrait.getLocalName());
            byDefinition = byTrait.getDefinition();
            makeAbstract = true;
        }

        if (downTo != null) {
            IConcept trait = byTrait == null ? main : byTrait;
            if (!NS.isTrait(trait)) {
                throw new KlabValidationException("cannot use 'down to' on non-trait observables");
            }
            allowedDetail.addAll(Types.getChildrenAtLevel(trait, Types.getDetailLevel(trait, downTo)));
            cId += "DownTo" + cleanInternalId(downTo.getLocalName());
            // display label stays the same
            downToDefinition = downTo.getDefinition();
        }

        String roleDefinition = "";
        String roleIds = "";
        List<String> rids = new ArrayList<>();
        Set<IConcept> acceptedRoles = new HashSet<>();

        if (roles != null && roles.size() > 0) {
            for (IConcept role : roles) {
                if (Roles.getRoles(main).contains(role)) {
                    throw new KlabValidationException("concept " + NS.getDisplayName(main)
                            + " already has role " + NS.getDisplayName(role));
                }
                roleDefinition += (roleDefinition.isEmpty() ? "" : ",") + role.getDefinition();
                rids.add(NS.getDisplayName(role));
                acceptedRoles.add(role);
                if (role.isAbstract()) {
                    makeAbstract = true;
                }
            }
        }

        if (rids.size() > 0) {
            Collections.sort(rids);
            for (String s : rids) {
                roleIds += s;
            }
        }

        /*
         * add the main identity to the ID after all traits and before any context
         */
        if (!roleIds.isEmpty()) {
            cId += "As" + roleIds;
            // only add role names to user description if roles are not from the
            // root of the worldview
            if (!rolesAreFundamental(roles)) {
                cDs = roleIds + NS.getDisplayName(main);
            }
        }

        /*
         * Add a lowercase prefix to the ID to ensure no conflict can exist with a
         * situation like "TraitConcept is Trait Concept". The name without prefix will be
         * in the display label annotation.
         */
        cId = "i" + cId;

        ret = ontology.getConcept(cId);
        boolean needUntransformed = byTrait != null || downTo != null;
        if (needUntransformed) {
            needUntransformed = !uId.equals(cleanInternalId(main.getLocalName()))
                    && ontology.getConcept("i" + uId) == null;
        }

        IConcept uret = null;
        String untransformedDefinition = mainDefinition;

        if (ret == null) {

            List<IAxiom> axioms = new ArrayList<>();
            axioms.add(Axiom.ClassAssertion(cId));
            axioms.add(Axiom.AnnotationAssertion(cId, NS.DISPLAY_LABEL_PROPERTY, cDs));

            if (needUntransformed) {
                axioms.add(Axiom.ClassAssertion("i" + uId));
            }

            /*
             * if there is a 'by', this is the child of the class that exposes it, not the
             * original concept's.
             */
            axioms.add(Axiom
                    .SubClass((byTrait == null ? main.toString() : makeTypeFor(byTrait).toString()), cId));

            if (needUntransformed) {
                axioms.add(Axiom.SubClass(main.toString(), uId));
            }

            /*
             * Add the definition
             */
            if (makeAbstract) {
                axioms.add(Axiom.AnnotationAssertion(cId, NS.IS_ABSTRACT, "true"));
            }

            boolean isDerived = false;
            if (!traitDefinition.isEmpty()) {
                mainDefinition += "+T(" + traitDefinition + ")";
                if (needUntransformed) {
                    untransformedDefinition += "+T(" + untransformedDefinition + ")";
                }
                isDerived = true;
            }
            if (!inherentDefinition.isEmpty()) {
                mainDefinition += (traitDefinition.isEmpty() ? "+" : ",") + "I(" + inherentDefinition + ")";
                if (needUntransformed) {
                    untransformedDefinition += (traitDefinition.isEmpty() ? "+" : ",") + "I("
                            + inherentDefinition
                            + ")";
                }
                isDerived = true;
            }
            if (!contextDefinition.isEmpty()) {
                mainDefinition += ((traitDefinition.isEmpty() && inherentDefinition.isEmpty()) ? "+" : ",")
                        + "C("
                        + contextDefinition + ")";
                if (needUntransformed) {
                    untransformedDefinition += ((traitDefinition.isEmpty() && inherentDefinition.isEmpty())
                            ? "+" : ",")
                            + "C(" + contextDefinition + ")";
                }
                isDerived = true;
            }
            if (!roleDefinition.isEmpty()) {
                mainDefinition += ((traitDefinition.isEmpty() && inherentDefinition.isEmpty()
                        && contextDefinition.isEmpty()) ? "+" : ",") + "R(" + roleDefinition + ")";
                isDerived = true;
            }
            if (!byDefinition.isEmpty()) {
                mainDefinition += ((traitDefinition.isEmpty() && inherentDefinition.isEmpty()
                        && contextDefinition.isEmpty() && roleDefinition.isEmpty()) ? "+" : ",") + "B("
                        + byDefinition
                        + ")";
                isDerived = true;
            }
            if (!downToDefinition.isEmpty()) {
                mainDefinition += ((traitDefinition.isEmpty() && inherentDefinition.isEmpty()
                        && contextDefinition.isEmpty() && roleDefinition.isEmpty() && byDefinition.isEmpty())
                                ? "+"
                                : ",")
                        + "D(" + downToDefinition + ")";
                isDerived = true;
            }

            /*
             * add the ontology where to create the concept TODO disabled for now
             * (everything goes in the same ontology). See if we want to add a parameter
             * to enable it.
             */
            // if (isDerived) {
            // mainDefinition += ",O(" + ontology.getConceptSpace() + ")";
            // }

            axioms.add(Axiom.AnnotationAssertion(cId, NS.CONCEPT_DEFINITION_PROPERTY, mainDefinition));

            if (needUntransformed) {
                axioms.add(Axiom.AnnotationAssertion("i"
                        + uId, NS.CONCEPT_DEFINITION_PROPERTY, untransformedDefinition));
                axioms.add(Axiom
                        .AnnotationAssertion("i" + cId, NS.UNTRANSFORMED_CONCEPT_PROPERTY, "i" + uId));
            } else if (byTrait != null || downTo != null) {
                // untransformed is the main concept
                axioms.add(Axiom
                        .AnnotationAssertion(cId, NS.UNTRANSFORMED_CONCEPT_PROPERTY, main.toString()));
            }

            ontology.define(axioms);
            ret = ontology.getConcept(cId);
            if (needUntransformed) {
                uret = ontology.getConcept("i" + uId);
            }
        }

        if (identities.size() > 0) {
            Traits.restrict(ret, KLAB.p(NS.HAS_IDENTITY_PROPERTY), LogicalConnector.INTERSECTION, identities);
            if (needUntransformed) {
                Traits.restrict(uret, KLAB
                        .p(NS.HAS_IDENTITY_PROPERTY), LogicalConnector.INTERSECTION, identities);
            }
        }
        if (realms.size() > 0) {
            Traits.restrict(ret, KLAB.p(NS.HAS_REALM_PROPERTY), LogicalConnector.INTERSECTION, realms);
            if (needUntransformed) {
                Traits.restrict(uret, KLAB.p(NS.HAS_REALM_PROPERTY), LogicalConnector.INTERSECTION, realms);
            }
        }
        if (attributes.size() > 0) {
            Traits.restrict(ret, KLAB
                    .p(NS.HAS_ATTRIBUTE_PROPERTY), LogicalConnector.INTERSECTION, attributes);
            if (needUntransformed) {
                Traits.restrict(uret, KLAB
                        .p(NS.HAS_ATTRIBUTE_PROPERTY), LogicalConnector.INTERSECTION, attributes);
            }
        }
        if (acceptedRoles.size() > 0) {
            OWL.restrictSome(ret, KLAB.p(NS.HAS_ROLE_PROPERTY), LogicalConnector.INTERSECTION, acceptedRoles);
        }
        if (inherent != null) {
            OWL.restrictSome(ret, KLAB.p(NS.IS_INHERENT_TO_PROPERTY), inherent);
            if (needUntransformed) {
                OWL.restrictSome(uret, KLAB.p(NS.IS_INHERENT_TO_PROPERTY), inherent);
            }
        }
        if (context != null) {
            OWL.restrictSome(ret, KLAB.p(NS.HAS_CONTEXT_PROPERTY), context);
            if (needUntransformed) {
                OWL.restrictSome(uret, KLAB.p(NS.HAS_CONTEXT_PROPERTY), context);
            }
        }
        if (byTrait != null) {
            OWL.restrictSome(ret, KLAB.p(NS.REPRESENTED_BY_PROPERTY), byTrait);
        }
        if (downTo != null) {
            OWL.restrictSome(ret, KLAB.p(NS.LIMITED_BY_PROPERTY), LogicalConnector.UNION, allowedDetail);
        }

        return ret;
    }

    private static boolean rolesAreFundamental(Collection<IConcept> roles) {
        for (IConcept c : roles) {
            if (KLAB.getWorldview() != null && !c.getConceptSpace().equals(KLAB.getWorldview())) {
                return false;
            }
        }
        return true;
    }

    /**
     * If this observable has a 'by' and/or a 'down to' clause, return the version without
     * those transformations. Otherwise return the original concept.
     * 
     * @param observable
     * @return
     */
    public static IConcept getUntransformedObservable(IConcept observable) {
        String uid = observable.getMetadata().getString(NS.UNTRANSFORMED_CONCEPT_PROPERTY);
        if (uid == null) {
            return observable;
        }
        return uid.contains(":") ? KLAB.c(uid) : observable.getOntology().getConcept(uid);
    }

    // just remove the starting 'i' if any.
    private static String cleanInternalId(String s) {
        return s.startsWith("i") ? s.substring(1) : s;
    }

    /**
     * Produce a type that exposes a single passed trait.
     * 
     * @param trait
     * @return
     */
    public static IConcept makeTypeFor(IConcept trait) {

        if (NS.isClass(trait)) {
            return trait;
        }

        String traitID = cleanInternalId(trait.getLocalName()) + "Type";
        IConcept ret = trait.getOntology().getConcept(traitID);

        if (ret == null) {

            List<IAxiom> axioms = new ArrayList<>();
            axioms.add(Axiom.ClassAssertion(traitID));
            axioms.add(Axiom.SubClass(NS.TYPE, traitID));
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.TYPE
                    .name().toLowerCase() + " " + ((Concept) trait).getAssertedDefinition()));
            axioms.add(Axiom.AnnotationAssertion(traitID, IMetadata.DC_LABEL, trait.getLocalName()));
            // type of x are base declarations.
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.BASE_DECLARATION, "true"));
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.IS_TYPE_DELEGATE, "true"));
            trait.getOntology().define(axioms);
            ret = trait.getOntology().getConcept(traitID);
            OWL.restrictSome(ret, KLAB.p(NS.EXPOSES_TRAIT_PROPERTY), trait);

            /*
             * types inherit the context from their trait
             */
            IConcept context = getContextType(trait);
            if (context != null) {
                OWL.restrictSome(ret, KLAB.p(NS.HAS_CONTEXT_PROPERTY), context);
            }
        }

        return ret;
    }

    public static IConcept makeValue(IKnowledge concept, IKnowledge comparison) {

        if (concept.is(KLAB.c(NS.CORE_VALUE))) {
            return (IConcept) concept;
        }

        String cName = "ValueOf" + cleanInternalId(concept.getLocalName())
                + (comparison == null ? "" : ("Over" + cleanInternalId(comparison.getLocalName())));

        /*
         * make a ConceptCount if not there, and ensure it's a continuously quantifiable
         * quality. Must be in same ontology as the original concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_VALUE, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.VALUE.name()
                    .toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()
                    + (comparison == null ? "" : ("," + ((Concept) comparison).getAssertedDefinition()))));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
        }

        return ret;
    }

    public static IConcept makeRatio(IConcept concept, IConcept comparison) {

        /*
         * accept only two qualities of the same physical nature (TODO)
         */
        if (!(NS.isQuality(concept) || NS.isTrait(concept)) || !NS.isQuality(comparison)) {
            return null;
        }

        String cName = cleanInternalId(concept.getLocalName()) + "To"
                + cleanInternalId(comparison.getLocalName())
                + "Ratio";
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_RATIO, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.RATIO.name()
                    .toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()
                    + (comparison == null ? "" : ("," + ((Concept) comparison).getAssertedDefinition()))));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
        }

        return ret;
    }

    public static IConcept makeProportion(IConcept concept, IConcept comparison) {

        if (concept.is(KLAB.c(NS.CORE_PROPORTION))) {
            return concept;
        }

        if (!(NS.isQuality(concept) || NS.isTrait(concept)) && (comparison != null && !NS.isQuality(comparison))) {
            return null;
        }

        String cName = cleanInternalId(concept.getLocalName()) + "ProportionIn"
                + (comparison == null ? "" : cleanInternalId(comparison.getLocalName()));
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_PROPORTION, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.PROPORTION
                    .name().toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()
                    + (comparison == null ? "" : ("," + ((Concept) comparison).getAssertedDefinition()))));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
        }

        return ret;
    }

    public static IConcept makeUncertainty(IConcept concept) {

        if (concept.is(KLAB.c(NS.CORE_UNCERTAINTY))) {
            return null;
        }

        String cName = cleanInternalId(concept.getLocalName()) + "Uncertainty";
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_UNCERTAINTY, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.UNCERTAINTY
                    .name().toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
        }

        return ret;
    }

    public static IConcept makeProbability(IConcept concept) {

        if (!NS.isEvent(concept)) {
            return null;
        }

        String cName = cleanInternalId(concept.getLocalName()) + "Probability";
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_PROBABILITY, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.PROBABILITY
                    .name().toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));

            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);

            /*
             * probability is inherent to the event that's possible.
             */
            OWL.restrictSome(ret, KLAB.p(NS.IS_INHERENT_TO_PROPERTY), concept);
        }

        return ret;
    }

    public static IConcept makeOccurrence(IConcept concept) {

        if (!NS.isProcess(concept) && !NS.isThing(concept)) {
            return null;
        }

        String cName = cleanInternalId(concept.getLocalName()) + "Occurrence";
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_PROBABILITY, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.OCCURRENCE
                    .name().toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));

            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);

            /*
             * probability is inherent to the event that's possible.
             */
            OWL.restrictSome(ret, KLAB.p(NS.IS_INHERENT_TO_PROPERTY), concept);
        }

        return ret;
    }

    public static IConcept makePresence(IKnowledge concept) {

        if (concept.is(KLAB.c(NS.CORE_PRESENCE))) {
            return (IConcept) concept;
        }

        if (NS.isQuality(concept) || NS.isConfiguration(concept) || NS.isTrait(concept)
                || NS.isRole(concept)) {
            throw new KlabRuntimeException("presence can be observed only for subjects, events, processes and relationships");
        }

        String cName = cleanInternalId(concept.getLocalName()) + "Presence";

        /*
         * make a ConceptCount if not there, and ensure it's a continuously quantifiable
         * quality. Must be in same ontology as the original concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_PRESENCE, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.PRESENCE.name()
                    .toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);

            /*
             * presence is inherent to the thing that's present.
             */
            OWL.restrictSome(ret, KLAB.p(NS.IS_INHERENT_TO_PROPERTY), (IConcept) concept);
        }

        return ret;
    }

    public static IConcept makeDistanceTo(INamespace namespace, String whatFrom) {

        String cName = "DistanceTo" + whatFrom;

        /*
         * make a ConceptCount if not there, and ensure it's a continuously quantifiable
         * quality. Must be in same ontology as the original concept.
         */
        IConcept ret = namespace.getOntology().getConcept(cName);

        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_DISTANCE, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.DISTANCE.name()
                    .toLowerCase() + " " + "#" + whatFrom + "#" + namespace));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            namespace.getOntology().define(ax);
            ret = namespace.getOntology().getConcept(cName);

        }

        return ret;
    }

    /*
     * TODO finish; may be an object this time.
     */
    public static IConcept makeDistance(IConcept concept) {

        if (concept.is(KLAB.c(NS.CORE_DISTANCE))) {
            return concept;
        }

        if (!NS.isObject(concept)) {
            throw new KlabRuntimeException("cannot compute the distance to a quality");
        }

        String cName = "DistanceTo" + cleanInternalId(concept.getLocalName());

        /*
         * make a ConceptCount if not there, and ensure it's a continuously quantifiable
         * quality. Must be in same ontology as the original concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_DISTANCE, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.DISTANCE.name()
                    .toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
        }

        return ret;
    }

    public static IConcept makeCount(IConcept concept) {

        /*
         * first, ensure we're counting countable things.
         */
        if (!NS.isCountable(concept)) {
            return null;
        }

        String cName = cleanInternalId(concept.getLocalName()) + "Count";

        /*
         * make a ConceptCount if not there, and ensure it's a continuously quantifiable
         * quality. Must be in same ontology as the original concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_COUNT, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.COUNT.name()
                    .toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);

            /*
             * numerosity is inherent to the thing that's counted.
             */
            OWL.restrictSome(ret, KLAB.p(NS.IS_INHERENT_TO_PROPERTY), concept);
        }

        return ret;

    }

    public static IConcept makeAssessment(IConcept concept) {

        String cName = cleanInternalId(concept.getLocalName()) + "Assessment";

        if (!NS.isQuality(concept)) {
            return null;
        }

        /*
         * make a ConceptAssessment if not there, and ensure it's a continuously
         * quantifiable quality. Must be in same ontology as the original concept.
         */
        IConcept ret = concept.getOntology().getConcept(cName);

        if (ret == null) {

            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(cName));
            ax.add(Axiom.SubClass(NS.CORE_ASSESSMENT, cName));
            ax.add(Axiom.AnnotationAssertion(cName, NS.CONCEPT_DEFINITION_PROPERTY, Qualities.ASSESSMENT
                    .name().toLowerCase() + " " + ((Concept) concept).getAssertedDefinition()));
            ax.add(Axiom.AnnotationAssertion(cName, NS.BASE_DECLARATION, "true"));
            concept.getOntology().define(ax);
            ret = concept.getOntology().getConcept(cName);
            OWL.restrictSome(ret, KLAB.p(NS.OBSERVES_PROPERTY), concept);
        }

        return ret;

    }

    /**
     * Get all the restricted target of the "applies to" specification for this concept.
     * 
     * @param main
     * @return
     */
    public static Collection<IConcept> getApplicableObservables(IKnowledge main) {
        if (!(main instanceof IConcept)) {
            return new HashSet<>();
        }
        return OWL.getRestrictedClasses((IConcept) main, KLAB.p(NS.APPLIES_TO_PROPERTY));
    }

    /**
     * Get all traits conferred by this process.
     * 
     * @param processConcept
     * @return
     */
    public static Collection<IConcept> getConferredTraits(IConcept processConcept) {
        return OWL.getRestrictedClasses(processConcept, KLAB.p(NS.CONFERS_TRAIT_PROPERTY));
    }

    /**
     * Get the quality described by this trait, or null.
     * 
     * @param trait
     * @return
     */
    public static IConcept getDescribedQuality(IConcept trait) {
        Collection<IConcept> cls = OWL.getRestrictedClasses(trait, KLAB.p(NS.DESCRIBES_QUALITY_PROPERTY));
        return cls.isEmpty() ? null : cls.iterator().next();
    }

    /**
     * Get the quality described by this trait, or null.
     * 
     * @param trait
     * @return
     */
    public static Collection<IConcept> getDescribedQualities(IConcept configuration) {
        return OWL.getRestrictedClasses(configuration, KLAB.p(NS.DESCRIBES_QUALITY_PROPERTY));
    }

    /**
     * Set the "applied to" clause for a trait or observable. Should also validate.
     * 
     * @param type
     * @param applicables
     */
    public static void setApplicableObservables(IConcept type, List<IConcept> applicables)
            throws KlabValidationException {
        // TODO validate
        OWL.restrictSome(type, KLAB.p(NS.APPLIES_TO_PROPERTY), LogicalConnector.UNION, applicables);
    }

    /**
     * Sets the X in "between X and Y" clause for a role that "applies to" relationship R.
     * Must restrict a role that applies to a subject.
     * 
     * @param type
     * @param applicables
     * @throws KlabValidationException
     */
    public static void setApplicableSource(IConcept type, IConcept applicable)
            throws KlabValidationException {
        if (!NS.isRole(applicable) || !NS.isThing(getApplicableObservables(applicable))) {
            throw new KlabValidationException("a role for the source of a relationship must apply to a subject");
        }
        IConcept previous = getApplicableSource(type);
        if (previous != null && !applicable.is(previous)) {
            throw new KlabValidationException("source role " + NS.getDisplayName(applicable)
                    + " is incompatible with previously defined " + NS.getDisplayName(previous));
        }
        OWL.restrictSome(type, KLAB.p(NS.IMPLIES_SOURCE_PROPERTY), LogicalConnector.UNION, Collections
                .singleton(applicable));
    }

    /**
     * Sets the Y in "between X and Y" clause for a role that "applies to" relationship R.
     * 
     * @param type
     * @param applicables
     * @throws KlabValidationException
     */
    public static void setApplicableDestination(IConcept type, IConcept applicable)
            throws KlabValidationException {
        if (!NS.isRole(applicable) || !NS.isThing(getApplicableObservables(applicable))) {
            throw new KlabValidationException("a role for the source of a relationship must apply to a subject");
        }
        IConcept previous = getApplicableDestination(type);
        if (previous != null && !applicable.is(previous)) {
            throw new KlabValidationException("destination role " + NS.getDisplayName(applicable)
                    + " is incompatible with previously defined " + NS.getDisplayName(previous));
        }
        OWL.restrictSome(type, KLAB.p(NS.IMPLIES_DESTINATION_PROPERTY), LogicalConnector.UNION, Collections
                .singleton(applicable));
    }

    /**
     * Return the source role implied by a relationship role.
     * 
     * @param relationshipRole
     * @return
     */
    public static IConcept getApplicableSource(IConcept relationshipRole) {
        Collection<IConcept> cls = OWL
                .getRestrictedClasses(relationshipRole, KLAB.p(NS.IMPLIES_SOURCE_PROPERTY));
        return cls.isEmpty() ? null : cls.iterator().next();
    }

    /**
     * Return the destination role implied by a relationship role.
     * 
     * @param relationshipRole
     * @return
     */
    public static IConcept getApplicableDestination(IConcept relationshipRole) {
        Collection<IConcept> cls = OWL
                .getRestrictedClasses(relationshipRole, KLAB.p(NS.IMPLIES_DESTINATION_PROPERTY));
        return cls.isEmpty() ? null : cls.iterator().next();
    }

    /**
     * Set the 'confers' clause for a process. TODO needs to record the optional 'to'
     * part.
     * 
     * @param type
     * @param conferred
     * @throws KlabValidationException
     */
    public static void setConferredTraits(IConcept type, List<IConcept> conferred)
            throws KlabValidationException {
        // TODO validate
        OWL.restrictSome(type, KLAB.p(NS.CONFERS_TRAIT_PROPERTY), LogicalConnector.UNION, conferred);
    }

    /**
     * Set the 'describes' quality for a trait.
     */
    public static void setDescribedQuality(IConcept trait, IConcept quality) throws KlabValidationException {
        // TODO validate
        OWL.restrictSome(trait, KLAB.p(NS.DESCRIBES_QUALITY_PROPERTY), quality);
    }

    /**
     * Set the 'describes' configuration for a quality.
     */
    public static void setDescribedConfiguration(IConcept quality, IConcept configuration)
            throws KlabValidationException {
        // TODO validate
        OWL.restrictSome(configuration, KLAB.p(NS.DESCRIBES_QUALITY_PROPERTY), quality);
    }

    /**
     * Return the observable object corresponding to the requested observation. Any object
     * not understood is returned unmodified; the ones that are translated to others are
     * traits (turned to types) and roles (turned to observations of their applicable
     * abstract observables with the role). If a context is passed and a concept is being
     * requested, ensure that the context is compatible with the current context; if not,
     * throw a validation exception.
     * 
     * @param observable
     * @param context
     * @return
     * @throws KlabValidationException
     */
    public static Object getObservableObject(Object observable, ISubject context) throws KlabException {

        // for error reporting only
        String odef = observable.toString();

        IConcept concept = observable instanceof IConcept ? ((IConcept) observable) : null;
        if (concept == null && observable instanceof String) {
            concept = KLAB.KM.getConcept(observable.toString());
        }
        if (concept != null) {

            IConcept restrictedContextType = getContextType(concept);
            IConcept actualContextType = context == null ? null
                    : (IConcept) context.getObservable().getType();

            if (actualContextType == null
                    && Environment.get().getSpatialForcing().getMaxX() > Environment.get()
                            .getSpatialForcing().getMinX()) {
                GeoNS.synchronize();
                actualContextType = GeoNS.PLAIN_REGION;
                Environment.get().defineLastContextObserved(Environment.get().getSpatialForcing());
            }

            if (NS.isTrait(concept)) {
                if (restrictedContextType == null) {
                    throw new KlabValidationException("trait " + concept
                            + " cannot be contextualized because it does not describe a contextualizable observable");
                }
                observable = Observables.makeTypeFor(concept);
            } else if (NS.isRole(concept)) {
                observable = Roles.getObservableWithRole(concept, actualContextType);
            }
            if (context != null && restrictedContextType != null
                    && !isCompatible(actualContextType, restrictedContextType)) {

                /*
                 * CHECK INHERENCY
                 */

                /*
                 * TODO exception only if no inherency is possible to distribute its
                 * context(s) within the actual context. For now pass everything.
                 */

                // throw new KlabValidationException(odef
                // + " cannot be observed in this context: a "
                // + NS.getDisplayName(actualContextType)
                // + " is not a " + NS.getDisplayName(restrictedContextType));
            }
        }

        return observable;
    }

    /**
     * Retrieve all possible matching identifiers to locate the likely name for a concept
     * in user input, in order of "goodness of match". Used to match to externally created
     * definitions, e.g. in imported models, Bayesian nodes etc.
     * 
     * @param observable
     * @return
     */
    public static List<String> getIdentifiersFor(IConcept observable) {

        List<String> ret = new ArrayList<>();

        IConcept concept = observable.getType();
        if (Types.isDelegate(concept)) {
            concept = Types.getExposedTraits(concept).iterator().next();
        }
        if (NS.isObservability(concept)) {
            concept = Traits.getImpliedObservable(concept);
        }

        insertIds(concept, ret);

        IConcept coreObservable = getCoreObservable(concept);
        if (coreObservable != null) {

            /**
             * Core observable with all traits and nothing else is a better match than the
             * core observable alone.
             */
            Collection<IConcept> traits = Traits.getTraits(concept);
            if (traits.size() > 0) {
                try {
                    IConcept traitsOnly = Observables.declareObservable(coreObservable, traits, null, null);
                    if (!traitsOnly.equals(concept)) {
                        insertIds(traitsOnly, ret);
                    }
                } catch (KlabValidationException e) {
                    // just don't - shoudldn't happen
                }

            }
            insertIds(coreObservable, ret);
        }

        return ret;
    }

    private static void insertIds(IConcept concept, List<String> ids) {
        String id = NS.getDisplayName(concept);
        if (!ids.contains(id)) {
            ids.add(id);
        }
        id = CamelCase.toLowerCase(id, '-');
        if (!ids.contains(id)) {
            ids.add(id);
        }
    }

    public static String getDeclaration(IConcept observable) {
        return getDeclaration(observable, null);
    }

    public static String getDeclaration(IConcept observable, IProject project) {

        String ret = "";
        try {

            Definition def = Definition.parse(observable.getDefinition());
            ret = def.getDeclaration(project);

        } catch (KlabValidationException e) {
            throw new KlabRuntimeException(e);
        }

        return ret;
    }

}
