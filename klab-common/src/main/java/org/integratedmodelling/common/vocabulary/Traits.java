package org.integratedmodelling.common.vocabulary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.errormanagement.CompileWarning;
import org.integratedmodelling.common.owl.Concept;
import org.integratedmodelling.common.owl.OWL;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.LogicalConnector;

public class Traits {

    public static Collection<IConcept> getTraits(IKnowledge concept) {

        Set<IConcept> ret = new HashSet<>();

        if (concept instanceof IConcept) {
            ret.addAll(OWL.getRestrictedClasses((IConcept) concept, KLAB.p(NS.HAS_REALM_PROPERTY)));
            ret.addAll(OWL.getRestrictedClasses((IConcept) concept, KLAB.p(NS.HAS_IDENTITY_PROPERTY)));
            ret.addAll(OWL.getRestrictedClasses((IConcept) concept, KLAB.p(NS.HAS_ATTRIBUTE_PROPERTY)));
        }
        return ret;
    }

    public static IConcept getTrait(IKnowledge concept, IConcept trait) {
        for (IConcept c : getTraits(concept)) {
            if (c.is(trait)) {
                return c;
            }
        }
        return null;
    }

    public static Collection<IConcept> getRealms(IConcept concept) {
        return OWL.getRestrictedClasses(concept, KLAB.p(NS.HAS_REALM_PROPERTY));
    }

    public static Collection<IConcept> getIdentities(IConcept concept) {
        return OWL.getRestrictedClasses(concept, KLAB.p(NS.HAS_IDENTITY_PROPERTY));
    }

    public static Collection<IConcept> getAttributes(IConcept concept) {
        return OWL.getRestrictedClasses(concept, KLAB.p(NS.HAS_ATTRIBUTE_PROPERTY));
    }

    public static IConcept getImpliedObservable(IKnowledge concept) {
        
        if (!(concept instanceof IConcept)) {
            return null;
        }
        Collection<IConcept> cls = OWL
                .getRestrictedClasses((IConcept) concept, KLAB.p(NS.IMPLIES_OBSERVABLE_PROPERTY));
        return cls.isEmpty() ? null : cls.iterator().next();
    }

    public static Collection<IConcept> getExposedTraits(IConcept type) {
        return OWL.getRestrictedClasses(type, KLAB.p(NS.EXPOSES_TRAIT_PROPERTY));
    }

    /**
     * Return the concept that defines the abstract trait of the observability of the
     * passed observable in the context, or the concrete trait of it being actually
     * observable. Use {@link #getNegation(IConcept)} to deny the concrete trait if
     * necessary. Both concepts (the abstract and the concrete) are created when missing
     * upon a single invocation.
     * 
     * @param observable
     * @param isAbstract
     * @return
     */
    public static IConcept getObservabilityOf(IConcept observable, boolean isAbstract) {

        if (observable.is(KLAB.c(NS.OBSERVABILITY_TRAIT))) {
            return observable;
        }

        String traitID = observable.getLocalName() + "Observability";
        String yesTraitID = observable.getLocalName() + "Observable";

        IConcept ret = observable.getOntology().getConcept(traitID);

        if (ret == null) {

            String mainDefinition = "observability " + ((Concept) observable).getAssertedDefinition();
            String yesDefinition = "observable " + ((Concept) observable).getAssertedDefinition();

            List<IAxiom> axioms = new ArrayList<>();

            axioms.add(Axiom.ClassAssertion(traitID));
            axioms.add(Axiom.SubClass(NS.OBSERVABILITY_TRAIT, traitID));
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.IS_ABSTRACT, "true"));
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.CONCEPT_DEFINITION_PROPERTY, mainDefinition));
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.DENIABILITY_PROPERTY, "true"));
            axioms.add(Axiom.ClassAssertion(yesTraitID));
            axioms.add(Axiom.SubClass(traitID, yesTraitID));
            axioms.add(Axiom.AnnotationAssertion(yesTraitID, NS.CONCEPT_DEFINITION_PROPERTY, yesDefinition));

            observable.getOntology().define(axioms);
            ret = observable.getOntology().getConcept(traitID);

			/*
			 * new trait inherits the context from the observable
			 */
			IConcept context = Observables.getContextType(observable);
			if (context != null) {
				OWL.restrictSome(ret, KLAB.p(NS.HAS_CONTEXT_PROPERTY), context);
			}
            
            /**
             * Add the implication restriction referring to the original observable
             */
            OWL.restrictSome(ret, KLAB.p(NS.IMPLIES_OBSERVABLE_PROPERTY), observable);

        }

        return isAbstract ? observable.getOntology().getConcept(traitID)
                : observable.getOntology().getConcept(yesTraitID);
    }

    /**
     * Get the negation ('not') of a trait. If the original trait implied an observable,
     * the negated trait will be made a subclass of Absence, to mean that it implies its
     * absence.
     * 
     * @param trait
     * @return
     * @throws KlabValidationException
     */
    public static IConcept getNegation(IConcept trait) throws KlabValidationException {

        IConcept base = NS.getBaseParentTrait(trait);

        if (base == null) {
            throw new KlabValidationException("trait " + trait
                    + " cannot be understood as coming from a known declaration");
        }

        
        String deniable = trait.getMetadata().getString(NS.DENIABILITY_PROPERTY);
        if (deniable == null && !base.equals(trait)) {
            deniable = base.getMetadata().getString(NS.DENIABILITY_PROPERTY);
        }
        
        if (deniable == null || !deniable.equals("true")) {
            throw new KlabValidationException("cannot negate the non-deniable attribute " + trait);
        }

        String traitID = "Not" + trait.getLocalName();
        IConcept ret = trait.getOntology().getConcept(traitID);
        IConcept implied = getImpliedObservable(trait);

        if (ret == null) {

            String mainDefinition = "not " + ((Concept) trait).getAssertedDefinition();

            List<IAxiom> axioms = new ArrayList<>();
            axioms.add(Axiom.ClassAssertion(traitID));
            axioms.add(Axiom.SubClass(NS.OBSERVABILITY_TRAIT, traitID));
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.IS_ABSTRACT, trait.isAbstract()));
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.CONCEPT_DEFINITION_PROPERTY, mainDefinition));
            axioms.add(Axiom.AnnotationAssertion(traitID, NS.ORIGINAL_TRAIT, trait.toString()));
            if (implied != null) {
                axioms.add(Axiom.SubClass(NS.ABSENCE_TRAIT, traitID));
            }
            for (IConcept parent : trait.getParents()) {
                axioms.add(Axiom.SubClass(parent.toString(), traitID));
            }
            trait.getOntology().define(axioms);
            ret = trait.getOntology().getConcept(traitID);
        }

        return ret;
    }

    /**
     * Add traits and/or context and inherent type to an existing concept, checking that
     * nothing is inconsistent. Expects a declared concept, so it will not change the
     * logical definition.
     * 
     * @param concept
     * @param traits
     * @param context
     * @param inherent
     * @return a collection of any warnings captured during the operation.
     * 
     * @throws KlabValidationException
     */
    public static Collection<ICompileWarning> addTraitsToDeclaredConcept(IConcept concept, Collection<IConcept> traits, IConcept context, IConcept inherent, int flags)
            throws KlabValidationException {

        List<ICompileWarning> warnings = new ArrayList<>();

        Set<IConcept> identities = new HashSet<>();
        Set<IConcept> attributes = new HashSet<>();
        Set<IConcept> realms = new HashSet<>();

        /*
         * to ensure traits are not conflicting
         */
        Set<IConcept> baseTraits = new HashSet<>();

        /*
         * to ensure we know if we concretized any abstract traits so we can properly
         * compute our abstract status.
         */
        Set<IConcept> abstractTraitBases = new HashSet<>();

        boolean allAttributes = true;

        /*
         * preload any base traits we already have. If any of them is abstract, take
         * notice so we can see if they are all concretized later.
         */
        for (IConcept c : getTraits(concept)) {
            IConcept base = NS.getBaseParentTrait(c);
            baseTraits.add(base);
            if (c.isAbstract()) {
                abstractTraitBases.add(base);
            }
            if (!c.is(KLAB.c(NS.ATTRIBUTE_TRAIT))) {
                allAttributes = false;
            }
        }

        /**
         * If we're adding traits to a trait, only attributes are allowed.
         */
        if (NS.isTrait(concept) && !allAttributes) {
            throw new KlabValidationException(concept
                    + ": traits can only adopt attributes as their own traits");
        }

        if (traits != null && traits.size() > 0) {

            Collection<IConcept> originalTraits = getTraits(concept);

            for (IConcept t : traits) {

                if (originalTraits.contains(t)) {
                    /*
                     * TODO this could be an exception in strict mode.
                     */
                    warnings.add(new CompileWarning("concept " + concept + " already adopts trait " + t));
                    continue;
                }

                if (t.is(KLAB.c(NS.CORE_IDENTITY_TRAIT))) {
                    identities.add(t);
                } else if (t.is(KLAB.c(NS.CORE_REALM_TRAIT))) {
                    realms.add(t);
                } else if (!NS.isSubjective(t)) {
                    attributes.add(t);
                } else if ((flags & Observables.ACCEPT_SUBJECTIVE_OBSERVABLES) == 0){
                    throw new KlabValidationException("an observable concept can only inherit subjective traits like "
                            + NS.getDisplayName(t) + " in a dependency or a private model");
                }

                IConcept base = NS.getBaseParentTrait(t);

                if (!baseTraits.add(base)) {
                    throw new KlabValidationException("cannot add trait " + t + " to concept " + concept
                            + " as it already adopts a trait of type " + base);
                }

                if (t.isAbstract()) {
                    abstractTraitBases.add(base);
                } else {
                    abstractTraitBases.remove(base);
                }
            }
        }

        boolean makeAbstract = false;
        if (concept.isAbstract()) {
            makeAbstract = identities.size() == 0 && realms.size() == 0;
        }
        if (!makeAbstract && abstractTraitBases.size() > 0) {
            makeAbstract = true;
        }

        /*
         * handle context and inherency
         */
        if (inherent != null) {
            IConcept other = Observables.getInherentType(concept);
            if (other != null && !inherent.is(other)) {
                throw new KlabValidationException("cannot add inherent type " + inherent + " to concept "
                        + concept + " as it already has an incompatible inherency: " + other);
            }
        }

        if (context != null) {
            IConcept other = Observables.getContextType(concept);
            if (other != null && !context.is(other)) {
                throw new KlabValidationException("cannot add context " + context + " to concept " + concept
                        + " as it already has an incompatible context: " + other);
            }
        }

        if (identities.size() > 0) {
            OWL.restrictSome(concept, KLAB
                    .p(NS.HAS_IDENTITY_PROPERTY), LogicalConnector.INTERSECTION, identities);
        }
        if (realms.size() > 0) {
            OWL.restrictSome(concept, KLAB.p(NS.HAS_REALM_PROPERTY), LogicalConnector.INTERSECTION, realms);
        }
        if (attributes.size() > 0) {
            OWL.restrictSome(concept, KLAB
                    .p(NS.HAS_ATTRIBUTE_PROPERTY), LogicalConnector.INTERSECTION, attributes);
        }
        if (inherent != null) {
            OWL.restrictSome(concept, KLAB.p(NS.IS_INHERENT_TO_PROPERTY), inherent);
        }
        if (context != null) {
            OWL.restrictSome(concept, KLAB.p(NS.HAS_CONTEXT_PROPERTY), context);
        }

        return warnings;

    }

    public static void restrict(IConcept target, IProperty property, LogicalConnector how, Set<IConcept> fillers)
            throws KlabValidationException {

        /*
         * divide up in bins according to base trait; take property from annotation;
         * restrict each group.
         */
        Map<IConcept, List<IConcept>> pairs = new HashMap<>();
        for (IConcept t : fillers) {
            IConcept base = NS.getBaseParentTrait(t);
            if (!pairs.containsKey(base)) {
                pairs.put(base, new ArrayList<>());
            }
            pairs.get(base).add(t);
        }

        for (IConcept base : pairs.keySet()) {

            String prop = base.getMetadata().getString(NS.TRAIT_RESTRICTING_PROPERTY);
            if (prop == null || KLAB.KM.getProperty(prop) == null) {
                if (NS.isSubjective(base)) {
                    /*
                     * we can assign any subjective traits to anything
                     */
                    prop = NS.HAS_SUBJECTIVE_TRAIT_PROPERTY;
                } else {
                    throw new KlabValidationException("cannot find property to restrict for trait " + base);
                }
            }
            OWL.restrictSome(target, KLAB.p(prop), how, pairs.get(base));
        }

    }

    /**
     * Check if concept k carries the passed trait. Uses is() on all explicitly expressed
     * traits.
     * 
     * @param k
     * @param trait
     * @return
     */
    public static boolean hasTrait(ISemantic k, IConcept trait) {

        IKnowledge type = k.getType();

        if (!(type instanceof IConcept)) {
            return false;
        }

        for (IConcept c : getTraits(type)) {
            if (c.is(trait)) {
                return true;
            }
        }

        return false;
    }
    
    /**
     * Check if concept k carries a trait T so that the passed trait is-a T.
     * 
     * @param k
     * @param trait
     * @return
     */
    public static boolean hasParentTrait(ISemantic k, IConcept trait) {

        IKnowledge type = k.getType();

        if (!(type instanceof IConcept)) {
            return false;
        }

        for (IConcept c : getTraits(type)) {
            if (trait.is(c)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return the observable that represent the passed one without any of the traits it
     * may inherit.
     * 
     * @param observable
     * @return naked observable
     */
    public static IKnowledge getBaseObservable(IKnowledge observable) {

        if (!(observable instanceof IConcept)) {
            return observable;
        }
        if (getTraits(observable).isEmpty()) {
            return observable;
        }
        for (IConcept o : ((IConcept) observable).getParents()) {
            if (NS.isObservable(o)) {
                return getBaseObservable(o);
            }
        }
        return null;
    }

    /**
     * Return the passed knowledge after removing any traits of the passed base type, or
     * the original knowledge if the trait was absent.
     * 
     * @param knowledge
     * @return knowledge without the trait
     * @throws KlabValidationException
     */
    public static IKnowledge removeTrait(IConcept knowledge, IConcept baseTrait)
            throws KlabValidationException {

        Pair<IConcept, Collection<IConcept>> trs = separateAttributes(knowledge);
        ArrayList<IConcept> traits = new ArrayList<>();

        boolean found = false;
        for (IConcept tr : trs.getSecond()) {
            if (tr.is(baseTrait)) {
                found = true;
            } else {
                traits.add(tr);
            }
        }

        return found
                ? Observables.declareObservable(trs.getFirst(), traits, Observables
                        .getContextType(knowledge), Observables
                                .getInherentType(knowledge))
                : knowledge;
    }

    /**
     * Analyze an observable concept and return the main observable with all the original
     * identities and realms but no attributes; separately, return the list of the
     * attributes that were removed.
     * 
     * @param observable
     * @return attribute profile
     * @throws KlabValidationException
     */
    public static Pair<IConcept, Collection<IConcept>> separateAttributes(IKnowledge observable)
            throws KlabValidationException {

        IKnowledge obs = getBaseObservable(observable);
        ArrayList<IConcept> tret = new ArrayList<>();
        ArrayList<IConcept> keep = new ArrayList<>();

        for (IConcept zt : getTraits(observable)) {
            if (zt.is(KLAB.c(NS.CORE_IDENTITY_TRAIT)) || zt.is(KLAB.c(NS.CORE_REALM_TRAIT))) {
                keep.add(zt);
            } else {
                tret.add(zt);
            }
        }

        return new Pair<>(Observables.declareObservable((IConcept) (obs == null ? observable
                : obs), keep, Observables.getContextType(observable), Observables
                        .getInherentType(observable)), tret);
    }

    /**
     * If passed concept is a trait, return it unmodified; else if the concept is
     * observable, return the abstract trait corresponding to the observability of the
     * concept and ensure that its superclass and negation exist in the knowledge base. If
     * the concept is not observable, return null.
     * 
     * @param concept
     * @return trait that represents concept
     */
    public static IConcept getTraitFor(IKnowledge concept) {

        if (NS.isTrait(concept)) {
            return (IConcept) concept;
        }
        IConcept ret = null;
        if (NS.isObservable(concept)) {
            String traitID = concept.getLocalName() + "Observability";
            ret = concept.getOntology().getConcept(traitID);
            if (ret == null) {
                String yesTraitID = concept.getLocalName() + "Observable";
                String noTraitID = concept.getLocalName() + "NotObservable";
                List<IAxiom> axioms = new ArrayList<>();
                axioms.add(Axiom.ClassAssertion(traitID));
                axioms.add(Axiom.AnnotationAssertion(traitID, NS.IS_ABSTRACT, "true"));
                axioms.add(Axiom.SubClass(NS.OBSERVABILITY_TRAIT, traitID));
                axioms.add(Axiom.ClassAssertion(yesTraitID));
                axioms.add(Axiom.ClassAssertion(noTraitID));
                axioms.add(Axiom.SubClass(traitID, yesTraitID));
                axioms.add(Axiom.SubClass(traitID, noTraitID));
                concept.getOntology().define(axioms);
                ret = concept.getOntology().getConcept(traitID);
            }
        }
        return ret;
    }

    /**
     * Get the concrete observability trait corresponding to the abstract observability
     * trait passed. Must have been created by system. Everything else will return null.
     * 
     * @param abstractObservabilityTrait
     * @param negated
     * @return observability of trait
     */
    public static IConcept getConcreteObservabilityTrait(IConcept abstractObservabilityTrait, boolean negated) {

        if (!abstractObservabilityTrait.is(KLAB.c(NS.OBSERVABILITY_TRAIT))
                || !abstractObservabilityTrait.getLocalName().endsWith("Observability")) {
            return null;
        }

        String traitID = abstractObservabilityTrait.getLocalName();
        traitID = traitID.substring(0, traitID.length() - "Observability".length());

        return negated ? abstractObservabilityTrait.getOntology().getConcept(traitID + "NotObservable")
                : abstractObservabilityTrait.getOntology().getConcept(traitID + "Observable");
    }
}
