package org.integratedmodelling.common.vocabulary.authority;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.metadata.ISearchResult;
import org.integratedmodelling.common.beans.authority.Authority;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.client.EngineController;
import org.integratedmodelling.common.client.ModelingClient;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Only authority implementation we need on the client side. Initialized by the
 * authorities returned in the engine's capabilities, in turn harvested from the network.
 * 
 * @author Ferd
 *
 */
public class ClientAuthority extends BaseAuthority {

    Map<String, AuthorityConcept> conceptCache = new HashMap<>();

    public ClientAuthority(Authority definition) {
        super(definition);
    }

    @Override
    public AuthorityConcept getConcept(String authority, String id) throws KlabValidationException {

        AuthorityConcept ret = conceptCache.get(authority + ":" + id);
        if (ret != null) {
            return ret;
        }

        EngineController controller = null;
        if (KLAB.ENGINE instanceof ModelingClient) {
            controller = ((ModelingClient) KLAB.ENGINE).getCurrentEngineController();
        }
        if (controller != null) {
            ret = controller.getAuthorityConcept(authority, id);
            if (ret != null) {
                conceptCache.put(authority + ":" + id, ret);
            } else {
                throw new KlabValidationException("authority " + authority + " does not recognize identifier "
                        + id);
            }
        }
        return ret;
    }

    @Override
    public ISearchResult<AuthorityConcept> search(String query, String authorityId) {
        EngineController controller = null;
        if (KLAB.ENGINE instanceof ModelingClient) {
            controller = ((ModelingClient) KLAB.ENGINE).getCurrentEngineController();
        }
        if (controller != null) {
            return controller.searchAuthority(authorityId, query);
        }
        return null;
    }

}
