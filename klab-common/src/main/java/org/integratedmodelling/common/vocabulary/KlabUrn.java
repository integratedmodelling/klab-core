package org.integratedmodelling.common.vocabulary;

import org.integratedmodelling.Version;

/**
 * Simple helper to decompose a URN into its constituents and access
 * them with proper semantics.
 * 
 * URN is formatted as <node name>:<originator>:<namespace>:<resource id>
 * 
 * @author Ferd
 *
 */
public class KlabUrn {

    private String urn;
    private String[] tokens;

    /**
     * Pass a valid URN string. For now does no validation.
     * 
     * @param urn
     */
    public KlabUrn(String urn) {
        this.urn = urn;
        this.tokens = urn.split(":");
    }
    
    /**
     * Node name, mandatory in all URNs. In universal ones it will
     * be "klab". In local ones, it will be "local". 
     * 
     * @return the node name.
     */
    public String getNodeName() {
        return tokens[0];
    }
    
    /**
     * Whether the URN should be processed by the same engine that
     * generates it.
     * 
     * @return true if local
     */
    public boolean isLocal() {
        return getNodeName().equals("local");
    }

    /**
     * Whether the URN can be processed by any node.
     * 
     * @return true if universal.
     */
    public boolean isUniversal() {
        return getNodeName().equals("klab");
    }

    /**
     * Return the originator, username or institution that owns the
     * resource. Never null.
     *  
     * @return the originator
     */
    public String getOriginator() {
        return tokens[1];
    }
    
    /**
     * Return the namespace of the resource if one has been set.
     * 
     * @return a namespace id, or null.
     */
    public String getNamespace() {
        return tokens.length > 2 ? tokens[2] : null;
    }
    
    /**
     * Return the resource ID. Never null.
     * 
     * @return the resource id.
     */
    public String getResourceId() {
        return tokens.length > 3 ? tokens[3] : null;
    }

    /**
     * Return the version, if any.
     * 
     * @return
     */
    public Version getVersion() {
    	return tokens.length > 4 ? new Version(tokens[4]) : null;
    }
    
    /**
     * Unmodified URN string.
     * 
     * @return the unmodified URN.
     */
    public String getUrn() {
        return urn;
    }
    
    @Override
    public String toString() {
        return urn;
    }

}
