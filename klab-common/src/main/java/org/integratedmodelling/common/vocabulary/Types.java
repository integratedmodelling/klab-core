package org.integratedmodelling.common.vocabulary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.classification.Classifier;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.OWL;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.lang.LogicalConnector;

/**
 * Methods to deal with classified types.
 * <p>
 * The following classified types can exist:
 * <p>
 * <ul>
 * Straight classifications: the classified type is a class and it's classified into its
 * subclasses. In data annotations, this produces as many models as traits exposed by the
 * class, with classifiers that extract the trait from the subclasses. Class must be
 * abstract, subtypes may be but that will trigger further resolution.
 * <li>Straight trait classification: classify one trait into its subtraits. This will be
 * represented by observability of that trait and imply the presence of any direct
 * observable it is inherent of.
 * <li>Classification of direct non-trait and resolves to observability (accepting only
 * the deniable observability expressed as the concept itself with/out 'no') without 'by'
 * clause.
 * <li>Classification of direct non-trait 'by' trait. Creates trait contextualized 'of'
 * (direct thing - must be compatible) 'within' context and observes its observability. A
 * further contextualized 'by x of z' should be admitted - say by Temperature Level of
 * Stream' that reverses the inherency and makes is a Level of (Temperature within Stream)
 * within (context).
 * <li>Classification of quality 'by' trait. If quantity by ordering, a 'use
 * discretization' error will ensue unless classify is discretize, in which case it will
 * resolve to a categorized ranking rather than a class. Otherwise
 * <li>Classification of
 * </ul>
 * <p>
 * All classifications contextualize universals as qualities, so they are subject to the
 * same rules of all qualities: they must have an inherent object and if that is not the
 * context of contextualization, a within statement.
 * 
 * @author Ferd
 *
 */
public class Types {

    /**
     * Return a (flat) list of all children up to the passed level of detail, using the
     * model object (stated) hierarchy and keeping the order of declaration (depth- first
     * if more levels are involved). Allows abstract concepts in the result - if only
     * concrete ones are desires, use {@link #getConcreteChildrenAtLevel} instead.
     * 
     * @param baseType
     * @param level
     * @return all children at level
     */
    public static List<IConcept> getChildrenAtLevel(IConcept baseType, int level) {

        List<IConcept> ret = new ArrayList<>();

        INamespace ns = KLAB.MMANAGER.getNamespace(baseType.getConceptSpace());
        if (ns == null) {
            return ret;
        }

        IModelObject mo = ns.getModelObject(baseType.getLocalName());
        if (mo == null) {
            return ret;
        }

        findAtLevel(mo, ret, level, 0, false);

        return ret;
    }

    /**
     * If current is a child of base at passed level, return it; otherwise return the
     * parent at the passed level, or null if the concept is unrelated or higher than
     * level. Uses the model object (stated) hierarchy.
     * 
     * @param base
     * @param current
     * @param level
     * @return parent at level
     */
    public static IConcept getParentAtLevel(IConcept base, IConcept current, int level) {
        IConcept ret = null;
        if (current.is(base)) {
            int l = getDetailLevel(base, current);
            if (l == level) {
                return current;
            }
            if (l > level) {
                for (IConcept c : getChildrenAtLevel(base, level)) {
                    if (current.is(c)) {
                        return c;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Get the level of detail corresponding to the passed key in the DECLARED hierarchy
     * of baseType - i.e. using the model objects declared in k.IM. Key can be the concept
     * fully qualified name or its ID alone, matched case-insensitive. Semantics alone
     * does not suffice: the concepts must be arranged in a declaration hierarchy for the
     * levels to be attributed. Also only works with trait and class types, as this is
     * only relevant to classifications.
     * 
     * @param baseType
     * @param key
     * @return detail level
     */
    public static int getDetailLevel(IKnowledge baseType, String key) {

        /*
         * go through children using the model object hierarchy. Will only find the ones
         * declared in a hierarchy.
         */
        INamespace ns = KLAB.MMANAGER.getNamespace(baseType.getConceptSpace());
        if (ns == null) {
            return -1;
        }

        IModelObject mo = ns.getModelObject(baseType.getLocalName());
        if (mo == null) {
            return -1;
        }

        return findLevel(mo, key, 0);
    }

    /**
     * Return a (flat) list of all CONCRETE children up to the passed level of detail,
     * using the model object (stated) hierarchy and keeping the order of declaration
     * (depth- first if more levels are involved).
     * 
     * @param baseType
     * @param level
     * @return concrete children at level
     */
    public static List<IConcept> getConcreteChildrenAtLevel(IConcept baseType, int level) {

        List<IConcept> ret = new ArrayList<>();

        INamespace ns = KLAB.MMANAGER.getNamespace(baseType.getConceptSpace());
        if (ns == null) {
            return ret;
        }

        IModelObject mo = ns.getModelObject(baseType.getLocalName());
        if (mo == null) {
            return ret;
        }

        findAtLevel(mo, ret, level, 0, true);

        return ret;
    }

    /**
     * Get the level of detail of current in the DECLARED hierarchy of base - i.e. using
     * the model objects declared in k.IM. Only works with trait and class types, as this
     * is only relevant to classifications.
     * 
     * @param base
     * @param current
     * @return detail level of current within base
     */
    public static int getDetailLevel(IConcept base, IConcept current) {
        return getDetailLevel(base, current.toString());
    }

    /**
     * Return the appropriate classification type for a stated 'classify' observer, with
     * optional 'by' clause. Any error throw an exception.
     * 
     * @param original
     * @param by
     * @return
     */
    public static IConcept getClassificationType(IConcept original) throws KlabValidationException {

        String problem = null;
        IConcept by = null; // TODO needs to be extracted from original
                            // observable.

        if (NS.isClass(original)) {
            if (original.isAbstract() || NS.isBaseDeclaration(original)) {
                return original;
            } else {
                problem = "classified type cannot be specific";
            }
        } else if (NS.isTrait(original)) {
            if (by == null) {
                return Observables.makeTypeFor(original);
            } else {
                // TODO
            }
        } else if (NS.isDirect(original)) {
            if (by == null) {
                return Observables.makeTypeFor(Traits.getObservabilityOf(original, true));
            } else {

            }

        } else if (NS.isQuality(original)) {

        }
        throw new KlabValidationException("concept " + original + " can not be classified"
                + (problem == null ? "" : (": " + problem)));
    }

    /**
     * Return the type we should use in a classifier that classifies the first one
     * (returned {@link #getClassificationType(IConcept, IConcept)} into the stated
     * classified concept. If the type is not adequate for that throw a validation
     * exception explaining why.
     * 
     * @param classifier
     * @param classified
     * @return
     */
    public static IConcept getClassifiedType(IConcept classifier, IConcept classified)
            throws KlabValidationException {
        return null;
    }

    private static int findLevel(IModelObject mo, String key, int level) {

        if (mo == null || mo.getName() == null) {
            return -1;
        }

        if (mo.getName().equals(key) || mo.getId().equalsIgnoreCase(key)) {
            return level;
        }

        for (IModelObject o : mo.getChildren()) {
            if (o instanceof IKnowledgeObject) {
                int l = findLevel(o, key, level + 1);
                if (l > 0) {
                    return l;
                }
            }
        }

        return -1;
    }

    private static void findAtLevel(IModelObject mo, List<IConcept> ret, int level, int current, boolean filterAbstract) {

        IConcept k = KLAB.c(mo.getName());
        if (!filterAbstract || !k.isAbstract()) {
            ret.add(k);
        }
        if (level < 0 || level < current) {
            for (IModelObject o : mo.getChildren()) {
                if (o instanceof IKnowledgeObject) {
                    findAtLevel(o, ret, level, current + 1, filterAbstract);
                }
            }
        }
    }

    public static Collection<IConcept> getExposedTraits(IConcept cls) {
        return OWL.getRestrictedClasses(cls, KLAB.p(NS.EXPOSES_TRAIT_PROPERTY));
    }

    public static boolean isDelegate(IConcept concept) {
        return concept.getMetadata().get(NS.IS_TYPE_DELEGATE) != null;
    }

    /**
     * Produces the quality observable that will carry the contextualizable meaning of one
     * or more universals. Concept must exist but it may not be a type yet.
     * 
     * @param exposedTraits
     * @return
     */
    public static void setExposedTraits(IConcept type, Collection<IConcept> exposedTraits) {

        // if (!type.is(KLAB.c(NS.TYPE))) {
        // type.getOntology().define(Collections.singleton(Axiom.SubClass(NS.TYPE,
        // type.getLocalName())));
        // }
        //
        OWL.restrictSome(type, KLAB.p(NS.EXPOSES_TRAIT_PROPERTY), LogicalConnector.UNION, exposedTraits);
    }

    /**
     * True if trait is exposedTrait, or if trait is a valid value for it - e.g. is part
     * of the restricted closure of a "down to" specification.
     * 
     * @param t
     * @param o
     * @return
     */
    public static boolean isExposedAs(IKnowledge trait, IConcept exposedTrait) {
        if (trait.is(exposedTrait)) {
            return true;
        }
        for (IConcept c : Observables.getRestrictedClosure(exposedTrait)) {
            if (trait.is(c)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Create a classification based on the encodings stored as metadata in the concept
     * hierarchy.
     * 
     * @param rootClass
     * @param metadataEncodingProperty
     * @return classification
     * @throws KlabValidationException
     */
    public static IClassification createClassificationFromMetadata(IConcept rootClass, String metadataEncodingProperty)
            throws KlabValidationException {

        Classification ret = new Classification(rootClass);

        for (IKnowledge c : ret.getConceptSpace().getSemanticClosure()) {

            IMetadata m = c.getMetadata();
            Object o = m.get(metadataEncodingProperty);

            if (o != null) {
                ret.addClassifier(new Classifier(o), (IConcept) c);
            }
        }

        return ret;
    }

}
