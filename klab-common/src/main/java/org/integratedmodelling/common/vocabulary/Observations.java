package org.integratedmodelling.common.vocabulary;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.common.beans.DirectObservation;
import org.integratedmodelling.common.beans.Observation;

/**
 * Methods to create, inquire and transform observations.
 * 
 * @author ferdinando.villa
 *
 */
public class Observations {
    
    public static String getKIMDefinition(IDirectObservation obs) {
                
        IConcept concept = obs.getObservable().getType();
        String name = obs.getName();
        
        IShape shape = obs.getScale().getSpace() == null ? null : obs.getScale().getSpace().getShape();
        
        return "observe " + concept + " named " + name + "\n" 
                + (shape == null ? "" :  "   over space (shape=\"EPSG:4326 " + shape + "\")") 
                +";\n";

    }

    /**
     * Create an observer (definition of a finished observation) from a bean built on the
     * fly or retrieved through a URI. The observer can be used to make more observations
     * like this when contextualized.
     * 
     * @param observation
     * @return
     */
    public IDirectObserver createObserver(DirectObservation observation) {
        return null;
    }

    /**
     * Reincarnate the full observation tree from a bean built on the fly or retrieved
     * through a URI.
     * 
     * @param observation
     * @return
     */
    public IObservation createObservation(Observation observation) {
        return null;
    }

    /**
     * Serialize a full observation tree into a bean.
     * 
     * TODO decide how to serialize storage when it does not fit into the bean.
     * 
     * @param observation
     * @return
     */
    public DirectObservation serializeObservation(IDirectObservation observation) {
        return null;
    }

    /**
     * Build an observation bean from the definitions in a direct observer specified in
     * k.IM.
     * 
     * @param observer
     * @return
     */
    public DirectObservation createFromObserver(IDirectObserver observer) {
        return null;
    }

}
