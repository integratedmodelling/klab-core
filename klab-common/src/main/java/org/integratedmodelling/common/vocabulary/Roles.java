package org.integratedmodelling.common.vocabulary;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.OWL;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

public class Roles {
    /**
     * Descriptor for a role. After KLAB-76: roles are scoped and not based on
     * restrictions, to allow their use within scenarios.
     * 
     * @author Ferd
     */
    public static class RoleDescriptor {

        IConcept within;
        IConcept target;
        IConcept targetSubject;
        IConcept role;
        String   scenario;
        String   namespaceId;
        IScale   coverage;

        private String sig() {
            return within + "|" + target + "|" + role + "|" + namespaceId;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof RoleDescriptor
                    && ((RoleDescriptor) o).sig().equals(sig());
        }

        @Override
        public int hashCode() {
            return sig().hashCode();
        };

        @Override
        public String toString() {
            return "concept " + target + " has role " + role + " within " + within +
                    (scenario == null ? "" : (" in scenario " + scenario));
        }

        public Map<?, ?> dump() {
            return MapUtils
                    .of("role", role.toString(), "target", target
                            .toString(), "for", targetSubject == null
                                    ? null
                                    : targetSubject
                                            .toString(), "namespace", namespaceId, "within", within
                                                    .toString());
        }
    }

    // role descriptors indexed by role
    private static MultiMap roles = new MultiValueMap();

    public static void removeRolesFor(INamespace namespace) {

        List<Pair<IConcept, RoleDescriptor>> toRemove = new ArrayList<>();
        for (Object o : roles.values()) {
            RoleDescriptor rd = (RoleDescriptor) o;
            if (rd.namespaceId.equals(namespace.getId())) {
                toRemove.add(new Pair<>(rd.role, rd));
            }
        }

        for (Pair<IConcept, RoleDescriptor> r : toRemove) {
            roles.remove(r.getFirst(), r.getSecond());
        }
    }

    /*
     * State that subject has (role) in (target subject in)? restricted.
     * 
     * @param role
     * 
     * @param subject
     * 
     * @param restricted
     */
    public static void addRole(IConcept role, IConcept subject, /* @Nullable */ IConcept targetSubject, IConcept restricted, INamespace namespace) {

        RoleDescriptor rd = new RoleDescriptor();
        rd.role = role;
        rd.target = subject;
        rd.targetSubject = targetSubject;
        rd.within = restricted;
        // rd.property = restrictingProperty;
        rd.namespaceId = namespace.getId();
        rd.scenario = namespace.isScenario() ? namespace.getId() : null;
        roles.put(role, rd);

    }

    /**
     * Get all dependencies that are implied by the roles attributed to observable in
     * context. Use stated semantics, scenarios and scale.
     * 
     * @param observable
     * @param scope
     * @return roles for observable in scope
     */
    public static Collection<IConcept> getRoles(IObservableSemantics observable, IResolutionScope scope) {

        Map<IConcept, List<RoleDescriptor>> cnd = new HashMap<>();
        List<IConcept> ret = new ArrayList<>();

        if (scope == null) {
            /*
             * no roles in no scope
             */
            return ret;
        }

        /*
         * collect all applicable
         */
        for (Object o : roles.values()) {
            RoleDescriptor rd = (RoleDescriptor) o;
            if (rd.within.is(observable)) {
                if (contextApplies(rd, scope)) {
                    if (!cnd.containsKey(rd.target)) {
                        cnd.put(rd.target, new ArrayList<RoleDescriptor>());
                    }
                    cnd.get(rd.target).add(rd);
                }
            }
        }

        ret.addAll(cnd.keySet());

        return ret;
    }

    /**
     * Get the concrete role implied by the role closure of a context role.
     * 
     * @param baseRole the abstract role incarnated in the context's role closure.
     * @param contextRole the context role, or an observable whose role will be checked.
     * 
     * @return the role that extends the base role passed, or null.
     */
    public static IConcept getRole(IConcept baseRole, IConcept contextRole) {

        List<IConcept> ctx = new ArrayList<>();
        if (!NS.isRole(contextRole) && NS.isObservable(contextRole)) {
            ctx.addAll(getRoles(contextRole));
        } else {
            ctx.add(contextRole);
        }
        return null;
    }

    /*
     * TODO make it smarter. Roles in general should be only considered when defined in
     * the namespace of the model being resolved or the user namespace.
     * 
     * @param rd
     * 
     * @param context
     * 
     * @return
     */
    private static boolean contextApplies(RoleDescriptor rd, IResolutionScope context) {

        if (rd.scenario != null) {
            return context.getScenarios().contains(rd.scenario);
        }
        if (rd.targetSubject != null) {
            return context.getSubject().getObservable().getSemantics()
                    .is(rd.targetSubject);
        }
        if (rd.coverage != null) {
            try {
                return context.getScale().intersects(rd.coverage);
            } catch (KlabException e) {
                return false;
            }
        }

        return true;
    }

    /**
     * Retrieve any roles that were stated with the concept ('with role'). This only
     * applies to observables declared in models, as there is no other legal place to
     * declare observables with roles.
     * 
     * @param concept
     * @return
     */
    public static Collection<IConcept> getRoles(ISemantic k) {

        IKnowledge concept = k.getType();

        Set<IConcept> ret = new HashSet<>();

        if (concept instanceof IConcept) {
            ret.addAll(OWL.getRestrictedClasses((IConcept) concept, KLAB
                    .p(NS.HAS_ROLE_PROPERTY)));
        }
        return ret;
    }

    public static boolean hasRole(ISemantic k, IConcept role) {

        IKnowledge type = k.getType();

        if (!(type instanceof IConcept)) {
            return false;
        }

        for (IConcept c : getRoles(type)) {
            if (c.is(role)) {
                return true;
            }
        }

        return false;
    }

    /**
     * True if k has a role R so that the passed role is-a R.
     * 
     * @param k
     * @param role
     * @return
     */
    public static boolean hasParentRole(ISemantic k, IConcept role) {

        IKnowledge type = k.getType();

        if (!(type instanceof IConcept)) {
            return false;
        }

        for (IConcept c : getRoles(type)) {
            if (role.is(c)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Return the role that is baseRole and is implied by the context observable, either
     * directly or through its implication closure.
     * 
     * @param baseRole
     * @param contextObservable
     * @return
     */
    public static IConcept getImpliedRole(IConcept baseRole, IConcept contextObservable) {
        Collection<IConcept> roles = NS.isRole(contextObservable) ? Collections.singleton(contextObservable)
                : getRoles(contextObservable);
        for (IConcept role : roles) {
            if (!role.isAbstract() && role.is(baseRole)) {
                return role;
            }
            for (IConcept irole : getImpliedObservableRoles(role, true)) {
                IConcept ret = getImpliedRole(baseRole, irole);
                if (ret != null) {
                    return ret;
                }
            }
        }
        return null;
    }

    /**
     * Get all other roles implied by this one. These must be concrete when the role is
     * used in the main observable for a model, which must produce or use them.
     * 
     * @param role
     * @return
     */
    public static Collection<IConcept> getImpliedObservableRoles(IConcept role) {
        return getImpliedObservableRoles(role, false);
    }

    /**
     * Get all other roles implied by this one. These must be concrete when the role is
     * used in the main observable for a model, which must produce or use them. Optionally
     * include the source and destination endpoints for all roles that apply to a
     * relationship.
     * 
     * @param role
     * @param includeRelationshipEndpoints if true, roles that apply to relationships will
     * add the specialized source and destination types.
     * @return
     */
    public static Collection<IConcept> getImpliedObservableRoles(IConcept role, boolean includeRelationshipEndpoints) {

        Set<IConcept> ret = new HashSet<>();
        if (includeRelationshipEndpoints
                && NS.contains(KLAB.c(NS.CORE_RELATIONSHIP), Observables.getApplicableObservables(role))) {

            IConcept source = Observables.getApplicableSource(role);
            IConcept destination = Observables.getApplicableDestination(role);

            if (source != null) {
                ret.add(source);
            }
            if (destination != null) {
                ret.add(destination);
            }
        }

        ret.addAll(OWL.getRestrictedClasses(role, KLAB.p(NS.IMPLIES_ROLE_PROPERTY), true));

        return ret;
    }

    /**
     * Pass a role and a context; return the generic observable that is expected to have
     * that role in that context so that we can query the semantic web for models.
     * 
     * @param concept
     * @param contextType
     * @return
     * @throws KlabException
     */
    public static IConcept getObservableWithRole(IConcept concept, IConcept contextType)
            throws KlabException {

        IConcept ret = null;

        Collection<IConcept> applicable = Observables
                .getApplicableObservables(concept);

        if (!applicable.isEmpty()) {

            IConcept appl = NS.getLeastGeneralConcept(applicable);
            if (appl == null) {
                KLAB.warn("using only the first applicable observable for observation of role "
                        + concept);
            }
            ret = Observables.declareObservable(appl, null, contextType, null, Collections
                    .singleton(concept), null, null, KLAB.REASONER
                            .getOntology());
        } else {
            throw new KlabValidationException("cannot observe a generic role: the role must apply to a specific observable type");
        }

        return ret;
    }

    /**
     * Return the role
     * @param observable
     * @param context
     * @return
     */
    public static Collection<IConcept> getRoleFor(IConcept observable, IConcept context) {
        Set<IConcept> ret = new HashSet<>();
        for (IConcept c : getRoles(context)) {
            for (IConcept r : getImpliedObservableRoles(c)) {
                for (IConcept applicable : Observables.getApplicableObservables(r)) {
                    if (applicable != null && !applicable.isAbstract() && observable.is(applicable)) {
                        ret.add(r);
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Ensure the passed observable contains all the roles implied for it within the
     * passed observation.
     * 
     * @param observable
     * @param roleContext
     * @return
     */
    public static IConcept assignRoles(IConcept observable, IObservation roleContext) {

        Collection<IConcept> roles = getRoleFor(observable, roleContext.getObservable().getSemantics()
                .getType());
        Collection<IConcept> existingRoles = getRoles(observable);

        if (existingRoles != null) {
            for (IConcept role : roles) {
                if (NS.contains(role, existingRoles)) {
                    roles.remove(role);
                }
            }
        }

        if (!roles.isEmpty()) {
            try {
                observable = Observables
                        .declareObservable(observable, null, null, null, roles, null, null, KLAB.REASONER
                                .getOntology());
            } catch (KlabValidationException e) {
                throw new KlabRuntimeException(e);
            }
        }
        return observable;
    }

}
