/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.vocabulary.authority;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.authority.Authority;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.lang.Axiom;
import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * Provides the basic concept generation and validation functions, based on the
 * description bean contents. Works both at the client and server side.
 * 
 * @author Ferd
 *
 */
public abstract class BaseAuthority implements IAuthority<AuthorityConcept>, NetworkSerializable {

    protected IOntology                   ontology;
    protected Authority                   definition;
    Map<String, IConcept>                 coreConcepts = new HashMap<>();

    // change to force cache rebuild.
    private static final String           CACHE_ID     = "v9104";

    /*
     * persistent cache
     */
    private static DB                     db;
    private Map<String, AuthorityConcept> descriptors;

    protected BaseAuthority(Authority definition) {

        this.definition = definition;

        if (KLAB.getWorldview() != null && definition.getWorldview() != null
                && !KLAB.getWorldview().equals(definition.getWorldview())) {
            // this should not happen as we should just ignore the authority, but
            // we check to avoid misuse
            throw new KlabRuntimeException("authority " + definition.getName()
                    + " is incompatible with current worldview " + KLAB.getWorldview());
        }

        for (String p : definition.getInitialProperties()) {
            Knowledge.createProperty(p, getOntology());
        }

        for (String c : definition.getInitialConcepts()) {
            Knowledge.createConcept(c, getOntology());
        }

        for (String c : definition.getCoreConcepts().keySet()) {
            String cconcept = definition.getCoreConcepts().get(c);
            if (!cconcept.contains(":")) {
                cconcept = getOntology().getConceptSpace() + ":" + cconcept;
            }
            IConcept concept = KLAB.KM.getConcept(cconcept);
            if (concept != null) {
                coreConcepts.put(c, concept);
            } else {
                throw new KlabRuntimeException("authority " + definition.getName()
                        + " requires unknown concept " + cconcept);
            }
        }

        /**
         * Dress up the core concepts
         */
        List<IAxiom> axioms = new ArrayList<>();
        for (IConcept c : coreConcepts.values()) {

            if (!c.getConceptSpace().equals(getOntology().getConceptSpace())) {
                continue;
            }

            axioms.add(Axiom.AnnotationAssertion(c.getLocalName(), NS.BASE_DECLARATION, "true"));
            axioms.add(Axiom
                    .AnnotationAssertion(c.getLocalName(), NS.AUTHORITY_ID_PROPERTY, getAuthorityId()));

            String pName = null;
            String pProp = null;

            if (c.is(KLAB.c(NS.CORE_IDENTITY_TRAIT))) {
                pProp = NS.HAS_IDENTITY_PROPERTY;
                pName = "is" + c.getLocalName();
            } else if (c.is(KLAB.c(NS.CORE_REALM_TRAIT))) {
                pProp = NS.HAS_REALM_PROPERTY;
                pName = "in" + c.getLocalName();
            } else {
                // just warn for now.
                KLAB.error("authority " + getAuthorityId()
                        + " provides unsupported core concepts (not identities or realms)");
            }

            if (pName != null) {
                axioms.add(Axiom.ObjectPropertyAssertion(pName));
                axioms.add(Axiom.SubObjectProperty(pProp, pName));
                axioms.add(Axiom.AnnotationAssertion(c
                        .getLocalName(), NS.TRAIT_RESTRICTING_PROPERTY, getOntology().getConceptSpace()
                                + ":" + pName));
            }
        }

        getOntology().define(axioms);

        if (db == null) {

            db = DBMaker
                    .newFileDB(new File(KLAB.CONFIG.getDataPath("authorities") + File.separator
                            + "authcache" + CACHE_ID))
                    // FIXME this is necessary because of frequent corruption that does
                    // not happen with other uses of file-based (not using Serializable
                    // beans).
                    // FIXME try again with newer MapDB versions and/or switch to custom
                    // serialization of AuthorityConcept.
                    // .newMemoryDB()
                    .closeOnJvmShutdown()
                    .make();
        }

        try {
            descriptors = db.getTreeMap(this.definition.getName() + "_cache");
        } catch (Throwable e) {
            throw new KlabRuntimeException("error instantiating authority cache: please remove directory "
                    + KLAB.CONFIG.getDataPath("authorities") + " and restart");
        }
    }

    @Override
    public String getAuthorityId() {
        return definition.getName();
    }

    @Override
    public boolean canSearch() {
        return definition.isSearchable();
    }

    @Override
    public String getDescription(String id) {

        for (int i = 0; i < definition.getAuthorities().size(); i++) {
            if (definition.getAuthorities().get(i).equals(id)) {
                return definition.getAuthorityDescriptions().get(i);
            }
        }
        return null;
    }

    @Override
    public IConcept getIdentity(String id, String authorityId) throws KlabValidationException {

        IConcept ret = null;

        AuthorityConcept desc = descriptors.get(authorityId + "." + id);
        if (desc != null) {

            if (desc.getError() != null) {
                throw new KlabValidationException(desc.getError());
            }

            ret = ontology.getConcept(desc.getId());
            if (ret == null) {

                // may not be there because we persist descriptors
                for (String def : desc.getConceptDefinition()) {
                    Knowledge.createConcept(def, ontology);
                }
                ret = ontology.getConcept(desc.getId());
                
                List<IAxiom> axioms = new ArrayList<>();
                axioms.add(Axiom
                        .AnnotationAssertion(ret
                                .getLocalName(), NS.AUTHORITY_ID_PROPERTY, getAuthorityId()));

                axioms.add(Axiom
                        .AnnotationAssertion(ret
                                .getLocalName(), NS.CONCEPT_DEFINITION_PROPERTY, "authority "
                                        + authorityId + " \""
                                        + desc.getDefinition() + "\""));

                // FIXME can't do this as weird labels get used to build concept names
                // when
                // composing observables. This should be fixed - display names should be
                // arbitrary

                // if (desc.getLabel() != null) {
                // axioms.add(Axiom
                // .AnnotationAssertion(ret
                // .getLocalName(), NS.DISPLAY_LABEL_PROPERTY, desc.getLabel()));
                // }
                ontology.define(axioms);
            }
            return ret;
        }

        desc = getConcept(authorityId, id);

        if (desc != null) {
            
            for (String def : desc.getConceptDefinition()) {
                Knowledge.createConcept(def, ontology);
            }
            
            ret = ontology.getConcept(desc.getId());
            List<IAxiom> axioms = new ArrayList<>();
            axioms.add(Axiom
                    .AnnotationAssertion(ret
                            .getLocalName(), NS.AUTHORITY_ID_PROPERTY, getAuthorityId()));

            axioms.add(Axiom
                    .AnnotationAssertion(ret
                            .getLocalName(), NS.CONCEPT_DEFINITION_PROPERTY, "authority "
                                    + authorityId + " \""
                                    + desc.getDefinition() + "\""));
            
            // FIXME can't do this as weird labels get used to build concept names when
            // composing observables. This should be fixed - display names should be
            // arbitrary

            // if (desc.getLabel() != null) {
            // axioms.add(Axiom
            // .AnnotationAssertion(ret
            // .getLocalName(), NS.DISPLAY_LABEL_PROPERTY, desc.getLabel()));
            // }
            ontology.define(axioms);

            /*
             * don't think this should be necessary, but the cache gets corrupted a lot.
             */
            synchronized (db) {
                descriptors.put(authorityId + "." + id, desc);
                db.commit();
            }
        }

        return ret;
    }

    @Override
    public String getDescription() {
        return definition.getOverallDescription();
    }

    @Override
    public List<String> getAuthorityIds() {
        return definition.getAuthorities();
    }

    protected IOntology getOntology() {
        if (ontology == null) {
            ontology = KLAB.KM.requireOntology(definition.getOntologyId());
            ((Ontology) ontology).setInternal(true);
            if (KLAB.MMANAGER.getNamespace(definition.getOntologyId()) == null) {
                ((KIMModelManager) KLAB.MMANAGER).registerNamespace(ontology, definition.getOntologyId());
            }
        }
        return ontology;
    }

    public abstract AuthorityConcept getConcept(String authority, String id) throws KlabValidationException;

    @Override
    public String validateIdentifiedConcept(IConcept knowledge, String id) {

        IConcept core = coreConcepts.get(id);
        if (core != null && !knowledge.is(core)) {
            return "Concepts identified by the "
                    + getAuthorityId() +
                    " authority must be " + core;
        }

        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!(desiredClass.isAssignableFrom(Authority.class))) {
            throw new KlabRuntimeException("cannot serialize an Authority to a "
                    + desiredClass.getCanonicalName());
        }

        return (T) definition;
    }

    public void setBaseIdentity(IConcept knowledge, String definedAuthority) {
        coreConcepts.put(definedAuthority, knowledge);
    }

    public IConcept getBaseIdentity(String definedAuthority) {
        return coreConcepts.get(definedAuthority);
    }
}
