/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/

package org.integratedmodelling.common.vocabulary;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.beans.ObservationData;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMDirectObserver;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.owl.Knowledge;

/**
 * Simple holder of the crucial metadata to instantiate a remote observation,
 * returned by services so we don't need to instantiate the results after query.
 * Not only a matter of efficiency, as the storing engine may have lost concepts
 * that are known to the client so we don't want to instantiate observables at
 * the engine side.
 * 
 * @author Ferd TODO use Observation bean and remove the duplication with
 *  ObservationData
 */
public class ObservationMetadata implements IObservationMetadata {

	public String name;
	public String id;
	public String namespaceId;
	public String serverId;
	public String observableName;
	public String description;
	public String geometryWKB;
	public Long start;
	public Long end;
	public Long step;
	public Map<String, Object> attributes = new HashMap<>();

	public ObservationMetadata() {
	}

	public ObservationMetadata(ObservationData obs) {
		this.name = obs.getName();
		this.namespaceId = obs.getNamespaceId();
		this.id = obs.getId();
		this.observableName = obs.getObservableName();
		this.description = obs.description;
		this.geometryWKB = obs.geometryWKB;
		this.start = obs.getStart();
		this.end = obs.getEnd();
		this.step = obs.getEnd();
		this.attributes.putAll(obs.getAttributes());
	}

	@Override
	public boolean equals(Object obj) {
		return obj instanceof ObservationMetadata && name.equals(((ObservationMetadata) obj).name);
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	public String getNamespaceId() {
		return namespaceId;
	}

	public void setNamespaceId(String namespaceId) {
		this.namespaceId = namespaceId;
	}

	public String getServerId() {
		return serverId;
	}

	public void setServerId(String serverId) {
		this.serverId = serverId;
	}

	public String getObservableName() {
		return observableName;
	}

	public void setObservableName(String observableName) {
		this.observableName = observableName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGeometryWKB() {
		return geometryWKB;
	}

	public void setGeometryWKB(String geometryWKB) {
		this.geometryWKB = geometryWKB;
	}

	public Long getStart() {
		return start;
	}

	public void setStart(Long start) {
		this.start = start;
	}

	public Long getEnd() {
		return end;
	}

	public void setEnd(Long end) {
		this.end = end;
	}

	public Long getStep() {
		return step;
	}

	public void setStep(Long step) {
		this.step = step;
	}

	public Map<String, Object> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "[OM " + observableName + " " + name + " from " + serverId + "]";
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getNodeId() {
		return serverId;
	}

	@Override
	public IConcept getConcept() {
		return (IConcept) Knowledge.parse(observableName);
	}

	/**
	 * Create a subject observer from data, assuming the concept exists and
	 * everything is legitimate.
	 * 
	 * @return an observer for this observation
	 */
	@Override
	public IDirectObserver getSubjectObserver(IMonitor monitor) {

		IConcept observable = (IConcept) Knowledge.parse(observableName);
		if (observable == null) {
			return null;
		}

		//
		INamespace namespace = KLAB.MMANAGER.getNamespace(namespaceId);
		if (namespace == null) {
			namespace = KLAB.MMANAGER.getLocalNamespace();
		}
		ITemporalExtent time = null;
		if (getN(start) != 0 || getN(end) != 0) {
			time = new Time(getN(start), getN(end), getN(step));
		}

		IDirectObserver ret = new KIMDirectObserver(namespace, observable, id,
				KLAB.MFACTORY.createScale(monitor, geometryWKB, time));

		return ret;

	}

	private long getN(Long n) {
		return n == null ? 0 : n;
	}
}
