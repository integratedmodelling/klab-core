/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.indexing;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.exceptions.KlabException;

public class OntologyIndexer implements KnowledgeIndex.IndexAdapter {

    IOntology _ontology;

    class DocumentIterator implements Iterator<Document> {

        Iterator<IConcept> _it;

        DocumentIterator() {
            _it = _ontology == null ? new ArrayList<IConcept>().iterator()
                    : _ontology.getConcepts().iterator();
        }

        @Override
        public boolean hasNext() {
            return _it.hasNext();
        }

        @Override
        public Document next() {
            return getDocument(_it.next());
        }

        @Override
        public void remove() {
        }

    }

    @Override
    public Iterator<Document> iterator() {
        return new DocumentIterator();
    }

    @Override
    public void initialize(Object object) throws KlabException {
        Ontology ontology = (Ontology) object;
        if (ontology.getResourceUrl() != null) {
            try {
                URL url = new URL(ontology.getResourceUrl());
                File f = new File(url.getFile());
                _ontology = ontology;
            } catch (MalformedURLException e) {
            }
        }
    }

    private Document getDocument(IConcept c) {

        Document ret = new Document();

        ret.add(new Field("id", c.toString(), Field.Store.YES, Field.Index.NO));
        ret.add(new Field("type", "C", Field.Store.YES, Field.Index.NO));
        ret.add(new Field("name", c.getLocalName(), Field.Store.YES,
                Field.Index.ANALYZED));
        ret.add(new Field("namespace", c.getConceptSpace(), Field.Store.YES,
                Field.Index.ANALYZED));

        IMetadata md = c.getMetadata();

        String comment = md.getString(IMetadata.DC_COMMENT);
        String label = md.getString(IMetadata.DC_LABEL);

        if (comment != null) {
            ret.add(new Field("description", comment, Field.Store.YES,
                    Field.Index.ANALYZED));
        }
        if (label != null) {
            ret.add(new Field("label", label, Field.Store.YES,
                    Field.Index.ANALYZED));
        }

        return ret;
    }

    @Override
    public String getUniversalQuery() {
        if (_ontology == null)
            return null;
        return "namespace:" + _ontology.getConceptSpace();
    }

}
