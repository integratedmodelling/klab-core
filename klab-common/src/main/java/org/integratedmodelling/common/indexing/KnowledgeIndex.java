/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.indexing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Fieldable;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopScoreDocCollector;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.factories.IProjectManager;
import org.integratedmodelling.api.knowledge.IKnowledgeIndex;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.OWLManager;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.owl.OWL;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Simplest possible index wrapper for knowledge of various kinds - ontologies,
 * model objects, concepts, semantic objects and their containers (projects,
 * kboxes)
 * 
 * NOTE: find version for Lucene 4.6 in incubator. Messes Neo4j up and I have no
 * time to upgrade the whole monster.
 * 
 * @author Ferd
 *
 */
public class KnowledgeIndex implements IKnowledgeIndex {

    String            _id;
    IndexWriter       _index;
    // FileCache _cache;
    ArrayList<Object> _toIndex  = new ArrayList<>();
    Analyzer          _analyzer;
    boolean           isFuzzy   = false;
    Listener          _listener = null;
    File              _dir;
    boolean           _isNew    = false;

    /**
     * Use one of these to react to events. NOTE: if a listener is installed,
     * exceptions are not thrown.
     * 
     * @author Ferd
     *
     */
    public static interface Listener {

        void writeStarted(KnowledgeIndex knowledgeIndex, int ndocs);

        void indexReady(KnowledgeIndex knowledgeIndex);

        void onError(KnowledgeIndex knowledgeIndex, Throwable t);
    }

    public static interface IndexAdapter extends Iterable<Document> {

        /*
         * return the query that will univocally select all the documents in the
         * object handled. Used to delete objects at reindex.
         */
        public String getUniversalQuery();

        public void initialize(Object object) throws KlabException;
    }

    /**
     * Install a listener. This will prevent exceptions to be thrown even for
     * I/O errors - they will be reported to the listener instead.
     * 
     * @param listener
     */
    public void addListener(Listener listener) {
        _listener = listener;
    }

    /**
     * Return true if this index is new, i.e., has been created from scratch and
     * is empty.
     * 
     * @return whether the index is new
     */
    public boolean isNew() {
        return _isNew;
    }

    public class IndexResult extends ArrayList<IMetadata> {

        private static final long serialVersionUID = -4111882848704689269L;
        private ScoreDoc[]        _collector;

        public IndexResult(ScoreDoc[] hits, IndexSearcher searcher) {
            _collector = hits;
            for (int i = 0; i < _collector.length; i++) {
                add(toMetadata(searcher, i));
            }
        }

        private IMetadata toMetadata(IndexSearcher searcher, int i) {

            // make this result printable
            Metadata ret = new Metadata() {
                @Override
                public String toString() {
                    return "["
                            +
                            this.get("type")
                            +
                            "] "
                            +
                            this.get("id")
                            + " ("
                            + (this._data.containsKey(IMetadata.DC_COMMENT) ? this
                                    .get(IMetadata.DC_COMMENT)
                                    : "no description")
                            + ")";
                }
            };
            Document zio;
            try {
                zio = searcher.doc(_collector[i].doc);
            } catch (Exception e) {
                throw new KlabRuntimeException(e);
            }

            for (Fieldable f : zio.getFields()) {
                ret.put(f.name(), f.stringValue());
            }

            return ret;
        }
    }

    /**
     * Merely creating the object will connect it to its Lucene index, creating
     * it if necessary.
     * 
     * @param id
     * @throws KlabException
     */
    public KnowledgeIndex(String id) throws KlabException {
        _id = id;
        _analyzer = new StandardAnalyzer(Version.LUCENE_35);
        _dir = KLAB.CONFIG.getScratchArea(IConfiguration.SUBSPACE_INDEX
                + File.separator + _id);

        initialize();
    }

    @Override
    public void clear() {

        if (_listener != null) {
            _listener.writeStarted(this, -1);
        }
        try {
            _index.deleteAll();
            _index.commit();
            _toIndex.clear();

        } catch (Throwable e) {
            KlabIOException t = new KlabIOException(e);
            if (_listener != null) {
                _listener.onError(this, t);
            }
            throw new KlabRuntimeException(e);
        }
    }

    @Override
    public void reindex() {

        if (_listener != null) {
            _listener.writeStarted(this, _toIndex.size());
        }

        try {

            for (Object o : _toIndex) {
                IndexAdapter adapter = getAdapter(o);

                if (adapter != null) {

                    adapter.initialize(o);

                    /*
                     * clear previous knowledge from indexer that refers to this
                     * object.
                     */
                    String qq = adapter.getUniversalQuery();
                    if (qq != null) {
                        Query q = new QueryParser(Version.LUCENE_35, "name", _analyzer).parse(qq);
                        _index.deleteDocuments(q);
                    }
                    for (Document d : adapter) {
                        if (d != null) {
                            _index.addDocument(d);
                        }
                    }
                }
            }

            _toIndex.clear();
            _index.commit();

        } catch (Exception e) {
            KlabIOException t = new KlabIOException(e);
            if (_listener != null) {
                _listener.onError(this, t);
            }
//            throw new KlabRuntimeException(t);
        }

        if (_listener != null) {
            _listener.indexReady(this);
        }

    }

    private IndexAdapter getAdapter(Object o) {
        if (o instanceof IOntology) {
            return new OntologyIndexer();
        } else if (o instanceof INamespace) {
            return new NamespaceIndexer();
        } else if (o instanceof IDirectObserver) {
            return new ObservationIndexer();
        }
        return null;
    }

    private void initialize() throws KlabException {

        try {
            SimpleFSDirectory fs = new SimpleFSDirectory(_dir);
            if (!IndexReader.indexExists(fs)) {
                _isNew = true;
            }
            IndexWriterConfig conf = new IndexWriterConfig(Version.LUCENE_35, _analyzer);
            conf.setOpenMode(OpenMode.CREATE_OR_APPEND);
            _index = new IndexWriter(FSDirectory.open(_dir), conf);
        } catch (Exception e) {
            KlabIOException t = new KlabIOException(e);
            if (_listener != null) {
                _listener.onError(this, t);
            } // else {
            throw t;
            // }
        }

        if (_listener != null) {
            _listener.indexReady(this);
        }
    }

    @Override
    protected void finalize() throws Throwable {

        if (_index != null) {
            try {
                _index.close();
                _index = null;
            } catch (IOException e1) {
                throw new KlabIOException(e1);
            }
        }

        super.finalize();
    }

    @Override
    public void index(Object object) {

        if (object instanceof OWLManager) {
            OWL owl = ((OWLManager) object).getOWLManager();
            for (IOntology o : owl.getOntologies(false)) {
                _toIndex.add(o);
            }
        } else if (object instanceof IProjectManager) {

            for (IProject project : ((IProjectManager) object).getProjects()) {
                for (INamespace ns : project.getNamespaces()) {
                    _toIndex.add(ns);
                    _toIndex.add(ns.getOntology());
                }
            }
        } else if (object instanceof IProject) {
            try {
                KLAB.PMANAGER.loadProject(((IProject) object).getId(), null);
                for (INamespace ns : ((IProject) object).getNamespaces()) {
                    _toIndex.add(ns);
                    // _toIndex.add(ns.getOntology());
                }
            } catch (KlabException e) {
                // just ignore
            }
        } else if (object instanceof INamespace || object instanceof IOntology
                || object instanceof IDirectObserver) {
            _toIndex.add(object);
        }
    }

    @Override
    public Collection<IMetadata> search(String query) throws KlabException {

        if (_index == null)
            return new ArrayList<>();

        if (!(query.endsWith(" ") || query.endsWith("*") || query.endsWith("~")))
            query += isFuzzy ? "~" : "*";

        Collection<IMetadata> ret = null;

        try {
            // TODO use this
            // MultiFieldQueryParser parser = new
            // MultiFieldQueryParser(searchFields, analyzer);

            IndexReader reader = IndexReader.open(_index, true);
            IndexSearcher searcher = new IndexSearcher(reader);
            QueryParser parser = new QueryParser(Version.LUCENE_35, "name", _analyzer);
            parser.setAllowLeadingWildcard(true);
            Query q = parser.parse(query);
            TopScoreDocCollector collector = TopScoreDocCollector.create(200, true);
            searcher.search(q, collector);
            ScoreDoc[] hits = collector.topDocs().scoreDocs;
            ret = new IndexResult(hits, searcher);
            reader.close();

        } catch (Exception e) {
            KlabIOException t = new KlabIOException(e);
            if (_listener != null) {
                _listener.onError(this, t);
            } // else {
            throw t;
            // }
        }

        return ret;
    }

    /**
     * Retrieve the object described by the metadata record passed, using the
     * proper adapter to locate or create it.
     * 
     * @param record
     * @return the retrieved object
     */
    public Object retrieveObject(IMetadata record) {
        String t = record.getString("type");
        if (t.equals("C")) {
            return KLAB.KM.getConcept(record.getString("id"));
        } else {
            /* model object */
            String ns = Path.getLeading(record.getString("id"), '.');
            String id = Path.getLast(record.getString("id"), '.');
            INamespace namespace = KLAB.MMANAGER.getNamespace(ns);
            if (namespace != null)
                return namespace.getModelObject(id);
        }

        // etc

        return null;
    }

    @Override
    public void deleteNamespace(String namespaceId) {
        String query = "namespace:" + namespaceId;
        try {
            Query q = new QueryParser(Version.LUCENE_35, "name", _analyzer).parse(query);
            _index.deleteDocuments(q);
        } catch (Exception e) {
            throw new KlabRuntimeException(e);
        }
    }

}
