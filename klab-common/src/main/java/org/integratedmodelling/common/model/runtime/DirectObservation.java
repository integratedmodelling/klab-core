package org.integratedmodelling.common.model.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.common.beans.ModelArtifact;
import org.integratedmodelling.common.knowledge.Observation;

public class DirectObservation extends Observation implements IDirectObservation {

    protected org.integratedmodelling.common.beans.DirectObservation bean;
    List<IState>                                           states         = new ArrayList<>();
    private List<String>                                   modelOutputs   = new ArrayList<>();
    private List<String>                                   datasetOutputs = new ArrayList<>();
    protected List<IProcess> processes = new ArrayList<>();
    protected List<IEvent> events = new ArrayList<>();
    
    @Override
    public INamespace getNamespace() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IState> getStates() {
        return states;
    }

    public void addArtifact(ModelArtifact artifact) {
        if (artifact.getArtifactType().equals("model")) {
            if (!modelOutputs.contains(artifact.getArtifactName())) {
                modelOutputs.add(artifact.getArtifactName());
            }
        } else {
            if (!datasetOutputs.contains(artifact.getArtifactName())) {
                datasetOutputs.add(artifact.getArtifactName());
            }
        }
    }

    public void deserialize(org.integratedmodelling.common.beans.DirectObservation bean) {
        super.deserialize(bean);
        this.bean = bean;
        modelOutputs.addAll(bean.getModelOutputs());
        datasetOutputs.addAll(bean.getDatasetOutputs());
    }

    @Override
    public IObservation find(String id) {
        if (this.id.equals(id)) {
            return this;
        }
        IObservation ret = null;
        for (IState sub : getStates()) {
            ret = ((Observation) sub).find(id);
            if (ret != null) {
                return ret;
            }
        }
        return ret;
    }

    public List<String> getModelOutputs() {
        return modelOutputs;
    }

    public List<String> getDatasetOutputs() {
        return datasetOutputs;
    }

    @Override
    public String getName() {
        return bean.getName();
    }

    @Override
    public void append(IObservation observation) {
        if (observation instanceof IState) {
            states.add((IState) observation);
        }
    }

    @Override
    public Collection<IConcept> getRolesFor(ISemantic observable) {
        Set<IConcept> ret = new HashSet<>();
        if (observable instanceof Observation) {
            ret.addAll(((Observation) observable).getExplicitRoles());
        }
        // TODO add those controlled by the semantics of the models - should be
        // communicated back by the engine.
        return ret;
    }

}
