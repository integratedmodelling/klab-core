package org.integratedmodelling.common.model.runtime;

import java.util.Map;

import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Simplest possible mediator. Just redefine {@link #mediate(Object)} based on the
 * observer passed to
 * {@link IActiveObserver#getMediator(org.integratedmodelling.api.modelling.IObserver, org.integratedmodelling.api.monitoring.IMonitor), org.integratedmodelling.api.monitoring.IMonitor)}
 * .
 * 
 * @author ferdinando.villa
 *
 */
public abstract class AbstractMediator extends AbstractStateContextualizer {

    public abstract Object mediate(Object value) throws KlabException;

    protected AbstractMediator(IMonitor monitor) {
        super(monitor);
    }
    
    @Override
    public Map<String, Object> initialize(int index, Map<String, Object> inputs) throws KlabException {
        return MapUtils.ofWithNull(getStateName(), mediate(inputs.get(getStateName())));
    }

    @Override
    public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
            throws KlabException {
        return MapUtils.ofWithNull(getStateName(), mediate(inputs.get(getStateName())));
    }

    @Override
    public boolean isProbabilistic() {
        // TODO Auto-generated method stub
        return false;
    }

}
