/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAction.Trigger;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.provenance.IProvenance.Artifact;
import org.integratedmodelling.base.HashableObject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.common.kim.KIMModel;
import org.integratedmodelling.common.provenance.ProvenanceNode;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabValidationException;

/*
 * 
 */
public abstract class Actuator extends HashableObject implements IActuator {

    private List<IAction>          actions                    = new ArrayList<>();
    protected List<IAction>        initializationActions      = new ArrayList<>();
    private Map<IConcept, IAction> eventActions               = new HashMap<>();
    private List<IAction>          temporalActions            = new ArrayList<>();
    private List<IAction>          terminationActions         = new ArrayList<>();
    protected List<IAction>        stateInitializationActions = new ArrayList<>();

    protected String               name;
    protected IMonitor             monitor;
    protected IDirectObservation   context;
    protected IObservation         observation;
    protected IModel               model;
    protected IProvenance.Artifact provenance;

    boolean                        isReinterpreter;
    IObservableSemantics           uninterpretedSemantics;
    IConcept                       interpretRole;

    // metadata from context that we want to make accessible to the actions.
    protected Map<String, Object>  contextMetadata;

    public Actuator(IObservation observation, IModel model) {
        this.observation = observation;
        this.model = model;

        if (model.isReinterpreter()) {
            this.isReinterpreter = true;
            this.uninterpretedSemantics = ((KIMModel) model).getUninterpretedObservable();
            this.interpretRole = ((KIMModel) model).getAddedRole();
        }

        /*
         * harvest referenceable metadata from the model's documentation
         */
        if (model.getDocumentation() != null) {
            observation.getContext().getReport()
                    .loadDocumentation((model.getDocumentation()));
        }
    }
    
    @Override
    public IModel getModel() {
        return model;
    }

    /**
     * This will substitute the call to the contextualizer if the actuator is for a
     * reinterpreting model.
     * 
     * @return
     * @throws KlabValidationException
     */
    protected Map<String, IObservation> reinterpretRoles(IActiveDirectObservation context)
            throws KlabValidationException {

        Map<String, IObservation> ret = new HashMap<>();
        if (!isReinterpreter) {
            // should never happen
            return ret;
        }

        /*
         * Create the reinterpreted type. This assumes user has done things right (i.e.
         * the role is not already there) - the condition should be checked in the parser,
         * not here.
         */
        IConcept reinterpretedType = Observables
                .declareObservable(uninterpretedSemantics.getType(), null, null, null, Collections
                        .singleton(interpretRole), null, null, KLAB.REASONER.getOntology(), 0);

        /*
         * find all the observations with the reinterpreted semantics
         */
        for (IObservation obs : context.select(uninterpretedSemantics.getType())) {
            ((ObservableSemantics) obs.getObservable().getSemantics()).setType(reinterpretedType);
            ret.put(getObservationName(obs), obs);
        }

        return ret;
    }

    private String getObservationName(IObservation obs) {
        String ret = null;
        if (obs instanceof IState) {
            if (((IActiveDirectObservation) context).getModel() != null) {
                return ((IActiveDirectObservation) context).getModel()
                        .getNameFor(observation.getObservable().getSemantics());
            }
            ret = observation.getObservable().getSemantics().getFormalName();
        }
        if (ret == null) {
            ret = ((IDirectObservation) obs).getName();
        }
        return ret;
    }

    public IObservation getObservation() {
        return this.observation;
    }

    public void addAction(IAction a) {

        /*
         * clone and compile action
         */
        IAction action = ((KIMAction) a).compileFor(observation.getScale(), model);

        /*
         * set target
         */
        if (action.getTargetStateId() == null) {
            ((KIMAction) action).setTargetStateId(getName());
        }

        /*
         * put action away where it belongs.
         */
        if (action.getTrigger() == Trigger.TRANSITION) {
            temporalActions.add(action);
        } else if (action.getTrigger() == Trigger.EVENT) {
            for (IConcept event : ((KIMAction) action).getTriggeringEvents()) {
                eventActions.put(event, action);
            }
        } else if (action.getTrigger() == Trigger.TERMINATION) {
            terminationActions.add(action);
        } else if (action.getTrigger() == Trigger.STATE_INITIALIZATION) {
            stateInitializationActions.add(action);
        } else {
            initializationActions.add(action);
        }

        /*
         * let's exagerate and also keep a list of all actions in order.
         */
        actions.add(a);

    }

    public void setContext(IDirectObservation context) {
        this.context = context;
    }

    public boolean hasTemporalActions() {
        return temporalActions.size() > 0;
    }

    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    protected IScale getScale() {
        return observation.getScale();
    }

    @Override
    public List<IAction> getActions() {
        return actions;
    }

    public List<IAction> getActions(IAction.Trigger trigger) {

        switch (trigger) {
        case TERMINATION:
            return terminationActions;
        case TRANSITION:
            return temporalActions;
        case STATE_INITIALIZATION:
            return stateInitializationActions;
        case EVENT:
            return new ArrayList<>(eventActions.values());
        default:
            List<IAction> ret = new ArrayList<>();
            for (IAction a : actions) {
                if (a.getTrigger() == trigger) {
                    ret.add(a);
                }
            }
            return ret;
        }

    }

    @Override
    public boolean hasEventHandlers() {
        return !eventActions.isEmpty();
    }

    @Override
    public boolean isTemporal() {
        return temporalActions.size() > 0;
    }

    protected IDirectObservation getContext() {
        return context;
    }

    public void copyActions(IActuator actuator) {

        /*
         * FIXME if there are other actions we should probably prepend, not add FIXME this
         * is only necessary with the current dataflow compiler, which should be
         * rewritten.
         */
        this.initializationActions.addAll(((Actuator) actuator).initializationActions);
        this.temporalActions.addAll(((Actuator) actuator).temporalActions);
        this.actions.addAll(((Actuator) actuator).actions);
        this.eventActions.putAll(((Actuator) actuator).eventActions);
        this.terminationActions.addAll(((Actuator) actuator).terminationActions);
    }

    /**
     * Use in non-temporal accessors to check if the initialized values should be
     * recomputed after temporal transitions.
     * 
     * @return
     */
    protected boolean inputsChanged() {
        return true;
    }

    public void setProvenance(Artifact provenanceArtifact) {
        this.provenance = provenanceArtifact;
        ((ProvenanceNode) this.provenance).setActuator(this);
    }

}
