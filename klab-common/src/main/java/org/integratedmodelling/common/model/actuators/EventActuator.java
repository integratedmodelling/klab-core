/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAction.Trigger;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveEvent;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IEventContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.common.interfaces.actuators.IEventActuator;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.exceptions.KlabException;

public class EventActuator extends DirectActuator<IActiveEvent> implements IEventActuator {

    private HashMap<IConcept, IState> inputsByConcept = new HashMap<>();
    private IEventContextualizer      contextualizer  = null;
    private Map<String, IState>       statesToInputs;
    private Set<IState>               toFlush         = new HashSet<>();

    public EventActuator(IActiveEvent observation, IModel model, IEventContextualizer ds,
            List<IAction> actions, IMonitor monitor) {
        // TODO Auto-generated constructor stub
        super(observation, model);
        this.contextualizer = ds;
        for (IAction action : actions) {
            this.addAction(action);
        }
        if (getActions(Trigger.EVENT).size() > 0) {
            observation.getContext().getEventBus().subscribe(observation, this);
        }
    }

    @Override
    public String toString() {
        return contextualizer == null ? "bare event"
                : Path.getLast(contextualizer.getClass().getCanonicalName(), '.');
    }

    protected IState getInput(IConcept concept) {
        for (IConcept c : inputsByConcept.keySet()) {
            if (c.is(concept)) {
                return inputsByConcept.get(c);
            }
        }
        return null;
    }

    protected Collection<IConcept> getInputKeys() {
        return inputsByConcept.keySet();
    }

    protected IState getInputState(IConcept observable) {
        return inputsByConcept.get(observable);
    }

    protected Collection<IState> getInputStates() {
        return inputsByConcept.values();
    }

    /**
     * TODO this is a cut and paste from process
     */
    @Override
    public Map<String, IObservation> initialize(IActiveEvent event, IActiveDirectObservation context, IResolutionScope resolutionContext, IMonitor monitor)
            throws KlabException {

        this.context = context;
        Map<String, IObservableSemantics> ous = new HashMap<>();
        IModel model = event.getModel();

        for (int i = 1; i < model.getObservables().size(); i++) {
            ous.put(model.getObservables().get(i).getFormalName(), model.getObservables().get(i));
        }

        monitor.info("initializing process contextualizer "
                + contextualizer.getClass().getSimpleName(), null);

        for (IAction action : getActions(Trigger.DEFINITION)) {
            ((KIMAction) action)
                    .execute(observation
                            .getContextObservation(), event, ITransition.INITIALIZATION, provenance, monitor);
        }

        Map<String, IObservation> ret = isReinterpreter ? reinterpretRoles(context)
                : (contextualizer == null ? null : contextualizer
                        .initialize(event, context, resolutionContext, inputsById, ous, monitor));

        this.statesToInputs = States.matchStatesToInputs(context, inputsById);

        // this allows observations to be made in init actions
        ((Observation) event).setInitialized(true);

        for (IObservation obs : ((IActiveDirectObservation) this.observation)
                .getActionGeneratedObservations(true)) {
            String name = obs instanceof IDirectObservation
                    ? ((IDirectObservation) obs).getName()
                    : ((IState) obs).getObservable().getSemantics().getFormalName();
            ret.put(name, obs);
        }

        /*
         * add all the dynamic returned states to the list of those to flush before
         * transitions
         */
        for (IObservation o : ret.values()) {
            if (o instanceof IState && ((IState) o).isDynamic()) {
                toFlush.add((IState) o);
            }
        }

        if (event.getScale().getTime() != null && event.getScale().getTime().getMultiplicity() > 1) {
            monitor.info("process " + event.getObservable().getType()
                    + " ready to run temporal transitions", Messages.INFOCLASS_COMPLETED);

        } else {
            monitor.info("process model " + event.getObservable().getType()
                    + " fully contextualized (non-dynamic)", null);
        }

        return ret;
    }

    @Override
    public Map<String, IObservation> processTransition(ITransition transition, IMonitor monitor)
            throws KlabException {

        Map<String, IState> inps = new HashMap<>();
        for (String s : statesToInputs.keySet()) {
            if (States.hasChanged(statesToInputs.get(s))) {
                inps.put(s, statesToInputs.get(s));
            }
        }

        /*
         * flush states
         */
        for (IState state : toFlush) {
            ((State) state).flushStorage(transition);
            States.setChanged(state, false);
        }
        Map<String, IObservation> outs = null;
        if (contextualizer != null) {
            outs = contextualizer.compute(transition, inps);
            if (outs != null) {
                for (IObservation o : outs.values()) {
                    ((Transition) transition).addModifiedObservation(o);
                }
            }
        }

        for (IAction action : getActions(Trigger.TRANSITION)) {
            if (((IActiveDirectObservation) this.observation).isActive()) {
                ((KIMAction) action).execute(observation
                        .getContextObservation(), getObservation(), transition, provenance, monitor);
            }
        }

        for (IObservation obs : ((IActiveDirectObservation) this.observation)
                .getActionGeneratedObservations(true)) {
            String name = obs instanceof IDirectObservation
                    ? ((IDirectObservation) obs).getName()
                    : ((IState) obs).getObservable().getSemantics().getFormalName();
            outs.put(name, obs);
        }

        return outs;
    }

    @Override
    public IActiveEvent getEvent() {
        return (IActiveEvent) observation;
    }

}
