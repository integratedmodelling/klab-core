/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.base.HashableObject;
import org.integratedmodelling.collections.ImmutableList;
import org.integratedmodelling.collections.MultidimensionalCursor;
import org.integratedmodelling.collections.MultidimensionalCursor.StorageOrdering;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.exceptions.KlabRuntimeException;

public abstract class AbstractScale extends HashableObject implements IScale, NetworkSerializable {

    protected List<IExtent>          extents      = new ArrayList<>();
    protected long                   multiplicity = 0;
    protected int                    sIndex       = -1;
    protected int                    tIndex       = -1;
    protected ITemporalExtent        time = null;
    protected ISpatialExtent        space = null;
    protected MultidimensionalCursor cursor;

    class ScaleIndex extends ImmutableList<Integer>implements Index {

        int[]             dimensions;
        int               dmax = -1;
        int               dind = -1;
        int               type = 0;      // 1 = temporal; 2 = spatial
        IExtent           extent;
        Iterable<Integer> cursor;

        public ScaleIndex(Iterable<Integer> dimensionScanner, int dimIndex) {

            /*
             * TODO dimIndex may be -1, in which case we only specify ONE point and must
             * behave properly.
             */

            this.cursor = dimensionScanner;
            dind = dimIndex;
            int i = 0;
            for (IExtent e : AbstractScale.this) {
                if (i == dind) {
                    dmax = (int) e.getMultiplicity();
                    if (e instanceof ITemporalExtent) {
                        type = 1;
                    } else if (e instanceof ISpatialExtent) {
                        type = 2;
                    }
                    extent = e;
                    break;
                }
                i++;
            }
        }

        /*
         * creates an index that only has one offset
         */
        public ScaleIndex(int elementOffset) {
            this.cursor = Collections.singleton(elementOffset);
        }

        @Override
        public boolean equals(Object obj) {
            return obj instanceof ScaleIndex && Arrays.equals(dimensions, ((ScaleIndex) obj).dimensions);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(dimensions);
        }

        int getOffsetFor(int i) {
            dimensions[dind] = i;
            return getCursor().getElementOffset(dimensions);
        }

        @Override
        public boolean contains(Object arg0) {
            throw new UnsupportedOperationException("operation not allowed");
        }

        @Override
        public Integer get(int i) {
            return getOffsetFor(i);
        }

        @Override
        public Iterator<Integer> iterator() {
            return cursor.iterator();
        }

        @Override
        public int size() {
            return dmax;
        }

        @Override
        public Object[] toArray() {
            throw new UnsupportedOperationException("operation not allowed");
        }

        @Override
        public <T> T[] toArray(T[] arg0) {
            throw new UnsupportedOperationException("operation not allowed");
        }

        @Override
        public boolean isSpatial() {
            return type == 1;
        }

        @Override
        public boolean isTemporal() {
            return type == 2;
        }

        @Override
        public IConcept getDomainConcept() {
            return extent.getDomainConcept();
        }

        @Override
        public int[] getOffsets() {
            int[] ret = dimensions.clone();
            ret[dind] = -1;
            return ret;
        }

        @Override
        public boolean isActive(int offset) {
            return extent.isCovered(offset);
        }
    }

    @Override
    public final Index getIndex(int sliceIndex, int sliceNumber, Locator... locators) {

        int variableDimension = -1;
        int[] exts = new int[getExtentCount()];
        Arrays.fill(exts, IExtent.GENERIC_LOCATOR);
        int i = 0;
        for (IExtent e : this) {
            for (Locator o : locators) {
                int n = e.locate(o);
                if (n != IExtent.INAPPROPRIATE_LOCATOR) {
                    exts[i] = n;
                    break;
                }
            }
            i++;
        }

        /*
         * 
         */
        int nm = 0;
        for (i = 0; i < exts.length; i++) {
            if (exts[i] == IExtent.GENERIC_LOCATOR) {
                nm++;
                variableDimension = i;
            }
        }

        if (nm > 1) {
            throw new KlabRuntimeException("cannot iterate a scale along more than one dimensions");
        }

        return new ScaleIndex(getCursor()
                .getDimensionScanner(variableDimension, exts, sliceIndex, sliceNumber), variableDimension);
    }

    @Override
    public final Index getIndex(Locator... locators) {

        int variableDimension = -1;
        int[] exts = new int[getExtentCount()];
        Arrays.fill(exts, IExtent.GENERIC_LOCATOR);
        int i = 0;
        for (IExtent e : this) {
            for (Locator o : locators) {
                int n = e.locate(o);
                if (n != IExtent.INAPPROPRIATE_LOCATOR) {
                    exts[i] = n;
                    break;
                }
            }
            i++;
        }

        /*
         * 
         */
        int nm = 0;
        for (i = 0; i < exts.length; i++) {
            if (exts[i] == IExtent.GENERIC_LOCATOR) {
                nm++;
                variableDimension = i;
            }
        }

        if (nm == 0) {
            return new ScaleIndex(getCursor().getElementOffset(exts));
        }

        if (nm > 1) {
            throw new KlabRuntimeException("cannot iterate a scale along more than one dimensions");
        }

        return new ScaleIndex(getCursor().getDimensionScanner(variableDimension, exts), variableDimension);
    }

    @Override
    public int getExtentOffset(IExtent extent, int overallOffset) {
        int n = 0;
        boolean found = false;
        for (IExtent e : this) {
            if (e.getDomainConcept().equals(extent.getDomainConcept())) {
                found = true;
                break;
            }
            n++;
        }
        if (!found) {
            throw new KlabRuntimeException("cannot locate extent " + extent.getDomainConcept()
                    + " in scale");
        }
        return getCursor().getElementIndexes(overallOffset)[n];
    }

    @Override
    public boolean isTemporallyDistributed() {
        return getTime() != null && getTime().getMultiplicity() > 1;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return getSpace() != null && getSpace().getMultiplicity() > 1;
    }

    protected void sort() {

        ArrayList<IExtent> order = new ArrayList<>(extents);

        /*
         * Is it fair to think that if two extent concepts have an ordering relationship,
         * they should know about each other? So that we can implement the ordering as a
         * relationship between extent observation classes? For now, all we care about is
         * that time, if present, comes first.
         */
        Collections.sort(order, new Comparator<IExtent>() {

            @Override
            public int compare(IExtent o1, IExtent o2) {
                // neg if o1 < o2
                boolean o1t = o1 instanceof ITemporalExtent;
                boolean o2t = o2 instanceof ITemporalExtent;
                if (o1t && !o2t) {
                    return -1;
                }
                if (!o1t && o2t) {
                    return 1;
                }
                return 0;
            }
        });

        multiplicity = 1L;
        int idx = 0;
        for (IExtent e : order) {

            if (e.getMultiplicity() == INFINITE) {
                multiplicity = INFINITE;
            }
            if (e instanceof ITemporalExtent) {
                tIndex = idx;
                time = (ITemporalExtent) e;
            } else if (e instanceof ISpatialExtent) {
                sIndex = idx;
                space = (ISpatialExtent) e;
            }

            if (multiplicity != INFINITE)
                multiplicity *= e.getMultiplicity();

            idx++;
        }

        // better safe than sorry. Only time can be infinite so this should be pretty safe
        // as long as
        // the comparator above works.
        if (multiplicity == INFINITE && extents.get(0).getMultiplicity() != INFINITE) {
            throw new KlabRuntimeException("internal error: infinite dimension not the first in scale");
        }

        // recompute strided offsets for quick extent access
        cursor = new MultidimensionalCursor(StorageOrdering.ROW_FIRST);
        int[] dims = new int[multiplicity == INFINITE ? extents.size() - 1 : extents.size()];
        int n = 0;
        for (int i = multiplicity == INFINITE ? 1 : 0; i < extents.size(); i++) {
            dims[n++] = (int) extents.get(i).getMultiplicity();
        }
        cursor.defineDimensions(dims);
        extents = order;
    }

    @Override
    public long locate(Locator... locators) {

        int[] loc = new int[getExtentCount()];
        int i = 0;
        for (IExtent e : this) {
            for (Locator l : locators) {
                int idx = e.locate(l);
                if (idx >= 0) {
                    loc[i++] = idx;
                    break;
                }
            }
        }
        return getCursor().getElementOffset(loc);
    }


    @Override
    public int getExtentCount() {
        return extents.size();
    }

    
    @Override
    public ISpatialExtent getSpace() {
        return space;
    }

    @Override
    public ITemporalExtent getTime() {
        return time;
    }

    
    @Override
    public boolean isCovered(int offset) {
        int[] oofs = getExtentIndex(offset);
        for (int i = 0; i < getExtentCount(); i++) {
            if (!getExtent(i).isCovered(oofs[i])) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isConsistent() {
        for (int i = 0; i < getExtentCount(); i++) {
            if (!getExtent(i).isConsistent()) {
                return false;
            }
        }
        return true;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Scale.class)) {
            throw new KlabRuntimeException("cannot serialize a Scale to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Scale ret = new org.integratedmodelling.common.beans.Scale();

        ret.setMultiplicity(getMultiplicity());
        for (int i = 0; i < this.getExtentCount(); i++) {

            IExtent ext = getExtent(i);
            org.integratedmodelling.common.beans.Extent ee = null;
            if (ext instanceof ISpatialExtent) {
                ret.setSpace(KLAB.MFACTORY.adapt(ext, org.integratedmodelling.common.beans.Space.class));
            } else if (ext instanceof ITemporalExtent) {
                ret.setTime(KLAB.MFACTORY.adapt(ext, org.integratedmodelling.common.beans.Time.class));
            }
        }
        return (T) ret;
    }

}
