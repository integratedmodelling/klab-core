package org.integratedmodelling.common.model.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/*
 * Client version of ISubject. 
 * 
 * For now we have to keep it an active subject, despite this not having any expectations of
 * being active. Not doing so will make the context refuse it, due to the context base
 * implementation being shared between engine and client. This explains all the dummy
 * methods at the bottom.
 * 
 * @author ferdinando.villa
 *
 */
public class Subject extends DirectObservation implements IActiveSubject, NetworkDeserializable {

	List<ISubject> subjects = new ArrayList<>();

	IStructure structure;

	public boolean hasChildren() {
		return subjects.size() > 0 || processes.size() > 0 || states.size() > 0
				|| (structure != null && structure.getRelationships().size() > 0);
	}

	public Subject() {
		structure = new Structure(this);
	}

	@Override
	public IStructure getStructure(Locator... locators) {
		return structure;
	}

	@Override
	public Collection<ISubject> getSubjects() {
		return subjects;
	}

	@Override
	public Collection<IProcess> getProcesses() {
		return processes;
	}

	@Override
	public Collection<IEvent> getEvents() {
		return events;
	}

	@Override
	public void deserialize(IModelBean object) {

		if (!(object instanceof org.integratedmodelling.common.beans.Subject)) {
			throw new KlabRuntimeException(
					"cannot deserialize a Subject from a " + object.getClass().getCanonicalName());
		}
		this.bean = (org.integratedmodelling.common.beans.Subject) object;
		super.deserialize(bean);
	}

    public IObservation get(String path) {

        if (path.startsWith("/")) {
            path = path.substring(1);
        }
        String[] s = path.split("\\/");
        IObservation ret = this;

        for (String id : s) {

            for (IState state : getStates()) {
                // states have no children
                if (((Observation) state).getInternalId().equals(id))
                    return state;
            }
            for (ISubject subject : getSubjects()) {
                if (((Observation) subject).getInternalId().equals(id))
                    ret = subject;
            }
        }

        return ret;
    }

	@Override
	public IObservation find(String id) {
		if (this.id.equals(id)) {
			return this;
		}
		IObservation ret = null;
		for (ISubject sub : getSubjects()) {
			ret = ((Observation) sub).find(id);
			if (ret != null) {
				return ret;
			}
		}
		for (IEvent sub : getEvents()) {
			ret = ((Observation) sub).find(id);
			if (ret != null) {
				return ret;
			}
		}
		for (IProcess sub : getProcesses()) {
			ret = ((Observation) sub).find(id);
			if (ret != null) {
				return ret;
			}
		}
		for (IState sub : getStates()) {
			ret = ((Observation) sub).find(id);
			if (ret != null) {
				return ret;
			}
		}
		return ret;
	}

	@Override
	public void append(IObservation observation) {

		if (observation instanceof IState) {
			states.add((IState) observation);
		} else if (observation instanceof IProcess) {
			processes.add((IProcess) observation);
		} else if (observation instanceof IEvent) {
			events.add((IEvent) observation);
		} else if (observation instanceof ISubject) {
			subjects.add((ISubject) observation);
		} else if (observation instanceof IRelationship) {
			((Structure) structure).append((IRelationship) observation);
		}
	}

	@Override
	public String toString() {
		return "<" + getObservable().getType() + " " + getName() + ">";
	}

	@Override
	public ICollision detectCollision(IObservationGraphNode myAgentState, IObservationGraphNode otherAgentState) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ITransition reEvaluateStates(ITimePeriod timePeriod) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ITransition performTemporalTransitionFromCurrentState(IObservationController controller) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<IProperty, IState> getObjectStateCopy() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IModel getModel() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IState getState(IObservableSemantics observable, Object... data) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IState getStaticState(IObservableSemantics observable) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void contextualize() throws KlabException {
		// TODO Auto-generated method stub

	}

	// @Override
	// public IResolutionStrategy getResolutionStrategy() {
	// // TODO Auto-generated method stub
	// return null;
	// }

	@Override
	public void addState(IState s) throws KlabException {
		// TODO Auto-generated method stub

	}

	@Override
	public IDataset getBackingDataset() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IEvent newEvent(IObservableSemantics observable, IScale scale, String name) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ISubject newSubject(IObservableSemantics observable, IScale scale, String name, IProperty relationship)
			throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public IProcess newProcess(IObservableSemantics observable, IScale scale, String name) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void addEventHandler(IAction action) {
		// TODO Auto-generated method stub

	}

	@Override
	public ICoverage getResolvedCoverage(ISemantic concept) {
		// TODO this should be communicated, too
		return null;
	}

	@Override
	public IRelationship connect(IActiveSubject source, IActiveSubject destination, IConcept relationshipType,
			IScale relationshipScale) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean setActive(boolean state) {
		boolean ret = bean.isActive();
		bean.setActive(state);
		return ret;
	}

	@Override
	public boolean isActive() {
		return bean.isActive();
	}


    @Override
    public Collection<IObservation> getActionGeneratedObservations(boolean reset) {
        // TODO Auto-generated method stub
        return null;
    }

}
