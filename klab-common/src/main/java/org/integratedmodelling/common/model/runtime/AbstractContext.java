package org.integratedmodelling.common.model.runtime;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.beans.Context;
import org.integratedmodelling.common.beans.ModelArtifact;
import org.integratedmodelling.common.beans.Scenario;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.knowledge.ObservationGroup;
import org.integratedmodelling.common.model.Coverage;
import org.integratedmodelling.common.monitoring.Notifiable;

/**
 * @author ferdinando.villa
 */
public abstract class AbstractContext extends Notifiable implements IContext {

    protected String             id;
    protected ISubject           subject;
    protected Deque<ITask>       tasks              = new ArrayDeque<>();
    protected Map<String, ITask> tasksById          = new HashMap<>();
    protected boolean            isFinished;
    protected boolean            isRunning;
    protected ICoverage          coverage;
    protected long               creationTime       = System.currentTimeMillis();
    protected Set<IObservation>  newObservations    = new HashSet<>();
    protected IOntology          ontology;

    /*
     * parent context. Null in the root context. Child contexts are only created by
     * focus(); with(scenario) etc. merely duplicate it.
     */
    protected IContext           parent;

    /*
     * user scenario, using a bean that merely records user choices, to be translated into
     * actual objects at the time of execution.
     */
    protected Scenario           scenario           = new Scenario();
    /*
     * the schedule of initialization, used to organize dependencies during contextualization.
     */
    private List<IDirectObservation> schedule = new LinkedList<>();

    public List<IDirectObservation> getSchedule() {
        return schedule;
    }

    public void schedule(IActiveDirectObservation observation) {
        schedule.add(observation);
        ((Observation)observation).setPriorityOrder(schedule.size() - 1);
    }

    // we keep these if we don't find an obs to attach them to right away
    private Set<ModelArtifact>   postponedArtifacts = new HashSet<>();

    protected AbstractContext() {
    }

    public IOntology getOntology() {
        if (this.ontology == null) {
            this.ontology = KLAB.KM.requireOntology(getId());
        }
        return this.ontology;
    }

    public Scenario getScenario() {
        return scenario;
    }
    

    @Override
    public String getSecurityKey() {
        // TODO Auto-generated method stub
        return null;
    }
    

    @Override
    public <T extends IIdentity> T getParentIdentity(Class<? extends IIdentity> type) {
        // TODO Auto-generated method stub
        return null;
    }

    protected AbstractContext(AbstractContext context) {
        this.id = context.id;
        this.subject = context.subject;
        this.tasks = context.tasks;
        this.tasksById = context.tasksById;
        this.isFinished = context.isFinished;
        this.isRunning = context.isRunning;
        this.coverage = context.coverage == null ? null : new Coverage(context.coverage);
        this.ontology = context.ontology;
        this.scenario = context.scenario;
        this.parent = context.parent;
    }

    @Override
    public List<ITask> getTasks() {
        return new ArrayList<>(tasks);
    }

    /**
     * @param id
     * @return the task with the given id, or null if not found.
     */
    public ITask getTask(String id) {
        return tasksById.get(id);
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public boolean isEmpty() {
        return subject == null;
    }

    @Override
    public boolean isFinished() {
        return isFinished;
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public ICoverage getCoverage() {
        return coverage;
    }

    @SuppressWarnings("javadoc")
    public void setCoverage(ICoverage coverage) {
        this.coverage = coverage;
    }

    @Override
    public IObservation get(String path) {
        return subject == null ? null : ((Subject) subject).get(path);
    }

    @Override
    public String getPathFor(IObservation observation) {

        IObservation parent = observation.getContextObservation();

        if (observation instanceof ObservationGroup) {
            return "G|" + ((Observation) parent).getInternalId() + "|"
                    + ((ObservationGroup) observation).getType().getDefinition();
        }

        String ret = parent == null ? "" : ((Observation) observation).getInternalId();

        // we don't want the root ID in the path.
        while (parent != null && parent.getContextObservation() != null) {
            ret = ((Observation) parent).getInternalId() + "/" + ret;
            parent = parent.getContextObservation();
        }

        return "/" + ret;
    }

    /**
     * @param task
     */
    public void addTask(ITask task) {
        tasks.push(task);
        tasksById.put(task.getTaskId(), task);
    }

    /**
     * Redefine the context as required by new information contained in the passed bean.
     * 
     * @param context
     */
    public void acceptDelta(Context context) {

        isFinished = context.isFinished();

        coverage = new Coverage(null, context.getCoverage());

        if (context.getDeltaSubjects() != null) {
            for (org.integratedmodelling.common.beans.Subject s : context
                    .getDeltaSubjects()) {
                insertObservation(KLAB.MFACTORY.adapt(s, Subject.class));
            }
        }
        if (context.getDeltaEvents() != null) {
            for (org.integratedmodelling.common.beans.Event s : context
                    .getDeltaEvents()) {
                insertObservation(KLAB.MFACTORY.adapt(s, Event.class));
            }
        }
        if (context.getDeltaProcesses() != null) {
            for (org.integratedmodelling.common.beans.Process s : context
                    .getDeltaProcesses()) {
                insertObservation(KLAB.MFACTORY.adapt(s, Process.class));
            }
        }
        if (context.getDeltaRelationships() != null) {
            for (org.integratedmodelling.common.beans.Relationship s : context
                    .getDeltaRelationships()) {
                insertObservation(KLAB.MFACTORY.adapt(s, Relationship.class));
            }
        }
        // states must be last
        if (context.getDeltaStates() != null) {
            for (org.integratedmodelling.common.beans.State s : context
                    .getDeltaStates()) {
                insertObservation(KLAB.MFACTORY.adapt(s, State.class));
            }
        }

    }

    private void insertObservation(IObservation observation) {

        ((Observation) observation).setContext(this);

        /*
         * put away model artifacts we got before this was sent
         */
        if (observation instanceof DirectObservation && postponedArtifacts.size() > 0) {

            List<ModelArtifact> added = new ArrayList<>();
            
            for (ModelArtifact artifact : postponedArtifacts) {
                if (artifact.getObservationId().equals(((Observation)observation).getInternalId())) {
                    ((DirectObservation)observation).addArtifact(artifact);
                    added.add(artifact);
                }
            }
            
            for (ModelArtifact a : added) {
                postponedArtifacts.remove(a);
            }
        }
        
        if (((Observation) observation).getParentId() == null) {
            if (observation instanceof IActiveSubject) {
                subject = (IActiveSubject) observation;
            }
        } else {
            IObservation obs = findWithId(((Observation) observation).getParentId());
            if (obs != null) {
                ((Observation) obs).append(observation);
                if (obs.equals(subject)) {
                    newObservations.add(observation);
                }
            } else {
                System.out.println("DIOCOZZA NON TROVO IL PORCO");
            }
        }
    }

    /**
     * Find the observation with the passed internal ID
     * 
     * @param id
     * @return observation with passed ID or null
     */
    public IObservation findWithId(String id) {
        if (subject == null) {
            return null;
        }
        return ((Observation) subject).find(id);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.integratedmodelling.api.runtime.IContext#getCreationTime()
     */
    @Override
    public long getCreationTime() {
        return creationTime;
    }

    public void registerArtifact(ModelArtifact a) {
        this.postponedArtifacts.add(a);
    }

}
