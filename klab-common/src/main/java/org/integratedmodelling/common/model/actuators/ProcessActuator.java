/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAction.Trigger;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveProcess;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IProcessContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.common.interfaces.actuators.IProcessActuator;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.model.runtime.AbstractContext;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;

public class ProcessActuator extends DirectActuator<IActiveProcess> implements IProcessActuator {

    private HashMap<IConcept, IState> inputsByConcept = new HashMap<>();
    private IProcessContextualizer    contextualizer  = null;
    private Map<String, IState>       statesToInputs;
    private Set<IState>               toFlush         = new HashSet<>();

    public ProcessActuator(IActiveProcess observation, IModel model, IProcessContextualizer ds,
            List<IAction> actions, IMonitor monitor) {
        super(observation, model);
        this.contextualizer = ds;
        for (IAction action : actions) {
            this.addAction(action);
        }
        if (getActions(Trigger.EVENT).size() > 0) {
            observation.getContext().getEventBus().subscribe(observation, this);
        }
    }

    @Override
    public String toString() {
        return contextualizer == null ? "bare process"
                : Path.getLast(contextualizer.getClass().getCanonicalName(), '.');
    }

    protected IState getInput(IConcept concept) {
        for (IConcept c : inputsByConcept.keySet()) {
            if (c.is(concept)) {
                return inputsByConcept.get(c);
            }
        }
        return null;
    }

    protected Collection<IConcept> getInputKeys() {
        return inputsByConcept.keySet();
    }

    protected IState getInputState(IConcept observable) {
        return inputsByConcept.get(observable);
    }

    protected Collection<IState> getInputStates() {
        return inputsByConcept.values();
    }

    @Override
    public Map<String, IObservation> initialize(IActiveProcess process, IActiveDirectObservation context, IResolutionScope resolutionContext, IMonitor monitor)
            throws KlabException {

        this.context = context;
        Map<String, IObservableSemantics> ous = new HashMap<>();

        IModel model = process.getModel();
        Map<String, IObservation> ret = new HashMap<>();

        for (int i = 1; i < model.getObservables().size(); i++) {
            ous.put(model.getObservables().get(i).getFormalName(), model.getObservables().get(i));
        }

        monitor.info("initializing process contextualizer "
                + (contextualizer != null ? contextualizer.getClass().getSimpleName() : ""), null);

        if (contextualizer != null) {
            ret = contextualizer
                    .initialize(process, context, resolutionContext, inputsById, ous, monitor);
        } else {
            /*
             * FIXME/TODO: output states are only created automatically if there is no
             * contextualizer, assuming the contextualizer will know what to do (and
             * potentially actions won't find the states if not).
             */
            createOutputStates(context, model);
        }
        
        if (isReinterpreter) {
            ret = reinterpretRoles(context);
        } 
        
        // this allows observations to be made in init actions 
        ((Observation)process).setInitialized(true);

        for (IAction action : getActions(Trigger.DEFINITION)) {
            if (((IActiveDirectObservation) this.observation).isActive()) {
                ((KIMAction) action).execute(observation
                        .getContextObservation(), process, ITransition.INITIALIZATION, provenance, monitor);
            }
        }

        if (((IActiveDirectObservation)this.observation).isActive()) {
            ((AbstractContext)observation.getContext()).schedule((IActiveDirectObservation) this.observation);
        }

        for (IObservation obs : ((IActiveDirectObservation) this.observation)
                .getActionGeneratedObservations(true)) {
            // may add obs to both self and context
            for (IObservation oo : context.getActionGeneratedObservations(true)) {
                ret.put(getObservationName(oo), oo);
            }
        }
        
        this.statesToInputs = States.matchStatesToInputs(context, inputsById);

        /*
         * add all the dynamic returned states to the list of those to flush before
         * transitions
         */
        if (ret != null) {
            for (IObservation o : ret.values()) {
                if (o instanceof IState && ((IState) o).isDynamic()) {
                    toFlush.add((IState) o);
                }
            }
        }

        for (IAction action : getActions(Trigger.RESOLUTION)) {
            /*
             * TODO pass states etc
             */
            ((KIMAction) action).execute(observation
                    .getContextObservation(), process, ITransition.INITIALIZATION, provenance, monitor);
            
            for (IObservation obs : ((IActiveDirectObservation) this.observation)
                    .getActionGeneratedObservations(true)) {
                // may add obs to both self and context
                for (IObservation oo : context.getActionGeneratedObservations(true)) {
                    ret.put(getObservationName(oo), oo);
                }
            }
        }
        
        if (process.getScale().getTime() != null && process.getScale().getTime().getMultiplicity() > 1) {
            monitor.info("process " + NS.getDisplayName(process.getObservable().getType())
                    + " ready to run temporal transitions", Messages.INFOCLASS_COMPLETED);

        } else {
            monitor.info("process model " + NS.getDisplayName(process.getObservable().getType())
                    + " fully contextualized (non-dynamic)", null);
        }

        return ret;
    }

    @Override
    public Map<String, IObservation> processTransition(ITransition transition, IMonitor monitor)
            throws KlabException {

        Map<String, IObservation> outs = new HashMap<>();
        Map<String, IState> inps = new HashMap<>();

        for (String s : statesToInputs.keySet()) {
            if (States.hasChanged(statesToInputs.get(s))) {
                inps.put(s, statesToInputs.get(s));
            }
        }

        /*
         * flush states
         */
        for (IState state : toFlush) {
            ((State) state).flushStorage(transition);
            States.setChanged(state, false);
        }

        if (contextualizer != null) {
            outs = contextualizer.compute(transition, inps);
            if (outs != null) {
                for (IObservation o : outs.values()) {
                    ((Transition) transition).addModifiedObservation(o);
                }
            }
        }

        for (IAction action : getActions(Trigger.TRANSITION)) {
            if (((IActiveDirectObservation) this.observation).isActive()) {
                ((KIMAction) action).execute(observation
                        .getContextObservation(), getProcess(), transition, provenance, monitor);
            }
        }

        for (IObservation obs : ((IActiveDirectObservation) this.observation)
                .getActionGeneratedObservations(true)) {
            String name = obs instanceof IDirectObservation
                    ? ((IDirectObservation) obs).getName()
                    : ((IState) obs).getObservable().getSemantics().getFormalName();
            outs.put(name, obs);
        }

        return outs;
    }

    @Override
    public IActiveProcess getProcess() {
        return (IActiveProcess) getObservation();
    }

}
