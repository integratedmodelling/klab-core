package org.integratedmodelling.common.model.runtime;

import java.util.Iterator;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Client version of a state.
 * 
 * @author ferdinando.villa
 *
 */
public class State extends Observation implements IState, NetworkDeserializable {

    private boolean   isConstant;
    private boolean   isDynamic;
    private Object    constantData;
    private Object    scalarData;
    private IObserver observer;
    
    /**
     * A descriptive label suggestive of both observation and observable, suitable for
     * display in UI.
     * 
     * @return the state label
     */
    public String getLabel() {

        IConcept obs = getObservable().getSemantics().getType();
        if (((ObservableSemantics) getObservable().getSemantics()).getLocalType() != null) {
            obs = ((ObservableSemantics) getObservable().getSemantics()).getLocalType();
        }

        String ret = NS.getDisplayName(obs);

        if (constantData != null || scalarData != null) {

            ret += " = " + (scalarData == null ? constantData : scalarData);

            /**
             * FIXME if has a value mediator - applies to measurement, count, value
             */
            if (getObserver() instanceof IMeasuringObserver) {
                ret += " " + ((IMeasuringObserver) getObserver()).getUnit();
            } else if (getObserver() instanceof IValuingObserver) {
                ret += " " + ((IValuingObserver) getObserver()).getCurrency().asText();
            }
        }
        return ret;
    }


    
    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.State)) {
            throw new KlabRuntimeException("cannot deserialize a State from a "
                    + object.getClass().getCanonicalName());
        }

        org.integratedmodelling.common.beans.State bean = (org.integratedmodelling.common.beans.State) object;
        super.deserialize(bean);

        this.isConstant = bean.isConstant();
        this.isDynamic = bean.isDynamic();
        this.constantData = bean.getConstantData();
        this.scalarData = bean.getScalarData();
        this.observer = KLAB.MFACTORY.adapt(bean.getObserver(), IObserver.class);

        if (bean.getMetadata() != null) {
            this.metadata = KLAB.MFACTORY.adapt(bean.getMetadata(), Metadata.class);
        } else {
            this.metadata = new Metadata();
        }
    }

    @Override
    public String toString() {
        return "S/" + getObservable().getSemantics().toString();
    }

    @Override
    public long getValueCount() {
        return getScale().getMultiplicity();
    }

    @Override
    public IObserver getObserver() {
        return observer;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return getScale().isSpatiallyDistributed();
    }

    @Override
    public boolean isTemporallyDistributed() {
        return getScale().isTemporallyDistributed();
    }

    @Override
    public boolean isTemporal() {
        return getScale().getTime() != null;
    }

    @Override
    public boolean isSpatial() {
        return getScale().getSpace() != null;
    }

    @Override
    public ISpatialExtent getSpace() {
        return getScale().getSpace();
    }

    @Override
    public ITemporalExtent getTime() {
        return getScale().getTime();
    }

    @Override
    public Object getValue(int index) {

        if (isConstant) {
            return constantData;
        }

        /*
         * TODO call engine
         */
        return null;
    }

    @Override
    public Iterator<?> iterator(Index index) {
        // TODO call engine for data
        return null;
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConstant() {
        return isConstant;
    }

    @Override
    public boolean isDynamic() {
        return isDynamic;
    }

    @Override
    public void addChangeListener(ChangeListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public IState as(IObserver observer) {
        // TODO Auto-generated method stub
        return null;
    }

}
