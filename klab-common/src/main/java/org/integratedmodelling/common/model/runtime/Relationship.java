package org.integratedmodelling.common.model.runtime;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.common.beans.State;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Relationships are fully functional only after they have been added to a Structure.
 * 
 * @author ferdinando.villa
 *
 */
public class Relationship extends DirectObservation implements IRelationship, NetworkDeserializable {

    String   sourceId;
    String   targetId;
    ISubject subject;

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Relationship)) {
            throw new KlabRuntimeException("cannot deserialize a Relationship from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Relationship bean = (org.integratedmodelling.common.beans.Relationship) object;
        super.deserialize(bean);

        for (State state : bean.getStates()) {
            // hm.
            this.states.add(KLAB.MFACTORY.adapt(state, org.integratedmodelling.common.model.runtime.State.class));
        }
        
        this.sourceId = bean.getSource();
        this.targetId = bean.getTarget();
    }

    @Override
    public ISubject getSource() {
        return (ISubject) ((Observation) subject).find(sourceId);
    }

    @Override
    public ISubject getTarget() {
        return (ISubject) ((Observation) subject).find(targetId);
    }

    public void setSubject(ISubject subject) {
        this.subject = subject;
    }

    @Override
    public ISubject getContextObservation() {
        return subject;
    }

}
