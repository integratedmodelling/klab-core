package org.integratedmodelling.common.model.runtime;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.exceptions.KlabRuntimeException;

public class Process extends DirectObservation implements IProcess, NetworkDeserializable {

    @Override
    public void deserialize(IModelBean object) {
        if (object instanceof org.integratedmodelling.common.beans.Process) {
            super.deserialize((org.integratedmodelling.common.beans.DirectObservation) object);
        } else {
            throw new KlabRuntimeException("Cannot deserialize a " + object.getClass().getCanonicalName()
                    + " to a Process");
        }
    }

    @Override
    public ISubject getContextObservation() {
        return (ISubject) super.getContextObservation();
    }

    @Override
    public boolean affects(IConcept c) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean creates(IConcept c) {
        // TODO Auto-generated method stub
        return false;
    }

}
