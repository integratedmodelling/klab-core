/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.ICondition;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAction.Trigger;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * TODO make this into the StateContextualizerActuator and remove the latter.
 * 
 * @author Ferd
 *
 */
public class StateActuator extends BaseStateActuator {

    int                         currentStateIndex = -1;
    boolean                     preprocessed      = false;
    boolean                     errors            = false;
    IState                      state             = null;

    /*
     * used to shuttle around contextualizer values, kept here for efficiency only.
     */
    private Map<String, Object> values            = new HashMap<>();

    public StateActuator(IActiveDirectObservation observation, IModel model, IObserver observer,
            IMonitor monitor) {
        super(observation, model);
        if (observer.getActions() != null) {
            for (IAction action : observer.getActions()) {
                addAction(action);
            }
        }
        if (model.getActions() != null) {
            for (IAction action : model.getActions()) {
                addAction(action);
            }
        }
        this.monitor = monitor;
    }

    protected int getStateIndex() {
        return currentStateIndex;
    }

    protected Object doAction(IAction action, Map<String, Object> parameters, ITransition object)
            throws KlabException {

        /*
         * TODO: spatial actions should only be called if the spatial context has changed.
         */

        if (action.getCondition() != null) {
            Object o = action.getCondition().eval(parameters, monitor);
            if (!(o instanceof Boolean))
                throw new KlabValidationException("condition in action does not evaluate to true or false");
            if (!((Boolean) o))
                return null;
        }

        return action.getAction() == null ? null : action.getAction().eval(parameters, monitor);
    }

    @Override
    public String getName() {
        return name;
    }

    protected synchronized void preprocessActions() {

        for (IAction a : getActions(Trigger.DEFINITION)) {
            if (a.getTargetStateId() == null) {
                ((KIMAction) a).setTargetStateId(getName());
            }
            ((KIMAction) a).preprocess(this.inputs, this.outputs);
        }
        this.preprocessed = true;
    }

    /*
     * resolves to nothing when we have no actions. (non-Javadoc)
     * 
     * @see org.integratedmodelling.thinklab.api.modelling.IStateAccessor#process(int)
     */
    @Override
    public Map<String, Object> process(int stateIndex, Map<String, Object> parameters, ITransition transition)
            throws KlabException {

        this.currentStateIndex = stateIndex;

        if (this.errors) {
            return parameters;
        }

        boolean hasErrors = false;

        if (!this.preprocessed) {
            preprocessActions();
        }

        if (this.observation != null) {

            parameters.put("scale", this.observation.getScale());

            if (this.observation.getScale().getSpace() != null) {
                parameters.put("space", this.observation.getScale().getSpace());
            }
            if (this.observation.getScale().getTime() != null) {
                parameters.put("time", this.observation.getScale().getTime());
            }
        }

        if (transition != null) {
            parameters.put("transition", transition);
        }

        /*
         * execute contextualizer chain
         * 
         * TODO/FIXME: contextualizers, processors, validators and actions need to be
         * sequenced properly. Validators must be provided at the end of the chain. So: we
         * tag validators with IStateValidator and if one or more are found, they are
         * executed after the actions.
         */
        if (this.contextualizers != null) {

            values.clear();
            values.putAll(parameters);
            for (IStateContextualizer contextualizer : this.contextualizers) {
                Map<String, Object> ret = transition == ITransition.INITIALIZATION
                        ? contextualizer.initialize(stateIndex, values)
                        : contextualizer.compute(stateIndex, transition, values);
                if (ret != null) {
                    values.putAll(ret);
                }
            }
        }

        parameters.putAll(values);

        /**
         * expose the monitor to the action TODO expose everything else we may need
         */
        if (monitor != null) {
            parameters.put("_monitor", monitor);
        }

        if (contextMetadata != null) {
            parameters.putAll(contextMetadata);
        }

        for (IAction a : (transition == null ? getActions(Trigger.DEFINITION)
                : getActions(Trigger.TRANSITION))) {

            if (a.getType() == IAction.Type.CHANGE || a.getType() == IAction.Type.SET) {

                String subject = a.getTargetStateId();
                
                /*
                 * FIXME this is a patch, although having self should be necessary for 
                 * states when name isn't defined
                 */
                if (parameters.containsKey(name) && !parameters.containsKey("self")) {
                    parameters.put("self", parameters.get(name));
                }
                
                if (checkCondition(a, parameters)) {
                    try {
                        Object res = a.getAction().eval(checkNulls(parameters), monitor);
                        parameters.put(subject, res);
                    } catch (Throwable e) {
                        // only report the first batch of errors
                        if (monitor != null && !hasErrors) {
                            monitor.error("error computing " + a.getAction() + ": " + e.getMessage());
                            hasErrors = true;
                        }
                    }
                }
            } else if (a.getType() == IAction.Type.DO) {

                if (checkCondition(a, parameters)) {
                    a.getAction().eval(parameters, monitor);
                }

            } else if (a.getType() == IAction.Type.INTEGRATE) {

                if (transition != null) {
                    /*
                     * TODO integrate the current value to the next. Observer must be a
                     * rate and we use the time unit to establish the integration interval
                     * w.r.t. the transition period.
                     */
                }
            }
        }

        if (hasErrors) {
            this.errors = true;
        }

        return parameters;
    }

    @Override
    public void performPostInitializationActions() throws KlabException {

        for (IAction action : getActions(Trigger.STATE_INITIALIZATION)) {
            /*
             * clone and recompile action
             */
            IAction a = ((KIMAction) action)
                    .compileFor(observation.getScale(), model);

            ((KIMAction) a)
                    .execute((IDirectObservation) observation, getState(), ITransition.INITIALIZATION, provenance, monitor);

        }

    }

    private Map<String, Object> checkNulls(Map<String, Object> parameters) {

        for (String key : inputs.keySet()) {
            if (parameters.get(key) == null && inputs.get(key) instanceof INumericObserver) {
                parameters.put(key, Double.NaN);
            }
        }

        return parameters;
    }

    private boolean checkCondition(IAction a, Map<String, Object> parameters) throws KlabException {

        boolean go = true;
        if (a.getCondition() != null) {

            Object iif = null;
            try {
                iif = a.getCondition().eval(parameters, monitor);
            } catch (Exception e) {
                // only report the first batch of errors
                if (monitor != null && !this.errors) {
                    monitor.error("error computing " + a.getAction() + ": " + e.getMessage());
                    this.errors = true;
                }
            }
            if (!(iif instanceof Boolean)) {
                // only report the first batch of errors
                if (monitor != null && !this.errors) {
                    monitor.error(new KlabValidationException("condition " + a.getCondition()
                            + " does not return true/false"));
                    this.errors = true;
                }
            }
            go = (Boolean) iif;
            if (a.getCondition() instanceof ICondition && ((ICondition) (a.getCondition())).isNegated()) {
                go = !go;
            }
        }
        return go;
    }

    @Override
    public boolean isProbabilistic() {
        return contextualizers == null || contextualizers.isEmpty() ? false
                : contextualizers.get(contextualizers.size() - 1).isProbabilistic();
    }

    @Override
    public IState getState() {

        if (state == null && model != null && observation instanceof IDirectObservation) {
            /*
             * observation is the context subject; find the state corresponding to the
             * observable of the model.
             */
            for (IState s : ((IDirectObservation) observation).getStates()) {
                if (s.getObservable().getSemantics().equals(model.getObservable())) {
                    this.state = s;
                    ((Observation) this.state).setActuator(this);

                    break;
                }
            }

        }
        return state;
    }

}
