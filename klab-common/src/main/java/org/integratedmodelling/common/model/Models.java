package org.integratedmodelling.common.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.Metadata;
import org.integratedmodelling.common.beans.Model;
import org.integratedmodelling.common.beans.Model.Mediation;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.kim.KIMModel;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Types;

/**
 * Static methods to facilitate dealing with models.
 * 
 * @author ferdinando.villa
 */
public class Models {

    /**
     * Return a collection of model beans that contains all the models implied by a model
     * statement (and the model itself, when appropriate).
     * 
     * @param model
     * @return
     */
    public static Collection<Model> inferModels(IModel model, IMonitor monitor) {
        List<Model> ret = new ArrayList<>();

        for (Model m : getModelDescriptors(model, monitor)) {
            ret.add(m);
        }
        if (ret.size() > 0) {
            /*
             * the observer come out of getAttributeObservers() with their inherent type
             * already set
             */
            for (Pair<String, IObserver> attr : model.getAttributeObservers(true)) {
                Model m = ret.get(0).copy();
                m.setObservable(attr.getSecond().getObservable().getType()
                        .getDefinition());
                m.setObservableConcept(attr.getSecond().getObservable().getType());
                m.setObservationType(attr.getSecond().getObservable().getObservationType()
                        .getDefinition());
                m.setObservationConcept(attr.getSecond().getObservable()
                        .getObservationType());
                m.setDereifyingAttribute(attr.getFirst());
                m.setMediation(Mediation.DEREIFY_QUALITY);
                ret.add(m);
            }

            if (model.isInstantiator()) {
                // TODO add presence model for main observable type and
                // dereifying models for all mandatory attributes of observable in context
            }
        }

        if (NS.isClass(model)) {

            Collection<IConcept> trs = Types
                    .getExposedTraits(model.getObservable().getType());
            IConcept context = Observables
                    .getContextType(model.getObservable().getType());
            IConcept inherent = Observables
                    .getInherentType(model.getObservable().getType());

            if (trs != null) {
                for (IConcept tr : trs) {
                    /**
                     * TODO add model for type of given trait with context and inherency
                     * from class
                     */
                    IConcept tinherent = Observables.getInherentType(tr);
                }
            }

        }

        return ret;
    }

    private static Collection<Model> getModelDescriptors(IModel model, IMonitor monitor) {

        List<Model> ret = new ArrayList<>();
        IScale scale = null;

        try {
            scale = model.getCoverage(monitor);
        } catch (Exception e) {
            return ret;
        }

        IGeometricShape spaceExtent = null;
        ITemporalExtent timeExtent = null;
        long spaceMultiplicity = -1;
        long timeMultiplicity = -1;
        long scaleMultiplicity = 1;
        long timeStart = -1;
        long timeEnd = -1;
        boolean isSpatial = false;
        boolean isTemporal = false;

        if (scale != null) {

            scaleMultiplicity = scale.getMultiplicity();
            if (scale.getSpace() != null) {
                spaceExtent = (IGeometricShape) scale.getSpace().getExtent();
                // may be null when we just say 'over space'.
                if (spaceExtent != null) {
                    spaceMultiplicity = scale.getSpace().getMultiplicity();
                }
                isSpatial = true;
            }
            if (scale.getTime() != null) {
                timeExtent = scale.getTime().getExtent();
                if (timeExtent != null) {
                    if (timeExtent.getStart() != null) {
                        timeStart = timeExtent.getStart().getMillis();
                    }
                    if (timeExtent.getEnd() != null) {
                        timeEnd = timeExtent.getEnd().getMillis();
                    }
                    timeMultiplicity = scale.getTime().getMultiplicity();
                }
                isTemporal = true;
            }
        }

        boolean first = true;
        for (IObservableSemantics obs : model.getObservables()) {

            Model m = new Model();

            m.setId(model.getId());
            m.setName(model.getName());
            m.setNamespaceId(model.getNamespace().getId());
            m.setProjectId(model.getNamespace().getProject().getId());
            if (model.getNamespace().getProject().isRemote()) {
                m.setServerId(model.getNamespace().getProject().getOriginatingNodeId());
            }

            m.setTimeEnd(timeEnd);
            m.setTimeStart(timeStart);
            m.setTimeMultiplicity(timeMultiplicity);
            m.setSpaceMultiplicity(spaceMultiplicity);
            m.setScaleMultiplicity(scaleMultiplicity);
            m.setSpatial(isSpatial);
            m.setTemporal(isTemporal);
            m.setShape(spaceExtent);

            m.setObservable(obs.getType().getDefinition());
            m.setObservationType(obs.getObservationType().getDefinition());
            m.setObservableConcept(obs.getType());
            m.setObservationConcept(obs.getObservationType());

            m.setPrivateModel(model.isPrivate());
            m.setInScenario(model.getNamespace().isScenario());
            m.setReification(model.isInstantiator());
            m.setResolved(model.isResolved());
            m.setHasDirectData(((KIMModel) model).hasDatasource());
            m.setHasDirectObjects(((KIMModel) model).hasObjectSource());

            m.setMinSpatialScaleFactor(model.getMetadata()
                    .getInt(IMetadata.IM_MIN_SPATIAL_SCALE, ISpatialExtent.MIN_SCALE_RANK));
            m.setMaxSpatialScaleFactor(model.getMetadata()
                    .getInt(IMetadata.IM_MAX_SPATIAL_SCALE, ISpatialExtent.MAX_SCALE_RANK));
            m.setMinTimeScaleFactor(model.getMetadata()
                    .getInt(IMetadata.IM_MIN_TEMPORAL_SCALE, ITemporalExtent.MIN_SCALE_RANK));
            m.setMaxTimeScaleFactor(model.getMetadata()
                    .getInt(IMetadata.IM_MAX_TEMPORAL_SCALE, ITemporalExtent.MAX_SCALE_RANK));

            m.setPrimaryObservable(first);
            first = false;

            m.setMetadata(KLAB.MFACTORY.adapt(model.getMetadata(), Metadata.class));

            ret.add(m);
        }
        return ret;
    }

    public static String generateObjectModelSource(IConcept observable, String objectSource, Map<String, IObserver> attributes, String nameAttribute) {

        String ret = "model each " + objectSource + " as "
                + Observables.getDeclaration(observable);

        if (nameAttribute != null || (attributes != null && attributes.size() > 0)) {
            ret += "\n   interpret\n";
            if (nameAttribute != null) {
                ret += "      " + nameAttribute.toUpperCase() + " as im:name"
                        + (attributes != null && attributes.size() > 0 ? "," : "") + "\n";
            }
            if (attributes != null) {
                int n = 0;
                for (String s : attributes.keySet()) {
                    ret += "      " + s.toUpperCase() + " as "
                            + KIM.getDefinition(attributes.get(s))
                            + (n == attributes.size() - 1 ? "" : ",") + "\n";
                    n++;
                }
            }
        }

        ret += ";";

        return ret;
    }
    
}
