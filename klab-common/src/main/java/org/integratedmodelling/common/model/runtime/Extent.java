/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.Iterator;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

public abstract class Extent extends org.integratedmodelling.common.knowledge.Extent {

    IConcept       domain;
    protected long multiplicity;

    @Override
    public long getValueCount() {
        return multiplicity;
    }

    @Override
    public IObserver getObserver() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getMultiplicity() {
        return multiplicity;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isTemporal() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isSpatial() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConstant() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isDynamic() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void addChangeListener(ChangeListener listener) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public IExtent intersection(IExtent other) throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public IExtent union(IExtent other) throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean contains(IExtent o) throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean overlaps(IExtent o) throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean intersects(IExtent o) throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public double getCoveredExtent() {
        return 0;
    }

//    @Override
//    public IProperty getCoverageProperty() {
//        return null;
//    }

    @Override
    public IExtent collapse() {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean isCovered(int stateIndex) {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws KlabException {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean isConsistent() {
        throw new KlabRuntimeException("illegal operation on client object");
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public Object getValue(int index) {
        return getExtent(index);
    }

    @Override
    public Iterator<Object> iterator(Index index) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int[] getDimensionSizes() {
        // TODO Auto-generated method stub
        return new int[] { 1 };
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        return new int[] { linearOffset };
    }

}
