/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.common.vocabulary.NS;
import org.jgrapht.graph.DefaultDirectedGraph;

/**
 * @author Ferd
 *
 */
public class Structure extends DefaultDirectedGraph<IObservation, IRelationship>implements IStructure {

    boolean           neverSeen = true;
    boolean           changed   = true;
    ISubject          subject   = null;
    Set<IObservation> added     = new HashSet<>();
    Set<IObservation> removed   = new HashSet<>();

    // holds the subjects and relationships before link() when this is created from the network.
    HashMap<String, ISubject> subCatalog = new HashMap<>();
    List<?>                   relCatalog = new ArrayList<>();

    public Structure(ISubject subject) {
        super(Relationship.class);
        this.subject = subject;
    }

    private static final long serialVersionUID = 204382567041257188L;

    @Override
    public Set<IObservation> getObservations() {
        return this.vertexSet();
    }

    @Override
    public Set<IRelationship> getRelationships() {
        return this.edgeSet();
    }

    @Override
    public Set<IRelationship> getOutgoingRelationships(IObservation observation) {
        return this.outgoingEdgesOf(observation);
    }

    @Override
    public Set<IRelationship> getIncomingRelationships(IObservation observation) {
        return this.incomingEdgesOf(observation);
    }

    public void addSubject(ISubject subject) {
        addVertex(subject);
    }

    @Override
    public void link(ISubject from, ISubject to, IRelationship relationship) {
        if (addVertex(from)) {
            changed = added.add(from);
        }
        if (addVertex(to)) {
            changed = added.add(to);
        }
        if (relationship != null) {
            added.add(relationship);
            addEdge(from, to, relationship);
            changed = true;
        }
    }

    @Override
    public void removeObservation(IObservation observation) {
        if (removeVertex(observation)) {
            removed.add(observation);
        }
        /*
         * TODO must add the removed relationships to the removed set. Not sure how that's
         * done with jGrapht.
         */
        changed = true;
    }

    @Override
    public ISubject getSubject() {
        return subject;
    }


    /**
     * Return whether any of the relationships in this context is a flow relationship. This will
     * be used to enable flow-specific displays.
     * 
     * @return true if at least one relationship is a flow
     */
    public boolean hasFlows() {
        if (!NS.synchronize()) {
            return false;
        }
        for (IRelationship r : getRelationships()) {
            if (r.getObservable().getSemantics().is(NS.FLOW)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Non-API: return the flows as a list of (from, to, weight) lists, suitable for display.
     * @return
     */
    public List<List<Object>> getFlows() {
        
        List<List<Object>> ret = new ArrayList<>();
        
        Random ra = new Random();
        
        for (IRelationship r : getRelationships()) {
            if (r.getObservable().getSemantics().is(NS.FLOW)) {
                List<Object> flw = new ArrayList<>();
                flw.add(r.getSource().getName());
                flw.add(r.getTarget().getName());
                // TODO actual throughput!
                flw.add(ra.nextDouble() + .5);
                ret.add(flw);
            }
        }
        
        return ret;
    }
    
    public void append(IRelationship observation) {
        ((Relationship) observation).setSubject(this.subject);
        addVertex(observation.getSource());
        addVertex(observation.getTarget());
        addEdge(observation.getSource(), observation.getTarget(), observation);
    }

}
