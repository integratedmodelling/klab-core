/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.agents.IObservationTask;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.joda.time.DateTimeConstants;

/**
 * Should replace the transitions in other packages when working.
 * 
 * @author ferdinando.villa
 *
 */
public class Transition extends Scale implements ITransition, NetworkSerializable {

    private IAgentState                                                 agentState;
    private boolean                                                     agentSurvives;
    private final Collection<Pair<ITimeInstant, IObservationGraphNode>> observationDependencies = new LinkedList<>();
    private final Collection<IObservationTask>                          furtherObservationTasks = new LinkedList<>();
    private IScale                                                      scale                   = null;
    private ITimePeriod                                                 period                  = null;
    private HashSet<IObservation>                                       modified                = new HashSet<>();
    int                                                                 timeIndex               = -1;

    public static String asString(ITransition transition) {
        // if the scale is known, this should be enough to reconstruct the transition for the time being.
        return "" + transition.getTimeIndex();
    }

    public Transition(IScale scale, IAgentState agentState, boolean agentSurvives) {
        this.agentState = agentState;
        this.agentSurvives = agentSurvives;

        /*
         * TODO recreate the scale using all extents and the agentState.getTimePeriod for time. Keep
         * pointer to parent scale.
         */
        IExtent[] exts = new IExtent[scale.getExtentCount()];
        int i = 0;
        for (IExtent ext : scale) {
            if (ext instanceof ITemporalExtent && agentState != null) {
                exts[i++] = agentState.getTimePeriod();
            } else {
                exts[i++] = ext;
            }
        }
        initialize(exts);

        this.scale = scale;
    }
    
    public double getDays() {
        return (double)(getMilliseconds())/(double)DateTimeConstants.MILLIS_PER_DAY;
    }

    public double getHours() {
        return (double)(getMilliseconds())/(double)DateTimeConstants.MILLIS_PER_HOUR;
    }

    public double getMinutes() {
        return (double)(getMilliseconds())/(double)DateTimeConstants.MILLIS_PER_MINUTE;
    }

    public double getSeconds() {
        return (double)(getMilliseconds())/(double)DateTimeConstants.MILLIS_PER_SECOND;
    }

    public double getWeeks() {
        return (double)(getMilliseconds())/(double)DateTimeConstants.MILLIS_PER_WEEK;
    }

    public double getYears() {
        return (double)(getMilliseconds())/(double)(DateTimeConstants.MILLIS_PER_DAY * 365);
    }
    
    public long getMilliseconds() {
        ITimePeriod peri = getTransitionTimePeriod();
        return peri.getEnd().getMillis() - peri.getStart().getMillis();
    }

    /*
     * create a transition without an explicit agent state.
     */
    public Transition(IScale scale, int timeOffset, boolean agentSurvives) {

        /*
         * should never get a null time, but you know.
         */
        period = (ITimePeriod) (scale.getTime() == null ? null : scale.getTime().getExtent(timeOffset));
        this.agentSurvives = scale.getTime() != null && scale.getTime().getMultiplicity() < (timeOffset - 1);

        // prevents lots of calculations later.
        this.timeIndex = timeOffset;

        /*
         * recreate the scale using all extents and the agentState.getTimePeriod for time. Keep
         * pointer to parent scale.
         */
        IExtent[] exts = new IExtent[scale.getExtentCount()];
        int i = 0;
        for (IExtent ext : scale) {
            if (ext instanceof ITemporalExtent) {
                exts[i++] = period;
            } else {
                exts[i++] = ext;
            }
        }
        initialize(exts);

        this.scale = scale;
    }

    @Override
    public ITemporalExtent getTime() {
        return getTransitionTimePeriod();
    }

    @Override
    public ISpatialExtent getSpace() {
        return scale.getSpace();
    }

    /**
     * TODO proxy all other appropriate methods as a delegate to the wrapped scale
     * @return the period
     */

    // @Override
    public ITimePeriod getTransitionTimePeriod() {
        return agentState == null ? period : agentState.getTimePeriod();
    }

    @Override
    public boolean agentSurvives() {
        return agentSurvives;
    }

    @Override
    public IAgentState getAgentState() {
        return agentState;
    }

    @Override
    public void addFurtherObservationTask(IObservationTask task) {
        furtherObservationTasks.add(task);
    }

    @Override
    public Collection<IObservationTask> getFurtherObservationTasks() {
        return furtherObservationTasks;
    }

    public void addObservationDependency(ITimeInstant time, IObservationGraphNode node) {
        Pair<ITimeInstant, IObservationGraphNode> pair = new Pair<>(time, node);
        observationDependencies.add(pair);
    }

    @Override
    public Collection<Pair<ITimeInstant, IObservationGraphNode>> getObservationDependencies() {
        return observationDependencies;
    }

    @Override
    public boolean isLast() {
        // the last transition has either no time or the last time.
        return scale.getTime() == null || scale.getTime().getEnd().equals(this.getTime().getEnd());
    }

    @Override
    public String toString() {
        if ((agentState == null || agentState.getTimePeriod() == null) && period == null) {
            return "|";
        }
        ITimePeriod peri = getTransitionTimePeriod();
        return getTimeIndex() + ": " + peri.getStart().getMillis() + "|" + peri.getEnd().getMillis();
    }

    @Override
    public int getTimeIndex() {

        // if set explicitly, make it easy.
        if (timeIndex >= 0) {
            return timeIndex;
        }

        /*
         * TODO/FIXME this will silently produce bull if called on irregular timegrids, which we
         * will probably never use.
         * 
         * TODO the verbosity is due to debugging because the original expression (commented out) 
         * originally caused seemingly random rounding errors with values being higher by 1 and
         * transitions being skipped, to disastrous results. I don't see any reason why or why
         * this version should work, but this does not cause any errors so it stays for now.
         * 
         */
        long trstart = getTime().getStart().getMillis();
        long scstart = scale.getTime().getStart().getMillis();
        long relstart = trstart - scstart;
        long trduration = getTime().getStep().getMilliseconds();
        long trindex = relstart / trduration;
        int ret = (int) trindex;
        return /*(int) ((getTime().getStart().getMillis() - scale.getTime().getStart().getMillis())
               / scale.getTime().getStep().getMilliseconds());*/ret;
    }

    @Override
    public ITransition previous() {
        if (getTimeIndex() == 1) {
            return null;
        }
        return new Transition(scale, getTimeIndex() - 1, agentSurvives);
    }

    @Override
    public ITransition previous(Object locator) {
        if (locator instanceof Integer) {
            int t = getTimeIndex();
            int f = t - (Integer) locator;
            if (f == 0) {
                return null;
            } else if (f < scale.getTime().getMultiplicity()) {
                return new Transition(scale, getTimeIndex() + (Integer) locator, agentSurvives);
            }
        }
        throw new KlabRuntimeException("illegal transition locator");
    }

    @Override
    public ITransition next() {
        if (isLast()) {
            throw new KlabRuntimeException("contextualization is finished: cannot ask for the next transition");
        }
        return new Transition(scale, getTimeIndex() + 1, agentSurvives);
    }

    public void addModifiedObservation(IObservation obs) {
        modified.add(obs);
    }

    @Override
    public Collection<IObservation> getModifiedObservations() {
        return modified;
    }

    @Override
    public int getDimensionCount() {
        return 1;
    }

    @Override
    public boolean isAll() {
        return false;
    }

    @Override
    public String asText() {
        return asString(this);
    }

    @Override
    public double getWeight() {
        return 1.0;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Transition.class)) {
            throw new KlabRuntimeException("cannot serialize a Transition to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Transition ret = new org.integratedmodelling.common.beans.Transition();

        ret.setStartTime(this.getTime().getStart().getMillis());
        ret.setEndTime(this.getTime().getEnd().getMillis());
        ret.setTimeIndex(this.getTimeIndex());
        
        return (T) ret;
    }
    
    @Override
    public IConcept getDomainConcept() {
        return KLAB.c(NS.TIME_DOMAIN);
    }
}
