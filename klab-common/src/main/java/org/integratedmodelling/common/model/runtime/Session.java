package org.integratedmodelling.common.model.runtime;

import java.io.IOException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Date;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IResourceConfiguration.StaticResource;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.auth.User;
import org.integratedmodelling.common.beans.Context;
import org.integratedmodelling.common.beans.Notification;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.runtime.Task;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.Monitorable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.monitoring.Notifiable;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * @author ferdinando.villa
 *
 */
public abstract class Session extends Notifiable implements ISession, Monitorable, NetworkSerializable {

    private static final int MAX_DEFAULT_CONTEXTS = 100;

    protected String                id;
    protected IUser                 user;
    protected boolean               isReopenable;
    protected Deque<IContext>       contexts     = new ArrayDeque<IContext>(MAX_DEFAULT_CONTEXTS);
    protected Map<String, IContext> contextsById = new HashMap<>();
    protected long                  startTime    = new Date().getTime();
    protected long                  lastChecked;
    protected IMonitor              monitor;
    protected List<Listener>        listeners    = new ArrayList<>();
    protected boolean               localFilesystem = false;

    /*
     * these are notifications for tasks and contexts that have been received before the
     * receivers themselves have been notified. They are processed and removed as soon as
     * the corresponding receivers appear.
     */
    Map<String, List<Notification>> forwardTaskNotifications    = new ConcurrentHashMap<>();
    Map<String, List<Notification>> forwardContextNotifications = new ConcurrentHashMap<>();

    /**
     * 
     */
    public Session() {
    }


    @Override
    public String getSecurityKey() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends IIdentity> T getParentIdentity(Class<? extends IIdentity> type) {
        // TODO Auto-generated method stub
        return null;
    }
    /**
     * 
     * @param user a valid user
     */
    public Session(IUser user) {
        this.user = user;
        this.id = NameGenerator.shortUUID();
        this.lastChecked = startTime;
        this.monitor = ((Monitor) KLAB.ENGINE.getMonitor()).get(this);
        // for notifications
        this.addMonitor(this.monitor);
    }

    /**
     * Get the monitor for this session, whose getSession() method will return this.
     * 
     * @return the monitor. Never null.
     */
    public IMonitor getMonitor() {
        return monitor;
    }

    @Override
    public void close() throws IOException {
        // TODO Auto-generated method stub
        if (!isReopenable()) {

        }
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public List<IContext> getContexts() {
        return new ArrayList<IContext>(contexts);
    }

    @Override
    public boolean isExclusive() {
        return KLAB.ENGINE.getResourceConfiguration()
                .isAuthorized(StaticResource.ENGINE_LOCK, getUser(), null);
    }

    @Override
    public boolean isReopenable() {
        return isReopenable;
    }

    /**
     * @param ctx
     */
    public void pushContext(IContext ctx) {
        if (contexts.size() == MAX_DEFAULT_CONTEXTS) {
            IContext c = contexts.pollLast();
            contextsById.remove(c.getId());
        }
        contexts.push(ctx);
        contextsById.put(ctx.getId(), ctx);
    }

    /**
     * @return time of last check
     */
    public long getLastChecked() {
        return lastChecked;
    }

    /**
     * @param time
     */
    public void setLastChecked(long time) {
        lastChecked = time;
    }

    /**
     * Get a specific context. 
     * @param id
     * @return the context with the specified id, or null if not found.
     */
    public IContext getContext(String id) {
        return contextsById.get(id);
    }

    @Override
    public void acceptNotification(Notification notification) {

        boolean done = false;
        if (notification.getContext() != null) {
            
            boolean isNew = false;
            IContext ctx = contextsById.get(notification.getContext().getId());
            if (ctx == null) {
                pushContext((ctx = KLAB.MFACTORY.adapt(notification
                        .getContext(), org.integratedmodelling.common.client.runtime.Context.class)));
                ((org.integratedmodelling.common.client.runtime.Context) ctx).setMonitor(this.monitor);
                List<Notification> forwardNotifications = forwardContextNotifications.get(ctx.getId());
                if (forwardNotifications != null) {
                    for (Notification fn : forwardNotifications) {
                        ((org.integratedmodelling.common.client.runtime.Context) ctx).acceptNotification(fn);
                    }
                    forwardContextNotifications.remove(ctx.getId());
                }

                isNew = true;
                Environment.get().setContext(ctx);
            
            } else {
                ((AbstractContext) ctx).acceptDelta(notification.getContext());
            }


            if (notification.getNotificationClass() != null
                    && notification.getNotificationClass().startsWith(Messages.TASK_MESSAGE_PREFIX)) {
                ITask task = ((AbstractContext) ctx).getTask(notification.getTaskId());
                if (task != null) {
                    ((AbstractTask) task).acceptNotification(notification);
                    for (Listener listener : listeners) {
                        listener.taskEvent(task, false);
                    }
                }
            }
            for (Listener listener : listeners) {
                listener.contextEvent(ctx, isNew);
            }
            done = true;
        }

        if (notification.getTask() != null) {

            /*
             * see if we have it - we shouldn't
             */
            IContext ctx = contextsById.get(notification.getTask().getContextId());
            if (ctx != null) {
                boolean isNew = false;
                ITask task = ((AbstractContext) ctx).getTask(notification.getTaskId());
                if (task == null) {
                    ((AbstractContext) ctx)
                            .addTask((task = KLAB.MFACTORY.adapt(notification.getTask(), Task.class)));
                    ((Task) task).setMonitor(((org.integratedmodelling.common.client.runtime.Context) ctx)
                            .getMonitor());
                    List<Notification> forwardNotifications = forwardTaskNotifications.get(task.getTaskId());
                    if (forwardNotifications != null) {
                        for (Notification fn : forwardNotifications) {
                            ((Task) task).acceptNotification(fn);
                        }
                        forwardTaskNotifications.remove(task.getTaskId());
                    }

                    isNew = true;
                    Environment.get().setContext(ctx);

                } else {
                    ((AbstractTask) task).acceptDelta(notification.getTask());
                }
                for (Listener listener : listeners) {
                    listener.taskEvent(task, isNew);
                }
            }
            done = true;
        }

        if (!done) {
            if (notification.getTaskId() != null) {
                IContext ctx = contextsById.get(notification.getContextId());
                if (ctx != null) {
                    ITask task = ((AbstractContext) ctx).getTask(notification.getTaskId());
                    if (task != null) {
                        ((AbstractTask) task).acceptNotification(notification);
                    } else {
                        addForwardTaskNotification(notification);
                    }
                    for (Listener listener : listeners) {
                        listener.taskEvent(task, false);
                    }
                }
            } else if (notification.getContextId() != null) {
                IContext ctx = contextsById.get(notification.getTask().getContextId());
                if (ctx != null) {
                    ((AbstractContext) ctx).acceptNotification(notification);
                } else {
                    addForwardContextNotification(notification);
                }
                for (Listener listener : listeners) {
                    listener.contextEvent(ctx, false);
                }
            } else {
                super.acceptNotification(notification);
            }
        }
    }

    private void addForwardContextNotification(Notification notification) {

        synchronized (forwardContextNotifications) {
            List<Notification> fnots = forwardContextNotifications.get(notification.getContextId());
            if (fnots == null) {
                fnots = new ArrayList<>();
                forwardContextNotifications.put(notification.getContextId(), fnots);
            }
            fnots.add(notification);
        }
    }

    private void addForwardTaskNotification(Notification notification) {

        synchronized (forwardTaskNotifications) {
            List<Notification> fnots = forwardTaskNotifications.get(notification.getTaskId());
            if (fnots == null) {
                fnots = new ArrayList<>();
                forwardTaskNotifications.put(notification.getTaskId(), fnots);
            }
            fnots.add(notification);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Session.class)) {
            throw new KlabRuntimeException("cannot serialize a Session to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Session ret = new org.integratedmodelling.common.beans.Session();

        ret.setId(id);
        ret.setReopenable(isReopenable);
        ret.setStartTime(startTime);
        ret.setUser(((User) getUser()).getProfile());
        ret.setUserAuthToken(user.getSecurityKey());
        ret.setLocalFilesystem(localFilesystem);
        for (IContext context : getContexts()) {
            ret.getContexts().add((Context) ((NetworkSerializable) context).serialize(Context.class));
        }
        return (T) ret;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = ((Monitor) monitor).get(this);
    }

    @Override
    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    public void setLocalFilesystem(boolean mode) {
        this.localFilesystem = mode;
    }
    
    public boolean isLocalFilesystem() {
        return this.localFilesystem;
    }
    
}
