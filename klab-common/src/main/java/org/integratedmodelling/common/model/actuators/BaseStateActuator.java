/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.interfaces.actuators.IStateActuator;
import org.integratedmodelling.common.kim.KIMObserver;
import org.integratedmodelling.exceptions.KlabException;

public abstract class BaseStateActuator extends Actuator implements IStateActuator {

	public BaseStateActuator(IObservation observation, IModel model) {
		super(observation, model);
	}

	List<IStateContextualizer> contextualizers;
    List<IStateContextualizer> validators;
	protected HashMap<String, IObserver> outputs = new HashMap<String, IObserver>();
	protected HashMap<String, IObserver> inputs = new HashMap<String, IObserver>();
	Set<String> inputKeys = new HashSet<String>();
	Set<String> outputKeys = new HashSet<String>();
	protected IResolutionScope scope;

	@Override
	public void define(String name, IObserver observer, IActiveDirectObservation contextObservation,
			IResolutionScope scope, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs,
			IMonitor monitor) throws KlabException {

		this.monitor = monitor;
		this.scope = scope;
		this.context = contextObservation;
		this.setName(name);

		notifyObserver(observer);
		
		for (String ikey : expectedInputs.keySet()) {
			notifyInput(expectedInputs.get(ikey), expectedInputs.get(ikey).getObserver(), ikey);
		}

		for (String okey : expectedOutputs.keySet()) {
			notifyOutput(expectedOutputs.get(okey), expectedOutputs.get(okey).getObserver(), okey);
		}

		int i = 0;
		int nC = contextualizers.size();
		for (IStateContextualizer contextualizer : contextualizers) {
			contextualizer.define(name, observer, contextObservation, scope, expectedInputs, expectedOutputs,
					i == nC - 1, monitor);
			i++;
		}
	}
	
	public boolean isConstant() {
		return contextualizers != null && contextualizers.size() > 0 && contextualizers.get(0).isConstant();
	}

	/**
	 * Return the input formal names.
	 * 
	 * @return
	 */
	protected Collection<String> getInputKeys() {
		return inputKeys;
	}

	/**
	 * Return all the output formal names. First one will be the main
	 * observable, equal to getName().
	 * 
	 * @return
	 */
	protected Collection<String> getOutputKeys() {
		return outputKeys;
	}

	/**
	 * Return the observer for the passed output keys.
	 * 
	 * @param key
	 * @return
	 */
	protected IObserver getObserverForOutput(String key) {
		return outputs.get(key);
	}

	/**
	 * This one will return the observer for the passed input key.
	 * 
	 * @param key
	 * @return
	 */
	protected IObserver getObserverForInput(String key) {
		return inputs.get(key);
	}

	@Override
	public void setContextualizers(List<IStateContextualizer> contextualizers) {
		this.contextualizers = new ArrayList<>(contextualizers);
	}

	/**
	 * Use this one to intercept the expected outputs.
	 * 
	 * @param observable
	 * @param observer
	 * @param key
	 * @param isMainObservable
	 * @throws KlabException
	 */
	public void notifyOutput(IObservableSemantics observable, IObserver observer, String key)
			throws KlabException {
        outputKeys.add(key);
        IObserver oob = ((KIMObserver) observer).getRepresentativeObserver();
        outputs.put(key, oob);
	}

	/**
	 * Use this one to intercept the available dependencies when process() is
	 * called.
	 * 
	 */
	@Override
	public void notifyInput(IObservableSemantics observable, IObserver observer, String key)
			throws KlabException {
	    inputKeys.add(key);
        IObserver oob = ((KIMObserver) observer).getRepresentativeObserver();
	    inputs.put(key, oob);
	}


	@Override
	public final void notifyObserver(IObserver observer) throws KlabException {
		outputKeys.add(getName());
		IObserver oob = ((KIMObserver) observer).getRepresentativeObserver();
		outputs.put(getName(), oob);
		notifyOutput(observer.getObservable(), oob, getName());
	}

	protected IDirectObservation getContext() {
		return context;
	}

}
