package org.integratedmodelling.common.model.runtime;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.resolution.IDataflow;
import org.integratedmodelling.api.modelling.resolution.IResolution;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.beans.ModelArtifact;
import org.integratedmodelling.common.beans.Notification;
import org.integratedmodelling.common.beans.Task;
import org.integratedmodelling.common.beans.generic.Graph;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.runtime.Dataflow;
import org.integratedmodelling.common.client.runtime.Resolution;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Notifiable;

/**
 * @author ferdinando.villa
 */
public abstract class AbstractTask extends Notifiable implements ITask {

    protected Status      status = Status.QUEUED;
    protected String      id;
    protected long        start;
    protected long        end    = -1;
    protected String      description;
    protected IMonitor    monitor;
    protected ITransition lastTransition;
    protected String      exception;
    protected IDataflow   dataflow;
    protected IResolution resolution;
    protected ITask       parent;
    private boolean       runTask;

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public String getTaskId() {
        return id;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public long getStartTime() {
        return start;
    }

    @Override
    public long getEndTime() {
        return end;
    }

    public ITransition getCurrentTransition() {
        return lastTransition;
    }

    /**
     * @param task
     */
    public void acceptDelta(Task task) {

        this.status = Status.valueOf(task.getStatus());
        this.end = task.getEndTime();
        this.runTask = task.isRunTask();

        if (task.getCurrentTransition() != null) {
            this.lastTransition = new Transition(getContext().getSubject().getScale(), task
                    .getCurrentTransition().getTimeIndex(), true);
        }
        this.exception = task.getException();
    }

    @Override
    public void acceptNotification(Notification notification) {

        Environment.get().setFocusTask(this);

        if (notification.getNotificationClass() != null
                && notification.getNotificationClass().equals(Messages.TASK_FAILED)) {
            this.status = Status.ERROR;
            if (monitor != null) {
                String exception = "";
                monitor.error(description + " terminated with errors"
                        + (exception == null ? "" : (": " + exception)));
            }
        } else if (notification.getNotificationClass() != null
                && notification.getNotificationClass().equals(Messages.TASK_INTERRUPTED)) {
            this.status = Status.INTERRUPTED;
            if (monitor != null) {
                monitor.warn(description + " interrupted by user");
            }
        } else if (notification.getNotificationClass() != null
                && notification.getNotificationClass().equals(Messages.TASK_FINISHED)) {
            this.status = Status.FINISHED;
            this.end = System.currentTimeMillis();
            if (monitor != null) {
                if (this.runTask) {
                    ((AbstractContext) monitor.getContext()).isFinished = true;
                }
                monitor.info(description + " observed successfully", Messages.INFOCLASS_COMPLETED);
            }
        } else {
            super.acceptNotification(notification);
        }
    }

    @Override
    protected void receive(IModelBean object) {
        if (object instanceof Graph) {
            if (((Graph) object).getType().equals(Messages.GRAPH_DATAFLOW)) {
                this.dataflow = KLAB.MFACTORY.adapt(object, Dataflow.class);
            } else if (((Graph) object).getType().equals(Messages.GRAPH_RESOLUTION)) {
                this.resolution = KLAB.MFACTORY.adapt(object, Resolution.class);
            }
        } else if (object instanceof ModelArtifact) {
            ModelArtifact a = (ModelArtifact) object;
            IObservation obs = ((AbstractContext) getContext()).findWithId(a.getObservationId());
            if (obs instanceof DirectObservation) {
                ((DirectObservation) obs).addArtifact(a);
            } else if (obs == null) {
                // put away for later
                ((AbstractContext) getContext()).registerArtifact(a);
            }
        }
        super.receive(object);
    }

    @Override
    public IDataflow getDataflow() {
        return this.dataflow;
    }

    @Override
    public IResolution getResolution() {
        return this.resolution;
    }
    
    public ITask getParentTask() {
        // TODO not set at the moment.
        return parent;
    }
    
    @Override
    public String getSecurityKey() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public <T extends IIdentity> T getParentIdentity(Class<? extends IIdentity> type) {
        // TODO Auto-generated method stub
        return null;
    }
}
