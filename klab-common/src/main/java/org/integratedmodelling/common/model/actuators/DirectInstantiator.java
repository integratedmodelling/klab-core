package org.integratedmodelling.common.model.actuators;

import java.util.Collection;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.common.interfaces.actuators.IInstantiator;
import org.integratedmodelling.exceptions.KlabException;

public abstract class DirectInstantiator<T extends IActiveDirectObservation> extends DirectActuator<T> implements IInstantiator<T> {
    
    public DirectInstantiator(IObservation observation, IModel model) {
        super(observation, model);
    }

    /**
     * Called by the dataflow before resolution of the direct observation (or each of the
     * instantiated ones). 
     * 
     * @throws KlabException
     * @returns any observation made by action code
     */
    abstract public Collection<IObservation> performPreResolutionActions(IActiveDirectObservation observation) throws KlabException;

    /**
     * Called by the dataflow after resolution of the direct observation (or each of the
     * instantiated ones).
     * 
     * @throws KlabException
     * @returns any observation made by action code
     */
    abstract public Collection<IObservation> performPostResolutionActions(IActiveDirectObservation observation) throws KlabException;
    
}
