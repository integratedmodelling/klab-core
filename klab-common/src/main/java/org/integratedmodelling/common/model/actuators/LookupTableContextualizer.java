/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.data.ITableSet;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.lang.INamespaceQualified;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IValueResolver;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.TableFactory;
import org.integratedmodelling.common.model.runtime.AbstractStateContextualizer;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * This one serves multiple purposes: it's created by the resolver when a 'table'
 * statement is found, and also linked to the lookuptable.import function. When the second
 * is used, it will also contain an external tableset and provide input/output matching
 * after caching the data contents to a persistent map in the filesystem.
 * 
 * @author ferdinando.villa
 *
 */
@Prototype(
        id = "lookuptable.import",
        args = {
                "file",
                Prototype.TEXT,
                "sheet",
                Prototype.TEXT,
                "input-column",
                Prototype.TEXT,
                "output-column",
                Prototype.TEXT },
        returnTypes = { NS.LOOKUP_TABLE, NS.STATE_CONTEXTUALIZER })
public class LookupTableContextualizer extends AbstractStateContextualizer
        implements IFunctionCall, IExpression, INamespaceQualified, IValueResolver {

    /**
     * params for the inline form created from the language-embedded table
     */
    ITable             table;
    List<String>       args        = new ArrayList<>();
    int                startLine, endLine;
    int                columnIndex = -1;
    private boolean    reported;
    private INamespace namespace;

    /*
     * params specific to the lookuptable.import version
     */
    File                file;
    String              sheet;
    String              inputCol;
    String              outputCol;
    ITableSet           tableset;
    Map<String, Object> mapping;
    boolean             initialized;

    /*
     * Do not use this constructor: only for usage as a IExpression, generated by the
     * models that use it to call eval() on it and return a usable instance.
     */
    public LookupTableContextualizer() {
        super(null);
    }

    public LookupTableContextualizer(File table, String sheet, String inputColumn, String outputColumn,
            INamespace namespace) {
        super(null);
        this.file = table;
        this.sheet = sheet;
        this.inputCol = inputColumn;
        this.outputCol = outputColumn;
        this.namespace = namespace;
    }

    /**
     * This one is called to produce the function call that is stored with the observer.
     * When called, this should produce a copy of itself, with the same table, arguments
     * and namespace.
     * 
     * @param lookupTable
     * @param namespace
     * @param args
     */
    public LookupTableContextualizer(ITable lookupTable, INamespace namespace, List<String> args) {

        super(null);

        this.table = lookupTable;
        this.namespace = namespace;

        for (int i = 0; i < args.size(); i++) {
            if (args.get(i).equals("?")) {
                this.columnIndex = i;
            } else {
                this.args.add(args.get(i));
            }
        }
        if (this.columnIndex < 0) {
            this.columnIndex = this.args.size();
        }
    }

    public void initialize() throws KlabException {

        if (!this.initialized && this.file != null) {

            this.tableset = TableFactory.open(this.file);
            if (this.tableset == null) {
                throw new KlabIOException("lookup table: file " + this.file
                        + " is not a recognized table format");
            }
            this.table = this.tableset.getTable(this.sheet);
            if (this.table == null) {
                throw new KlabIOException("lookup table: table " + this.sheet
                        + " not found in imported file");
            }
            this.mapping = this.table.getMapping(this.inputCol, this.outputCol);
            if (this.mapping == null) {
                throw new KlabIOException("lookup table: mapping " + this.inputCol + " -> " + this.outputCol
                        + " cannot be established: check column names");
            }
        }
        this.initialized = true;
    }

    // @Override
    public Map<String, Object> process(int stateIndex, Map<String, Object> inputs, ITransition transition)
            throws KlabException {

        Map<String, Object> ret = new HashMap<>();

        initialize();
        Object value = null;

        if (this.mapping != null) {

            if (getInputKeys().size() != 1) {
                throw new KlabValidationException("lookup table: exactly one mapping is allowed");
            }

            String s = getInputKeys().iterator().next();
            value = (s == null || inputs.get(s) == null) ? null : this.mapping.get(this.table
                    .sanitizeKey(inputs.get(s)));

        } else {

            Object[] match = new Object[this.args.size()];
            for (int i = 0; i < this.args.size(); i++) {
                match[i] = inputs.get(this.args.get(i));
            }

            List<Object> val = null;
            try {
                val = this.table.lookup(this.columnIndex, match);
            } catch (Exception e) {
                if (!this.reported) {
                    monitor.error(e);
                    this.reported = true;
                }
            }

            value = val == null ? null : (val.size() > 0 ? val.get(0) : null);

            /*
             * that's cool, man.
             */
            if (value instanceof IExpression) {
                value = ((IExpression) value).eval(inputs, monitor, (IConcept[]) null);
            }
        }

        for (String s : getOutputKeys()) {
            ret.put(s, value);
        }

        return ret;
    }

    @Override
    public int getFirstLineNumber() {
        return this.startLine;
    }

    @Override
    public int getLastLineNumber() {
        return this.endLine;
    }

    @Override
    public String getId() {
        return "lookuptable.import";
    }

    @Override
    public IPrototype getPrototype() {
        return ServiceManager.get().getFunctionPrototype(getId());
    }

    @Override
    public Map<String, Object> getParameters() {
        return new HashMap<>();
    }

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws KlabException {

        if (parameters.containsKey("file")) {
            File file = new File(getNamespace().getProject().getLoadPath() + File.separator
                    + parameters.get("file").toString());
            String table = parameters.containsKey("sheet") ? parameters.get("sheet").toString() : null;
            String inputcol = parameters.get("input-column").toString();
            String outputcol = parameters.get("output-column").toString();

            if (!file.exists() || !file.isFile() || !file.canRead()) {
                throw new KlabIOException("lookup table: file " + file
                        + " does not exist or is not readable");
            }
            return new LookupTableContextualizer(file, table, inputcol, outputcol, this.namespace);
        }

        /*
         * If we get here, we've been called by ModelFactory.callFunction(), because we're
         * also a function call created by parsing a KIMLookupTable.
         */
        return new LookupTableContextualizer(this.table, this.namespace, this.args);
    }

    @Override
    public boolean returns(IConcept resultType) {
        return resultType.is(KLAB.c(NS.LOOKUP_TABLE)) || resultType.is(KLAB.c(NS.STATE_CONTEXTUALIZER));
    }

    @Override
    public INamespace getNamespace() {
        return this.namespace;
    }

    @Override
    public void setNamespace(INamespace namespace) {
        this.namespace = namespace;
    }

    @Override
    public Map<String, Object> initialize(int index, Map<String, Object> inputs) throws KlabException {
        return process(index, inputs, ITransition.INITIALIZATION);
    }

    @Override
    public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
            throws KlabException {
        return process(index, inputs, transition);
    }

    @Override
    public boolean isProbabilistic() {
        // for now - may later enable distributions as functions and check them.
        return false;
    }

    @Override
    public String getName() {
        return namespace.getId() + ":" + table.getName();
    }

    @Override
    public String getLabel() {
        return "lookup " + table.getName();
    }

}
