/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAction.Trigger;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.ISubjectContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.interfaces.actuators.ISubjectActuator;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.model.runtime.AbstractContext;
import org.integratedmodelling.common.model.runtime.Transition;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.exceptions.KlabException;

public class SubjectContextualizerActuator extends DirectActuator<IActiveSubject> implements ISubjectActuator {

	private ISubjectContextualizer contextualizer;
	private Map<String, IState> statesToInputs;
	private Set<IState> toFlush = new HashSet<>();

	public SubjectContextualizerActuator(IActiveSubject subject, IModel model, ISubjectContextualizer ds,
			List<IAction> actions, IMonitor monitor) {
		super(subject, model);
		this.contextualizer = ds;
		for (IAction action : actions) {
			this.addAction(action);
		}
        if (getActions(Trigger.EVENT).size() > 0) {
            subject.getContext().getEventBus().subscribe(subject, this);
        }
	}

	@Override
	public String toString() {
		return contextualizer == null ? "bare subject"
				: Path.getLast(contextualizer.getClass().getCanonicalName(), '.');
	}

	protected Collection<String> getInputKeys() {
		return statesToInputs.keySet();
	}

	protected IState getInputState(IConcept observable) {
		return statesToInputs.get(observable);
	}

	protected Collection<IState> getInputStates() {
		return statesToInputs.values();
	}

	@Override
	public Map<String, IObservation> initialize(IActiveSubject subject, IActiveDirectObservation context,
			IResolutionScope resolutionContext, IMonitor monitor) throws KlabException {

		this.context = context;
		Map<String, IObservableSemantics> ous = new HashMap<>();
		Map<String, IObservation> ret = new HashMap<>();
		IModel model = subject.getModel();

		if (model != null) {
			for (int i = 1; i < model.getObservables().size(); i++) {
				ous.put(model.getObservables().get(i).getFormalName(), model.getObservables().get(i));
			}
		}

		if (contextualizer != null) {
			ret = contextualizer.initialize(subject, (IActiveSubject) context, resolutionContext, inputsById, ous,
					monitor);
		} else {
			/*
			 * FIXME/TODO: output states are only created automatically if there
			 * is no contextualizer, assuming the contextualizer will know what
			 * to do (and potentially actions won't find the states if not).
			 */
			createOutputStates(subject, model);
		}

		if (context != null) {
			this.statesToInputs = States.matchStatesToInputs(context, inputsById);
		}

		if (isReinterpreter) {
			ret = reinterpretRoles(context);
		}

		// this allows observations to be made in init actions
		((Observation) subject).setInitialized(true);

		for (IAction action : getActions(Trigger.DEFINITION)) {
			if (((IActiveDirectObservation) this.observation).isActive()) {
				((KIMAction) action).execute(observation.getContextObservation(), subject, ITransition.INITIALIZATION,
						provenance, monitor);
			}
		}

		if (((IActiveDirectObservation) this.observation).isActive()) {
			((AbstractContext) observation.getContext()).schedule((IActiveDirectObservation) this.observation);
		}

		// add any action-generated observations
		for (IObservation obs : ((IActiveDirectObservation) this.observation).getActionGeneratedObservations(true)) {
			String name = obs instanceof IDirectObservation ? ((IDirectObservation) obs).getName()
					: ((IState) obs).getObservable().getSemantics().getFormalName();
			ret.put(name, obs);
		}

		/*
		 * add all the dynamic returned states to the list of those to flush
		 * before transitions
		 */
		for (IObservation o : ret.values()) {
			if (o instanceof IState && ((IState) o).isDynamic()) {
				toFlush.add((IState) o);
			}
		}

		return ret;

	}

	@Override
	public Map<String, IObservation> processTransition(ITransition transition, IMonitor monitor) throws KlabException {

		Map<String, IObservation> ret = new HashMap<>();
		Map<String, IState> inps = new HashMap<>();
		if (statesToInputs != null) {
			for (String s : statesToInputs.keySet()) {
				if (States.hasChanged(statesToInputs.get(s))) {
					inps.put(s, statesToInputs.get(s));
				}
			}
		}

		/*
		 * flush states
		 */
		for (IState state : toFlush) {
			((State) state).flushStorage(transition);
			States.setChanged(state, false);
		}

		if (contextualizer != null) {
			Map<String, IObservation> result = contextualizer.compute(transition, inps);
			if (ret != null) {
			    ret.putAll(result);
				for (IObservation o : ret.values()) {
					((Transition) transition).addModifiedObservation(o);
				}
			}

		}

		for (IAction action : getActions(Trigger.TRANSITION)) {
			if (((IActiveDirectObservation) this.observation).isActive()) {
				((KIMAction) action).execute(observation.getContextObservation(), getSubject(), transition, provenance,
						monitor);
			}
		}

		for (IObservation obs : ((IActiveDirectObservation) this.observation).getActionGeneratedObservations(true)) {
			String name = obs instanceof IDirectObservation ? ((IDirectObservation) obs).getName()
					: ((IState) obs).getObservable().getSemantics().getFormalName();
			ret.put(name, obs);
		}

		return ret;
	}

	@Override
	public IActiveSubject getSubject() {
		return (IActiveSubject) getObservation();
	}

}
