package org.integratedmodelling.common.model.runtime;

import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.exceptions.KlabRuntimeException;

public class Event extends DirectObservation implements IEvent, NetworkDeserializable {

    @Override
    public void deserialize(IModelBean object) {
        if (object instanceof org.integratedmodelling.common.beans.Event) {
            super.deserialize((org.integratedmodelling.common.beans.DirectObservation) object);
        } else {
            throw new KlabRuntimeException("Cannot deserialize a " + object.getClass().getCanonicalName()
                    + " to a Event");
        }
    }

    @Override
    public ISubject getContextObservation() {
        return (ISubject) super.getContextObservation();
    }

}
