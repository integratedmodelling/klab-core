/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.api.space.ITessellation;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.space.ShapeValue;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.common.utils.NumberUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.exceptions.KlabInternalRuntimeException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.io.ParseException;
import com.vividsolutions.jts.io.WKTReader;

public class Space extends Extent
        implements ISpatialExtent, IGeometricShape, NetworkSerializable,
        NetworkDeserializable {

    /*
     * bounding box; also doubles as region of interest in forcings.
     */
    double                maxx = Double.NaN, maxy = Double.NaN, minx = Double.NaN, miny = Double.NaN;

    /*
     * location of interest in forcings (drop region for observables).
     */
    double                locx = Double.NaN, locy = Double.NaN;
    double                cellHeight, cellLength;
    String                crs;
    private int           gridx;
    private int           gridy;
    private String        shapedef;
    private Grid          grid;
    private Tessellation  tessellation;
    private ShapeValue    shape;
    private IUnit         forcingUnit;
    private double        forcingSize;
    private ISpatialIndex index;
    private int           scaleRank;

    /**
     * Return a partially specified space that only serves to complete or force another
     * into a given grid size.
     * 
     * @param linearSize
     * @param unit
     * @return new space forcing
     */
    public static Space getForcing(double linearSize, String unit) {
        return new Space(linearSize, new Unit(unit));
    }

    @Override
    public IState as(IObserver observer) {
        return this;
    }

    
    @Override
    public boolean isConsistent() {
        return !getShape().isEmpty() && getShape().isValid();
    }

    class Grid implements IGrid {

        public Grid() {
        }

        public Grid(String string) {
            String[] ss = string.split(",");
            gridx = Integer.parseInt(ss[1]);
            gridy = Integer.parseInt(ss[2]);
        }

        @Override
        public int getYCells() {
            return gridy;
        }

        @Override
        public int getXCells() {
            return gridx;
        }

        @Override
        public int getCellCount() {
            return gridx * gridy;
        }

        @Override
        public int getOffset(int x, int y) {
            return (y * getXCells()) + x;
        }

        @Override
        public int[] getXYOffsets(int index) {
            int xx = index % getXCells();
            int yy = getYCells() - (index / getXCells()) - 1;
            return new int[] { xx, yy };
        }

        public double[] getCoordinatesAt(int x, int y) {
            double x1 = getMinX() + (cellLength * x);
            double y1 = getMinY() + (cellHeight * (getYCells() - y - 1));
            return new double[] { x1 + (cellLength / 2), y1 + (cellHeight / 2) };
        }

        @Override
        public double[] getCoordinates(int index) {
            int[] xy = getXYOffsets(index);
            return getCoordinatesAt(xy[0], xy[1]);
        }

        @Override
        public int getOffsetFromWorldCoordinates(double x, double y) {
            if (x < minx || x > maxx || y < miny || y > maxy)
                return -1;
            int xx = (int) (((x - minx) / (maxx - minx)) * gridx);
            int yy = (int) (((y - miny) / (maxy - miny)) * gridy);
            return getIndex(xx, yy);
        }

        @Override
        public boolean isActive(int x, int y) {
            // TODO Auto-generated method stub
            return true;
        }

        @Override
        public Locator getLocator(int x, int y) {
            return new SpaceLocator(x, gridy - y - 1);
        }

        @Override
        public double getMinX() {
            return minx;
        }

        @Override
        public double getMaxX() {
            return maxx;
        }

        @Override
        public double getMinY() {
            return miny;
        }

        @Override
        public double getMaxY() {
            return maxy;
        }

        @Override
        public Iterator<Cell> iterator() {

            return new Iterator<Cell>() {

                int n = 0;

                @Override
                public boolean hasNext() {
                    return n < getCellCount();
                }

                @Override
                public Cell next() {
                    return getCell(n++);
                }

                @Override
                public void remove() {
                }

            };
        }

        protected Cell getCell(int i) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public double getCellWidth() {
            return (maxx - minx) / gridx;
        }

        @Override
        public double getCellHeight() {
            return (maxy - miny) / gridy;
        }

        @Override
        public double getCellArea(boolean forceMeters) {
            // FIXME does not consider the parameter
            return getCellWidth() * getCellHeight();
        }

        @Override
        public double snapX(double xCoordinate, int direction) {
            int steps = (int) ((xCoordinate - getMinX()) / getCellWidth());
            if (direction == RIGHT && steps < getXCells() - 1) {
                steps++;
            }
            return getCellWidth() * steps;
        }

        @Override
        public double snapY(double yCoordinate, int direction) {
            int steps = (int) ((yCoordinate - getMinY()) / getCellHeight());
            if (direction == TOP && steps < getYCells() - 1) {
                steps++;
            }
            return getCellHeight() * steps;
        }

    }

    class Tessellation implements ITessellation {

        List<IShape> shapes = new ArrayList<>();

        public Tessellation(String string) {
            // TODO Auto-generated constructor stub
        }

        @Override
        public Iterator<IShape> iterator() {
            return shapes.iterator();
        }
    }

    public String getShapeDefinition() {
        return shapedef;
    }

    public String getCRSDefinition() {
        return crs;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Space)) {
            return false;
        }
        Space os = (Space) o;

        // TODO compare shape and representation
        return NumberUtils.equal(maxx, os.maxx) && NumberUtils.equal(minx, os.minx)
                && NumberUtils.equal(miny, os.miny) && NumberUtils.equal(maxy, os.maxy);

    }

    public Space(ShapeValue shape) {

        this.shape = shape;
        shapedef = shape.asText();
        crs = shape.getCRS();
        domain = KLAB.c(NS.SPACE_DOMAIN);
        Envelope envelope = shape.getEnvelope();
        maxx = envelope.getMaxX();
        maxy = envelope.getMaxY();
        minx = envelope.getMinX();
        miny = envelope.getMinY();

        multiplicity = 1;
    }

    /**
     * Don't remove - needed for auto constructors.
     */
    public Space() {
    }

    public void setLocationOfInterest(double x, double y) {
        this.locx = x;
        this.locy = y;
    }

    /**
     * This will not create a usable space, but just store the desired grid resolution. In
     * order to use this, it will need to be applied to the shape of another spatial
     * extent.
     * 
     * @param linearSize
     * @param unit
     */
    private Space(double linearSize, IUnit unit) {
        forcingUnit = unit;
        forcingSize = linearSize;
    }

    /*
     * Used only for forcings. Use the model factory and Space beans for anything else.
     * 
     * structure is in field separated by | field content 0 multiplicity 1 srs 2
     * minx,miny,maxx,maxy 3 G,x,y if grid; T,wkt1,... if tessellation; N if single extent
     * w/o spatial structure [4 shape - not added for now] [5 encoded activation layer -
     * not added for now]
     * 
     * TODO switch to serialized JSON and remove.
     */
    @SuppressWarnings("javadoc")
    public static String asString(ISpatialExtent e) {

        if (e instanceof Space && ((Space) e).isForcing()) {
            return "F!" + ((Space) e).forcingSize + "|" + ((Space) e).forcingUnit;
        }

        String ret = "";
        ret += e.getValueCount() + "|";
        ret += e.getCRSCode() + "|";
        ret += e.getMinX() + "," + e.getMinY() + "," + e.getMaxX() + "," + e.getMaxY()
                + "|";
        ret += e.getShape().asText() + "|";
        if (e.getGrid() != null) {
            ret += "G," + e.getGrid().getXCells() + "," + e.getGrid().getYCells();
        } else if (e.getTessellation() != null) {
            // TODO add shapes
            ret += "T";
        } else {
            ret += "N";
        }
        return ret;
    }

    /*
     * Used only to save and create the default spatial forcing from configured
     * properties. Use is strongly discouraged for anything else. Use the model factory
     * and beans for serialization and deserialization. Should switch to serialized JSON
     * and abandon, although not deprecating yet.
     */
    @SuppressWarnings("javadoc")
    public Space(String definition) {

        if (definition.startsWith("F!")) {
            String[] ss = definition.substring(2).split("\\|");
            forcingSize = Double.parseDouble(ss[0]);
            forcingUnit = new Unit(ss[1]);
        } else {
            String[] ss = definition.split("\\|");
            domain = KLAB.c(NS.SPACE_DOMAIN);
            multiplicity = Long.parseLong(ss[1]);
            crs = ss[2];
            String[] sz = ss[3].split(",");
            minx = Double.parseDouble(sz[0]);
            miny = Double.parseDouble(sz[1]);
            maxx = Double.parseDouble(sz[2]);
            maxy = Double.parseDouble(sz[3]);
            String shapedef = ss[4].trim();
            if (!shapedef.isEmpty()) {
                shape = new ShapeValue(shapedef);
            }
            if (ss[5].startsWith("G")) {
                grid = new Grid(ss[5]);
                cellLength = (maxx - minx) / gridx;
                cellHeight = (maxy - miny) / gridy;
            } else if (ss[5].startsWith("T")) {
                tessellation = new Tessellation(ss[4]);
            }
        }
    }

    public boolean isForcing() {
        return forcingUnit != null && forcingSize != 0;
    }

    @Override
    public String toString() {

        if (isForcing()) {
            return "Force " + forcingSize + " " + forcingUnit + " grid if unassigned";
        }

        /*
         * TODO improve
         */
        if (gridx != 0) {
            return "Grid " + gridx + " by " + gridy + " [" + getMultiplicity()
                    + " grid cells]";
        }
        return forcingUnit != null ? "As defined" : "Uniform location";
    }

    @Override
    public double getMinX() {
        return minx;
    }

    @Override
    public double getMinY() {
        return miny;
    }

    @Override
    public double getMaxX() {
        return maxx;
    }

    @Override
    public double getMaxY() {
        return maxy;
    }

    @Override
    public ISpatialExtent getExtent(int index) {
        return null;
    }

    public int getIndex(int x, int y) {
        if (gridx != 0) {
            return (y * gridx) + x;
        }
        return 0;
    }

    public double getWidth() {
        return getMaxX() - getMinX();
    }

    public double getHeight() {
        return getMaxY() - getMinY();
    }

    @Override
    public String getCRSCode() {
        return crs;
    }

    @Override
    public IGrid getGrid() {
        return grid;
    }

    @Override
    public ITessellation getTessellation() {
        return tessellation;
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int locate(Locator locator) {
        if (locator instanceof SpaceLocator) {
            if (locator.isAll())
                return GENERIC_LOCATOR;
            if (grid != null) {
                if (((SpaceLocator) locator).isLatLon()) {
                    return getGrid()
                            .getOffsetFromWorldCoordinates(((SpaceLocator) locator).lon, ((SpaceLocator) locator).lat);
                } else {
                    return getIndex(((SpaceLocator) locator).x, ((SpaceLocator) locator).y);
                }
            }
        }
        return INAPPROPRIATE_LOCATOR;
    }

    @Override
    public ShapeValue getShape() {
        if (shape == null) {
            if (shapedef != null) {
                try {
                    shape = new ShapeValue(new WKTReader().read(fixShapedef(shapedef)));
                } catch (ParseException e) {
                    throw new KlabRuntimeException(e);
                }
            }
            /*
             * last resort - shape is the bounding box or point.
             */
            if (shape /* still */ == null) {
                shape = new ShapeValue(minx, miny, maxx, maxy);
            }
        }
        return shape;
    }

    private String fixShapedef(String sh) {
        if (sh.startsWith("EPSG:")) {
            int sid = sh.indexOf(' ');
            return sh.substring(sid + 1);
        }
        return sh;
    }

    @Override
    public ISpatialExtent getExtent() {
        return new Space(getShape());
    }

    @Override
    public Geometry getStandardizedGeometry() {
        return getShape().getStandardizedGeometry();
    }

    @Override
    public Geometry getGeometry() {
        return getShape().getGeometry();
    }

    public void setForcing(double size, String unit) {
        forcingSize = size;
        forcingUnit = new Unit(unit);
    }

    public IUnit getForcingUnit() {
        return forcingUnit;
    }

    public double getForcingSize() {
        return forcingSize;
    }

    public String getForcingDefinition() {
        return forcingSize + " " + forcingUnit;
    }

    @Override
    public IConcept getDomainConcept() {
        return KLAB.c(NS.SPACE_DOMAIN);
    }

    @Override
    public Type getGeometryType() {
        return getShape().getGeometryType();
    }

    @Override
    public int getSRID() {
        return Integer.parseInt(Path.getLast(crs, ':'));
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservableSemantics observable, IConcept trait) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getArea() {
        return getShape().getArea();
    }

    @Override
    public ISpatialIndex getIndex(boolean makeNew) {

        if (makeNew) {
            return KLAB.MFACTORY.getSpatialIndex(this);
        }

        if (this.index == null) {
            this.index = KLAB.MFACTORY.getSpatialIndex(this);
        }
        return this.index;

    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Space)) {
            throw new KlabInternalRuntimeException("cannot adapt a "
                    + object.getClass().getCanonicalName() + " to a space extent");
        }

        org.integratedmodelling.common.beans.Space bean = (org.integratedmodelling.common.beans.Space) object;

        this.multiplicity = bean.getMultiplicity();

        maxx = bean.getMaxX();
        maxy = bean.getMaxY();
        minx = bean.getMinX();
        miny = bean.getMinY();

        locx = bean.getLocX();
        locy = bean.getLocY();
        
        scaleRank = bean.getScaleRank();

        if (bean.isForcing()) {
            this.forcingSize = bean.getForcingSize();
            this.forcingUnit = new Unit(bean.getForcingUnit());
        } else {

            shapedef = bean.getShape();
            crs = bean.getCrs();

            if (bean.getGrid() != null) {
                gridx = bean.getGrid().getXDivs();
                gridy = bean.getGrid().getYDivs();
                grid = new Grid();
                cellLength = (maxx - minx) / gridx;
                cellHeight = (maxy - miny) / gridy;
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass
                .isAssignableFrom(org.integratedmodelling.common.beans.Space.class)) {
            throw new KlabInternalRuntimeException("cannot adapt a space extent to "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Space ret = new org.integratedmodelling.common.beans.Space();

        ret.setMinX(minx);
        ret.setMinY(miny);
        ret.setMaxX(maxx);
        ret.setMaxY(maxy);
        ret.setLocX(locy);
        ret.setLocY(locy);
        ret.setScaleRank(scaleRank);

        if (isForcing()) {

            ret.setForcing(true);
            ret.setForcingUnit(forcingUnit.asText());
            ret.setForcingSize(forcingSize);

        } else {

            ret.setMultiplicity(multiplicity);
            ret.setCrs(crs);

            if (grid != null) {
                org.integratedmodelling.common.beans.Grid grd = new org.integratedmodelling.common.beans.Grid();
                grd.setMinX(grid.getMinX());
                grd.setMinY(grid.getMinY());
                grd.setMaxX(grid.getMaxX());
                grd.setMaxY(grid.getMaxY());
                grd.setXDivs(grid.getXCells());
                grd.setYDivs(grid.getYCells());
                grd.setShape(shapedef);
                ret.setGrid(grd);
            }
        }

        return (T) ret;
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public String asText() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return multiplicity > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporal() {
        return false;
    }

    @Override
    public boolean isSpatial() {
        return true;
    }

    @Override
    public ISpatialExtent getSpace() {
        return this;
    }

    @Override
    public ITemporalExtent getTime() {
        return null;
    }

    public void setRegionOfInterest(double minx2, double maxx2, double miny2, double maxy2) {
        this.minx = minx2;
        this.maxx = maxx2;
        this.miny = miny2;
        this.maxy = maxy2;
    }

    public double[] getLocationOfInterest() {
        if (!Double.isNaN(locx)) {
            return new double[] { locx, locy };
        }
        return null;
    }

    @Override
    public Class<?> getGeometryClass() {
        return getGeometry() == null ? null : getGeometry().getClass();
    }

    @Override
    public int getScaleRank() {
        return scaleRank;
    }
}
