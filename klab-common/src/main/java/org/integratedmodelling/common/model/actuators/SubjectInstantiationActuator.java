/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IAction.Trigger;
import org.integratedmodelling.api.modelling.contextualization.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.common.interfaces.actuators.ISubjectActuator;
import org.integratedmodelling.common.kim.KIMAction;
import org.integratedmodelling.common.states.States;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;

public class SubjectInstantiationActuator extends DirectInstantiator<IActiveSubject>
        implements ISubjectActuator {

    protected HashMap<IConcept, IState> inputStates = new HashMap<IConcept, IState>();
    protected ISubjectInstantiator      instantiator;
    private Map<String, IState>         statesToInputs;

    public SubjectInstantiationActuator(IActiveSubject context, IModel model,
            ISubjectInstantiator ds,
            List<IAction> actions, IMonitor monitor) {
        super(context, model);
        instantiator = ds;
        this.monitor = monitor;
        for (IAction action : actions) {
            this.addAction(action);
        }
    }

    @Override
    public String toString() {
        return instantiator == null ? "?"
                : Path.getLast(instantiator.getClass().getCanonicalName(), '.');
    }

    protected IState getInput(IConcept concept) {
        for (IConcept c : inputStates.keySet()) {
            if (c.is(concept)) {
                return inputStates.get(c);
            }
        }
        return null;
    }

    protected Collection<IConcept> getInputKeys() {
        return inputStates.keySet();
    }

    protected IState getInputState(IConcept observable) {
        return inputStates.get(observable);
    }

    protected Collection<IState> getInputStates() {
        return inputStates.values();
    }

    @Override
    public Map<String, IObservation> initialize(IActiveSubject subject, IActiveDirectObservation context, IResolutionScope resolutionContext, IMonitor monitor)
            throws KlabException {

        Map<String, IObservableSemantics> ins = new HashMap<>();
        Map<String, IObservableSemantics> ous = new HashMap<>();

        for (IDependency d : model.getDependencies()) {
            ins.put(d.getFormalName(), d.getObservable());
        }

        for (int i = 1; i < model.getObservables().size(); i++) {
            ous.put(model.getObservables().get(i).getFormalName(), model.getObservables()
                    .get(i));
        }

        monitor.info("initializing subject instantiator for "
                + model.getName(), Messages.INFOCLASS_MODEL);

        IConcept subjectType = model.getObservable().getType();
        if (instantiator != null) {
            instantiator.initialize(subject, resolutionContext, model, ins, ous, monitor);
        }
        this.statesToInputs = States.matchStatesToInputs(subject, ins);

        Map<String, IObservation> result = new HashMap<>();

        if (instantiator != null) {
            result.putAll(instantiator
                    .createSubjects(subject, ITransition.INITIALIZATION, this.statesToInputs));
            monitor.info((result == null ? 0 : result.size()) + " new observations of "
                    + NS.getDisplayName(subjectType)
                    + " created within " + subject.getName(), Messages.INFOCLASS_MODEL);
        }

        if (isReinterpreter) {
            result = reinterpretRoles(subject);
        } 
        
        for (IAction action : getActions(Trigger.DEFINITION)) {
            ((KIMAction) action).execute(observation
                    .getContextObservation(), subject, ITransition.INITIALIZATION, provenance, monitor);
        }
        // may add obs to both self and context
        for (IObservation obs : subject.getActionGeneratedObservations(true)) {
            result.put(getObservationName(obs), obs);
        }

        return result;
    }

    @Override
    public Map<String, IObservation> processTransition(ITransition transition, IMonitor monitor)
            throws KlabException {

        Map<String, IState> inps = new HashMap<>();
        for (String s : statesToInputs.keySet()) {
            if (States.hasChanged(statesToInputs.get(s))) {
                inps.put(s, statesToInputs.get(s));
            }
        }

        return instantiator == null ? new HashMap<>()
                : instantiator.createSubjects(getSubject(), transition, inps);

        /*
         * TODO ACTIONS
         */
    }

    @Override
    public IActiveSubject getSubject() {
        // FIXME this shouldn't be here, instantiators have no subject (well the
        // context, but that's
        // not what this means.
        return null;
    }

    @Override
    public Collection<IObservation> performPostResolutionActions(IActiveDirectObservation observation)
            throws KlabException {

        // on resolution
        List<IObservation> ret = new ArrayList<>();

        IActiveSubject subject = (IActiveSubject) observation;

        for (IAction action : getActions(Trigger.RESOLUTION)) {
            /*
             * clone and recompile action
             */
            IAction a = ((KIMAction) action)
                    .compileFor(observation.getScale(), model);
            ((KIMAction) a).execute(observation
                    .getContextObservation(), observation, ITransition.INITIALIZATION, provenance, monitor);

            // may add obs to both self and context
            ret.addAll(observation.getActionGeneratedObservations(true));
            if (observation.getContextObservation() != null) {
                ret.addAll(((IActiveDirectObservation) observation
                        .getContextObservation())
                                .getActionGeneratedObservations(true));
            }

        }

        for (IAction action : getActions(Trigger.EVENT)) {
            /**
             * Add event handlers. If they're here we are reactive, no need
             * to check.
             */
            subject.addEventHandler(action);
        }

        return ret;

    }

    @Override
    public Collection<IObservation> performPreResolutionActions(IActiveDirectObservation observation)
            throws KlabException {

        // on instantiation
        List<IObservation> ret = new ArrayList<>();

        for (IAction action : getActions(Trigger.INSTANTIATION)) {
            /*
             * clone and recompile action
             */
            IAction a = ((KIMAction) action)
                    .compileFor(observation.getScale(), model);
            ((KIMAction) a).execute(observation
                    .getContextObservation(), observation, ITransition.INITIALIZATION, provenance, monitor);

            // may add obs to both self and context
            ret.addAll(observation.getActionGeneratedObservations(true));
            if (observation.getContextObservation() != null) {
                ret.addAll(((IActiveDirectObservation) observation
                        .getContextObservation())
                                .getActionGeneratedObservations(true));
            }
        }

        return ret;
    }
}
