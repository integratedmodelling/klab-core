package org.integratedmodelling.common.model.runtime;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.base.HashableObject;
import org.integratedmodelling.common.interfaces.Monitorable;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Base contextualizer to ease definition of others. This one assumes that
 * the states will not be constant.
 * 
 * @author fvilla
 *
 */
public abstract class AbstractStateContextualizer extends HashableObject
        implements IStateContextualizer, Monitorable {

    private String                   stateName;
    private IObserver                observer;
    private IResolutionScope         scope;
    private IActiveDirectObservation contextSubject;
    private Map<String, IObservableSemantics> expectedOutputs;
    private Map<String, IObservableSemantics> expectedInputs;
    protected IMonitor               monitor;
    private boolean                  validate = false;
    protected IProvenance.Artifact provenance;

    protected AbstractStateContextualizer(IMonitor monitor) {
        this.monitor = monitor;
    }

    /**
     * For the dataflow graph: one word, simple, short and lowercase
     * 
     * @return
     */
    public abstract String getLabel();

    @Override
    public boolean canDispose() {
        return false;
    }
    
	@Override
	public boolean isConstant() {
		return false;
	}

    @Override
    public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance) throws KlabValidationException {
       this.provenance = provenance;
    }

    @Override
    public void setMonitor(IMonitor monitor) {
        this.monitor = monitor;
    }

    @Override
    public Map<String, IObservation> define(String name, IObserver observer, IActiveDirectObservation contextSubject, IResolutionScope context, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, boolean isLastInChain, IMonitor monitor)
            throws KlabException {

        this.stateName = name;
        this.observer = observer;
        this.scope = context;
        this.contextSubject = contextSubject;
        this.expectedInputs = expectedInputs;
        this.expectedOutputs = expectedOutputs;
        this.validate = isLastInChain;

        return null;
    }

    /**
     * If this returns true, we should validate the data against any criteria from the
     * semantics or the definition.
     * 
     * @return
     */
    protected boolean isValidating() {
        return validate;
    }

    protected String getStateName() {
        return stateName;
    }

    public void setStateName(String name) {
        this.stateName = name;
    }

    protected Collection<String> getOutputKeys() {
        return expectedOutputs.keySet();
    }

    protected Collection<String> getInputKeys() {
        return expectedInputs.keySet();
    }

    protected IScale getScale() {
        return scope.getScale();
    }

    protected IActiveDirectObservation getContextObservation() {
        return (IActiveDirectObservation) scope.getSubject();
    }

    protected Map<String, IObserver> getInputObservers() {
        Map<String, IObserver> ret = new HashMap<>();
        for (String key : expectedInputs.keySet()) {
            ret.put(key, expectedInputs.get(key).getObserver());
        }
        return ret;
    }

    protected Map<String, IObserver> getOutputObservers() {
        Map<String, IObserver> ret = new HashMap<>();
        for (String key : expectedOutputs.keySet()) {
            ret.put(key, expectedOutputs.get(key).getObserver());
        }
        return ret;
    }
}
