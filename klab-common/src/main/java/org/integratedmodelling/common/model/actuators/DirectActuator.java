/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.actuators;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveEvent;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.interfaces.actuators.IDirectActuator;
import org.integratedmodelling.common.interfaces.actuators.IInstantiator;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;

public abstract class DirectActuator<T extends IActiveDirectObservation> extends Actuator
        implements IDirectActuator<T> {

    public DirectActuator(IObservation observation, IModel model) {
        super(observation, model);
        if (observation instanceof IDirectObservation
                && !(this instanceof IInstantiator)) {
            ((Observation) observation).setActuator(this);
        }
    }

    @Override
    public IDirectObservation getObservation() {
        return (IDirectObservation) observation;
    }

    protected static void createOutputStates(IDirectObservation context, IModel model)
            throws KlabException {
        if (model != null) {
            for (IObservableSemantics obs : model.getObservables()) {
                if (NS.isQuality(obs) && !obs.getType().isAbstract()) {
                    ((IActiveDirectObservation) context).getState(obs);
                }
            }
        }
    }

    private ArrayList<Triple<String, IObserver, IObservableSemantics>> outputs           = new ArrayList<>();
    private Set<IObservableSemantics>                                  outputObservables = new HashSet<>();
    protected Map<String, IObservableSemantics>                        inputsById        = new HashMap<>();

    @Override
    public void notifyExpectedOutput(IObservableSemantics observable, IObserver observer, String name) {
        outputs.add(new Triple<>(name, observer, observable));
        outputObservables.add(observable);
    }

    @Override
    public void notifyExpectedInput(String key, IObservableSemantics observable) {
        inputsById.put(key, observable);
    }

    @Override
    public void notifyModel(IModel model) {
        this.model = model;
    }

    protected String getObservationName(IObservation obs) {
        return obs instanceof IState
                ? ((IState) obs).getObservable().getSemantics().getFormalName()
                : ((IDirectObservation) obs).getName();
    }

    public void dispatch(IActiveEvent event) {
        
        // TODO
        
    }
    
}
