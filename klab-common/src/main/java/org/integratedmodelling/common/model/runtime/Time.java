/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.model.runtime;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeDuration;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.time.DurationValue;
import org.integratedmodelling.common.time.PeriodValue;
import org.integratedmodelling.common.time.TimeLocator;
import org.integratedmodelling.common.time.TimeValue;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabInternalRuntimeException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

/**
 * TODO/FIXME: must accommodate single-period extents in parsing from definition and
 * elsewhere. For now it only works with grids.
 * 
 * @author ferdinando.villa
 *
 */
public class Time extends Extent implements ITemporalExtent, NetworkSerializable, NetworkDeserializable {

    long    start;
    long    end;
    long    step      = 0;
    boolean isForcing = false;
    int     scaleRank;

    public Time() {
    }

    /**
     * Return a partially specified time that only serves to complete or force another
     * into a given grid size.
     * 
     * @param start
     * @param end
     * @param step
     * @return a new time forcing
     * 
     */
    public static Time getForcing(long start, long end, long step) {
        Time ret = new Time(start, end, step);
        ret.isForcing = true;
        return ret;
    }

    public boolean isForcing() {
        return isForcing;
    }

    @Override
    public IState as(IObserver observer) {
        return this;
    }

    @Override
    public boolean isEmpty() {
        return start == 0 && end == 0;
    }

    public static ITemporalExtent create(long start, long end, long step) {

        if (start == 0 && end == 0) {
            return null;
        }

        if (step == 0) {
            return new PeriodValue(start, end);
        }

        return new Time(start, end, step);
    }

    public Time(long start, long end, long step) {
        this.start = start;
        this.end = end;
        this.step = step;
    }

    public Time(long time) {
        this.start = time;
        this.end = time;
    }

    /*
     * used only for forcings
     */
    @SuppressWarnings("javadoc")
    public static String asString(ITemporalExtent e) {

        if (e instanceof Time && ((Time) e).isForcing()) {
            return "F!" + ((Time) e).start + "," + ((Time) e).end + "," + ((Time) e).step;
        }

        String ret = "";
        ret += e.getValueCount() + ",";
        ret += e.getStart().getMillis() + ",";
        ret += e.getEnd().getMillis() + ",";
        ret += e.getStep().getMilliseconds();
        return ret;
    }

    /*
     * used only for forcings
     */
    @SuppressWarnings("javadoc")
    public Time(String definition) {

        if (definition.startsWith("F!")) {
            String[] ss = definition.substring(2).split(",");
            start = Long.parseLong(ss[0]);
            end = Long.parseLong(ss[1]);
            step = Long.parseLong(ss[2]);
            isForcing = true;
        } else {
            String[] ss = definition.split(",");
            multiplicity = Long.parseLong(ss[0]);
            start = Long.parseLong(ss[1]);
            end = Long.parseLong(ss[2]);
            step = Long.parseLong(ss[3]);
            domain = KLAB.c(NS.TIME_DOMAIN);
        }
    }

    @Override
    public String toString() {
        if (isEmpty()) {
            return "No temporal context";
        }

        /*
         * TODO improve
         */
        DateTimeFormatter format = DateTimeFormat.forPattern("dd/MM/YYYY");
        String dt1 = this.start == 0 ? "up" : new DateTime(this.start).toString(format);
        String dt2 = end == 0 ? "infinite" : new DateTime(this.end).toString(format);
        return dt1 + " to " + dt2 + (this.step <= 0 ? "" : (" [" + getMultiplicity() + " steps]"));
    }

    @Override
    public ITimeInstant getStart() {
        return start <= 0 ? null : new TimeValue(start);
    }

    @Override
    public long getMultiplicity() {
        return step <= 0 ? 1 : ((end - start) / step);
    }

    @Override
    public ITimeInstant getEnd() {
        return end <= 0 ? null : new TimeValue(end);
    }

    @Override
    public ITemporalExtent getExtent(int index) {
        return new PeriodValue(start + (step * index - 1), start
                        + (step * (index)), getDomainConcept());
    }

    @Override
    public ITimePeriod collapse() {
        return null;
    }

    @Override
    public ITemporalExtent intersection(IExtent other) throws KlabException {
        return null;
    }

    @Override
    public ITimeDuration getStep() {
        return step <= 0 ? null : new DurationValue(step);
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int locate(Locator locator) {
        if (locator == null)
            return 0;
        if (locator instanceof ITransition) {
            return ((ITransition) locator).getTimeIndex();
        }
        if (locator instanceof TimeLocator) {
            return ((TimeLocator) locator).isAll() ? GENERIC_LOCATOR : ((TimeLocator) locator).getSlice();
        }
        return INAPPROPRIATE_LOCATOR;
    }

    @Override
    public ITemporalExtent getExtent() {
        return new PeriodValue(start, end);
    }

    @Override
    public IConcept getDomainConcept() {
        return KLAB.c(NS.TIME_DOMAIN);
    }

    public static Time getForcing(ITemporalExtent time) {
        if (time == null) {
            return getForcing(0, 0, 0);
        } else {
            return getForcing(time.getStart() == null ? 0 : time.getStart().getMillis(), time.getEnd() == null
                    ? 0 : time.getEnd().getMillis(), time.getStep() == null ? 0
                            : time.getStep().getMilliseconds());
        }
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservableSemantics observable, IConcept trait) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Time)) {
            throw new KlabInternalRuntimeException("cannot adapt a "
                    + object.getClass().getCanonicalName() + " to a time extent");
        }

        org.integratedmodelling.common.beans.Time bean = (org.integratedmodelling.common.beans.Time) object;

        this.start = bean.getStart();
        this.end = bean.getEnd();
        this.step = bean.getStep();
        this.isForcing = bean.isForcing();
        this.multiplicity = bean.getMultiplicity();

    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Time.class)) {
            throw new KlabInternalRuntimeException("cannot adapt a space extent to "
                    + desiredClass.getCanonicalName());
        }
        org.integratedmodelling.common.beans.Time ret = new org.integratedmodelling.common.beans.Time();

        ret.setDomain(this.getDomainConcept().toString());
        ret.setEnd(this.end);
        ret.setStart(this.start);
        ret.setStep(this.step);
        ret.setForcing(this.isForcing);
        ret.setMultiplicity(this.multiplicity);

        return (T) ret;
    }

    @Override
    public boolean isDynamic() {
        return getMultiplicity() > 1;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporal() {
        return true;
    }

    @Override
    public boolean isSpatial() {
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public int getScaleRank() {
        return scaleRank;
    }

    @Override
    public ITransition getTransition(int i) {
        // TODO Auto-generated method stub
        return null;
    }
}
