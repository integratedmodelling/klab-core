package org.integratedmodelling.common.auth;

import java.util.List;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;

public interface IUserRegistry {

    /**
     * Authorize connection to node. Called by /identify service upon successful
     * validation of a node. Connection should persist for as long as needed.
     * 
     * @param request
     * @return
     */
    String authorize(ConnectionAuthorization request);

    /**
     * Get previously authorized connection authorization.
     * 
     * @param token
     * @return
     */
    ConnectionAuthorization getConnectionAuthorization(String token);

    /**
     * Get user from one of a set of possible tokens (just one in any current
     * application).
     * 
     * @param list
     * @return
     */
    IUser getUser(List<String> list);

    /**
     * Register service user (from a modeling engine) with its own authorization key. Key
     * should persist for the allowed amount of time.
     * 
     * @param user
     */
    void register(IUser user);

}
