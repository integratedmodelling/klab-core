package org.integratedmodelling.common.auth;

import java.io.File;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.exceptions.KlabAuthenticationException;

/**
 * @author ferdinando.villa
 *
 */
public class AuthenticationFactory {

    /**
     * Create a user from a certificate file. The user will need to be
     * authenticated through a session before it can make observations.
     * 
     * @param certificate
     * @return a user profile from the certificate.
     * @throws KlabAuthenticationException 
     */
    public static IUser createFromCertificate(File certificate) throws KlabAuthenticationException {
        return null;
    }

}
