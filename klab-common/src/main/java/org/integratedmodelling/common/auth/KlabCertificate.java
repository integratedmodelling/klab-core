package org.integratedmodelling.common.auth;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

/**
 * Wraps an IM certificate and checks for encryption and validity.
 * 
 * @author Ferd
 *
 */
public class KlabCertificate {

    File                        file             = null;
    Properties                  properties       = null;
    static File                 publicKey;
    String                      cause            = null;
    Set<String>                 groups           = null;
    DateTime                    expiry;

    private static final String PUBRING_URL      = "http://www.integratedmodelling.org/downloads/pubring.gpg";

    /**
     * Property key for username
     */
    public static final String  USER_KEY         = "user";
    /**
     * Property key for primary node.
     */
    public static final String  PRIMARY_NODE_KEY = "primary.server";

    /**
     * Get the file for the certificate according to configuration. Does not
     * check that the file exists.
     * 
     * @return the configured certificate file location
     */
    public static File getCertificateFile() {
        if (System.getProperty(IConfiguration.CERTFILE_PROPERTY) != null) {
            return new File(System.getProperty(IConfiguration.CERTFILE_PROPERTY));
        }
        return new File(KLAB.CONFIG.getDataPath() + File.separator + "im.cert");
    }

    /**
     * Get the file from its configured locations and open it. Check
     * {@link #isValid()} after construction.
     */
    public KlabCertificate() {
        this(getCertificateFile());
    }

    public KlabCertificate(String s) {
        try {
            this.file = File.createTempFile("imcert", "cert");
            FileUtils.writeStringToFile(this.file, s);
        } catch (IOException e) {
            throw new KlabRuntimeException(e);
        }
        if (publicKey == null) {
            File pubring = new File(KLAB.CONFIG.getDataPath("ssh") + File.separator
                    + "pubring.gpg");
            if (pubring.exists()) {
                publicKey = pubring;
            } else {
                try {
                    FileUtils.copyURLToFile(new URL(PUBRING_URL), pubring);
                    if (pubring.exists()) {
                        publicKey = pubring;
                    } else {
                        cause = "cannot find or download public keyring";
                    }
                } catch (IOException e) {
                    cause = "cannot find or download public keyring";
                }
            }
        }
    }

    /**
     * Create from a certificate file, downloading the public keyring from the
     * net if not already installed. Check {@link #isValid()} after
     * construction.
     * 
     * @param file
     */
    public KlabCertificate(File file) {
        this.file = file;
        if (publicKey == null) {
            File pubring = new File(KLAB.CONFIG.getDataPath("ssh") + File.separator
                    + "pubring.gpg");
            if (pubring.exists()) {
                publicKey = pubring;
            } else {
                try {
                    FileUtils.copyURLToFile(new URL(PUBRING_URL), pubring);
                    if (pubring.exists()) {
                        publicKey = pubring;
                    } else {
                        cause = "cannot find or download public keyring";
                    }
                } catch (IOException e) {
                    cause = "cannot find or download public keyring";
                }
            }
        }
    }

    /**
     * If this returns true, the certificate exists, is readable and properly
     * encrypted, and is current.
     * 
     * If this returns false, {@link #getCause} will contain the reason why.
     * 
     * @return true if everything is OK.
     */
    public boolean isValid() {
        if (cause != null) {
            return false;
        }
        if (!file.exists() || !file.isFile() || !file.canRead()) {
            cause = "certificate file cannot be read";
            return false;
        }
        if (properties == null) {
            try {
                properties = LicenseManager.readCertificate(file, publicKey);

                /*
                 * check expiration
                 */
                String exp = properties.getProperty("expiry");
                if (exp != null) {
                    try {
                        this.expiry = DateTime.parse(properties.getProperty("expiry"));
                    } catch (Throwable e) {
                    }
                    if (expiry /* still */ == null) {
                        KLAB.info("error parsing expiry date: setting to tomorrow");
                        this.expiry = DateTime.now().plusDays(1);
                    }
                }
                if (expiry == null) {
                    cause = "certificate has no expiration date. Please obtain a new certificate.";
                    return false;
                } else if (expiry.isBeforeNow()) {
                    cause = "certificate expired on " + expiry;
                    return false;
                }

            } catch (Throwable e) {
                cause = "error reading certificate: " + e.getMessage();
                return false;
            }
        }

        return true;
    }

    public DateTime getExpiryDate() {
        return expiry;
    }

    /**
     * If {@link #isValid()} returns false, return the reason why.
     * 
     * @return cause of invalidity
     */
    public String getCause() {
        return cause;
    }

    /**
     * Return the file we've been read from.
     * 
     * @return the certificate file
     */
    public File getFile() {
        return file;
    }

    /**
     * The properties defined in the certificate. Call after checking validity.
     * 
     * @return properties. Only null if {@link KlabCertificate#isValid()} has
     *         not been called or has returned false.
     */
    public Properties getProperties() {
        return properties;
    }

    /**
     * 
     * @return the certificate contents
     * @throws KlabIOException
     */
    public String getCertificate() throws KlabIOException {
        try {
            return FileUtils.readFileToString(file);
        } catch (IOException e) {
            throw new KlabIOException(e);
        }
    }

    /**
     * Groups extracted from certificate. May not be the same groups as the user
     * profile.
     * 
     * @return the groups in the cert file.
     */
    public Set<String> getGroups() {
        if (this.groups == null) {
            this.groups = new HashSet<>();
            if (properties != null && properties.containsKey(IUser.GROUPS)) {
                for (String g : StringUtils
                        .split(properties.getProperty(IUser.GROUPS), ',')) {
                    groups.add(g);
                }
            }
        }
        return this.groups;
    }

    /**
     * Descriptive string with user name, email, and expiry.
     * 
     * @return printable description.
     */
    public String getUserDescription() {
        if (properties != null && properties.containsKey(IUser.USER)) {
            return properties.getProperty(IUser.USER) + " ("
                    + properties.getProperty(IUser.EMAIL) + "), valid until "
                    + DateTimeFormat.forPattern("MM/dd/YYYY").print(expiry);
        }
        return "anonymous user";
    }
}
