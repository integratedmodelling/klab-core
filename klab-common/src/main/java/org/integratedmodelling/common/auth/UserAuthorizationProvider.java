package org.integratedmodelling.common.auth;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.integratedmodelling.exceptions.KlabAuthorizationException;

/**
 * Nodes may provide authorization in DIRECT or INDIRECT ways - or none at all. The
 * authorization they provide is part of their capabilities. DIRECT means they are capable
 * of authenticating using their own user directory; INDIRECT means they defer to another
 * node. In all cases, they must respond to the authentication endpoints.
 * 
 * Authenticated users will return an authentication token. The provider must be able to
 * autonomously retrieve previously authenticated user details from the token alone.
 * 
 * @author ferdinando.villa
 *
 */
public interface UserAuthorizationProvider {

    /**
     * Authenticate with username and password. Authentication failure should throw
     * exceptions, never return null.
     * 
     * @param username
     * @param password
     * @return a response
     * @throws KlabAuthorizationException
     */
    AuthorizationResponse authenticateUser(String username, String password)
            throws KlabAuthorizationException;

    /**
     * Authenticate with encrypted certificate contents. Authentication failure should
     * throw exceptions, never return null.
     * 
     * @param certificate
     * 
     * @return a response
     * @throws KlabAuthorizationException
     */
    AuthorizationResponse authenticateUser(String certificate)
            throws KlabAuthorizationException;

    /**
     * Pass an authorization token for a previously authenticated user and return its
     * profile. Throw an exception if the token is unknown or expired.
     * 
     * @param token
     * @return a user
     * @throws KlabAuthorizationException
     */
    IUser getAuthenticatedUser(String token) throws KlabAuthorizationException;

    /**
     * Get the set of privileges connected with an authorization token. This is used at
     * both engine and node ends, as the engine deals with users but nodes only store
     * privileges.
     * 
     * @param token
     * @return the privileges connected to this token
     */
    ConnectionAuthorization getAuthorization(String token);
    
    /**
     * This should return true if the server is managing its own user directory, false
     * if it defers to a primary k.LAB node.
     * 
     * @return
     */
    boolean isAuthorizationDirect();

}
