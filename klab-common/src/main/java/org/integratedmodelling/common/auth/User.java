package org.integratedmodelling.common.auth;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.common.beans.auth.Role;
import org.integratedmodelling.common.beans.auth.UserProfile;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.joda.time.DateTime;

/**
 * @author ferdinando.villa
 *
 */
public class User implements IUser, Serializable {
	
    private static final long serialVersionUID = 3443181942793326160L;

    UserProfile profile;
	String authToken;
	String primaryServerUrl;
	Status onlineStatus = Status.UNKNOWN;
	boolean canLockEngine;

	@SuppressWarnings("javadoc")
	public User(AuthorizationResponse data) {
		this.profile = data.getProfile();
		this.authToken = data.getAuthToken();
	}

	@SuppressWarnings("javadoc")
	public User(UserProfile data, String authToken) {
		this.profile = data;
		this.authToken = authToken;
	}

	public User(KlabCertificate cert) {
	    this.profile = new UserProfile();
	    canLockEngine = true;
	    this.profile.setUsername(cert.getProperties().getProperty((IUser.USER)));
        this.profile.setEmail(cert.getProperties().getProperty((IUser.EMAIL)));
        this.onlineStatus = Status.OFFLINE;
	}
	
	@Override
	public String toString() {
		return getUsername() + " (" + getEmailAddress() + ")";
	}

	@Override
	public String getUsername() {
		return profile.getUsername();
	}

	@Override
	public Set<String> getRoles() {
		Set<String> ret = new HashSet<>();
		for (Role r : profile.getRoles()) {
			ret.add(r.name());
		}
		return ret;
	}

	@Override
	public Set<String> getGroups() {
		Set<String> ret = new HashSet<>();
		ret.addAll(profile.getGroups());
		return ret;
	}

	/**
	 * Only retain the groups and roles that the passed user shares with us.
	 * 
	 * @param otherUser
	 */
	public void restrictPrivileges(IUser otherUser) {
		profile.getRoles().retainAll(otherUser.getRoles());
		profile.getGroups().retainAll(otherUser.getGroups());
	}

	@Override
	public boolean isAnonymous() {
		return getUsername().equals(IUser.ANONYMOUS_USER_ID);
	}

	@Override
	public String getServerURL() {
		return profile.getServerUrl();
	}

	@Override
	public String getSecurityKey() {
		return authToken;
	}

	@Override
	public String getEmailAddress() {
		return profile.getEmail();
	}

	@Override
	public String getFirstName() {
		return profile.getFirstName();
	}

	@Override
	public String getLastName() {
		return profile.getLastName();
	}

	@Override
	public String getInitials() {
		return profile.getInitials();
	}

	@Override
	public String getAffiliation() {
		return profile.getAffiliation();
	}

	@Override
	public String getComment() {
		return profile.getComments();
	}

	@Override
	public Date getLastLogin() {
		return new Date(DateTime.parse(profile.getLastLogin()).getMillis());
	}

	/**
	 * Online status is set from the outside.
	 * 
	 * @param online
	 */
	public void setOnline(Status online) {
		this.onlineStatus = online;
	}

	@Override
	public Status getOnlineStatus() {
		return onlineStatus;
	}

	public UserProfile getProfile() {
		return profile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((profile == null) ? 0 : profile.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof User)) {
			return false;
		}
		if (this.getUsername() == null && ((User) obj).getUsername() == null) {
			return true;
		}
		if (this.getUsername() == null || ((User) obj).getUsername() == null) {
			return false;
		}
		return ((IUser) obj).getUsername().equals(this.getUsername());
	}

	public void setAuthToken(String token) {
		this.authToken = token;
	}

    @Override
    public <T extends IIdentity> T getParentIdentity(Class<? extends IIdentity> type) {
        // TODO Auto-generated method stub
        return null;
    }


}
