/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.auth;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

import javax.naming.AuthenticationException;

import org.integratedmodelling.common.configuration.KLAB;

/**
 * Unique place for configuration, using one file.
 * 
 * Take a property file from the environment and provide simple access to the properties in
 * it. The THINKLAB_AUTH_PROPERTIES environmental variable must be defined and point to the
 * property file, which must contains the keys in the class constants. If it's not defined,
 * the thinklab.auth.property system property is tried. If that's not defined either,
 * $HOME/.thinklab/auth.properties is used. 
 * 
 * @author Ferd
 *
 */
public class AuthConfig {

    private static AuthConfig _this;

    private static Properties _properties;

    public static final String AUTHENTICATION_PROPERTIES_ENV_VARIABLE = "THINKLAB_AUTH_PROPERTIES";
    public static final String AUTHENTICATION_PROPERTIES_PROPERTY     = "thinklab.auth.property";

    public static final String ENCRYPTION_KEY_PROPERTY    = "encryption.key";
    public static final String SHARED_USER_SPACE_PROPERTY = "user.space";
    public static final String KEYRING_FILE_PROPERTY      = "keyring.file";
    public static final String PASSPHRASE_PROPERTY        = "passphrase";
    public static final String KEY_PROPERTY               = "key";
    public static final String EMAIL_USER                 = "email.user";
    public static final String EMAIL_PASSWORD             = "email.pass";

    public static void preloadProperties(String file) throws AuthenticationException {
        try {
            _properties = new Properties();
            InputStream inp = new FileInputStream(new File(file));
            _properties.load(inp);
            inp.close();
        } catch (Exception e) {
            throw new AuthenticationException("configuration error: authentication properties file missing");
        }
    }

    public static String getProperty(String key) throws AuthenticationException {
        return getProperties().getProperty(key);
    }

    public static String getProperty(String key, String defaultValue) throws AuthenticationException {
        return getProperties().getProperty(key, defaultValue);
    }

    private static Properties getProperties() throws AuthenticationException {

        if (_properties == null) {
            String pf = System.getenv(AUTHENTICATION_PROPERTIES_ENV_VARIABLE);
            if (pf == null) {
                pf = System.getProperty(AUTHENTICATION_PROPERTIES_PROPERTY);
            }
            if (pf == null) {
                pf = KLAB.CONFIG.getDataPath() + File.separator + "auth.properties";
            }
            if (pf == null || !new File(pf).exists()) {
                throw new AuthenticationException("configuration error: authentication properties file missing");
            }
            try {
                _properties = new Properties();
                InputStream inp = new FileInputStream(new File(pf));
                _properties.load(inp);
                inp.close();
            } catch (Exception e) {
                throw new AuthenticationException("configuration error: authentication properties file unreadable");
            }
        }
        return _properties;
    }

    public static String getEncryptionKey() throws AuthenticationException {
        return getProperties().getProperty(ENCRYPTION_KEY_PROPERTY);
    }

    public static File getSharedUserSpace() throws AuthenticationException {
        return new File(getProperties().getProperty(SHARED_USER_SPACE_PROPERTY));
    }

    public static void saveProperties(Properties properties, File file) throws AuthenticationException {
        try {
            OutputStream out = new FileOutputStream(file);
            properties.store(out, null);
            out.close();
        } catch (Exception e) {
            throw new AuthenticationException("Errors accessing authentication properties file. Exiting.");
        }
    }

    public static AuthConfig get() {
        if (_this == null) {
            _this = new AuthConfig();
        }
        return _this;
    }

    public static URL getPublicKeyURL() throws AuthenticationException {
        File f = new File(getEncryptionKey());
        if (!f.exists()) {
            throw new AuthenticationException("public key file does not exist");
        }
        try {
            return f.toURI().toURL();
        } catch (MalformedURLException e) {
            throw new AuthenticationException("error accessing public key");
        }
    }

    public static String getKeyringFile() throws AuthenticationException {
        return getProperties().getProperty(KEYRING_FILE_PROPERTY);
    }

    public static String getPassword() throws AuthenticationException {
        return getProperties().getProperty(PASSPHRASE_PROPERTY);
    }

    public static String getKey() throws AuthenticationException {
        return getProperties().getProperty(KEY_PROPERTY);
    }
}
