/*******************************************************************************
 * Copyright (C) 2007, 2014:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.eclipse.jgit.api.Git;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * @author Ferd
 *
 */
public class Workspace extends AbstractBaseWorkspace {

    File directory;
    File deployDirectory;
    
    /**
     * Project IDs of projects sent by the client, which should not be deployed or undeployed.
     */
    Set<String> clientProjects = new HashSet<>();

    /**
     * Assumes that the directory passed has been checked for validity before this is
     * called.
     * 
     * @param directory
     * @param createIfAbsent
     */
    public Workspace(File directory, boolean createIfAbsent) {

        Map<String, File> pmap = new HashMap<>();

        this.directory = directory;
        
        /**
         * Check if we have some properties telling us to clone some Git repos before
         * anything happens. This is an undocumented feature to speed up testing, we can
         * expose it at some point, only for developers.
         * 
         * Format is klab.clone.repository.<projectname> = gitURL,branch (master default)
         */
        for (Object p : KLAB.CONFIG.getProperties().keySet()) {
            if (p.toString().startsWith("klab.clone.repository.")) {
                String pname = Path.getLast(p.toString(), '.');
                File pdir = new File(directory + File.separator + pname);
                if (pdir.exists()) {
                    try {
                        FileUtils.deleteDirectory(pdir);
                    } catch (Throwable e) {
                        throw new KlabRuntimeException(e);
                    }
                }

                String pdef = KLAB.CONFIG.getProperties().getProperty(p.toString());
                String[] pdefs = pdef.split(",");
                String branch = pdefs.length < 2 ? "master" : pdefs[1];
                String url = pdefs[0];

                KLAB.ENGINE.getMonitor().info("cloning Git repository " + url + " branch " + branch + " ...", Messages.INFOCLASS_DOWNLOAD);
                try (Git result = Git.cloneRepository()
                        .setURI(url)
                        .setBranch(branch)
                        .setDirectory(pdir)
                        .call()) {
                    KLAB.ENGINE.getMonitor().info("Checked out Git repository: "
                            + result.getRepository(), Messages.INFOCLASS_DOWNLOAD);
                } catch (Throwable e) {
                    throw new KlabRuntimeException(e);
                }
            }
        }
        
        this.deployDirectory = new File(directory + File.separator + "deploy");
        
        /*
         * remove all deployed project if we're offline.
         */
        if (KLAB.CONFIG.isOffline() && this.deployDirectory.exists()) {
            try {
                FileUtils.deleteDirectory(this.deployDirectory);
            } catch (IOException e) {
                KLAB.error(e);
                // ok, move on
            }
        }
        
        if (!this.deployDirectory.exists() && createIfAbsent) {
            this.deployDirectory.mkdirs();
        }

        if (deployDirectory.exists() && deployDirectory.isDirectory()) {
            for (File f : deployDirectory.listFiles()) {
                if (ProjectManager.isKlabProject(f) && ProjectManager.isOpen(f)) {
                    pDirs.add(f);
                    pmap.put(MiscUtilities.getFileName(f.toString()), f);
                }
            }
        }

        if (directory.exists() && directory.isDirectory()) {
            for (File f : directory.listFiles()) {
                if (ProjectManager.isKlabProject(f) && ProjectManager.isOpen(f)) {
                    if (pmap.containsKey(MiscUtilities.getFileName(f.toString()))) {
                        pDirs.remove(pmap.get(MiscUtilities.getFileName(f.toString())));
                        pmap.remove(MiscUtilities.getFileName(f.toString()));
                    }
                    pDirs.add(f);
                }
            }
        }

        /*
         * Check if we have forced projects from the client that are not in the workspace directory;
         * if so, register them (do not load now).
         */
        if (System.getProperty(IConfiguration.KLAB_CLIENT_PROJECTS) != null) {
            
            String[] pdirs = System.getProperty(IConfiguration.KLAB_CLIENT_PROJECTS).split(",");
            for (String pd : pdirs) {
                File f = new File(pd);
                if (ProjectManager.isKlabProject(f) && ProjectManager.isOpen(f)) {
                    if (pmap.containsKey(MiscUtilities.getFileName(f.toString()))) {
                        pDirs.remove(pmap.get(MiscUtilities.getFileName(f.toString())));
                        pmap.remove(MiscUtilities.getFileName(f.toString()));
                    }
                    pDirs.add(f);
                    try {
                        IProject p = KLAB.PMANAGER.registerProject(f);
                        clientProjects.add(p.getId());
                    } catch (KlabException e) {
                        // shouldn't happen
                        throw new KlabRuntimeException(e);
                    }
                }
            }
        }
    }

    public boolean isClientProject(String id) {
        return clientProjects.contains(id);
    }
    
    @Override
    public File getDefaultFileLocation() {
        return directory;
    }

    @Override
    public File getDeployLocation() {
        return deployDirectory;
    }

    @Override
    public IProject create(String project) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void delete(String project) {
        // TODO Auto-generated method stub

    }

}
