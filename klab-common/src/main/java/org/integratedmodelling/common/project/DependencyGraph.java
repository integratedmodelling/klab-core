/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IDependencyGraph;
import org.integratedmodelling.common.configuration.KLAB;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

public class DependencyGraph extends DefaultDirectedGraph<String, DefaultEdge> implements IDependencyGraph {

    public DependencyGraph() {
        super(DefaultEdge.class);
    }

    private static final long serialVersionUID = -5918551081464530262L;

    @Override
    public Collection<INamespace> getDependents(INamespace ns) {
        HashSet<String> nset = new HashSet<String>();
        List<INamespace> ret = new ArrayList<INamespace>();
        collectDependents(ns.getId(), nset, true);
        nset.remove(ns.getId());
        for (String s : nset) {
            ret.add(KLAB.MMANAGER.getNamespace(s));
        }
        return ret;
    }

    @Override
    public Collection<INamespace> getPrerequisites(INamespace ns) {
        HashSet<String> nset = new HashSet<String>();
        List<INamespace> ret = new ArrayList<INamespace>();
        collectDependents(ns.getId(), nset, false);
        nset.remove(ns.getId());
        for (String s : nset) {
            ret.add(KLAB.MMANAGER.getNamespace(s));
        }
        return ret;
    }

    private void collectDependents(String ns, HashSet<String> nset, boolean incoming) {

        if (!nset.contains(ns)) {
            nset.add(ns);
            if (this.containsVertex(ns) /* may not contain it if there were parse errors */) {
                for (DefaultEdge e : (incoming ? incomingEdgesOf(ns) : outgoingEdgesOf(ns))) {
                    collectDependents(incoming ? getEdgeSource(e) : getEdgeTarget(e), nset, incoming);
                }
            }
        }
    }

}
