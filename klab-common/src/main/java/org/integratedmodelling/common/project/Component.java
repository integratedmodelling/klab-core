/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.components.Initialize;
import org.integratedmodelling.api.components.Setup;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.client.palette.PaletteManager;
import org.integratedmodelling.common.command.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabInternalErrorException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

import jodd.util.ClassLoaderUtil;

/**
 * One of these is created per
 * {@link org.integratedmodelling.api.components.Component} annotation and
 * recorded by each node. The corresponding info is transmitted to the client in
 * a capabilities call.
 * 
 * The API is collected based on the server's package - all prototypes whose
 * implementation is in a (sub)package of the annotated object is part of its
 * API.
 * 
 * TODO make this a simple extension of a project and give it self-deploy
 * methods from jars.
 * 
 * @author Ferd
 *
 */
public class Component extends Project implements IComponent {

	public static final String COMPONENT_ID_PROPERTY = "component.id";
	public static final String COMPONENT_PACKAGE_PROPERTY = "component.package";

	List<IPrototype> services = new ArrayList<>();
	Class<?> implementation;
	private boolean isActive = true;
	private boolean isInitialized;
	private List<String> importedKnowledge;
	private String id;
	private String domain;
	// ID of engine that provides the component.
	// private String engineId = null;

	private String initMethod = null;
	private String setupMethod = null;
	private boolean isSetupAsynchronous = false;
	boolean linked = false;
	private boolean binaryAssetsLoaded;

	/**
	 * @param path
	 */
	public Component(File path) {

		super(path);

		/*
		 * check for inline Maven install; if not, substitute paths with the
		 * regular ones.
		 */
		if (!new File(path + File.separator + "target").exists()) {
			this.srcRelativePath = "knowledge";
			this.libRelativePath = "lib";
		}

		if (getProperties().getProperty(COMPONENT_ID_PROPERTY) == null) {
			throw new KlabRuntimeException("component does not declare its ID in properties");
		}
		if (getProperties().getProperty(COMPONENT_PACKAGE_PROPERTY) == null) {
			throw new KlabRuntimeException("component does not declare its packages in properties");
		}

		this.id = getProperties().getProperty(COMPONENT_ID_PROPERTY);
	}

	public void loadBinaryAssets() {

		if (!binaryAssetsLoaded) {

			binaryAssetsLoaded = true;

			/*
			 * load binary assets in /lib - at the very least, that will contain
			 * our own code. Validation through signing should have prevented
			 * getting here if the code is suspicious. This doesn't get hit in
			 * development components.
			 */
			if (getLibDirectory().exists() && getLibDirectory().isDirectory()) {
				for (Iterator<File> it = FileUtils.iterateFiles(getLibDirectory(), new String[] { "jar" }, false); it
						.hasNext();) {
					File jarFile = it.next();
					ClassLoaderUtil.addFileToClassPath(jarFile, this.getClass().getClassLoader());
				}
			}

			/*
			 * binary assets - usually from target/classes in development
			 * components.
			 */
			if (getBinDirectory().exists() && getBinDirectory().isDirectory()) {
				ClassLoaderUtil.addFileToClassPath(getBinDirectory(), this.getClass().getClassLoader());
			}

			try {
				for (Pair<Annotation, Class<?>> decl : KLAB.ENGINE
						.scanPackage(getProperties().getProperty(COMPONENT_PACKAGE_PROPERTY))) {
					if (decl.getFirst() instanceof org.integratedmodelling.api.components.Component) {
						this.implementation = decl.getSecond();
						this.domain = ((org.integratedmodelling.api.components.Component) decl.getFirst()).worldview();
						this.version = Version
								.parse(((org.integratedmodelling.api.components.Component) decl.getFirst()).version()
										.toString());

						for (Method method : implementation.getMethods()) {
							if (method.isAnnotationPresent(Initialize.class)) {
								initMethod = method.getName();
							}
							if (method.isAnnotationPresent(Setup.class)) {
								setupMethod = method.getName();
								Setup setup = method.getAnnotation(Setup.class);
								isSetupAsynchronous = setup.asynchronous();
							}
						}

					} else if (decl.getFirst() instanceof org.integratedmodelling.api.services.annotations.Prototype) {
						IPrototype prototype = KLAB.ENGINE.getFunctionPrototype(
								((org.integratedmodelling.api.services.annotations.Prototype) decl.getFirst()).id());
						((Prototype) prototype).setComponentId(id);
						this.services.add(prototype);
					}

				}
			} catch (KlabException e) {
				// don't break for now, just deactivate, log the error and move
				// on.
				KLAB.error(e);
				isActive = false;
			}

			KLAB.info("component " + id + " loaded  (" + this.services.size() + " services provided)");
		}
	}

	@Override
	public String toString() {
		return "<C " + id + " (" + services.size() + " services" + ")>";
	}

	public Class<?> getExecutor() {
		return implementation;
	}

	@Override
	public Collection<IPrototype> getAPI() {
		return services;
	}

	@Override
	public boolean isActive() {
		return isActive;
	}

	@Override
	public String getId() {
		return id;
	}

	public void addServicePrototype(IPrototype prototype) {
		((Prototype) prototype).setComponentId(id);
		services.add(prototype);
	}

	/**
	 * @param monitor
	 * @throws KlabException
	 */
	public void initialize(IMonitor monitor) throws KlabException {

		loadBinaryAssets();
		
		if (!isActive) {
			return;
		}

		String missing = "";
		if (importedKnowledge != null) {
			for (String imp : importedKnowledge) {
				if (KLAB.MMANAGER.getExportedKnowledge(imp) == null) {
					missing += (missing.isEmpty() ? "" : ", ") + imp;
				}
			}
		}

		if (!missing.isEmpty()) {
			throw new KlabValidationException(
					"component " + id + " is missing the following exported knowledge IDs: " + missing);
		}

		if (isInitialized) {
			return;
		}
		if (implementation == null) {
			isActive = false;
			return;
		}

		if (initMethod == null) {
			isActive = true;
			isInitialized = true;
			return;
		}

		File toolkitPath = new File(getLoadPath() + File.separator + "toolkits");
		if (toolkitPath.exists() && toolkitPath.isDirectory()) {
		    PaletteManager.get().readPalettes(toolkitPath);
		}
		
		
		KLAB.info("initializing component " + id);

		Object executor = null;
		try {
			executor = implementation.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new KlabInternalErrorException("cannot instantiate component executor for " + id);
		}

		if (executor == null) {
			throw new KlabInternalErrorException("component " + id + " has not been configured properly");
		}

		/*
		 * find executing method and call it
		 */
		Method method = null;
		try {
			method = executor.getClass().getMethod(initMethod);
		} catch (NoSuchMethodException | SecurityException e) {
			return;
		}

		try {
			isActive = (Boolean) method.invoke(executor);
		} catch (Exception e) {
			throw (e instanceof KlabException ? (KlabException) e
					: new KlabInternalErrorException("error calling component initializer for " + id + ": "
							+ ExceptionUtils.getRootCauseMessage(e)));
		}

		isInitialized = true;

		KLAB.info("component " + id + " initialized successfully and ready for operation");
	}

	/**
	 * @throws KlabException
	 */
	public void setup() throws KlabException {

		if (implementation == null) {
			return;
		}
		if (setupMethod == null) {
			return;
		}
		Object executor = null;
		try {
			executor = implementation.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			throw new KlabInternalErrorException("cannot instantiate component executor for " + id);
		}

		if (executor == null) {
			throw new KlabInternalErrorException("component " + id + " has not been configured properly");
		}

		/*
		 * find executing method and call it
		 */
		Method method = null;

		try {
			method = executor.getClass().getMethod(setupMethod);
		} catch (NoSuchMethodException | SecurityException e) {
			return;
		}

		try {
			/*
			 * TODO do the task thing if isSetupAsynchronous
			 */
			isActive = (Boolean) method.invoke(executor);
		} catch (Exception e) {
			throw (e instanceof KlabException ? (KlabException) e
					: new KlabInternalErrorException("error calling component initializer for " + id + ": "
							+ ExceptionUtils.getRootCauseMessage(e)));
		}
	}

	public void setActive(boolean b) {
		isActive = b;
	}

	@Override
	public Collection<File> getBinaryAssets() {
		// TODO Auto-generated method stub
		return null;
	}

}
