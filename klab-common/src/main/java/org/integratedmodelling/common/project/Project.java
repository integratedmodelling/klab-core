/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.lang.IParsingScope;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.errormanagement.CompileError;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.kim.KIMNamespace;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.utils.ZipUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabResourceNotFoundException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

public class Project implements IProject {

    public static final String THINKLAB_META_INF                  = "META-INF";
    public static final String THINKLAB_PROPERTIES_FILE           = "klab.properties";

    // properties for META-INF/thinklab.properties file. May also have metadata
    // according to IMetadata fields.
    public static final String SOURCE_FOLDER_PROPERTY             = "thinklab.source.folder";
    public static final String ONTOLOGY_NAMESPACE_PREFIX_PROPERTY = "thinklab.ontology.prefix";
    public static final String PREREQUISITES_PROPERTY             = "thinklab.prerequisites";
    public static final String IMPORTS_PROPERTY                   = "thinklab.imports";
    public static final String VERSION_PROPERTY                   = "project.version";
    public static final String DEFAULT_NAMESPACE_PREFIX           = "http://www.integratedmodelling.org/ns";
    public static final String ORIGINATING_NODE_PROPERTY          = "klab.origin";
    public static final String WORLDVIEW_PROPERTY                 = "klab.worldview";

    /**
     * We can have the project create a namespace then write it using this function
     * instead of creating an empty one. we take care of opening/closing and refreshing
     * when done.
     * 
     * @author ferdinando.villa
     *
     */
    public static interface NamespaceGenerator {
        void write(String id, boolean isScenario, boolean isAppending, PrintWriter out) throws Exception;
    }

    final HashMap<String, INamespace> namespaces   = new HashMap<>();

    private String                    id;
    protected File                    path;
    private File                      pfile;
    private Properties                properties;
    protected List<String>            dependencies = new ArrayList<>();
    private long                      modificationDate;
    private boolean                   isWrapped;
    private String                    worldview;
    protected Version                 version;

    // /**
    // * Non-worldview projects have one ontology which only contains aliases for complex
    // * worldview concepts. Any namespace that contains its own concepts is marked
    // "dirty"
    // * and prevents publishing for the whole project. The actual ontology is created on
    // * first reference only (getOntology), which raises a runtime exception if the
    // project
    // * is part of a worldview.
    // */
    // private INamespace ontology = null;

    public Project() {
        // TODO Auto-generated constructor stub
    }

    String                          srcRelativePath = "src";
    String                          binRelativePath = "bin";
    String                          libRelativePath = "lib";

    /*
     * package private - set by project manager
     */
    boolean                         loaded;

    private final ArrayList<String> projectErrors   = new ArrayList<>();

    /**
     * This is the "standard" project, expecting knowledge in src/ and all dependencies in
     * META-INF/thinklab.properties, persisted when settings are changed through the API.
     * 
     * @param path
     */
    public Project(File path) {

        this.path = path;
        id = MiscUtilities.getFileName(path.toString());

        /*
         * FIXME weak check for wrapped packages vs. user-defined projects.
         */
        File wSrc = new File(path + File.separator + "src" + File.separator + "main" + File.separator
                + "resources"
                + File.separator + "knowledge");
        File wMvn = new File(path + File.separator + "target" + File.separator + "classes");
        File wLib = new File(path + File.separator + "target" + File.separator + "dependency");
        this.isWrapped = wSrc.exists() || wMvn.exists();

        if (wSrc.exists()) {
            this.srcRelativePath = "src/main/resources/knowledge";
        }
        if (wMvn.exists()) {
            this.binRelativePath = "target/classes";
        }
        if (wLib.exists()) {
            this.libRelativePath = "target/dependency";
        }

        File cProps = new File(path + File.separator + "src" + File.separator + "main" + File.separator
                + "resources"
                + File.separator + "META-INF" + File.separator + "klab.properties");

        if (cProps.exists()) {
            this.pfile = cProps;
        }

        this.properties = getProperties();
        String pp = getProperties().getProperty(PREREQUISITES_PROPERTY, "");
        String[] deps = pp.isEmpty() ? new String[0]
                : getProperties().getProperty(PREREQUISITES_PROPERTY, "").split(",");
        for (String s : deps) {
            this.dependencies.add(s);
        }

        this.worldview = getProperties().getProperty(WORLDVIEW_PROPERTY);

        if (this.worldview != null) {
            if (KLAB.getWorldview() != null && !KLAB.getWorldview().equals(this.worldview)) {
                addError(new KlabValidationException("project " + id + " subscribes to worldview "
                        + this.worldview + ", incompatible with current " + KLAB.getWorldview()));
            } else {
                if (KLAB.getWorldview() == null) {
                    KLAB.setWorldview(this.worldview);
                }
                KLAB.notifyWorldviewProject(this);
            }
        }
        
        setProjectKnowledge();
    }

    /**
     * This project does not persist properties and is used for wrapped projects coming
     * from components.
     * 
     * @param path
     * @param prerequisites
     * @param relativeSrcPath
     * @param relativeBinPath
     * @param relativeLibPath
     */
    public Project(File path, String[] prerequisites, String relativeSrcPath, String relativeBinPath,
            String relativeLibPath, long modificationDate) {

        this.isWrapped = true;
        this.modificationDate = modificationDate;
        this.path = path;
        this.id = MiscUtilities.getFileName(path.toString());
        this.properties = getProperties();
        String pp = getProperties().getProperty(PREREQUISITES_PROPERTY, "");
        String[] deps = pp.isEmpty() ? new String[0]
                : getProperties().getProperty(PREREQUISITES_PROPERTY, "").split(",");
        for (String s : deps) {
            this.dependencies.add(s);
        }
        
        this.worldview = getProperties().getProperty(WORLDVIEW_PROPERTY);
        
        setProjectKnowledge();
    }

    private void setProjectKnowledge() {
        if (this.worldview == null) {
            String key = id + ".knowledge";
            File nsfile = getProjectNamespaceFile();

            if (!nsfile.exists()) {
                try {
                    FileUtils.write(nsfile, "namespace " + key + ";\n\n");
                } catch (IOException e) {
                    throw new KlabRuntimeException(e);
                }
            }
        }
    }

    public Project(String id) {
        this.id = id;
    }

    public void setSrcRelativePath(String path) {
        this.srcRelativePath = path;
    }

    public void setBinRelativePath(String path) {
        this.binRelativePath = path;
    }

    public void setLibRelativePath(String path) {
        this.libRelativePath = path;
    }

    public boolean isLoaded() {
        return this.loaded;
    }

    // /*
    // * Non-API - sets without check
    // * @param namespace
    // */
    // public void setProjectNamespace(INamespace namespace) {
    // this.ontology = namespace;
    // }
    //
    // /*
    // * Non-API: gets without check
    // */
    // public INamespace getProjectNamespace() {
    // return this.ontology;
    // }

    @Override
    public INamespace getUserKnowledge() {
        return namespaces.get(id + ".knowledge");
    }

    @Override
    public String getId() {
        return this.id;
    }

    public void clear() {
        this.namespaces.clear();
        this.projectErrors.clear();
        this.loaded = false;
    }

    @Override
    public File findResource(String resource) {

        File ff = new File(this.path + File.separator + getSourceRelativePath() + File.separator + resource);

        if (ff.exists()) {
            return ff;
        }
        return null;

    }

    @Override
    public INamespace findNamespaceForResource(File resource) {

        for (INamespace ns : this.namespaces.values()) {
            if (ns.getLocalFile().equals(resource)) {
                return ns;
            }
        }
        return null;
    }

    @Override
    public Properties getProperties() {

        if (this.properties == null) {

            this.properties = new Properties();

            try {

                if (pfile == null) {
                    File pfile = new File(this.path + File.separator + "META-INF" + File.separator
                            + "klab.properties");

                    /**
                     * TODO remove legacy support when possible.
                     */
                    if (!pfile.exists()) {
                        pfile = new File(this.path + File.separator + "META-INF" + File.separator
                                + "thinklab.properties");
                    }

                    if (pfile.exists()) {
                        this.pfile = pfile;
                    }
                }

                if (pfile != null && pfile.exists()) {
                    this.modificationDate = pfile.lastModified();
                    InputStream inp = null;
                    try {
                        inp = new FileInputStream(pfile);
                        this.properties.load(inp);
                        inp.close();
                    } catch (Exception e) {
                        throw new KlabIOException(e);
                    }
                }

            } catch (KlabException e) {
                throw new KlabRuntimeException(e);
            }
        }
        // }
        return this.properties;
    }

    @Override
    public File getLoadPath() {
        return this.path;
    }

    @Override
    public Collection<INamespace> getNamespaces() {
        return this.namespaces.values();
    }

    @Override
    public String getSourceRelativePath() {
        return getProperties().getProperty(SOURCE_FOLDER_PROPERTY, this.srcRelativePath);
    }

    @Override
    public File findResourceForNamespace(String namespace, boolean returnIfAbsent) {

        String fp = namespace.replace('.', File.separatorChar);
        for (String extension : new String[] { KIM.DEFAULT_FILE_EXTENSION, "tql", "k", "owl" }) {
            File ff = new File(this.path + File.separator + getSourceRelativePath() + File.separator + fp
                    + "." + extension);
            if (ff.exists()) {
                return ff;
            }
        }

        if (returnIfAbsent) {
            return new File(this.path + File.separator + getSourceRelativePath() + File.separator + fp + "."
                    + KIM.DEFAULT_FILE_EXTENSION);
        }

        return null;
    }

    @Override
    public List<IProject> getPrerequisites() throws KlabException {

        ArrayList<IProject> ret = new ArrayList<>();
        for (String s : this.dependencies) {
            IProject p = KLAB.PMANAGER.getProject(s);

            if (p == null && this instanceof Component) {
                /*
                 * may be a component, too - this will get it from our repository or try
                 * to get it from the network if available.
                 */
                p = KLAB.PMANAGER.getDeployedComponent(s);
            }

            if (p == null) {
                throw new KlabResourceNotFoundException("project " + s + " required by project " + this.id
                        + " not found in workspace");
            }
            ret.add(p);
        }
        return ret;
    }

    @Override
    public long getLastModificationTime() {

        long lastmod = 0L;
        for (File f : FileUtils.listFiles(this.path, null, true)) {
            if (f.lastModified() > lastmod) {
                lastmod = f.lastModified();
            }
        }

        if (this.pfile != null && this.pfile.lastModified() > lastmod) {
            lastmod = this.pfile.lastModified();
        }

        return lastmod;
    }

    @Override
    public boolean equals(Object arg0) {
        return arg0 instanceof Project ? this.id.equals(((Project) arg0).id) : false;
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }

    @Override
    public String toString() {
        return "[project " + this.id + "]";
    }

    /**
     * @param plugin
     * @throws KlabException
     */
    public void addDependency(String plugin) throws KlabException {

        String pp = getProperties().getProperty(PREREQUISITES_PROPERTY, "");
        String[] deps = pp.isEmpty() ? new String[0]
                : getProperties().getProperty(PREREQUISITES_PROPERTY, "").split(",");

        String dps = "";
        for (String s : deps) {
            if (s.equals(plugin)) {
                return;
            }
            dps += (dps.isEmpty() ? "" : ",") + s;
        }

        dps += (dps.isEmpty() ? "" : ",") + plugin;
        getProperties().setProperty(PREREQUISITES_PROPERTY, dps);
        deps = dps.split(",");

        persistProperties();

    }

    /**
     * Set the project version and persist it.
     * 
     * @param version
     * @throws KlabException
     */
    @Override
    public void setVersion(Version version) throws KlabException {
        this.version = version;
        getProperties().setProperty(VERSION_PROPERTY, version.toString());
        persistProperties();
    }

    /**
     * @param projectIds
     * @throws KlabException
     */
    public void setDependencies(Collection<String> projectIds) throws KlabException {

        String dps = StringUtils.join(projectIds, ',');
        getProperties().setProperty(PREREQUISITES_PROPERTY, dps);
        persistProperties();
        this.dependencies.clear();
        this.dependencies.addAll(projectIds);
    }

    /**
     * @param dependencies
     * @throws KlabException
     */
    public void createManifest(Version version, String[] dependencies) throws KlabException {

        File td = new File(this.path + File.separator + "META-INF");
        td.mkdirs();

        new File(this.path + File.separator + getSourceRelativePath()).mkdirs();
        setVersion(version);

        if (dependencies != null && dependencies.length > 0) {
            for (String d : dependencies) {
                addDependency(d);
            }
        } else {
            persistProperties();
        }
    }

    /**
     * Persist the properties.
     */
    public void persistProperties() {

        if (!this.isWrapped) {
            File td = new File(this.path + File.separator + "META-INF" + File.separator + "klab.properties");

            try (OutputStream is = new FileOutputStream(td)) {
                getProperties().store(is, null);
            } catch (Exception e) {
                throw new KlabRuntimeException(e);
            }
        }
    }

    /**
     * @return zipped project in temporary file.
     * @throws KlabException
     */
    public File getZipArchive() throws KlabException {

        File ret = null;
        try {
            ret = new File(KLAB.CONFIG.getTempArea("pack") + File.separator + NameGenerator.shortUUID()
                    + ".zip");
            ZipUtils.zip(ret, this.path, true, false);
        } catch (Exception e) {
            throw new KlabIOException(e);
        }
        return ret;
    }

    /**
     * @param ns
     * @param isScenario
     * @return the file where the new namespace resides.
     * @throws KlabException
     */
    public File createNamespace(String ns, boolean isScenario) throws KlabException {

        File ret = new File(this.path + File.separator + getSourceRelativePath() + File.separator
                + ns.replace('.', File.separatorChar) + "." + KIM.DEFAULT_FILE_EXTENSION);
        File dir = new File(MiscUtilities.getFilePath(ret.toString()));

        try {
            dir.mkdirs();
            PrintWriter out = new PrintWriter(ret);
            out.println((isScenario ? "scenario " : "namespace ") + ns + ";\n");
            out.close();
        } catch (Exception e) {
            throw new KlabIOException(e);
        }

        ((ProjectManager) KLAB.PMANAGER).notifyNewNamespace(this, ns, ret);

        return new File(getSourceRelativePath() + File.separator + ns.replace('.', File.separatorChar) + "."
                + KIM.DEFAULT_FILE_EXTENSION);
    }

    /**
     * @param ns
     * @param isScenario
     * @param isAppending
     * @param generator
     * @return namespace file
     * @throws KlabException
     */
    public File createNamespace(String ns, boolean isScenario, boolean isAppending, NamespaceGenerator generator)
            throws KlabException {

        File ret = new File(this.path + File.separator + getSourceRelativePath() + File.separator
                + ns.replace('.', File.separatorChar) + "." + KIM.DEFAULT_FILE_EXTENSION);
        File dir = new File(MiscUtilities.getFilePath(ret.toString()));

        try {
            dir.mkdirs();
            PrintWriter out = new PrintWriter(ret);
            generator.write(ns, isScenario, isAppending, out);
            out.close();
        } catch (Exception e) {
            throw new KlabIOException(e);
        }

        ((ProjectManager) KLAB.PMANAGER).notifyNewNamespace(this, ns, ret);
        return new File(getSourceRelativePath() + File.separator + ns.replace('.', File.separatorChar)
                + KIM.DEFAULT_FILE_EXTENSION);
    }

    /**
     * @return all the project IDs we depend on
     */
    public String[] getPrerequisiteIds() {
        return this.dependencies.toArray(new String[this.dependencies.size()]);
    }

    @Override
    public boolean providesNamespace(String namespaceId) {
        return findResourceForNamespace(namespaceId, false) != null;
    }

    @Override
    public List<String> getUserResourceFolders() {

        ArrayList<String> ret = new ArrayList<>();

        for (File f : this.path.listFiles()) {
            if (f.isDirectory() && !f.equals(new File(this.path + File.separator + getSourceRelativePath()))
                    && !((ProjectManager) KLAB.PMANAGER)
                            .isManagedDirectory(MiscUtilities.getFileName(f.toString()), this)) {
                ret.add(MiscUtilities.getFileBaseName(f.toString()));
            }
        }

        return ret;
    }

    @Override
    public boolean hasErrors() {

        if (this.projectErrors.size() > 0) {
            return true;
        }

        for (INamespace n : this.namespaces.values()) {
            if (n.hasErrors()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasWarnings() {

        for (INamespace n : this.namespaces.values()) {
            if (n.hasWarnings()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Version getVersion() {
        return Version.parse(getProperties().getProperty(VERSION_PROPERTY, "0.0.1"));
    }

    @SuppressWarnings("javadoc")
    public void addError(Throwable e) {
        this.projectErrors.add(e.getMessage());
    }

    @Override
    public INamespace findNamespaceForImport(String namespace, IParsingScope context) throws KlabException {

        if (this.namespaces.containsKey(namespace)) {
            return this.namespaces.get(namespace);
        }

        if (KLAB.PMANAGER == null) {
            return null;
        }

        File resource = findResourceForNamespace(namespace, false);
        if (resource != null && resource.exists()) {

            INamespace ret = context.hasSeen(namespace) ? KLAB.MMANAGER.getNamespace(namespace)
                    : KIM.parse(resource, context.forNamespace(namespace));

            if (ret != null) {
                return ret;
            }
        }

        for (IProject p : getPrerequisites()) {
            INamespace ret = p.findNamespaceForImport(namespace, context.get(p));
            if (ret != null) {
                return ret;
            }
        }

        return null;
    }

    void notifyNamespace(INamespace ns) {
        this.namespaces.put(ns.getId(), ns);
    }

    @Override
    public Collection<File> getScripts() {

        ArrayList<File> ret = new ArrayList<>();

        File dir = new File(getLoadPath() + File.separator + ".scripts");

        if (dir.exists()) {
            for (File f : dir.listFiles()) {
                if (f.isFile() && f.canRead()
                        && MiscUtilities.getFileExtension(f.toString()).equals("groovy")) {
                    ret.add(f);
                }
            }
        }

        return ret;
    }

    public boolean needsFullRebuild() {
        return false;// this.pfile != null && this.pfile.lastModified() >
                     // this.modificationDate;
    }

    public void resetAfterFullBuild() {
        this.modificationDate = this.pfile == null ? new Date().getTime() : this.pfile.lastModified();
    }

    public Collection<String> deleteUnavailableNamespaces() {

        ArrayList<String> ret = new ArrayList<>();
        for (String s : this.namespaces.keySet()) {
            INamespace ns = this.namespaces.get(s);
            if (ns.getLocalFile() != null && !ns.getLocalFile().exists()) {
                ret.add(s);
                KLAB.MMANAGER.releaseNamespace(s);
            }
        }

        for (String s : ret) {
            this.namespaces.remove(s);
        }

        return ret;
    }

    @Override
    public synchronized void open(boolean open) {
        File check = new File(getLoadPath() + File.separator + "META-INF" + File.separator + ".closed");
        if (open) {
            FileUtils.deleteQuietly(check);
        } else {
            try {
                FileUtils.touch(check);
            } catch (IOException e) {
                throw new KlabRuntimeException(e);
            }
        }
    }

    @Override
    public boolean isOpen() {
        return !(new File(getLoadPath() + File.separator + "META-INF" + File.separator + ".closed").exists());
    }

    @Override
    public boolean isRemote() {
        File nodeFile = new File(getLoadPath() + File.separator + ".node");
        if (nodeFile.exists()) {
            return true;
        }
        String nodeId = getProperties().getProperty(ORIGINATING_NODE_PROPERTY);
        return nodeId != null && !nodeId.equals(KLAB.NAME);
    }

    /**
     * @return dir where binary contributed assets are
     */
    public File getLibDirectory() {
        return new File(getLoadPath() + File.separator + this.libRelativePath);
    }

    /**
     * @return directory where own binary assets are
     */
    public File getBinDirectory() {
        return new File(getLoadPath() + File.separator + this.binRelativePath);
    }

    /**
     * Return the relative paths (normalized) to all binary assets in lib dir. Ignores bin
     * (should only be relevant to remote project, which are never in development mode).
     * 
     * @return what I just wrote.
     */
    public Collection<String> getBinaryAssetPaths() {

        List<String> ret = new ArrayList<>();
        if (getLibDirectory().exists()) {
            for (File f : getLibDirectory().listFiles()) {
                if (f.toString().endsWith(".jar")) {
                    ret.add(this.libRelativePath + "/" + MiscUtilities.getFileName(f.toString()));
                }
            }
        }
        return ret;
    }

    public List<File> getDataAssetPaths() {
        List<File> ret = new ArrayList<>();

        /*
         * TODO use a smarter strategy
         */
        for (String path : new String[] { "data", "assets", "bn" }) {
            File f = new File(this.path + File.separator + path);
            if (f.exists() && f.isDirectory()) {
                ret.add(f);
            }
        }

        return ret;
    }

    public String getRelativeSrcPath() {
        return this.srcRelativePath;
    }

    public String getRelativeLibPath() {
        return this.libRelativePath;
    }

    /**
     * @param id
     */
    public void deleteNamespace(String id) {

        INamespace ns = this.namespaces.get(id);
        if (ns == null) {
            return;
        }
        this.namespaces.remove(id);
        KLAB.MMANAGER.releaseNamespace(id);
        File ff = ns.getLocalFile();
        if (ff.exists()) {
            FileUtils.deleteQuietly(ff);
        }
    }

    @Override
    public String getWorldview() {
        return worldview;
    }

    @Override
    public String getOriginatingNodeId() {
        return getProperties().getProperty(ORIGINATING_NODE_PROPERTY);
    }

    public void setOriginatingNodeId(String id) {
        getProperties().setProperty(ORIGINATING_NODE_PROPERTY, id);
        persistProperties();
    }

    public INamespace addUnreadableNamespace(String namespaceId, File resource) {
        KIMNamespace namespace = new KIMNamespace(namespaceId, resource, (IOntology) null);
        namespace.setVoid(true);
        namespace.setProject(this);
        namespace.addError(new CompileError(namespace, "unrecoverable parse error", 1));
        ((KIMModelManager) KLAB.MMANAGER).addNamespace(namespace);
        return null;
    }

    @Override
    public List<String> getErrors() {
        return projectErrors;
    }

    public File getProjectNamespaceFile() {
        return new File(path + File.separator + "META-INF" + File.separator + "knowledge.kim");
    }

    @Override
    public boolean isTainted() {
        for (INamespace ns : namespaces.values()) {
            if (ns.isTainted()) {
                return true;
            }
        }
        return false;
    }

}
