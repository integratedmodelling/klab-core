/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IWorkspace;

/**
 * Common methods for a workspace - essentially communication with the project manager.
 * 
 * @author ferdinando.villa
 *
 */
public abstract class AbstractBaseWorkspace implements IWorkspace {

    protected ArrayList<File> pDirs = new ArrayList<>();

    void notifyProjectRegistered(IProject project) {

        if (!getProjectLocations().contains(project.getLoadPath())) {
            getProjectLocations().remove(project.getLoadPath());
        }
    }

    void notifyProjectUnregistered(IProject project) {

        if (getProjectLocations().contains(project.getLoadPath())) {
            getProjectLocations().remove(project.getLoadPath());
        }
    }

    @Override
    public final Collection<File> getProjectLocations() {
        return pDirs;
    }

    @Override
    public boolean isRemotelySynchronized(IProject project) {
        return project.getLoadPath().equals(new File(getDeployLocation() + File.separator + project.getId()));
    }
}
