/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.project;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchEvent.Kind;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.factories.IProjectFactory;
import org.integratedmodelling.api.factories.IProjectManager;
import org.integratedmodelling.api.lang.IParsingScope;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.IProjectLifecycleListener;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.project.IDependencyGraph;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IWorkspace;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.responses.LocalExportResponse;
import org.integratedmodelling.common.client.KlabClient;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIM;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.kim.KIMNamespace;
import org.integratedmodelling.common.kim.KIMScope;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.ZipUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabResourceNotFoundException;
import org.integratedmodelling.exceptions.KlabValidationException;

import jodd.util.PropertiesUtil;

public class ProjectManager implements IProjectManager, IProjectFactory {

    // just used for special handling of the project namespace in META-INF/knowledge.kim
    private static final String     PROJECT_NAMESPACE_ID       = "__PROJECT_NAMESPACE__";

    private Map<String, Project>    projects                   = new HashMap<>();
    private Map<String, IComponent> ownComponents              = new HashMap<>();
    private Map<String, IComponent> deployedComponents         = new HashMap<>();

    /**
     * Contains the subset of ownComponents that is loaded from Maven development
     * directories and is declared in the properties.
     */
    private List<IComponent>        localDevelopmentComponents = new ArrayList<>();

    class ResourceInfo {
        INamespace namespace;
        IProject   project;
        boolean    readOnly = false;
        long       modificationDate;
        File       file;
    }

    private Map<File, ResourceInfo>           resourceInfo        = new HashMap<>();
    private DependencyGraph                   dependencyGraph     = null;
    private List<IProjectLifecycleListener>   listeners           = new ArrayList<>();
    private Set<File>                         registeredLocations = new HashSet<>();

    // watcher service to notify of project changes. Complex due to editors
    // creating many
    // events for a single user action.
    private WatchService                      watcher             = null;
    private Map<WatchKey, Path>               keys                = new HashMap<>();
    private Map<Path, WatchKey>               keypaths            = new HashMap<>();

    /*
     * do not make this private.
     */
    Map<Path, Pair<Long, WatchEvent.Kind<?>>> fileEvents          = new HashMap<>();

    /*
     * this is where we deploy remote components
     */
    File                                      componentDeployPath;
    private AtomicBoolean                     isParsing           = new AtomicBoolean(false);

    public ProjectManager() {

        try {
            if (KLAB.ENGINE instanceof KlabClient) {
                this.watcher = FileSystems.getDefault().newWatchService();
            }
            componentDeployPath = new File(KLAB.CONFIG.getDataPath("components")
                    + File.separator + "deploy");
            componentDeployPath.mkdirs();
        } catch (IOException e) {
            // just null
        }

        /*
         * start monitor thread. Will do nothing until there is something to watch.
         */
        new FileMonitor().start();
    }

    public List<IComponent> getLocalDevelopmentComponents() {
        return localDevelopmentComponents;
    }

    /*
     * One of these threads is started the first time a file is modified. It will start a
     * timer and monitor events for the file until the last event on the file is older
     * than a given time. At that point it will report the last file event to the project
     * manager.
     */
    class FileChangeActuator extends Thread {

        Path _file;
        long _idleTime;

        FileChangeActuator(Path file, long idleTime) {
            _file = file;
            _idleTime = idleTime;
        }

        @Override
        public void run() {

            for (;;) {

                long time = new Date().getTime();
                boolean oldEnough = false;

                synchronized (fileEvents) {
                    long delta = time - fileEvents.get(_file).getFirst();
                    oldEnough = delta >= _idleTime;
                }

                if (oldEnough) {
                    Kind<?> lastEvent = null;
                    synchronized (fileEvents) {
                        lastEvent = fileEvents.get(_file).getSecond();
                        fileEvents.remove(_file);
                    }
                    notifyFileEvent(_file, lastEvent);
                    return;
                }

                synchronized (fileEvents) {
                    fileEvents.get(_file).setFirst(time);
                }
                try {
                    Thread.sleep(_idleTime);
                } catch (InterruptedException e) {
                }
            }
        }
    }

    class FileMonitor extends Thread {

        @Override
        public void run() {

            for (;;) {
                /*
                 * re-check every whole second if there is nothing to watch.
                 */
                // synchronized (keys) {
                if (keys.isEmpty()) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                } else {

                    WatchKey key;
                    try {
                        key = watcher.take();
                    } catch (InterruptedException x) {
                        /*
                         * TODO - continue? Signal an error? Does this happen?
                         */
                        return;
                    }

                    Path dir = keys.get(key);
                    if (dir == null) {
                        continue;
                    }

                    for (WatchEvent<?> event : key.pollEvents()) {
                        WatchEvent.Kind<?> kind = event.kind();

                        // TODO - handle OVERFLOW (?)
                        if (kind == StandardWatchEventKinds.OVERFLOW) {
                            continue;
                        }

                        // Context for directory entry event is the file name of
                        // entry
                        WatchEvent<Path> ev = cast(event);
                        Path name = ev.context();
                        Path child = dir.resolve(name);

                        if (!isRelevant(child)) {
                            continue;
                        }

                        // TODO use event
                        notifyFileEvent(event, child);

                        // if directory is created, and watching recursively,
                        // then
                        // register it and its sub-directories
                        if (kind == StandardWatchEventKinds.ENTRY_CREATE) {
                            try {
                                if (Files.isDirectory(child, LinkOption.NOFOLLOW_LINKS)) {
                                    watchProjectDirectory(child);
                                }
                            } catch (IOException x) {
                                // don't see why this should happen
                            }
                        }
                    }

                    // reset key and remove from set if directory no longer
                    // accessible
                    boolean valid = key.reset();
                    if (!valid) {
                        keys.remove(key);

                        // all directories are inaccessible
                        if (keys.isEmpty()) {
                            break;
                        }
                    }
                    // }
                }
            }
        }

        private void notifyFileEvent(WatchEvent<?> event, Path child) {

            long time = new Date().getTime();

            synchronized (fileEvents) {
                boolean isNew = !fileEvents.containsKey(child);
                fileEvents.put(child, new Pair<Long, WatchEvent.Kind<?>>(time, event
                        .kind()));
                if (isNew) {
                    new FileChangeActuator(child, 500).start();
                }
            }

        }

        private boolean isRelevant(Path child) {
            return KLAB.MMANAGER.isModelFile(child.toFile())
                    || child.toString().endsWith(".properties")
                    || child.toString().endsWith(".project")
                    || child.toString().endsWith(".owl");
        }
    }

    @SuppressWarnings("unchecked")
    private static <T> WatchEvent<T> cast(WatchEvent<?> event) {
        return (WatchEvent<T>) event;
    }

    /**
     * Problem: on fast machines, resource info can be null due to asyncronicity, and
     * namespaces do not get notified. That does not happen when debugging.
     * 
     * @param path
     * @param lastEvent
     */
    public void notifyFileEvent(Path path, Kind<?> lastEvent) {

        // System.out.println("FE: init");

        ResourceInfo rinfo = resourceInfo.get(path.toFile());
        if (rinfo != null) {
            // System.out.println("FE: rinfo != null");
            if (rinfo.namespace != null) {
                // System.out.println("FE: namespace != null");

                for (IProjectLifecycleListener listener : listeners) {
                    if (lastEvent.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
                        listener.namespaceModified(rinfo.namespace
                                .getId(), rinfo.project);
                    } else if (lastEvent.equals(StandardWatchEventKinds.ENTRY_DELETE)) {
                        listener.namespaceDeleted(rinfo.namespace.getId(), rinfo.project);
                    }
                }
            }
        } else {

            // System.out.println("FE: rinfo == null");

            /*
             * TODO may be a properties file event or a new namespace created.
             */
            IProject project = getProjectForPath(path);
            if (project != null) {
                for (IProjectLifecycleListener listener : listeners) {
                    if (path.toString().contains("klab.properties")
                            || path.toString().endsWith(".project")) {
                        listener.projectPropertiesModified(project, path.toFile());
                    } else {
                        if (lastEvent.equals(StandardWatchEventKinds.ENTRY_MODIFY)) {
                            listener.fileModified(project, path.toFile());
                        } else if (lastEvent
                                .equals(StandardWatchEventKinds.ENTRY_DELETE)) {
                            listener.fileDeleted(project, path.toFile());
                        } else if (lastEvent
                                .equals(StandardWatchEventKinds.ENTRY_CREATE)) {
                            listener.fileCreated(project, path.toFile());
                        }
                    }
                }
            }
        }
    }

    private IProject getProjectForPath(Path path) {
        for (Project p : this.projects.values()) {
            if (path.toString().startsWith(p.getLoadPath().toString())) {
                return p;
            }
        }
        return null;
    }

    /**
     * Directories in a project that make this one return true are not shown to users.
     * 
     * @param fileBaseName the BASE name of the directory, without the path.
     * @param project unused for now
     * @return true if directory is managed
     */
    public boolean isManagedDirectory(String fileBaseName, Project project) {
        return fileBaseName.equals("META-INF") || fileBaseName.startsWith(".");
    }

    /**
     * True if directory contains a project. Note that a component is a project so this
     * will return true for a component. If we want to check if dir contains a component,
     * use {@link #isKlabComponent(File)} instead.
     * 
     * @param dir
     * @return true if the passed directory contains a k.LAB project.
     */
    public static boolean isKlabProject(File dir) {
        File f = new File(dir + File.separator + "META-INF" + File.separator
                + "klab.properties");
        if (f.exists()) {
            return true;
        }
        // TODO remove when possible
        f = new File(dir + File.separator + "META-INF" + File.separator
                + "thinklab.properties");
        return f.exists();
    }

    /**
     * Check if the directory contains a k.LAB component.
     * 
     * @param dir
     * @return true if the passed directory contains a k.LAB component.
     */
    public static boolean isKlabComponent(File dir) {
        if (!isKlabProject(dir)) {
            return false;
        }
        File f = new File(dir + File.separator + "lib");
        if (f.exists()) {
            for (File ff : f.listFiles()) {
                if (ff.toString().endsWith(".jar")) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Deploy the components installed in jar files in the data directory. Should only be
     * called at the engine side.
     * 
     * @return
     * @throws KlabException
     */
    public Collection<IComponent> deployComponents() throws KlabException {
        List<IComponent> ret = new ArrayList<>();

        Properties diocan = System.getProperties();

        /*
         * components deployed from development directories
         */
        if (diocan.getProperty(IConfiguration.KLAB_LOCAL_COMPONENTS) != null) {
            String[] props = diocan
                    .getProperty(IConfiguration.KLAB_LOCAL_COMPONENTS)
                    .split(",");
            for (String s : props) {
                if (s.trim().isEmpty()) {
                    continue;
                }
                File f = new File(s);
                try {
                    IComponent component = new Component(f);
                    ret.add(component);
                    localDevelopmentComponents.add(component);
                    this.ownComponents.put(component.getId(), component);
                } catch (Throwable e) {
                    KLAB.error("exception while reading local component in " + f + ": "
                            + e.getMessage());
                }
            }
        }

        /*
         * deployed from jar in components/
         */
        for (Iterator<File> it = FileUtils
                .iterateFiles(KLAB.CONFIG
                        .getDataPath("components"), new String[] { "jar" }, false); it
                                .hasNext();) {
            File file = it.next();
            try {
                IComponent component = unpackComponent(file);
                if (!this.ownComponents.containsKey(component.getId())) {
                    ret.add(component);
                    this.ownComponents.put(component.getId(), component);
                } else {
                    KLAB.info("Deployed component " + component.getId()
                            + " overridden by installed development version");
                }
            } catch (Throwable e) {
                KLAB.error("exception while unpacking component in " + file + ": "
                        + e.getMessage());
            }
        }

        /*
         * go through the components and resolve any others they depend on; if OK, load
         * binary assets
         */
        for (IComponent component : ret) {

            for (IProject project : component.getPrerequisites()) {
                if (project instanceof IComponent
                        && !this.ownComponents.containsKey(project.getId())) {
                    ret.add((IComponent) project);
                    ((Component) ret).loadBinaryAssets();
                }
            }

            ((Component) component).loadBinaryAssets();
        }
        return ret;
    }

    public IComponent unpackComponent(File jarFile) throws KlabException {

        File projectFolder = new File(KLAB.WORKSPACE.getDeployLocation() + File.separator
                + MiscUtilities.getFileBaseName(jarFile.toString()));

        projectFolder.mkdirs();
        File libDir = new File(projectFolder + File.separator + "lib");
        libDir.mkdirs();

        KLAB.info("unpacking component " + jarFile);

        ZipUtils.extractDirectories(jarFile, projectFolder, new String[] {
                "META-INF",
                "knowledge",
                "lib" });

        try {
            FileUtils.copyFileToDirectory(jarFile, libDir);
        } catch (IOException e) {
            throw new KlabIOException(e);
        }

        IComponent ret = new Component(projectFolder);

        /*
         * create file digest so that we can synchronize this component quickly if we're
         * exporting it.
         */
        if (KLAB.ENGINE.getResourceConfiguration().getComponentIds()
                .contains(ret.getId())) {
            KLAB.info("creating file digest for exported component " + ret.getId());
            org.integratedmodelling.common.utils.FileUtils
                    .createMD5Digest(projectFolder, "filelist.txt");
        }

        return ret;
    }

    /**
     * Check if resource needs to be re-parsed; if so, parse it, reset its info in
     * resourceInfo and return the namespace; otherwise return null to mean "no action was
     * necessary".
     * 
     * @param resource
     * @param monitor
     * @return
     * @throws KlabException
     */
    INamespace checkIn(File resource, IProject project, String namespaceId, IParsingScope context)
            throws KlabException {

        ResourceInfo info = resourceInfo.get(resource);

        if (info == null || info.modificationDate < resource.lastModified()) {

            if (info == null) {
                info = new ResourceInfo();
            }

            /*
             * update and store info before we read. If anything bad happens, the NS in
             * the descriptor will be null.
             */
            info.modificationDate = resource.lastModified();
            info.project = project;
            info.file = resource;
            resourceInfo.put(resource, info);

            INamespace ns = ((context.hasSeen(namespaceId))
                    ? KLAB.MMANAGER.getNamespace(namespaceId)
                    : KIM.parse(resource, context.forNamespace(namespaceId)));

            info.namespace = ns;
            if (ns == null) {
                ns = ((Project) project).addUnreadableNamespace(namespaceId, resource);
            } else if (!((KIMNamespace) ns).isSidecarFile()) {
                ((Project) project).notifyNamespace(ns);
            }
            return ns;
        }

        return null;
    }

    @Override
    public void addListener(IProjectLifecycleListener listener) {
        this.listeners.add(listener);
    }

    @Override
    public IProject getProject(String projectId) {
        return this.projects.get(projectId);
    }

    @Override
    public Collection<IProject> getProjects() {
        ArrayList<IProject> ret = new ArrayList<>();
        for (Project p : this.projects.values())
            ret.add(p);
        return ret;
    }

    /**
     * Go through every registered project and refresh its contents, reading whatever
     * resource has changed or wasn't seen before. Return the list of all the namespaces
     * that were modified compared to the previous load().
     * 
     * @throws KlabException
     */
    @Override
    public synchronized List<INamespace> load(boolean forceReload, IParsingScope context)
            throws KlabException {

        isParsing.set(true);

        if (forceReload) {
            KLAB.MMANAGER.releaseAll();
            resourceInfo.clear();
            for (IProject p : getProjects()) {
                ((Project) p).clear();
            }
        }

        ArrayList<INamespace> ret = new ArrayList<>();
        HashSet<IProject> pread = new HashSet<>();
        for (IProject p : getProjects()) {
            ret.addAll(loadProject((Project) p, pread, context.get(p)));
        }

        if (forceReload) {
            /*
             * reload component code
             */
            for (IComponent component : getComponents()) {
                loadComponent(component, context.get(component));
            }
        }

        /*
         * refresh everything that can be affected. If we don't have a dependency graph,
         * we are loading for the first time so nothing needs to be refreshed.
         */
        if (dependencyGraph != null) {

            HashSet<INamespace> reloaded = new HashSet<>(ret);
            for (INamespace ns : ret) {
                for (INamespace dep : getDependencyGraph().getDependents(ns)) {
                    if (dep == null) {
                        // happens after project close and no cleanup
                        continue;
                    }
                    if (!reloaded.contains(dep) && dep.getLocalFile() != null
                            && dep.getProject() != null) {
                        reloaded.add(checkIn(dep.getLocalFile(), dep.getProject(), dep
                                .getId(), context));
                    }
                }
            }

            /*
             * included reloaded NS in returned list.
             */
            ret.clear();
            ret.addAll(reloaded);
        }

        rebuildDependencyGraph();

        isParsing.set(false);
        ;

        for (IProjectLifecycleListener listener : listeners) {
            listener.onReload(forceReload);
        }

        return ret;
    }

    private void rebuildDependencyGraph() {

        dependencyGraph = new DependencyGraph();

        for (IProject p : getProjects()) {
            for (INamespace s : p.getNamespaces()) {
                dependencyGraph.addVertex(s.getId());
            }
        }

        for (String s : dependencyGraph.vertexSet()) {
            for (INamespace ns : KLAB.MMANAGER.getNamespace(s).getImportedNamespaces()) {
                if (ns != null
                        /* happens in error, we don't want NPEs here */
                        && dependencyGraph.containsVertex(s)
                        && dependencyGraph.containsVertex(ns.getId())) {
                    dependencyGraph.addEdge(s, ns.getId());
                }
            }
        }
    }

    /*
     * "smart" project loader.
     */
    private List<INamespace> loadProject(Project project, Set<IProject> read, IParsingScope context)
            throws KlabException {

        List<INamespace> ret = new ArrayList<>();

        if (read.contains(project)) {
            return ret;
        }

        read.add(project);

        try {
            for (IProject p : project.getPrerequisites()) {
                ret.addAll(loadProject((Project) p, read, context.get(p)));
            }
        } catch (KlabException e) {
            project.addError(e);
            // throw e;
        }

        /*
         * dispose of any namespaces backed by files that were removed.
         */
        for (String ns : project.deleteUnavailableNamespaces()) {
            for (IProjectLifecycleListener listener : listeners) {
                listener.namespaceDeleted(ns, project);
            }
        }

        KLAB.info("loading project " + project.getId() + " from directory "
                + project.getLoadPath());

        /*
         * if there is a project namespace, load that first
         */
        File kfile = project.getProjectNamespaceFile();
        if (kfile.exists()) {
            loadFile(kfile, PROJECT_NAMESPACE_ID, project, ret, context);
        }
        /*
         * load each file in project's source directory
         */
        File srcDir = new File(project.getLoadPath() + File.separator
                + project.getSourceRelativePath());
        if (srcDir.exists()) {
            for (File fl : srcDir.listFiles()) {
                loadFile(fl, "", project, ret, context);
            }
        } else {
            KLAB.info("project " + project.getId() + " has no source files");
        }

        /*
         * register with watcher
         */
        if (watcher != null && project.getLoadPath() != null) {
            try {
                watchProjectDirectory(project.getLoadPath().toPath());
            } catch (IOException e) {
                // throw new ThinklabIOException(e);
            }
        }

        KLAB.info("project " + project.getId() + " loaded successfully");

        return ret;
    }

    private void loadFile(File f, String path, Project project, List<INamespace> ret, IParsingScope context) {

        if (path != null && path.equals(PROJECT_NAMESPACE_ID)) {
            INamespace ns = null;
            try {
                ns = checkIn(f, project, project.getId() + ".knowledge", ((KIMScope) context)
                        .get(KIMScope.Type.PROJECT_DEFAULT_KNOWLEDGE));
            } catch (KlabException e) {
                project.addError(e);
            }

            if (ns != null) {
                ret.add(ns);
            }

        } else {

            String pth = path == null ? ""
                    : (path + (path.isEmpty() ? "" : ".")
                            + CamelCase.toLowerCase(MiscUtilities
                                    .getFileBaseName(f.toString()), '-'));

            if (f.isDirectory()) {

                for (File fl : f.listFiles()) {
                    loadFile(fl, pth, project, ret, context);
                }

            } else if (KLAB.MMANAGER.isModelFile(f)) {
                INamespace ns = null;
                try {
                    ns = checkIn(f, project, pth, context);
                } catch (KlabException e) {
                    project.addError(e);
                }
                if (ns != null) {
                    ret.add(ns);
                }
            }
        }
    }

    @Override
    public void undeployProject(String projectId) throws KlabException {

        Project project = this.projects.get(projectId);
        if (project != null) {
            unloadProject(projectId);
            unregisterProject(projectId);
        }
    }

    @Override
    public IProject deployProject(String pluginId, String resourceId, IMonitor monitor)
            throws KlabException {

        if (getProject(pluginId) != null) {
            if (((Workspace) KLAB.WORKSPACE).isClientProject(pluginId)) {
                return getProject(pluginId);
            }
            undeployProject(pluginId);
        }

        File destdir = KLAB.WORKSPACE.getDefaultFileLocation();
        destdir.mkdirs();

        File destprj = new File(destdir + File.separator + pluginId);
        destdir.mkdirs();
        if (destprj.exists()) {
            try {
                FileUtils.deleteDirectory(destprj);
            } catch (IOException e) {
                KLAB.error(e);
                throw new KlabIOException(e);
            }
        }
        ZipUtils.unzip(new File(resourceId), destdir);

        IProject project = registerProject(destprj);

        KLAB.info("registered deployed project " + pluginId + " into " + destdir);

        return project;
    }

    @Override
    public IProject registerProject(File projectDir) throws KlabException {

        Project project = null;
        String projectId = MiscUtilities.getFileName(projectDir.toString());

        if (registeredLocations.contains(projectDir)) {
            return getProject(projectId);
        }

        registeredLocations.add(projectDir);
        project = this.projects.get(projectId);
        boolean success = false;

        if (project == null) {
            project = new Project(projectDir);
            if (project.isOpen()) {
                this.projects.put(project.getId(), project);
                success = true;
            }
        }
        if (success) {
            for (IProjectLifecycleListener listener : listeners) {
                listener.projectRegistered(project);
            }
        }
        if (success && KLAB.WORKSPACE instanceof AbstractBaseWorkspace) {
            ((AbstractBaseWorkspace) KLAB.WORKSPACE).notifyProjectRegistered(project);
        }

        return project;
    }

    @Override
    public List<INamespace> loadProject(String projectId, IParsingScope context)
            throws KlabException {

        Project project = this.projects.get(projectId);
        if (project == null) {
            throw new KlabResourceNotFoundException("cannot load unknown project "
                    + projectId);
        }
        HashSet<IProject> pread = new HashSet<>();
        List<INamespace> ret = loadProject(project, pread, context.get(project));
        return ret;
    }

    @Override
    public void unregisterProject(String projectId) throws KlabException {

        Project project = this.projects.get(projectId);
        if (project != null) {
            if (project.isLoaded()) {
                unloadProject(projectId);
            }
            this.projects.remove(projectId);
            registeredLocations.remove(project.getLoadPath());
            for (IProjectLifecycleListener listener : listeners) {
                listener.projectUnregistered(project);
            }
            if (KLAB.WORKSPACE instanceof AbstractBaseWorkspace) {
                ((AbstractBaseWorkspace) KLAB.WORKSPACE)
                        .notifyProjectUnregistered(project);
            }
        }
    }

    @Override
    public void registerProjectDirectory(File projectDirectory) throws KlabException {

        /*
         * register all projects in configured directory
         */
        ArrayList<File> pdirs = new ArrayList<>();
        if (projectDirectory.exists() && projectDirectory.isDirectory()) {
            for (File f : projectDirectory.listFiles()) {
                if (isKlabProject(f)) {
                    pdirs.add(f);
                }
            }
        }
        if (pdirs.size() > 0) {
            for (File pdir : pdirs) {
                registerProject(pdir);
            }
        }
    }

    /**
     * Register the given directory and all its sub-directories with the watch service.
     */
    private void watchProjectDirectory(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                    throws IOException {
                watch(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Unregister the given directory and all its sub-directories with the watch service.
     * 
     * @throws IOException
     */
    private void unwatchProjectDirectory(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {
                try {
                    unwatch(dir);
                } catch (IOException e) {
                    //
                }
                return FileVisitResult.CONTINUE;
            }
        });
    }

    /**
     * Register the given directory with the WatchService
     */
    public void watch(Path dir) throws IOException {
        // synchronized (keys) {
        WatchKey key = dir
                .register(watcher, StandardWatchEventKinds.ENTRY_CREATE, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
        keys.put(key, dir);
        keypaths.put(dir, key);
        // }
    }

    public void unwatch(Path dir) throws IOException {
        // synchronized (keys) {
        WatchKey key = keypaths.get(dir);
        if (key != null) {
            key.cancel();
            keypaths.remove(dir);
            keys.remove(key);
        }
        // }
    }

    @Override
    public void unloadProject(String projectId) throws KlabException {

        IProject p = this.projects.get(projectId);
        if (p != null) {

            /*
             * clean up resource info
             */
            ArrayList<File> resources = new ArrayList<>();
            for (ResourceInfo res : resourceInfo.values()) {
                if (res.project.getId().equals(projectId)) {
                    resources.add(res.file);
                }
            }
            for (File file : resources) {
                resourceInfo.remove(file);
            }

            if (watcher != null) {
                try {
                    unwatchProjectDirectory(p.getLoadPath().toPath());
                } catch (IOException e) {
                    KLAB.error(e);
                }
            }

            for (INamespace s : p.getNamespaces()) {
                KLAB.MMANAGER.releaseNamespace(s.getId());
            }

        }
    }

    @Override
    public IProject createProject(File projectPath, String[] prerequisites)
            throws KlabException {

        String pid = MiscUtilities.getFileName(projectPath.toString());
        Version version = new Version("0.0.1");

        IProject project = this.projects.get(pid);

        if (project != null)
            throw new KlabValidationException("cannot create already existing project: "
                    + pid);

        project = new Project(projectPath);
        ((Project) project).createManifest(version, prerequisites);

        project = registerProject(projectPath);
        KLAB.PMANAGER.loadProject(project.getId(), KLAB.MFACTORY.getRootParsingContext());

        return project;
    }

    @Override
    public void deleteProject(String projectId) throws KlabException {

        IProject p = this.projects.get(projectId);
        if (p == null)
            throw new KlabResourceNotFoundException("project " + projectId
                    + " does not exist");
        File path = p.getLoadPath();

        undeployProject(projectId);

        try {
            FileUtils.deleteDirectory(path);
        } catch (IOException e) {
            KLAB.error(e);
            throw new KlabIOException(e);
        }
    }

    @Override
    public File archiveProject(String projectId) throws KlabException {

        IProject p = this.projects.get(projectId);

        if (p == null)
            throw new KlabResourceNotFoundException("project " + projectId
                    + " does not exist");

        return ((Project) p).getZipArchive();
    }

    @Override
    public IDependencyGraph getDependencyGraph() {
        return dependencyGraph;
    }

    @Override
    public boolean hasBeenLoaded() {
        return dependencyGraph != null;
    }

    public void notifyNewNamespace(Project project, String ns, File ret) {

        INamespace namespace = new KIMNamespace(ns, ret, project);
        project.namespaces.put(ns, namespace);
        ((KIMModelManager) KLAB.MMANAGER).addNamespace(namespace);
        //
        //
        // checkIn(ret, project, ns, KIMScope.forProject(project));
        for (IProjectLifecycleListener listener : listeners) {
            listener.namespaceAdded(ns, project);
        }
        // TODO add to model manager (not needed now because the clients do
        // that, but it
        // should be automatic)
    }

    @Override
    public Collection<INamespace> renameNamespace(INamespace namespace, String newName)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Call on closed projects to open it and register it. Otherwise opening a
     * non-existing project is a tough operation to do. TODO see if this should be floated
     * to the API, or there is a more elegant way.
     * 
     * @param files
     * @return project IDs
     * @throws KlabException
     */
    public IProject openAndRegisterProject(File file) throws KlabException {

        File check = new File(file + File.separator + "META-INF" + File.separator
                + ".closed");
        if (check.exists()) {
            FileUtils.deleteQuietly(check);
        }
        return registerProject(file);
    }

    public static boolean isOpen(File file) {
        File check = new File(file + File.separator + "META-INF" + File.separator
                + ".closed");
        return !check.exists();
    }

    @Override
    public Collection<IComponent> getComponents() {
        return ownComponents.values();
    }

    @Override
    public IComponent getComponent(String componentId) {
        return ownComponents.get(componentId);
    }

    @Override
    public IComponent getDeployedComponent(String componentId) throws KlabException {

        IComponent ret = getComponent(componentId);

        if (ret != null) {
            if (!ret.isActive()) {
                throw new KlabResourceNotFoundException("component " + componentId
                        + " exists in this engine but could not be initialized");
            }
            return ret;
        }

        ret = deployedComponents.get(componentId);
        if (ret != null) {
            if (!ret.isActive()) {
                throw new KlabResourceNotFoundException("component " + componentId
                        + " exists in this engine but could not be initialized");
            }
            return ret;
        }

        /*
         * sync the component so we have it. At the moment this can only be done once in
         * the lifetime of an engine, so just do it if it's not there. TODO if existing,
         * compare versions and warn if component has changed.
         */
        for (IServer node : KLAB.ENGINE.getNetwork().getNodes()) {
            if (node.provides(componentId)) {
                File compPath = ResourceFactory
                        .synchronizeComponent(node, componentId, componentDeployPath);
                ret = new Component(compPath);
                deployedComponents.put(componentId, ret);
                ((Component) ret).initialize(KLAB.ENGINE.getMonitor());

                if (!ret.isActive()) {
                    throw new KlabResourceNotFoundException("component " + componentId
                            + " could not be initialized");
                }

                /*
                 * load the knowledge
                 */
                loadComponent(ret, KLAB.MFACTORY.getRootParsingContext());

                KLAB.ENGINE.getMonitor().info("component " + componentId
                        + " loaded successfully", Messages.INFOCLASS_COMPONENT);
            }
        }

        return ret;
    }

    @Override
    public List<INamespace> loadComponent(IComponent component, IParsingScope scope)
            throws KlabException {
        HashSet<IProject> pread = new HashSet<>();
        List<INamespace> ret = loadProject((Component) component, pread, scope
                .get(component));
        return ret;
    }

    @Override
    public void clearWorkspace(boolean clearDeployed) throws KlabException {

        List<File> toDelete = new ArrayList<>();
        for (IProject project : getProjects()) {
            if ((project.isRemote() && !clearDeployed)
                    || ((Workspace) KLAB.WORKSPACE).isClientProject(project.getId())) {
                continue;
            }
            KLAB.info("clearing workspace: removing project " + project.getId());
            toDelete.add(project.getLoadPath());
            undeployProject(project.getId());
        }

        for (File f : toDelete) {
            try {
                FileUtils.deleteDirectory(f);
            } catch (IOException e) {
                KLAB.error("could not delete directory " + f
                        + ": project undeployed but files are still in place");
            }
        }

    }

    @Override
    public IComponent registerComponent(File compPath) throws KlabException {

        String componentId = extractComponentId(compPath);
        IComponent component = null;

        if (componentId == null) {
            return null;
        }

        if (ownComponents.containsKey(componentId)
                || deployedComponents.containsKey(componentId)) {
            return getDeployedComponent(componentId);
        }

        component = new Component(compPath);
        deployedComponents.put(component.getId(), component);
        ((Component) component).initialize(KLAB.ENGINE.getMonitor());

        if (!component.isActive()) {
            KLAB.error("component " + component.getId() + " could not be initialized");
        }

        /*
         * load the knowledge
         */
        loadComponent(component, KLAB.MFACTORY.getRootParsingContext());

        KLAB.ENGINE.getMonitor().info("component " + component.getId()
                + " loaded successfully", Messages.INFOCLASS_COMPONENT);

        return component;
    }

    private String extractComponentId(File compPath) {

        File propFile = new File(compPath + File.separator + "META-INF" + File.separator
                + "klab.properties");
        if (propFile.exists()) {
            try {
                Properties properties = PropertiesUtil.createFromFile(propFile);
                return properties.getProperty(Component.COMPONENT_ID_PROPERTY);
            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }

    @Override
    public INamespace reloadNamespace(INamespace ns) throws KlabException {

        INamespace ret = null;

        IProject project = ns.getProject();
        File resource = ns.getLocalFile();
        ResourceInfo info = resourceInfo.get(resource);

        if (info == null) {
            info = new ResourceInfo();
        }

        /*
         * update and store info before we read. If anything bad happens, the NS in the
         * descriptor will be null.
         */
        info.modificationDate = resource.lastModified();
        info.project = project;
        info.file = resource;
        resourceInfo.put(resource, info);
        IParsingScope context = KLAB.MFACTORY.getRootParsingContext().get(project);
        ret = KIM.parse(resource, context.forNamespace(ns.getId()));

        info.namespace = ret;
        if (ret == null) {
            ret = ((Project) project).addUnreadableNamespace(ns.getId(), resource);
        } else {
            ((Project) project).notifyNamespace(ret);
        }

        return ret;
    }

    public void overrideDeployed(IWorkspace workspace) throws KlabException {
        for (File pdir : workspace.getProjectLocations()) {
            String pname = MiscUtilities.getFileName(pdir.toString());
            IProject p = getProject(pname);
            if (p != null && !p.getLoadPath().equals(pdir)) {
                unregisterProject(pname);
                registerProject(pdir);
            }
        }
    }

    public synchronized boolean isParsing() {
        return isParsing.get();
    }

    public void exportFilesToProject(LocalExportResponse def, IProject prj) throws KlabIOException {

        File path = prj.getLoadPath();
        if (def.getRelativeExportPath() != null) {
            path = new File(path + File.separator + def.getRelativeExportPath());
            path.mkdirs();
        }

        for (String s : def.getFiles()) {
            String fname = MiscUtilities.getFileName(s);
            try {
                FileUtils.copyFile(new File(s), new File(path + File.separator + fname));
            } catch (IOException e) {
                throw new KlabIOException(e);
            }
        }
    }
}
