/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils.auth;

import java.io.File;
import java.io.IOException;

import javax.naming.AuthenticationException;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.common.auth.AuthConfig;

/**
 * Uses a lock file to establish a locking status.
 * @author Ferd
 *
 */
public class Lock {

    final File _file;

    public Lock(String fileName) throws AuthenticationException {
        _file = new File(AuthConfig.getSharedUserSpace() + File.separator + fileName);
    }

    public boolean lock() {

        synchronized (_file) {
            try {
                if (!_file.exists()) {
                    FileUtils.touch(_file);
                } else {
                    return false;
                }

            } catch (IOException e) {
                return false;
            }

            return true;
        }
    }

    public boolean unlock() {
        synchronized (_file) {
            if (isLocked()) {
                FileUtils.deleteQuietly(_file);
            }
            return !isLocked();
        }
    }

    public boolean isLocked() {
        synchronized (_file) {
            return _file.exists();
        }
    }

}
