/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.HashSet;
import java.util.Set;

/**
 * A simple filter for filetypes. It implements the {@code java.io.FilenameFilter} interface, so it can be used directly 
 * through the {@code java.io.File} methods, as {@code list()} and {@code listFile()}.
 * Furthermore, provides with implementations that search through subfolders.
 * Note that the matching is not case sensitive (i.e. xml and XML are the same endings).
 * 
 * Certain filetype endings used in Thinklab are added as public static attributes.
 * 
 * @author Ioannis N. Athanasiadis, Dalle Molle Institute for Artificial Intelligence, USI/SUPSI
 *
 * @since 19 Apr 2006
 */
public class FileTypeFilter implements FilenameFilter {
    public static String OWLFileType = ".owl";
    public static String RepositoryFileType = ".repository";
    public static String JavaFileType = ".java";
    public static String HBMFileType = ".hbm.xml";
    public static String HBCFileType = ".cfg.xml";
    public static String XMLFileType = ".xml";
    protected String pattern;
    private String fileseparator = System.getProperty("file.separator");

    /**
     * The default contructor that creates the filter 
     * 
     * @param str file type ending
     */
    public FileTypeFilter(String str) {
        pattern = str;
    }

    /* (non-Javadoc)
     * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
     */
    public boolean accept(File dir, String name) {
        return name.toLowerCase().endsWith(pattern.toLowerCase());
    }

    /**
     * Returns all files matching the filter as a set of files
     * @param folder to search in
     * @return Set of Files
     */
    public Set<File> listFilesSubFoldersIncluded(File folder) {
        HashSet<File> set = new HashSet<File>();
        listFilesSubFoldersIncludedHelper(set, folder);
        return set;
    }

    private void listFilesSubFoldersIncludedHelper(Set<File> set, File folder) {
        for (File file : folder.listFiles()) {
            if (file.isDirectory())
                listFilesSubFoldersIncludedHelper(set, file);
            else if (file.getName().toLowerCase().endsWith(pattern.toLowerCase()))
                set.add(file);
        }
    }

    /**
     * Returns all files matching the filter as a set of Strings relative to the root folder
     * @param folder to search in
     * @return Set of Strings
     */
    public Set<String> listSubFoldersIncluded(File folder) {
        HashSet<String> set = new HashSet<String>();
        listSubFoldersIncludedHelper(set, folder, "");
        return set;
    }

    private void listSubFoldersIncludedHelper(Set<String> set, File folder, String path) {
        for (File file : folder.listFiles()) {
            if (file.isDirectory())
                listSubFoldersIncludedHelper(set, file, path + file.getName() + fileseparator);
            else if (file.getName().toLowerCase().endsWith(pattern.toLowerCase()))
                set.add(path + file.getName());
        }
    }

}
