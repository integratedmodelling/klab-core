/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.Collection;
import java.util.Iterator;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Pass a collection (e.g. a clojure list of :kw value pairs) and retrieve pairs of key/value with the key 
 * converted to a string and the leading colon removed. Also performs minimal error checking and is null-tolerant.
 * 
 * @author Ferdinando
 *
 */
public class OptionListIterator implements Iterator<Pair<String, Object>> {

    Iterator<?> _it = null;

    public OptionListIterator(Object o) {
        if (o != null)
            _it = ((Collection<?>) o).iterator();
    }

    @Override
    public boolean hasNext() {
        return _it == null ? false : _it.hasNext();
    }

    @Override
    public Pair<String, Object> next() {
        String key = _it.next().toString();
        Object val = _it.next();
        if (!key.startsWith(":"))
            throw new KlabRuntimeException(
                    "keyword list improperly formatted: key is not a clojure keyword");
        key = key.substring(1);
        return new Pair<String, Object>(key, val);
    }

    @Override
    public void remove() {
        _it.remove();
        _it.remove();
    }

}
