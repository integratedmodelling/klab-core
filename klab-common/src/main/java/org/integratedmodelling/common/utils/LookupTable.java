/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Hashtable;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * A lookup table that can be initialized from a string using PERL syntax, extended to support numeric ranges for
 * values.
 * 
 * Pseudo-grammar for table:
 * 
 * table := pair*
 * pair := {value|range} => value 
 * value := IDENTIFIER | 'IDENTIFIER' // quotes are literals
 * range := [NUMBER:NUMBER] // []- are literals
 * 
 * @author Ferdinando Villa
 * @date October 1, 2007
 */
public class LookupTable {

    private boolean                                       hasRange    = false;
    String                                                literal     = null;
    private Hashtable<String, String>                     translation = null;
    // now that's fun
    private ArrayList<Pair<Pair<Double, Double>, String>> ranges      = null;

    private void mustBe(StreamTokenizer lex, int token) throws KlabException {

        int tok = 0;
        try {
            tok = lex.nextToken();
        } catch (IOException e) {
            throw new KlabIOException(e);
        }

        if (tok != token) {
            throw new KlabValidationException("lookup table: syntax error in string: " + literal);
        }

    }

    protected void initialize(String s) throws KlabException {

        StringReader input = new StringReader(s);
        StreamTokenizer lex = new StreamTokenizer(input);

        lex.wordChars('=', '=');
        lex.wordChars('>', '>');

        int token = 0;

        try {
            for (token = lex.nextToken(); token != StreamTokenizer.TT_EOF; token = lex.nextToken()) {

                String key = null;
                Pair<Double, Double> range = null;

                if (token == ';') {
                    token = lex.nextToken();
                    if (token == StreamTokenizer.TT_EOF)
                        break;
                }

                if (token == '[') {
                    Double min = null;
                    Double max = null;

                    token = lex.nextToken();
                    if (token == StreamTokenizer.TT_NUMBER) {
                        min = new Double(lex.nval);
                        token = lex.nextToken();
                    }

                    if (token == ':')
                        token = lex.nextToken();

                    if (token == StreamTokenizer.TT_NUMBER) {
                        max = new Double(lex.nval);
                        token = lex.nextToken();
                    }

                    if (token != ']')
                        throw new KlabValidationException("range specification should be [min-max]");

                    range = new Pair<Double, Double>(min, max);

                } else {
                    key = lex.sval;
                }

                lex.nextToken();
                if (!lex.sval.equals("=>")) {
                    throw new KlabValidationException("lookup table: each pair should be separated by =>");
                }

                token = lex.nextToken();
                String value = token == StreamTokenizer.TT_NUMBER ? removeZero(lex.nval) : lex.sval;

                if (key != null) {

                    if (translation == null)
                        translation = new Hashtable<String, String>();

                    translation.put(key, value);

                } else if (range != null) {

                    if (ranges == null)
                        ranges = new ArrayList<Pair<Pair<Double, Double>, String>>();

                    ranges.add(new Pair<Pair<Double, Double>, String>(range, value));
                }
            }
        } catch (IOException e) {
            throw new KlabIOException(e);
        }

        /* check that we don't have strange mixes of keys and ranges */
        if (ranges != null && translation != null)
            throw new KlabValidationException("lookup table: ranges are mixed with keys: only keys or ranges should be specified");
    }

    private String removeZero(double nval) {

        String s = Double.toString(nval);
        if (s.endsWith(".0"))
            s = s.substring(0, s.length() - 2);
        return s;
    }

    public LookupTable(String s) throws KlabException {
        initialize(s);
    }

    /**
     * Look value up in table. If value matches one of the keys or is a number in one of the ranges, the
     * correspondent value is returned. If no match is found, the original value is returned with no
     * modification.
     * 
     * @param value
     * @return value found or original value
     */
    public String lookup(String value) {

        String ret = value;

        if (ranges != null) {

            Double val = Double.parseDouble(value);
            for (Pair<Pair<Double, Double>, String> ks : ranges) {

                Double min = ks.getFirst().getFirst();
                Double max = ks.getFirst().getSecond();

                boolean ok = min == null || (val >= min);
                if (ok)
                    ok = max == null || (val < max);

                if (ok) {
                    ret = ks.getSecond();
                    break;
                }
            }

        } else if (translation != null) {

            if (translation.contains(value))
                ret = translation.get(value);

        }

        return ret;
    }

    /**
     * Lookup a double value in table with ranges.
     * 
     * @param val
     * @return the associated result, or null if no range matches.
     * @throws KlabValidationException if no ranges are defined.
     */
    public String lookup(double val) throws KlabValidationException {

        String ret = null;

        if (ranges == null) {
            throw new KlabValidationException("lookup table does not contain ranges");
        }
        for (Pair<Pair<Double, Double>, String> ks : ranges) {

            Double min = ks.getFirst().getFirst();
            Double max = ks.getFirst().getSecond();

            boolean ok = min == null || (val >= min);
            if (ok)
                ok = max == null || (val < max);

            if (ok) {
                ret = ks.getSecond();
                break;
            }
        }

        return ret;
    }

    public static void main(String[] args) {

        try {
            LookupTable t1 = new LookupTable("[-100:] => 1; [1:20] => 'ont:concept'; [21:] => 2");
            LookupTable t2 = new LookupTable("key1 => value1; key2 => value2");
        } catch (KlabException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
