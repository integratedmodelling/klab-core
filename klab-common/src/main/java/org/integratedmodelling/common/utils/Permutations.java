/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Permutations {

    private static <T> void permute(List<T> arr, int k, List<List<T>> addTo) {
        for (int i = k; i < arr.size(); i++) {
            java.util.Collections.swap(arr, i, k);
            permute(arr, k + 1, addTo);
            java.util.Collections.swap(arr, k, i);
        }
        if (k == arr.size() - 1) {
            addTo.add(new ArrayList<T>(arr));
        }
    }

    /**
     * Get all possible permutations of a given set.
     * 
     * @param objects
     * @return all permutations
     */
    public static <T> List<List<T>> getPermutations(List<T> objects) {

        List<List<T>> ret = new ArrayList<List<T>>();
        permute(objects, 0, ret);
        return ret;
    }

    /**
     * Get the powerset of a collection. Will also return the empty set.
     * 
     * @param originalSet
     * @return the powerset
     */
    public static <T> Set<Set<T>> powerSet(Collection<T> originalSet) {

        Set<Set<T>> sets = new HashSet<Set<T>>();
        if (originalSet.isEmpty()) {
            sets.add(new HashSet<T>());
            return sets;
        }
        List<T> list = new ArrayList<T>(originalSet);
        T head = list.get(0);
        Set<T> rest = new HashSet<T>(list.subList(1, list.size()));
        for (Set<T> set : powerSet(rest)) {
            Set<T> newSet = new HashSet<T>();
            newSet.add(head);
            newSet.addAll(set);
            sets.add(newSet);
            sets.add(set);
        }
        return sets;
    }

    public static void main(String[] args) {

        ArrayList<String> test = new ArrayList<>();
        test.add("uno");
        test.add("due");
        test.add("tre");

        System.out.println("--- permutations ----");

        for (List<String> s : getPermutations(test)) {
            System.out.println(Arrays.toString(s.toArray()));
        }

        System.out.println("--- powerset ----");

        for (Set<String> ps : powerSet(test)) {
            System.out.println(Arrays.toString(ps.toArray()));
        }
    }
}
