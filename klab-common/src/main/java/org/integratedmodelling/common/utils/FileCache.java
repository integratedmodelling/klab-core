/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * A simple persistent object that can check files in and out and is
 * aware of whether a file was modified more recently than its cached
 * version. Independent of the files themselves - just stores paths and
 * modification timestamps.
 * 
 * @author Ferd
 *
 */
public class FileCache {

    public final static String STORAGE_FILE_NAME = "kindex.xml";
    HashMap<File, Long> _cache = new HashMap<File, Long>();
    File _storage = null;

    public FileCache(String id) throws KlabException {
        _storage = new File(KLAB.CONFIG.getScratchArea(id) + File.separator + STORAGE_FILE_NAME);
        load();
    }

    /**
     * Clear any existing cache with this ID.
     * @param id
     */
    public static void clear(String id) {
        try {
            File d = KLAB.CONFIG.getScratchArea(id);
            if (d.exists())
                FileUtils.deleteDirectory(d);
        } catch (IOException e) {
            throw new KlabRuntimeException(e);
        }
    }

    public void clear() throws KlabException {
        _cache.clear();
        persist();
    }

    /**
     * Check a file in the cache; if the file wasn't there or was there with a former
     * date, return true. After calling this, the cache will be updated to reflect the
     * current file's date.
     * 
     * @param file
     * @return true if the cached file was outdated.
     * @throws KlabException 
     */
    public boolean checkIn(File file) throws KlabException {

        long f = file.lastModified();
        boolean outdated = !_cache.containsKey(file) || _cache.get(file) < f;

        if (outdated) {
            _cache.put(file, f);
            persist();
        }
        return outdated;
    }

    /**
     * Check a file in the cache with a user-specified modification date; if the file wasn't there or was there with a former
     * date, return true. After calling this, the cache will be updated to reflect the
     * current file's date.
     * 
     * @param file
     * @return true if the cached file was outdated.
     * @throws KlabException 
     */
    public boolean checkIn(File file, long f) throws KlabException {

        boolean outdated = !_cache.containsKey(file) || _cache.get(file) < f;

        if (outdated) {
            _cache.put(file, f);
            persist();
        }
        return outdated;
    }

    @SuppressWarnings("unchecked")
    public void load() throws KlabException {
        if (_storage.exists()) {
            try {
                XStream xstream = new XStream(new StaxDriver());
                BufferedReader input = new BufferedReader(new InputStreamReader(
                        new FileInputStream(_storage), "UTF-8"));
                _cache = (HashMap<File, Long>) xstream.fromXML(input);
                input.close();
            } catch (Exception e) {
                throw new KlabIOException(e);
            }

        }
    }

    public void persist() throws KlabException {
        XStream xstream = new XStream(new StaxDriver());
        String xml = xstream.toXML(_cache);
        try {
            FileUtils.writeStringToFile(_storage, xml);
        } catch (IOException e1) {
            throw new KlabIOException(e1);
        }

    }
}
