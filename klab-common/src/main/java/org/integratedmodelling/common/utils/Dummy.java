/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservable;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IStructure;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.agents.IAgentState;
import org.integratedmodelling.api.modelling.agents.ICollision;
import org.integratedmodelling.api.modelling.agents.IObservationController;
import org.integratedmodelling.api.modelling.agents.IObservationGraphNode;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.common.knowledge.Observable;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Provides dummies for various things where we just need model objects to carry concepts in
 * remote services and the like.
 * 
 * @author Ferd
 *
 */
public class Dummy {

    public static IActiveSubject subject(IConcept observable, IContext context, IScale scale, INamespace namespace) {
        return new Subject(observable, context, scale, namespace);
    }

    static class Subject extends Observation implements IActiveSubject {

        Observable observable;
        IScale     scale;
        INamespace namespace;

        public Subject(IConcept observable, IContext context, IScale scale, INamespace namespace) {
            this.observable = new Observable(observable, NameGenerator.shortUUID(), context, false);
            this.scale = scale;
            this.namespace = namespace;
        }

        @Override
        public IObservable getObservable() {
            return observable;
        }

        @Override
        public IScale getScale() {
            return scale;
        }

        @Override
        public Collection<IEvent> getEvents() {
            return new ArrayList<>();
        }


        @Override
        public IStructure getStructure(Locator... locators) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<IState> getStates() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<ISubject> getSubjects() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Collection<IProcess> getProcesses() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String getName() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public INamespace getNamespace() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IConcept getType() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IDirectObservation getContextObservation() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ICollision detectCollision(IObservationGraphNode myAgentState, IObservationGraphNode otherAgentState) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean doesThisCollisionAffectYou(IAgentState agentState, ICollision collision) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public ITransition reEvaluateStates(ITimePeriod timePeriod) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ITransition performTemporalTransitionFromCurrentState(IObservationController controller) throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<IProperty, IState> getObjectStateCopy() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IModel getModel() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IState getState(IObservableSemantics observable, Object... data) throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IState getStaticState(IObservableSemantics observable) throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void contextualize() throws KlabException {
            // TODO Auto-generated method stub
            
        }

//        @Override
//        public IResolutionStrategy getResolutionStrategy() {
//            // TODO Auto-generated method stub
//            return null;
//        }

        @Override
        public void addState(IState s) throws KlabException {
            // TODO Auto-generated method stub
            
        }

        @Override
        public IDataset getBackingDataset() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IEvent newEvent(IObservableSemantics observable, IScale scale, String name) throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ISubject newSubject(IObservableSemantics observable, IScale scale, String name, IProperty relationship)
                throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IProcess newProcess(IObservableSemantics observable, IScale scale, String name) throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public void addEventHandler(IAction action) {
            // TODO Auto-generated method stub
            
        }

        @Override
        public Collection<IConcept> getRolesFor(ISemantic observable) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public ICoverage getResolvedCoverage(ISemantic concept) {
            // TODO Auto-generated method stub
            return null;
        }
        
        @Override
        public IRelationship connect(IActiveSubject source, IActiveSubject destination, IConcept relationshipType, IScale relationshipScale)
                throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean setActive(boolean state) {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public boolean isActive() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public Collection<IObservation> getActionGeneratedObservations(boolean reset) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public IActuator getActuator() {
            // TODO Auto-generated method stub
            return null;
        }

//		@Override
//		public IObservationScanner get(Object... fiters) {
//			// TODO Auto-generated method stub
//			return null;
//		}
//
//		@Override
//		public IObservationScanner all() {
//			// TODO Auto-generated method stub
//			return null;
//		}

    }

}
