/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.ILock;
import org.integratedmodelling.common.configuration.KLAB;

/**
 * Uses a lock file to establish a locking status. Locks have a user associated.
 * 
 * @author Ferd
 *
 */
public class Lock implements ILock {

    final File file;
    IUser      user;
    boolean    forced = false;

    /**
     * @param fileName
     */
    public Lock(String fileName) {
        file = new File(KLAB.CONFIG.getDataPath() + File.separator + fileName);
        if (file.exists()) {
            KLAB.warn("removing stale lock file created " + new Date(file.lastModified()));
            org.integratedmodelling.common.utils.FileUtils.deleteQuietly(file);
        }
    }

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public synchronized boolean lock(IUser user, boolean force) {

        if (force) {
            this.user = user;
            this.forced = true;
            return true;
        }

        synchronized (file) {
            try {
                if (!file.exists()) {
                    FileUtils.touch(file);
                    this.user = user;
                    KLAB.info("engine lock established for " + user);
                } else {
                    return false;
                }

            } catch (IOException e) {
                return false;
            }

            return true;
        }
    }

    @Override
    public synchronized boolean unlock(IUser user) {

        if (!this.user.equals(user)) {
            return false;
        }

        if (forced) {
            forced = false;
            return true;
        }

        synchronized (file) {
            if (isLocked()) {
                FileUtils.deleteQuietly(file);
                KLAB.info("engine lock from " + this.user + " released");
                this.user = null;
            }
            return !isLocked();
        }
    }

    @Override
    public boolean isLocked() {

        if (forced) {
            return true;
        }

        synchronized (file) {
            return file.exists();
        }
    }

}
