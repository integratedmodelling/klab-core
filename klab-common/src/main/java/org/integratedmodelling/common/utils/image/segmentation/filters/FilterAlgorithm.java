package org.integratedmodelling.common.utils.image.segmentation.filters;

import org.integratedmodelling.common.utils.image.segmentation.ImageMatrix;

/**
 * 
 * Interfaz que deben implementar todas las clases que filtran una imagen antes
 * de segmentarla.
 * 
 */
public interface FilterAlgorithm {

	public void filter(ImageMatrix input, ImageMatrix output);
	
}
