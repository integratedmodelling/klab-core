/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Finds an object based on the class of the key. Not efficient - does a linear search every
 * time - so use with small sets. Also uses instanceof, so the first matching class will match, 
 * in order of insertion.
 * 
 * @author ferdinando.villa
 *
 * @param <T>
 */
public class TypedMap<T> implements Map<Class<?>, T> {

    Map<Class<?>, T> map     = new HashMap<>();
    List<Class<?>>   classes = new ArrayList<>();

    @Override
    public void clear() {
        map.clear();
        classes.clear();
    }

    @Override
    public boolean containsKey(Object arg0) {
        return findMatchingClass(arg0) != null;
    }

    private Class<?> findMatchingClass(Object arg0) {
        for (Class<?> cls : classes) {
            if (cls.isAssignableFrom(arg0 instanceof Class<?> ? (Class<?>) arg0 : arg0.getClass())) {
                return cls;
            }
        }
        return null;
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsValue(value);
    }

    @Override
    public Set<java.util.Map.Entry<Class<?>, T>> entrySet() {
        return map.entrySet();
    }

    @Override
    public T get(Object key) {
        Class<?> cls = findMatchingClass(key);
        return cls == null ? null : map.get(cls);
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public Set<Class<?>> keySet() {
        return map.keySet();
    }

    @Override
    public T put(Class<?> key, T value) {
        if (!map.containsKey(key)) {
            classes.add(key);
        }
        map.put(key, value);
        return value;
    }

    @Override
    public void putAll(Map<? extends Class<?>, ? extends T> m) {
        for (java.util.Map.Entry<? extends Class<?>, ? extends T> e : m.entrySet()) {
            put(e.getKey(), e.getValue());
        }
    }

    @Override
    public T remove(Object key) {
        T v = get(key);
        if (v != null) {
            map.remove(key);
            classes.remove(key);
        }
        return v;
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public Collection<T> values() {
        return map.values();
    }

}
