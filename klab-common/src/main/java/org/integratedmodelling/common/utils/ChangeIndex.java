/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.ArrayList;

import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * A simple list that indexes the changed states of a process and can relate a sequential
 * index to the latest changed state. Used to keep track of significant temporal transitions without
 * duplicating state at every time step.
 * 
 * There is at least one state indexed at 0.
 * 
 * @author ferdinando.villa
 *
 */
public class ChangeIndex extends ArrayList<Integer> {

    private static final long serialVersionUID = -819687628614555988L;

    /**
     * If one of these is installed, it will be called whenever notifyChangeAt sees
     * a transition that hadn't been seen already.
     * 
     * @author ferdinando.villa
     *
     */
    public static interface TransitionListener {
        void onTransition(int sequentialIndex, int stateIndex);
    }

    private TransitionListener listener;

    public ChangeIndex() {
        add(0);
    }

    public ChangeIndex(TransitionListener listener) {
        add(0);
        this.listener = listener;
    }

    /**
     * Get the index of the latest state that changed.
     * 
     * @return index
     */
    public int getLatestTransitionIndex() {
        return get(size() - 1);
    }

    /**
     * Get the index of the changed state that is closest in the history to the sequential state index passed.
     * 
     * @param sequentialIndex
     * @return index
     */
    public int getStateIndexFor(int sequentialIndex) {
        int idx = 0;
        for (int i : this) {
            if (i > sequentialIndex) {
                return idx;
            }
            idx = i;
        }
        return idx;
    }

    /**
     * Notify that a transition happened at the passed sequential index. If the transition hadn't been
     * seen before and a listener is installed, call it.
     * 
     * @param sequentialIndex
     * @return the history index of the new transition.
     */
    public int notifyChangeAt(int sequentialIndex) {

        if (getLatestTransitionIndex() > sequentialIndex) {
            throw new KlabRuntimeException("change index: cannot add a transition before the latest one: "
                    + sequentialIndex + " < " + getLatestTransitionIndex());
        }
        if (getLatestTransitionIndex() < sequentialIndex) {
            add(sequentialIndex);
            if (listener != null) {
                listener.onTransition(sequentialIndex, size() - 1);
            }
        }
        return size();
    }

    static public void main(String[] a) {

        ChangeIndex chind = new ChangeIndex(new TransitionListener() {
            @Override
            public void onTransition(int sequentialIndex, int stateIndex) {
                System.out.println("adding transition at " + sequentialIndex + " in position " + stateIndex);
            }
        });

        chind.notifyChangeAt(3);
        chind.notifyChangeAt(12);

        System.out.println("latest change was at " + chind.getLatestTransitionIndex());
        System.out.println("transition closest to 0 was " + chind.getStateIndexFor(0));
        System.out.println("transition closest to 5 was " + chind.getStateIndexFor(5));
        System.out.println("transition closest to 15 was " + chind.getStateIndexFor(15));

    }
}
