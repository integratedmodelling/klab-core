/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.HashMap;

/**
 * A simple class that parses a key-value string and initializes a map to its values.
 * TODO uses stupid "split" method and won't work with embedded spaces in values. Should be
 * redone with smarter tokenizer.
 * 
 * @author Ferdinando Villa
 *
 */
public class KeyValueMap extends HashMap<String, String> {

    private static final long serialVersionUID = 1123680512640721726L;

    private void initialize(String s, String separator) {

        String[] pairs = s.trim().split(separator);
        for (String p : pairs) {
            addPair(p);
        }
    }

    public void addPair(String p) {

        String[] kv = p.split("=");
        if (kv.length == 2) {
            String v = kv[1];
            if (v.startsWith("\"") || v.startsWith("'")) {
                v = v.substring(1);
            }
            if (v.endsWith("\"") || v.endsWith("'")) {
                v = v.substring(0, v.length() - 1);
            }
            put(kv[0], v);
        }
    }

    public KeyValueMap() {

    }

    public KeyValueMap(String string) {
        initialize(string, " ");
    }

    public KeyValueMap(String string, String separator) {
        initialize(string, separator);
    }

    public static void main(String[] args) {

        KeyValueMap kv = new KeyValueMap(" dio=\"ciao\" ostia=\"1.0\"");

        for (String entry : kv.keySet()) {

            System.out.println(entry + " = " + kv.get(entry));

        }
    }

    public int getInt(String key) {
        return Integer.parseInt(get(key));
    }

    public float getFloat(String key) {
        return Float.parseFloat(get(key));
    }

    public double getDouble(String key) {
        return Double.parseDouble(get(key));
    }

    public boolean getBoolean(String key) {
        return Boolean.parseBoolean(get(key));
    }

}
