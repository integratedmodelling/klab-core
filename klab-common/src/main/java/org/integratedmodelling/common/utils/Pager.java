/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.utils;

import java.util.Collection;

import org.integratedmodelling.exceptions.KlabException;

/** 
 * An object to assist organizing a collection of objects into pages, and the appropriate indexing and
 * visualization of the pages themselves.
 * 
 * @author Ferdinando Villa
 *
 */
public class Pager {

    int totalItems;
    int itemsPerPage;
    int nPagesToShow;
    int currentItem;
    int currentPage;
    int currentItemOffset;
    int totalPages;
    int itemsOnLastPage;

    private Collection<?> queryResult = null;

    /*
     * recalculate all parameters based on the current item index
     */
    private void calcBoundaries() throws KlabException {

        int formerPage = currentPage;

        /* ensure that the current item is not out of range */
        if (currentItem < 0 || totalItems == 0)
            currentItem = 0;
        else if (currentItem >= totalItems)
            currentItem = totalItems - 1;

        /* determine the current page shown */
        int cpg = currentItem / itemsPerPage;
        currentItemOffset = currentItem - cpg * itemsPerPage;

        totalPages = totalItems / itemsPerPage;
        itemsOnLastPage = totalItems - totalPages * itemsPerPage;

        if (itemsOnLastPage > 0)
            totalPages++;
        else
            itemsOnLastPage = itemsPerPage;

        /* recalc current page */
        currentPage = cpg;

        /* 
         * if we have a query object and we switched page, ask for the proper
         * results.
         */
        if (currentPage != formerPage && queryResult != null) {

            // queryResult.moveTo(currentItem, itemsPerPage);

        }

    }

    /**
     * 
     * @param totalItems the total number of items in the collection
     * @param itemsPerPage the number of items shown in one page
     * @param nPagesToShow the number of pages that we want to show in a pager index at one time 
     * 		(using ellipsis for previous and next pages).
     * @throws KlabException 
     */
    public Pager(int totalItems, int itemsPerPage, int nPagesToShow) throws KlabException {

        this.currentPage = 0;
        this.totalItems = totalItems;
        this.itemsPerPage = itemsPerPage;
        this.nPagesToShow = nPagesToShow;
        setCurrentItem(0, 0);
    }

    /**
     * Associate the pager with a query result. If this constructor is used, any
     * switch of the current page will cause the correspondent results to be 
     * loaded automatically from the queriable associated with the results object.
     * 
     * @param queryResult
     * @param itemsPerPage
     * @param nPagesToShow
     * @throws KlabException 
     */
    public Pager(Collection<?> queryResult, int itemsPerPage, int nPagesToShow) throws KlabException {

        this.currentItem = 0;
        this.currentPage = 0;
        this.totalItems = queryResult.size();
        this.queryResult = queryResult;
        this.itemsPerPage = itemsPerPage;
        this.nPagesToShow = nPagesToShow;
        setCurrentItem(0, 0);
    }

    /**
     * Return the total number of results in the query (which may have retrieved
     * only some of them).
     * @return total items
     */
    public int getTotalItemsCount() {
        return totalItems;
    }

    /*
     * set the current item index, adjust it to be within boundaries if necessary and
     * set the remaining parameters.
     */
    public void setCurrentItem(int index, int base) throws KlabException {
        currentItem = index - base;
        calcBoundaries();
    }

    public int getCurrentItem(int base) {
        return currentItem + base;
    }

    public int getNItemsOnPage(int pageIndex, int base) {
        return pageIndex == getLastPage(base) ? itemsOnLastPage : itemsPerPage;
    }

    public int getNItemsOnCurrentPage() {
        return getNItemsOnPage(currentPage, 0);
    }

    /**
     * Get the number of pages.
     * Just a synonym for the 1-based index of the last page.
     */
    public int getTotalPagesCount() {
        return getLastPage(1);
    }

    /**
     * Set the current page. The current item becomes the first item in the page.
     * @param pageIndex the page number (starting at 1).
     * @throws KlabException 
     */
    public void setCurrentPage(int pageIndex, int base) throws KlabException {

        pageIndex -= base;
        currentItem = pageIndex * itemsPerPage;
        calcBoundaries();
    }

    /**
     * The page that shows the current item. Starts at base.
     * @return current page
     */
    public int getCurrentPage(int base) {
        return currentPage + base;
    }

    /**
     * Last possible page number, which may be shown in the pager or not according to
     * how many items we want to show in the pager. Page numbering starts at 1.
     * @return last page
     */
    public int getLastPage(int base) {
        return totalItems <= 0 ? -1 : (totalPages - 1 + base);
    }

    /**
     * First page that we want to link to in the pager. First page returns base. No pages returns -1.
     * @return first page
     */
    public int getFirstPageInPager(int base) {

        int pofs = totalPages / currentPage;
        return totalItems == 0 ? -1 : pofs + base;
    }

    /**
     * Last page that we want to link to in the pager.
     * @return the page number starting from base, ready for display. Returns -1 if there are no pages to display.
     */
    public int getLastPageInPager(int base) {

        int ret = getFirstPageInPager(base) + nPagesToShow - 1;
        if (ret > getLastPage(base)) {
            ret = getLastPage(base);
        }
        return ret;
    }

    public int getFirstItemIndexInCurrentPage(int base) {
        return (getCurrentPage(0) * itemsPerPage) + base;
    }

    public int getLastItemIndexInCurrentPage(int base) {

        int lastIfPresent = getFirstItemIndexInCurrentPage(base)
                + getNItemsOnPage(getCurrentPage(base), base) - 1;

        return ((totalItems - 1) > lastIfPresent) ? lastIfPresent : (totalItems - 1);
    }

    /**
     * False if there is only zero or one page to show.
     * @return true if pager is needed
     */
    public boolean hasPager() {
        return totalPages > 1;
    }

    /**
     * If the first page in the pager is not 0, we need to show an ellipsis.
     * @return true if we need to show ellipsis at beginning
     */
    public boolean hasLeftEllipsis() {
        return getFirstPageInPager(0) > 0;
    }

    /**
     * If the last page in the pager is not the last possible page, we need to show an ellipsis.
     * @return true if we need to show ellipsis at end
     */
    public boolean hasRightEllipsis() {
        return getLastPageInPager(0) < getLastPage(0);
    }
}
