package org.integratedmodelling.common.utils.image.segmentation;

public interface SegmentationObserver {
	
	public void onChange();

	public void onComplete();
	
}
