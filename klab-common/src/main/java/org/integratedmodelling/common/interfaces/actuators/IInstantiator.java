package org.integratedmodelling.common.interfaces.actuators;

import org.integratedmodelling.api.modelling.IActiveDirectObservation;

public interface IInstantiator<T extends IActiveDirectObservation> extends IDirectActuator<T> { 

}
