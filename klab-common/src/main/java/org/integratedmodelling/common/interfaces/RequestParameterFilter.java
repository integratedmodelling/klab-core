package org.integratedmodelling.common.interfaces;

import org.integratedmodelling.api.data.IDataAsset;

/**
 * This can be implemented by those services that need to change or
 * filter the GET parameters in the original /get requests.
 * 
 * @author Ferd
 *
 */
public interface RequestParameterFilter {

    /**
     * Return the accepted value for the parameter, or null if it must
     * be removed.
     * 
     * @param key
     * @param value
     * @param resource
     * @return value or null
     */
    String filter(String key, String value, IDataAsset resource);

    /**
     * Return the accepted value for the header, or null if it must
     * be removed.
     * 
     * @param key
     * @param value
     * @param resource
     * @return value or null
     */
    String filterHeader(String key, String value, IDataAsset resource);

}
