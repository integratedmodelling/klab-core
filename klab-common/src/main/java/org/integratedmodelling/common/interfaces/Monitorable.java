package org.integratedmodelling.common.interfaces;

import org.integratedmodelling.api.monitoring.IMonitor;

/**
 * @author ferdinando.villa
 *
 */
public interface Monitorable {
    /**
     * @param monitor
     */
    void setMonitor(IMonitor monitor);
}
