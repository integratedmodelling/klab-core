package org.integratedmodelling.common.interfaces.actuators;

import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.runtime.IActuator;

/**
 * The actuator for a model that contextualizes an abstract concept into
 * a concrete one.
 * 
 * @author fvilla
 *
 */
public interface IConcretizingActuator extends IActuator {

	/**
	 * Return the set of contextually appropriate concrete concepts for the
	 * abstract one encountered in a dependency. All the concepts will be
	 * turned into dependencies and resolved.
	 * 
	 * @param abstractConcept
	 * @return
	 */
	Collection<IConcept> contextualizeConcept(IConcept abstractConcept);
	
	/**
	 * Return which one of the previously returned concepts applies for the
	 * passed context state, which defines which branch of the dataflow is
	 * used .
	 */
	IConcept getContextualizedConcept(int stateOffset);
	
}
