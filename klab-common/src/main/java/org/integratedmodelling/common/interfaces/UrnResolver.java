package org.integratedmodelling.common.interfaces;

import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.data.IDataAsset;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;

/**
 * @author ferdinando.villa
 *
 */
public interface UrnResolver {

    /**
     * @param urn the URN to resolve
     * @param authorization the authorization data, either from a user or from a set of
     *            remote privileges. May be null, if we allow unauthenticated connections
     *            to go through by returning true in
     *            {@link ResourceService#allowsUnauthenticated(String)}.
     * @param resourceConfiguration
     * @return the data record corresponding to the URN, or null.
     */
    IDataAsset resolveUrn(String urn, ConnectionAuthorization authorization, IResourceConfiguration resourceConfiguration);

}
