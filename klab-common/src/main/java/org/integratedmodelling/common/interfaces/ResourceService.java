package org.integratedmodelling.common.interfaces;

import org.integratedmodelling.api.data.IDataAsset;

/**
 * Tag interface for a service that can consume URNs and return resources. Two
 * subinterfaces define methods for the modalities of URN consumption: direct (translate
 * the URN directly into a resource) or indirect (receive a {@link IDataAsset} object from
 * a {@link UrnResolver} and translate the result into a resource).
 * 
 * @author ferdinando.villa
 *
 */
public interface ResourceService {

    /**
     * Check if we allow unauthenticated access to this URN. This only gets called if we
     * get a /get call without authentication.
     * 
     * @param urn
     * @return true if this can go through.
     */
    boolean allowsUnauthenticated(String urn);

}
