package org.integratedmodelling.common.interfaces;

import java.util.Map;

import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.data.IDataAsset;
import org.integratedmodelling.api.data.IDataService;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A resource handler makes available a generic mechanism for exchanging resources using
 * the engine /get endpoint. In order to work, resource handlers must be tagged with the
 * ResourceService annotation.
 * 
 * @author ferdinando.villa
 *
 */
public abstract interface IndirectResourceService extends ResourceService {

    /**
     * Check access to URN and if OK, translate it to an object that can be sent to the
     * requester.
     * 
     * Return value: if a bean is returned, it's used directly through JSON mapping. If
     * it's a file, its contents are sent over. If it's a URL, its contents are proxied
     * through the URL, after adding any parameters from the original request.
     *
     * @param assets
     * @param authorization the authorization data, either from a user or from a set of
     *            remote privileges. May be null, if we allow unauthenticated connections
     *            to go through by returning true in
     *            {@link ResourceService#allowsUnauthenticated(String)}.
     * @param element if passed, a specifier for a part of the request - e.g. a file in a
     *            project. These are given in URNs as fragments (#...) although they're
     *            turned into GET parameters before they can be passed to the server.
     * @param configuration
     * @param map request parameters, to allow us to filter request types and behave
     *            differently according to requests.
     * @return the resource, used according to conventions
     * @throws KlabException
     */
    Object resolveUrn(IDataAsset assets, IDataService service, ConnectionAuthorization authorization, IResourceConfiguration configuration, Map<String, String[]> map)
            throws KlabException;
}
