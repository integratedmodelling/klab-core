/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.interfaces.actuators;

import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;

/**
 * FIXME this should move into the engine package once factored out of all API
 * dependencies.
 * 
 * @author Ferd
 *
 */
public interface IStateActuator extends IActuator {

	IState getState();
	
    /**
     * Notifies the accessor of the observation semantics it is expected to express: the
     * observer passed is the one that the accessor has been linked to. In most cases this
     * will be expected to produce states corresponding to the key returned by
     * getSelfID().
     * 
     * @param observable
     * @param observer
     * @throws KlabException
     */
    void notifyObserver(IObserver observer) throws KlabException;

    /**
     * This method is called once per data dependency before any values are extracted,
     * passing the key that will be available for get() when values are extracted.
     * 
     * @param observable
     * @param observer
     * 
     * @param key
     *            the formal name of the parameter that will be passed to the
     * @param isMediation
     * @param accessor
     *            the observer that will be used to get the dependency.
     * 
     * @throws KlabException
     */
    void notifyInput(IObservableSemantics observable, IObserver observer, String key)
            throws KlabException;

    /**
     * Called at initialization to inform the accessor that it's expected to produce
     * states for the passed observable, and make them accessible by passing the given key
     * string to getValue(). Note that notifyObserver() will ALSO be passed the same
     * observer for the "main" observable of the model but this will be called also for
     * all other observers.
     * 
     * @param observable
     * @param observer
     * @param key
     * @throws KlabException
     */
    void notifyOutput(IObservableSemantics observable, IObserver observer, String key)
            throws KlabException;

    /**
     * Compute anything the accessor computes over the expected context (which will be one
     * state of the overall context our observer may have passed us)
     * 
     * After this is called, the appropriate getValue will be called to retrieve the
     * output(s).
     * 
     * NOTE: this may be called more than once with the same observable and different
     * names. It must be capable of handling that correctly.
     * 
     * @param stateIndex
     * @param transition
     * @throws KlabException
     * @returns the computed values (may contain more values due to actions and
     *          contextualizers).
     * 
     */
    Map<String, Object> process(int stateIndex, Map<String, Object> parameters, ITransition transition)
            throws KlabException;


    /**
     * If true, this actuator will produce probability distributions as states.
     * 
     * @return
     */
    boolean isProbabilistic();
    
    /**
     * If true, this actuator will produce a constant value.
     * @return
     */
    boolean isConstant();

    /**
     * Pass the chain of contextualizers we want the actuator to run.
     * 
     * @param contextualizers
     */
    void setContextualizers(List<IStateContextualizer> contextualizers);

    /**
     * One-step definition to take over the various notify* functions. Also passes the
     * resolution scope to enable interactive debugging when I can do it.
     * 
     * @param name
     * @param observer
     * @param contextObservation
     * @param scope
     * @param expectedInputs
     * @param expectedOutputs
     * @param monitor
     * @throws KlabException
     */
    void define(String name, IObserver observer, IActiveDirectObservation contextObservation, IResolutionScope scope, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, IMonitor monitor)
            throws KlabException;

    /**
     * Perform any action connected to the 'initialize' trigger, on the state as a whole.
     * 
     * @throws KlabException
     */
    void performPostInitializationActions() throws KlabException;
}
