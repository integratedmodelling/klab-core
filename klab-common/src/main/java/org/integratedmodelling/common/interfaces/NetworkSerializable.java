package org.integratedmodelling.common.interfaces;

import org.integratedmodelling.api.factories.IModelFactory;
import org.integratedmodelling.api.modelling.IModelBean;

/**
 * Implement this one to return a bean that can be sent and reconstructed through REST. The
 * {@link IModelFactory adapt()} method automatically use these if implemented.
 * 
 * @author ferdinando.villa
 *
 */
public interface NetworkSerializable {
    /**
     * @param desiredClass
     * @return a bean of the requested class from which the object can be reconstructed
     */
    <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass);
}
