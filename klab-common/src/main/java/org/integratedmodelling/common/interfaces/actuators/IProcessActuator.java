/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.interfaces.actuators;

import org.integratedmodelling.api.modelling.IActiveProcess;

/**
 * ISubjectAccessors can be attached to subject, process and event models and
 * when present, receive the fully computed state of the subject over the whole
 * context. They do not exist by default, but user accessors may be defined to
 * provide specific computations. Any complex model being wrapped semantically
 * is essentially a ISubjectAccessor as an entry point.
 * 
 * SubjectAccessors can modify the context for the object they handle and do
 * pretty much what they want, compatibly with the semantics. So they can also
 * handle context transformations such as GIS operations.
 * 
 * @author Ferd
 */
public interface IProcessActuator extends IDirectActuator<IActiveProcess> {

	IActiveProcess getProcess();

}
