package org.integratedmodelling.common.interfaces;

import org.integratedmodelling.api.modelling.IModelBean;

/**
 * Implement this one to ensure that objects can be created from network beans. The
 * ModelFactory methods will do the rest. Classes that implement this interface must have
 * an empty constructor.
 * 
 * @author ferdinando.villa
 *
 */
public interface NetworkDeserializable {
    /**
     * @param object
     */
    void deserialize(IModelBean object);
}
