package org.integratedmodelling.common.interfaces;

import java.util.Map;

import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A resource handler makes available a generic mechanism for exchanging resources using
 * the engine /get endpoint. In order to work, resource handlers must be tagged with the
 * ResourceService annotation.
 * 
 * @author ferdinando.villa
 *
 */
public abstract interface DirectResourceService extends ResourceService {

    /**
     * Check access to URN and if OK, translate it to an object that can be sent to the
     * requester.
     * 
     * Return value: if a bean is returned, it's used directly through JSON mapping. If
     * it's a file, its contents are sent over. If it's a URL, its contents are proxied
     * through the URL, after adding any parameters from the original request.
     * 
     * 
     * @param urn
     * @param element if passed, a specifier for a part of the request - e.g. a file in a
     *            project. These are given in URNs as fragments (#...) although they're
     *            turned into GET parameters before they can be passed to the server.
     * @param authorization the authorization data, either from a user or from a set of
     *            remote privileges. May be null, if we allow unauthenticated connections
     *            to go through by returning true in
     *            {@link ResourceService#allowsUnauthenticated(String)}.
     * @param configuration
     * @param map the arguments in the request: this gives us a chance to authenticate
     *            only for certain types of requests.
     * @return whatever we have resolved the URN to
     * @throws KlabException if things go wrong; make sure it's a
     *             KlabAuthorizationException if the problem is access to the URN data.
     */
    Object resolveUrn(String urn,  String element, ConnectionAuthorization authorization, IResourceConfiguration configuration, Map<String, String[]> map)
            throws KlabException;
}
