/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.visualization;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map.Entry;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.visualization.IColormap;
import org.integratedmodelling.api.modelling.visualization.ILegend;
import org.integratedmodelling.api.modelling.visualization.IMedia;
import org.integratedmodelling.api.modelling.visualization.IViewport;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.FileBackedDoubleVector;
import org.integratedmodelling.common.utils.FileBackedIntVector;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;

public class Legend implements ILegend {

    String label           = "";
    String description     = "";
    String longDescription = "";

    // added by the observer to qualify the data (typically a unit)
    String                   dataLabel     = "";
    /*
     * three levels of description.
     */
    HashMap<Integer, String> _labels       = new HashMap<Integer, String>();
    HashMap<Integer, String> _descriptions = new HashMap<Integer, String>();
    HashMap<Integer, String> _comments     = new HashMap<Integer, String>();

    /*
     * we just shuttle this one around
     */
    IColormap _colormap;

    /*
     * these contain the data - at most one of those is saved.
     */
    File displayDataFile;
    File numberDataFile;
    int  dataLength;

    public Legend(HashMap<Object, Integer> values, int[] data, double[] ddata, IObserver observer,
            double lowerBound, double upperBound, IColormap colormap, IMetadata metadata) {

        if (metadata != null) {
            this.label = metadata.getString(NS.DISPLAY_LABEL_PROPERTY);
        }

        if (label == null) {
            this.label = observer.getModel() != null ? observer.getModel().getId() : CamelCase
                    .toLowerCase(observer.getObservable().getLocalName(), '-');
        }

        dataLabel = observer.getMetadata().getString(NS.INTERNAL_DATA_LABEL_PROPERTY, "");

        if (values != null) {

            for (Entry<Object, Integer> i : values.entrySet()) {

                Object o = i.getKey();

                String vlab = o.toString();
                String vdes = o.toString();
                String vcom = o.toString();

                if (o instanceof IConcept) {
                    vlab = ((IConcept) o).getMetadata()
                            .getString(NS.DISPLAY_LABEL_PROPERTY, ((IConcept) o).getLocalName());
                    vdes = ((IConcept) o).getMetadata().getString(IMetadata.DC_LABEL, vdes);
                    vcom = ((IConcept) o).getMetadata().getString(IMetadata.DC_COMMENT, vcom);
                }

                _labels.put(i.getValue(), vlab);
                _descriptions.put(i.getValue(), vdes);
                _comments.put(i.getValue(), vcom);

            }
        }
    }

    @Override
    public IColormap getColormap() {
        return _colormap;
    }

    @Override
    public String getDescription(Object data, int type) {

        if (data == null || (data instanceof Double && Double.isNaN((Double) data))) {
            return "No data";
        }

        if (data instanceof Integer) {
            switch (type) {
            case ILegend.LABEL:
                return _labels.get(data) + (dataLabel.isEmpty() ? "" : (" " + dataLabel));
            case ILegend.SHORT_DESCRIPTION:
                return _descriptions.get(data);
            case ILegend.VERBOSE_DESCRIPTION:
                return _comments.get(data);
            }
        }

        return data.toString() + (dataLabel.isEmpty() ? "" : (" " + dataLabel));
    }

    @Override
    public String getDescription(int type) {

        switch (type) {
        case ILegend.LABEL:
            return label;
        case ILegend.SHORT_DESCRIPTION:
            return description;
        case ILegend.VERBOSE_DESCRIPTION:
            return longDescription;
        }
        return null;
    }

    @Override
    public Object getValue(int offset) {

        Object ret = null;

        if (numberDataFile != null) {

            try {
                FileBackedDoubleVector ll = new FileBackedDoubleVector(numberDataFile.toString(), dataLength);
                ret = ll.get(offset);
                ll.close();
            } catch (Exception e) {
                ret = null;
            }

        } else if (displayDataFile != null) {

            try {
                FileBackedIntVector ll = new FileBackedIntVector(displayDataFile.toString(), dataLength);
                ret = ll.get(offset);
                ll.close();
            } catch (Exception e) {
                ret = null;
            }
        }

        return ret;
    }

    // @Override
    // public MimeType getMIMEType() {
    // // TODO Auto-generated method stub
    // return null;
    // }

    @Override
    public ILegend getLegend() {
        return this;
    }

    @Override
    public IMedia scale(IViewport viewport) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getMediaType() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ByteArrayInputStream getStream() throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

}
