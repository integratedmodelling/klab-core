/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.visualization;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.visualization.IHistogram;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.exceptions.KlabRuntimeException;

import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * Serializable histogram with
 * @author Ferd
 *
 */
public class Histogram implements IHistogram, IModelBean, NetworkSerializable, NetworkDeserializable {

    private static final long serialVersionUID = -6099970020521809900L;

    String _description;

    int[]    _bins;
    double[] _boundaries;
    String[] _binLegends;

    /**
     * True if there are nodata values. Redundant.
     */
    boolean _nodata = false;

    long   _nodataCount     = 0;
    double _aggregatedMean  = 0;
    double _aggregatedTotal = Double.NaN;

    /**
     * When the data are not numeric, this contains the amount of occurrences per
     * category, using the string value of the object counted (which appears also in
     * _binLegends in the same order as the bins).
     */
    HashMap<String, Integer> _occurrences;

    /**
     * Count of nodata values in the data seen.
     */
    @Override
    public long getNoDataCount() {
        return _nodataCount;
    }

    @Override
    public int[] getBins() {
        return _bins;
    }

    @Override
    public boolean isEmpty() {
        return _nodata;
    }

    @Override
    public double[] getNumericBoundaries() {
        return _boundaries;
    }

    @Override
    public String[] getValueDescriptions() {
        return _binLegends;
    }

    /**
     * Aggregated mean is aware of the physical nature of what we observe and the nature
     * of the observation (aggregated or not).
     */
    @Override
    public double getAggregatedMean() {
        return _aggregatedMean;
    }

    /**
     * Aggregated total is NaN unless we're dealing with an extensive physical property,
     * in which case it's the total amount over the extents of space and time.
     */
    @Override
    public double getAggregatedTotal() {
        return _aggregatedTotal;
    }

    @Override
    public String getDescription() {
        return _description;
    }

    @Override
    public String toString() {
        XStream xstream = new XStream(new StaxDriver());
        return xstream.toXML(this);
    }

    public static Histogram fromString(String xml) {
        XStream xstream = new XStream(new StaxDriver());
        return (Histogram) xstream.fromXML(xml);
    }

    @Override
    public Image getImage(int w, int h) {

        int divs = _bins == null ? 0 : _bins.length;

        BufferedImage img = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        img.createGraphics();
        Graphics2D g = (Graphics2D) img.getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, w, h);
        if (_nodata || divs == 0) {
            g.setColor(Color.RED);
            g.drawLine(0, 0, w - 1, h - 1);
            g.drawLine(0, h - 1, w - 1, 0);
        } else {
            int max = max(_bins);
            int dw = w / divs;
            int dx = 0;
            g.setColor(Color.GRAY);
            for (int d : _bins) {
                int dh = (int) ((double) h * (double) d / max);
                g.fillRect(dx, h - dh, dw, dh);
                dx += dw;
            }
        }
        return img;
    }

    private int max(int[] a) {
        int ret = a[0];
        for (int i = 1; i < a.length; i++) {
            if (ret < a[i]) {
                ret = a[i];
            }
        }
        return ret;
    }

    /**
     * If the histogram represents occurrences of discrete categories, return the map of
     * each category name in the data to its numerosity. Otherwise, return null.
     * 
     * @return the key
     */
    public Map<String, Integer> getKey() {
        return _occurrences;
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Histogram)) {
            throw new KlabRuntimeException("cannot deserialize a Histogram from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Histogram bean = (org.integratedmodelling.common.beans.Histogram) object;

        // TODO Auto-generated method stub
        this._aggregatedMean = bean.getAggregatedMean();
        this._aggregatedTotal = bean.getAggregatedTotal();
        this._nodata = bean.isNodata();
        this._nodataCount = bean.getNodataCount();
        if (bean.getBinLegends() != null) {
            this._binLegends = bean.getBinLegends().toArray(new String[bean.getBinLegends().size()]);
        }
        if (bean.getBins() != null) {
            this._bins = Ints.toArray(bean.getBins());
        }
        if (bean.getBoundaries() != null) {
            this._boundaries = Doubles.toArray(bean.getBoundaries());
        }
        this._occurrences = bean.getOccurrences();
        this._description = bean.getDescription();

    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Histogram.class)) {
            throw new KlabRuntimeException("cannot serialize an Histogram to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Histogram ret = new org.integratedmodelling.common.beans.Histogram();

        ret.setAggregatedMean(_aggregatedMean);
        ret.setAggregatedTotal(_aggregatedTotal);
        ret.setDescription(_description);
        ret.setNodata(_nodata);
        ret.setNodataCount(_nodataCount);
        ret.setOccurrences(_occurrences);
        if (_binLegends != null) {
            ret.setBinLegends(Arrays.asList(_binLegends));
        }
        if (_bins != null) {
            ret.setBins(Ints.asList(_bins));
        }
        if (_boundaries != null) {
            ret.setBoundaries(Doubles.asList(_boundaries));
        }

        return (T) ret;
    }
}
