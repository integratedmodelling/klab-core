/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.visualization;

import java.awt.Color;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.data.IProbabilityDistribution;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.visualization.IColormap;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.states.State;
import org.integratedmodelling.common.storage.BooleanStorage;
import org.integratedmodelling.common.storage.ConceptStorage;
import org.integratedmodelling.common.storage.NumberStorage;
import org.integratedmodelling.common.visualization.knowledge.KnowledgeGraph;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabRuntimeException;

import com.google.common.primitives.Doubles;

/**
 * Provides static methods that will analyze and synthesize the data in a state. All the
 * methods save their results in the state metadata, so they can be called repeatedly
 * without fear of wasting resources.
 * 
 * @author Ferd
 *
 */
public class VisualizationFactory {

    private static final int HISTOGRAM_DEFAULT_LEVEL = 10;
    private static String    HAS_UNKNOWNS            = "__HAS_UNKNOWNS__";
    private static String    MISSING_CLASSIFICATION  = "__MISSING_CLASSIFICATION__";

    public static KnowledgeGraph getKnowledgeGraph(Object object, int depth, int flags) {
        KnowledgeGraph ret = new KnowledgeGraph();
        ret.define(object, depth, flags);
        return ret;
    }

    public static double[] getStateDataAsNumbers(IState state, Iterable<Locator> locators) {

        List<Double> ret = new ArrayList<>();
        IClassification classification = getClassification(state);
        List<IScale.Locator> ls = new ArrayList<>();
        for (IScale.Locator l : locators) {
            ls.add(l);
        }

        for (int n : state.getScale().getIndex(ls.toArray(new Locator[ls.size()]))) {

//            int i = state.getScale().getExtentOffset(state.getScale().getSpace(), n);
            Double d = Double.NaN;
            Object o = ((State) state).getValue(n);

            if (o instanceof Number) {
                d = ((Number) o).doubleValue();
            } else if (o instanceof IndexedCategoricalDistribution) {
                d = ((IndexedCategoricalDistribution) o).getMean();
            } else if (o instanceof Boolean) {
                d = ((Boolean) o) ? 1.0 : 0.0;
            } else if (o instanceof IConcept) {
                if (classification != null) {
                    d = classification.getNumericCode((IConcept) o);
                }
            } else if (o != null) {
                throw new KlabRuntimeException("internal: unexpected state value in VisualizationFactory.getStateData");
            }

            ret.add(d);
        }

        return Doubles.toArray(ret);
    }

    public static double[] getStateDataAsNumbers(IState state) {

        double[] ret = new double[(int) state.getValueCount()];
        IClassification classification = getClassification(state);

        for (int i = 0; i < state.getValueCount(); i++) {

            Double d = Double.NaN;
            Object o = ((State) state).getValue(i);

            if (o instanceof Number) {
                d = ((Number) o).doubleValue();
            } else if (o instanceof IndexedCategoricalDistribution) {
                d = ((IndexedCategoricalDistribution) o).getMean();
            } else if (o instanceof Boolean) {
                d = ((Boolean) o) ? 1.0 : 0.0;
            } else if (o instanceof IConcept) {
                if (classification != null) {
                    d = classification.getNumericCode((IConcept) o);
                }
            } else if (o != null) {
                throw new KlabRuntimeException("internal: unexpected state value in VisualizationFactory.getStateData");
            }

            ret[i] = d;
        }

        return ret;
    }

    /**
     * Get the histogram for a state without binning. Histogram is a list of objects with
     * their associated numerosity. Unknowns are not counted. Works also for numbers, but
     * on a continuously distributed state will produce a monster result as there is no
     * binning, so use only for .
     * 
     * Use
     * {@link #getHistogram(IState, org.integratedmodelling.api.modelling.IScale.Index, int)}
     * for a numeric state.
     * 
     * @param state
     * @return
     */
    public static List<Pair<Integer, Object>> getStateHistogram(IState state, Iterable<Locator> locators) {

        Map<Object, Integer> index = new HashMap<>();
        List<Pair<Integer, Object>> ret = new ArrayList<>();
        List<IScale.Locator> ls = new ArrayList<>();
        if (locators != null) {
            for (IScale.Locator l : locators) {
                ls.add(l);
            }
        }

        for (int n : state.getScale().getIndex(ls.toArray(new Locator[ls.size()]))) {

            Object o = ((State) state).getValue(n);
            if (o == null || o instanceof Number && Double.isNaN(((Number) o).doubleValue())) {
                continue;
            }

            Integer idx = index.get(o);
            if (idx == null) {
                Pair<Integer,Object> pair = new Pair<>(1, o);
                ret.add(pair);
                index.put(o, ret.size() - 1);
            } else {
                ret.get(idx).setFirst(ret.get(idx).getFirst() + 1);
            }
            
        }

        return ret;
    }

    /**
     * Return the histogram for the state. Note that other functions need the histogram
     * and will use the default number of subdivisions (10) unless this one is called
     * first thing with the levels wanted.
     * 
     * @param state
     * @param levels
     * @return the histogram
     */
    public static Histogram getHistogram(IState state, IScale.Index index, int levels) {

        Histogram ret = new Histogram();
        IUnit unit = null;
        boolean aggregateOverTime = false;
        boolean aggregateOverSpace = false;

        HashMap<Object, Integer> occurrences = new HashMap<>();

        IClassification discretization = null;
        if (state.getObserver() instanceof INumericObserver) {
            discretization = ((INumericObserver) state.getObserver()).getDiscretization();
        }

        if (state.getObserver() instanceof INumericObserver) {
            ret._boundaries = getBoundaries(state, index, false);
            if (Double.isNaN(ret._boundaries[0])) {
                ret._nodata = true;
                ret._nodataCount = state.getValueCount();
                ret._description = "only contains no-data";
                return ret;
            } else {
                ret._bins = new int[levels];
                ret._description = "Data range: " + NumberFormat.getInstance().format(ret._boundaries[0])
                        + " to " + NumberFormat.getInstance().format(ret._boundaries[1]);
            }

            if (state.getObserver() instanceof IMeasuringObserver) {
                unit = ((IMeasuringObserver) state.getObserver()).getUnit();
            }
        }

        if (unit != null) {
            if (state.getObserver().isExtensive(KLAB.c(NS.SPACE_DOMAIN))) {
                aggregateOverTime = !unit.isRate();
                aggregateOverSpace = !unit.isSpatialDensity(state.getSpace());
            }
        }

        double sum = 0;
        long nstat = 0;

        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {
            Object s = it.next();
            if (s == null || (s instanceof Double && Double.isNaN((Double) s))) {
                ret._nodataCount++;
            } else {
                double d = Double.NaN;
                if (s instanceof Number) {
                    d = ((Number) s).doubleValue();
                } else if (s instanceof IndexedCategoricalDistribution) {
                    d = ((IndexedCategoricalDistribution) s).getMean();
                } else if (s instanceof IConcept && discretization != null) {
                    d = discretization.getNumericCode((IConcept) s);
                }

                if (!Double.isNaN(d)) {
                    int bin = (int) ((d - ret._boundaries[0]) / (ret._boundaries[1] - ret._boundaries[0])
                            * (levels - 1));
                    ret._bins[bin]++;
                    sum += d;
                    nstat++;

                    // TODO handle aggregation

                } else {
                    Integer iv = occurrences.get(s);
                    iv = iv == null ? 1 : ++iv;
                    occurrences.put(s, iv);
                }
            }
        }

        if (occurrences.size() > 0) {
            ret._bins = new int[occurrences.size()];
            ret._binLegends = new String[occurrences.size()];
            ret._occurrences = new HashMap<>();
            int i = 0;
            for (Object o : occurrences.keySet()) {
                ret._bins[i] = occurrences.get(o);
                ret._binLegends[i] = o.toString();
                ret._occurrences.put(o.toString(), occurrences.get(o));
                i++;
            }
            ret._description = (state.getValueCount() - ret._nodataCount) + " values in "
                    + occurrences.size() + " categories";
        }

        if (ret._nodataCount > 0) {
            double ndp = (double) ret._nodataCount / (double) state.getValueCount();
            ret._description += " (" + NumberFormat.getPercentInstance().format(ndp) + " no data)";
        }

        if (nstat > 0) {
            ret._aggregatedMean = (sum / nstat);
            ret._aggregatedTotal = state.getObserver().isExtensive(KLAB.c(NS.SPACE_DOMAIN)) ? sum
                    : Double.NaN;
            ret._description += " [mean = "
                    + NumberFormat.getInstance().format(ret._aggregatedMean)
                    + (Double.isNaN(ret._aggregatedTotal) ? "" : ("; sum = " + NumberFormat.getInstance()
                            .format(ret._aggregatedTotal)))
                    + "]";
        }

        state.getMetadata().put(IMetadata.STATE_HISTOGRAM, ret);

        return ret;
    }

    /**
     * Get the actual boundaries in data. If every value is null, the boundaries will both
     * be NaN.
     * 
     * @param state
     * @return boundaries
     */
    public static double[] getBoundaries(IState state, IScale.Index index, boolean useOverallBoundaries) {

        double[] ret = new double[] { Double.NaN, Double.NaN };
        boolean hasUnknowns = false;

        IClassification discretization = null;
        if (state.getObserver() instanceof INumericObserver) {
            discretization = ((INumericObserver) state.getObserver()).getDiscretization();
        }

        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {
            Object s = it.next();
            double d = Double.NaN;
            if (s != null && !(s instanceof Double && Double.isNaN((Double) s))) {
                if (s instanceof Number) {
                    d = ((Number) s).doubleValue();
                } else if (s instanceof IndexedCategoricalDistribution) {
                    d = ((IndexedCategoricalDistribution) s).getMean();
                } else if (s instanceof Boolean) {
                    d = ((Boolean) s ? 1.0 : 0.0);
                } else if (s instanceof IConcept && discretization != null) {
                    d = discretization.getNumericCode((IConcept) s);
                }
                if (Double.isNaN(ret[0]) || ret[0] > d) {
                    ret[0] = d;
                }
                if (Double.isNaN(ret[1]) || ret[1] < d) {
                    ret[1] = d;
                }
            } else {
                hasUnknowns = true;
            }
        }

        if (useOverallBoundaries && !Double.isNaN(state.getStorage().getMin())
                && !Double.isNaN(state.getStorage().getMax())) {
            ret[0] = state.getStorage().getMin();
            ret[1] = state.getStorage().getMax();
        }

        if (hasUnknowns) {
            state.getMetadata().put(HAS_UNKNOWNS, new Boolean(true));
        }

        state.getMetadata().put(IMetadata.STATE_BOUNDARIES, ret);

        return ret;
    }

    /**
     * Analyze the data and check which type of visualization we should expect, plus
     * whether the colors should have a transparent zero that means "nothing".
     * 
     * @param state
     * @return state class
     */
    public static StateClass getStateClass(IState state, IScale.Index index) {

        // if (checkIndex(state, index)) {
        // if (state.getMetadata().get(IMetadata.STATE_CLASS) != null) {
        // return (StateClass) state.getMetadata().get(IMetadata.STATE_CLASS);
        // }
        // }

        IClassification classification = null;
        StateClass ret = new StateClass();

        double[] boundaries = getBoundaries(state, index, false);
        double lowerBound = boundaries[0];
        double upperBound = boundaries[1];

        IObserver observer = state.getObserver();

        // use flag put there by getBoundaries().
        ret.needsZeroInColormap = state.getMetadata().get(HAS_UNKNOWNS) != null;

        if (observer instanceof INumericObserver) {

            classification = ((INumericObserver) observer).getDiscretization();

            if (classification != null) {
                ret.type = StateClass.Type.DISCRETIZED_RANGES;
            } else if (!Double.isNaN(lowerBound) && !Double.isNaN(upperBound)) {
                ret.type = StateClass.Type.QUANTITATIVE_BOUNDED;
            } else {
                ret.type = StateClass.Type.QUANTITATIVE_UNBOUNDED;
            }

            // if we are unclassified and the lower bound is exactly 0, we want the map to
            // show zeros as
            // blank.
            if (!ret.needsZeroInColormap) {
                ret.needsZeroInColormap = classification == null && lowerBound == 0.0;
            }

        } else if (observer instanceof IClassifyingObserver) {

            classification = ((IClassifyingObserver) observer).getClassification();

            if (classification != null) {
                if (classification.isDiscretization()) {
                    ret.type = StateClass.Type.DISCRETIZED_RANGES;
                } else {
                    ret.type = classification.hasZeroRank() ? StateClass.Type.ORDERED_CLASSIFICATION_Z
                            : StateClass.Type.ORDERED_CLASSIFICATION_NZ;
                }
            } else {
                /*
                 * create an emergency classification from children - this is for
                 * classifications coming from datasources, accessors or other
                 * observations that do not have a full annotation.
                 */
                classification = new Classification(state.getObservable().getSemantics().getType(), true);
                state.getMetadata().put(MISSING_CLASSIFICATION, classification);
                ret.type = classification.hasZeroRank() ? StateClass.Type.ORDERED_CLASSIFICATION_Z
                        : StateClass.Type.ORDERED_CLASSIFICATION_NZ;
            }

        } /*
           * else if (observer instanceof ICategorizingObserver) { ret.type =
           * StateClass.Type.ENUMERATED; }
           */ else if (observer instanceof IPresenceObserver) {
            ret.type = StateClass.Type.BOOLEAN;
            ret.needsZeroInColormap = true;
        }

        state.getMetadata().put(IMetadata.STATE_CLASS, ret);

        return ret;
    }

    /**
     * Returns the boundaries stated in the relevant metadata or by the observer. If no
     * boundaries have been stated, returns null.
     * 
     * @param state
     * @return stated boundaries, or null
     */
    public static double[] getStatedBoundaries(IState state, IScale.Index index) {

        // if (checkIndex(state, index)) {
        // if (state.getMetadata().get(IMetadata.STATE_STATED_BOUNDARIES) != null) {
        // return (double[]) state.getMetadata().get(IMetadata.STATE_STATED_BOUNDARIES);
        // }
        // }

        IMetadata metadata = state.getMetadata();

        double lowerBound = Double.NaN;
        double upperBound = Double.NaN;

        if ((Double.isNaN(upperBound) || Double.isNaN(lowerBound))
                && state.getObserver() instanceof INumericObserver) {

            lowerBound = ((INumericObserver) (state.getObserver())).getMinimumValue();
            upperBound = ((INumericObserver) (state.getObserver())).getMaximumValue();
        }

        if (Double.isInfinite(lowerBound) && metadata.get(NS.LOWER_BOUND_PROPERTY) != null) {
            lowerBound = metadata.getDouble(NS.LOWER_BOUND_PROPERTY);
        }
        if (Double.isInfinite(upperBound) && metadata.get(NS.UPPER_BOUND_PROPERTY) != null) {
            upperBound = metadata.getDouble(NS.UPPER_BOUND_PROPERTY);
        }

        if (Double.isInfinite(upperBound) || Double.isInfinite(lowerBound))
            return null;

        double[] ret = new double[] { lowerBound, upperBound };

        state.getMetadata().put(IMetadata.STATE_STATED_BOUNDARIES, ret);

        return ret;
    }

    /**
     * Get the colormap for the state, honoring any specifications made with the metadata
     * along the provenance chain (that includes concepts with specific colors attached,
     * as long as every concept has one).
     * 
     * @param state
     * @return the colormap
     */
    public static IColormap getColormap(IState state, IScale.Index index) {

        // if (checkIndex(state, index)) {
        // if (state.getMetadata().get(IMetadata.STATE_COLORMAP) != null) {
        // return (IColormap) state.getMetadata().get(IMetadata.STATE_COLORMAP);
        // }
        // }

        String cmapDef = null;
        if (state.getMetadata().get(NS.COLORMAP_PROPERTY) != null) {
            cmapDef = state.getMetadata().getString(NS.COLORMAP_PROPERTY);
        }

        StateClass sclass = getStateClass(state, index);
        IColormap ret = makeColormapFromConcepts(state, index);
        Histogram histogram = getHistogram(state, index, HISTOGRAM_DEFAULT_LEVEL);

        int levels = 0;
        if (ret == null && cmapDef != null) {
            levels = getColorCount(state, index);
            ret = ColorMap.getColormap(cmapDef, levels, sclass.needsZeroInColormap);
        }

        if (ret /* still */ == null) {

            levels = getColorCount(state, index);
            if (histogram.getKey() != null) {
                ret = sclass.type.equals(StateClass.Type.ENUMERATED) ? ColorMap.random(levels)
                        : (levels < 10 ? (levels > 2 ? ColorMap
                                .getColormap("YlOrRd()", levels, sclass.needsZeroInColormap) : ColorMap
                                        .getColormap("Blues()", levels, sclass.needsZeroInColormap))
                                : ColorMap
                                        .jet(levels, sclass.needsZeroInColormap));
            } else {
                ret = ColorMap.jet(levels, sclass.needsZeroInColormap);
            }
        }

        state.getMetadata().put(IMetadata.STATE_COLORMAP, ret);

        return ret;
    }

    /**
     * Get the colormap for the state, honoring any specifications made with the metadata
     * along the provenance chain. Ignore index, i.e. use
     * 
     * @param state
     * @return the colormap
     */
    public static IColormap getColormap(IState state) {

        String cmapDef = null;
        if (state.getMetadata().get(NS.COLORMAP_PROPERTY) != null) {
            cmapDef = state.getMetadata().getString(NS.COLORMAP_PROPERTY);
        }

        IColormap ret = null;

        int levels = 0;
        if (cmapDef != null) {
            levels = getColorCount(state);
            ret = ColorMap.getColormap(cmapDef, levels, true);
        }

        if (ret /* still */ == null) {

            levels = getColorCount(state);
            if (state.getStorage() instanceof NumberStorage) {
                ret = ColorMap.jet(levels, true);
            } else if (state.getStorage() instanceof BooleanStorage) {
                ret = ColorMap.getColormap("Blues()", 2, true);
            } else {
                ret = ColorMap.random(levels);
            }
        }

        return ret;
    }

    private static int getColorCount(IState state) {
        if (state.getStorage() instanceof NumberStorage) {
            return 255;
        } else if (state.getStorage() instanceof BooleanStorage) {
            return 2;
        } else if (state.getStorage() instanceof ConceptStorage) {
            return getClassification(state).getConceptOrder().size()
                    + (getClassification(state).hasZeroRank() ? 0 : 1);
        }
        /*
         * TODO find a way.
         */
        return 12;
    }

    /**
     * Get the number of colors needed to represent the data. Does not compute a colormap.
     * 
     * @param state
     * @return color count
     */
    public static int getColorCount(IState state, IScale.Index index) {

        Histogram histogram = getHistogram(state, index, HISTOGRAM_DEFAULT_LEVEL);

        int ret = histogram.getNoDataCount() > 0 ? 256 : 255;
        if (histogram.getKey() != null) {
            ret = histogram.getNoDataCount() > 0 ? histogram.getKey().size() + 1 : histogram.getKey().size();
        }
        return ret;
    }

    /*
     * if we have a legend and it contains concepts, check concepts metadata; if they all
     * have colors associated, use those and that's it.
     */
    private static IColormap makeColormapFromConcepts(IState state, IScale.Index index) {

        Histogram histogram = getHistogram(state, index, HISTOGRAM_DEFAULT_LEVEL);

        IColormap ret = null;

        if (histogram.getKey() != null) {

            Color[] colors = new Color[histogram.getKey().size()];

            for (String o : histogram.getKey().keySet()) {

                IConcept c = KLAB.KM.getConcept(o);

                if (c == null) {
                    return null;
                }

                String col = c.getMetadata().getString(NS.COLOR_PROPERTY);
                if (col == null) {
                    return null;
                }

                Color color = null;
                try {
                    color = ColorStringParser.parse(col);
                } catch (Exception e) {
                    return null;
                }

                colors[histogram.getKey().get(o)] = color;
            }

            ret = new ColorMap(colors);
        }

        return ret;
    }

    /**
     * Get the indexed (0-255 max) data to map to a colormap for display.
     * 
     * @param state
     * @param index
     * @return indexed data
     */
    public static int[] getDisplayData(IState state, IScale.Index index, boolean useOverallBoundaries) {

        if (state.getStorage().getDataClass() == null) {
            return null;
        }

        int[] ret = null;

        if (state.getStorage().getDataClass().equals(String.class)) {
            ret = matchEnumeration(state, index);
        } else if (state.getStorage().getDataClass().equals(IProbabilityDistribution.class)) {
            ret = matchDistribution(state, index, useOverallBoundaries);
        } else if (Number.class.isAssignableFrom(state.getStorage().getDataClass())) {
            ret = matchNumbers(state, index, useOverallBoundaries);
        } else if (IConcept.class.isAssignableFrom(state.getStorage().getDataClass())) {
            if (state.getObserver() instanceof INumericObserver) {
                ret = matchNumbers(state, index, useOverallBoundaries);
            } else {
                ret = matchClassification(state, index, getClassification(state));
            }
        } else if (state.getStorage().getDataClass().equals(Boolean.class)) {
            ret = matchBooleans(state, index);
        }

        /**
         * this won't work if the scale is dynamic - don't remember things.
         */
        if (!state.getStorage().isDynamic()) {
            state.getMetadata().put(IMetadata.STATE_DISPLAY_DATA, ret);
        }
        return ret;
    }

    protected static IClassification getClassification(IState state) {

        IClassification ret = null;

        if (state.getObserver() instanceof INumericObserver) {
            ret = ((INumericObserver) state.getObserver()).getDiscretization();
        } else if (state.getObserver() instanceof IClassifyingObserver) {
            ret = ((IClassifyingObserver) state.getObserver()).getClassification();
        }

        if (ret == null) {
            ret = (IClassification) state.getMetadata().get(MISSING_CLASSIFICATION);
        }

        return ret;
    }

    private static int[] matchNumbers(IState state, IScale.Index index, boolean useOverallBoundaries) {

        int[] ret = new int[index.size()];

        double[] boundaries = getBoundaries(state, index, useOverallBoundaries);
        double[] boundstate = getStatedBoundaries(state, index);

        double lowerBound = boundstate == null ? boundaries[0] : boundstate[0];
        double upperBound = boundstate == null ? boundaries[1] : boundstate[1];

        // compute boundaries from data
        boolean isReal = false;
        boolean hasUnk = false;
        boolean isUnk = true;

        IClassification discretization = null;
        if (state.getObserver() instanceof INumericObserver) {
            discretization = ((INumericObserver) state.getObserver()).getDiscretization();
        }

        Double min = null, max = null;
        int i = 0;
        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {

            Object vs = it.next();
            if (!index.isActive(i)) {
                vs = null;
            }

            Double v = null;
            if (vs != null) {
                if (vs instanceof Boolean) {
                    v = ((Boolean) vs) ? 1.0 : Double.NaN;
                } else if (vs instanceof Number) {
                    v = ((Number) vs).doubleValue();
                } else if (vs instanceof IConcept && discretization != null) {
                    v = discretization.getNumericCode((IConcept) vs);
                }
            }

            if (v != null && !Double.isNaN(v)) {

                isUnk = false;

                if (min == null) {
                    min = v;
                } else {
                    if (v < min) {
                        min = v;
                    }
                }
                if (max == null) {
                    max = v;
                } else {
                    if (v > max) {
                        max = v;
                    }
                }

                if (!isReal && (v - Math.rint(v) != 0)) {
                    isReal = true;
                }

            } else {
                hasUnk = true;
            }
            i++;
        }

        if (isUnk) {
            return ret;
        }

        /*
         * if we have no-data, reserve the 0 for that and flag this so we can use
         * transparency. Otherwise 0 is a fine value and the colormap will decide how it's
         * displayed.
         */
        int offset = hasUnk ? 1 : 0;
        int levels = getColorCount(state, index);

        i = 0;
        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {

            Object o = it.next();
            Double v = null;
            if (index.isActive(i)) {
                if (o instanceof Boolean) {
                    v = ((Boolean) o) ? 1.0 : 0.0;
                } else if (o instanceof Number) {
                    v = ((Number) o).doubleValue();
                } else if (o instanceof IConcept && discretization != null) {
                    v = discretization.getNumericCode((IConcept) o);
                }
            }

            if (v == null || Double.isNaN(v)) {
                ret[i] = 0;
            } else {
                ret[i] = (int) (((v - lowerBound) / (upperBound - lowerBound)) * (levels - 1)) + offset;
                // ARGH FIXME
                if (ret[i] > 1) {
                    ret[i]--;
                }
            }

            i++;
        }

        return ret;
    }

    private static int[] matchClassification(IState state, IScale.Index index, IClassification classification) {

        HashMap<Object, Integer> values = new HashMap<>();

        int idx = 1;
        int i = 0;
        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {

            Object o = it.next();
            o = index.isActive(i) ? o : null;
            if (o != null && !values.containsKey(o)) {
                values.put(o, idx++);
            }
            i++;
        }

        i = 1;
        for (IConcept c : classification.getConceptOrder()) {
            values.put(c, i++);
        }

        int[] ret = new int[index.size()];

        i = 0;
        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {
            IConcept c = (IConcept) it.next();
            ret[i] = c == null ? 0 : (!index.isActive(i) ? 0 : values.get(c));
            // ARGH FIXME
            if (ret[i] > 1) {
                ret[i]--;
            }
            i++;
        }

        return ret;
    }

    private static int[] matchDistribution(IState state, IScale.Index index, boolean useOverallBoundaries) {

        /*
         * discrete distribution require both preserving the distribution values and
         * keeping the quantitative relationship of their means. This only uses the mean
         * for visualization.
         */
        int[] ret = new int[index.size()];

        double[] boundaries = getBoundaries(state, index, useOverallBoundaries);
        double[] boundstate = getStatedBoundaries(state, index);

        double lowerBound = boundstate == null ? boundaries[0] : boundstate[0];
        double upperBound = boundstate == null ? boundaries[1] : boundstate[1];

        // compute boundaries from data
        boolean isReal = false;
        boolean hasUnk = false;
        boolean isUnk = true;

        Double min = null, max = null;

        int i = 0;
        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {
            Object o = it.next();
            IndexedCategoricalDistribution dist = !index.isActive(i) ? null
                    : (IndexedCategoricalDistribution) o;
            if (dist != null) {

                double v = dist.getMean();
                isUnk = false;

                if (min == null) {
                    min = v;
                } else {
                    if (v < min) {
                        min = v;
                    }
                }
                if (max == null) {
                    max = v;
                } else {
                    if (v > max) {
                        max = v;
                    }
                }

                if (!isReal && (v - Math.rint(v) != 0)) {
                    isReal = true;
                }

            } else {
                hasUnk = true;
            }

            i++;
        }

        if (isUnk) {
            return ret;
        }

        /*
         * if we have no-data, reserve the 0 for that and flag this so we can use
         * transparency. Otherwise 0 is a fine value and the colormap will decide how it's
         * displayed.
         */
        int offset = hasUnk ? 1 : 0;
        int levels = hasUnk ? 255 : 256;

        i = 0;
        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {

            Object o = it.next();
            IndexedCategoricalDistribution dist = !index.isActive(i) ? null
                    : (IndexedCategoricalDistribution) o;
            double v = dist == null ? Double.NaN : dist.getMean();

            if (dist == null) {
                ret[i] = 0;
            } else {
                ret[i] = (int) (((v - lowerBound) / (upperBound - lowerBound)) * (levels - 1)) + offset;
                // ARGH FIXME
                if (ret[i] > 1) {
                    ret[i]--;
                }
            }
            i++;
        }

        return ret;
    }

    private static int[] matchEnumeration(IState state, IScale.Index index) {

        HashMap<Object, Integer> values = new HashMap<>();

        int idx = 1;
        int i = 0;

        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {
            Object o = it.next();
            if (!index.isActive(i))
                o = null;
            if (o != null && !values.containsKey(o)) {
                values.put(o, idx++);
            }
            i++;
        }

        int[] ret = new int[index.size()];
        i = 0;
        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {
            Object c = it.next();
            if (!index.isActive(i))
                c = null;
            ret[i] = c == null ? 0 : values.get(c);
            i++;
        }

        return ret;
    }

    private static int[] matchBooleans(IState state, IScale.Index index) {
        int i = 0;
        int[] ret = new int[index.size()];
        for (Iterator< ?>it = state.iterator(index); it.hasNext();) {
            Object c = it.next();
            if (!index.isActive(i))
                c = null;
            ret[i] = c != null && c instanceof Boolean && ((Boolean) c) ? 1 : 0;
            i++;
        }

        return ret;
    }

    /**
     * Visualize graph. If a user passed is not null, add the "client" node to it with the
     * user's ID.
     * 
     * @return visualizable graph
     */
    public static GraphVisualization visualizeNetwork() {

        GraphVisualization ret = new GraphVisualization();
        // ret.adapt(((Network) KLAB.NETWORK).getGraph(), new GraphAdapter<Object,
        // NetworkConnection>() {
        //
        // @Override
        // public String getNodeType(Object o) {
        // return o instanceof IUser ? "user" : (getNodeLabel(o).equals(KLAB.NAME) ?
        // "clientnode"
        // : "networknode");
        // }
        //
        // @Override
        // public String getNodeId(Object o) {
        // return o instanceof IUser ? ((IUser) o).getUsername() : ((IEngine)
        // o).getName();
        // }
        //
        // @Override
        // public String getNodeLabel(Object o) {
        // return o instanceof IUser ? (((IUser) o).getUsername() + " (" + getIdleTime(o)
        // + ")")
        // : ((IEngine) o).getName();
        // }
        //
        // private long getIdleTime(Object o) {
        // long now = new Date().getTime();
        // return (now - ((IUser) o).getLastLogin().getTime()) / 60000l;
        // }
        //
        // @Override
        // public String getNodeDescription(Object o) {
        // return "";
        // }
        //
        // @Override
        // public String getEdgeType(NetworkConnection o) {
        // // TODO connection or authentication if source == user node
        // return null;
        // }
        //
        // @Override
        // public String getEdgeId(NetworkConnection o) {
        // return o.getName(o.getSourceObject()) + "|" +
        // o.getName(o.getDestinationObject());
        // }
        //
        // @Override
        // public String getEdgeLabel(NetworkConnection o) {
        // return o.getSourceObject() instanceof IUser
        // || o.getName(o.getSourceObject()).equals(KLAB.NAME) ? "auth" : "";
        // }
        //
        // @Override
        // public String getEdgeDescription(NetworkConnection o) {
        // return "";
        // }
        // });

        return ret;
    }
}
