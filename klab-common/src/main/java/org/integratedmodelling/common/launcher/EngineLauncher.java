package org.integratedmodelling.common.launcher;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.ExecuteResultHandler;
import org.apache.commons.lang.StringUtils;
import org.apache.maven.shared.invoker.DefaultInvocationRequest;
import org.apache.maven.shared.invoker.DefaultInvoker;
import org.apache.maven.shared.invoker.InvocationRequest;
import org.apache.maven.shared.invoker.InvocationResult;
import org.apache.maven.shared.invoker.Invoker;
import org.apache.maven.shared.invoker.MavenInvocationException;
import org.apache.maven.shared.invoker.SystemOutLogger;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.collections.OS;
import org.integratedmodelling.common.client.ModelingClient;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.JavaUtils;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.common.utils.URLUtils;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * A launcher for an engine, handling both local Maven installs for developers and
 * official distributions synchronized from the network. The launcher also handles
 * cleaning and restarting a crashed engine if necessary.
 * 
 * @author Ferd
 *
 */
public abstract class EngineLauncher {

    private static final String LOGGING_FILE = "logging.file";

    public static interface LaunchListener {

        void shutdownStarted();
        
        /**
         * only called if Maven launch is done
         */
        void compilationSuccessful();

        /**
         * called if compilation happens and fails
         */
        void compilationFailed();

        /**
         * called after launch
         */
        void launchSuccessful();

        /**
         * called if launch fails
         * 
         * @param exception
         */
        void launchFailed(Throwable exception);
    }

    /**
     * 
     * @author ferdinando.villa
     *
     */
    public static class Source {
        boolean        isDevelop;
        File           jarFile;
        int            debugPort;
        public boolean useDebug;
        public boolean isLocal;
        public File    mavenDirectory;

        public boolean requiresRestart(Source previous) {
            if (isDevelop != previous.isDevelop) {
                // TODO!
            }
            return false;
        }
    }

    File                mavenDirectory;
    File                engineJar;
    File                engineJarDev;
    File                logFile;

    static final String BUILD_FILE_URL       = "http://www.integratedmodelling.org/downloads/build.txt";
    static final String BUILD_DEV_FILE_URL   = "http://www.integratedmodelling.org/downloads/build_dev.txt";

    int                 memLimitMb           = 2048;
    int                 networkPort          = 8183;
    File                dataDir              = null;
    File                certificate          = null;

    int                 debugPort            = 8897;

    boolean             launchAutomatically;
    boolean             upgradeAutomatically;
    boolean             stopOnExit;
    boolean             useLocal;
    boolean             useDeveloper;
    boolean             useDebug;

    // not tied to options - just set when necessary.
    boolean             offlineMode          = true;

    Integer             localBuildNumber     = null;
    Integer             localBuildNumberDev  = null;
    Integer             remoteBuildNumber    = null;
    Integer             remoteBuildNumberDev = null;

    String              context              = "/kmodeler";

    LaunchListener      listener;

    protected abstract void error(String error);

    /*
     * obsolete, as we require Java 8; if we didn't this would be a nice permsize constant
     * to use.
     */
    final int       PERMSIZE = 256;
    private String  clientSignature;
    private boolean klabDebug;

    /**
     * Create a launcher with the passed certificate and datadir. If a client signature is
     * passed, it is compared with anything found in .clientsig in the datadir, and if the
     * two are the same the engine will assume that it is running on the same filesystem
     * as the client and avoid web transfer for project synchronization.
     * 
     * @param certificate
     * @param dataDir
     * @param clientSignature
     */
    public EngineLauncher(File certificate, File dataDir, String clientSignature) {

        /*
         * TODO talk to the client and see if we have local engines already running. If
         * these do not correspond to our port, change message to notify that engine ops
         * will be allowed when the configured engine is the active one.
         */

        this.dataDir = dataDir;
        this.clientSignature = clientSignature;

        File certFile = new File(dataDir + File.separator + "im.cert");
        if (!certificate.equals(certFile)) {
            try {
                FileUtils.copyFile(certificate, certFile);
                this.certificate = certFile;
            } catch (IOException e) {
                throw new KlabRuntimeException(e);
            }
        }
        if (KLAB.CONFIG != null) {
            define(KLAB.CONFIG.getProperties());
        }
        this.logFile = new File(dataDir + File.separator + "logs");
        this.logFile.mkdirs();
        this.logFile = new File(this.logFile + File.separator + "klab.log");
    }

    public File getMavenDirectory() {
        return mavenDirectory;
    }

    /**
     * Determine the OS-dependent binary artifacts to make available to the engine and
     * ensure that they are available, downloading if necessary. In case of error it just
     * logs and returns false.
     */
    public boolean downloadExtBinaryArtifacts() {

        boolean ret = true;

        String ext = "win64";
        String[] libs = new String[] { "smile32.jar", "smile64.jar", "jsmile.dll" };
        if (KLAB.CONFIG.getOS().equals(OS.UNIX)) {
            ext = "linux";
            libs = new String[] {
                    "smile32.jar",
                    "smile64.jar",
                    "libjsmile.so",
                    "libjsmile32.so",
                    "libjsmile64.so" };
        } else if (KLAB.CONFIG.getOS().equals(OS.MACOS)) {
            ext = "osx";
            libs = new String[] { "smile.jar", "libjsmile.jnilib" };
        }
        File extdir = new File(KLAB.CONFIG.getDataPath("engine") + File.separator + "ext"
                + File.separator + ext);

        if (!extdir.exists()) {
            extdir.mkdirs();
            for (String lib : libs) {
                try {
                    URL url = new URL("http://www.integratedmodelling.org/downloads/extlibs/"
                            + lib);
                    URLUtils.copy(url, new File(extdir + File.separator + lib));
                } catch (Exception e) {
                    error("cannot download external binary artifacts from main server: functionalities may be limited: "
                            + e.getMessage());
                    KLAB.error("cannot download external binary artifacts from main server: functionalities may be limited: "
                            + e.getMessage());
                    ret = false;
                }
            }
        }

        return ret;
    }

    /**
     * Check for available upgrades.
     * 
     * @return true if the currently selected distribution can be upgraded.
     */
    public boolean canUpgrade() {
        Integer rb = getBuildNumber();
        if (rb == null) {
            return false;
        }
        if (useDeveloper) {
            return localBuildNumberDev == null || localBuildNumberDev < rb;
        }
        return localBuildNumber == null || localBuildNumber < rb;
    }

    public void setMavenDirectory(File mavenDirectory) {
        this.mavenDirectory = mavenDirectory;
    }

    public File getEngineJar() {
        return useDeveloper ? engineJarDev : engineJar;
    }

    public void setEngineJar(File engineJar) {
        this.engineJar = engineJar;
    }

    public void setEngineJarDev(File engineJarDev) {
        this.engineJarDev = engineJarDev;
    }

    public int getMemLimitMb() {
        return memLimitMb;
    }

    public void setMemLimitMb(int memLimitMb) {
        this.memLimitMb = memLimitMb;
    }

    public int getNetworkPort() {
        return networkPort;
    }

    public void setNetworkPort(int networkPort) {
        this.networkPort = networkPort;
    }

    public File getDataDir() {
        return dataDir;
    }

    public void setDataDir(File dataDir) {
        this.dataDir = dataDir;
    }

    public File getCertificate() {
        return certificate;
    }

    public void setCertificate(File certificate) {
        File certFile = new File(dataDir + File.separator + "im.cert");
        if (!certificate.equals(certFile)) {
            try {
                FileUtils.copyFile(certificate, certFile);
            } catch (IOException e) {
                throw new KlabRuntimeException(e);
            }
        }
        this.certificate = certFile;
    }

    public boolean isLaunchAutomatically() {
        return launchAutomatically;
    }

    public void setLaunchAutomatically(boolean launchAutomatically) {
        this.launchAutomatically = launchAutomatically;
    }

    public boolean isUpgradeAutomatically() {
        return upgradeAutomatically;
    }

    public void setUpgradeAutomatically(boolean upgradeAutomatically) {
        this.upgradeAutomatically = upgradeAutomatically;
    }

    public boolean isStopOnExit() {
        return stopOnExit;
    }

    public void setStopOnExit(boolean stopOnExit) {
        this.stopOnExit = stopOnExit;
    }

    public boolean isUseLocal() {
        return useLocal;
    }

    public void setUseLocal(boolean useLocal) {
        this.useLocal = useLocal;
    }

    public boolean isUseDeveloper() {
        return useDeveloper;
    }

    public void setUseDeveloper(boolean useDeveloper) {
        this.useDeveloper = useDeveloper;
        KLAB.CONFIG.getProperties().setProperty(IConfiguration.KLAB_ENGINE_USE_DEVELOPER_NETWORK, useDeveloper
                ? "true" : "false");
    }

    public boolean isUseDebug() {
        return useDebug;
    }

    public boolean isKlabDebug() {
        return this.klabDebug;
    }

    public void setUseDebug(boolean useDebug) {
        this.useDebug = useDebug;
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_USE_DEBUG, useDebug ? "true" : "false");
    }

    public void setKlabDebug(boolean klabDebug) {
        this.klabDebug = klabDebug;
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_KLAB_DEBUG, klabDebug ? "true" : "false");
    }

    private void define(Properties properties) {

        File engineJarFile = new File(KLAB.CONFIG.getDataPath("engine") + File.separator
                + "kmodeler.jar");
        if (engineJarFile.exists()) {
            engineJar = engineJarFile;
        }
        File engineJarFileDev = new File(KLAB.CONFIG.getDataPath("engine")
                + File.separator + "kmodeler_dev.jar");
        if (engineJarFileDev.exists()) {
            engineJarDev = engineJarFileDev;
        }

        String srcDir = properties.getProperty(IConfiguration.KLAB_SOURCE_DISTRIBUTION);
        if (srcDir != null) {
            File srcDirectory = new File(srcDir);
            if (srcDirectory.exists()) {
                mavenDirectory = srcDirectory;
            }
        }

        if (properties.containsKey(BUILD_DEV_PROPERTY)) {
            localBuildNumberDev = Integer
                    .parseInt(properties.getProperty(BUILD_DEV_PROPERTY));
        }
        if (properties.containsKey(BUILD_PROPERTY)) {
            localBuildNumber = Integer.parseInt(properties.getProperty(BUILD_PROPERTY));
        }

        if (properties.containsKey(MAX_MEMORY_PROPERTY)) {
            memLimitMb = Integer.parseInt(properties.getProperty(MAX_MEMORY_PROPERTY));
        }

        /*
         * reset defaults if previously stored
         */
        if (properties.getProperty(IConfiguration.KLAB_ENGINE_DATADIR) != null) {
            dataDir = new File(properties
                    .getProperty(IConfiguration.KLAB_ENGINE_DATADIR));
        }
        if (properties.getProperty(IConfiguration.KLAB_ENGINE_CERTIFICATE) != null) {
            certificate = new File(properties
                    .getProperty(IConfiguration.KLAB_ENGINE_CERTIFICATE));
        }

        useReasoning = Boolean.parseBoolean(properties
                .getProperty(IConfiguration.KLAB_REASONING, "false"));
        launchAutomatically = Boolean.parseBoolean(properties
                .getProperty(IConfiguration.KLAB_ENGINE_LAUNCH_AUTOMATICALLY, "true"));
        upgradeAutomatically = Boolean.parseBoolean(properties
                .getProperty(IConfiguration.KLAB_ENGINE_UPGRADE_AUTOMATICALLY, "true"));
        stopOnExit = Boolean.parseBoolean(properties
                .getProperty(IConfiguration.KLAB_ENGINE_SHUTDOWN_ON_EXIT, "false"));
        useLocal = Boolean.parseBoolean(properties
                .getProperty(IConfiguration.KLAB_ENGINE_USE_LOCAL_INSTALLATION, "false"));
        useDeveloper = Boolean.parseBoolean(properties
                .getProperty(IConfiguration.KLAB_ENGINE_USE_DEVELOPER_NETWORK, "false"));
        useDebug = Boolean.parseBoolean(properties
                .getProperty(IConfiguration.KLAB_ENGINE_USE_DEBUG, "false"));
        klabDebug = Boolean.parseBoolean(properties
                .getProperty(IConfiguration.KLAB_ENGINE_KLAB_DEBUG, "false"));
        debugPort = Integer.parseInt(properties
                .getProperty(IConfiguration.KLAB_ENGINE_DEBUG_PORT, "8893"));
    }

    /**
     * True if engine has already been launched.
     * 
     * @return true if engine is online
     */
    public boolean isOnline() {
        return NetUtilities.urlResponds("http://127.0.0.1:" + networkPort + context
                + "/capabilities");
    }

    public Source getActiveSource() {

        Source ret = new Source();
        ret.debugPort = debugPort;
        ret.useDebug = useDebug;
        ret.isDevelop = useDeveloper;
        ret.isLocal = useLocal;
        ret.jarFile = getEngineJar();
        ret.mavenDirectory = mavenDirectory;

        return ret;
    }

    /**
     * 
     * @param maxSeconds
     * @return true if engine is offline, whether as a result of the shutdown or for its
     *         own reasons.
     */
    public boolean shutdown(int maxSeconds) {

        if (isOnline()) {
            
            if (listener != null) {
                listener.shutdownStarted();
            }
            
            long timeout = 0;
            try (InputStream is = new URL("http://127.0.0.1:" + networkPort + context
                    + API.SHUTDOWN).openStream()) {
                //
            } catch (Exception e) {
                error("exception calling shutdown: " + e.getMessage());
                return false;
            }
            do {
                try {
                    Thread.sleep(500);
                    timeout += 500;
                } catch (Exception e) {
                }
            } while (isOnline() && timeout < (maxSeconds * 1000));

            if (timeout >= (maxSeconds * 1000) && isOnline()) {
                error("Timeout reached while stopping the engine. Please use OS facilities to stop it manually.");
            }

        }

        if (!isOnline()) {

            if (KLAB.ENGINE instanceof ModelingClient) {
                ((ModelingClient) KLAB.ENGINE).disconnect(false);
            }

            return true;
        }

        return false;
    }

    public boolean launch(boolean waitForEngine) {
        return launch(waitForEngine, null);
    }

    public boolean launch(boolean waitForEngine, LaunchListener listener) {

        this.listener = listener;

        if (!isOnline()) {

            downloadExtBinaryArtifacts();

            return (useLocal && mavenDirectory != null) ? launchMaven(waitForEngine)
                    : launchJar(waitForEngine);
        }
        return true;
    }

    /**
     * @param waitForRunning
     * @return true if successfully starting or started
     */
    public boolean launchMaven(boolean waitForRunning) {

        if (mavenDirectory != null) {

            if (System.getProperty("maven.home") == null) {
                String mavenHome = System.getenv("MAVEN_HOME");
                if (mavenHome != null) {
                    System.setProperty("maven.home", mavenHome);
                }
                /*
                 * if not, give it a try anyway - on some architectures it succeeds
                 * nevertheless. In case, error message will be seem at launch.
                 */
            }

            String ext = "ext" + File.separator + "win64";
            if (KLAB.CONFIG.getOS().equals(OS.UNIX)) {
                ext = "ext/linux";
            } else if (KLAB.CONFIG.getOS().equals(OS.MACOS)) {
                ext = "ext/osx";
            }

            File cPath = KLAB.CONFIG.getDataPath("engine");

            final InvocationRequest compile = new DefaultInvocationRequest();
            final InvocationRequest request = new DefaultInvocationRequest();

            String mavenOptions = "";

            for (String s : JavaUtils.getOptions(512, memLimitMb, true, PERMSIZE)) {
                mavenOptions += (mavenOptions.isEmpty() ? "" : " ") + s;
            }

            if (useDebug) {
                mavenOptions += (mavenOptions.isEmpty() ? "" : " ")
                        + "-Xdebug -Xbootclasspath/p:lib/jsr166.jar -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address="
                        + debugPort;
            }

            mavenOptions += (mavenOptions.isEmpty() ? "" : " ") + "-Djava.library.path="
                    + cPath + File.separator + ext;

            request.setMavenOpts(mavenOptions);

            request.setBaseDirectory(new File(mavenDirectory + File.separator
                    + "klab-modeler"));
            request.setRecursive(true);
            request.setPomFile(new File(mavenDirectory + File.separator + "klab-modeler"
                    + File.separator + "pom.xml"));
            request.setGoals(Arrays.asList("spring-boot:run"));

            compile.setBaseDirectory(mavenDirectory);
            compile.setRecursive(true);
            compile.setPomFile(new File(mavenDirectory + File.separator + "pom.xml"));
            compile.setGoals(Arrays.asList("install"));

            compile.setOffline(offlineMode);

            Properties properties = new Properties();
            properties
                    .setProperty(IConfiguration.KLAB_DATA_DIRECTORY, dataDir.toString());
            properties.setProperty("server-port", "" + networkPort);
            properties.setProperty(IConfiguration.KLAB_REASONING, useReasoning ? "true"
                    : "false");
            properties.setProperty(IConfiguration.KLAB_ENGINE_USE_DEVELOPER_NETWORK, useDeveloper ? "true"
                    : "false");
            properties.setProperty(IConfiguration.KLAB_OFFLINE, KLAB.CONFIG.isOffline() ? "true"
                    : "false");
            properties.setProperty(LOGGING_FILE, logFile.toString());
            if (KLAB.CONFIG.isOffline()) {
                // we'll need these
                properties.setProperty(IConfiguration.KLAB_CLIENT_PROJECTS, StringUtils
                        .join(KLAB.WORKSPACE.getProjectLocations(), ","));
            }
            if (KLAB.CONFIG.getProperties()
                    .getProperty(IConfiguration.KLAB_LOCAL_COMPONENTS) != null) {
                properties.setProperty(IConfiguration.KLAB_LOCAL_COMPONENTS, KLAB.CONFIG
                        .getProperties()
                        .getProperty(IConfiguration.KLAB_LOCAL_COMPONENTS));
            }
            request.setProperties(properties);

            status = Status.STARTING;

            /*
             * spawn Maven task in thread and wait for engine to be seen online.
             */
            new Thread() {

                @Override
                public void run() {

                    final Invoker invoker = new DefaultInvoker();

                    invoker.setLogger(new SystemOutLogger());

                    InvocationResult result;
                    try {
                        result = invoker.execute(compile);
                        if (listener != null) {
                            if (result.getExitCode() == 0) {
                                listener.compilationSuccessful();
                            } else {
                                listener.compilationFailed();
                            }
                        }
                        result = invoker.execute(request);
                    } catch (MavenInvocationException e) {
                        synchronized (status) {
                            status = Status.ERROR;
                        }
                        if (listener != null) {
                            listener.launchFailed(e);
                        }
                        return;
                    } catch (IllegalStateException e) {
                        synchronized (status) {
                            error(e.getMessage());
                            status = Status.ERROR;
                            if (listener != null) {
                                listener.launchFailed(e);
                            }
                        }
                        return;
                    }

                    if (result.getExitCode() != 0) {
                        if (result.getExecutionException() != null) {
                            synchronized (status) {
                                status = Status.ERROR;
                            }
                            if (listener != null) {
                                listener.launchFailed(result.getExecutionException());
                            }
                            return;
                        } else {
                            synchronized (status) {
                                status = Status.STOPPED;
                            }
                            if (listener != null) {
                                listener.launchFailed(null);
                            }
                            return;
                        }
                    } else {
                        if (listener != null) {
                            listener.launchSuccessful();
                        }
                    }
                }
            }.start();
        }

        /*
         * Note: multiplying timeout by 10 if we're using Maven.
         */

        if (waitForRunning) {
            long timeout = 0;
            do {
                try {
                    Thread.sleep(500);
                    timeout += 500;
                } catch (Exception e) {
                }
            } while (!isOnline() && timeout < (TIMEOUT * 10));

            if (timeout >= (TIMEOUT * 10) && !isOnline()) {
                error("Timeout reached while starting the engine. Please check configuration and computer requirements.");
            } else {
                status = Status.RUNNING;
            }
        }

        return status != Status.STOPPED;
    }

    enum Status {
        STOPPED,
        STARTING,
        STOPPING,
        RUNNING,
        ERROR;
    };

    Status                     status              = Status.STOPPED;

    /*
     * default timeout 1min until we decide that the server didn't start up properly. FV
     * made it 2mins to accommodate BC3 worst-in-their-class laptops for modelers, or long
     * Maven builds.
     */
    private static long        TIMEOUT             = 120000;

    public static final String BUILD_DEV_PROPERTY  = "klab.build.dev";
    public static final String BUILD_PROPERTY      = "klab.build";
    public static final String MAX_MEMORY_PROPERTY = "klab.max.memory";

    /**
     * Launch (if necessary) the selected Jar distribution. If waitForRunning is true,
     * return only when engine is responsive or after error. Otherwise return during
     * startup. Check isOnline() to see what happened.
     * 
     * @param waitForRunning
     * @return true if operation went as expected
     */
    public boolean launchJar(boolean waitForRunning) {

        String ext = "ext" + File.separator + "win64";
        if (KLAB.CONFIG.getOS().equals(OS.UNIX)) {
            ext = "ext/linux";
        } else if (KLAB.CONFIG.getOS().equals(OS.MACOS)) {
            ext = "ext/osx";
        }

        File cPath = KLAB.CONFIG.getDataPath("engine");

        CommandLine cmdLine = new CommandLine(JavaUtils.getJavaExecutable());

        for (String s : JavaUtils.getOptions(512, memLimitMb, true, PERMSIZE)) {
            cmdLine.addArgument(s);
        }

        if (useDebug) {
            cmdLine.addArgument("-Xdebug");
            cmdLine.addArgument("-Xbootclasspath/p:lib/jsr166.jar");
            cmdLine.addArgument("-Xrunjdwp:transport=dt_socket,server=y,suspend=n,address="
                    + debugPort);
        }

        cmdLine.addArgument("-Djava.library.path=" + cPath + File.separator + ext);
        cmdLine.addArgument("-D" + IConfiguration.KLAB_DATA_DIRECTORY + "=" + dataDir);
        cmdLine.addArgument("-Dserver-port=" + networkPort);
        cmdLine.addArgument("-Dlogging.file=" + logFile);
        cmdLine.addArgument("-D" + IConfiguration.KLAB_REASONING + "=" + (useReasoning ? "true" : "false"));
        cmdLine.addArgument("-D" + IConfiguration.KLAB_ENGINE_USE_DEVELOPER_NETWORK + "="
                + (useDeveloper ? "true" : "false"));
        cmdLine.addArgument("-D" + IConfiguration.KLAB_OFFLINE + "="
                + (KLAB.CONFIG.isOffline() ? "true" : "false"));
        if (KLAB.CONFIG.isOffline()) {
            // we're gonna need these
            cmdLine.addArgument("-D" + IConfiguration.KLAB_CLIENT_PROJECTS + "="
                    + StringUtils.join(KLAB.WORKSPACE.getProjectLocations(), ","));
        }
        if (KLAB.CONFIG.getProperties()
                .getProperty(IConfiguration.KLAB_LOCAL_COMPONENTS) != null) {
            cmdLine.addArgument("-D" + IConfiguration.KLAB_LOCAL_COMPONENTS + "="
                    + KLAB.CONFIG.getProperties()
                            .getProperty(IConfiguration.KLAB_LOCAL_COMPONENTS));
        }

        cmdLine.addArgument("-jar");
        cmdLine.addArgument(useDeveloper ? engineJarDev.toString()
                : engineJar.toString());

        DefaultExecutor executor = new DefaultExecutor();
        executor.setWorkingDirectory(cPath);

        Map<String, String> env = new HashMap<>();
        env.putAll(System.getenv());

        status = Status.STARTING;

        try {
            KLAB.info("starting engine from " + cPath);
            executor.execute(cmdLine, env, new ExecuteResultHandler() {

                @Override
                public void onProcessFailed(ExecuteException arg0) {
                    synchronized (status) {
                        status = Status.STOPPED;
                    }
                    if (listener != null) {
                        listener.launchFailed(arg0);
                    }
                }

                @Override
                public void onProcessComplete(int arg0) {
                    synchronized (status) {
                        status = Status.STOPPED;
                    }
                }
            });
        } catch (Exception e) {
            status = Status.STOPPED;
        }

        if (waitForRunning && status == Status.STARTING) {
            long timeout = 0;
            do {
                try {
                    Thread.sleep(500);
                    timeout += 500;
                } catch (Exception e) {
                }
            } while (!isOnline() && timeout < TIMEOUT);

            if (timeout >= TIMEOUT && !isOnline()) {
                error("Timeout reached while starting the engine. Please check configuration and computer requirements.");
                if (listener != null) {
                    listener.launchFailed(null);
                }
            } else {
                status = Status.RUNNING;
                if (listener != null) {
                    listener.launchSuccessful();
                }
            }
        }

        return status != Status.STOPPED;
    }

    /**
     * 
     */
    public void persistProperties() {

        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_LAUNCH_AUTOMATICALLY, launchAutomatically
                        ? "true" : "false");
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_UPGRADE_AUTOMATICALLY, upgradeAutomatically
                        ? "true" : "false");
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_SHUTDOWN_ON_EXIT, stopOnExit
                        ? "true" : "false");
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_USE_LOCAL_INSTALLATION, useLocal
                        ? "true" : "false");
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_USE_DEVELOPER_NETWORK, useDeveloper
                        ? "true" : "false");
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_KLAB_DEBUG, klabDebug ? "true"
                        : "false");
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_USE_DEBUG, useDebug ? "true"
                        : "false");
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_DEBUG_PORT, "" + debugPort);
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_DATADIR, dataDir.toString());
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_ENGINE_CERTIFICATE, certificate
                        .toString());
        KLAB.CONFIG.getProperties()
                .setProperty(IConfiguration.KLAB_REASONING, useReasoning ? "true"
                        : "false");
        KLAB.CONFIG.getProperties().setProperty(MAX_MEMORY_PROPERTY, memLimitMb + "");
        if (mavenDirectory != null) {
            KLAB.CONFIG.getProperties()
                    .setProperty(IConfiguration.KLAB_SOURCE_DISTRIBUTION, mavenDirectory
                            .toString());
        }
        KLAB.CONFIG.save();
    }

    /**
     * Must be called after stopping the engine
     */
    public void runCleaningCycle() {

        if (isOnline()) {
            error("cannot run a cleaning cycle with a running engine");
            return;
        }

        File kboxDir = new File(KLAB.CONFIG.getDataPath("engine") + File.separator
                + "kbox");

        if (runModelClean && kboxDir.exists()) {
            for (File file : kboxDir.listFiles()) {
                if (file.isFile() && MiscUtilities.getFileName(file.toString())
                        .startsWith("models2_")) {
                    FileUtils.deleteQuietly(file);
                }
            }
        }
        if (runDataClean) {
            File cacheDir = new File(KLAB.CONFIG.getDataPath("engine") + File.separator
                    + ".scratch");
            if (cacheDir.exists() && cacheDir.isDirectory()) {
                try {
                    FileUtils.deleteDirectory(cacheDir);
                } catch (IOException e) {
                    error("could not delete directory " + cacheDir);
                }
            }
        }
        if (runObsClean && kboxDir.exists()) {
            for (File file : kboxDir.listFiles()) {
                if (file.isFile() && MiscUtilities.getFileName(file.toString())
                        .startsWith("observations_")) {
                    FileUtils.deleteQuietly(file);
                }
            }
        }

    }

    /*
     * TODO
     */
    String[]        additionalRelativePaths = new String[] {};
    private boolean runGC;
    private boolean runObsClean;
    private boolean runDataClean;
    private boolean runModelClean;
    private boolean runClean;

    // non-persistent, just to capture user choice from GUI.
    private boolean rememberSettings        = true;
    private boolean stopEngine              = false;
    private boolean useReasoning            = true;

    public String getClasspath() {

        String CLASSPATH_SEPARATOR = getClasspathSeparator();
        File basePath = useLocal ? mavenDirectory
                : new File(dataDir + File.separator + "engine");
        String ret = "";

        boolean ok = true;
        for (String s : additionalRelativePaths) {
            File f = new File(basePath + File.separator + s);
            if (!(f.exists() && f.isDirectory())) {
                ok = false;
            }
        }

        if (ok) {
            /*
             * TODO more tests
             */

            /*
             * build classpath with development classes first
             */
            ret = "";
            for (String s : additionalRelativePaths) {
                ret += (ret.isEmpty() ? "" : CLASSPATH_SEPARATOR) + basePath
                        + File.separator + s;
            }

            // ret += CLASSPATH_SEPARATOR + basePath + File.separator +
            // LIB_FILES_PATH +
            // "/*";
        }

        return ret;
    }

    /*
     * can't believe this is necessary.
     */
    protected static String getClasspathSeparator() {
        switch (KLAB.CONFIG.getOS()) {
        case MACOS:
        case UNIX:
            return ":";
        case WIN:
            return ";";
        }
        return ":";
    }

    /**
     * Return the build number from the network distribution, or null if the network
     * cannot be accessed.
     * 
     * @return the build number for the set (remote) engine. Ignores local distribution
     *         setting.
     */
    public Integer getBuildNumber() {

        if (remoteBuildNumberDev == null) {
            try {
                File bn = File.createTempFile("build", "txt");
                URLUtils.copy(new URL(BUILD_DEV_FILE_URL), bn);
                remoteBuildNumberDev = Integer
                        .parseInt(FileUtils.readFileToString(bn).trim());
            } catch (Exception e) {
                // leave it null
            }
        }
        if (remoteBuildNumber == null) {
            try {
                File bn = File.createTempFile("build", "txt");
                URLUtils.copy(new URL(BUILD_FILE_URL), bn);
                remoteBuildNumber = Integer
                        .parseInt(FileUtils.readFileToString(bn).trim());
            } catch (Exception e) {
                // leave it null
            }
        }
        return useDeveloper ? remoteBuildNumberDev : remoteBuildNumber;
    }

    /**
     * Return whether the settings correspond to defined properties so that an engine can
     * be launched.
     * 
     * @return true if we can be launched with the current settings.
     */
    public boolean canLaunch() {
        if (useLocal) {
            return mavenDirectory != null && mavenDirectory.exists()
                    && mavenDirectory.isDirectory()
                    && new File(mavenDirectory + File.separator + "pom.xml").exists();
        }
        if (useDeveloper) {
            return engineJarDev != null && engineJarDev.isFile()
                    && engineJarDev.canRead();
        }
        return engineJar != null && engineJar.isFile() && engineJar.canRead();
    }

    /**
     * Call after a download attempt to rescan the files and reset the build numbers.
     */
    public void rescanDistributions() {

        File engineJarFile = new File(KLAB.CONFIG.getDataPath("engine") + File.separator
                + "kmodeler.jar");
        if (engineJarFile.exists()) {
            engineJar = engineJarFile;
            if (KLAB.CONFIG.getProperties().containsKey(BUILD_PROPERTY)) {
                localBuildNumber = Integer.parseInt(KLAB.CONFIG.getProperties()
                        .getProperty(BUILD_PROPERTY));
            }
        }
        File engineJarFileDev = new File(KLAB.CONFIG.getDataPath("engine")
                + File.separator + "kmodeler_dev.jar");
        if (engineJarFileDev.exists()) {
            engineJarDev = engineJarFileDev;
            if (KLAB.CONFIG.getProperties().containsKey(BUILD_DEV_PROPERTY)) {
                localBuildNumberDev = Integer.parseInt(KLAB.CONFIG.getProperties()
                        .getProperty(BUILD_DEV_PROPERTY));
            }
        }

        remoteBuildNumber = null;
        remoteBuildNumberDev = null;

        getBuildNumber();
    }

    public void setRunGC(boolean selection) {
        this.runGC = selection;
    }

    public void setCleanObservationCache(boolean selection) {
        this.runObsClean = selection;
    }

    public void setCleanDataCache(boolean selection) {
        this.runDataClean = selection;
    }

    public void setCleanModelCache(boolean selection) {
        this.runModelClean = selection;
    }

    public void setCleanCycle(boolean selection) {
        this.runClean = selection;
    }

    public boolean isRestartRequested() {
        return runObsClean || runDataClean || runModelClean || runClean;
    }

    public void setRememberSettings(boolean selection) {
        this.rememberSettings = selection;
    }

    public boolean isRememberSettings() {
        return rememberSettings;
    }

    public void setStopEngineNow(boolean selection) {
        this.stopEngine = selection;
    }

    public boolean isStopEngineNow() {
        return this.stopEngine;
    }

    public void setDebugPort(int port) {
        debugPort = port;
    }

    public int getDebugPort() {
        return debugPort;
    }

    public void setUseReasoning(boolean selection) {
        useReasoning = selection;
    }

    public boolean isUseReasoning() {
        return useReasoning;
    }

    public boolean isDevelop() {
        return useDeveloper;
    }

}
