/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.owl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.data.IList;
import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IIndividual;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.SemanticType;
import org.semanticweb.owlapi.io.OWLXMLOntologyFormat;
import org.semanticweb.owlapi.model.AddAxiom;
import org.semanticweb.owlapi.model.AddImport;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLImportsDeclaration;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyChangeException;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyFormat;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.OWLProperty;

/**
 * A proxy for an ontology. Holds a list of concepts and a list of axioms. Can be turned
 * into a list and marshalled to a server for actual knowledge creation. Contains no
 * instances, properties or restrictions directly, just concepts for indexing and axioms
 * for the actual stuff. The {@link #addDelegateConcept(String, IConcept)} method also
 * allows temporarily hosting a concept from another ontology under a local name, so that
 * the 'identified as' mechanism can work properly.
 * 
 * @author Ferd
 */
public class Ontology implements IOntology {

    String                        id;
    List<IList>                   axioms       = new ArrayList<>();

    private Set<String>           imported     = new HashSet<>();
    private Map<String, IConcept> delegates    = new HashMap<>();
    Map<String, IIndividual>      individuals  = new HashMap<>();

    OWLOntology                   ontology;
    private String                prefix;
    // private String _base;
    private OWL                   manager;
    private Map<String, IConcept> conceptIDs   = new HashMap<>();

    /*
     * all properties
     */
    Set<String>                   propertyIDs  = new HashSet<>();

    /*
     * property IDs by class - no other way to return the OWL objects quickly. what a pain
     */
    Set<String>                   opropertyIDs = new HashSet<>();
    Set<String>                   dpropertyIDs = new HashSet<>();
    Set<String>                   apropertyIDs = new HashSet<>();
    private String                resourceUrl;
    private boolean               isInternal   = false;

    Ontology(OWLOntology ontology, String id, OWL manager) {

        this.id = id;
        this.ontology = ontology;
        this.manager = manager;
        this.prefix = ontology.getOntologyID().getOntologyIRI().toString();

        /**
         * FIXME can't spend a week figuring this out, but this is NOT nice.
         */
        if (this.prefix.equals("http://purl.obolibrary.org/obo/bfo.owl")) {
            this.prefix = "http://purl.obolibrary.org/obo";
        }
        // this.namespace = ontology.getOntologyID().getOntologyIRI().getNamespace();

        /*
         * get the prefix for itself
         */
        // PrefixOWLOntologyFormat pm = (PrefixOWLOntologyFormat)
        // manager.manager.getOntologyFormat(ontology);
        // Map<String, String> prefixMap = pm.getPrefixName2PrefixMap();
        // if (prefixMap.containsKey(":")) {
        // this.prefix = prefixMap.get(":");
        // }
        scan();
    }

    public OWL getManager() {
        return manager;
    }

    public String getPrefix() {
        return this.prefix;
    }

    /*
     * build a catalog of names, as there seems to be no way to quickly assess if an
     * ontology contains a named entity or not. This needs to be kept in sync with any
     * changes, which is a pain. FIXME reintegrate the conditionals when define() works
     * properly.
     */
    private void scan() {

        for (OWLClass c : this.ontology.getClassesInSignature(false)) {
            if (c.getIRI().toString().contains(this.prefix)) {
                this.conceptIDs.put(c.getIRI().getFragment(), new Concept(c, this.manager, this.id));
            }
        }
        for (OWLProperty<?, ?> p : this.ontology.getDataPropertiesInSignature(false)) {
            if (p.getIRI().toString().contains(this.prefix)) {
                this.dpropertyIDs.add(p.getIRI().getFragment());
                this.propertyIDs.add(p.getIRI().getFragment());
            }
        }
        for (OWLProperty<?, ?> p : this.ontology.getObjectPropertiesInSignature(false)) {
            if (p.getIRI().toString().contains(this.prefix)) {
                this.opropertyIDs.add(p.getIRI().getFragment());
                this.propertyIDs.add(p.getIRI().getFragment());
            }
        }
        for (OWLAnnotationProperty p : this.ontology
                .getAnnotationPropertiesInSignature()) {
            if (p.getIRI().toString().contains(this.prefix)) {
                this.apropertyIDs.add(p.getIRI().getFragment());
                this.propertyIDs.add(p.getIRI().getFragment());
            }
        }
    }

    public void addDelegateConcept(String id, INamespace namespace, IConcept concept) {
        this.delegates.put(id, concept);
        concept.getOntology()
                .define(Collections.singleton(Axiom.AnnotationAssertion(concept
                        .getLocalName(), NS.LOCAL_ALIAS_PROPERTY, namespace.getId() + ":"
                                + id)));
    }

    @Override
    public IOntology getOntology() {
        return this;
    }

    @Override
    public Collection<IConcept> getConcepts() {

        ArrayList<IConcept> ret = new ArrayList<>(conceptIDs.values());
//        for (String s : this.conceptIDs) {
//            ret.add(getConcept(s));
//        }
        return ret;
    }

    @Override
    public Collection<IProperty> getProperties() {
        ArrayList<IProperty> ret = new ArrayList<>();
        for (OWLProperty<?, ?> p : this.ontology
                .getDataPropertiesInSignature(false)) {
            ret.add(new Property(p, this.manager, this.id));
        }
        for (OWLProperty<?, ?> p : this.ontology
                .getObjectPropertiesInSignature(false)) {
            ret.add(new Property(p, this.manager, this.id));
        }
        for (OWLAnnotationProperty p : this.ontology
                .getAnnotationPropertiesInSignature()) {
            ret.add(new Property(p, this.manager, this.id));
        }

        return ret;
    }

    /**
     * Special purpose function: return all concepts that have no parents that belong to
     * this ontology - making them "top-level" in it. Optionally, just return those whose
     * ONLY parent is owl:Thing.
     * 
     * @param observables if true, only check observables
     * @return top concepts
     */
    public synchronized List<IConcept> getTopConcepts(boolean observables) {
        ArrayList<IConcept> ret = new ArrayList<>();
        for (IConcept c : getConcepts()) {
            boolean ok = true;
            if (!observables || NS.isParticular(c)) {
                Collection<IConcept> parents = c.getParents();
                for (IConcept cc : parents) {
                    if (cc.getConceptSpace().equals(this.id)) {
                        ok = false;
                        break;
                    }
                }
                if (ok) {
                    ret.add(c);
                }
            }
        }
        return ret;
    }

    @Override
    public IConcept getConcept(String ID) {
        IConcept ret = this.conceptIDs.get(ID);
        if (ret != null) {
            return ret;
        }
//        if (this.conceptIDs.contains(ID)) {
//            return new Concept(this.ontology.getOWLOntologyManager()
//                    .getOWLDataFactory()
//                    .getOWLClass(IRI
//                            .create(this.prefix + "#" + ID)), this.manager, this.id);
//        }
        return delegates.get(ID);
    }

    @Override
    public IProperty getProperty(String ID) {
        if (this.opropertyIDs.contains(ID)) {
            return new Property(this.ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLObjectProperty(IRI
                            .create(this.prefix + "#" + ID)), this.manager, this.id);
        }
        if (this.dpropertyIDs.contains(ID)) {
            return new Property(this.ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLDataProperty(IRI
                            .create(this.prefix + "#" + ID)), this.manager, this.id);
        }
        if (this.apropertyIDs.contains(ID)) {
            return new Property(this.ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLAnnotationProperty(IRI
                            .create(this.prefix + "#" + ID)), this.manager, this.id);
        }
        return null;
    }

    @Override
    public String getURI() {
        return this.ontology.getOWLOntologyManager()
                .getOntologyDocumentIRI(this.ontology).toString();
    }

    @Override
    public boolean write(File file, boolean writeImported) throws KlabException {

        if (writeImported) {

            Set<IOntology> authorities = new HashSet<>(getDelegateOntologies());

            File path = MiscUtilities.getPath(file.toString());
            String myns = this.ontology.getOntologyID().getOntologyIRI().getNamespace();
            for (OWLOntology o : this.ontology.getImportsClosure()) {
                String iri = o.getOntologyID().getOntologyIRI().toString();
                if (iri.startsWith(myns) && !o.equals(this.ontology)) {
                    String fr = o.getOntologyID().getOntologyIRI().getFragment();
                    INamespace other = KLAB.MMANAGER.getNamespace(fr);
                    if (other != null) {
                        if (!fr.endsWith(".owl")) {
                            fr += ".owl";
                        }
                        File efile = new File((path.toString().equals(".") ? ""
                                : (path + File.separator))
                                + fr);
                        other.getOntology().write(efile, false);
                        authorities.addAll(((Ontology) other.getOntology())
                                .getDelegateOntologies());
                    }
                } 
            }

            for (IOntology o : authorities) {
                File efile = new File((path.toString().equals(".") ? ""
                        : (path + File.separator))
                        + o.getConceptSpace() + ".owl");
                o.write(efile, false);
            }
        }

        OWLOntologyFormat format = this.ontology.getOWLOntologyManager()
                .getOntologyFormat(this.ontology);
        OWLXMLOntologyFormat owlxmlFormat = new OWLXMLOntologyFormat();

        if (format.isPrefixOWLOntologyFormat()) {
            owlxmlFormat.copyPrefixesFrom(format.asPrefixOWLOntologyFormat());
        }
        try {
            this.ontology.getOWLOntologyManager()
                    .saveOntology(this.ontology, owlxmlFormat, IRI.create(file.toURI()));
        } catch (OWLOntologyStorageException e) {
            throw new KlabIOException(e);
        }

        return true;
    }

    /**
     * Return the ontologies that host all authority concepts we delegate to.
     * 
     * @return
     */
    public Collection<IOntology> getDelegateOntologies() {
        Set<IOntology> ret = new HashSet<>();
        for (IConcept c : delegates.values()) {
            ret.add(c.getOntology());
        }
        return ret;
    }

    @Override
    public Collection<String> define(Collection<IAxiom> axioms) {

        ArrayList<String> errors = new ArrayList<>();

        /*
         * ACHTUNG remember to add IDs to appropriate catalogs as classes and property
         * assertions are encountered. This can be called incrementally, so better not to
         * call scan() every time.
         */
        OWLDataFactory factory = this.ontology.getOWLOntologyManager()
                .getOWLDataFactory();

        for (IAxiom axiom : axioms) {

            // System.out.println("Axiom: " + axiom + " [" + id + "]");

            try {

                if (axiom.is(IAxiom.CLASS_ASSERTION)) {

                    OWLClass newcl = factory.getOWLClass(IRI.create(this.prefix
                            + "#" + axiom.getArgument(0)));
                    this.ontology.getOWLOntologyManager()
                            .addAxiom(this.ontology, factory
                                    .getOWLDeclarationAxiom(newcl));
                    this.conceptIDs.put(axiom.getArgument(0).toString(), new Concept(newcl, this.manager, axiom.getArgument(0).toString()));

                    // this.conceptOptions.put(axiom.getArgument(0).toString(),
                    // ((Axiom) axiom).getOptions());

                } else if (axiom.is(IAxiom.SUBCLASS_OF)) {

                    OWLClass subclass = findClass(axiom.getArgument(1)
                            .toString(), errors);
                    OWLClass superclass = findClass(axiom.getArgument(0)
                            .toString(), errors);

                    if (subclass != null && superclass != null) {
                        this.manager.manager.addAxiom(this.ontology, factory
                                .getOWLSubClassOfAxiom(subclass, superclass));
                    }

                } else if (axiom.is(IAxiom.ANNOTATION_PROPERTY_ASSERTION)) {

                    OWLAnnotationProperty p = factory
                            .getOWLAnnotationProperty(IRI.create(this.prefix + "#"
                                    + axiom.getArgument(0)));
                    // this.ontology.getOWLOntologyManager().addAxiom(this.ontology,
                    // factory.getOWLDeclarationAxiom(p));
                    this.propertyIDs.add(axiom.getArgument(0).toString());
                    this.apropertyIDs.add(axiom.getArgument(0).toString());
                    OWLMetadata._metadataVocabulary
                            .put(p.getIRI().toString(), getConceptSpace() + ":"
                                    + axiom.getArgument(0));

                } else if (axiom.is(IAxiom.DATA_PROPERTY_ASSERTION)) {

                    OWLDataProperty p = factory.getOWLDataProperty(IRI
                            .create(this.prefix + "#" + axiom.getArgument(0)));
                    this.ontology.getOWLOntologyManager()
                            .addAxiom(this.ontology, factory.getOWLDeclarationAxiom(p));
                    this.propertyIDs.add(axiom.getArgument(0).toString());
                    this.dpropertyIDs.add(axiom.getArgument(0).toString());

                } else if (axiom.is(IAxiom.DATA_PROPERTY_DOMAIN)) {

                    OWLEntity property = findProperty(axiom.getArgument(0)
                            .toString(), true, errors);
                    OWLClass classExp = findClass(axiom.getArgument(1)
                            .toString(), errors);

                    if (property != null && classExp != null) {
                        this.manager.manager
                                .addAxiom(this.ontology, factory
                                        .getOWLDataPropertyDomainAxiom(property
                                                .asOWLDataProperty(), classExp));
                    }

                } else if (axiom.is(IAxiom.DATA_PROPERTY_RANGE)) {

                    OWLEntity property = findProperty(axiom.getArgument(0)
                            .toString(), true, errors);
                    /*
                     * TODO XSD stuff
                     */

                    // _manager.manager.addAxiom(
                    // _ontology,
                    // factory.getOWLDataPropertyRangeAxiom(property.asOWLDataProperty(),
                    // classExp));

                } else if (axiom.is(IAxiom.OBJECT_PROPERTY_ASSERTION)) {

                    OWLObjectProperty p = factory.getOWLObjectProperty(IRI
                            .create(this.prefix + "#" + axiom.getArgument(0)));
                    this.ontology.getOWLOntologyManager()
                            .addAxiom(this.ontology, factory.getOWLDeclarationAxiom(p));
                    this.propertyIDs.add(axiom.getArgument(0).toString());
                    this.opropertyIDs.add(axiom.getArgument(0).toString());

                } else if (axiom.is(IAxiom.OBJECT_PROPERTY_DOMAIN)) {

                    OWLEntity property = findProperty(axiom.getArgument(0)
                            .toString(), false, errors);
                    OWLClass classExp = findClass(axiom.getArgument(1)
                            .toString(), errors);

                    if (property != null && classExp != null) {
                        this.manager.manager.addAxiom(this.ontology, factory
                                .getOWLObjectPropertyDomainAxiom(property
                                        .asOWLObjectProperty(), classExp));
                    }

                } else if (axiom.is(IAxiom.OBJECT_PROPERTY_RANGE)) {

                    OWLEntity property = findProperty(axiom.getArgument(0)
                            .toString(), false, errors);
                    OWLClass classExp = findClass(axiom.getArgument(1)
                            .toString(), errors);

                    if (property != null && classExp != null) {
                        this.manager.manager.addAxiom(this.ontology, factory
                                .getOWLObjectPropertyRangeAxiom(property
                                        .asOWLObjectProperty(), classExp));
                    }

                } else if (axiom.is(IAxiom.ALL_VALUES_FROM_RESTRICTION)) {

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);
                    OWLClassExpression restr = factory
                            .getOWLObjectAllValuesFrom(property
                                    .asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        this.manager.manager
                                .addAxiom(this.ontology, factory
                                        .getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.AT_LEAST_N_VALUES_FROM_RESTRICTION)) {

                    int n = ((Number) axiom.getArgument(3)).intValue();

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    OWLClassExpression restr = factory
                            .getOWLObjectMinCardinality(n, property
                                    .asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        this.manager.manager
                                .addAxiom(this.ontology, factory
                                        .getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.AT_MOST_N_VALUES_FROM_RESTRICTION)) {

                    int n = ((Number) axiom.getArgument(3)).intValue();

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    OWLClassExpression restr = factory
                            .getOWLObjectMaxCardinality(n, property
                                    .asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        this.manager.manager
                                .addAxiom(this.ontology, factory
                                        .getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.EXACTLY_N_VALUES_FROM_RESTRICTION)) {

                    int n = ((Number) axiom.getArgument(3)).intValue();

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    OWLClassExpression restr = factory
                            .getOWLObjectExactCardinality(n, property
                                    .asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        this.manager.manager
                                .addAxiom(this.ontology, factory
                                        .getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.SOME_VALUES_FROM_RESTRICTION)) {

                    OWLEntity property = findProperty(axiom.getArgument(1)
                            .toString(), false, errors);
                    OWLClass target = findClass(axiom.getArgument(0).toString(), errors);
                    OWLClass filler = findClass(axiom.getArgument(2).toString(), errors);

                    OWLClassExpression restr = factory
                            .getOWLObjectSomeValuesFrom(property
                                    .asOWLObjectProperty(), filler);

                    if (property != null && filler != null && target != null
                            && restr != null) {
                        this.manager.manager
                                .addAxiom(this.ontology, factory
                                        .getOWLSubClassOfAxiom(target, restr));
                    }

                } else if (axiom.is(IAxiom.DATATYPE_DEFINITION)) {

                } else if (axiom.is(IAxiom.DISJOINT_CLASSES)) {

                    Set<OWLClassExpression> classExpressions = new HashSet<>();
                    for (Object arg : axiom) {
                        OWLClass p = factory.getOWLClass(IRI.create(this.prefix
                                + "#" + arg));
                        classExpressions.add(p);
                    }
                    this.manager.manager.addAxiom(this.ontology, factory
                            .getOWLDisjointClassesAxiom(classExpressions));

                } else if (axiom.is(IAxiom.ASYMMETRIC_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.DIFFERENT_INDIVIDUALS)) {

                } else if (axiom.is(IAxiom.DISJOINT_OBJECT_PROPERTIES)) {

                } else if (axiom.is(IAxiom.DISJOINT_DATA_PROPERTIES)) {

                } else if (axiom.is(IAxiom.DISJOINT_UNION)) {

                } else if (axiom.is(IAxiom.EQUIVALENT_CLASSES)) {

                    Set<OWLClassExpression> classExpressions = new HashSet<>();
                    for (Object arg : axiom) {
                        OWLClass classExp = findClass(arg.toString(), errors);
                        classExpressions.add(classExp);
                    }
                    this.manager.manager.addAxiom(this.ontology, factory
                            .getOWLEquivalentClassesAxiom(classExpressions));

                } else if (axiom.is(IAxiom.EQUIVALENT_DATA_PROPERTIES)) {

                } else if (axiom.is(IAxiom.EQUIVALENT_OBJECT_PROPERTIES)) {

                } else if (axiom.is(IAxiom.FUNCTIONAL_DATA_PROPERTY)) {

                    OWLDataProperty prop = factory.getOWLDataProperty(IRI
                            .create(this.prefix + "#" + axiom.getArgument(0)));
                    this.manager.manager
                            .addAxiom(this.ontology, factory
                                    .getOWLFunctionalDataPropertyAxiom(prop));

                } else if (axiom.is(IAxiom.FUNCTIONAL_OBJECT_PROPERTY)) {

                    OWLObjectProperty prop = factory.getOWLObjectProperty(IRI
                            .create(this.prefix + "#" + axiom.getArgument(0)));
                    this.manager.manager
                            .addAxiom(this.ontology, factory
                                    .getOWLFunctionalObjectPropertyAxiom(prop));

                } else if (axiom.is(IAxiom.INVERSE_FUNCTIONAL_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.INVERSE_OBJECT_PROPERTIES)) {

                } else if (axiom.is(IAxiom.IRREFLEXIVE_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.NEGATIVE_DATA_PROPERTY_ASSERTION)) {

                } else if (axiom.is(IAxiom.NEGATIVE_OBJECT_PROPERTY_ASSERTION)) {

                } else if (axiom.is(IAxiom.REFLEXIVE_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.SUB_ANNOTATION_PROPERTY_OF)) {

                } else if (axiom.is(IAxiom.SUB_DATA_PROPERTY)) {

                    OWLDataProperty subdprop = (OWLDataProperty) findProperty(axiom
                            .getArgument(1)
                            .toString(), true, errors);
                    OWLDataProperty superdprop = (OWLDataProperty) findProperty(axiom
                            .getArgument(0)
                            .toString(), true, errors);

                    if (subdprop != null && superdprop != null) {
                        this.manager.manager.addAxiom(this.ontology, factory
                                .getOWLSubDataPropertyOfAxiom(subdprop, superdprop));
                    }

                } else if (axiom.is(IAxiom.SUB_ANNOTATION_PROPERTY)) {

                    OWLAnnotationProperty suboprop = (OWLAnnotationProperty) findProperty(axiom
                            .getArgument(1)
                            .toString(), false, errors);
                    OWLAnnotationProperty superoprop = (OWLAnnotationProperty) findProperty(axiom
                            .getArgument(0)
                            .toString(), false, errors);

                    if (suboprop != null && superoprop != null) {
                        this.manager.manager.addAxiom(this.ontology, factory
                                .getOWLSubAnnotationPropertyOfAxiom(suboprop, superoprop));
                    }
                    
                } else if (axiom.is(IAxiom.SUB_OBJECT_PROPERTY)) {

                    OWLObjectProperty suboprop = (OWLObjectProperty) findProperty(axiom
                            .getArgument(1)
                            .toString(), false, errors);
                    OWLObjectProperty superoprop = (OWLObjectProperty) findProperty(axiom
                            .getArgument(0)
                            .toString(), false, errors);

                    if (suboprop != null && superoprop != null) {
                        this.manager.manager.addAxiom(this.ontology, factory
                                .getOWLSubObjectPropertyOfAxiom(suboprop, superoprop));
                    }

                } else if (axiom.is(IAxiom.SUB_PROPERTY_CHAIN_OF)) {

                } else if (axiom.is(IAxiom.SYMMETRIC_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.TRANSITIVE_OBJECT_PROPERTY)) {

                } else if (axiom.is(IAxiom.SWRL_RULE)) {

                } else if (axiom.is(IAxiom.HAS_KEY)) {

                } else if (axiom.is(IAxiom.ANNOTATION_ASSERTION)) {

                    OWLAnnotationProperty property = findAnnotationProperty(axiom
                            .getArgument(1)
                            .toString(), errors);
                    Object value = axiom.getArgument(2);
                    OWLLiteral literal = null;
                    OWLEntity target = findKnowledge(axiom.getArgument(0)
                            .toString(), errors);

                    if (target != null) {

                        if (value instanceof String)
                            literal = factory
                                    .getOWLLiteral(org.integratedmodelling.common.utils.StringUtils
                                            .pack((String) value));
                        else if (value instanceof Integer)
                            literal = factory.getOWLLiteral((Integer) value);
                        else if (value instanceof Long)
                            literal = factory.getOWLLiteral((Long) value);
                        else if (value instanceof Float)
                            literal = factory.getOWLLiteral((Float) value);
                        else if (value instanceof Double)
                            literal = factory.getOWLLiteral((Double) value);
                        else if (value instanceof Boolean)
                            literal = factory.getOWLLiteral((Boolean) value);

                        /*
                         * TODO determine literal from type of value and property
                         */
                        if (property != null && literal != null) {
                            OWLAnnotation annotation = factory
                                    .getOWLAnnotation(property, literal);
                            this.manager.manager.addAxiom(this.ontology, factory
                                    .getOWLAnnotationAssertionAxiom(target
                                            .getIRI(), annotation));
                        }
                    }

                } else if (axiom.is(IAxiom.ANNOTATION_PROPERTY_DOMAIN)) {

                } else if (axiom.is(IAxiom.ANNOTATION_PROPERTY_RANGE)) {

                }

            } catch (OWLOntologyChangeException e) {
                errors.add(e.getMessage());
            }

        }

        scan();
        return errors;
    }

    /* must exist, can be property or class */
    private OWLEntity findKnowledge(String string, ArrayList<String> errors) {

        if (this.conceptIDs.containsKey(string)) {
            return findClass(string, errors);
        } else if (this.propertyIDs.contains(string)) {
            if (this.apropertyIDs.contains(string)) {
                return findAnnotationProperty(string, errors);
            }
            return findProperty(string, this.dpropertyIDs.contains(string), errors);
        }

        return null;
    }

    private OWLClass findClass(String c, ArrayList<String> errors) {

        OWLClass ret = null;

        if (c.contains(":")) {

            IConcept cc = this.manager.getConcept(c);
            if (cc == null) {
                errors.add("concept " + cc + " not found");
                return null;
            }

            /*
             * ensure ontology is imported
             */
            if (!cc.getConceptSpace().equals(this.id)
                    && !this.imported.contains(cc.getConceptSpace())) {

                this.imported.add(cc.getConceptSpace());
                IRI importIRI = ((Ontology) cc.getOntology()).ontology
                        .getOntologyID().getOntologyIRI();
                OWLImportsDeclaration importDeclaraton = this.ontology
                        .getOWLOntologyManager().getOWLDataFactory()
                        .getOWLImportsDeclaration(importIRI);
                this.manager.manager
                        .applyChange(new AddImport(this.ontology, importDeclaraton));
            }

            ret = ((Concept) cc)._owl;

        } else {

            ret = this.ontology.getOWLOntologyManager().getOWLDataFactory()
                    .getOWLClass(IRI.create(this.prefix + "#" + c));

            this.conceptIDs.put(c, new Concept(ret, this.manager, c));
        }

        return ret;
    }

    private OWLEntity findProperty(String c, boolean isData, ArrayList<String> errors) {

        OWLEntity ret = null;

        if (c.contains(":")) {

            IProperty cc = this.manager.getProperty(c);
            if (cc == null) {
                errors.add("property " + cc + " not found");
                return null;
            }

            /*
             * ensure ontology is imported
             */
            if (!cc.getConceptSpace().equals(this.id)
                    && !this.imported.contains(cc.getConceptSpace())) {

                this.imported.add(cc.getConceptSpace());
                IRI importIRI = ((Ontology) cc.getOntology()).ontology
                        .getOntologyID().getOntologyIRI();
                OWLImportsDeclaration importDeclaraton = this.ontology
                        .getOWLOntologyManager().getOWLDataFactory()
                        .getOWLImportsDeclaration(importIRI);
                this.manager.manager
                        .applyChange(new AddImport(this.ontology, importDeclaraton));
            }

            ret = ((Property) cc)._owl;

            if (isData && ret instanceof OWLObjectProperty) {
                throw new KlabRuntimeException(cc
                        + " is an object property: data expected");
            }
            if (!isData && ret instanceof OWLDataProperty) {
                throw new KlabRuntimeException(cc
                        + " is a data property: object expected");
            }

        } else {
            ret = isData ? this.ontology.getOWLOntologyManager()
                    .getOWLDataFactory()
                    .getOWLDataProperty(IRI.create(this.prefix + "#" + c))
                    : this.ontology
                            .getOWLOntologyManager()
                            .getOWLDataFactory()
                            .getOWLObjectProperty(IRI.create(this.prefix + "#" + c));

            if (isData) {
                this.dpropertyIDs.add(c);
            } else {
                this.opropertyIDs.add(c);
            }
            this.propertyIDs.add(c);
        }

        return ret;
    }

    private OWLAnnotationProperty findAnnotationProperty(String c, ArrayList<String> errors) {

        OWLAnnotationProperty ret = null;

        if (c.contains(":")) {

            IProperty cc = this.manager.getProperty(c);
            if (cc != null) {
                OWLEntity e = ((Property) cc)._owl;
                if (e instanceof OWLAnnotationProperty)
                    return (OWLAnnotationProperty) e;
            } else {
                SemanticType ct = new SemanticType(c);
                IOntology ontology = this.manager.getOntology(ct.getConceptSpace());
                if (ontology == null) {
                    INamespace ns = KLAB.MMANAGER.getNamespace(ct
                            .getConceptSpace());
                    if (ns != null)
                        ontology = ns.getOntology();
                }
                if (ontology != null) {
                    ret = ((Ontology) ontology).ontology
                            .getOWLOntologyManager()
                            .getOWLDataFactory()
                            .getOWLAnnotationProperty(IRI
                                    .create(((Ontology) ontology).prefix
                                            + "#" + ct.getLocalName()));
                }
            }
        } else {
            ret = this.ontology.getOWLOntologyManager().getOWLDataFactory()
                    .getOWLAnnotationProperty(IRI.create(this.prefix + "#" + c));
            this.apropertyIDs.add(c);
            this.propertyIDs.add(c);
        }

        return ret;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getConceptSpace() {
        return this.id;
    }

    @Override
    public int getConceptCount() {
        return this.conceptIDs.size();
    }

    @Override
    public int getPropertyCount() {
        return this.propertyIDs.size();
    }

    public void setResourceUrl(String string) {
        this.resourceUrl = string;
    }

    /**
     * Return the URL of the resource this was read from, or null if it was created by the
     * API.
     * 
     * @return URL of source
     */
    public String getResourceUrl() {
        return this.resourceUrl;
    }

    public IConcept createConcept(String newName) {

        IConcept ret = getConcept(newName);
        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(Axiom.ClassAssertion(newName));
            define(ax);
            ret = getConcept(newName);
        }
        return ret;

    }

    public IProperty createProperty(String newName, boolean isData) {

        IProperty ret = getProperty(newName);
        if (ret == null) {
            ArrayList<IAxiom> ax = new ArrayList<>();
            ax.add(isData ? Axiom.DataPropertyAssertion(newName)
                    : Axiom.ObjectPropertyAssertion(newName));
            define(ax);
            ret = getProperty(newName);
        }
        return ret;

    }

    public boolean isInternal() {
        return this.isInternal;
    }

    public void setInternal(boolean b) {
        this.isInternal = b;
    }

    public void createReasoner() {

        try {
            this.ontology.getOWLOntologyManager()
                    .loadOntology(IRI.create(this.ontology.getOntologyID()
                            .getOntologyIRI().toString()));
        } catch (OWLOntologyCreationException e) {
            throw new KlabRuntimeException(e);
        }

    }

    public void addImport(IOntology ontology) {

        if (!this.ontology.getImports().contains(((Ontology) ontology).ontology)) {
            OWLDataFactory factory = this.ontology.getOWLOntologyManager()
                    .getOWLDataFactory();
            OWLImportsDeclaration imp = factory.getOWLImportsDeclaration(IRI
                    .create(((Ontology) ontology).ontology.getOntologyID()
                            .getOntologyIRI().toString()));
            this.ontology.getOWLOntologyManager()
                    .applyChange(new AddImport(this.ontology, imp));
            this.imported.add(ontology.getConceptSpace());
        }
    }

    @Override
    public String toString() {
        return "<O " + id + " (" + ontology.getOntologyID() + ")>";
    }

    public OWLOntology getOWLOntology() {
        return this.ontology;
    }

    public IIndividual getSingletonIndividual(IConcept concept) {
        String iName = "the" + concept.getLocalName();
        if (individuals.containsKey(iName)) {
            return individuals.get(iName);
        }
        IIndividual ret = new Individual();
        ((Individual) ret).define(concept, iName, this);
        return ret;
    }

    public void linkIndividuals(IIndividual source, IIndividual destination, IProperty link) {

        OWLObjectPropertyAssertionAxiom axiom = manager.manager.getOWLDataFactory()
                .getOWLObjectPropertyAssertionAxiom(((Property) link)._owl
                        .asOWLObjectProperty(), ((Individual) source).individual, ((Individual) destination).individual);

        AddAxiom addAxiom = new AddAxiom(ontology, axiom);
        manager.manager.applyChange(addAxiom);
    }
}
