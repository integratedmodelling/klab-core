/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

import java.io.File;
import java.util.Properties;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.network.IEngineNetwork;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.common.auth.LicenseManager;
import org.integratedmodelling.common.auth.User;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.beans.Node;
import org.integratedmodelling.common.beans.responses.UserView;
import org.integratedmodelling.common.client.EngineController;
import org.integratedmodelling.common.client.palette.Palette;
import org.integratedmodelling.common.client.palette.PaletteManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.FileUtils;

/**
 * This one is responsible for building up the list of nodes based on calling
 * context. A list must be read from the suitable source:
 * 
 * the personal engine for clients, the primary server for personal engines, the
 * public server configuration and the nodes themselves for public servers.
 * 
 * After the nodes are in, the resource catalog should be rebuilt. The node list
 * should be refreshed periodically, and any differences due to nodes coming
 * online or going offline should be reflected in the resource catalog.
 */
public class EngineNetwork extends AbstractBaseNetwork implements IEngineNetwork {

    IUser            user;
    boolean          isOnline;
    EngineController primaryServer;
    File             certificate;

    /**
     * 
     */
    public EngineNetwork() {
    }

    /**
     * @param certificate
     */
    public EngineNetwork(File certificate) {
        this.certificate = certificate;
    }

    /**
     * @return true if initialization is successful
     */
    public boolean initialize() {

        if (certificate == null) {
            String predefClientCertfile = System
                    .getProperty(IConfiguration.CERTFILE_PROPERTY);
            File clientCertFile = new File(predefClientCertfile != null
                    ? predefClientCertfile
                    : (KLAB.CONFIG.getDataPath() + File.separator + "im.cert"));
            this.certificate = clientCertFile;
        }
        File publicKey = new File(KLAB.CONFIG.getDataPath() + File.separator + "ssh"
                + File.separator + "pubring.gpg");

        if (certificate.exists() && publicKey.exists()) {
            return (isOnline = initializeFromPrimaryServer(certificate, publicKey));
        } else {
            KLAB.error("certificate could not be read: network was not initialized, cannot continue.");
            System.exit(255);
        }

        /*
         * return true but we remain anonymous. We'll become useful when
         * configured through the admin interface.
         */

        return true;
    }

    private boolean initializeFromPrimaryServer(File clientCertFile, File publicKey) {

        Properties identity = null;
        try {

            identity = LicenseManager.readCertificate(clientCertFile, publicKey);

            String username = identity.getProperty("user");
            this.url = identity.getProperty("primary.server");
            if (KLAB.CONFIG.useDeveloperNetwork()) {
                this.url = this.url + KLAB.CONFIG.getDeveloperNetworkURLPostfix();
            }
            KLAB.NAME = username;

            /*
             * TODO check expiration
             */

            this.primaryServer = new EngineController("primary", this.url);

            KLAB.info("engine " + username + " connecting to network through "
                    + this.url);

            /*
             * Connect to the network through the primary server and set up
             * resources.
             */
            UserView userview = primaryServer
                    .connect(FileUtils.readFileToString(clientCertFile));

            if (userview == null || userview.getNetwork() == null) {
                KLAB.error("Network connection unsuccessful: please ensure that you are online and have a current certificate");
                return false;
            }

            KLAB.info("engine " + username + " connected to network through node "
                    + userview.getNetwork().getNodeId()
                    + ": " + userview.getNetwork().getNodes().size()
                    + " nodes available");

            userview.getUserProfile().setServerUrl(this.url);
            this.user = new User(userview.getUserProfile(), userview.getAuthToken());
            this.deserialize(userview.getNetwork());

            for (Node node : userview.getNetwork().getNodes()) {
                for (Palette palette : node.getToolkits()) {
                    PaletteManager.get().addRemotePalette(palette);
                }
            }
            
            /*
             * this establishes the proper node authentication so we can access
             * the network later.
             */
            primaryServer.setNode(getNode(userview.getNetwork().getNodeId()));

            setNetworkMonitor(new NetworkMonitor() {
                @Override
                protected Network getNetwork() {
                    return primaryServer.getNetwork();
                }
            });

            ((ResourceConfiguration) KLAB.ENGINE.getResourceConfiguration())
                    .setNetworkResources(userview.getNetwork(), user);

        } catch (Exception e) {
            KLAB.error("engine initialization failed: ", e);
            return false;
        }
        return true;
    }

    @Override
    public IUser getUser() {
        return user;
    }

    @Override
    public boolean isOnline() {
        return isOnline;
    }

    @Override
    public IServer getPrimaryNode() {
        return primaryServer == null ? null : primaryServer.getNode();
    }

    @Override
    public void activate(boolean active) {
        // TODO Auto-generated method stub

    }

    /*
     * only used in offline mode when session is established.
     */
    public void setUser(IUser user2) {
        this.user = user2;
    }

}
