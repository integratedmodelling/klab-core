package org.integratedmodelling.common.network;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.beans.Node;
import org.integratedmodelling.common.beans.Relationship;
import org.integratedmodelling.common.client.NodeClient;
import org.integratedmodelling.common.client.palette.Palette;
import org.integratedmodelling.common.client.palette.PaletteManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * @author ferdinando.villa TODO make it extend a JGraphT graph of nodes and remove
 *         downstream graph adapters
 */
public abstract class AbstractBaseNetwork implements INetwork, NetworkSerializable, NetworkDeserializable {

    protected List<Listener>     listeners   = new ArrayList<>();
    protected Map<String, IServer> nodes       = new HashMap<>();
    protected String             url;
    protected NetworkMonitor     networkMonitor;
    protected boolean            hasChanged  = true;
    protected Set<Relationship>  connections = new HashSet<>();

    private static long MAX_TIMEOUT_REMOTE_SERVICE_SECONDS = 2;

    protected void setNetworkMonitor(NetworkMonitor monitor) {
        this.networkMonitor = monitor;
        monitor.setNetwork(this);
        monitor.startPolling();
    }

    @SuppressWarnings("javadoc")
    public NetworkMonitor getMonitor() {
        return networkMonitor;
    }

    @Override
    public IPrototype findPrototype(String id) {
        
        for (IServer node : getNodes()) {
            IPrototype ret = node.getFunctionPrototype(id);
            if (ret != null) {
                return ret;
            }
        }
        return null;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public Collection<IServer> getNodesProviding(Object resource) {
        List<IServer> ret = new ArrayList<>();
        synchronized (nodes) {
            for (IServer node : nodes.values()) {
                if (node.provides(resource)) {
                    ret.add(node);
                }
            }
        }
        return ret;
    }

    @Override
    public Collection<IServer> getNodes() {
        synchronized (nodes) {
            return nodes.values();
        }
    }

    @Override
    public IServer getNode(String s) {
        synchronized (nodes) {
            return nodes.get(s);
        }
    }

    @Override
    public boolean hasChanged() {
        boolean ret = hasChanged;
        hasChanged = false;
        return ret;
    }

    @Override
    public <T1, T2> T2 broadcast(final DistributedOperation<T1, T2> operation, final IMonitor monitor) {

        class Service implements Callable<T1> {

            IServer                        node;
            DistributedOperation<T1, T2> operation;

            public Service(IServer node, DistributedOperation<T1, T2> operation) {
                this.node = node;
                this.operation = operation;
            }

            @Override
            public T1 call() {
                return operation.executeCall(node);
            }

        }

        ArrayList<IServer> nn = new ArrayList<>();
        ArrayList<Callable<T1>> rr = new ArrayList<>();
        for (IServer n : nodes.values()) {
            if (operation.acceptNode(n)) {
                nn.add(n);
                rr.add(new Service(n, operation));
            }
        }

        /*
         * create threads, call them all and wait for completion; then merge results,
         * which should be collections and whose members should provide proper equal() and
         * hashCode() implementations.
         */
        ExecutorService executor = Executors.newScheduledThreadPool(nn.size());
        List<T1> results = new ArrayList<>();
        try {
            List<Future<T1>> res = executor.invokeAll(rr);
            executor.awaitTermination(MAX_TIMEOUT_REMOTE_SERVICE_SECONDS, TimeUnit.SECONDS);

            for (Future<T1> r : res) {
                T1 o = null;
                try {
                    o = r.get();
                } catch (ExecutionException e) {
                }

                if (o != null) {
                    results.add(o);
                }
            }

        } catch (InterruptedException e) {
            throw new KlabRuntimeException(e);
        }

        return operation.merge(results);
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.integratedmodelling.api.network.INetwork#addListener(org.integratedmodelling.
     * api.network.INetwork.Listener)
     */
    @Override
    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    /**
     * Remove listeners.
     */
    public void removeListeners() {
        listeners.clear();
    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.integratedmodelling.common.interfaces.NetworkDeserializable#deserialize(org.
     * integratedmodelling.api.modelling.IModelBean)
     */
    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Network)) {
            throw new KlabRuntimeException("cannot deserialize an Observable from a "
                    + object == null ? "null" : object.getClass().getCanonicalName());
        }
        
        Network network = (Network) object;

        Set<IServer> tmp = new HashSet<>();
        Set<String> currentIds = new HashSet<>(nodes.keySet());
        PaletteManager.get().removeRemotePalettes();
        for (Node node : network.getNodes()) {
            IServer n = KLAB.MFACTORY.adapt(node, NodeClient.class);
            if (!currentIds.remove(n.getId())) {
                for (Listener listener : listeners) {
                    listener.nodeOnline(n);
                }
                hasChanged = true;
            }
            tmp.add(n);
            for (Palette palette : node.getToolkits()) {
                PaletteManager.get().addRemotePalette(palette);
            }
        }

        for (String id : currentIds) {
            for (Listener listener : listeners) {
                listener.nodeOffline(nodes.get(id));
            }
            hasChanged = true;
        }

        synchronized (nodes) {

            nodes.clear();
            for (IServer n : tmp) {
                nodes.put(n.getId(), n);
            }

            Set<Relationship> tmpr = new HashSet<>(connections);
            for (Relationship r : network.getStructure()) {
                if (!tmpr.remove(r)) {
                    hasChanged = true;
                }
            }

            if (tmpr.size() > 0) {
                hasChanged = true;
            }

            connections.clear();
            connections.addAll(network.getStructure());
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * org.integratedmodelling.common.interfaces.NetworkSerializable#serialize(java.lang.
     * Class)
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Network.class)) {
            throw new KlabRuntimeException("cannot serialize a network to a "
                    + desiredClass.getCanonicalName());
        }

        Network ret = new Network();
        ret.setTimestamp(System.currentTimeMillis());
        ret.setNodeId(KLAB.ENGINE.getName());
        synchronized (nodes) {
            for (IServer node : nodes.values()) {
                ret.getNodes().add(KLAB.MFACTORY.adapt(node, Node.class));
            }
            ret.getStructure().addAll(connections);
        }
        return (T) ret;
    }

    public Collection<? extends Relationship> getConnections() {
        synchronized (nodes) {
            return connections;
        }
    }

}
