package org.integratedmodelling.common.network;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import org.integratedmodelling.common.beans.Network;

/**
 * Base network monitor, checking on the network at regular intervals and reporting on events. The calls that
 * keep the network updated differ by caller. In clients, each session has a potentially different view of the
 * network, so there is one per session.
 * 
 * @author Ferd
 *
 */
public abstract class NetworkMonitor {

    ScheduledExecutorService executor                  = null;
    ScheduledFuture<?>       future                    = null;
    volatile Boolean         _monitoring               = false;
    int                      _errorcount               = 0;
    static int               MAX_ERRORS_BEFORE_WARNING = 3;
    AbstractBaseNetwork      network;

    void setNetwork(AbstractBaseNetwork network) {
        this.network = network;
    }

    /*
     * default: poll network every minute; nodes will decide whether to actually update their status according
     * to the state they're in.
     */
    static long POLLING_PERIOD_MILLISECONDS = 1 * 60 * 1000;

    class PollingService implements Runnable {

        @Override
        public void run() {
            synchronized (_monitoring) {
                monitor();
            }
        }
    }

    protected abstract Network getNetwork();

    /**
     * The function doing the monitoring.
     */
    protected void monitor() {
        Network nk = getNetwork();
        if (nk != null) {
            /*
             * should only happen to be null when debugging, but that's a 
             * noble cause.
             */
            network.deserialize(nk);
        }
    }

    /**
     * Add a monitor for notification of server status and spawn an appropriate monitoring cycle. Monitor will
     * get all notifications sent (as instances of EngineNotification).
     */
    public void startPolling() {

        if (future != null) {
            future.cancel(false);
            executor.shutdown();
        }

        executor = Executors.newScheduledThreadPool(24);
        future = executor
                .scheduleWithFixedDelay(new PollingService(), 0, POLLING_PERIOD_MILLISECONDS, TimeUnit.MILLISECONDS);
    }

    /**
     * Stop the polling.
     */
    public void pausePolling() {

        synchronized (_monitoring) {
            if (future != null) {
                future.cancel(false);
                executor.shutdown();
                future = null;
            }
        }
    }

}
