/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

import java.io.File;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.network.INodeNetwork;
import org.integratedmodelling.common.auth.LicenseManager;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.beans.Node;
import org.integratedmodelling.common.beans.Relationship;
import org.integratedmodelling.common.client.NodeClient;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.KlabAuthorizationException;

/**
 * This one is responsible for building up the list of nodes based on calling context. A
 * list must be read from the suitable source:
 * 
 * the personal engine for clients, the primary server for personal engines, the public
 * server configuration and the nodes themselves for public servers.
 * 
 * After the nodes are in, the resource catalog should be rebuilt. The node list should be
 * refreshed periodically, and any differences due to nodes coming online or going offline
 * should be reflected in the resource catalog.
 */
public class NodeNetwork extends AbstractBaseNetwork implements INodeNetwork {

    boolean    isOnline;
    String     key;
    Set<IServer> quarantined = new HashSet<>();

    public NodeNetwork() {
    }

    public boolean initialize() {

        String predefClientCertfile = System.getProperty(IConfiguration.CERTFILE_PROPERTY);

        File serverCertFile = new File(predefClientCertfile != null ? predefClientCertfile
                : KLAB.CONFIG.getDataPath() + File.separator + "server.cert");
        File publicKey = new File(KLAB.CONFIG.getDataPath()
                + File.separator + "ssh" +
                File.separator + "pubring.gpg");

        if (!serverCertFile.exists()) {
            KLAB.error("server certificate not found in " + KLAB.CONFIG.getDataPath()
                    + ": configuration aborted");
            return false;
        }

        return initializePublicNode(serverCertFile, publicKey);
    }

    public boolean initializePublicNode(File serverCertFile, File publicKey) {

        Properties identity = null;
        File serverDir = new File(KLAB.CONFIG.getDataPath()
                + File.separator + "network");
        try {

            identity = LicenseManager.readCertificate(serverCertFile, publicKey, new String[] {
                    "url",
                    "name",
                    "key" });

            serverDir.mkdirs();

            KLAB.NAME = identity.getProperty("name");
            this.url = identity.getProperty("url");
            this.key = identity.getProperty("key");

            /*
             * TODO check expiration
             */

        } catch (Exception e) {
            KLAB.error("network initialization failed: ", e);
            return false;
        }

        KLAB.info("initializing node " + KLAB.NAME + " for public use: scanning certified network peers");

        nodes.clear();

        /*
         * scan all remaining nodes
         */
        for (File sc : serverDir.listFiles()) {
            if (sc.canRead() && sc.isFile() && sc.toString().endsWith(".cert")) {
                try {

                    IServer node = new NodeClient(sc);
                    nodes.put(node.getId(), node);

                    Relationship r = new Relationship();
                    r.setSource(KLAB.ENGINE.getName());
                    r.setTarget(node.getId());
                    connections.add(r);

                    KLAB.info("read certificate for node " + node.getId());

                } catch (KlabAuthorizationException e) {

                    // bad certificate; warn and move on
                    KLAB.warn("error connecting to node "
                            + MiscUtilities.getFileBaseName(sc.toString()) + ": ignored: "
                            + ExceptionUtils.getFullStackTrace(e));
                    continue;

                } catch (Exception e) {

                    continue;
                }
            }
        }

        setNetworkMonitor(new NetworkMonitor() {

            @Override
            protected Network getNetwork() {

                Network ret = new Network();

                for (IServer n : quarantined) {

                    ((NodeClient) n).checkActivity();

                    if (n.isActive()) {
                        ret.getNodes().add(KLAB.MFACTORY.adapt(n, Node.class));
                        Relationship r = new Relationship();
                        r.setSource(KLAB.ENGINE.getName());
                        r.setTarget(n.getId());
                        ret.getStructure().add(r);
                        quarantined.remove(n);
                    }
                }
                synchronized (nodes) {
                    for (IServer n : NodeNetwork.this.nodes.values()) {
                        ret.getNodes().add(KLAB.MFACTORY.adapt(n, Node.class));
                        Relationship r = new Relationship();
                        r.setSource(KLAB.ENGINE.getName());
                        r.setTarget(n.getId());
                        ret.getStructure().add(r);
                    }
                }
                return ret;
            }
        });

        return true;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public boolean isOnline() {
        return isOnline;
    }

    public void quarantine(IServer node) {
        synchronized (nodes) {
            nodes.remove(node.getId());
            quarantined.add(node);
        }
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public void activate(boolean active) {
        // do nothing. Nodes are online by definition.
    }

}
