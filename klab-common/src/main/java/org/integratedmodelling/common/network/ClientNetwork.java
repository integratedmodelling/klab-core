/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.network;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.client.EngineController;
import org.integratedmodelling.common.configuration.KLAB;

/**
 * This one is responsible for building up the list of nodes based on calling context. A
 * list must be read from the suitable source:
 * 
 * the personal engine for clients, the primary server for personal engines, the public
 * server configuration and the nodes themselves for public servers.
 * 
 * After the nodes are in, the resource catalog should be rebuilt. The node list should be
 * refreshed periodically, and any differences due to nodes coming online or going offline
 * should be reflected in the resource catalog.
 */
public class ClientNetwork extends AbstractBaseNetwork implements INetwork {

    boolean          isOnline;
    EngineController engine;

    /**
     * This is empty unless we're a server; if so, it contains only the names of the nodes
     * we have certificates for.
     */
    List<String>     connectedNodeNames = new ArrayList<>();

    public ClientNetwork() {
    }

    /**
     * Pass an engine controller, which will be used to monitor the network at regular
     * intervals.
     * 
     * @param engine
     */
    public ClientNetwork(EngineController engine) {
        this.engine = engine;
        setNetworkMonitor(new NetworkMonitor() {
            @Override
            protected Network getNetwork() {
                try {
                    return engine.getNetwork();
                } catch (Throwable e) {
                    // Do nothing - may happen when engine is shutting down, too.
                    // TODO we should probably stop polling at this point?
                    return null;
                }
            }
        });
    }

    public boolean initialize() {
        return (isOnline = initializeClient());
    }

    public boolean initializeClient() {
        this.url = KLAB.ENGINE.getUrl();
        return true;
    }

    @Override
    public boolean isOnline() {
        return isOnline;
    }

    @Override
    public void activate(boolean active) {
        // TODO Auto-generated method stub

    }

}
