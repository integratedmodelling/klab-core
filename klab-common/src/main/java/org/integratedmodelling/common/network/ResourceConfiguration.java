package org.integratedmodelling.common.network;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.beans.Node;
import org.integratedmodelling.common.configuration.EditableProperties;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.common.utils.GitUtils;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Replacement for ResourceCatalog - just use (and maintain dynamically) a dedicated
 * property file detailing all aspects of server access. Only reflect and manage the
 * stated configuration; resources will be checked against this for
 * 
 * @author ferdinando.villa
 *
 */
public class ResourceConfiguration extends EditableProperties
        implements IResourceConfiguration {

    /*
     * prefix for component names to store with ID as key. Enum StaticResources uses the
     * name + "|".
     */
    private final static String ENGINE    = "E|";
    private final static String COMPONENT = "C|";
    private final static String SYNCED    = "S|";
    private final static String SHARED    = "Z|";

    /*
     * special meaning prefixes
     */
    private final static String SELF_USER = "_SELF_";
    private final static String NO_USER   = "_NONE_";
    private final static String LOCAL_IPS = "192.168.*.*";
    private final static String LOCALHOST = "127.0.0.1";

    class Identity {
        List<String> usernames = new ArrayList<>();
        Set<String>  groups    = new HashSet<>();
        List<String> ips       = new ArrayList<>();
    }

    class ResourcePermissions {
        boolean  allowed;
        Identity whitelist;
        Identity blacklist;
    }

    private Map<String, ResourcePermissions> permissions                    = new HashMap<>();
    private Set<String>                      declaredComponentIds           = new HashSet<>();
    private Set<String>                      declaredSharedProjectIds       = new HashSet<>();
    private Set<String>                      declaredSynchronizedProjectIds = new HashSet<>();
    private Set<StaticResource>              staticResources                = new HashSet<>();
    private String                           administrationKey;
    private List<String>                     gitUrls                        = new ArrayList<>();

    public ResourceConfiguration() {
        super("access.properties", null);
    }

    public ResourceConfiguration(URL prototype) {
        super("access.properties", prototype);
        process();
    }

    private void process() {

        if (getProperty("admin.key") != null) {
            administrationKey = getProperty("admin.key").toString();
            /*
             * TODO use vt-password to enforce key sanity
             */
        }

        /*
         * FIXME temporary - see what is configured, this should only be provided if there
         * are no restrictions on it.
         * 
         * FIXME this must be built by nodes and communicated
         */
        staticResources.add(StaticResource.MODEL_QUERY);
        staticResources.add(StaticResource.OBSERVATION_QUERY);

        /*
         * clear - we may do this repeatedly
         */
        permissions.clear();

        /*
         * set up defaults. We need defaults for the engine, changing according to the
         * role of the engine.
         */
        permissions.put(ENGINE, getDefaultEnginePermissions());

        /*
         * fill in resources first
         */
        for (Iterator< String>it = getKeys("server"); it.hasNext();) {
            String key = it.next();

        }
        for (Iterator< String>it = getKeys("observations"); it.hasNext();) {
            String key = it.next();

        }
        for (Iterator< String>it = getKeys("models"); it.hasNext();) {
            String key = it.next();

        }

        for (Iterator< String>it = getKeys("components"); it.hasNext();) {
            String key = it.next();
            if (key.equals("components")) {
                for (Object o : this.getList(key)) {
                    declaredComponentIds.add(o.toString());
                }
            }
        }
        for (Iterator< String>it = getKeys("projects"); it.hasNext();) {
            String key = it.next();
            if (key.equals("projects.shared")) {
                for (Object o : this.getList(key)) {
                    String pid = processProject(o.toString());
                    if (pid != null) {
                        declaredSharedProjectIds.add(pid);
                    }
                }
            } else if (key.equals("projects.synchronized")) {
                for (Object o : this.getList(key)) {
                    String pid = processProject(o.toString());
                    if (pid != null) {
                        declaredSynchronizedProjectIds.add(pid);
                    }
                }
            }
        }

        /*
         * then process restrictions for each
         */
    }

    /**
     * If project is a Git URL, clone or pull first and return project ID. Otherwise just
     * return passed string.
     * 
     * @param string
     * @return
     */
    private String processProject(String string) {
        String ret = string;
        if (GitUtils.isRemoteGitURL(string)) {
            try {
                ret = GitUtils.requireUpdatedRepository(string, KLAB.CONFIG.getDataPath("workspace/deploy"));
                this.gitUrls.add(string);
            } catch (KlabException e) {
                string = null;
                KLAB.error("error synchronizing repository " + string + ": " + e.getMessage());
            }
        }
        return ret;
    }

    private ResourcePermissions getDefaultEnginePermissions() {
        ResourcePermissions ret = new ResourcePermissions();

        return ret;
    }

    @Override
    public Collection<String> getComponentIds() {
        return declaredComponentIds;
    }

    @Override
    public Collection<String> getSharedProjectIds() {
        return declaredSharedProjectIds;
    }

    @Override
    public Collection<String> getSynchronizedProjectIds() {
        return declaredSynchronizedProjectIds;
    }

    @Override
    public boolean isAuthorized(IUser user, /* @Nullable */ String clientAddress) {
        return isAuthorized(user == null ? null : user.getUsername(), user == null
                ? new HashSet<>()
                : user.getGroups(), clientAddress);
    }

    @Override
    public boolean isAuthorized(IProject project, /* @Nullable */ IUser user, /* @Nullable */ String clientAddress) {
        return isAuthorized(project, user == null ? null
                : user.getUsername(), user == null ? new HashSet<>()
                        : user.getGroups(), clientAddress);
    }

    @Override
    public boolean isAuthorized(IComponent component, /* @Nullable */ IUser user, /* @Nullable */ String clientAddress) {
        return isAuthorized(component, user == null ? null
                : user.getUsername(), user == null
                        ? new HashSet<>() : user.getGroups(), clientAddress);
    }

    @Override
    public boolean isAuthorized(StaticResource resource, /* @Nullable */ IUser user, /* @Nullable */ String clientAddress) {
        return isAuthorized(resource, user == null ? null
                : user.getUsername(), user == null ? new HashSet<>()
                        : user.getGroups(), clientAddress);
    }

    @Override
    public boolean isAuthorized(/* @Nullable */ String username, Set<String> groups, String clientAddress) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAuthorized(IProject project, /* @Nullable */ String username, Set<String> groups, String clientAddress) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAuthorized(IComponent component, /* @Nullable */ String username, Set<String> groups, String clientAddress) {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public boolean isAuthorized(StaticResource resource, /* @Nullable */ String username, Set<String> groups, String clientAddress) {
        // TODO Auto-generated method stub
        return true;
    }

    /**
     * Called on modeling engines to merge in the capabilities from the wide area network.
     * 
     * TODO/CHECK: should be callable multiple times as the network changes, but for now
     * it doesn't diff anything. It should be OK if the resource factory is smart.
     * 
     * @param network
     * @throws KlabException
     */
    public void setNetworkResources(Network network, IUser user) throws KlabException {

        if (!KLAB.CONFIG.isOffline()) {

            for (Node node : network.getNodes()) {

                for (String urn : node.getSynchronizedProjectUrns()) {
                    IProject project = ResourceFactory
                            .getProjectFromURN(node.getUrl(), urn, node
                                    .getId(), KLAB.WORKSPACE
                                            .getDeployLocation(), user);
                    if (project != null) {
                        declaredSynchronizedProjectIds.add(project.getId());
                    }
                }

                for (String urn : node.getComponentUrns()) {
                    declaredComponentIds.add(ResourceFactory.getProjectIdFromUrn(urn));
                }
            }
        }

    }

    @Override
    public Collection<StaticResource> getStaticResources() {
        return staticResources;
    }

    @Override
    public String getAdministrationKey() {
        return administrationKey;
    }

    @Override
    public boolean updateRemoteProjects() throws KlabException {

        boolean ret = false;
        for (String url : gitUrls) {
            GitUtils.requireUpdatedRepository(url, KLAB.CONFIG.getDataPath("workspace/deploy"));
            ret = true;
        }
        return ret;
    }

}
