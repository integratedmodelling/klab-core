/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.metadata;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.exceptions.KlabInternalRuntimeException;

public class Metadata implements IMetadata, NetworkSerializable, NetworkDeserializable {

    protected Map<String, Object> _data = null;

    public Metadata() {
        _data = new HashMap<String, Object>();
    }

    public Metadata(Map<String, Object> data) {
        _data = data;
    }

    /**
     * Parse a string in the form key=value,... Only basic token recognition is done, for
     * integer, double and string. Commas in the string values will be a source of endless
     * fun.
     * 
     * @param s
     * @return parsed metadata
     */
    public static Metadata parse(String s) {

        Map<String, Object> map = new HashMap<>();

        String[] ss = s.split(",");
        for (String sss : ss) {
            String[] kv = sss.split("=");
            Integer intg = null;
            Double doub = null;
            if (kv.length == 2) {
                try {
                    intg = Integer.parseInt(kv[1]);
                } catch (Throwable e) {
                }
                if (intg == null) {
                    try {
                        doub = Double.parseDouble(kv[1]);
                    } catch (Throwable e) {
                    }
                }
                map.put(kv[0], intg != null ? intg : (doub != null ? doub : kv[1]));
            }
        }
        return new Metadata(map);
    }

    @Override
    public void put(String id, Object value) {
        _data.put(id, value);
    }

    @Override
    public Collection<String> getKeys() {
        return _data.keySet();
    }

    @Override
    public Object get(String string) {
        return _data.get(string);
    }

    @Override
    public void merge(IMetadata md, boolean overwriteExisting) {
        if (overwriteExisting) {
            _data.putAll(((Metadata) md)._data);
        } else {
            for (String key : md.getKeys()) {
                if (!_data.containsKey(key))
                    _data.put(key, md.get(key));
            }
        }
    }

    @Override
    public String getString(String field) {
        Object o = _data.get(field);
        return o != null ? o.toString() : null;
    }

    @Override
    public Integer getInt(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).intValue()
                : (o instanceof String ? Integer.parseInt(o.toString()) : null);
    }

    @Override
    public Long getLong(String field) {
        Object o = _data.get(field);
        return o instanceof Number ? ((Number) o).longValue()
                : (o instanceof String ? Long.parseLong(o.toString()) : null);
    }

    @Override
    public Double getDouble(String field) {
        Object o = _data.get(field);
        try {
            if (o instanceof Number)
                return ((Number) o).doubleValue();
            if (o instanceof String)
                return Double.parseDouble(o.toString());
        } catch (Throwable e) {
            // just null
        }
        return null;
    }

    @Override
    public Float getFloat(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).floatValue()
                : (o instanceof String ? Float.parseFloat(o.toString()) : null);
    }

    @Override
    public Boolean getBoolean(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof Boolean ? (Boolean) o
                : (o instanceof String ? Boolean.parseBoolean(o.toString()) : null);
    }

    @Override
    public IConcept getConcept(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof IConcept ? (IConcept) o
                : (o instanceof String ? Knowledge.parse(o.toString()) : null);
    }

    @Override
    public String getString(String field, String def) {
        Object o = _data.get(field);
        return o != null ? o.toString() : def;
    }

    @Override
    public int getInt(String field, int def) {
        Object o = _data.get(field);
        return o != null ? getInt(field) : def;
    }

    @Override
    public long getLong(String field, long def) {
        Object o = _data.get(field);
        return o != null ? getLong(field) : def;
    }

    @Override
    public double getDouble(String field, double def) {
        Object o = _data.get(field);
        return o != null ? getDouble(field) : def;
    }

    @Override
    public float getFloat(String field, float def) {
        Object o = _data.get(field);
        return o != null ? getFloat(field) : def;
    }

    @Override
    public boolean getBoolean(String field, boolean def) {
        Object o = _data.get(field);
        return o != null ? getBoolean(field) : def;
    }

    @Override
    public IConcept getConcept(String field, IConcept def) {
        Object o = _data.get(field);
        return o != null ? getConcept(field) : def;
    }

    @Override
    public Collection<Object> getValues() {
        return _data.values();
    }

    public void putAll(IMetadata metadata) {
        _data.putAll(((Metadata) metadata)._data);
    }

    @Override
    public void remove(String key) {
        _data.remove(key);
    }

    @Override
    public Map<? extends String, ? extends Object> getDataAsMap() {
        return _data;
    }

    @Override
    public boolean contains(String key) {
        return _data.containsKey(key);
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Metadata)) {
            throw new KlabInternalRuntimeException("cannot adapt a "
                    + object.getClass().getCanonicalName() + " to a metadata object");
        }

        org.integratedmodelling.common.beans.Metadata bean = (org.integratedmodelling.common.beans.Metadata) object;
        _data.putAll(bean.getData());
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Metadata.class)) {
            throw new KlabInternalRuntimeException("cannot adapt a space extent to "
                    + desiredClass.getCanonicalName());
        }
        org.integratedmodelling.common.beans.Metadata ret = new org.integratedmodelling.common.beans.Metadata();
        for (String k : _data.keySet()) {
            // we don't know where it's been and JSON is fussy
            Object o = _data.get(k);
            if (o instanceof String || o instanceof Number || o instanceof Boolean
                    || o instanceof IModelBean) {
                ret.getData().put(k, o instanceof String ? ((String) o).replaceAll("\\P{Print}", "") : o);
            }
        }
        return (T) ret;
    }

    @Override
    public boolean isEmpty() {
        return _data == null || _data.isEmpty();
    }

}
