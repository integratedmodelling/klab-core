///*******************************************************************************
// *  Copyright (C) 2007, 2015:
// *  
// *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
// *    - integratedmodelling.org
// *    - any other authors listed in @author annotations
// *
// *    All rights reserved. This file is part of the k.LAB software suite,
// *    meant to enable modular, collaborative, integrated 
// *    development of interoperable data and model components. For
// *    details, see http://integratedmodelling.org.
// *    
// *    This program is free software; you can redistribute it and/or
// *    modify it under the terms of the Affero General Public License 
// *    Version 3 or any later version.
// *
// *    This program is distributed in the hope that it will be useful,
// *    but without any warranty; without even the implied warranty of
// *    merchantability or fitness for a particular purpose.  See the
// *    Affero General Public License for more details.
// *  
// *     You should have received a copy of the Affero General Public License
// *     along with this program; if not, write to the Free Software
// *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// *     The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
//package org.integratedmodelling.common;
//
//import java.io.File;
//import java.io.FileFilter;
//import java.util.HashSet;
//import java.util.StringTokenizer;
//
//import org.apache.commons.io.FilenameUtils;
//import org.apache.commons.io.filefilter.WildcardFileFilter;
//import org.integratedmodelling.Version;
//import org.integratedmodelling.common.configuration.KLAB;
//import org.integratedmodelling.common.engine.LocalDevelopmentEngine;
//import org.integratedmodelling.common.utils.ZipUtils;
//
//@Deprecated
//public class Resources {
//
//    static public File     jarfile      = null;
//    static HashSet<String> _resourceSet = new HashSet<>();
//
//    // if this is set before synchronization, we're looking for a commons plugin with that version (used
//    // within Eclipse.
//    static public String versionSuffix = null;
//
//    static {
//        _resourceSet.add("knowledge/");
//        _resourceSet.add("war/");
//        _resourceSet.add("ssh/");
//        _resourceSet.add("datamapper/");
//    }
//
//    static private File _kPath;
//    @Deprecated
//    static private File _wPath;
//    static private File _ePath;
//    static private File _vPath;
//
//    /**
//     * Returns a reference to a file with the specified name that is located
//     * somewhere on the classpath.
//     */
//    public static File findFileOnClassPath(final String fileName) {
//
//        HashSet<File> dirs = new HashSet<>();
//
//        String classpath = System.getProperty("java.class.path");
//        if (classpath.endsWith(".jar")) {
//            classpath = FilenameUtils.getPath(classpath).toString();
//        }
//        final String pathSeparator = System.getProperty("path.separator");
//        final StringTokenizer tokenizer = new StringTokenizer(classpath, pathSeparator);
//
//        while (tokenizer.hasMoreTokens()) {
//
//            final String pathElement = tokenizer.nextToken();
//            final File directoryOrJar = new File(pathElement);
//            final File absoluteDirectoryOrJar = directoryOrJar.getAbsoluteFile();
//
//            // System.out.println("looking for " + fileName + " in file: " + pathElement);
//
//            if (absoluteDirectoryOrJar.isFile()) {
//
//                final File target = new File(absoluteDirectoryOrJar.getParent(), fileName);
//                if (target.exists()) {
//                    return target;
//                }
//
//            } else {
//
//                if (dirs.contains(absoluteDirectoryOrJar)) {
//                    continue;
//                }
//
//                dirs.add(absoluteDirectoryOrJar);
//
//                FileFilter fileFilter = new WildcardFileFilter(fileName);
//                File[] files = directoryOrJar.listFiles(fileFilter);
//                // TODO/CHECK: assuming the latest version is last alphabetically.
//                if (files != null && files.length > 0) {
//                    return files[files.length - 1];
//                }
//            }
//        }
//
//        return null;
//    }
//
//    @Deprecated
//    public static void synchronizeResources() throws Exception {
//
//        if (_wPath != null && _kPath != null && _ePath != null && _vPath != null) {
//            return;
//        }
//
//        if (jarfile != null && jarfile.isDirectory()) {
//            _wPath = new File(jarfile + File.separator + "war");
//            _kPath = new File(jarfile + File.separator + "knowledge");
//            _ePath = new File(jarfile + File.separator + "ssh");
//            _vPath = new File(jarfile + File.separator + "datamapper");
//            return;
//        }
//
//        File wpath = KLAB.CONFIG.getDataPath();
//
//        if (System.getProperties()
//                .getProperty(LocalDevelopmentEngine.THINKLAB_SOURCE_DISTRIBUTION_PROPERTY) != null) {
//
//            File cpath = new File(System.getProperties()
//                    .getProperty(LocalDevelopmentEngine.THINKLAB_SOURCE_DISTRIBUTION_PROPERTY));
//
//            File kdir = new File(cpath + "/common/src/main/resources/knowledge");
//            if (kdir.exists() && kdir.isDirectory()) {
//                _kPath = kdir;
//            }
//            kdir = new File(cpath + "/common/src/main/resources/war");
//            if (/* !forceNodev && */kdir.exists() && kdir.isDirectory()) {
//                _wPath = kdir;
//            }
//            kdir = new File(cpath + "/common/src/main/resources/ssh");
//            if (/* !forceNodev && */kdir.exists() && kdir.isDirectory()) {
//                _ePath = kdir;
//            }
//            kdir = new File(cpath + "/common/src/main/resources/datamapper");
//            if (/* !forceNodev && */kdir.exists() && kdir.isDirectory()) {
//                _vPath = kdir;
//            }
//        }
//
//        if (_wPath != null && _kPath != null && _ePath != null && _vPath != null) {
//            return;
//        }
//
//        if (jarfile == null) {
//            jarfile = findFileOnClassPath(Version.getJarFilename("im-common"));
//        }
//        if (jarfile == null) {
//            // TODO must use version that gets plugin by matching start name
//            jarfile = findFileOnClassPath(versionSuffix == null ? "org.integratedmodelling.common_*.jar"
//                    : "org.integratedmodelling.common_" + versionSuffix + "*.jar");
//        }
//
//        if (jarfile != null) {
//            ZipUtils.extractDirectories(jarfile, wpath, _resourceSet);
//        }
//
//        if (_kPath == null) {
//            File kdir = new File(wpath + File.separator + "knowledge");
//            if (kdir.exists() && kdir.isDirectory()) {
//                _kPath = kdir;
//            }
//        }
//        if (_wPath == null) {
//            File kdir = new File(wpath + File.separator + "war");
//            if (kdir.exists() && kdir.isDirectory()) {
//                _wPath = kdir;
//            }
//        }
//        if (_ePath == null) {
//            File kdir = new File(wpath + File.separator + "ssh");
//            if (kdir.exists() && kdir.isDirectory()) {
//                _ePath = kdir;
//            }
//        }
//        if (_vPath == null) {
//            File kdir = new File(wpath + File.separator + "datamapper");
//            if (kdir.exists() && kdir.isDirectory()) {
//                _vPath = kdir;
//            }
//        }
//
//    }
//
//    /**
//     * TODO move to configuration
//     * @return
//     * @throws Exception
//     */
//    public static File getKnowledgePath() throws Exception {
//
//        File ret = null;
//
//        if (System.getProperties()
//                .getProperty(LocalDevelopmentEngine.THINKLAB_SOURCE_DISTRIBUTION_PROPERTY) != null) {
//
//            File cpath = new File(System.getProperties()
//                    .getProperty(LocalDevelopmentEngine.THINKLAB_SOURCE_DISTRIBUTION_PROPERTY));
//
//            File kdir = new File(cpath + "/common/src/main/resources/knowledge");
//            if (kdir.exists() && kdir.isDirectory()) {
//                ret = kdir;
//            }
//        }
//
//        if (ret == null) {
//            ret = KLAB.CONFIG.getDataPath("knowledge");
//        }
//
//        return ret;
//    }
//
//    @Deprecated
//    public static File getWarPath() throws Exception {
//        synchronizeResources();
//        return _wPath;
//    }
//
//    @Deprecated
//    public static File getEtcPath() throws Exception {
//        synchronizeResources();
//        return _ePath;
//    }
//
//    @Deprecated
//    public static File getVisualizerPath() throws Exception {
//        synchronizeResources();
//        return _vPath;
//    }
//
//}
