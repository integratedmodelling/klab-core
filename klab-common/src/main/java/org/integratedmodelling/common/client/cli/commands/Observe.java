/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import java.util.ArrayList;

import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Session;
import org.integratedmodelling.exceptions.KlabException;

import com.google.common.collect.Iterables;

/**
 * The observe command will observe a new context if the current context is null or
 * if the observable is a direct observation and the --add option was not passed.
 * 
 * @author Ferd
 *
 */
@CLIPrototype
@Prototype(
        id = "observe",
        description = "start an observation in the current context",
        args = {
                "what",
                Prototype.TEXT,
                "# what1",
                Prototype.TEXT,
                "# what2",
                Prototype.TEXT,
                "# what3",
                Prototype.TEXT,
                "# what4",
                Prototype.TEXT,
                "# what5",
                Prototype.TEXT,
                "# what6",
                Prototype.TEXT,
                "# what7",
                Prototype.TEXT,
                "? a|add",
                Prototype.NONE,
                "? c|context",
                Prototype.INT
        },
        argDescriptions = {
                "what to observe",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "what to observe (optional)",
                "reset context before observation",
                "choose the context to observe into" })
public class Observe {

    /**
     * @param command
     * @return the result of observing (normally a task).
     * @throws KlabException
     */
    @Execute
    public Object observe(IServiceCall command) throws KlabException {

        if (!(KLAB.ENGINE instanceof IModelingEngine)) {
            return null;
        }

        IContext context = null;
        if (command.has("context")) {
            context = ((Session) command.getSession()).getContext(command.getString("context"));
        } else {
            context = Environment.get().getContext();
        }

        IModelObject mob = KLAB.MMANAGER.findModelObject(command.getString("what"));
        boolean isDirect = mob instanceof IDirectObserver;

        /*
         * TODO add specifications
         */
        java.util.List<String> scenarios = new ArrayList<>();

        if (context == null || (isDirect && !command.has("add"))) {
            java.util.List<IExtent> forcings = new ArrayList<>();

            // TODO link to options
            forcings.add(Environment.get().getSpatialForcing());

            return command.getSession()
                    .observe(command.getString("what"), scenarios, forcings);
        }
        return context.inScenario(Iterables.toArray(scenarios, String.class)).observe(command.get("what"));
    }
}
