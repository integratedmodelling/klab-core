/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client;

import java.io.File;
import java.net.URL;

import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.URLUtils;

/**
 * Interfaces to the Google Drive document(s) containing updated release notes
 * for the system. Used to show what updates are available. May eventually connect
 * to more appropriate JIRA services.
 * 
 * @author ferdinando.villa
 *
 */
public class ReleaseNotes {

    private static String url = "https://drive.google.com/uc?export=download&id=0B8J2y9DGE9t3cmtqMmdDNlp2X2s";

    /**
     * Get the full release notes document as a string of ASCII text. If anything goes wrong,
     * returns null. Never throws exceptions.
     * 
     * @return the release notes as a string
     */
    public static String get() {

        try {
            File temp = File.createTempFile("rln", "txt");
            URLUtils.copy(new URL(url), temp);
            return FileUtils.readFileToString(temp);
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        System.out.println(get());
    }
}
