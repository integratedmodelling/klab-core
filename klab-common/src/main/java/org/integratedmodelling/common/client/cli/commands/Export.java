/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import java.io.File;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Export states and datasets.
 * 
 * @author ferdinando.villa
 *
 */
@CLIPrototype
@Prototype(
        id = "export",
        description = "export ontologies, datasets from contexts and other resources",
        args = {
                "# target",
                Prototype.TEXT,
                "# output",
                Prototype.TEXT,
                "? c|context",
                Prototype.INT
        },
        argDescriptions = { "target identifier (state, namespace, observation)", "output file", "context (default current)" })
public class Export {

    @Execute
    public Object export(IServiceCall command) throws KlabException {

        File output = null;
        
        if (command.has("target")) {
            
            String target = command.getString("target");
            output = command.has("output") ? new File(command.getString("output")) : null;
            
            INamespace ns = KLAB.MMANAGER.getNamespace(target);
            if (ns != null) {
                ns.getOntology().write(output, true);
            }
            
            /*
             * 
             */
            
        } else {
            /*
             * TODO must have context, export it all
             */
        }
        
        return output;
    }
}
