package org.integratedmodelling.common.client.viewer;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.common.space.SpaceLocator;

class TimeView extends AbstractView implements DisplayState {

    /**
     * The stack of maps being shown at the moment. Changed by showMap() and reset when
     * the focus is set.
     */
    Deque<IState> states = new LinkedList<>();

    /**
     * Timeplots shown in this view.
     */
    List<List<Double>> timeplots = new ArrayList<>();

    /**
     * these are all the spatial locations we explored so far - kept so that we can show
     * them alongside the current one in a plot or table.
     */
    List<SpaceLocator> spaceExplored = new ArrayList<>();

    @Override
    public void clear() {
        states.clear();
        timeplots.clear();
        spaceExplored.clear();
    }

    @Override
    public boolean isShown(IObservation observation) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Result show(IObservation observation) {
        // TODO Auto-generated method stub
        return Result.IGNORE;
    }

    @Override
    public Result hide(IObservation observation) {
        // TODO Auto-generated method stub
        return Result.IGNORE;
    }
}
