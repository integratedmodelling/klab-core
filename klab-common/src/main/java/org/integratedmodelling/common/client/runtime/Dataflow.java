package org.integratedmodelling.common.client.runtime;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.resolution.IDataflow;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.common.beans.generic.Graph;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.jgrapht.graph.DefaultDirectedGraph;

public class Dataflow extends DefaultDirectedGraph<IDataflow.ProcessingStep, IDataflow.Datapath>
		implements IDataflow, NetworkDeserializable {

	private static final long serialVersionUID = 6120910715977831258L;

	class Step implements IDataflow.ProcessingStep {

		String id;
		String label;
		String type;
		IMetadata metadata;

		Step(String id, String label, String type, IMetadata md) {
			this.id = id;
			this.label = label;
			this.metadata = md;
			this.type = type;
		}

		@Override
		public int hashCode() {
			return id.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return obj instanceof Step && ((Step) obj).id.equals(id);
		}
	}

	class Path implements IDataflow.Datapath {

		String sId;
		String tId;
		String id;
		String label;
		IMetadata metadata;

		@Override
		public ProcessingStep getSourceStep() {
			return nodes.get(sId);
		}

		@Override
		public ProcessingStep getTargetStep() {
			return nodes.get(tId);
		}

		Path(String id, String sId, String tId, String label, IMetadata metadata) {
			this.id = id;
			this.sId = sId;
			this.tId = tId;
			this.label = label.startsWith("_") ? "" : label;
			this.metadata = metadata;
		}

	}

	Map<String, Step> nodes = new HashMap<>();

	public Dataflow() {
		super(IDataflow.Datapath.class);
	}

	public GraphVisualization visualize() {

		GraphVisualization ret = new GraphVisualization();

		ret.adapt(this, new GraphVisualization.GraphAdapter<IDataflow.ProcessingStep, IDataflow.Datapath>() {

			@Override
			public String getNodeType(ProcessingStep o) {
				return ((Step)o).type;
			}

			@Override
			public String getNodeId(ProcessingStep o) {
				return ((Step) o).id;
			}

			@Override
			public String getNodeLabel(ProcessingStep o) {
				return ((Step) o).label;
			}

			@Override
			public String getNodeDescription(ProcessingStep o) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getEdgeType(Datapath o) {
				return "datapath";
			}

			@Override
			public String getEdgeId(Datapath o) {
				return ((Path) o).id;
			}

			@Override
			public String getEdgeLabel(Datapath o) {
				return ((Path) o).label;
			}

			@Override
			public String getEdgeDescription(Datapath o) {
				return null;
			}
		});

		return ret;
	}

	@Override
	public void deserialize(IModelBean object) {

		if (!(object instanceof Graph)) {
			throw new KlabRuntimeException(
					"cannot deserialize an Dataflow from a " + object.getClass().getCanonicalName());
		}
		Graph bean = (Graph) object;

		for (String v : bean.getNodes()) {
			String[] s = v.split("\\|");
			;
			Step step = new Step(s[0], s[1], s[2], KLAB.MFACTORY.adapt(bean.getMetadata().get(s[0]), Metadata.class));
			nodes.put(s[0], step);
			addVertex(step);
		}

		for (String v : bean.getRelationships()) {
			String[] s = v.split("\\|");
			Path path = new Path(s[0], s[1], s[2], (s.length > 3 ? s[3] : ""),
					KLAB.MFACTORY.adapt(bean.getMetadata().get(s[0]), Metadata.class));
			Step source = nodes.get(s[1]);
			Step target = nodes.get(s[2]);
			addEdge(source, target, path);
		}
	}

	/*
	 * these dataflows won't run.
	 */
	@Override
	public boolean run(ITransition transition) {
		return false;
	}

    @Override
    public int getDepth() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getDepth(IDirectObservation observation) {
        // TODO Auto-generated method stub
        return 0;
    }

}
