/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import java.io.File;
import java.util.Properties;
import java.util.UUID;

import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.auth.LicenseManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.exceptions.KlabConfigurationException;
import org.joda.time.DateTime;

/**
 * Create certificate for server. Will only work if keys are in place and referenced through 
 * auth.properties.
 * 
 * Call it with either the -u <username> option or the -s <serverId> option to create either
 * user or server certificates.
 * 
 * @author Ferd
 *
 */
@CLIPrototype
@Prototype(
        id = "certify",
        description = "create a server certificate (needs full authorization configuration)",
        args = {
                "? s|server",
                Prototype.TEXT,
                "? u|user",
                Prototype.TEXT,
                "? tp|testport",
                Prototype.INT,
                "? l|list",
                Prototype.NONE,
                "url",
                Prototype.TEXT,
                "# email",
                Prototype.TEXT,
                "# auth",
                "NONE|DIRECT|INDIRECT",
                "? o|output",
                Prototype.TEXT })
public class Certify {

    private static final int CERT_FILE_TTL_DAYS = 365;

    @Execute
    public Object createServerCertificate(IServiceCall command) throws Exception {

        boolean isServer = command.has("server");
        boolean isUser = command.has("user");

        String outputFile = isServer ? "server.cert" : "im.cert";
        if (command.has("output")) {
            outputFile = command.get("output").toString();
        }

        Properties properties = new Properties();
        File out = new File(outputFile);

        /**
         * Create a copy of an existing certificate with a test.port property added to it. The
         * certificate will authenticate properly but it can be used for local testing.
         */
        if (command.has("testport") || command.has("list")) {

            File certFile = new File(command.getString("url"));
            File publicKey = new File(KLAB.CONFIG.getDataPath()
                    + File.separator + "ssh" +
                    File.separator + "pubring.gpg");

            properties = LicenseManager.readCertificate(certFile, publicKey);

            if (command.has("list")) {
                return MapUtils.dump(properties);
            }

            properties.setProperty("test.port", "" + Integer.parseInt(command.getString("testport")));
            LicenseManager.createCertificate(properties, out);
            return out.getAbsolutePath();
        }

        if (!(isServer || isUser)) {
            throw new KlabConfigurationException("either a user or a server name must be specified");
        }

        if (!command.has("email")) {
            throw new KlabConfigurationException("an email address must be specified");
        }

        if (isServer) {
            IServer.Authentication auth = IServer.Authentication.valueOf(command.getString("auth"));
            properties.setProperty("name", command.getString("server"));
            properties.setProperty("authentication", auth.name());
            properties.setProperty("url", command.getString("url"));
        }

        if (isUser) {
            properties.setProperty("user", command.getString("user"));
            properties.setProperty("primary.server", command.getString("url"));
        }

        properties.setProperty("email", command.getString("email"));
        properties.setProperty("key", UUID.randomUUID().toString());
        properties.setProperty("expiry", DateTime.now().plusDays(CERT_FILE_TTL_DAYS).toString());

        LicenseManager.createCertificate(properties, out);
        return out.getAbsolutePath();
    }

}
