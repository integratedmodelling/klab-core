/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.viewer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.collections.ImmutableList;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Initialize with a context path; it will produce the observation(s) in it and the
 * context they're in.
 * 
 * Context is defined as C<n>, followed by slash-separated locators using
 *  S for state
 *  E for event
 *  R for relationship
 *  O for subject
 *  P for process
 * 
 * So third state in first sub-object of context 3 is C3/O1/S3. Counting starts at 1 and
 * reflects the numbering in state display.
 * 
 * Omitting the number selects all the matching observation at that level:
 * 
 *      C1/O all sub-objects
 *      C1/S all states
 *      etc...
 * 
 * @author ferdinando.villa
 *
 */
public class ContextPath extends ImmutableList<IObservation> {

    IContext           context;
    List<IObservation> observations = new ArrayList<>();
    static Set<String> admitted     = new HashSet<>();

    static {
        admitted.add("S");
        admitted.add("C");
        admitted.add("O");
        admitted.add("R");
        admitted.add("P");
        admitted.add("E");
    }

    /**
     * Return a "table of contents" string representation of the current context.
     * @param context
     * @return text describing the TOC for the context
     */
    public static String dumpContextPath(IContext context) {
        return dumpInternal(context.getSubject(), 0, new int[] { 1, 1, 1, 1, 1 });
    }

    private static String dumpInternal(IDirectObservation subject, int indent, int[] counters) {

        String ret = StringUtils.spaces(indent);

        int counter = 0;
        if (subject instanceof ISubject) {
            counter = counters[0];
            counters[0] = counter + 1;
            ret += "O";
        } else if (subject instanceof IEvent) {
            counter = counters[1];
            counters[1] = counter + 1;
            ret += "E";
        } else if (subject instanceof IRelationship) {
            counter = counters[2];
            counters[2] = counter + 1;
            ret += "R";
        } else if (subject instanceof IProcess) {
            counter = counters[3];
            counters[3] = counter + 1;
            ret += "P";
        }

        ret += counter + " " + subject;

        for (IState s : subject.getStates()) {
            ret += "\n" + StringUtils.spaces(indent + 3) + "S" + counters[4] + " " + s;
            counters[4] = counters[4] + 1;
        }
        if (subject instanceof ISubject) {
            for (ISubject s : ((ISubject) subject).getSubjects()) {
                ret += "\n" + dumpInternal(s, indent + 3, counters);
            }
            for (IEvent s : ((ISubject) subject).getEvents()) {
                ret += "\n" + dumpInternal(s, indent + 3, counters);
            }
            for (IProcess s : ((ISubject) subject).getProcesses()) {
                ret += "\n" + dumpInternal(s, indent + 3, counters);
            }
            for (IRelationship s : ((ISubject) subject).getStructure().getRelationships()) {
                ret += "\n" + dumpInternal(s, indent + 3, counters);
            }
        }
        return ret;
    }

    private Pair<Character, Integer> loc(String s) {

        if (s.isEmpty()) {
            return null;
        }

        String ch = s.substring(0, 1);

        if (!admitted.contains(ch)) {
            throw new KlabRuntimeException("cannot interpret path " + s
                    + ". Specifiers can be O(bject), E(vent), R(elationship), P(rocess), or S(tate).");
        }

        int id = -1;
        if (s.length() > 1) {
            id = Integer.parseInt(s.substring(1));
        }

        if (id == 0) {
            throw new KlabRuntimeException("observation selectors are numbered starting at 1");
        }

        return new Pair<>(ch.charAt(0), id);
    }

    public ContextPath(IContext context, String path) {

        this.context = context;
        IDirectObservation container = context.getSubject();

        String[] elements = path.split("/");

        for (int i = 0; i < elements.length; i++) {

            Pair<Character, Integer> loc = loc(elements[i]);

            if (i == 0) {
                if (elements.length == 1) {
                    this.observations.add(container);
                }
            } else if (i == elements.length - 1) {
                /*
                 * the last element(s) we want to visualize
                 */
                switch (loc.getFirst()) {
                case 'O':
                    fillWithSubjects(container, loc.getSecond());
                    break;
                case 'E':
                    fillWithEvents(container, loc.getSecond());
                    break;
                case 'R':
                    fillWithRelationships(container, loc.getSecond());
                    break;
                case 'P':
                    fillWithProcesses(container, loc.getSecond());
                    break;
                case 'S':
                    fillWithStates(container, loc.getSecond());
                    break;
                }

            } else {

                if (loc.getSecond() < 0) {
                    throw new KlabRuntimeException("cannot use a generic path at the intermediate level: please indicate a specific observation with its id");
                }

                switch (loc.getFirst()) {
                case 'O':
                    if (container instanceof ISubject) {
                        for (ISubject s : ((ISubject) container).getSubjects()) {
                            if (loc.getSecond() == (i + 1)) {
                                container = s;
                                break;
                            }
                            i++;
                        }
                    }
                    break;
                case 'E':
                    if (container instanceof ISubject) {
                        for (IEvent s : ((ISubject) container).getEvents()) {
                            if (loc.getSecond() == (i + 1)) {
                                container = s;
                                break;
                            }
                            i++;
                        }
                    }
                    break;
                case 'R':
                    break;
                default:
                    throw new KlabRuntimeException("the element at " + loc.getFirst()
                            + " cannot be at the intermediate level");
                }
            }
        }
    }

    private void fillWithStates(IDirectObservation container, Integer second) {
        int i = 0;
        for (IState s : container.getStates()) {
            if (second < 1 || (second == (i + 1))) {
                observations.add(s);
            }
            i++;
        }
    }

    private void fillWithProcesses(IDirectObservation container, Integer second) {
        int i = 0;
        if (container instanceof ISubject) {
            for (IProcess s : ((ISubject) container).getProcesses()) {
                if (second < 1 || (second == (i + 1))) {
                    observations.add(s);
                }
                i++;
            }
        }
    }

    private void fillWithRelationships(IDirectObservation container, Integer second) {
        int i = 0;
        if (container instanceof ISubject) {
            for (IRelationship s : ((ISubject) container).getStructure().getRelationships()) {
                if (second < 1 || (second == (i + 1))) {
                    observations.add(s);
                }
                i++;
            }
        }
    }

    private void fillWithEvents(IDirectObservation container, Integer second) {
        int i = 0;
        if (container instanceof ISubject) {
            for (IEvent s : ((ISubject) container).getEvents()) {
                if (second < 1 || (second == (i + 1))) {
                    observations.add(s);
                }
                i++;
            }
        }
    }

    private void fillWithSubjects(IDirectObservation container, Integer second) {
        int i = 0;
        if (container instanceof ISubject) {
            for (ISubject s : ((ISubject) container).getSubjects()) {
                if (second < 1 || (second == (i - 1))) {
                    observations.add(s);
                }
                i++;
            }
        }
    }

    public IContext getContext() {
        return context;
    }

    @Override
    public boolean contains(Object arg0) {
        return observations.contains(arg0);
    }

    @Override
    public IObservation get(int arg0) {
        return observations.get(arg0);
    }

    @Override
    public Iterator<IObservation> iterator() {
        return observations.iterator();
    }

    @Override
    public int size() {
        return observations.size();
    }

    @Override
    public Object[] toArray() {
        return observations.toArray();
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        return observations.toArray(arg0);
    }

}
