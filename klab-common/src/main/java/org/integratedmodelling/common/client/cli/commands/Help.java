/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import java.util.ArrayList;

import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Context command.
 * 
 * @author ferdinando.villa
 *
 */
@CLIPrototype
@Prototype(
        id = "help",
        description = "get help",
        args = { "# command", Prototype.TEXT },
        argDescriptions = { "command for detailed description" })
public class Help {

    @Execute
    public Object help(IServiceCall command) {

        if (command.has("command")) {

            IPrototype prototype = ServiceManager.get().getPrototype(command.get("command").toString());
            if (prototype == null) {
                throw new KlabRuntimeException("command " + command.get("command") + " unknown");
            }
            return prototype.getSynopsis();
        }

        ArrayList<String> ret = new ArrayList<>();
        for (IPrototype p : ServiceManager.get().getPrototypes()) {
            ret.add(p.getShortSynopsis());
            ret.add("  " + p.getDescription());
        }

        return ret;
    }
}
