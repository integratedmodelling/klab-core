package org.integratedmodelling.common.client.palette;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.client.referencing.KnowledgeSearch.InherencyModifier;
import org.integratedmodelling.common.client.referencing.KnowledgeSearch.SearchModifier;
import org.integratedmodelling.common.utils.NameGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * These are glorified bookmarks with children and persistent UI specifiers (color,
 * column) and transient fields that allow them to keep state for use in complex user
 * tools. They are usually arranged in palette folders read from palette files, but may
 * exist alone (e.g. as the results of searches). Some (e.g. those that represent
 * modifiers for other concepts) cannot be persisted, but any other can be persisted as
 * JSON in palettes to become part of a toolkit instrumentation.
 * 
 * @author Ferd
 *
 */
@EqualsAndHashCode(
        exclude = {
                "description",
                "status",
                "children",
                "requires",
                "accepts" })
public @Data class PaletteItem {

    private String                      name;
    private String                      description;
    private String                      help;
    private String                      modelObject;
    private int                         column;
    private String                      color;
    private String                      status;
    private String                      onSelect;

    /*
     * objects that must be in the context before this can be dropped on an observation
     * canvas. Usually roles or observables, but may be anything.
     */
    private List<String>                requires    = new ArrayList<>();

    /*
     * items that may be in the context before this can be dropped on an observation
     * canvas.
     */
    private List<String>                accepts     = new ArrayList<>();

    @JsonIgnore
    transient private boolean           expanded    = false;
    @JsonIgnore
    transient private boolean           active      = false;
    @JsonIgnore
    transient private boolean           selected    = false;

    /*
     * this is hosted in an item only during searches. Items incarnating one of these
     * cannot become part of a palette.
     */
    @JsonIgnore
    transient private SearchModifier    modifier    = null;
    @JsonIgnore
    transient private InherencyModifier inherency   = null;
    @JsonIgnore
    transient private INamespace        namespace   = null;
    @JsonIgnore
    transient private IConcept          concept     = null;

    /*
     * current percent readiness, i.e. ability to be observed. Reset to default (0 or 1
     * according to having dependencies) after read and before write. Has to be 1 for the
     * tool to be observable.
     */
    @JsonIgnore
    transient private float             readiness   = 0;
    @JsonIgnore
    transient private List<String>      readyColors = new ArrayList<>();
    @JsonIgnore
    transient private boolean           observed;

    private List<PaletteItem>           children    = new ArrayList<>();

    /**
     * managed by {@link Palette#initialize()}
     */
    @JsonIgnore
    transient private String            internalId;
    @JsonIgnore
    transient private PaletteFolder     folder;

    public String createTransferSignature() {
        return "@PI|" + internalId;
    }

    void initialize(PaletteFolder folder, String parentId) {
        this.folder = folder;
        this.readiness = this.requires.size() > 0 ? 0 : 1;
        this.observed = false;
        this.internalId = parentId + "/" + NameGenerator.shortUUID();
        for (PaletteItem item : children) {
            item.initialize(folder, this.internalId);
        }
    }

    void addChild(PaletteItem item) {
        item.initialize(this.folder, this.internalId);
        children.add(item);
    }

    public void detach() {
        List<PaletteItem> list = folder.findListContaining(folder.getItems(), this);
        if (list != null) {
            list.remove(this);
        }
    }

    public PaletteItem() {
    }

    public PaletteItem(SearchModifier search, String name, String description) {
        this.modifier = search;
        this.name = name;
        this.description = description;
    }

    public PaletteItem(InherencyModifier search, String name, String description) {
        this.inherency = search;
        this.name = name;
        this.description = description;
    }

    public PaletteItem(INamespace namespace, String description) {
        this.namespace = namespace;
        this.name = namespace.getId();
        this.description = description;
    }

    public PaletteItem(IConcept concept) {
        this.concept = concept;
        this.modelObject = concept.getDefinition();
        // TODO use best label
        this.name = concept.toString();
        this.description = concept.getMetadata().getString(IMetadata.DC_COMMENT);
        if (this.description == null) {
            this.description = "This concept should really have a description associated.";
        }
    }
}
