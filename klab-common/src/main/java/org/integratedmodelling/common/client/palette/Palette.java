package org.integratedmodelling.common.client.palette;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.common.beans.Scenario;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.utils.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

public @Data class Palette {
    private String               name;
    private String               description;
    private String               icon;
    private String               version = "0.0.0";
    private long                 lastModified;
    private boolean              readOnly;
    private List<PaletteFolder>  folders       = new ArrayList<>();
    private List<String>         allowedGroups = new ArrayList<>();

    @JsonIgnore
    transient private File       workPath;
    @JsonIgnore
    transient private String     internalId;

    /**
     * Associated namespace for data imports and maybe models from components.
     */
    @JsonIgnore
    transient private INamespace namespace;

    /**
     * Associated scenario for roles and options.
     */
    @JsonIgnore
    transient private Scenario   scenario;

    /**
     * Call after construction to setup paths and IDs in all objects so that each element
     * can identify itself in the item tree.
     */
    void initialize() {
        this.internalId = NameGenerator.shortUUID();
        this.workPath = new File(KLAB.CONFIG.getDataPath("palettes") + File.separator
                + sanitizeName(this.name));
        this.workPath.mkdirs();
        for (PaletteFolder folder : folders) {
            folder.initialize(this, internalId);
        }
    }

    public INamespace getNamespace() {
        if (this.namespace == null) {
            String namespaceId = "user.toolkit." + sanitizeName(this.name);
            this.namespace = ((KIMModelManager) KLAB.MMANAGER).requireNamespace(namespaceId);
            /*
             * TODO enqueue task to collect all .k files in palette folder and
             */
        }
        return this.namespace;
    }

    private String sanitizeName(String text) {
        text = text.trim();
        text = StringUtils.replaceWhitespace(text, "_");
        text = StringUtils.lowerCase(text);
        return text.replaceAll("/", "_")
                .replaceAll("\\+", "_")
                .replaceAll("\\.", "_")
                .replaceAll(":", "_");
    }
}
