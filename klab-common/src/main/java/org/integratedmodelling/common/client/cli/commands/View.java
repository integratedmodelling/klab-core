/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.visualization.IViewer;
import org.integratedmodelling.api.modelling.visualization.IViewer.Display;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.runtime.ClientSession;
import org.integratedmodelling.common.client.viewer.ContextPath;
import org.integratedmodelling.exceptions.KlabException;

/**
 * @author ferdinando.villa
 *
 */
@CLIPrototype
@Prototype(
        id = "view",
        description = "start an observation in the current context",
        args = {
                "# path",
                Prototype.TEXT,
                "? n|new-viewer",
                Prototype.NONE,
                "? f|focus",
                Prototype.TEXT,
                "? cm|cycle-maps",
                Prototype.NONE,
                "? md|map-display",
                Prototype.NONE,
                "? gd|graph-display",
                Prototype.NONE,
                "? fd|flow-display",
                Prototype.NONE,
                "? td|timeline-display",
                Prototype.NONE,
                "? dd|data-display",
                Prototype.NONE,
        },
        argDescriptions = {
                "path to what to show (e.g. C1/S2 for the second state in context 1)",
                "open a new browser window",
                "make the observation the new focus in view (must specify a direct observation)",
                "cycle base map layer",
                "set display to geographical map",
                "set display to timeseries graph",
                "set display to flow diagram",
                "set display to timeline graph",
                "set display to data spreadsheet"
        })
public class View {

    /**
     * @param command
     * @return whatever we return, typically just text for display.
     * @throws KlabException
     */
    @Execute
    public Object view(IServiceCall command) throws KlabException {

        IViewer viewer = ((ClientSession) command.getSession()).getViewer();
        String ret = null;

        if (Environment.get().getContext() != null) {
            viewer.show(Environment.get().getContext());
        }

        if (command.has("cycle-maps")) {
            viewer.cycleView();
        }

        if (command.has("graph-display")) {
            viewer.setDisplay(Display.TIMEPLOT);
        }
        if (command.has("flow-display")) {
            viewer.setDisplay(Display.FLOWS);
        }
        if (command.has("timeline-display")) {
            viewer.setDisplay(Display.TIMELINE);
        }
        if (command.has("data-display")) {
            viewer.setDisplay(Display.DATAGRID);
        }
        if (command.has("map-display")) {
            viewer.setDisplay(Display.MAP);
        } else if (!command.has("path") && Environment.get().getContext() != null) {
            ret = ContextPath.dumpContextPath(Environment.get().getContext());
        }

        if (command.has("path")) {

            if (Environment.get().getContext() == null) {
                return "context is not set: path cannot be resolved";
            }

            String path = command.getString("path");
            ContextPath cp = new ContextPath(Environment.get().getContext(), path);

            for (IObservation observation : cp) {
                viewer.show(observation);
            }
        }
        return ret;
    }
}
