/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.data.IExport;
import org.integratedmodelling.api.data.IExport.Aggregation;
import org.integratedmodelling.api.data.IExport.Format;
import org.integratedmodelling.api.engine.IModelingEngine.Listener;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.auth.LicenseManager;
import org.integratedmodelling.common.auth.User;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.beans.ObservationData;
import org.integratedmodelling.common.beans.Project;
import org.integratedmodelling.common.beans.Session;
import org.integratedmodelling.common.beans.Space;
import org.integratedmodelling.common.beans.Task;
import org.integratedmodelling.common.beans.Time;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.beans.authority.AuthorityQueryResponse;
import org.integratedmodelling.common.beans.generic.Graph;
import org.integratedmodelling.common.beans.requests.DeployRequest;
import org.integratedmodelling.common.beans.requests.LocalExportRequest;
import org.integratedmodelling.common.beans.requests.ObservationRequest;
import org.integratedmodelling.common.beans.requests.SessionRequest;
import org.integratedmodelling.common.beans.requests.ViewerCommand;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.integratedmodelling.common.beans.responses.Capabilities;
import org.integratedmodelling.common.beans.responses.FileResource;
import org.integratedmodelling.common.beans.responses.LocalExportResponse;
import org.integratedmodelling.common.beans.responses.LockResponse;
import org.integratedmodelling.common.beans.responses.Observations;
import org.integratedmodelling.common.beans.responses.StateData;
import org.integratedmodelling.common.beans.responses.StateSummary;
import org.integratedmodelling.common.beans.responses.UserView;
import org.integratedmodelling.common.beans.responses.ValueSummary;
import org.integratedmodelling.common.client.EngineNotifier.EngineData;
import org.integratedmodelling.common.client.runtime.ClientSession;
import org.integratedmodelling.common.client.runtime.Provenance;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.knowledge.ExportableArtifact;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.model.runtime.AbstractContext;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRemoteException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

/**
 * Wraps the full REST API of a remote k.LAB server, talking back to the caller with the
 * model beans in {@link org.integratedmodelling.common.beans}.
 * 
 * @author ferdinando.villa
 */
public class EngineController {

    String                     url;
    IUser                      user;
    IServer                      node;
    ISession                   session;

    private RestTemplateHelper template;
    private RestTemplate       plainTemplate;
    private HttpHeaders        textHeaders;
    private HttpHeaders        jsonHeaders;
    private String             id;
    private List<Listener>     listeners = new ArrayList<>();
    private IMonitor           monitor;

    /**
     * Used by the modeling engine: sets in the same listeners as the engine and the
     * monitor.
     * 
     * @param ed
     * @param listeners
     * @param monitor
     */
    public EngineController(EngineData ed, List<Listener> listeners, IMonitor monitor) {
        this.url = ed.getUrl();
        this.textHeaders = new HttpHeaders();
        this.textHeaders.setContentType(MediaType.TEXT_PLAIN);
        this.jsonHeaders = new HttpHeaders();
        this.jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        this.listeners = listeners;
        this.monitor = monitor;
        this.template = RestTemplateHelper.newTemplate();
        this.plainTemplate = new RestTemplate(clientHttpRequestFactory());
    }

    /**
     * Start with just a URL and a name.
     * 
     * @param id
     * @param url
     */
    public EngineController(String id, String url) {
        this.url = url;
        this.id = id;
        this.textHeaders = new HttpHeaders();
        this.textHeaders.setContentType(MediaType.TEXT_PLAIN);
        this.jsonHeaders = new HttpHeaders();
        this.jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        this.template = RestTemplateHelper.newTemplate();
        this.plainTemplate = new RestTemplate(clientHttpRequestFactory());
    }

    /**
     * Initialize from the passed node, which will be used for authentication.
     * 
     * @param node
     */
    public EngineController(IServer node) {
        this.url = node.getUrl();
        this.id = node.getId();
        this.node = node;
        this.textHeaders = new HttpHeaders();
        this.textHeaders.setContentType(MediaType.TEXT_PLAIN);
        this.jsonHeaders = new HttpHeaders();
        this.jsonHeaders.setContentType(MediaType.APPLICATION_JSON);
        this.template = RestTemplateHelper.newTemplate();
        this.plainTemplate = new RestTemplate(clientHttpRequestFactory());
    }

    public static ClientHttpRequestFactory clientHttpRequestFactory() {

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        if (KLAB.CONFIG.getProperties().containsKey(IConfiguration.KLAB_CONNECTION_TIMEOUT)) {
            int connectTimeout = 1000 * Integer.parseInt(KLAB.CONFIG.getProperties()
                    .getProperty(IConfiguration.KLAB_CONNECTION_TIMEOUT));
            factory.setReadTimeout(connectTimeout);
            factory.setConnectTimeout(connectTimeout);
        }
        return factory;
    }

    /**
     * Authenticate user through certificate. This is done on the primary server, not the
     * engine.
     * 
     * @param certFileContents
     * @return an authenticated user
     */
    public IUser authenticate(String certFileContents) {
        try {
            AuthorizationResponse response = plainTemplate.postForObject(this.url
                    + API.AUTHENTICATION_CERT_FILE, certFileContents, AuthorizationResponse.class);
            return (this.user = new User(response));
        } catch (ResourceAccessException e) {
            processException(e);
        }
        return null;
    }

    /**
     * Connect to the network through the primary server using the certificate. Used by
     * model engines.
     * 
     * @param certFileContents
     * @return the user details, including the user's view of the network with access
     *         tokens for all nodes.
     */
    public UserView connect(String certFileContents) {
        try {
            UserView ret = plainTemplate
                    .postForObject(this.url + API.CONNECT, certFileContents, UserView.class);
            this.user = new User(ret.getUserProfile(), ret.getAuthToken());
            return ret;
        } catch (ResourceAccessException e) {
            processException(e);
        }
        return null;
    }

    /**
     * Return the user's view of the network at the time of calling. This is called by
     * different actors with different authentication, so we look for the one that carries
     * authentication.
     * 
     * @return the network
     */
    public Network getNetwork() {
        try {
            return template.with(session == null ? (node == null ? user : node) : session)
                    .get(url + API.GET_NETWORK, Network.class);
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    private String getEndpoint(String service) {
        return url + (service.startsWith("/") ? "" : "/") + service + ".json";
    }

    private String getEndpoint(String service, String suffix) {
        return url + (service.startsWith("/") ? "" : "/") + service + (suffix != null ? ("." + suffix) : "");
    }

    /**
     * ID of engine controller - node, engine or whatever we control.
     * 
     * @return the ID we're using
     */
    public String getId() {
        return id;
    }

    public Map<?, ?> getURNMetadata(String urn) {
        try {
            return template.with(user)
                    .get(getEndpoint(API.GET_RESOURCE_INFO).replace("{urn}", urn), Map.class);
        } catch (KlabRemoteException e) {
            processException(e);
        }
        return null;
    }

    @SuppressWarnings("javadoc")
    public URL getPublicKeyURL() {
        try {
            return new URL(url + "/get/file/" + API.CORE_PUBKEY_URN);
        } catch (MalformedURLException e) {
            // ok, will never ever happen, but be good
            throw new KlabRuntimeException(e);
        }
    }

    /**
     * Undeploy passed project at remote end.
     * 
     * @param project
     *            the project to undeploy
     */
    public void undeployProject(IProject project) {
        try {
            template.with(session).delete(getEndpoint(API.UNDEPLOY.replace("{id}", project.getId())));
        } catch (ResourceAccessException e) {
            processException(e);
        }
    }

    /**
     * Reload projects at remote end.
     * 
     * @param project
     *            the project to undeploy
     */
    public void reloadProjects(boolean forceReload) {
        try {
            template.with(session).postForObject(getEndpoint(API.RELOAD), null, String[].class, MapUtils
                    .of("forced", new Boolean(forceReload)));
        } catch (ResourceAccessException e) {
            processException(e);
        }
    }

    /**
     * Clear remote workspace.
     * 
     * @throws KlabException
     */
    public void clearWorkspace() throws KlabException {

        if (lockEngine()) {
            try {
                template.with(session).delete(getEndpoint(API.CLEAR_WORKSPACE));
            } catch (Exception e) {
                monitor.warn("could not lock engine to clear workspace");
                processException(e);
            }
            unlockEngine();
            monitor.info("engine locked and remote workspace cleared", Messages.INFOCLASS_LOCK);
        } else {
            monitor.warn("could not lock engine to clear workspace");
        }
    }

    /**
     * Deploy the passed project to the engine, replacing any previously deployed version
     * and overriding any version in the regular workspace.
     * 
     * @param project
     * @return a project bean describing the deployed project
     * @throws KlabException
     */
    public Project deployProject(IProject project) throws KlabException {

        Project ret = null;
        boolean isLocal = ((org.integratedmodelling.common.model.runtime.Session) session)
                .isLocalFilesystem();

        if (lockEngine()) {

            monitor.info("deploying project " + project.getId()
                    + (isLocal ? " from local filesystem" : "  to remote engine"), Messages.INFOCLASS_UPLOAD);

            if (isLocal) {

                DeployRequest request = new DeployRequest();
                request.setId(project.getId());
                request.setPath(project.getLoadPath().toString());
                request.setTimestamp(project.getLastModificationTime());
                ret = template.with(session)
                        .postForEntity(getEndpoint(API.DEPLOY_LOCAL), request, Project.class)
                        .getBody();

            } else {

                File archive = KLAB.PMANAGER.archiveProject(project.getId());
                LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();
                map.add("file", new FileSystemResource(archive));
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.MULTIPART_FORM_DATA);
                ResponseEntity<Project> result = null;
                try {
                    HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map, headers);
                    result = template.with(session).exchange(getEndpoint(API.DEPLOY.replace("{id}", project
                            .getId())), HttpMethod.POST, requestEntity, Project.class);
                    ret = result.getBody();

                } catch (Exception e) {
                    processException(e);
                }
            }
            unlockEngine();
            return ret;
        }

        monitor.error("could not lock engine: update aborted");
        return null;
    }

    /**
     * Open a session on the remote engine and return its data.
     * 
     * @param certificate
     * @param publicKey
     * @param needExclusive
     * @return a Session bean describing the opened session.
     */
    public Session openSession(File certificate, URL publicKey, boolean needExclusive) {

        try {
            String cert = FileUtils.readFileToString(certificate);
            Properties properties = LicenseManager.readCertificate(cert, publicKey);
            String clientSignature = KLAB.ENGINE instanceof KlabClient
                    ? ((KlabClient) KLAB.ENGINE).getClientSignature()
                    : null;
            KLAB.ENGINE.getMonitor().info("requesting session as user "
                    + properties.getProperty("user"), Messages.INFOCLASS_USER_OWN);
            return template.postForObject(getEndpoint(API.OPEN_SESSION), new SessionRequest(cert, properties
                    .getProperty("primary.server"), needExclusive, clientSignature), Session.class);
        } catch (ResourceAccessException e) {
            processException(e);
        } catch (Exception e) {
            throw new KlabRuntimeException(e);
        }
        return null;
    }

    /**
     * Retrieve the capabilities bean from the remote engine.
     * 
     * @return capabilities.
     */
    public Capabilities getCapabilities() {
        try {
            return template.get(getEndpoint(API.CAPABILITIES), Capabilities.class);
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    /**
     * Observe a direct object in a session. Establishes a new context.
     * 
     * @param directObserverName
     * @param session
     * @param scenarioIds
     * @param scaleForcings
     * @return the running observation task.
     */
    public ITask observe(String directObserverName, ISession session, Collection<String> scenarioIds, Collection<IExtent> scaleForcings) {

        ObservationRequest request = new ObservationRequest();
        request.defineObservable(directObserverName);
        if (scenarioIds != null) {
            request.getScenarios().addAll(scenarioIds);
        }
        if (scaleForcings != null) {
            for (IExtent ext : scaleForcings) {
                if (ext instanceof ISpatialExtent) {
                    request.setSpaceForcing(KLAB.MFACTORY.adapt(ext, Space.class));
                } else if (ext instanceof ITemporalExtent) {
                    request.setTimeForcing(KLAB.MFACTORY.adapt(ext, Time.class));
                }
            }
        }
        try {
            Task task = template.with(session)
                    .post(getEndpoint(API.OBSERVE), request, Task.class);
            return KLAB.MFACTORY.adapt(task, org.integratedmodelling.common.client.runtime.Task.class);
        } catch (ResourceAccessException e) {
            processException(e);
        }
        return null;
    }

    /**
     * Observe anything in a context.
     * 
     * @param observable
     * @param context
     * @param scenarioIds
     * @return the running task
     */
    public ITask observe(Object observable, IContext context, Collection<String> scenarioIds) {

        ObservationRequest request = new ObservationRequest();

        request.setContext(context.getId());
        request.defineObservable(observable);
        request.setFocusObservation(((Observation) context.getSubject()).getInternalId());
        request.setUserScenario(((AbstractContext) context).getScenario());

        if (scenarioIds != null) {
            request.getScenarios().addAll(scenarioIds);
        }
        try {
            Task task = template.with(context).post(getEndpoint(API.OBSERVE_IN_CONTEXT), request, Task.class);
            return KLAB.MFACTORY.adapt(task, org.integratedmodelling.common.client.runtime.Task.class);
        } catch (ResourceAccessException e) {
            processException(e);
        }
        return null;
    }

    /**
     * Run the passed context.
     * 
     * @param context
     * @return the running task.
     */
    public ITask run(IContext context) {
        try {
            Task task = template.with(context).get(getEndpoint(API.OBSERVE_RUN), Task.class);
            return KLAB.MFACTORY.adapt(task, org.integratedmodelling.common.client.runtime.Task.class);
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    /**
     * Send a command to the viewer. Not returning anything as this communication is
     * two-way but does not return answers.
     * 
     * @param command
     */
    public void sendViewerCommand(ViewerCommand command) {
        try {
            template.with(session).put(this.url + API.VIEW, command);
        } catch (ResourceAccessException e) {
            // just ignore in this case - happens when we disconnect and users
            // keep
            // clicking like monkeys before the map disappears
        }
    }

    private void processException(Exception e) {

        System.out.println("POFFARBACCO " + e.getMessage());

        /*
         * obtain the error from JSON
         */

        /*
         * send to monitor
         */
        if (monitor != null) {
            monitor.error(e);
        } else {
            KLAB.error(e);
        }
    }

    /**
     * Update the content of the passed namespace to the modeling engine.
     * 
     * @param ns
     */
    public void pushNamespace(String ns) {

        if (((org.integratedmodelling.common.model.runtime.Session) session).isLocalFilesystem()) {

            /*
             * local filesystem: just reload as needed
             */
            template.with(session).put(this.url + API.RELOAD, null, "force", Boolean.FALSE);

        } else {

            INamespace namespace = KLAB.MMANAGER.getNamespace(ns);
            File file = namespace.getLocalFile();
            try {
                if (file != null && file.isFile()) {
                    String nscontent;
                    try {
                        nscontent = FileUtils.readFileToString(file);
                    } catch (IOException e) {
                        throw new KlabRuntimeException(e);
                    }
                    if (lockEngine()) {
                        template.with(session)
                                .put(this.url + API.UPDATE_NAMESPACE
                                        .replace("{project}", namespace.getProject().getId())
                                        .replace("{id}", ns), nscontent);
                        unlockEngine();
                    } else {
                        monitor.error("could not lock engine: update aborted");
                    }
                }
            } catch (ResourceAccessException e) {
                processException(e);
            }
        }

    }

    public List<IObservationMetadata> importObservation(File file) {

        List<IObservationMetadata> ret = new ArrayList<>();

        if (((org.integratedmodelling.common.model.runtime.Session) session).isLocalFilesystem()) {

            monitor.info("importing observations from file " + file, Messages.INFOCLASS_UPLOAD);

            DeployRequest request = new DeployRequest();
            request.setPath(file.toString());

            try {
                Observations response = template.with(session)
                        .post(getEndpoint(API.IMPORT_OBSERVATIONS), request, Observations.class);
                for (ObservationData obs : response.getObservations()) {
                    ObservationMetadata om = new ObservationMetadata(obs);
                    ret.add(om);
                }
            } catch (Throwable e) {
                monitor.error("errors occurred during import: " + e.getMessage());

            }

        } else {
            monitor.error("observations can only be imported from external files on a local engine");
        }

        // List<IObservationMetadata> ret = new ArrayList<>();
        // LinkedMultiValueMap<String, Object> map = new
        // LinkedMultiValueMap<>();
        // map.add("file", new FileSystemResource(file));
        // HttpHeaders headers = new HttpHeaders();
        // headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        // ResponseEntity<Observations> result = null;
        // try {
        // HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new
        // HttpEntity<LinkedMultiValueMap<String, Object>>(
        // map, headers);
        // result =
        // template.with(session).exchange(getEndpoint(API.IMPORT_OBSERVATIONS),
        // HttpMethod.POST,
        // requestEntity, Observations.class);
        // for (ObservationMetadata om : result.getBody().getObservations()) {
        // ret.add(om);
        // }
        //
        // } catch (Exception e) {
        // processException(e);
        // }

        return ret;

    }

    /**
     * Attempt to lock the engine and, if requested, clear the remote workspace. Return
     * true if successful.
     * 
     * @return true if we have a lock
     */
    public boolean lockEngine() {
        try {
            LockResponse ret = template.with(session).get(this.url + API.LOCK_ENGINE, LockResponse.class);
            if (ret.isLocked()) {
                for (Listener listener : listeners) {
                    listener.engineLocked();
                }
            }
            return ret.isLocked();
        } catch (Exception e) {
            processException(e);
            return false;
        }
    }

    /**
     * Release a lock on the engine. Return true if we are OK.
     * 
     * @return true if lock was successfully released.
     */
    public boolean unlockEngine() {
        try {
            LockResponse ret = template.with(session).get(this.url + API.LOCK_ENGINE, LockResponse.class);
            if (!ret.isLocked()) {
                for (Listener listener : listeners) {
                    listener.engineUnlocked();
                }
            }
            return !ret.isLocked();
        } catch (Exception e) {
            processException(e);
            return false;
        }
    }

    /**
     * Remove the passed namespace from the modeling engine.
     * 
     * @param ns
     */
    public void deleteNamespace(String ns) {

        if (((org.integratedmodelling.common.model.runtime.Session) session).isLocalFilesystem()) {

            /*
             * local filesystem: just reload as needed
             */
            template.with(session).put(this.url + API.RELOAD, null, "force", Boolean.FALSE);

        } else {

            INamespace namespace = KLAB.MMANAGER.getNamespace(ns);
            if (namespace != null) {
                try {
                    /*
                     * TODO if session.isLocalFilesystem() change the call to avoid web
                     * transfer
                     */

                    template.with(session).delete(this.url + API.DELETE_NAMESPACE.replace("{id}", ns));
                } catch (ResourceAccessException e) {
                    processException(e);
                }
            }
        }
    }

    @SuppressWarnings("javadoc")
    public void setSession(ClientSession session) {
        this.session = session;
    }

    @SuppressWarnings("javadoc")
    public void setUser(IUser user) {
        this.user = user;
    }

    @SuppressWarnings("javadoc")
    public void setNode(IServer node) {
        this.node = node;
    }

    /**
     * Get the node we were given with setNode() or at initialization.
     * 
     * @return the node or null.
     */
    public IServer getNode() {
        return node;
    }

    /**
     * Query a remote authority for the concept encoding of a known identifier.
     * 
     * @param authorityId
     * @param conceptId
     * @return
     */
    public AuthorityConcept getAuthorityConcept(String authorityId, String conceptId) {
        try {
            return template.get(getEndpoint(API.RESOLVE_AUTHORITY
                    .replace("{authority}", authorityId)
                    .replace("{id}", URLEncoder.encode(conceptId, "UTF-8"))), AuthorityConcept.class);
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    /**
     * Store passed observation in kbox; return URN.
     * 
     * @param context
     * @param observation
     * @return
     */
    public String storeObservation(IContext context, IObservation observation) {
        try {
            FileResource ret = template.with(context).get(getEndpoint(API.CONTEXT_STORE_OBSERVATION)
                    .replace("{observation}", ((Observation) observation)
                            .getInternalId()), FileResource.class);
            return ret.getUrn();
        } catch (KlabRemoteException e) {
            processException(e);
        }
        return null;
    }

    /**
     * Query a remote authority for matches to a search string.
     * 
     * @param authorityId
     * @param query
     * @return
     */
    public AuthorityQueryResponse searchAuthority(String authorityId, String query) {
        try {
            return template.get(getEndpoint(API.QUERY_AUTHORITY
                    .replace("{authority}", authorityId)
                    .replace("{query}", URLEncoder.encode(query, "UTF-8"))), AuthorityQueryResponse.class);
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    /**
     * Get summary descriptors for the state such as histogram and colormap.
     * 
     * @param state
     * @param locators
     * @return
     */
    public StateSummary getStateSummary(IState state, Iterable<Locator> locators) {

        try {
            return template.with(((Observation) state).getContext()).get(getEndpoint(API.CONTEXT_GET_SUMMARY
                    .replace("{context}", ((Observation) state).getContext().getId())
                    .replace("{observation}", ((Observation) state)
                            .getInternalId())), StateSummary.class, MapUtils
                                    .of("index", Scale.locatorsAsText(locators)));
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    /**
     * Return all temporal data for a temporal state.
     * 
     * @param state
     * @return
     */
    public StateData getStateData(IState state) {

        try {
            return template.with(((Observation) state).getContext()).get(getEndpoint(API.CONTEXT_GET_VALUES
                    .replace("{context}", ((Observation) state).getContext().getId())
                    .replace("{observation}", ((Observation) state).getInternalId())), StateData.class);
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    /**
     * Return the provenance graph for a context as a visualizable graph bean.
     * 
     * @param state
     * @return
     */
    public Provenance getProvenance(IContext context) {

        try {
            Graph graph = template.with(context)
                    .get(getEndpoint(API.CONTEXT_GET_PROVENANCE)
                            .replace("{context}", context.getId()), Graph.class);
            return KLAB.MFACTORY.adapt(graph, Provenance.class);
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    /**
     * Get a descriptor for the value of the state at this offset.
     * 
     * @param state
     * @param idx
     * @return
     */
    public ValueSummary getValueSummary(IState state, long idx) {

        try {
            return template.with(((Observation) state).getContext()).get(getEndpoint(API.CONTEXT_GET_VALUE
                    .replace("{context}", ((Observation) state).getContext().getId())
                    .replace("{observation}", ((Observation) state)
                            .getInternalId())), ValueSummary.class, MapUtils.of("state-offset", idx));
        } catch (Exception e) {
            processException(e);
        }
        return null;
    }

    /**
     * If the artifact is a model where nothing is actually exported until merged into a
     * project, pass null as output file.
     * 
     * @param context
     * @param artifact
     * @param output
     * @return
     */
    public LocalExportResponse exportArtifact(IContext context, ExportableArtifact artifact, File output) {

        LocalExportRequest request = new LocalExportRequest();
        request.setObservationId("A|" + artifact.getName() + "|"
                + ((Observation) artifact.getContextObservation()).getInternalId() + "|"
                + (artifact.isModel() ? "model" : "dataset"));

        LocalExportResponse ret = template.with(context)
                .post(getEndpoint(API.CONTEXT_EXPORT_DATA_LOCAL), request, LocalExportResponse.class);

        if (ret == null) {
            monitor.error("no response for export request");
            return null;
        }

        if (ret.getFiles().size() > 0) {
            monitor.info("export to " + ret.getFiles().get(0) + " complete", Messages.INFOCLASS_DOWNLOAD);
        } else {
            monitor.warn("export: no files returned");
        }

        return ret;
    }

    /**
     * Ask local engine to open an editor on the passed exportable artifact.
     * 
     * @param context
     * @param artifact
     */
    public void editArtifact(IContext context, ExportableArtifact artifact) {

        LocalExportRequest request = new LocalExportRequest();
        request.setObservationId("A|" + artifact.getName() + "|"
                + ((Observation) artifact.getContextObservation()).getInternalId() + "|"
                + (artifact.isModel() ? "model" : "dataset"));

        /*
         * just ask and return.
         */
        template.with(context)
                .post(getEndpoint(API.CONTEXT_EDIT_LOCAL), request, LocalExportResponse.class);
    }

    public void abortTask(ITask task) {
        try {
            // FIXME uses task ID in URL, should not be necessary. Also should be a DELETE
            // endpoint.
            template.with(task)
                    .get(getEndpoint(API.CONTEXT_ABORT_TASK).replace("{context}", task.getContext().getId())
                            .replace("{task}", task.getTaskId()), Task.class);
        } catch (KlabRemoteException e) {
            processException(e);
        }
    }

    /**
     * Export the passed observation path to the passed file (or directory) copying data
     * from the engine.
     * 
     * @param path
     * @param file
     * @param options
     *            may contain aggregation and format specifiers as well as a collection of
     *            scale locators.
     */
    public void export(IContext context, String path, File file, Object[] options) {

        boolean isLocal = ((org.integratedmodelling.common.model.runtime.Session) session)
                .isLocalFilesystem();

        IExport.Aggregation aggregation = IExport.Aggregation.AVERAGE;
        IExport.Format format = IExport.Format.SCIENTIFIC_DATASET;
        String locators = null;

        List<IScale.Locator> locs = new ArrayList<>();

        for (Object o : options) {
            if (o instanceof IExport.Format) {
                format = (Format) o;
            } else if (o instanceof IExport.Aggregation) {
                aggregation = (Aggregation) o;
            } else if (o instanceof Collection) {
                for (Object oo : ((Collection<?>) o)) {
                    if (o instanceof IScale.Locator) {
                        locs.add((Locator) oo);
                    }
                }
            } else if (o instanceof IScale.Locator) {
                locs.add((Locator) o);
            } else if (o == null) {
                locs.add(ITransition.INITIALIZATION);
            }
        }

        try {

            if (isLocal) {

                if (locs.size() > 0) {
                    locators = Scale.locatorsAsText(locs);
                }

                LocalExportRequest request = new LocalExportRequest();
                request.setObservationId(path);
                request.setOutputFile(file);
                request.setAggregation(aggregation);
                request.setFormat(format);
                request.setLocators(locators);

                LocalExportResponse ret = template.with(context)
                        .post(getEndpoint(API.CONTEXT_EXPORT_DATA_LOCAL), request, LocalExportResponse.class);

                if (ret.getFiles().size() > 0) {
                    monitor.info("export to " + ret.getFiles().get(0)
                            + " complete", Messages.INFOCLASS_DOWNLOAD);
                } else {
                    monitor.warn("export: no files returned");
                }
            } else {

                template.with(context).getDownload(getEndpoint(API.CONTEXT_GET_DATA, "tif")
                        .replace("{context}", context.getId()).replace("{observation}", path), file);
            }
        } catch (KlabRemoteException e) {
            processException(e);
        }

    }

    public String getContextReport(IContext context) {

        String ret = "";
        try (InputStream in = new URL(getEndpoint(API.CONTEXT_GET_REPORT, "html")
                .replace("{context}", context.getId())).openStream()) {
            ret = IOUtils.toString(in);
        } catch (IOException e) {
            processException(e);
        }

        return ret;
    }

}
