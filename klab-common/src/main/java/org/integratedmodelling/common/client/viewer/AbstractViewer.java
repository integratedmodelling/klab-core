package org.integratedmodelling.common.client.viewer;

import org.integratedmodelling.api.modelling.visualization.IViewer;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.common.beans.requests.ViewerNotification;
import org.integratedmodelling.common.client.runtime.ClientSession;

/**
 * 
 * @author ferdinando.villa
 *
 */
public abstract class AbstractViewer implements IViewer {

    protected ClientSession session;

    @Override
    public ISession getSession() {
        return session;
    }

    public void acceptNotification(ViewerNotification notification) {
    }

}
