package org.integratedmodelling.common.client.runtime;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.common.beans.generic.Graph;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.jgrapht.graph.DefaultDirectedGraph;

public class Provenance extends DefaultDirectedGraph<IProvenance.Node, IProvenance.Action>
		implements IProvenance, NetworkDeserializable {

	private static final long serialVersionUID = 6120910715977831258L;

	class Node implements IProvenance.Node {

		String id;
		String label;
		String type;
		IMetadata metadata;

		Node(String id, String label, String type, IMetadata md) {
			this.id = id;
			this.label = label;
			this.metadata = md;
			this.type = type;
		}

		@Override
		public int hashCode() {
			return id.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return obj instanceof Node && ((Node) obj).id.equals(id);
		}

        @Override
        public IMetadata getMetadata() {
            return metadata;
        }

        @Override
        public String getName() {
            return id;
        }

        @Override
        public IObservation getObservation() {
            // TODO Auto-generated method stub
            return null;
        }
	}

	class Action implements IProvenance.Action {

		String sId;
		String tId;
		String id;
		String label;
		IMetadata metadata;

		Action(String id, String sId, String tId, String label, IMetadata metadata) {
			this.id = id;
			this.sId = sId;
			this.tId = tId;
			this.label = label.startsWith("_") ? "" : label;
			this.metadata = metadata;
		}

        @Override
        public IMetadata getMetadata() {
            return metadata;
        }

        @Override
        public org.integratedmodelling.api.provenance.IProvenance.Action getCause() {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Agent getAgent() {
            // TODO Auto-generated method stub
            return null;
        }

	}

	Map<String, Node> nodes = new HashMap<>();

	public Provenance() {
		super(IProvenance.Action.class);
	}

	public GraphVisualization visualize() {

		GraphVisualization ret = new GraphVisualization();

		ret.adapt(this, new GraphVisualization.GraphAdapter<IProvenance.Node, IProvenance.Action>() {

			@Override
			public String getNodeType(IProvenance.Node o) {
				return ((Node)o).type;
			}

			@Override
			public String getNodeId(IProvenance.Node o) {
				return ((Node) o).id;
			}

			@Override
			public String getNodeLabel(IProvenance.Node o) {
				return ((Node) o).label;
			}

			@Override
			public String getNodeDescription(IProvenance.Node o) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getEdgeType(IProvenance.Action o) {
				return "datapath";
			}

			@Override
			public String getEdgeId(IProvenance.Action o) {
				return ((Action) o).id;
			}

			@Override
			public String getEdgeLabel(IProvenance.Action o) {
				return ((Action) o).label;
			}

			@Override
			public String getEdgeDescription(IProvenance.Action o) {
				return null;
			}
		});

		return ret;
	}

	@Override
	public void deserialize(IModelBean object) {

		if (!(object instanceof Graph)) {
			throw new KlabRuntimeException(
					"cannot deserialize an Dataflow from a " + object.getClass().getCanonicalName());
		}
		Graph bean = (Graph) object;

		for (String v : bean.getNodes()) {
			String[] s = v.split("\\|");
			;
			Node step = new Node(s[0], s[1], s[2], KLAB.MFACTORY.adapt(bean.getMetadata().get(s[0]), Metadata.class));
			nodes.put(s[0], step);
			addVertex(step);
		}

		for (String v : bean.getRelationships()) {
			String[] s = v.split("\\|");
			Action path = new Action(s[0], s[1], s[2], (s.length > 3 ? s[3] : ""),
					KLAB.MFACTORY.adapt(bean.getMetadata().get(s[0]), Metadata.class));
			Node source = nodes.get(s[1]);
			Node target = nodes.get(s[2]);
			addEdge(source, target, path);
		}
	}


    @Override
    public boolean isEmpty() {
        return vertexSet().size() == 0;
    }

    @Override
    public IMetadata collectMetadata(Object node) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<IProvenance.Action> getPrimaryActions() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IProvenance.Action add(IProvenance.Node actor,IProvenance.Action action, IProvenance.Node result) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IProvenance.Action add(IProvenance.Node actor, IProvenance.Action action, IProvenance.Node result, IProvenance.Action cause) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Artifact getRootArtifact() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<Artifact> getArtifacts() {
        List<Artifact> ret = new ArrayList<>();
        for (org.integratedmodelling.api.provenance.IProvenance.Node n : this.vertexSet()) {
            if (n instanceof Artifact) {
                ret.add((Artifact)n);
            }
        }
        return ret;
    }

}
