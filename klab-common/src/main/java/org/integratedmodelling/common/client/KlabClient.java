/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.configuration.IResourceConfiguration.StaticResource;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.runtime.IWorkspace;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.Service;
import org.integratedmodelling.common.beans.authority.Authority;
import org.integratedmodelling.common.beans.responses.Capabilities;
import org.integratedmodelling.common.command.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Notifiable;
import org.integratedmodelling.common.network.ResourceConfiguration;
import org.integratedmodelling.common.project.Workspace;
import org.integratedmodelling.common.vocabulary.authority.AuthorityFactory;
import org.integratedmodelling.exceptions.KlabException;
import org.jgroups.util.UUID;

/**
 * Client implementation for k.LAB 1.0. It's nothing but an IEngine; the IClient class
 * makes no sense. Booting this should be preceded by a certificate check (the certificate
 * file is passed to the constructor). Upon construction, it will stay idle until an
 * engine is available. Appropriate GUIs should intercept engine discovery and give a
 * chance to choose one. By default, it will automatically connect to an engine running on
 * localhost.
 *
 * @author ferdinando.villa
 */
public abstract class KlabClient extends Notifiable implements IEngine {

    protected boolean                     needExclusive;
    protected File                        certificate;
    protected IMonitor                    monitor;
    protected long                        bootTime;
    protected IResourceConfiguration      resourceConfiguration = new ResourceConfiguration();
    protected HashMap<String, IPrototype> prototypes            = new HashMap<>();
    protected INetwork                    network;
    protected String                      primaryServerUrl;
    protected String                      signature;
    protected boolean                     interactiveMode;

    public KlabClient() {
    }

    /**
     * @param certificate
     * @throws KlabException
     */
    public KlabClient(File certificate) throws KlabException {
        this(certificate, new ClientMonitor());
    }

    @Override
    public IMonitor getMonitor() {
        return monitor;
    }

    /**
     * The constructor just sticks out an antenna and starts listening for engines on the
     * local network.
     * 
     * @param certificate
     * @param monitor
     * @throws KlabException
     */
    public KlabClient(File certificate, IMonitor monitor) throws KlabException {
        this.monitor = monitor;
        this.certificate = certificate;
        createSignature();
        addMonitor(monitor);
    }
    

//    @Override
    public String getSecurityKey() {
        // TODO Auto-generated method stub
        return null;
    }

//    @Override
    public <T extends IIdentity> T getParentIdentity(Class<? extends IIdentity> type) {
        // TODO Auto-generated method stub
        return null;
    }

    /*
     * create signature for engine to check if it's on the same filesystem.
     */
    private void createSignature() {
        File sigFile = new File(KLAB.CONFIG.getDataPath("engine") + File.separator + ".clientsig");
        if (!sigFile.exists()) {
            this.signature = UUID.randomUUID().toString();
            try {
                FileUtils.writeStringToFile(sigFile, this.signature);
            } catch (IOException e) {
                // ignore; engine will work in remote mode.grazie
            }
        } else {
            try {
                this.signature = FileUtils.readFileToString(sigFile);
            } catch (IOException e) {
                /*
                 * ok, have it the hard way.
                 */
                this.signature = UUID.randomUUID().toString();
            }
        }
    }

    /**
     * Redefine to redefine how the workspace is managed. Regardless of the location, we
     * put the stuff we deploy automatically under deploy/ in it; if we have the same
     * projects in the workspace, these take over. Default workspace will be in
     * ${data-dir}/workspace.
     * 
     * @return
     */
    protected IWorkspace createWorkspace() {
        return new Workspace(KLAB.CONFIG.getDataPath("workspace"), true);
    }

    @Override
    public String submitObservation(IDirectObserver observer, boolean store) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<IObservationMetadata> queryObservations(String text, boolean localOnly) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IDirectObserver retrieveObservation(String observationid, String nodeId) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<IModelMetadata> queryModels(IObservableSemantics observable, IResolutionScope context)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITask setupComponent(String componentId) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Return a unique signature for the running client, also written in
     * engine/.clientsig.
     * 
     * @return
     */
    public String getClientSignature() {
        return signature;
    }

    @Override
    public void removeObservations(Collection<String> observationNames) throws KlabException {
        // TODO Auto-generated method stub

    }

    @Override
    public long getBootTime() {
        return bootTime;
    }

    @Override
    public void shutdown(int delaySeconds) {
        // TODO Auto-generated method stub
    }

    @Override
    public boolean provides(Object id) {
        if (id instanceof String) {
            return resourceConfiguration.getSynchronizedProjectIds().contains(id)
                    || resourceConfiguration.getComponentIds().contains(id);
        }
        if (id instanceof StaticResource) {
            return resourceConfiguration.getStaticResources().contains(id);
        }
        return false;
    }

    @Override
    public IResourceConfiguration getResourceConfiguration() {
        return resourceConfiguration;
    }

    protected void getPrototypesFromCapabilities(Capabilities capabilities) {

        prototypes.clear();
        for (Service pr : capabilities.getFunctions()) {
            IPrototype prototype = KLAB.MFACTORY.adapt(pr, Prototype.class);
            prototypes.put(prototype.getId(), prototype);
        }
    }

    protected void getAuthoritiesFromCapabilities(Capabilities capabilities) {

        for (Authority pr : capabilities.getAuthorities()) {
            AuthorityFactory.get().addRemoteAuthority(pr);
        }
    }

    @Override
    public INetwork getNetwork() {
        return network;
    }

    public String getPrimaryServerUrl() {
        return primaryServerUrl;
    }

    @Override
    public List<Pair<Annotation, Class<?>>> scanPackage(String packageId) throws KlabException {
        return new ArrayList<>();
    }

    public void setInteractiveMode(boolean mode) {
        this.interactiveMode = mode;
    }

    public boolean isInteractive() {
        return this.interactiveMode;
    }

}
