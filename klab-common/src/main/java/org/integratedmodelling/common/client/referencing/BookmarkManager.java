/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.referencing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.ui.IBookmark;
import org.integratedmodelling.api.ui.IBookmarkManager;
import org.integratedmodelling.common.client.palette.PaletteItem;
import org.integratedmodelling.common.client.palette.PaletteManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;

/**
 * We maintain bookmarks organized by class. Within each class, bookmarks are arranged in
 * folders. Both bookmarks and folders are instances of class Bookmark. Subclasses
 * implement tests, tasks etc.
 *
 * @author Ferd
 *
 */
public class BookmarkManager implements IBookmarkManager {

    private static BookmarkManager     _this;

    public static final String         CLASS_CHECK         = "|issue|todo|fixme|task|";
    public static final String         CLASS_TEST          = "test";
    public static final String         CLASS_BOOKMARK      = "bookmark";

    public static final String         PARAMETER_WHO       = "who";
    public static final String         PARAMETER_GROUP     = "group";

    public Map<String, List<Bookmark>> _bookmarks          = new HashMap<String, List<Bookmark>>();

    public Map<String, BookmarkGraph>  _graphs             = new HashMap<String, BookmarkGraph>();

    /*
     * read from disk at beginning, tracking any new bookmarks set by user afterwards.
     */
    private List<Bookmark>             _permanentBookmarks = new ArrayList<Bookmark>();
    private Set<String>                _permanentIndex     = new HashSet<>();

    // public static BookmarkManager get() {
    // if (_this == null) {
    // _this = new BookmarkManager();
    // }
    // return _this;
    // }

    public BookmarkManager() {
    }

    /**
     * Reset the bookmarks for a given namespace. Also removes the stored bookmark graph
     * for each class touched.
     *
     * Use sequentially for all touched namespaces before calling getBookmarks() again, so
     * that we only create the graph once.
     *
     * @param ns
     */
    public synchronized void removeAllForNamespace(String ns) {

        for (String bclass : _bookmarks.keySet()) {
            List<Bookmark> lb = _bookmarks.get(bclass);
            ArrayList<Bookmark> ai = new ArrayList<Bookmark>();
            for (int i = 0; i < lb.size(); i++) {
                if (!lb.get(i).namespace.equals(ns)) {
                    ai.add(lb.get(i));
                }
            }
            _bookmarks.put(bclass, ai);
            if (ai.size() > 0) {
                _graphs.remove(bclass);
            }
        }
    }

    public Bookmark getBookmark(String name, String bclass) {

        Bookmark ret = null;
        List<Bookmark> bb = _bookmarks.get(bclass);
        if (bb != null) {
            for (Bookmark b : bb) {
                if (b.id.equals(name)) {
                    ret = b;
                    break;
                }
            }
        }
        return ret;
    }

    /**
     * Return the bookmark hierarchy for the given class, recreating it if anything has
     * changed.
     *
     * @param bclass
     * @return the bookmark graph for this class
     */
    public BookmarkGraph getBookmarks(String bclass) {

        BookmarkGraph ret = null; // _graphs.get(bclass);
        if (ret == null) {
            ret = makeHierarchy(getBookmarkList(bclass));
            _graphs.put(bclass, ret);
        }
        return ret;
    }

    public List<Bookmark> getRootBookmarks(String bclass) {
        List<Bookmark> ret = _bookmarks.get(bclass);
        if (bclass.equals(CLASS_BOOKMARK) && _permanentBookmarks != null) {
            if (ret == null) {
                ret = new ArrayList<Bookmark>();
            }
            ret.addAll(_permanentBookmarks);
        }
        return ret;
    }

    public List<Bookmark> getBookmarkList(String bclass) {
        List<Bookmark> ret = _bookmarks.get(bclass);
        if (bclass.equals(CLASS_BOOKMARK) && _permanentBookmarks != null) {
            if (ret == null) {
                ret = new ArrayList<Bookmark>();
            }
            ret.addAll(_permanentBookmarks);
        }
        return ret;
    }

    private BookmarkGraph makeHierarchy(List<Bookmark> list) {

        BookmarkGraph ret = new BookmarkGraph();

        if (list == null) {
            return ret;
        }
        /*
         * 1. make hash of all available IDs
         */
        HashMap<String, Bookmark> hash = new HashMap<String, Bookmark>();
        for (Bookmark b : list) {
            hash.put(b.id, b);
        }

        /*
         * 2. create all the non-existing intermediate "folder" bookmarks
         */
        for (Bookmark b : list) {
            String path = "";
            for (String s : b.inheritance) {
                path += (path.isEmpty() ? "" : "/") + s;
                Bookmark bparent = hash.get(path);
                if (bparent == null) {
                    bparent = new Bookmark(path, b.namespace);
                    hash.put(bparent.id, bparent);
                }
                ret.addVertex(bparent);
            }
        }

        /*
         * 3. we have them all, link'em up
         */
        for (Bookmark b : hash.values()) {

            // link every child to its parent
            for (int i = 1; i < b.inheritance.length; i++) {
                String cid = StringUtils.join(Arrays.copyOfRange(b.inheritance, 0, i + 1), "/");
                String pid = StringUtils.join(Arrays.copyOfRange(b.inheritance, 0, i), "/");
                ret.addEdge(hash.get(cid), hash.get(pid));
            }
        }

        return ret;
    }

    public void processAnnotations(IModelObject object, IUser user) {

        for (IAnnotation a : object.getAnnotations()) {

            if (!belongsTo(a, user)) {
                continue;
            }

            if (CLASS_CHECK.contains(a.getId())) {
                add(new Bookmark(object, a), CLASS_CHECK);
            } else if (a.getId().equals(CLASS_TEST)) {
                add(new Bookmark(object, a), CLASS_TEST);
            } else if (a.getId().equals(CLASS_BOOKMARK)) {
                add(new Bookmark(object, a), CLASS_BOOKMARK);
            }
        }
    }

    private boolean belongsTo(IAnnotation a, IUser user) {

        if (a.getParameters().get(PARAMETER_GROUP) == null && a.getParameters().get(PARAMETER_WHO) == null) {
            return true;
        }

        String who = Bookmark.check(a.getParameters().get(PARAMETER_WHO));
        String grp = Bookmark.check(a.getParameters().get(PARAMETER_GROUP));

        boolean whoOK = who == null;
        boolean grpOK = grp == null;

        if (who != null && user != null) {
            whoOK = who.contains(user.getUsername());
        }

        if (grp != null) {
            for (String g : user.getGroups()) {
                if (grp.contains(g)) {
                    grpOK = true;
                    break;
                }
            }
        }

        return whoOK && grpOK;
    }

    private void add(Bookmark bookmark, String bclass) {

        List<Bookmark> lb = _bookmarks.get(bclass);
        if (lb == null) {
            lb = new ArrayList<Bookmark>();
            _bookmarks.put(bclass, lb);
        }
        lb.add(bookmark);
    }

    @Override
    public void addPersistentBookmark(IBookmark bookmark) {
        _permanentBookmarks.add((Bookmark) bookmark);
        _permanentIndex.add(((Bookmark) bookmark).getId());
        _graphs.remove(CLASS_BOOKMARK);
        try {
            persistBookmarks();
        } catch (KlabIOException e) {
            throw new KlabRuntimeException(e);
        }
        /*
         * TODO Listener
         */
    }

    private void persistBookmarks() throws KlabIOException {

        File f = new File(KLAB.CONFIG.getDataPath() + File.separator + "bookmarks.xml");
        XStream xstream = new XStream(new StaxDriver());
        String xml = xstream.toXML(_permanentBookmarks);
        try {
            FileUtils.writeStringToFile(f, xml);
        } catch (IOException e) {
            throw new KlabIOException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public void restoreBookmarks() throws KlabIOException {

        try {
            File xfile = new File(KLAB.CONFIG.getDataPath() + File.separator + "bookmarks.xml");
            if (xfile.exists()) {
                XStream xstream = new XStream(new StaxDriver());
                BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(xfile), "UTF-8"));
                _permanentBookmarks = (List<Bookmark>) xstream.fromXML(input);
                for (Bookmark b : _permanentBookmarks) {
                    _permanentIndex.add(b.id);
                }
                input.close();
            }
        } catch (Exception e) {
            throw new KlabIOException(e);
        }

    }

    @Override
    public void deleteBookmark(IBookmark bookmark) throws KlabIOException {
        if (isPermanent(bookmark)) {
            boolean gotIt = false;
            for (Iterator<Bookmark> it = _permanentBookmarks.iterator(); it.hasNext();) {
                if (it.next().id.equals(((Bookmark) bookmark).id)) {
                    it.remove();
                    _permanentIndex.remove(((Bookmark) bookmark).id);
                    gotIt = true;
                }
            }
            if (gotIt) {
                // this is a tiny bit complicated, to put it gently.
                List<Bookmark> zio = _bookmarks.get(CLASS_BOOKMARK);
                for (Iterator<Bookmark> it = zio.iterator(); it.hasNext();) {
                    if (it.next().id.equals(((Bookmark) bookmark).id)) {
                        it.remove();
                    }
                }
                _graphs.remove(CLASS_BOOKMARK);
                persistBookmarks();
            }
        }
    }

    @Override
    public boolean isPermanent(IBookmark bookmark) {
        return _permanentIndex.contains(((Bookmark) bookmark).id);
    }

    @Override
    public IBookmark bookmark(IModelObject modelObject, String name, String description) {
        Bookmark bookmark = new Bookmark(modelObject, name, description);
        addPersistentBookmark(bookmark);
        return bookmark;
    }

    /**
     * Pass a string signature from a bookmark or palette item, and return the result as a
     * concept if it is a concept, or null if not.
     * 
     * @param toDrop
     * @return
     */
    public static IConcept getConcept(String toDrop) {
        IConcept ret = null;
        if (toDrop.startsWith("@BM|")) {
            ret = KLAB.KM.getConcept(toDrop.substring(4));
        } else if (toDrop.startsWith("@PI|")) {
            PaletteItem item = PaletteManager.get().getItem(toDrop.substring(4));
            if (item != null) {
                ret = item.getConcept();
                if (ret == null) {
                    IModelObject mo = KLAB.MMANAGER.findModelObject(item.getModelObject());
                    if (mo instanceof IKnowledgeObject) {
                        ret = ((IKnowledgeObject) mo).getConcept();
                    }
                    if (ret == null) {
                        ret = KLAB.KM.getConcept(item.getModelObject());
                    }
                }
            }
        }
        return ret;
    }

}
