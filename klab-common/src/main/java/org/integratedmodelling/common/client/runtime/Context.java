package org.integratedmodelling.common.client.runtime;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IReport;
import org.integratedmodelling.api.modelling.IEventBus;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.model.runtime.AbstractContext;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Client version of a context.
 * 
 * @author Ferd
 *
 */
public class Context extends AbstractContext implements NetworkDeserializable {

    String              description;
    IMonitor            monitor;
    List<String>        scenarios = new ArrayList<>();
    private ITransition lastTransition;

    public Context() {
    }

    protected Context(Context context) {
        super(context);
        this.monitor = context.monitor;
        this.scenarios.addAll(context.scenarios);
    }

    @Override
    public ISession getSession() {
        return monitor.getSession();
    }

    @Override
    public String toString() {
        return "[C "
                + (getSubject() == null ? "(empty)" : getSubject())
                + (getSubject() == null ? "" : (" O(" + getSubject().getSubjects().size() + ")"))
                + (getSubject() == null ? "" : (" E(" + getSubject().getEvents().size() + ")"))
                + (getSubject() == null ? "" : (" P(" + getSubject().getProcesses().size() + ")"))
                + (getSubject() == null ? "" : (" S(" + getSubject().getStates().size() + ")"))
                + (getSubject() == null ? ""
                        : (" R(" + getSubject().getStructure().getRelationships().size() + ")"))
                + "] ";
    }

    @Override
    public IContext inScenario(String... scenarios) {
        if (scenarios == null || scenarios.length == 0) {
            return this;
        }

        Context ret = new Context(this);
        ret.scenarios.clear();
        for (String s : scenarios) {
            ret.scenarios.add(s);
        }
        return ret;
    }

    @Override
    public void persist(File file, String path, Object... options) throws KlabException {
    	((ClientSession)getSession()).getEngineController().export(this, path, file, options);
    }

    @SuppressWarnings("javadoc")
    public void setMonitor(IMonitor monitor) {
        this.monitor = ((Monitor) monitor).get(this);
        this.addMonitor(this.monitor);
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Context)) {
            throw new KlabRuntimeException("cannot deserialize a Context from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Context bean = (org.integratedmodelling.common.beans.Context) object;
        
        this.isFinished = bean.isFinished();
        this.id = bean.getId();
        this.description = bean.getDescription();
    }

    public IMonitor getMonitor() {
        return monitor;
    }

    @Override
    public IContext focus(ISubject observation) {

        if (subject.equals(observation)) {
            return this;
        }

        Context ret = new Context(this);
        ret.subject = observation;
        return ret;
    }

    @Override
    public ITask run() throws KlabException {
        return ((ClientSession) getSession()).getEngineController().run(this);

    }

    @Override
    public ITask observe(Object observable) throws KlabException {
        newObservations.clear();
        return ((ClientSession) getSession()).getEngineController().observe(Observables.getObservableObject(observable, getSubject()), this, scenarios);
    }

    public void setLastTransition(ITransition transition) {
        this.lastTransition = transition;
    }

    @Override
    public ITransition getLastTemporalTransition() {
        return this.lastTransition;
    }

    @Override
    public IProvenance getProvenance() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IObservation> getNewObservations() {
        Collection<IObservation> ret = new HashSet<>(newObservations);
        newObservations.clear();
        return ret;
    }

    @Override
    public IResolutionScope getScope() {
        // TODO Auto-generated method stub
		return null;
	}

    @Override
    public IReport getReport() {
        // TODO transfer report from engine at each call.
        return null;
    }

    @Override
    public IContext rescale(Collection<IExtent> extents) {
        return this;
    }

    @Override
    public IEventBus getEventBus() {
        // TODO Auto-generated method stub
        return null;
    }

}
