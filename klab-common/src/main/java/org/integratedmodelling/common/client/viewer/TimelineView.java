package org.integratedmodelling.common.client.viewer;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IEvent;

/**
 * Contains time data plus event timelines
 * @author ferdinando.villa
 *
 */
class TimelineView extends TimeView {
    List<IEvent> events = new ArrayList<>();

    @Override
    public void clear() {
        super.clear();
        events.clear();
    }
}