package org.integratedmodelling.common.client;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.configuration.IResourceConfiguration.StaticResource;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.auth.LicenseManager;
import org.integratedmodelling.common.beans.Node;
import org.integratedmodelling.common.beans.requests.ModelQuery;
import org.integratedmodelling.common.client.palette.Palette;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.knowledge.Definition;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.common.utils.NetUtilities;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * The client that represents a network node.
 * 
 * @author ferdinando.villa
 *
 */
public class NodeClient extends KlabClient implements IServer, NetworkSerializable, NetworkDeserializable {

    /*
     * two minutes of inactivity before we quarantine a node
     */
    private static final long MAX_INACTIVE_TIME = 2 * 60 * 1000;

    String           url;
    EngineController engine;
    String           id;
    String           key;
    Version          version;
    String           build;
    
    List<Palette> palettes = new ArrayList<>();

    private long           lastTimeAlive = 0;
    private Authentication authentication;

    /*
     * for the serializer
     */
    @SuppressWarnings("javadoc")
    public NodeClient() {

    }

    @Override
    public List<IObservationMetadata> importObservations(File file) throws KlabException {
        if (engine != null) {
            return engine.importObservation(file);
        }
        return null;
    }

    /**
     * read a server certificate
     * 
     * @param certificate
     * @throws KlabException
     */
    public NodeClient(File certificate) throws KlabException {
        super(certificate);
        initialize();
    }

    private void initialize() throws KlabException {

        File publicKey = new File(KLAB.CONFIG.getDataPath() + File.separator + "ssh" + File.separator
                + "pubring.gpg");

        Properties identity = null;
        try {
            identity = LicenseManager
                    .readCertificate(certificate, publicKey, new String[] { "url", "name", "key" });

            this.id = identity.getProperty("name");
            this.url = identity.getProperty("url");
            this.key = identity.getProperty("key");

            /*
             * TODO check expiration
             */

            this.engine = new EngineController(this);

        } catch (Exception e) {
            throw new KlabAuthorizationException(e);
        }
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public Authentication getAuthentication() {
        return authentication;
    }

    @Override
    public boolean isActive() {
        /*
         * we assume that a node that was never checked is alive. This is a stretch, but
         * allows to propagate a full network and check again at the next chance.
         */
        return lastTimeAlive == 0 || (System.currentTimeMillis() - lastTimeAlive) < MAX_INACTIVE_TIME;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getDescription() {
        // TODO Auto-generated method stub
        return null;
    }
    //
    // public <T> T call(IServiceCall call, Class<? extends T> desiredClass) {
    // // TODO Auto-generated method stub
    // return null;
    // }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public IPrototype getFunctionPrototype(String id) {
        return prototypes.get(id);
    }

    @Override
    public Collection<IPrototype> getFunctionPrototypes() {
        return prototypes.values();
    }

    @Override
    public String getName() {
        return id;
    }

    @Override
    public boolean isRunning() {
        /*
         * we remove non-responsive nodes, so if it's here it's running.
         */
        return true;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#hashCode()
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((url == null) ? 0 : url.hashCode());
        return result;
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        NodeClient other = (NodeClient) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (url == null) {
            if (other.url != null)
                return false;
        } else if (!url.equals(other.url))
            return false;
        return true;
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Node)) {
            throw new KlabRuntimeException("cannot deserialize a INode from a "
                    + object.getClass().getCanonicalName());
        }

        Node node = (Node) object;

        this.url = node.getUrl();
        this.id = node.getId();
        this.key = node.getAuthToken();
        this.authentication = node.getAuthentication();
        this.engine = new EngineController(this);

        for (Palette palette : node.getToolkits()) {
            this.palettes.add(palette);
        }
        
        if (node.getVersion() != null) {
            this.version = Version.parse(node.getVersion());
        }
        this.build = node.getBuild();

        for (String urn : node.getComponentUrns()) {
            this.resourceConfiguration.getComponentIds().add(ResourceFactory.getProjectIdFromUrn(urn));
        }
        for (String c : node.getSynchronizedProjectUrns()) {
            this.resourceConfiguration.getSynchronizedProjectIds().add(Path.getLast(c, ':'));
        }
        for (String s : node.getStaticResourceIds()) {
            this.resourceConfiguration.getStaticResources().add(StaticResource.valueOf(s));
        }
        this.getPrototypesFromCapabilities(node);

    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Node.class)) {
            throw new KlabRuntimeException("cannot serialize a node to a " + desiredClass.getCanonicalName());
        }

        Node ret = new Node();

        ret.setUrl(url);
        ret.setId(id);
        ret.setAuthToken(key);
        ret.setAuthentication(authentication);
        ret.setVersion(version.toString());
        ret.setBuild(build);

        for (Palette palette : this.palettes) {
            ret.getToolkits().add(palette);
        }
        
        for (String s : this.resourceConfiguration.getComponentIds()) {
            IComponent component = KLAB.PMANAGER.getComponent(s);
            if (component != null) {
                ret.getComponentUrns().add(ResourceFactory.getComponentUrn(component));
            }
        }

        for (String pid : KLAB.ENGINE.getResourceConfiguration().getSynchronizedProjectIds()) {
            IProject project = KLAB.PMANAGER.getProject(pid);
            if (project != null) {
                ret.getSynchronizedProjectUrns().add(ResourceFactory.getProjectUrn(project));
            }
        }

        for (StaticResource sr : StaticResource.values()) {
            ret.getStaticResourceIds().add(sr.name());
        }

        return (T) ret;
    }

    /**
     * Check if URL responds and set lastTimeAlive. Only called on nodes that are
     * quarantined.
     */
    public void checkActivity() {
        if (NetUtilities.urlResponds(url)) {
            lastTimeAlive = System.currentTimeMillis();
        }
    }

    /**
     * Check the current version (Version.CURRENT) against the one in the node, and
     * perform any necessary conversion before sending the bean in a request.
     * 
     * @param bean
     * @return
     */
    public IModelBean checkVersion(ModelQuery bean) {

        /**
         * TODO remove this when 0.9.10 is production
         */
        if (version.compareTo(Version.parse("0.9.10")) < 0) {
            /*
             * change observation: ontology to imcore: in both observable and
             * scope.observable
             */
            bean.getObservable().setObservationType(bean.getObservable().getObservationType()
                    .replace("observation:", "imcore:"));
            bean.getScope().getObservable()
                    .setObservationType(bean.getScope().getObservable().getObservationType()
                            .replace("observation:", "imcore:"));
            bean.getObservable().setType(Definition.downgrade(bean.getObservable().getType()));
            bean.getScope().getObservable().setType(Definition.downgrade(bean.getScope().getObservable().getType()));
        }
        return bean;
    }

}
