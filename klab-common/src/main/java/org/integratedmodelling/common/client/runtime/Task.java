package org.integratedmodelling.common.client.runtime;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.model.runtime.AbstractTask;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * @author ferdinando.villa
 *
 */
public class Task extends AbstractTask implements NetworkDeserializable {

    @Override
    public String toString() {
        return "[T " + getStatus().name() + " "
                + ((getDescription() == null || getDescription().isEmpty()) ? "" : (": " + getDescription()))
                + "]";
    }

    @Override
    public void interrupt() {
        ((ClientSession)this.getContext().getSession()).getEngineController().abortTask(this);
    }

    @Override
    public IContext finish() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IContext getContext() {
        return monitor.getContext();
    }

    @SuppressWarnings("javadoc")
    public void setMonitor(IMonitor monitor) {
        this.monitor = ((Monitor) monitor).get(this);
        this.addMonitor(this.monitor);
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Task)) {
            throw new KlabRuntimeException("cannot deserialize a Task from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Task bean = (org.integratedmodelling.common.beans.Task) object;

        this.id = bean.getId();
        this.start = bean.getStartTime();
        this.end = bean.getEndTime();
        this.status = Status.valueOf(bean.getStatus());
        this.description = bean.getDescription();

    }

}
