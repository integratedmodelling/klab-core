package org.integratedmodelling.common.client.runtime;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.resolution.IResolution;
import org.integratedmodelling.common.beans.generic.Graph;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.visualization.GraphVisualization;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.jgrapht.graph.DefaultDirectedGraph;

public class Resolution extends DefaultDirectedGraph<IResolution.Node, IResolution.Dependency>
		implements IResolution, NetworkDeserializable {

	private static final long serialVersionUID = 6120910715977831258L;

	class Node implements IResolution.Node {

		String id;
		String label;
		String type;
		IMetadata metadata;

		Node(String id, String label, String type, IMetadata md) {
			this.id = id;
			this.label = label;
			this.metadata = md;
			this.type = type;
		}

		@Override
		public int hashCode() {
			return id.hashCode();
		}

		@Override
		public boolean equals(Object obj) {
			return obj instanceof Node && ((Node) obj).id.equals(id);
		}
	}

	class Dependency implements IResolution.Dependency {

		String sId;
		String tId;
		String id;
		String label;
		IMetadata metadata;

		@Override
		public Node getSourceNode() {
			return nodes.get(sId);
		}

		@Override
		public Node getTargetNode() {
			return nodes.get(tId);
		}

		Dependency(String id, String sId, String tId, String label, IMetadata metadata) {
			this.id = id;
			this.sId = sId;
			this.tId = tId;
			this.label = label.startsWith("_") ? "" : label;
			this.metadata = metadata;
		}

	}

	Map<String, Node> nodes = new HashMap<>();

	public Resolution() {
		super(IResolution.Dependency.class);
	}

	public GraphVisualization visualize() {

		GraphVisualization ret = new GraphVisualization();

		ret.adapt(this, new GraphVisualization.GraphAdapter<IResolution.Node, IResolution.Dependency>() {

			@Override
			public String getNodeType(IResolution.Node o) {
				return ((Node)o).type;
			}

			@Override
			public String getNodeId(IResolution.Node o) {
				return ((Node) o).id;
			}

			@Override
			public String getNodeLabel(IResolution.Node o) {
				return ((Node) o).label;
			}

			@Override
			public String getNodeDescription(IResolution.Node o) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String getEdgeType(IResolution.Dependency o) {
				return "datapath";
			}

			@Override
			public String getEdgeId(IResolution.Dependency o) {
				return ((Dependency) o).id;
			}

			@Override
			public String getEdgeLabel(IResolution.Dependency o) {
				return ((Dependency) o).label;
			}

			@Override
			public String getEdgeDescription(IResolution.Dependency o) {
				return null;
			}
		});

		return ret;
	}

	@Override
	public void deserialize(IModelBean object) {

		if (!(object instanceof Graph)) {
			throw new KlabRuntimeException(
					"cannot deserialize an Dataflow from a " + object.getClass().getCanonicalName());
		}
		Graph bean = (Graph) object;

		for (String v : bean.getNodes()) {
			String[] s = v.split("\\|");
			Node step = new Node(s[0], s[1], s[2], KLAB.MFACTORY.adapt(bean.getMetadata().get(s[0]), Metadata.class));
			nodes.put(s[0], step);
			addVertex(step);
		}

		for (String v : bean.getRelationships()) {
			String[] s = v.split("\\|");
			Dependency path = new Dependency(s[0], s[1], s[2], s[3],
					KLAB.MFACTORY.adapt(bean.getMetadata().get(s[0]), Metadata.class));
			Node source = nodes.get(s[1]);
			Node target = nodes.get(s[2]);
			addEdge(source, target, path);
		}
	}

	@Override
	public boolean isEmpty() {
		return nodes.size() == 0;
	}

	@Override
	public IMetadata collectMetadata(Object node) {
		// TODO Auto-generated method stub
		return null;
	}

}
