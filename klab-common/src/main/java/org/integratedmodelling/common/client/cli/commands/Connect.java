/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.client.EngineNotifier.EngineData;
import org.integratedmodelling.common.client.ModelingClient;
import org.integratedmodelling.common.configuration.KLAB;

/**
 * Create certificate for server. Will only work if keys are in place and referenced through 
 * auth.properties.
 * 
 * Call it with either the -u <username> option or the -s <serverId> option to create either
 * user or server certificates.
 * 
 * @author Ferd
 *
 */
@CLIPrototype
@Prototype(
        id = "connect",
        description = "create a server certificate (needs full authorization configuration)",
        args = {
                "# engine",
                Prototype.INT
        })
public class Connect {

    @Execute
    public Object connect(IServiceCall command) throws Exception {

        if (!(KLAB.ENGINE instanceof ModelingClient)) {
            return null;
        }

        java.util.List<EngineData> engines = ((ModelingClient) KLAB.ENGINE).getEngines();

        if (engines.size() > 1 && !command.has("engine")) {
            return "there is more than one engine available: please use 'list engines' and pass an engine number";
        }

        int engine = command.has("engine") ? Integer.parseInt(command.getString("engine")) : 0;
        if (engines.size() > engine) {
            return ((ModelingClient) KLAB.ENGINE).connect(engines.get(engine));
        }
        return false;
    }
}
