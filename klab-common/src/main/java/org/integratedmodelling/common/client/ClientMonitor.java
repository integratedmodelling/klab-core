/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client;

import java.util.logging.Level;

import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.monitoring.Monitor;

/**
 * Basic monitor for default clients. Just logs stuff. Any UI will want to
 * call the {@link ModelingClient} constructor with a customized monitor for
 * appropriate user notification.
 * 
 * Note: if you don't want the task, session and context to be notified 
 * (potentially creating infinite recursion if it is a notification that
 * calls this) pass a Notification to the functions.
 * 
 * @author ferdinando.villa
 *
 */
public class ClientMonitor extends Monitor {

    @Override
    public void warn(Object o) {
        KLAB.warn(notify(o, Level.WARNING, null));
    }

    @Override
    public void info(Object o, String infoClass) {
        KLAB.info(notify(o, Level.INFO, infoClass));
    }

    @Override
    public void error(Object o) {
        KLAB.error(notify(o, Level.SEVERE, null));
        hasErrors = true;
    }

    @Override
    public void debug(Object o) {
        KLAB.debug(notify(o, Level.FINE, null));
    }

    @Override
    public void send(Object o) {
        /*
         * TODO pass message through engine's notification bus.
         */
    }

}
