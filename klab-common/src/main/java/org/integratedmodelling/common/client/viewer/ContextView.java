package org.integratedmodelling.common.client.viewer;

import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IViewer;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.common.model.runtime.Structure;
import org.integratedmodelling.common.space.SpaceLocator;
import org.integratedmodelling.common.time.TimeLocator;

import lombok.Data;

/**
 * Holds the status of a user's view of a context, tracking map and feature stacks and
 * keeping note of events timelines and timeplots.
 * 
 * @author Ferd
 */
public @Data class ContextView implements DisplayState {

    /**
     * Context we're watching
     */
    IContext                           context;

    /**
     * Subject in focus
     */
    IDirectObservation                 focus;

    /**
     * The active displays (those that are under the user's eyes)
     */
    Set<IViewer.Display>               activeDisplays  = new HashSet<>();

    /**
     * The enabled displays (those that are either active or that the user may switch to).
     */
    Deque<IViewer.Display>             enabledDisplays = new LinkedList<>();

    /**
     * State at the top of the stack
     */
    IState                             state;

    /**
     * if not null, we're looking at a particular time. Should not be null unless in
     * non-temporal contexts.
     */
    TimeLocator                        timeLocator;

    /**
     * if not null, we're looking at a subset - likely a point - of the context's space.
     */
    SpaceLocator                       spaceLocator;

    /**
     * whether the outline or point (if spatial) of the focal observation is shown.
     */
    boolean                            focusShown      = false;

    /*
     * The holders of the view state for the different displays
     */
    Map<IViewer.Display, DisplayState> displayState    = new HashMap<>();

    ContextView(IContext context) {
        this.context = context;
        this.focus = context.getSubject();
        if (this.focus != null) {
            initializeDisplay();
        }
    }

    @Override
    public Locator getTime() {
        return timeLocator;
    }

    private void initializeDisplay() {
        displayState.clear();
        if (this.focus.getScale().getSpace() != null) {
            activeDisplays.add(IViewer.Display.MAP);
            enabledDisplays.add(IViewer.Display.MAP);
            displayState.put(IViewer.Display.MAP, new MapView());
        }
        if (this.focus.getScale().isTemporallyDistributed()) {
            enabledDisplays.add(IViewer.Display.TIMEPLOT);
            enabledDisplays.add(IViewer.Display.TIMELINE);
            displayState.put(IViewer.Display.TIMEPLOT, new TimeView());
            displayState.put(IViewer.Display.TIMELINE, new TimelineView());
            if (activeDisplays.isEmpty()) {
                activeDisplays.add(IViewer.Display.TIMEPLOT);
            }
        }
        if (this.focus instanceof ISubject
                && ((Structure) ((ISubject) this.focus).getStructure()).hasFlows()) {
            enabledDisplays.add(IViewer.Display.FLOWS);
            displayState.put(IViewer.Display.FLOWS, new FlowView());
            if (activeDisplays.isEmpty()) {
                activeDisplays.add(IViewer.Display.FLOWS);
            }
        }
        if (activeDisplays.isEmpty()) {
            activeDisplays.add(IViewer.Display.DATAGRID);
        }
        displayState.put(IViewer.Display.DATAGRID, new GridView());
    }

    public MapView getMapView() {
        return (MapView) displayState.get(IViewer.Display.MAP);
    }
    
    /**
     * Shift focus to another observation.
     * 
     * @param observation
     * @return true if focus has changed.
     */
    public boolean setFocus(IDirectObservation observation) {

        if (this.focus != null && this.focus.equals(observation)) {
            return false;
        }
        focusShown = true;
        if (observation == null) {
            this.focus = this.context.getSubject();
        } else {
            this.focus = observation;
        }
        clear();
        initializeDisplay();
        return true;
    }

    /**
     * Check if anything in the view needs to be updated from the current state of the
     * passed context, and return whether the user view needs update.
     * 
     * @param context
     * @return true if state changes affects the displayed view
     */
    public boolean update(IContext context) {

        return false;
    }

    @Override
    public void clear() {
        for (DisplayState dd : displayState.values()) {
            dd.clear();
        }
    }

    @Override
    public boolean isShown(IObservation observation) {

        for (IViewer.Display d : activeDisplays) {
            if (displayState.get(d).isShown(observation)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public Result show(IObservation observation) {

        Result ret = Result.IGNORE;
        for (IViewer.Display d : displayState.keySet()) {
            if (activeDisplays.contains(d)) {
                Result r = displayState.get(d).show(observation);
                if (r != Result.IGNORE) {
                    ret = r;
                }
            }
        }

        if (ret == Result.IGNORE && !isShown(observation)) {
            
            /*
             * needs a change
             */
            if (observation.getScale().isSpatiallyDistributed()) {
                ret = Result.OBJECT_DISTRIBUTED;
            } else if (observation.getScale().isTemporallyDistributed()) {
                ret = Result.OBJECT_DISTRIBUTED;
            } else if (observation.getScale().getSpace() != null) {
                ret = observation.getScale().getSpace().getShape()
                        .getGeometryType() == IShape.Type.POINT ? Result.OBJECT_POINT
                                : Result.OBJECT_AREA;
            }
        }

        return ret;
    }

    @Override
    public Result hide(IObservation observation) {
        Result ret = Result.IGNORE;
        for (IViewer.Display d : displayState.keySet()) {
            if (activeDisplays.contains(d)) {
                Result r = displayState.get(d).hide(observation);
                if (r != Result.IGNORE) {
                    ret = r;
                }
            }
        }
        return ret;
    }

    /**
     * Get the location of the view in time and space, if one has been set.
     * 
     * @return the collection of user-defined locators
     */
    public Iterable<Locator> getLocators() {
        
        List<Locator> ret = new ArrayList<>();
        if (spaceLocator != null) {
            ret.add(spaceLocator);
        }
        if (timeLocator != null) {
            ret.add(timeLocator);
        }
        return ret;
    }

    /**
     * Set locators throughout the set of displays. Return true if
     * time has changed.
     * 
     * @param locators
     */
    public boolean locate(Locator... locators) {
        
        boolean ret = false;
        
        for (Locator locator : locators) {
            if (locator instanceof SpaceLocator) {
                setSpaceLocator((SpaceLocator) locator);
            } else if (locator instanceof TimeLocator) {
                setTimeLocator((TimeLocator) locator);
                for (DisplayState ds : displayState.values()) {
                    ((AbstractView) ds).setTime(locator);
                }
                ret = true;
            }
        }
        
        return ret;
    }

}
