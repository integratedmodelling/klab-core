package org.integratedmodelling.common.client.referencing;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledgeIndex;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.Filter;
import org.integratedmodelling.common.client.palette.PaletteItem;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.indexing.KnowledgeIndex;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Incremental knowledge search allowing textual input depending on context and initial
 * search configuration.
 * 
 * @author Ferd
 *
 */
public class KnowledgeSearch {

    private SearchType      statedSearchType = null;
    private Set<SearchType> types            = new HashSet<>();
    private static int      JUSTIFY          = 45;

    /**
     * Types of search that users may be doing.
     * 
     * @author Ferd
     *
     */
    static public enum SearchType {
        OBSERVATION,
        CONCEPT,
        SUBJECT,
        QUALITY,
        PROCESS,
        TRAIT,
        EVENT,
        ROLE,
        MODIFIER,
        INHERENCY,
        NAMESPACE
    }

    /**
     * Modifier elements that will affect a compatible search result.
     * 
     * @author Ferd
     *
     */
    static public enum SearchModifier {
        PRESENCE,
        DISTANCE,
        PROBABILITY,
        RATIO,
        VALUE,
        COUNT,
        PERCENTAGE
    }

    static public enum InherencyModifier {
        OF,
        WITHIN
    }

    class KFilter implements Filter<IConcept> {

        INamespace      namespace;
        IConcept        match;
        Set<SearchType> restrictedTypes = new HashSet<>();

        boolean isAdmitted(SearchType s) {
            if (!restrictedTypes.isEmpty()) {
                return restrictedTypes.contains(s);
            }
            return types.contains(s);
        }

        @Override
        public boolean match(IConcept o) {
            if (namespace != null && !o.getConceptSpace().equals(namespace.getId())) {
                return false;
            }
            if (match != null && !o.is(match)) {
                return false;
            }
            if (isAdmitted(SearchType.CONCEPT)) {
                return true;
            }
            boolean isProcess = NS.isProcess(o);
            if (isProcess && !isAdmitted(SearchType.PROCESS)) {
                return false;
            }
            boolean isTrait = NS.isTrait(o);
            if (isTrait && !isAdmitted(SearchType.TRAIT)) {
                return false;
            }
            boolean isQuality = NS.isQuality(o);
            if (isQuality && !isAdmitted(SearchType.QUALITY)) {
                return false;
            }
            return true;
        }

    }

    /*
     * modifier items for all mods to support search
     */
    static Map<String, PaletteItem>                   modifiers          = new HashMap<>();
    static List<String>                               modifierNames      = new ArrayList<>();
    static List<String>                               inherencyNames     = new ArrayList<>();
    List<String>                                      namespaceNames     = new ArrayList<>();

    /*
     * each new choice is recorded here.
     */
    List<PaletteItem>                                 currentChoices     = new ArrayList<>();
    KFilter                                           currentFilter      = new KFilter();
    private List<Pair<SearchType, List<PaletteItem>>> lastResult;

    /*
     * As we accept concepts we update the current meaning here; if a concept is here, it
     * must be observable. The pointer sets the beginning of successive resolution that
     * add to the current meaning.
     */
    private IConcept                                  currentSemantics   = null;
    private int                                       lastResolvedChoice = -1;

    static {

        modifiers
                .put("presence of", new PaletteItem(SearchModifier.PRESENCE, "presence of", StringUtils
                        .justifyLeft("Select this modifier to observe the presence of the subject, event, process or relationship you select next.", JUSTIFY)));
        modifiers
                .put("distance to", new PaletteItem(SearchModifier.DISTANCE, "distance to", StringUtils
                        .justifyLeft("Select this modifier to observe the distance from instances of the subject, event, process or relationship you select next.", JUSTIFY)));
        modifiers
                .put("probability of", new PaletteItem(SearchModifier.PROBABILITY, "probability of", StringUtils
                        .justifyLeft("Select this modifier to observe the probability of the event you select next.", JUSTIFY)));
        modifiers
                .put("ratio", new PaletteItem(SearchModifier.RATIO, "ratio", StringUtils
                        .justifyLeft("Select this modifier to observe a quantitative ratio between the numeric quantities you select next.", JUSTIFY)));
        modifiers
                .put("value of", new PaletteItem(SearchModifier.VALUE, "value of", StringUtils
                        .justifyLeft("Select this modifier to observe a value for the concept you select next.", JUSTIFY)));
        modifiers
                .put("count", new PaletteItem(SearchModifier.COUNT, "count", StringUtils
                        .justifyLeft("Select this modifier to count subjects or events of the type you select next.", JUSTIFY)));
        modifiers
                .put("percentage of", new PaletteItem(SearchModifier.PERCENTAGE, "percentage of", StringUtils
                        .justifyLeft("Select this modifier to observe the percentage of entities showing the trait you select next.", JUSTIFY)));
        modifiers
                .put("of", new PaletteItem(InherencyModifier.OF, "of", StringUtils
                        .justifyLeft("Select this modifier to define the entity where you are observing ", JUSTIFY)));
        modifiers
                .put("within", new PaletteItem(InherencyModifier.WITHIN, "within", StringUtils
                        .justifyLeft("Select this modifier to observe the context for the ", JUSTIFY)));

        modifierNames.addAll(modifiers.keySet());
        inherencyNames.add("of");
        inherencyNames.add("within");
        modifierNames.remove("of");
        modifierNames.remove("within");
        Collections.sort(modifierNames);
    }

    public KnowledgeSearch(SearchType searchType) {
        setType(searchType);
        for (INamespace ns : KLAB.MMANAGER.getNamespaces()) {
            namespaceNames.add(ns.getId());
        }
        Collections.sort(namespaceNames);
    }

    /**
     * Compute the current target and if it's observable, return it; otherwise return
     * null.
     * 
     * @return
     */
    public PaletteItem getObservableTarget() {
        return null;
    }

    /**
     * Remove the last item added and recompute semantics.
     */
    public void removeLastChoice() {
        if (!currentChoices.isEmpty()) {
            currentChoices.remove(currentChoices.size() - 1);
            computeSemantics();
        }
    }

    public List<PaletteItem> getCurrentChoices() {
        return currentChoices;
    }

    /**
     * Add an item between those returned to the current choices and recompute the
     * resulting semantics. Return null unless the item is simply a part of the next
     * allowed choice (for now this only applies to a Namespace); if a string is returned,
     * this will be a prefix to constrain the next choice.
     * 
     * @param item
     */
    public String addChoice(PaletteItem item) {
        currentChoices.add(item);
        if (item.getNamespace() != null) {
            this.currentFilter.namespace = item.getNamespace();
            return item.getNamespace().getId();
        }
        computeSemantics();
        return null;
    }

    private void computeSemantics() {

        // TODO Auto-generated method stub

        /*
         * TODO redefine search type
         */

        /*
         * OF allowed only if semantics is there and is a quality without inherency.
         * WITHIN allowed only if OF is allowed or it's a quality without context.
         */

    }

    /**
     * Set the type and automatically add all other accessory types that apply.
     * 
     * @param type
     */
    public void setType(SearchType type) {

        statedSearchType = type;

        types.clear();

        if (type == SearchType.OBSERVATION) {
            types.add(SearchType.SUBJECT);
            types.add(SearchType.TRAIT);
        } else if (type == SearchType.CONCEPT || type == SearchType.QUALITY) {
            types.add(SearchType.MODIFIER);
        }

        if (type == SearchType.SUBJECT) {
            types.add(SearchType.ROLE);
        }
        if (type != SearchType.OBSERVATION) {
            types.add(SearchType.NAMESPACE);
        }
        if (type != SearchType.TRAIT && type != SearchType.ROLE) {
            types.add(SearchType.TRAIT);
        }

        types.add(type);
    }

    /**
     * Run a search for this string matching the current configuration. Results are
     * wrapped in palette items and arranged by type.
     * 
     * @return
     * @throws KlabException
     */
    public synchronized List<Pair<SearchType, List<PaletteItem>>> run(String match) throws KlabException {

        List<Pair<SearchType, List<PaletteItem>>> ret = new ArrayList<>();

        if (Character.isLowerCase(match.charAt(0)) && types.contains(SearchType.MODIFIER)) {
            List<String> rr = StringUtils.matchIn(match, modifierNames, 3);
            if (rr.size() > 0) {
                List<PaletteItem> rets = new ArrayList<>();
                for (String r : rr) {
                    rets.add(modifiers.get(r));
                }
                ret.add(new Pair<>(SearchType.MODIFIER, rets));
            }
        }

        if (Character.isLowerCase(match.charAt(0)) && types.contains(SearchType.INHERENCY)) {
            List<String> rr = StringUtils.matchIn(match, inherencyNames, 1);
            if (rr.size() > 0) {
                List<PaletteItem> rets = new ArrayList<>();
                for (String r : rr) {
                    rets.add(modifiers.get(r));
                }
                ret.add(new Pair<>(SearchType.INHERENCY, rets));
            }
        }

        if (Character.isLowerCase(match.charAt(0)) && types.contains(SearchType.NAMESPACE)) {
            List<String> rr = StringUtils.matchIn(match, namespaceNames, 2);
            if (rr.size() > 0) {
                List<PaletteItem> rets = new ArrayList<>();
                for (String r : rr) {
                    INamespace namespace = KLAB.MMANAGER.getNamespace(r);
                    String description = namespace.getDescription() == null
                            ? "Select to search a concept within this namespace."
                            : namespace.getDescription();
                    rets.add(new PaletteItem(namespace, StringUtils.justifyLeft(description, JUSTIFY)));
                }
                ret.add(new Pair<>(SearchType.NAMESPACE, rets));
            }
        }

        /*
         * Now for the actual search
         */
        IKnowledgeIndex i = KLAB.KM.getIndex();
        if (i != null) {
            List<PaletteItem> prets = new ArrayList<>();
            List<PaletteItem> qrets = new ArrayList<>();
            List<PaletteItem> srets = new ArrayList<>();
            List<PaletteItem> erets = new ArrayList<>();
            List<PaletteItem> trets = new ArrayList<>();
            for (IMetadata m : i.search(match)) {
                Object o = ((KnowledgeIndex) i).retrieveObject(m);
                PaletteItem b = use(o);
                if (b != null) {
                    if (NS.isThing((IConcept) o)) {
                        srets.add(b);
                    } else if (NS.isProcess((IConcept) o)) {
                        prets.add(b);
                    } else if (NS.isTrait((IConcept) o)) {
                        trets.add(b);
                    } else if (NS.isQuality((IConcept) o)) {
                        qrets.add(b);
                    } else if (NS.isEvent((IConcept) o)) {
                        erets.add(b);
                    } 
                }
            }
            if (srets.size() > 0) {
                ret.add(new Pair<>(SearchType.SUBJECT, srets));
            }
            if (prets.size() > 0) {
                ret.add(new Pair<>(SearchType.PROCESS, prets));
            }
            if (trets.size() > 0) {
                ret.add(new Pair<>(SearchType.TRAIT, trets));
            }
            if (qrets.size() > 0) {
                ret.add(new Pair<>(SearchType.QUALITY, qrets));
            }
            if (erets.size() > 0) {
                ret.add(new Pair<>(SearchType.EVENT, erets));
            }
        }

        this.lastResult = ret;

        return ret;
    }

    private PaletteItem use(Object o) {
        if (o instanceof IConcept) {
            return (currentFilter == null || currentFilter.match((IConcept) o))
                    ? new PaletteItem((IConcept) o) : null;
        }
        return null;
    }

    /**
     * If the last search produced one and only one result, return it. Otherwise return
     * null.
     * 
     * @return
     */
    public PaletteItem getOnlyResult() {
        if (lastResult != null && lastResult.size() == 1 && lastResult.get(0).getSecond().size() == 1) {
            return lastResult.get(0).getSecond().get(0);
        }
        return null;
    }

    public SearchType getType() {
        return statedSearchType;
    }

}
