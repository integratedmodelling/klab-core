/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// package org.integratedmodelling.common.client.cli.commands;
/// *******************************************************************************
// * Copyright (C) 2007, 2014:
// *
// * - Ferdinando Villa <ferdinando.villa@bc3research.org>
// * - integratedmodelling.org
// * - any other authors listed in @author annotations
// *
// * All rights reserved. This file is part of the k.LAB software suite,
// * meant to enable modular, collaborative, integrated
// * development of interoperable data and model components. For
// * details, see http://integratedmodelling.org.
// *
// * This program is free software; you can redistribute it and/or
// * modify it under the terms of the Affero General Public License
// * Version 3 or any later version.
// *
// * This program is distributed in the hope that it will be useful,
// * but without any warranty; without even the implied warranty of
// * merchantability or fitness for a particular purpose. See the
// * Affero General Public License for more details.
// *
// * You should have received a copy of the Affero General Public License
// * along with this program; if not, write to the Free Software
// * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// * The license is also available at: https://www.gnu.org/licenses/agpl.html
// *******************************************************************************/
// package org.integratedmodelling.client.commands;
//
// import org.integratedmodelling.api.services.IServiceCall;
// import org.integratedmodelling.api.services.annotations.Execute;
// import org.integratedmodelling.api.services.annotations.Prototype;
// import org.integratedmodelling.client.Client;
// import org.integratedmodelling.exceptions.ThinklabException;
//
/// **
// * Context command.
// *
// * @author ferdinando.villa
// *
// */
// @Prototype(id = "find",
// description = "search for knowledge",
// args = {
// "what", Prototype.TEXT,
// "? r|reindex", Prototype.NONE
// },
// argDescriptions = { "search expression", "reindex knowledge" })
// public class Find {
//
// @Execute
// public Object find(IServiceCall command) throws ThinklabException {
// checkReindex(command);
// return Client.get().getKnowledgeIndex().search(command.get("what").toString());
// }
//
// @Execute(command = "model")
// public Object findModel(IServiceCall command) {
// checkReindex(command);
// return Client.get().getTasks();
// }
//
// @Execute(command = "trait")
// public Object findTrait(IServiceCall command) {
// checkReindex(command);
// return Client.get().getTasks();
// }
//
// @Execute(command = "subject")
// public Object findSubject(IServiceCall command) {
// checkReindex(command);
// return Client.get().getTasks();
// }
//
// @Execute(command = "quality")
// public Object findQuality(IServiceCall command) {
// checkReindex(command);
// return Client.get().getTasks();
// }
//
// private void checkReindex(IServiceCall command) {
// if (command.has("reindex")) {
// Client.get().reindexKnowledge();
// }
// }
//
// }
