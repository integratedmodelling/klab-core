/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.runtime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.auth.IIdentity;
import org.integratedmodelling.api.auth.IUser.Status;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.visualization.IViewer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.common.auth.User;
import org.integratedmodelling.common.beans.Context;
import org.integratedmodelling.common.beans.Notification;
import org.integratedmodelling.common.beans.requests.ViewerNotification;
import org.integratedmodelling.common.client.EngineController;
import org.integratedmodelling.common.client.EngineNotifier;
import org.integratedmodelling.common.client.EngineNotifier.EngineData;
import org.integratedmodelling.common.client.viewer.AbstractViewer;
import org.integratedmodelling.common.client.viewer.WebViewer;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.model.runtime.Session;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.monitoring.Monitor;
import org.integratedmodelling.common.network.Broadcaster;
import org.integratedmodelling.common.network.ClientNetwork;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Client implementation of a Session, using EngineApi to talk to a remote engine.
 * 
 * @author ferdinando.villa
 *
 */
public class ClientSession extends Session implements NetworkDeserializable {

    EngineData            engine;
    EngineController      engineController;
    INetwork              networkMonitor;
    private IViewer       viewer;
    private ClientNetwork network;

    @SuppressWarnings("javadoc")
    public ClientSession() {
    }

    /**
     * @param notifier
     */
    public void connect(EngineNotifier notifier) {
        String channel = notifier.getStatus(engine).getName() + "/session/" + id;
        notifier.subscribe(channel, new Broadcaster.Listener<Notification>(Notification.class) {
            @Override
            public void onMessage(Notification payload) {
                acceptNotification(payload);
            }
        });
    }

    /**
     * Return whether a viewer was created.
     * 
     * @return true if we have a viewer.
     */
    public boolean hasViewer() {
        return viewer != null;
    }

    /**
     * Get a view controller, creating it if necessary. It will need to be started after
     * this.
     * 
     * @return the viewer for this environment and the current session
     */
    public IViewer getViewer() {
        if (viewer == null) {
            viewer = new WebViewer(this);
        }
        return viewer;
    }

    public EngineController getEngineController() {
        return engineController;
    }

    /**
     * @param notifier
     */
    public void disconnect(EngineNotifier notifier) {
        String channel = notifier.getStatus(engine).getName() + "/session/" + id;
        notifier.unsubscribe(channel);
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Session)) {
            throw new KlabRuntimeException("cannot deserialize a ClientSession from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Session bean = (org.integratedmodelling.common.beans.Session) object;
        user = new User(bean.getUser(), bean.getUserAuthToken());
        ((User) user).setOnline(Status.ONLINE);
        this.isReopenable = bean.isReopenable();
        this.id = bean.getId();
        this.startTime = bean.getStartTime();
        this.localFilesystem = bean.isLocalFilesystem();
        for (Context ctx : bean.getContexts()) {
            contexts.push(KLAB.MFACTORY
                    .adapt(ctx, org.integratedmodelling.common.client.runtime.Context.class));
        }
    }

    @Override
    protected void receive(IModelBean object) {
        if (object instanceof ViewerNotification
                && viewer instanceof AbstractViewer
                && ((ViewerNotification) object).getSessionId().equals(getId())) {
            ((AbstractViewer) viewer).acceptNotification((ViewerNotification) object);
        }
        super.receive(object);
    }

    /**
     * To be called by client engine after creation, to establish all connections.
     * 
     * @param monitor
     * @param engine
     * @param engineApi
     */
    public void set(IMonitor monitor, EngineData engine, EngineController engineApi) {
        this.engine = engine;
        this.engineController = engineApi;
        this.monitor = ((Monitor) monitor).get(this);
        this.network = new ClientNetwork(engineApi);
    }

    @Override
    public ITask observe(Object directObservable, Object... objects)
            throws KlabException {

        List<String> scenarios = new ArrayList<>();
        List<IExtent> forcings = new ArrayList<>();
        ISpatialExtent forceSpace = null;
        directObservable = Observables.getObservableObject(directObservable, null);
        String observable = directObservable instanceof IModelObject
                ? ((IModelObject) directObservable).getName()
                : directObservable.toString();

        /*
         * happens when we re-observe the context after using the 
         * implicit ROI
         */
        if (directObservable instanceof Space) {
            observable = "region-of-interest";
            forceSpace = (ISpatialExtent) directObservable;
        }

        /*
         * TODO add error checking
         */
        for (Object o : MiscUtilities.flattenParameterList(objects)) {
            if (o instanceof INamespace) {
                scenarios.add(((INamespace) o).getId());
            } else if (o instanceof String) {
                scenarios.add((String) o);
            } else if (o instanceof IExtent) {

                if (forceSpace != null && o instanceof ISpatialExtent) {
                    o = forceSpace;
                }
                forcings.add((IExtent) o);
            }
        }

        return engineController.observe(observable, this, scenarios, forcings);
    }

    @Override
    public boolean requestLock() {
        return engineController.lockEngine();
    }

    @Override
    public boolean releaseLock() {
        return engineController.unlockEngine();
    }

    @SuppressWarnings("javadoc")
    public void setViewer(IViewer viewer) {
        this.viewer = viewer;
    }

    @Override
    public INetwork getNetwork() {
        return network;
    }

    /*
     * (non-Javadoc)
     * @see org.integratedmodelling.common.model.runtime.Session#close()
     */
    @Override
    public void close() throws IOException {
        network.getMonitor().pausePolling();
        super.close();
    }


}
