package org.integratedmodelling.common.client.viewer;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.visualization.IView;
import org.integratedmodelling.api.modelling.visualization.IViewer;

/**
 * Hold the state of a display of observation, incarnating a given display paradigm such
 * as map, plot or flow, reacting to show/hide events from a user and guiding a
 * {@link IViewer} in choosing the appropriate action. Actions are returned categorized
 * according to the coverage of the returned view, interpreted differently by different
 * views.
 * 
 * @author ferdinando.villa
 *
 */
public interface DisplayState extends IView {

    /**
     * The type of display change required after a show/hide event.
     * 
     * @author ferdinando.villa
     *
     */
    enum Result {
        /**
         * Do nothing response to show/hide action.
         */
        IGNORE,
        /**
         * Show/hide something that contains multiple states in one view, such as a map or
         * line graph.
         */
        OBJECT_DISTRIBUTED,
        /**
         * Show/hide something that contains a single value that can be expressed
         * atomically.
         */
        OBJECT_POINT,
        /**
         * Show/hide something that represents a single object but has non-trivial areal
         * extent in this view.
         */
        OBJECT_AREA,
        /**
         * Show/hide something that represents a single object but has non-trivial
         * volumetric extent in this view.
         */
        OBJECT_VOLUME
    }

    /**
     * Reset view state to empty.
     */
    void clear();

    /**
     * Return whether the appropriate view stack contains the passed observation.
     * 
     * @param observation
     * @return true if shown
     */
    @Override
    boolean isShown(IObservation observation);

    /**
     * Put observation in the appropriate view stack, if appropriate, or ignore.
     * 
     * @param observation
     * @return the appropriate {@link Result} if observation was shown and view state was
     *         changed, {@link Result#IGNORE} otherwise.
     */
    Result show(IObservation observation);

    /**
     * Remove observation from the view stack it belongs to, if present. If not present,
     * send user an email saying "it's ok, I don't mind removing non-existing
     * observations".
     * 
     * @param observation
     * @return the appropriate {@link Result} if observation was hidden and view state was
     *         changed, {@link Result#IGNORE} otherwise.
     */
    Result hide(IObservation observation);

}
