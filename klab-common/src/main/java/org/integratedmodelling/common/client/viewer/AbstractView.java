package org.integratedmodelling.common.client.viewer;

import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.visualization.IView;
import org.integratedmodelling.common.time.TimeLocator;

/**
 * Common view methods.
 * 
 * @author ferdinando.villa
 *
 */
public abstract class AbstractView implements IView {

    TimeLocator timeLocator;

    @Override
    public Locator getTime() {
        return timeLocator;
    }

    protected void setTime(Locator locator) {
        this.timeLocator = (TimeLocator) locator;
    }
}
