/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.referencing;

import java.util.ArrayList;
import java.util.List;

import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;

/**
 * Make a graph out of all the resolved bookmarks, ignoring the others.
 * 
 * @author Ferd
 *
 */
public class BookmarkGraph extends DefaultDirectedGraph<Bookmark, DefaultEdge> {

    public BookmarkGraph() {
        super(DefaultEdge.class);
    }

    private static final long serialVersionUID = 309650128173662677L;

    public List<Bookmark> getTopLevelBookmarks() {

        List<Bookmark> ret = new ArrayList<Bookmark>();
        for (Bookmark b : vertexSet()) {
            if (b.inheritance.length == 1 && b.isResolved()) {
                ret.add(b);
            }
        }
        return ret;
    }

    public Bookmark[] childrenOf(Bookmark parentElement) {

        ArrayList<Object> ret = new ArrayList<Object>();
        for (DefaultEdge e : incomingEdgesOf(parentElement)) {
            if (this.getEdgeSource(e).isResolved())
                ret.add(this.getEdgeSource(e));
        }
        return ret.toArray(new Bookmark[ret.size()]);
    }

    public boolean hasChildren(Bookmark parent) {
        return childrenOf(parent).length > 0;
    }

    public Bookmark getParent(Bookmark element) {
        if (outgoingEdgesOf(element).size() > 0) {
            return getEdgeTarget(outgoingEdgesOf(element).iterator().next());
        }
        return null;
    }

}
