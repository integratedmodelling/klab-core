/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import org.integratedmodelling.api.client.Interactive;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Concept;
import org.integratedmodelling.common.owl.Property;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLObject;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLQuantifiedRestriction;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;

@CLIPrototype
@Prototype(id = "reason")
public class Reason {

    @Execute
    public Object edit(IServiceCall command) {

        Interactive ui = command.getInteractiveUI();

        if (ui != null) {

            ui.grabCommandLine("reason> ", "quit", new Interactive.CommandListener() {

                @Override
                public Object execute(String input) {

                    try {

                        String[] commands = input.split("\\s+");

                        String ret = "";

                        if (commands.length > 0) {

                            IKnowledge k = null;
                            IOntology o = null;
                            IKnowledge ko = null;

                            if (commands.length > 1) {
                                if (commands[1].contains(":")) {
                                    k = KLAB.k(commands[1]);
                                } else {
                                    o = KLAB.KM.getOntology(commands[1]);
                                }
                            }

                            if (commands.length > 2) {
                                if (commands[2].contains(":")) {
                                    ko = KLAB.k(commands[2]);
                                }
                            }

                            switch (commands[0]) {
                            case "help":
                                return "children <concept>\n"
                                        + "parents <concept>\n"
                                        + "unsatisfiable\n"
                                        + "restrictions <concept>\n"
                                        + "describe <concept>\n"
                                        + "coreobs <concept>\n"
                                        + "flush\n"
                                        + "is <concept1> <concept2>\n"
                                        + "compatible <concept1> <concept2>\n"
                                        + "ccompatible <concept1> <concept2>\n"
                                        + "commonparent <concept1> <concept2>\n";
                            case "children":
                                if (k instanceof IConcept) {
                                    if (KLAB.REASONER.isOn()) {
                                        return displayNodes(KLAB.REASONER
                                                .getSubClasses(((Concept) k)
                                                        .getOWLClass(), false), "hierarchy of "
                                                                + k);
                                    } else {
                                        String r = "";
                                        for (IConcept c : ((IConcept) k).getChildren()) {
                                            r += (r.isEmpty() ? "" : "\n") + c;
                                        }
                                        return r;
                                    }
                                } else if (k instanceof IProperty) {
                                    if (KLAB.REASONER.isOn()) {
                                        return displayNodes(KLAB.REASONER
                                                .getSubObjectProperties((OWLObjectPropertyExpression) ((Property) k)
                                                        .getOWLEntity(), false), "hierarchy of "
                                                                + k);
                                    } else {
                                        String r = "";
                                        for (IProperty c : ((IProperty) k).getChildren()) {
                                            r += (r.isEmpty() ? "" : "\n") + c;
                                        }
                                        return r;
                                    }
                                }
                                return "please specify a concept or property";
                            case "parents":
                                if (k instanceof IConcept) {
                                    if (KLAB.REASONER.isOn()) {
                                        return displayNodes(KLAB.REASONER
                                                .getSuperClasses(((Concept) k)
                                                        .getOWLClass(), false), "hierarchy of "
                                                                + k);
                                    } else {
                                        String r = "";
                                        for (IConcept c : ((IConcept) k).getParents()) {
                                            r += (r.isEmpty() ? "" : "\n") + c;
                                        }
                                        return r;
                                    }
                                } else if (k instanceof IProperty) {
                                    if (KLAB.REASONER.isOn()) {
                                        return displayNodes(KLAB.REASONER
                                                .getSuperObjectProperties((OWLObjectPropertyExpression) ((Property) k)
                                                        .getOWLEntity(), false), "hierarchy of "
                                                                + k);
                                    } else {
                                        String r = "";
                                        for (IProperty c : ((IProperty) k).getParents()) {
                                            r += (r.isEmpty() ? "" : "\n") + c;
                                        }
                                        return r;
                                    }
                                }
                                return "please specify a concept or property";
                            case "unsatisfiable":
                                int n = 0;
                                for (OWLClass s : KLAB.REASONER.getUnsatisfiableClasses()) {
                                    ret += (ret.isEmpty() ? "" : "\n") + s;
                                    n++;
                                }
                                ret += "\n" + n + " unsatisfiable classes";
                                return ret;
                            case "restrictions":
                                for (OWLQuantifiedRestriction<?, ?, ?> zio : ((Concept) k)
                                        .getRestrictions().getObjectRestrictions()) {
                                    ret += (ret.isEmpty() ? "" : "\n") + zio.getProperty()
                                            + " " + zio.getFiller();
                                }
                                return ret;
                            case "describe":
                                return Observables.describe(k);
                            case "coreobs":
                                return Observables.getCoreObservable((IConcept) k);
                            case "flush":
                                KLAB.REASONER.flush();
                                return ret;
                            case "is":
                                return KLAB.REASONER.is(k, ko) ? "true" : "false";
                            case "compatible":
                                if (ko == null) {
                                    return Observables.getCompatibleWith((IConcept) k);
                                }
                                return Observables.isCompatible((IConcept) k, (IConcept) ko)
                                        ? "true" : "false";
                            case "ccompatible":
                                return Observables
                                        .isContextCompatible((IConcept) k, (IConcept) ko)
                                                ? "true" : "false";
                            case "commonparent":
                                return NS.getLeastGeneralCommonConcept((IConcept) k, (IConcept) ko);
                            }

                        }
                    } catch (Throwable e) {
                        return e;
                    }
                    return null;
                }

            });
        }

        return null;

    }

    protected Object displayNodes(NodeSet<?> set, String title) {

        String ret = title;

        for (OWLObject zio : set.getFlattened()) {
            ret += "\n   " + zio;
        }

        ret += "\n\n" + set;

        return ret;
    }

    private void displayNode(Node<?> n, String ret, int i) {
        String prefix = StringUtils.spaces(i);
        ret += prefix + n;
        for (OWLObject ch : n) {

        }
    }

}
