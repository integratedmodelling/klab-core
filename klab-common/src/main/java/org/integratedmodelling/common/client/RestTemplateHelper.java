/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any other authors listed
 * in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable modular, collaborative,
 * integrated development of interoperable data and model components. For details, see
 * http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the Affero
 * General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any warranty; without even the
 * implied warranty of merchantability or fitness for a particular purpose. See the Affero General Public
 * License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this program; if not, write
 * to the Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. The license
 * is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.data.lists.Escape;
import org.integratedmodelling.exceptions.KlabRemoteException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Helper to avoid having to write 10 lines of shit every time I need to do a GET with
 * headers. It can be given authorization objects using the with(...) idiom:
 * 
 * <code> RestTemplateHelper template = new RestTemplateHelper(); ...
 * template.with(session).post(....) ... </code>
 * 
 * with(..) can be called with any of IUser, INode, ITask, IContext and ISession according
 * to the type of authentication required by the receiver.
 *
 * The template also configures an objectmapper for optimal use in k.LAB, manages errors
 * and inserts user agent headers.
 *
 * Note that the template keeps the previous authorization token. This should not be
 * relied upon, but if headers need to be reset be sure to invoke as template.reset()....
 * 
 * @author ferdinando.villa
 *
 */
public class RestTemplateHelper extends RestTemplate {

    ObjectMapper objectMapper;
    String       authToken;

    public class JSONResponseErrorHandler implements ResponseErrorHandler {

        @Override
        public void handleError(ClientHttpResponse response) throws IOException {
        }

        @Override
        public boolean hasError(ClientHttpResponse response) throws IOException {
            HttpStatus status = response.getStatusCode();
            HttpStatus.Series series = status.series();
            return (HttpStatus.Series.CLIENT_ERROR.equals(series)
                    || HttpStatus.Series.SERVER_ERROR.equals(series));
        }
    }

    /**
     * Interceptor to add user agents and ensure that the authorization token gets in.
     * 
     * @author ferdinando.villa
     *
     */
    public class AuthorizationInterceptor implements ClientHttpRequestInterceptor {

        @Override
        public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution)
                throws IOException {

            HttpRequestWrapper requestWrapper = new HttpRequestWrapper(request);
            HttpHeaders headers = requestWrapper.getHeaders();
            headers.set("Accept", "application/json");
            headers.set("X-User-Agent", "k.LAB " + Version.CURRENT);
            headers.set(API.KLAB_VERSION_HEADER, Version.CURRENT);
            if (authToken != null) {
                headers.set(API.AUTHENTICATION_HEADER, authToken);
            }
            return execution.execute(requestWrapper, body);
        }
    }

    private void setup() {

        objectMapper = new ObjectMapper();
        List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
        MappingJackson2HttpMessageConverter jsonMessageConverter = new MappingJackson2HttpMessageConverter();
        StringHttpMessageConverter utf8 = new StringHttpMessageConverter(Charset.forName("UTF-8"));
        FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        // ByteArrayHttpMessageConverter byteConverter = new
        // ByteArrayHttpMessageConverter();
        jsonMessageConverter.setObjectMapper(objectMapper);

        setErrorHandler(new JSONResponseErrorHandler());
        messageConverters.add(jsonMessageConverter);
        messageConverters.add(utf8);
        messageConverters.add(formHttpMessageConverter);
        // messageConverters.add(byteConverter);
        setMessageConverters(messageConverters);
        this.setInterceptors(Collections.singletonList(new AuthorizationInterceptor()));
    }

    /**
     * Create a template, which at this point is exactly like a regular RestTemplate. Use
     * with() to create cheap delegates that automatically insert headers when the local
     * get/post functions are called.
     * 
     * Use this one to get a new template, to ensure that all timeout settings and any other
     * future configuration gets in.
     */
    public static RestTemplateHelper newTemplate() {
        return new RestTemplateHelper(EngineController.clientHttpRequestFactory());
    }
    
    private RestTemplateHelper(ClientHttpRequestFactory factory) {
        super(factory);
        setup();
    }

    /**
     * Set the authorization to the passed object.
     * 
     * @param authorizer a user, context, session, task or node.
     * @return self for fluency
     */
    public RestTemplateHelper with(Object authorizer) {

        authToken = null;

        if (authorizer instanceof IUser) {
            authToken = ((IUser) authorizer).getSecurityKey();
        } else if (authorizer instanceof ISession) {
            authToken = ((ISession) authorizer).getId();
        } else if (authorizer instanceof IContext) {
            authToken = ((IContext) authorizer).getId();
        } else if (authorizer instanceof IServer) {
            authToken = ((IServer) authorizer).getKey();
        } else if (authorizer instanceof ITask) {
            authToken = ((ITask) authorizer).getTaskId();
        } else if (authorizer instanceof String) {
            authToken = (String) authorizer;
        }

        return this;
    }

    /**
     * Reset all headers and returns the clean template.
     * 
     * @return a cleaned template with no authorization info.
     */
    public RestTemplateHelper reset() {
        authToken = null;
        return this;
    }

    /**
     * Instrumented for header communication
     * 
     * @param url
     * @param object
     * @param cls
     * @return the deserialized result
     */
    public <T extends Object> T post(String url, IModelBean object, Class<? extends T> cls) {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        headers.set(API.KLAB_VERSION_HEADER, Version.CURRENT);
        if (authToken != null) {
            headers.set(API.AUTHENTICATION_HEADER, authToken);
        }

        HttpEntity<IModelBean> entity = new HttpEntity<>(object, headers);

        try {
            return postForObject(new URI(url), entity, cls);
        } catch (RestClientException | URISyntaxException e) {
            throw new KlabRuntimeException(e);
        }
    }

    public boolean getDownload(String url, File output) throws KlabRemoteException {

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new ByteArrayHttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        headers.set(API.KLAB_VERSION_HEADER, Version.CURRENT);

        HttpEntity<String> entity = new HttpEntity<String>(headers);

        // HttpHeaders headers = new HttpHeaders();
        // headers.setAccept(Arrays.asList(MediaType.APPLICATION_OCTET_STREAM));
        // HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<byte[]> response = restTemplate.exchange(url, HttpMethod.GET, entity, byte[].class);

        if (response.getBody() == null) {
            return false;
        }
        if (response.getStatusCode() == HttpStatus.OK) {
            try {
                Files.write(output.toPath(), response.getBody());
            } catch (IOException e) {
                throw new KlabRemoteException(e);
            }
        }

        return true;
    }

    /**
     * Instrumented for header communication and error parsing
     * 
     * @param url
     * @param cls
     * @return the deserialized result
     * @throws KlabRemoteException
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public <T extends Object> T get(String url, Class<? extends T> cls) throws KlabRemoteException {

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", "application/json");
        headers.set(API.KLAB_VERSION_HEADER, Version.CURRENT);
        if (authToken != null) {
            headers.set(API.AUTHENTICATION_HEADER, authToken);
        }
        HttpEntity<String> entity = new HttpEntity<>(headers);

        if (!IModelBean.class.isAssignableFrom(cls)) {
            HttpEntity<T> response = (HttpEntity<T>) exchange(url, HttpMethod.GET, entity, cls);
            return response.getBody();
        }

        HttpEntity<Map> response = exchange(url, HttpMethod.GET, entity, Map.class);

        if (response.getBody() == null) {
            return null;
        }
        if (response.getBody().containsKey("exception") && response.getBody().get("exception") != null) {
            Object exception = response.getBody().get("exception");
            Object path = response.getBody().get("path");
            Object message = response.getBody().get("message");
            Object error = response.getBody().get("error");
            throw new KlabRemoteException("remote exception: " + (message == null ? exception : message));
        }

        return objectMapper.convertValue(response.getBody(), cls);
    }

    /**
     * Instrumented for header communication and error parsing
     * 
     * @param url
     * @param cls
     * @param urlVariables
     * @return the deserialized result
     * @throws KlabRemoteException
     */
    public <T extends Object> T get(String url, Class<? extends T> cls, Map<String, ?> urlVariables)
            throws KlabRemoteException {
        return get(addParameters(url, urlVariables), cls);
    }

    /**
     * Create a GET URL from a base url and a set of parameters. Yes I know I can use
     * URIComponentsBuilder etc.
     * 
     * @param url
     * @param parameters
     * @return
     */
    public static String addParameters(String url, Map<String, ?> parameters) {
        String ret = url;
        if (parameters != null) {
            for (String key : parameters.keySet()) {
                if (ret.length() == url.length()) {
                    ret += "?";
                } else {
                    ret += "&";
                }
                ret += key + "=" + Escape.forURL(parameters.get(key).toString());
            }
        }
        return ret;
    }

}
