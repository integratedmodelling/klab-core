/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.viewer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IObservable;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.knowledge.ExportableArtifact;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.knowledge.Observation.ObservationVisitor;
import org.integratedmodelling.common.knowledge.ObservationGroup;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.runtime.DirectObservation;

/**
 * A view of a subject's structure made for display and browsing. Uses folders to group
 * subjects and relationships meaningfully and provides uniform access methods to the
 * hierarchy.
 * 
 * @author ferdinando.villa
 **/
public class ContextStructure {

    IContext                         context;
    ISubject                         subject;
    ISubject                         povSubject;
    Map<String, Object>              parents                = new HashMap<>();
    Map<ISubject, Set<IObservation>> subjectiveObservations = new HashMap<>();

    /**
     * A folder is an observation of a group of direct observations. It may hold
     * relationships, in which case they may be of different types and the observable will
     * be null.
     */
    @SuppressWarnings("javadoc")
    public class Folder extends ObservationGroup {

        private static final long serialVersionUID = -6948404167166780198L;
        IObservable               observable;
        String                    id;
        boolean                   isRelationship   = false;
        IMetadata                 metadata         = new Metadata();
        ISubject                  parent;

        Folder(IConcept observable, ISubject parent) {
            super(observable, parent);
            parents.put(this.id, parent);
        }
    }

    /**
     * Create a context's structure. This structure is immutable and each modification to
     * the context should create a new structure.
     * 
     * @param context a context
     */
    public ContextStructure(IContext context, ISubject povSubject) {
        this.context = context;
        this.subject = context.getSubject();
        if (subject != null) {
            parents.put(context.getPathFor(subject), this);
        }
        this.povSubject = povSubject;
        scanSubjective();
    }

    private void scanSubjective() {

        if (subject == null) {
            return;
        }
        
        ((Observation) subject).visit(new ObservationVisitor() {

            @Override
            public void visitSubject(ISubject observation) {

            }

            @Override
            public void visitState(IState observation) {

                ISubject subject = observation.getObservingSubject();
                if (subject != null) {
                    Set<IObservation> set = subjectiveObservations.get(subject);
                    if (set == null) {
                        set = new HashSet<>();
                        subjectiveObservations.put(subject, set);
                    }
                    set.add(observation);
                }
            }

            @Override
            public void visitRelationship(IRelationship observation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void visitProcess(IProcess observation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void visitObservation(Observation observation) {
                // TODO Auto-generated method stub

            }

            @Override
            public void visitEvent(IEvent observation) {
                // TODO Auto-generated method stub

            }
        });
    }

    public ISubject getPOVSubject() {
        return povSubject;
    }

    /**
     * Check if passed folder is in the structure
     * 
     * @param folder
     * @return true if folder is in there
     */
    public boolean containsFolder(Folder folder) {
        return parents.containsKey(folder.getId());
    }

    /**
     * Return all children of the passed object
     * 
     * @param observation an observation, folder, or the structure itself
     * @return an array of child observations
     */
    public IObservation[] getChildren(Object observation) {
        ArrayList<IObservation> ret = new ArrayList<>();

        if (observation instanceof ContextStructure) {
            return new IObservation[] { subject };
        } else if (observation instanceof IDirectObservation) {

            /*
             * add states
             */
            for (IState state : ((IDirectObservation) observation).getStates()) {
                if (checkSubjective(state)) {
                    parents.put(context.getPathFor(state), observation);
                    ret.add(state);
                }
            }

            if (observation instanceof ISubject) {
                
                /*
                 * accumulate subjects in catalog
                 */
                Map<IConcept, List<ISubject>> catalog = new HashMap<>();
                for (ISubject s : ((ISubject) observation).getSubjects()) {
                    List<ISubject> list = catalog
                            .get(s.getObservable().getSemantics().getType());
                    if (list == null) {
                        list = new ArrayList<>();
                        catalog.put(s.getObservable().getSemantics().getType(), list);
                    }
                    list.add(s);
                }

                /*
                 * add folders with 2 subjects or more
                 */
                for (IConcept key : catalog.keySet()) {
                    if (catalog.get(key).size() > 1) {
                        Folder folder = new Folder(key, (ISubject) observation);
                        for (ISubject s : catalog.get(key)) {
                            parents.put(context.getPathFor(s), folder);
                            folder.add(s);
                        }
                        ret.add(folder);
                    }
                }

                /*
                 * add all lone subjects
                 */
                for (IKnowledge key : catalog.keySet()) {
                    if (catalog.get(key).size() == 1) {
                        parents.put(context.getPathFor(catalog.get(key).get(0)), observation);
                        ret.add(catalog.get(key).get(0));
                    }
                }

                /*
                 * add all relationships, in a folder if > 1
                 */
                Set<IRelationship> relationships = ((ISubject) observation).getStructure()
                        .getRelationships();
                if (relationships.size() > 1) {
                    Folder folder = new Folder(null, (ISubject) observation);
                    for (IRelationship r : relationships) {
                        parents.put(context.getPathFor(r), folder);
                        folder.add(r);
                    }
                    ret.add(folder);
                } else if (relationships.size() > 0) {
                    IRelationship r = relationships.iterator().next();
                    parents.put(context.getPathFor(r), observation);
                    ret.add(r);
                }
                
                /*
                 * all processes individually
                 * FIXME these should be possible in all countables - including relationships and
                 * events. Need ICountableObservation in between.
                 */
                for (IProcess p : ((ISubject) observation).getProcesses()) {
                    parents.put(context.getPathFor(p), observation);
                    ret.add(p);
                }
            }

        } else if (observation instanceof Folder) {
            ret.addAll((Folder) observation);
        }

        if (observation instanceof DirectObservation) {
            /*
             * add all ExportableArtifact beans
             */
            for (String s : ((DirectObservation) observation).getDatasetOutputs()) {
                ret.add(new ExportableArtifact(s, (IDirectObservation) observation, false));
            }
            for (String s : ((DirectObservation) observation).getModelOutputs()) {
                ret.add(new ExportableArtifact(s, (IDirectObservation) observation, true));
            }
        }

        return ret.toArray(new IObservation[ret.size()]);
    }

    private boolean checkSubjective(IState state) {

        if (state.getObservingSubject() != null && (povSubject == null
                || !state.getObservingSubject().equals(povSubject))) {
            return false;
        }
        return true;
    }

    /**
     * Get the parent of the passed object.
     * 
     * @param observation an observation, folder, or the structure itself.
     * @return the parent, or null
     */
    public Object getParent(Object observation) {
        return observation instanceof ExportableArtifact
                ? ((ExportableArtifact) observation).getParent()
                : (parents.get(observation instanceof Folder ? ((Folder) observation).id
                        : (observation instanceof ContextStructure ? null
                                : context.getPathFor((IObservation) observation))));
    }

    /**
     * Check if passed element has children.
     * 
     * @param element an observation, folder, or the structure itself
     * @return true if children are present
     */
    public boolean hasChildren(Object element) {
        if (element instanceof Folder) {
            return ((Folder) element).size() > 0;
        } else if (element instanceof IDirectObservation) {

            if (((IDirectObservation) element).getStates().size() > 0
                    || ((DirectObservation) element).getModelOutputs().size() > 0
                    || ((DirectObservation) element).getDatasetOutputs().size() > 0) {
                return true;
            } else if (element instanceof ISubject) {
                if (((ISubject) element).getSubjects().size() > 0
                        || ((ISubject) element).getProcesses().size() > 0) {
                    return true;
                }
            }

        } else if (element instanceof ContextStructure) {
            return hasChildren(subject);
        }
        return false;
    }

    /**
     * Count all the observation of the passed concept (if countable) or with the passed
     * role (if role).
     * 
     * @param concept
     * @return
     */
    public int count(IConcept concept) {
        // TODO Auto-generated method stub
        return 0;
    }

    /**
     * True if there are observations in this context that represent the subjective view
     * of the passed subject.
     * 
     * @param obs
     * @return
     */
    public boolean hasSubjectiveObservations(ISubject obs) {
        return subjectiveObservations.containsKey(obs)
                && subjectiveObservations.get(obs).size() > 0;
    }

    public boolean isPOVSubject(ISubject s) {
        if (s == null) {
            return povSubject == null;
        } else if (povSubject != null) {
            return s.equals(povSubject);
        }
        return false;
    }
}
