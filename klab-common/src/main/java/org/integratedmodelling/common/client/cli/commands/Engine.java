/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import java.io.File;

import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.configuration.Configuration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.launcher.EngineLauncher;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Create certificate for server. Will only work if keys are in place and referenced
 * through auth.properties.
 * 
 * Call it with either the -u <username> option or the -s <serverId> option to create
 * either user or server certificates.
 * 
 * @author Ferd
 *
 */
@CLIPrototype
@Prototype(
        id = "engine",
        description = "start and stop the engine",
        args = {
                "what",
                "start|stop"
        })
public class Engine {

    static EngineLauncher launcher;

    @Execute
    public Object connect(IServiceCall command) throws Exception {

        if (launcher == null) {
            
            File certfile = new File(System.getProperty("user.home") + File.separator + Configuration.KLAB_RELATIVE_WORK_PATH
                    + File.separator + "im.cert");
            File datadir = new File(System.getProperty("user.home") + File.separator + Configuration.KLAB_RELATIVE_WORK_PATH);
            if (!certfile.exists()) {
                throw new KlabValidationException("cannot find certificate file");
            }
            
            launcher = new EngineLauncher(certfile, datadir, null) {
                @Override
                protected void error(String error) {
                    KLAB.ENGINE.getMonitor().error(error);
                }
            };
        }
        
        if (launcher != null) {
            if (command.getString("what").equals("start")) {
                launcher.launch(false);
                return "engine starting ...";
            } else if (command.getString("what").equals("stop")) {
                launcher.shutdown(0);
                return "engine stopping...";
            }
        }

    return "no action taken";
}}
