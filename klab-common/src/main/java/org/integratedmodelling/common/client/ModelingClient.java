/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.lang.IParsingScope;
import org.integratedmodelling.api.metadata.IObservationMetadata;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.INotificationBus;
import org.integratedmodelling.api.monitoring.IProjectLifecycleListener;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.network.INetwork;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.auth.KlabCertificate;
import org.integratedmodelling.common.beans.Notification;
import org.integratedmodelling.common.beans.responses.Capabilities;
import org.integratedmodelling.common.beans.responses.Directory;
import org.integratedmodelling.common.client.EngineNotifier.EngineData;
import org.integratedmodelling.common.client.EngineNotifier.EngineListener;
import org.integratedmodelling.common.client.runtime.ClientSession;
import org.integratedmodelling.common.configuration.Configuration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.network.Broadcaster;
import org.integratedmodelling.common.network.Broadcaster.EngineStatus;
import org.integratedmodelling.common.network.ClientNetwork;
import org.integratedmodelling.common.owl.KnowledgeManager;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabInternalErrorException;

/**
 * The client version of a modeling engine. Will connect to any engine seen on the local
 * network (if on the same machine this will happen automatically) and enable the same
 * workflow as a regular engine.
 * 
 * @author ferdinando.villa
 */
public class ModelingClient extends KlabClient implements IModelingEngine {

    /**
     * If we pass this many seconds without receiving a heartbeat, we are not running.
     */
    private static final int                    MAX_IDLE_SECONDS_FOR_OFFLINE = 30;

    /**
     * All installed listeners. If they are EngineListener, they also get the heartbeat
     * signal from the engine.
     */
    List<Listener>                              listeners                    = new ArrayList<>();

    /**
     * Sessions by engine
     */
    private Map<EngineData, ISession>           sessions                     = new HashMap<>();

    /**
     * Sessions by ID
     */
    private Map<String, ISession>               sessionsById                 = new HashMap<>();

    /**
     * Engine handlers by engine
     */
    protected Map<EngineData, EngineController> engines                      = new HashMap<>();

    /**
     * Engine capabilities. We may want to periodically refresh.
     */
    private Map<EngineData, Capabilities>       capabilities                 = new HashMap<>();

    /*
     * The current engine URL.
     */
    protected EngineData                        currentEngine                = null;
    protected EngineNotifier                    engineNotifier               = null;
    private boolean                             localBootDone;
    private long                                lastHeartbeat                = 0;
    private IUser                               user;
    private String                              communicationChannel;

    private KlabCertificate                     klabCertificate;

    /**
     * @param certificate
     * @throws KlabException
     */
    public ModelingClient(File certificate) throws KlabException {
        super(certificate);
        initialize();
    }

    private void initialize() {

        this.klabCertificate = new KlabCertificate(certificate);
        if (!klabCertificate.isValid()) {
            System.out.println(klabCertificate.getCause());
            System.exit(0);
        }

        primaryServerUrl = klabCertificate.getProperties().getProperty(KlabCertificate.PRIMARY_NODE_KEY);

        engineNotifier = new EngineNotifier(klabCertificate.getProperties()
                .getProperty(KlabCertificate.USER_KEY), new EngineListener() {

                    @Override
                    public boolean engineOffline(EngineData ed) {

                        if (currentEngine != null && currentEngine.equals(ed)) {

                            int secs = getSecondsOffline();

                            if (secs > 60) {
                                return disconnect(true);
                            } else if (secs > 30) {
                                monitor.warn("engine unresponsive for 30 seconds; will close session at 1 minute");
                            }
                        }

                        KLAB.info("engine at " + ed.getIp() + ":" + ed.getPort() + " (" + ed.getName()
                                + ") went offline");

                        return false;
                    }

                    private int getSecondsOffline() {
                        return lastHeartbeat == 0 ? 0
                                : (int) ((System.currentTimeMillis() - lastHeartbeat) / 1000);
                    }

                    @Override
                    public void engineAvailable(EngineData ed) {
                        /*
                         * keep a running list of available engines
                         */
                        KLAB.info("engine at " + ed.getIp() + ":" + ed.getPort() + " (" + ed.getName()
                                + ") is online");
                    }

                    @Override
                    public void engineHeartbeat(EngineData ed, EngineStatus status) {
                        if (currentEngine != null && currentEngine.equals(ed)) {
                            lastHeartbeat = System.currentTimeMillis();
                        }
                    }
                });
    }

    @Override
    public String getUrl() {
        return currentEngine == null ? null : currentEngine.getUrl();
    }

    /**
     * Override this one to do any necessary saving or cleanup. This default
     * implementation merely logs, notifies any listeners and disconnects. Return
     * super.disconnect() to do the actual disconnection.
     * 
     * @param onError
     * @return
     * @throws IOException
     */
    public boolean disconnect(boolean onError) {

        if (onError) {
            monitor.info("engine "
                    + (currentEngine.isLocal() ? ""
                            : ("at " + currentEngine.getIp() + ":" + currentEngine.getPort()))
                    + " is offline: disconnecting session", Messages.INFOCLASS_STOP);
        }
        ISession prev = sessions.remove(currentEngine);
        if (prev != null) {
            for (Listener listener : listeners) {
                listener.sessionClosed(prev);
            }
            ((ClientSession) prev).disconnect(engineNotifier);
            try {
                ((ClientSession) prev).close();
            } catch (IOException e) {
                monitor.error(e);
            }
        }
        currentEngine = null;
        lastHeartbeat = 0;
        return true;
    }

    /**
     * Implements the default engine choosing logic: choose the local engine if there is
     * one. Do not return until an engine is available.
     * 
     * @param needExclusive
     * @param certificate
     * @return the descriptor for the chosen engine when available.
     * @throws KlabException
     */
    protected EngineData connectToEngine() throws KlabException {

        try {

            EngineData localEngine = null;
            while (localEngine == null) {
                localEngine = engineNotifier.getLocalEngineUrl();
                if (localEngine != null) {
                    return localEngine;
                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            throw new KlabInternalErrorException(e);
        }

        return null;
    }

    /**
     * @param engine
     * @return true if connection was successful
     * @throws KlabException
     */
    public boolean connect(EngineData engine) throws KlabException {

        if (getCurrentSession() != null) {
            disconnect(false);
        }

        KLAB.info("connecting with local engine at " + engine.getIp() + ":" + engine.getPort());

        EngineController api = new EngineController(engine, listeners, monitor);

        this.user = api.authenticate(klabCertificate.getCertificate());

        engines.put(engine, api);
        currentEngine = engine;
        org.integratedmodelling.common.beans.Session session;
        session = api.openSession(certificate, api.getPublicKeyURL(), needExclusive);
        if (session != null) {
            ClientSession s = KLAB.MFACTORY.adapt(session, ClientSession.class);
            engines.get(currentEngine).setSession(s);
            s.set(monitor, currentEngine, engines.get(currentEngine));
            sessions.put(engine, s);
            sessionsById.put(s.getId(), s);
            monitor.info("opened session with " + currentEngine.getIp() + ":"
                    + currentEngine.getPort(), Messages.INFOCLASS_USER_OWN);
            s.connect(engineNotifier);
            performUserBoot();
            Environment.get().setSession(s);
            for (Listener listener : listeners) {
                listener.sessionOpened(s);
            }
            lastHeartbeat = 0;
            monitor.info(getNetwork().getNodes().size()
                    + " k.LAB nodes are online", Messages.INFOCLASS_NETWORK);
            return true;
        }
        return false;
    }

    /**
     * Boot functions that require an engine connected. When we get here we have a current
     * engine and no current session. This should be called every time we switch to a new
     * engine.
     * 
     * @throws KlabException
     */
    protected void performNetworkBoot() throws KlabException {

        File dir = KLAB.CONFIG.getDataPath("knowledge");
        FileUtils.deleteQuietly(dir);
        ResourceFactory.getDirectoryFromURN(currentEngine
                .getUrl(), "directory", API.CORE_KNOWLEDGE_URN, dir, getUser(), Directory.class);

        if (KLAB.KM == null) {
            KLAB.KM = new KnowledgeManager();
            ((KnowledgeManager) KLAB.KM).initialize();
            setupListeners();
        }

        EngineController api = new EngineController(currentEngine, listeners, monitor);
        engines.put(currentEngine, api);

        /*
         * authenticate our user from certificate, without opening a session. We will
         * redefine the user when we do, so that our user key is always current.
         */
        this.user = api.authenticate(klabCertificate.getCertificate());
        for (Listener listener : listeners) {
            listener.engineUserAuthenticated(this.user);
        }

    }

    /**
     * Boot functions that require a session active and are repeated at each session
     * switch. This is called after successful connection.
     * 
     * @throws KlabException
     */
    protected void performUserBoot() throws KlabException {
        loadCapabilities();
        synchronizeNetworkProjects();
        synchronizeWorkspace();
        loadProjects();
    }

    private void loadCapabilities() throws KlabIOException {

        Capabilities cp = engines.get(currentEngine).getCapabilities();
        if (cp == null) {
            throw new KlabIOException("could not read engine capabilities for " + currentEngine);
        }

        this.communicationChannel = cp.getCommunicationChannel();
        if (communicationChannel != null) {
            engineNotifier
                    .subscribe(communicationChannel, new Broadcaster.Listener<Notification>(Notification.class) {
                        @Override
                        public void onMessage(Notification payload) {
                            acceptNotification(payload);
                        }
                    });
        }

        this.capabilities.put(currentEngine, cp);
        this.getPrototypesFromCapabilities(cp);
        this.getAuthoritiesFromCapabilities(cp);

    }

    protected void setupListeners() {

        KLAB.PMANAGER.addListener(new IProjectLifecycleListener() {

            @Override
            public void projectUnregistered(IProject project) {
                if (!KLAB.WORKSPACE.isRemotelySynchronized(project)) {
                    if (getCurrentSession() != null && !getCurrentSession().isExclusive()) {
                        monitor.warn("this session is non-privileged: " + project
                                + " will not be considered");
                    } else if (currentEngine != null) {
                        try {
                            engines.get(currentEngine).undeployProject(project);
                            monitor.info("project " + project.getId()
                                    + "  de-registered with engine", Messages.INFOCLASS_UPLOAD);
                        } catch (Throwable e) {
                            monitor.error("project " + project.getId()
                                    + "  could not be de-registered with engine: "
                                    + e.getMessage());
                        }
                    }
                }
            }

            @Override
            public void projectRegistered(IProject project) {
                if (!KLAB.WORKSPACE.isRemotelySynchronized(project)) {
                    if (getCurrentSession() != null && !getCurrentSession().isExclusive()) {
                        monitor.warn("this session is non-privileged: " + project
                                + " will not be considered");
                    } else if (currentEngine != null) {
                        try {
                            engines.get(currentEngine).deployProject(project);
                        } catch (KlabException e) {
                            monitor.error("project " + project.getId()
                                    + "  could not be registered with engine: "
                                    + e.getMessage());
                        }
                    }
                }
            }

            @Override
            public void projectPropertiesModified(IProject project, File file) {
            }

            @Override
            public void onReload(boolean full) {
            }

            @Override
            public void namespaceModified(String ns, IProject project) {

                if (KLAB.WORKSPACE.isRemotelySynchronized(project)) {
                    // this happens legitimately with no harm.
                    // monitor.warn("project " + project.getId() + " is read
                    // only but the
                    // namespace " + ns
                    // + " in it was modified");
                } else if (getCurrentSession() != null && !getCurrentSession().isExclusive()) {
                    monitor.warn("this session is non-privileged: modifications to " + ns
                            + " will not be considered");
                } else if (currentEngine != null) {

                    monitor.info("reloading " + ns + " into engine", Messages.INFOCLASS_UPLOAD);
                    engines.get(currentEngine).pushNamespace(ns);

                    if (KLAB.KM.getIndex() != null) {
                        INamespace namespace = KLAB.MMANAGER.getNamespace(ns);
                        if (namespace != null) {
                            KLAB.KM.getIndex().index(namespace);
                            KLAB.KM.getIndex().reindex();
                        }
                    }
                }

            }

            @Override
            public void namespaceDeleted(String ns, IProject project) {
                if (KLAB.WORKSPACE.isRemotelySynchronized(project)) {
                    monitor.warn("project " + project.getId() + " is read only but the namespace " + ns
                            + " in it was deleted");
                } else if (getCurrentSession() != null && !getCurrentSession().isExclusive()) {
                    monitor.warn("this session is non-privileged: modifications to " + ns
                            + " will not be considered");
                } else if (currentEngine != null) {
                    if (KLAB.KM.getIndex() != null) {
                        INamespace namespace = KLAB.MMANAGER.getNamespace(ns);
                        if (namespace != null) {
                            KLAB.KM.getIndex().deleteNamespace(ns);
                        }
                    }
                    engines.get(currentEngine).deleteNamespace(ns);
                }
            }

            @Override
            public void namespaceAdded(String ns, IProject project) {
                if (KLAB.WORKSPACE.isRemotelySynchronized(project)) {
                    monitor.warn("project " + project.getId() + " is read only but the namespace " + ns
                            + " in it was modified");
                } else if (getCurrentSession() != null && !getCurrentSession().isExclusive()) {
                    monitor.warn("this session is non-privileged: modifications to " + ns
                            + " will not be considered");
                } else if (currentEngine != null) {
                    engines.get(currentEngine).pushNamespace(ns);
                    monitor.info("adding " + ns + " into engine", Messages.INFOCLASS_UPLOAD);
                    if (KLAB.KM.getIndex() != null) {
                        INamespace namespace = KLAB.MMANAGER.getNamespace(ns);
                        if (namespace != null) {
                            KLAB.KM.getIndex().index(namespace);
                            KLAB.KM.getIndex().reindex();
                        }
                    }

                }
            }

            @Override
            public void fileModified(IProject project, File file) {
            }

            @Override
            public void fileDeleted(IProject project, File file) {
            }

            @Override
            public void fileCreated(IProject project, File file) {
            }
        });
    }

    private void loadProjects() throws KlabException {

        IParsingScope context = KLAB.MFACTORY.getRootParsingContext();
        for (INamespace ns : KLAB.PMANAGER.load(true, context)) {
            KLAB.KM.getIndex().index(ns);
        }

        /*
         * ensure we have freshly indexed knowledge
         */
        if (KLAB.KM.getIndex() != null) {
            KLAB.KM.getIndex().reindex();
        }

        monitor.info(KLAB.PMANAGER.getProjects().size() + " projects loaded with " + context.getErrorCount()
                + " errors and " + context.getWarningCount() + " warnings", null);
    }

    /**
     * Read the client workspace. When we get here we have synchronized our project with
     * those exposed by the engine, so we may need to tell the engine to override some of
     * those if we have them in the client workspace and we have write privileges on the
     * engine.
     * 
     * @throws KlabException
     */
    private void synchronizeWorkspace() throws KlabException {

        engines.get(currentEngine).clearWorkspace();
        boolean needReload = false;

        for (IProject project : KLAB.PMANAGER.getProjects()) {
            if (!KLAB.WORKSPACE.isRemotelySynchronized(project)) {
                needReload = true;
                try {
                    engines.get(currentEngine).deployProject(project);
                } catch (Exception e) {
                    monitor.error("cannot deploy local project " + project.getId() + " to engine: "
                            + e.getMessage());
                }
            }
        }

        /*
         * send a final full reload to the engine
         */
        if (needReload) {
            engines.get(currentEngine).reloadProjects(true);
        }
    }

    /**
     * This will boot automatically when one or more engines come online. If more than one
     * engine is available, chooseEngine will be called and its result used. We can have
     * independent sessions active for different engines. Only engines that accept our
     * identity through the passed certificate are kept in the list. If needExclusive is
     * true, only engines that allow exclusive access will be selected for boot, although
     * others will remain available for explicit connection. If an engine is available on
     * the localhost and it's suitable for the needExclusive setting, it will be
     * automatically connected to, and user can
     * 
     * @throws KlabException if certificate file cannot be read or connection errors
     * happen
     */
    public void boot() throws KlabException {

        performLocalBoot();
        this.currentEngine = connectToEngine();
        if (this.currentEngine != null) {
            performNetworkBoot();
            connect(this.currentEngine);
        }
        this.bootTime = System.currentTimeMillis();
    }

    @Override
    public IUser getUser() {
        return user;
    }

    /**
     * Return the current session if any. The user way to do this is by using
     * {@link Environment#getSession()} .
     * 
     * @return the current session, or null.
     */
    public ISession getCurrentSession() {
        return currentEngine == null ? null : sessions.get(currentEngine);
    }

    /**
     * Get a copy of all network projects from the engine capabilities and synchronize
     * them into ${workspace}/deploy.
     * 
     * @throws KlabException
     */
    private void synchronizeNetworkProjects() throws KlabException {

        Capabilities cp = capabilities.get(currentEngine);
        for (String urn : cp.getSynchronizedProjectUrns()) {

            monitor.info("synchronizing project " + ResourceFactory.getProjectIdFromUrn(urn)
                    + " from network", Messages.INFOCLASS_DOWNLOAD);

            IProject project = ResourceFactory.getProjectFromURN(currentEngine
                    .getUrl(), urn, null, KLAB.WORKSPACE.getDeployLocation(), getUser());

            if (project == null) {
                monitor.warn("project URN " + urn + " could not be synchronized from modeling engine");
            }
        }
    }

    /**
     * Boot functions that don't require an engine to be connected. Redefine to add
     * anything necessary - GUI setup etc.
     * 
     * @throws KlabException
     */
    public void performLocalBoot() throws KlabException {

        if (!localBootDone) {

            KLAB.ENGINE = this;
            KLAB.CONFIG = new Configuration(true);
            KLAB.PMANAGER = new ProjectManager();
            KLAB.MMANAGER = new KIMModelManager();
            KLAB.MFACTORY = new ModelFactory();
            KLAB.WORKSPACE = createWorkspace();
            this.network = new ClientNetwork();
            localBootDone = true;
        }
    }

    /**
     * The list of engines that are alive at this moment. Use as a key to obtain
     * capabilities and status.
     * 
     * @return the list of engine data.
     */
    public List<EngineData> getEngines() {
        return engineNotifier.getEngines(60);
    }

    /**
     * Use this constructor to install a specific monitor.
     * 
     * @param certificate
     * @param monitor
     * @throws KlabException
     */
    public ModelingClient(File certificate, IMonitor monitor) throws KlabException {
        super(certificate, monitor);
        initialize();
    }

    @Override
    public IModelingEngine start() {
        try {
            boot();
        } catch (KlabException e) {
            return null;
        }
        return this;
    }

    @Override
    public boolean stop() {
        shutdown(0);
        return true;
    }

    @Override
    public String getName() {
        return user == null
                ? (getCurrentSession() == null ? "anonymous" : getCurrentSession().getUser().getUsername())
                : user.getUsername();
    }

    @Override
    public ISession createSession(IUser user) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IPrototype getFunctionPrototype(String id) {
        return prototypes.get(id);
    }

    @Override
    public Collection<IPrototype> getFunctionPrototypes() {
        return prototypes.values();
    }

    @Override
    public void addListener(Listener listener) {
        listeners.add(listener);
    }

    @Override
    public boolean isRunning() {
        return lastHeartbeat > 0
                && (System.currentTimeMillis() - lastHeartbeat) < (MAX_IDLE_SECONDS_FOR_OFFLINE * 1000);
    }

    @Override
    public INotificationBus getNotificationBus() {
        return engineNotifier;
    }

    @Override
    public String getCommunicationChannel() {
        return communicationChannel;
    }

    @Override
    public INetwork getNetwork() {
        ClientSession session = (ClientSession) getCurrentSession();
        return session == null ? network : session.getNetwork();
    }

    public EngineData getCurrentEngine() {
        return currentEngine;
    }

    public EngineController getCurrentEngineController() {
        return currentEngine == null ? null : engines.get(currentEngine);
    }

    @Override
    public List<IObservationMetadata> importObservations(File file) throws KlabException {
        if (currentEngine != null) {
            return engines.get(currentEngine).importObservation(file);
        }
        return null;
    }

    public void setOnlineStatus(boolean active) {
        KLAB.CONFIG.getProperties().setProperty(IConfiguration.KLAB_OFFLINE, active ? "off" : "on");
        KLAB.CONFIG.save();
    }

}
