package org.integratedmodelling.common.client.viewer;

import java.util.Deque;
import java.util.LinkedList;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.space.IShape.Type;

/**
 * Data for map display
 * 
 * @author ferdinando.villa
 *
 */
class MapView extends AbstractView implements DisplayState {

    /**
     * The stack of maps being shown at the moment. Changed by showMap() and reset when
     * the focus is set.
     */
    Deque<IState> maps = new LinkedList<>();

    /**
     * Stack of objects whose representation is points
     */
    Deque<IDirectObservation> pointFeatures = new LinkedList<>();

    /**
     * Stack of objects whose representation is a 2d spatial feature
     */
    Deque<IDirectObservation> features = new LinkedList<>();

    @Override
    public void clear() {
        maps.clear();
        features.clear();
        pointFeatures.clear();
    }

    @Override
    public boolean isShown(IObservation observation) {
        if (observation instanceof IState) {
            return maps.contains(observation);
        }
        return features.contains(observation) || pointFeatures.contains(observation);
    }

    @Override
    public Result show(IObservation observation) {
        if (isShown(observation)) {
            hide(observation);
        }
        if (observation instanceof IState && observation.getScale().isSpatiallyDistributed()) {
            maps.addFirst((IState) observation);
            return Result.OBJECT_DISTRIBUTED;
        } else if ((observation instanceof ISubject || observation instanceof IRelationship || observation instanceof IEvent) && observation.getScale().getSpace() != null) {
            if (observation.getScale().getSpace().getShape().getGeometryType() == Type.POINT) {
                pointFeatures.addFirst((IDirectObservation) observation);
                return Result.OBJECT_POINT;
            } else {
                features.addFirst((IDirectObservation) observation);
                return Result.OBJECT_AREA;
            }
        }
        return Result.IGNORE;
    }

    @Override
    public Result hide(IObservation observation) {
        if (observation instanceof IState) {
            if (maps.remove(observation)) {
                return Result.OBJECT_DISTRIBUTED;
            }
        }
        if (features.remove(observation)) {
            return Result.OBJECT_AREA;
        }
        if (pointFeatures.remove(observation)) {
            return Result.OBJECT_POINT;
        }
        return Result.IGNORE;
    }
}
