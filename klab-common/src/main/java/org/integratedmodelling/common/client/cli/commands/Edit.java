/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;

import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.command.ServiceCall;
import org.integratedmodelling.common.configuration.KLAB;

@CLIPrototype
@Prototype(
        id = "edit",
        description = "open the system's default editor on an existing namespace or a script",
        args = { "resource", Prototype.TEXT },
        argDescriptions = { "namespace or script to edit" })
public class Edit {

    @Execute
    public Object edit(IServiceCall command) {

        INamespace ns = KLAB.MMANAGER.getNamespace(command.get("resource").toString());
        boolean ok = false;
        if (ns != null) {
            File f = ns.getLocalFile();
            if (f != null && f.exists()) {
                ok = editFile(f);
            }
        }

        if (!ok) {
            ((ServiceCall) command).getMonitor().error("cannot edit file: editing not supported by host");
        }

        return null;
    }

    public static boolean editFile(final File file) {

        if (!Desktop.isDesktopSupported()) {
            return false;
        }

        Desktop desktop = Desktop.getDesktop();
        if (!desktop.isSupported(Desktop.Action.EDIT)) {
            return false;
        }

        try {
            // Bill knows why
            if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                String cmd = "rundll32 url.dll,FileProtocolHandler " + file.getCanonicalPath();
                Runtime.getRuntime().exec(cmd);
            } else {
                desktop.edit(file);
            }
        } catch (IOException e) {
            // Log an error
            return false;
        }

        return true;
    }
}
