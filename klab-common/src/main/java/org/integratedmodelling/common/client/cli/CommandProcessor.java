/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli;

import org.integratedmodelling.api.client.IConsole;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.client.cli.commands.Certify;
import org.integratedmodelling.common.client.cli.commands.Component;
import org.integratedmodelling.common.client.cli.commands.Connect;
import org.integratedmodelling.common.client.cli.commands.Edit;
import org.integratedmodelling.common.client.cli.commands.Engine;
import org.integratedmodelling.common.client.cli.commands.Export;
import org.integratedmodelling.common.client.cli.commands.Help;
import org.integratedmodelling.common.client.cli.commands.List;
import org.integratedmodelling.common.client.cli.commands.Observe;
import org.integratedmodelling.common.client.cli.commands.Project;
import org.integratedmodelling.common.client.cli.commands.Reason;
import org.integratedmodelling.common.client.cli.commands.Task;
import org.integratedmodelling.common.client.cli.commands.TestInteractive;
import org.integratedmodelling.common.client.cli.commands.View;
import org.integratedmodelling.common.client.cli.commands.Workspace;
import org.integratedmodelling.common.command.ServiceCall;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;

public class CommandProcessor extends com.eleet.dragonconsole.CommandProcessor {

    IMonitor monitor;
    protected IConsole terminal;

    public CommandProcessor(IConsole console, IMonitor monitor) {
        terminal = console;
        this.monitor = monitor;
        ServiceManager.setInteractiveUI(console);
        registerCommand(Certify.class);
        registerCommand(Component.class);
        registerCommand(Edit.class);
        registerCommand(Export.class);
//        if (KLAB.CONFIG.isDebug()) {
            registerCommand(TestInteractive.class);
//        }
        registerCommand(Engine.class);
        registerCommand(Help.class);
        registerCommand(List.class);
        registerCommand(Observe.class);
        registerCommand(Project.class);
        registerCommand(View.class);
        registerCommand(Task.class);
        registerCommand(Workspace.class);
        registerCommand(Connect.class);
        registerCommand(Reason.class);
    }

    @Override
    public void processCommand(String input) {

        input = input.trim();

        if (input.equals("exit")) {
            System.exit(0);
        } else if (input.length() > 0) {

            IServiceCall command = null;
            try {
                command = ServiceManager.get().parseCommandLine(input, null, null);
                boolean ok = command != null;
                if (command == null) {
                    terminal.warning("Command '" + input + "' incorrect or unknown");
                } else {
                    ((ServiceCall) command).setMonitor(monitor);
                    try {
                        /*
                         * TODO run asynchronously if command requires it.
                         */
                        terminal.echo("> " + input);
                        Object ret = command.execute();
                        terminal.outputResult(input, ret);
                    } catch (Throwable e) {
                        ok = false;
                        terminal.error(e);
                    }
                }
                terminal.reportCommandResult(input, ok);

            } catch (KlabException e) {
                terminal.error(e);
            }
        }

    }

    /**
     * Overrides the default output in CommandProcessor to determine if ANSI
     * Colors are processed or DCCC and converts accordingly.
     * @param s The String to output.
     */
    @Override
    public void output(String s) {
        if (getConsole().isUseANSIColorCodes())
            super.output(convertToANSIColors(s));
        else
            super.output(s);
    }

    /**
     * Manually register commands. With the CLI being largely a developer add-on, no need to go
     * all fancy on this.
     *  
     * @param target
     */
    protected void registerCommand(Class<?> target) {
        ServiceManager.get().processPrototypeDeclaration(target.getAnnotation(Prototype.class), target);
    }
}
