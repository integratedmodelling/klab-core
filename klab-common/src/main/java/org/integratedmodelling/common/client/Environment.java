package org.integratedmodelling.common.client;

import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.runtime.ITask;
import org.integratedmodelling.common.client.runtime.ClientSession;
import org.integratedmodelling.common.client.viewer.ContextStructure;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.time.TimeLocator;

import lombok.Data;

/**
 * Client environment - a singleton that holds all "current" objects during an
 * interactive client session.
 * 
 * @author Ferd
 *
 */
public @Data class Environment {

	class LastContext {
		IDirectObserver lastContextObserved = null;
		Space lastRegionObserved = null;

		LastContext(IDirectObserver obs) {
			this.lastContextObserved = obs;
		}

		LastContext(Space obs) {
			this.lastRegionObserved = obs;
		}
	}

	private ClientSession session;
	private IContext context;
	private ISubject targetSubject;
	// the subject that has the current point of view. If null, it's the root
	// subject.
	private ISubject povSubject;
	private ITask focusTask;
	private ContextStructure contextStructure;
	private TimeLocator currentTime;

	private Space spatialForcing = Space.getForcing(1.0, "km");
	private Time temporalForcing = Time.getForcing(0, 0, 0);
	private Set<INamespace> scenarios = new HashSet<>();
	private LastContext lastContextObserved = null;

	private static Environment _this;

	private Environment() {
		if (KLAB.CONFIG != null) {
			if (KLAB.CONFIG.getProperties().containsKey("default.time.forcing")) {
				this.temporalForcing = new Time(KLAB.CONFIG.getProperties().getProperty("default.time.forcing"));
			}
			if (KLAB.CONFIG.getProperties().containsKey("default.space.forcing")) {
				this.spatialForcing = new Space(KLAB.CONFIG.getProperties().getProperty("default.space.forcing"));
			}
		}
	}

	/**
	 * Return the instance of the client environment, creating it if necessary.
	 * 
	 * @return the environment, dummy.
	 */
	public static Environment get() {
		if (_this == null) {
			_this = new Environment();
		}
		return _this;
	}

	/**
	 * To be called by anything that creates sessions once a "current" session
	 * has been established.
	 * 
	 * @param session
	 */
	public void setSession(ClientSession session) {
		this.session = session;
	}

	public ContextStructure getContextStructure() {
		if (context != null && contextStructure == null) {
			this.contextStructure = new ContextStructure(context, povSubject);
		}
		if (context != null && contextStructure != null && !contextStructure.isPOVSubject(povSubject)) {
			this.contextStructure = new ContextStructure(context, povSubject);
		}
		return contextStructure;
	}

	/**
	 * Activate or deactivate a scenario.
	 * 
	 * @param scenario
	 * @param activate
	 *            if true, add; if false, remove
	 */
	public void setScenario(INamespace scenario, boolean activate) {
		if (activate) {
			scenarios.add(scenario);
		} else {
			scenarios.remove(scenario);
		}
	}

	public IContext focus(ISubject subject) {
		return context.focus(this.targetSubject = subject);
	}

	public void setContext(IContext context) {

		this.context = context;
		this.contextStructure = null;
		this.targetSubject = context.getSubject();
		this.temporalForcing = this.targetSubject == null ? null
				: (targetSubject.getScale().getTime() == null ? null
						: Time.getForcing(targetSubject.getScale().getTime()));

		if (this.targetSubject != null && this.targetSubject.getScale().isTemporallyDistributed()) {
			this.currentTime = TimeLocator.initialization();
		}
	}

	public void setFocusTask(ITask task) {
		this.focusTask = task;
		if (this.context == null) {
			this.context = task.getContext();
		}
		if (this.context != null && this.context.equals(task.getContext())) {
			if (this.targetSubject == null) {
				this.targetSubject = task.getContext().getSubject();
			}
			if (this.targetSubject != null) {
				this.temporalForcing = Time.getForcing(this.targetSubject.getScale().getTime());
			}
		}
	}

	/**
	 * Get the names of all the scenarios currently set. Used to avoid
	 * unnecessarily messy code in functions like
	 * {@link IContext#inScenario(String...)} that require variable parameters.
	 * 
	 * @return all current scenario Ids
	 */
	public String[] getScenarioIds() {
		int i = 0;
		String[] ret = new String[scenarios.size()];
		for (INamespace scenario : scenarios) {
			ret[i++] = scenario.getId();
		}
		return ret;
	}

	/**
	 * Remove any scenario set so far.
	 */
	public void clearScenarios() {
		scenarios.clear();
	}

	/**
	 * Record the current forcings as properties so that they will be loaded
	 * again the next time.
	 * 
	 * TODO switch to serialized JSON.
	 * 
	 */
	public void persistForcings() {
		if (temporalForcing != null) {
			KLAB.CONFIG.getProperties().setProperty("default.time.forcing", Time.asString(temporalForcing));
		} else {
			KLAB.CONFIG.getProperties().remove("default.time.forcing");
		}
		if (spatialForcing != null) {
			KLAB.CONFIG.getProperties().setProperty("default.space.forcing", Space.asString(spatialForcing));
		} else {
			KLAB.CONFIG.getProperties().remove("default.space.forcing");
		}
		KLAB.CONFIG.save();
	}

	public void setEmpty() {
		this.context = null;
		this.contextStructure = null;
		this.currentTime = null;
		this.focusTask = null;
		this.targetSubject = null;
		spatialForcing = Space.getForcing(1.0, "km");
		temporalForcing = Time.getForcing(0, 0, 0);
		if (KLAB.CONFIG != null) {
			if (KLAB.CONFIG.getProperties().containsKey("default.time.forcing")) {
				this.temporalForcing = new Time(KLAB.CONFIG.getProperties().getProperty("default.time.forcing"));
			}
			if (KLAB.CONFIG.getProperties().containsKey("default.space.forcing")) {
				this.spatialForcing = new Space(KLAB.CONFIG.getProperties().getProperty("default.space.forcing"));
			}
		}
	}

	public void defineLastContextObserved(IDirectObserver observer) {
		lastContextObserved = new LastContext(observer);
	}

	public void defineLastContextObserved(Space observer) {
		lastContextObserved = new LastContext(observer);
	}

	public Object reconstructLastContextObserved() {
		if (lastContextObserved != null) {
			if (lastContextObserved.lastContextObserved != null) {
				return lastContextObserved.lastContextObserved;
			}
			return lastContextObserved.lastRegionObserved;
		}
		return null;
	}

}
