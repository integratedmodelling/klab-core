package org.integratedmodelling.common.client.palette;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.integratedmodelling.Version;
import org.integratedmodelling.common.configuration.KLAB;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PaletteManager {

    private static PaletteManager _this;
    /*
     * this list contains the "working" palettes - either locally read from
     * disk (possibly modified from original ones from the network) or the
     * default version in original when the same palette is not available locally.
     */
    private List<Palette>         palettes     = new ArrayList<>();
    /*
     * this list contains all the palettes available from the network. Some
     * or all may also be in palettes if there is not a modified version
     * on the local filesystem.
     */
    private List<Palette>         original     = new ArrayList<>();
    ObjectMapper                  objectMapper = new ObjectMapper();

    public static PaletteManager get() {
        if (_this == null) {
            _this = new PaletteManager();
        }
        return _this;
    }

    private PaletteManager() {
        initialize();
    }

    private void initialize() {

        final File pdir = KLAB.CONFIG.getDataPath("palettes");
        readPalettes(pdir);
    }

    public void removeRemotePalettes() {
        original.clear();
    }

    public boolean addRemotePalette(Palette palette) {
        
        /*
         * if there is an original one with the same ID, change it
         * only if the passed one is newer.    
         */
        boolean present = false;
        boolean swap = false;
        for (Palette p : original) {
            if (p.getName().equals(palette.getName())) {
                Version vpresent = Version.parse(p.getVersion());
                Version vremote  = Version.parse(palette.getVersion());
                if (vremote.isGreaterThan(vpresent)) {
                    swap = true;
                } else {
                    present = true;
                }
                break;
            }
        }
        if (swap) {
            /*
             * remove old palette
             */
        }
        if (!present) {
            original.add(palette);
            boolean add = true;
            for (Palette p : palettes) {
                if (p.getName().equals(palette.getName())) {
                    add = false;
                    break;
                }
            }
            if (add) {
                palettes.add(palette);
            }
        }
        
        return false;
    }

    private Palette getPalette(File file) {
        try {
            Palette palette = objectMapper.readValue(file, Palette.class);
            palette.initialize();
            return palette;
        } catch (IOException e) {
            KLAB.warn("cannot read palette file " + file);
        }
        return null;
    }

    /**
     * Return the current list of palettes, arranging them so that the first one is the
     * last used.
     * @return
     */
    public List<Palette> getPalettes() {
        return palettes;
    }

    public PaletteItem getItem(String path) {
        PaletteItem ret = null;
        String[] paths = path.split("\\/");
        for (Palette p : palettes) {
            if (p.getInternalId().equals(paths[0])) {
                for (PaletteFolder f : p.getFolders()) {
                    if (f.getInternalId().equals(paths[0] + "/" + paths[1])) {
                        return PaletteFolder.findItem(f.getItems(), paths);
                    }
                }
            }
        }
        return ret;
    }

    /**
     * Read all toolkits in the passed directory.
     * 
     * @param toolkitPath
     */
    public void readPalettes(File toolkitPath) {

        for (String pp : toolkitPath.list(new FilenameFilter() {

            @Override
            public boolean accept(File dir, String name) {
                return dir.equals(toolkitPath) && name.endsWith(".kpl");
            }
        })) {
            File source = new File(toolkitPath + File.separator + pp);
            Palette palette = getPalette(source);
            if (palette != null) {
                palette.setLastModified(source.lastModified());
                palettes.add(palette);
            }
        }

        /*
         * sort so that the most recently used is the first.
         */
        Collections.sort(palettes, new Comparator<Palette>() {
            @Override
            public int compare(Palette o1, Palette o2) {
                return (int) (o2.getLastModified() - o1.getLastModified());
            }
        });
    }
}
