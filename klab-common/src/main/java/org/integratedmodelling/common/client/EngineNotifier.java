/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.integratedmodelling.common.configuration.Configuration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.network.Broadcaster;
import org.integratedmodelling.common.utils.IPUtils;

import lombok.Data;

/**
 * Keeps a running list of active engines on the local network. Generates listenable events
 * when a new one comes online or an old one goes offline. 
 * 
 * Don't forget to call close() on shutdown.
 * 
 * @author ferdinando.villa
 *
 */
public class EngineNotifier extends Broadcaster {

    private static int MAX_IDLE_FOR_FAILURE_SECOND = 66;

    public static @Data class EngineData {
        
        final String ip;
        final int    port;
        final String name;

        String getUrl() {
            return "http://" + ip + ":" + port + "/" + Configuration.MODELER_APPLICATION_ID;
        }

        /**
         * Check if engine is local (same host as the client).
         * @return true if engine runs on localhost.
         */
		public boolean isLocal() {
			try {
			    /*
			     * TODO link to Broadcaster config
			     */
				return /* useLoopback? */ ip.startsWith("127.") /* :  */ || IPUtils.isLocal(ip);
			} catch (Exception e) {
				return false;
			}
		}
    }

    /**
     * 
     * @author ferdinando.villa
     *
     */
    static public interface EngineListener {

        /**
         * Notifies that a new engine has come online.
         * @param ed 
         */
        void engineAvailable(EngineData ed);

        /**
         * Notifies that a previously active engine has been unresponsive for more than
         * MAX_IDLE_FOR_FAILURE_SECOND seconds (default 10). If true is returned, the engine
         * is removed from further checks. The check is done with the same frequency.
         * @param ed 
         * 
         * @return true if we shoud forget about this engine
         */
        boolean engineOffline(EngineData ed);

        /**
         * Intercept the heartbeat of each engine, carrying status informations. 
         * It's essential that any processing is quick and this function returns
         * immediately.
         * 
         * @param ed
         * @param status
         */
        void engineHeartbeat(EngineData ed, EngineStatus status);
    }

    List<EngineListener>          listeners = new ArrayList<>();
    
    /*
     * this doesn't need to be static, but this way we're guaranteed that the cluster does not
     * send signals during construction - it happened.
     */
    static Map<EngineData, EngineStatus> engines   = new HashMap<>();
    // local time when each engine sent the last ping. Used to establish when an engine becomes unresponsive.
    Map<EngineData, Long>         lastseen  = new HashMap<>();
    private Timer                 ctimer;

    EngineNotifier() {
        super((KLAB.ENGINE == null ? "notifier" : (KLAB.ENGINE.getName())), Configuration.MODELER_APPLICATION_ID);
    }

    class CheckAlive extends TimerTask {

        @Override
        public void run() {
            long now = System.currentTimeMillis();
            Set<EngineData> toRemove = new HashSet<>();
            synchronized (engines) {
                for (EngineData ed : engines.keySet()) {
                    if (lastseen.containsKey(ed)) {
                        long nonresponsive = now - lastseen.get(ed);
                        if (nonresponsive > MAX_IDLE_FOR_FAILURE_SECOND * 1000) {
                            for (EngineListener listener : listeners) {
                                if (listener.engineOffline(ed)) {
                                    toRemove.add(ed);
                                    break;
                                }
                            }
                        }
                    }
                }
                if (toRemove.size() > 0) {
                    for (EngineData ed : toRemove) {
                        engines.remove(ed);
                    }
                }
            }
        }
    }

    EngineNotifier(String nodeName, EngineListener listener) {
        super(nodeName, Configuration.MODELER_APPLICATION_ID);
        this.listeners.add(listener);
        if (listener != null) {
            this.ctimer = new Timer();
            ctimer.schedule(new CheckAlive(), 0, MAX_IDLE_FOR_FAILURE_SECOND * 1000);
        }
    }

    /**
     * If there is an engine active on the localhost, return its URL. Otherwise return null.
     * 
     * @return the data corresponding to an engine running on localhost, if any.
     */
    public EngineData getLocalEngineUrl() {

        synchronized (engines) {
            for (EngineData ed : engines.keySet()) {
            	if (ed.isLocal()) {
                    return ed;
                }
            }
        }
        return null;
    }

    /**
     * Return a list of all engines that were active during the passed amounts of seconds. Don't pass
     * less than 10 seconds if you want to catch all the active ones, unless the server broadcasters
     * were redefined to change the default interval.
     * 
     * @param secondsLastActive
     * @return all the engines known at the time of calling
     */
    public List<EngineData> getEngines(int secondsLastActive) {
        long ms = new Date().getTime();
        List<EngineData> ret = new ArrayList<>();
        synchronized (engines) {
            for (EngineData ed : engines.keySet()) {
                if ((ms - engines.get(ed).getLastEngineTime()) < secondsLastActive * 1000) {
                    ret.add(ed);
                }
            }
        }
        return ret;
    }

    /**
     * Get most current status of passed engine.
     * @param ed
     * @return status of requested engine at time of calling
     */
    public EngineStatus getStatus(EngineData ed) {
        synchronized (engines) {
            return engines.get(ed);
        }
    }

    @Override
    protected boolean isReceiving() {
        return KLAB.ENGINE != null;
    }

    @Override
    protected void onSignalAvailable(String ipAddress, int port, EngineStatus status) {
        
        synchronized (engines) {
            
            EngineData ed = new EngineData(ipAddress, port, status.getName());

//            System.out.println("got signal " + ed);

            boolean isKnown = engines.containsKey(ed);
            if (!listeners.isEmpty() && !isKnown) {
                for (EngineListener listener : listeners) {
                    listener.engineAvailable(ed);
                }
            }
            if (isKnown) {
                for (EngineListener listener : listeners) {
                    listener.engineHeartbeat(ed, status);
                }
            }
            engines.put(ed, status);
            lastseen.put(ed, System.currentTimeMillis());
        }
    }

    /**
     * Add an engine listener to be notified of engine events and heartbeat.
     * 
     * @param listener
     */
    public void addListener(EngineListener listener) {
        listeners.add(listener);
    }

}
