/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.client.cli.commands;

import java.util.ArrayList;

import org.integratedmodelling.api.errormanagement.ICompileNotification;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IServiceCall;
import org.integratedmodelling.api.services.annotations.CLIPrototype;
import org.integratedmodelling.api.services.annotations.Execute;
import org.integratedmodelling.api.services.annotations.Prototype;
import org.integratedmodelling.common.client.EngineNotifier.EngineData;
import org.integratedmodelling.common.client.ModelingClient;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.exceptions.KlabException;

@CLIPrototype
@Prototype(
        id = "list",
        args = {
                "# what",
                Prototype.TEXT,
                "? r|remote",
                Prototype.NONE,
                "? e|errors",
                Prototype.NONE
        },
        argDescriptions = {
                "what to list",
                "send request to remote server",
                "list errors (when applicable)" })
public class List {

    @Execute(requires = { "what" })
    public Object listObject(IServiceCall command) {

        ArrayList<Object> ret = new ArrayList<>();
        String s = command.get("what").toString();

        if (KLAB.MMANAGER.getNamespace(s) != null) {
            return KLAB.MMANAGER.getNamespace(s).getModelObjects();
        }

        return ret;
    }

    @Execute(command = "projects")
    public Object listProjects(IServiceCall command) {
        ArrayList<String> projects = new ArrayList<>();
        for (IProject p : KLAB.PMANAGER.getProjects()) {
            projects.add(p.getId());
        }
        return projects;
    }

    @Execute(command = "namespaces")
    public Object listNamespaces(IServiceCall command) {
        ArrayList<String> namespaces = new ArrayList<>();
        for (INamespace p : KLAB.MMANAGER.getNamespaces()) {
            namespaces.add(p.getId());
            if (command.has("errors")) {
                for (ICompileNotification a : p.getCodeAnnotations()) {
                    namespaces.add("   " + a);
                }
            }
        }
        return namespaces;
    }

    @Execute(command = "tasks")
    public Object listTasks(IServiceCall command) {
        return null; // Client.get().getTasks();
    }

    @Execute(command = "contexts")
    public Object listContexts(IServiceCall command) {
        return null; // Client.get().getContexts();
    }

    @Execute(command = "properties")
    public Object listProperties(IServiceCall command) {
        ArrayList<String> ret = new ArrayList<>();
        for (Object o : KLAB.CONFIG.getProperties().keySet()) {
            ret.add(o + " = " + KLAB.CONFIG.getProperties().getProperty(o.toString()));
        }
        return ret;
    }
    
    @Execute(command = "definitions")
    public Object listDefinitions(IServiceCall command) {
        ArrayList<String> ret = new ArrayList<>();
        for (IOntology o : KLAB.KM.getOntologies(false)) {
            for (IConcept c : o.getConcepts()) {
                String def = c.getDefinition();
                if (!def.equals(c.toString())) {
                    ret.add(def);
                }
            }
        }
        return ret;
    }

    @Execute(command = "engines")
    public Object listEngines(IServiceCall command) {
        ArrayList<String> ret = new ArrayList<>();
        if (KLAB.ENGINE instanceof ModelingClient) {
            for (EngineData o : ((ModelingClient) KLAB.ENGINE).getEngines()) {
                ret.add(o + " = " + KLAB.CONFIG.getProperties().getProperty(o.toString()));
            }
        }
        return ret;
    }

    @Execute(command = "network")
    public Object listNetwork(IServiceCall command) throws KlabException {

        ArrayList<Object> ret = new ArrayList<>();
        for (IServer node : KLAB.ENGINE.getNetwork().getNodes()) {

            ret.add(node.getId() + (node.getUrl() == null ? "" : (" [" + node.getUrl() + "]"))
                    + (node.isActive() ? " [ok]" : "[offline]") + ":");

            // for (String s : node.getResourceCatalog().getSynchronizedProjectIds()) {
            // ret.add(" [P] " + s);
            // }
            // if (node.getResourceCatalog().getSynchronizedProjectIds().size() == 0) {
            // ret.add(" <no projects>");
            // }
        }
        return ret;
    }
}
