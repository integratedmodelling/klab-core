package org.integratedmodelling.common.client.viewer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.visualization.IColormap;
import org.integratedmodelling.api.modelling.visualization.IHistogram;
import org.integratedmodelling.api.modelling.visualization.IView;
import org.integratedmodelling.api.modelling.visualization.IViewer;
import org.integratedmodelling.api.modelling.visualization.IViewport;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.requests.ViewerCommand;
import org.integratedmodelling.common.beans.responses.StateData;
import org.integratedmodelling.common.beans.responses.StateSummary;
import org.integratedmodelling.common.beans.responses.ValueSummary;
import org.integratedmodelling.common.client.Environment;
import org.integratedmodelling.common.client.runtime.ClientSession;
import org.integratedmodelling.common.client.viewer.DisplayState.Result;
import org.integratedmodelling.common.configuration.Configuration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.lists.Escape;
import org.integratedmodelling.common.geospace.SpatialServices;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.State;
import org.integratedmodelling.common.model.runtime.Structure;
import org.integratedmodelling.common.space.IGeometricShape;
import org.integratedmodelling.common.utils.BrowserUtils;
import org.integratedmodelling.common.visualization.ColorMap;
import org.integratedmodelling.common.visualization.Histogram;
import org.integratedmodelling.common.visualization.Viewport;
import org.integratedmodelling.common.vocabulary.NS;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Point;

/**
 * Controls a web-based viewer through the view REST endpoints and websockets.
 * 
 * @author ferdinando.villa
 */
public class WebViewer extends AbstractViewer {

    boolean                     started               = false;

    /*
     * initial location of map window if no geolocation is available
     */
    public static final double  DEFAULT_LAT           = 34.297256;
    public static final double  DEFAULT_LON           = 27.579316;

    /*
     * TODO differentiate defaults by observation type
     */
    private static final String DEFAULT_FEATURE_LINECOLOR = "#ff0000";
    private static final String DEFAULT_FEATURE_FILLCOLOR = "#ffa500";
    private static final float DEFAULT_FEATURE_OPACITY = 1.0f;

    enum Action {
        FOCUS,
        ADD,
        REMOVE
    }

    /*
     * We could manage >1 views at some point.
     */
    ContextView     view     = null;
    Viewport        viewport = new Viewport(800, 800);
    private Display currentDisplay;
    
    boolean showNames = true;

    /**
     * A viewer requires a valid session with an engine controller that is managing a
     * modeling engine.
     * 
     * @param session
     */
    public WebViewer(ClientSession session) {
        this.session = session;
    }
    
    public void showNames(boolean b) {
        this.showNames = b;
    }

    /**
     * Get the current viewport for image and map generation. By default set to 800x800,
     * override this in situations where we can track the size of the display.
     * 
     * @return the viewport
     */
    public IViewport getViewport() {
        return viewport;
    }

    /**
     * Start the browser and set the started flag to true. Does not check the flag so we
     * can do it as many times as we want after closing the browser window.
     */
    public void start() {
        String url = KLAB.ENGINE.getUrl() + "/dataviewer/index.html?sessionId=" + session.getId();
        Coordinate location = SpatialServices.geolocate();
        if (location == null) {
            url += "&lat=" + DEFAULT_LAT + "&lon=" + DEFAULT_LON;
        } else {
            url += "&lat=" + location.y + "&lon=" + location.x;
        }
        BrowserUtils.startBrowser(url);
        this.started = true;
    }

    @Override
    public void show(IContext context) {
        if (context.isEmpty()) {
            return;
        }
        if (this.view == null || !this.view.getContext().getId().equals(context.getId())) {
            this.view = new ContextView(context);
        }
        display(context.getSubject(), Action.FOCUS, null, null);
    }

    @Override
    public void show(IObservation observation) {
        Result result = view.show(observation);
        if (result != Result.IGNORE) {
            display(observation, Action.ADD, result, null);
        }
    }
    
    @Override
    public void show(IObservation observation, IViewer.Display display) {
        Result result = view.show(observation);
        if (result != Result.IGNORE) {
            display(observation, Action.ADD, result, display);
        }
    }

    @Override
    public void focus(IDirectObservation observation) {
        if (view.setFocus(observation)) {
            display(observation, Action.FOCUS, null, null);
        }
    }

    @Override
    public void hide(IObservation observation) {
        Result result = view.hide(observation);
        if (result != Result.IGNORE) {
            display(observation, Action.REMOVE, result, null);
        }
    }

    private void display(IObservation observation, Action action, Result result, IViewer.Display forceView) {

        /*
         * switch to appropriate view. For now we only support one at a time.
         */
        IViewer.Display display = forceView == null ? getAppropriateDisplay(observation) : forceView;

        switch (display) {
        case DATAGRID:
            displayGrid((GridView) view.displayState.get(display), observation, action, result);
            break;
        case FLOWS:
            displayFlows((FlowView) view.displayState.get(display), observation, action, result);
            break;
        case MAP:
            displayMap((MapView) view.displayState.get(display), observation, action, result);
            break;
        case TIMELINE:
            displayTimeline((TimelineView) view.displayState.get(display), observation, action, result);
            break;
        case TIMEPLOT:
            displayTimeplot((TimeView) view.displayState.get(display), observation, action, result);
            break;
        default:
            // do nothing for the others.
            break;
        }

    }

    private Display getAppropriateDisplay(IObservation observation) {

        Display ret = Display.DATAGRID;

        if (observation.getScale().isSpatiallyDistributed()) {
            ret = Display.MAP;
        } else if (observation instanceof IState && observation.getScale().isTemporallyDistributed()) {
            ret = Display.TIMEPLOT;
        } else if (observation.getScale().getSpace() != null) {
            ret = Display.MAP;
        } else if (observation instanceof IEvent) {
            ret = Display.TIMELINE;
        }
        return ret;
    }

    private void displayTimeplot(TimeView timeData, IObservation observation, Action action, Result result) {

        send("switch-view", "plot");
        currentDisplay = Display.TIMEPLOT;

        if (!(observation instanceof IState) || observation.getScale().getTime() == null
                || observation.getScale().getTime().getMultiplicity() != observation.getScale()
                        .getMultiplicity()) {
            return;
        }

        StateData data = session.getEngineController().getStateData((IState) observation);

        if (data != null) {
            send("draw-plots", data.getLabel(), data.getTimes(), data.getVdata());
        }
    }

    private void displayTimeline(TimelineView timelineData, IObservation observation, Action action, Result result) {
        // TODO Auto-generated method stub
        send("switch-view", "timeline");
        currentDisplay = Display.TIMELINE;

    }

    private void displayMap(MapView mapData, IObservation observation, Action action, Result result) {

        send("switch-view", "map");
        currentDisplay = Display.MAP;

        ISpatialExtent space = observation.getScale().getSpace();
        if (action == Action.FOCUS && space != null) {
            view.show(observation);
            String shape = getShapeWKT(space.getShape().toString());
            send("set-context", space.getMinX(), space.getMinY(), space.getMaxX(), space.getMaxY(), shape);
        }

        String stateUrls = "";
        String stateNames = "";
        int[] xy = null;

        if (result == Result.OBJECT_DISTRIBUTED) {
            /*
             * States - we always redisplay them all
             */
            for (IState state : mapData.maps) {
                if (state.isSpatiallyDistributed()) {
                    String url = getStateMediaUrl(state);
                    if (xy == null) {
                        xy = viewport.getSizeFor(state.getSpace().getMaxX() - state.getSpace()
                                .getMinX(), state.getSpace().getMaxY() - state.getSpace().getMinY());
                    }
                    stateUrls += (stateUrls.isEmpty() ? "" : "@") + url;
                    stateNames += (stateNames.isEmpty() ? "" : "@") + ((State) state).getLabel();
                }
            }
            if (!stateUrls.isEmpty()) {
                send("show-map", stateUrls, stateNames, space.getMinX(), space.getMinY(), space
                        .getMaxX(), space.getMaxY(), xy[0], xy[1]);
            }
        }

        /*
         * new features
         */
        if (result == Result.OBJECT_AREA) {
            if (observation instanceof IDirectObservation) {
                if (action == Action.REMOVE) {
                    send("hide-feature", ((Observation) observation).getInternalId());
                } else {
                    ISpatialExtent spc = observation.getScale().getSpace();
                    String shp = getShapeWKT(spc.getShape().toString());
                    boolean isLine = shp.startsWith("LINE") || shp.startsWith("MULTILINE");
                    send("add-feature", 
                            shp, 
                            (showNames ? fixName(((IDirectObservation) observation)) : ""), 
                            ((Observation) observation).getInternalId(), 
                            (observation
                                    .getMetadata().contains(IMetadata.KLAB_LINE_COLOR)
                                            ? observation.getMetadata().getString(IMetadata.KLAB_LINE_COLOR)
                                            : DEFAULT_FEATURE_LINECOLOR),
                            (isLine ? null : (observation
                                    .getMetadata().contains(IMetadata.KLAB_FILL_COLOR)
                                            ? observation.getMetadata().getString(IMetadata.KLAB_FILL_COLOR)
                                            : DEFAULT_FEATURE_FILLCOLOR)),                            
                            (isLine ? null : (observation
                                    .getMetadata().contains(IMetadata.KLAB_OPACITY)
                                            ? observation.getMetadata().getFloat(IMetadata.KLAB_OPACITY)
                                            : DEFAULT_FEATURE_OPACITY)));
                }
            } else {
                // TODO these are rasters or other under a subject - should probably zoom
                // in
                // and display
            }
        }

        /*
         * new markers
         */
        if (result == Result.OBJECT_POINT) {
            if (action == Action.REMOVE) {
                send("hide-marker", ((Observation) observation).getInternalId());
            } else if (observation instanceof IDirectObservation) {
                ISpatialExtent spc = observation.getScale().getSpace();
                Point point = ((IGeometricShape) spc.getShape()).getStandardizedGeometry().getCentroid();
                send("add-marker", ((IDirectObservation) observation).getName(), ((Observation) observation)
                        .getInternalId(), point.getX(), point.getY());

            }
        }
    }

    private String fixName(IDirectObservation obs) {
        // TODO ensure we have options here instead of hard-coding behavior.
        return obs instanceof IRelationship ? "" : obs.getName();
    }

    private String getStateMediaUrl(IState state) {

        String ret = "/" + Configuration.MODELER_APPLICATION_ID
                + API.CONTEXT_GET_MEDIA.replace("{context}", ((Observation) state).getContext().getId())
                        .replace("{observation}", ((Observation) state).getInternalId())
                + ".png";
        ret += "?viewport=" + Escape.forURL(viewport.toString());
        ret += "&index=" + Escape.forURL(Scale.locatorsAsText(view.getLocators()));
        return ret;
    }

    private void displayFlows(FlowView flowData, IObservation observation, Action action, Result result) {
        send("switch-view", "flow");
        send("draw-flows", ((Structure)((ISubject)observation).getStructure(Environment.get().getCurrentTime())).getFlows());
        currentDisplay = Display.FLOWS;
    }

    private void displayGrid(GridView gridData, IObservation observation, Action action, Result result) {
        send("switch-view", "data");
        currentDisplay = Display.DATAGRID;
    }

    private void send(String string, Object... args) {
        session.getEngineController().sendViewerCommand(new ViewerCommand(string, args));
    }

    private String getShapeWKT(String string) {
        if (string.startsWith("EPSG:")) {
            int idx = string.indexOf(' ');
            if (idx > -1) {
                return string.substring(idx);
            }
        }
        return string;
    }

    /**
     * True if this was started at least once. User may have closed the browser window.
     * 
     * @return true if start() was called and completed successfully
     */
    public boolean hasStarted() {
        return started;
    }

    @Override
    public Display getCurrentDisplay() {
        return currentDisplay;
    }

    @Override
    public Display setDisplay(Display display) {
        this.currentDisplay = display;
        return this.currentDisplay;
    }

    @Override
    public void cycleView() {
        if (currentDisplay == Display.MAP) {
            send("cycle-maps");
        }
    }

    @Override
    public IState getVisibleSpatialState() {
        MapView mapData = (MapView) view.displayState.get(Display.MAP);
        if (mapData != null && mapData.maps.size() > 0) {
            return mapData.maps.getFirst();
        }
        return null;
    }

    @Override
    public List<IState> getVisibleSpatialStates() {
        List<IState> ret = new ArrayList<>();
        MapView mapData = (MapView) view.displayState.get(Display.MAP);
        if (mapData != null && mapData.maps.size() > 0) {
            for (Iterator< IState>it = mapData.maps.descendingIterator(); it.hasNext();) {
                ret.add(it.next());
            }
        }
        return ret;
    }

    @Override
    public void setOpacity(IObservation observation, double opacity) {
        // TODO Auto-generated method stub
        System.out.println("SET OPACITY!");
    }

    @Override
    public boolean setContextEditMode(boolean on) {
        send("draw-mode", on ? "on" : "off");
        return on;
    }

    @Override
    public void setContextEditMode(DrawMode mode) {
        send("draw-mode", mode.name());
    }

    @Override
    public void clearAllStates() {
        if (view.displayState.containsKey(Display.MAP)) {
            send("remove-maps");
        }
        /*
         * TODO the rest
         */
    }

    @Override
    public IView getView(Display display) {
        return view.displayState.get(display);
    }

    @Override
    public IView getView() {
        return view;
    }

    @Override
    public String getLabel(IObservation observation) {

        if (observation instanceof IProcess) {
            return NS.getDisplayName(observation.getObservable().getType());
        }
        if (observation instanceof IDirectObservation) {
            return ((IDirectObservation) observation).getName();
        }
        return ((State) observation).getLabel();
    }

    @Override
    public void resetView() {
        // TODO Auto-generated method stub
        System.out.println("RESET VIEW!");
    }

    @Override
    public void locate(Locator... locators) {
        boolean timeChanged = view.locate(locators);
        if (timeChanged && currentDisplay == Display.MAP) {
            clearAllStates();
            for (IState zio : view.getMapView().maps) {
                show(zio);
            }
        }
    }

    @Override
    public void clear() {
        // TODO clear everything, not just the map;
        // FIXME map may not even be there.
        send("reset-map");
    }

    @Override
    public Pair<IColormap, IHistogram> getStateSummary(IState state) {
        StateSummary ssum = session.getEngineController().getStateSummary(state, view.getLocators());
        IHistogram histogram = ssum == null ? null
                : KLAB.MFACTORY.adapt(ssum.getHistogram(), Histogram.class);
        IColormap colormap = ssum == null ? null : KLAB.MFACTORY.adapt(ssum.getColormap(), ColorMap.class);
        return new Pair<>(colormap, histogram);
    }

    public ValueSummary getValueSummary(IState state, long idx) {
        return session.getEngineController().getValueSummary(state, idx);
    }

    public void redraw() {
        send("redraw-map");
    }

    // case "set-space":
    // showContext(message.args[0], message.args[1], message.args[2],
    // message.args[3],
    // message.args[4]);
    // break;
    // break;
    // case "show-map":
    // showMap(
    // message.args[0], // URL array
    // message.args[1], // ID array
    // message.args[2], // minX
    // message.args[3], // minY
    // message.args[4], // maxX
    // message.args[5], // maxY
    // message.args[6], // image X size
    // message.args[7]);// image Y size
    // break;
    // case "draw-mode":
    // drawMode(message.args[0] == "on");
    // break;
    // case "remove-map":
    // removeMap(message.args[0]);
    // break;
    // case "remove-maps":
    // removeAllMaps();
    // break;
    // case "set-opacity":
    // setOpacity(message.args[0], message.args[1]);
    // break;
    // case "reset-map":
    // resetMap();
    // break;
    // case "draw-flows":
    // // TODO params
    // drawFlows();
    // break;
    // case "draw-plots":
    // // TODO params
    // drawPlots();
    // break;
    // case "draw-timeline":
    // // TODO params
    // drawTimeline();
    // break;
    // case "switch-view":
    // switchView(message.args[0]);
    // break;
    // case "add-feature":
    // addFeature(
    // message.args[0], // shape wkt
    // message.args[1], // name
    // message.args[2], // id
    // (message.args.length > 3 ? message.args[3] : "#ff0000"), // color
    // (message.args.length > 4 ? message.args[4] : null), // fillColor
    // (message.args.length > 5 ? message.args[5] : 1.0) // opacity
    // );
    // break;
    // case "add-marker":
    // addMarker(message.args[0], message.args[1], message.args[2],
    // message.args[3]);
    // break;
    // case "show-marker":
    // showMarker(message.args[0]);
    // break;
    // case "hide-marker":
    // hideMarker(message.args[0]);
    // break;
    // case "show-feature":
    // showFeature(message.args[0]);
    // break;
    // case "hide-feature":
    // hideFeature(message.args[0]);
    // break;
    // case "show-concepts":
    // describeConcepts(message.args[0], "semquery");
    // switchView("query");
    // break;
    // case "show-subjects":
}
