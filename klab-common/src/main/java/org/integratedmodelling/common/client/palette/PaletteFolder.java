package org.integratedmodelling.common.client.palette;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.utils.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(exclude={"items", "description", "status"})
public @Data class PaletteFolder {

    private String            name;
    private String            description;
    private String            status;
    private List<PaletteItem> items = new ArrayList<>();
    private boolean           readOnly;

    @JsonIgnore
    transient private String            internalId;
    @JsonIgnore
    transient private Palette           palette;

    
    void initialize(Palette palette, String paletteId) {
        this.palette = palette;
        internalId = paletteId + "/" + NameGenerator.shortUUID();
        for (PaletteItem item : items) {
            item.initialize(this, internalId);
        }
    }

    public void addItem(PaletteItem item) {
        if (item.getInternalId() != null) {
            item.detach();
        }
        item.initialize(this, internalId);
        items.add(item);
    }

    static PaletteItem findItem(List<PaletteItem> list, String[] paths) {
        String id = StringUtils.join(paths, "/");
        for (PaletteItem target : list) {
            if (target.getInternalId().equals(id)) {
                return target;
            }
            PaletteItem child = findItem(target.getChildren(), paths);
            if (child != null) {
                return child;
            }
        }
        return null;
    }

    List<PaletteItem> findListContaining(List<PaletteItem> list, PaletteItem item) {

        for (PaletteItem target : list) {
            if (target.getInternalId().equals(item.getInternalId())) {
                return items;
            }
            List<PaletteItem> children = findListContaining(target.getChildren(), item);
            if (children != null) {
                return children;
            }
        }
        return null;
    }

}
