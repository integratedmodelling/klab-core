/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.integratedmodelling.api.data.IColumn;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.data.ITableSet;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Wraps an Excel spreadsheet. Columns must have headers and be well-formed. Each sheet mapped to a 
 * table.
 * 
 * @author Ferd
 *
 */
public class ExcelTableSet implements ITableSet {

    File                            _file       = null;
    private XSSFWorkbook            _workbook;
    private HashMap<String, ITable> _tablesById = new HashMap<String, ITable>();
    private ArrayList<ITable>       _tables     = new ArrayList<ITable>();

    class ExcelTable implements ITable {

        int _index;
        int _start, _end;

        private HashMap<String, IColumn> _columns;
        private ArrayList<String>        _columnIDs = new ArrayList<String>();

        class ExcelColumn implements IColumn {

            String       _name;
            int          _idx;
            List<Object> values = null;

            public ExcelColumn(Cell cell) {
                _name = cell.getStringCellValue().trim();
                _idx = cell.getColumnIndex();
            }

            @Override
            public String getName() {
                return _name;
            }

            @Override
            public int getValueCount() {
                // TODO check if that's the actual count
                return _workbook.getSheetAt(_index).getLastRowNum();
            }

            @Override
            public List<Object> getValues() {
                if (values == null) {
                    values = new ArrayList<Object>();
                    for (int i = 0; i < _workbook.getSheetAt(_index).getLastRowNum(); i++) {
                        XSSFRow row = _workbook.getSheetAt(_index).getRow(i);
                        // TODO use type and add specific object?
                        values.add(getCellValue(row.getCell(_idx)));
                    }
                }
                return values;
            }

            @Override
            public Object getValue(Object index) {
                if (index instanceof Number) {
                    return getValues().get(((Number) index).intValue());
                }
                // TODO
                return null;
            }

        }

        public ExcelTable(int i) {
            _index = i;
        }

        @Override
        public String getName() {
            return null;
        }

        @Override
        public Collection<IColumn> getColumns() {
            if (_columns == null) {
                _columns = new HashMap<String, IColumn>();
                XSSFRow headers = _workbook.getSheetAt(_index).getRow(_workbook.getSheetAt(_index)
                        .getFirstRowNum());
                for (Iterator<Cell> cit = headers.cellIterator(); cit.hasNext();) {
                    Cell cell = cit.next();
                    _columns.put(cell.getStringCellValue().trim(), new ExcelColumn(cell));
                    _columnIDs.add(cell.getStringCellValue().trim());
                }
            }
            return _columns.values();
        }

        @Override
        public IColumn getColumn(String columnName) {
            if (_columns == null) {
                getColumns();
            }
            return _columns.get(columnName);
        }

        @Override
        public List<Object> lookup(int columnIndex, Object... values) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<Object> lookup(String column, IExpression match, Map<String, Object> parameters, IMonitor monitor)
                throws KlabException {
            List<Object> ret = new ArrayList<Object>();
            for (int i = _workbook.getSheetAt(_index).getFirstRowNum() + 1; i < _workbook.getSheetAt(_index)
                    .getLastRowNum(); i++) {
                XSSFRow row = _workbook.getSheetAt(_index).getRow(i);

                int n = 0;
                HashMap<String, Object> parms = new HashMap<String, Object>();
                parms.putAll(parameters);

                Object val = null;
                for (Iterator<Cell> cit = row.cellIterator(); cit.hasNext() && n < _columnIDs.size();) {
                    Cell cell = cit.next();
                    if (!_columnIDs.get(n).equals(column)) {
                        parms.put(fixId(_columnIDs.get(n)), getCellValue(cell));
                    } else {
                        val = getCellValue(cell);
                    }
                    n++;
                }
                Object obj = match.eval(parms, monitor, (IConcept[]) null);
                if (!(obj instanceof Boolean)) {
                    throw new KlabValidationException("expression " + match
                            + " does not evaluate to true/false");
                }
                if ((Boolean) obj) {
                    ret.add(val);
                }
            }
            return ret;
        }

        private String fixId(String string) {
            String ret = string.replace(' ', '_');
            ret = ret.replace('(', '_');
            ret = ret.replace('[', '_');
            ret = ret.replace(']', '_');
            ret = ret.replace('{', '_');
            ret = ret.replace('}', '_');
            return ret.replace(')', '_');
        }

        @Override
        public List<Map<String, Object>> lookup(IExpression expression) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<String, Object> getMapping(String keyColumnName, String valueColumnName) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public String sanitizeKey(Object key) {
            return AbstractTableSet.sanitizeKey(key);
        }

    }

    public ExcelTableSet(File file) {
        _file = file;
    }

    @Override
    public Collection<ITable> getTables() {

        if (_workbook == null) {
            open();
        }
        return _tables;
    }

    private void open() {

        try {
            _workbook = new XSSFWorkbook(new FileInputStream(_file));
            for (int i = 0; i < _workbook.getNumberOfSheets(); i++) {
                ITable res = new ExcelTable(i);
                _tablesById.put(_workbook.getSheetName(i), res);
                _tables.add(res);
            }
        } catch (Exception e) {
            throw new KlabRuntimeException(e);
        }

    }

    @Override
    public ITable getTable(String tableName) {
        if (_workbook == null) {
            open();
        }
        return _tablesById.get(tableName);
    }

    @Override
    public String getName() {
        if (_workbook == null) {
            open();
        }
        return _workbook.getName("unnamed").getNameName();
    }

    public static Object getCellValue(Cell cell) {

        if (cell == null)
            return null;

        switch (cell.getCellType()) {
        case Cell.CELL_TYPE_BOOLEAN:
            return cell.getBooleanCellValue();
        case Cell.CELL_TYPE_NUMERIC:
            return cell.getNumericCellValue();
        case Cell.CELL_TYPE_STRING:
            return cell.getStringCellValue().trim();
        case Cell.CELL_TYPE_BLANK:
            return "";
        case Cell.CELL_TYPE_FORMULA:
            throw new KlabRuntimeException("cannot evaluate formulas in spreadsheet.");
        }
        return null;
    }
}
