/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.integratedmodelling.api.data.IColumn;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.data.ITableSet;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Handy accessor that can be initialized through a string and applied to
 * a table set to retrieve the value matching an input. Can apply multiple
 * searches to >1 table.
 * 
 * String is one or more space-separated tokens like
 * 	
 * 	table/column
 * 
 * each of which is a getter for the input, searched for in the table/column. Successive
 * getters are fed into  left to right.
 * 
 * @author Ferd
 *
 */
public class TableAccessor {

    List<IColumn> _columns = new ArrayList<IColumn>();

    /**
     * Parse the definition and prepare any necessary index. Returns 
     * true iif tables and columns are recognized for the given tableset.
     *  
     * @param definition
     * @return
     */
    boolean parse(String definition, ITableSet tableset) {

        String[] tokens = definition.trim().split("\\s+");

        if (tokens.length < 1)
            return false;

        for (String t : tokens) {
            String[] ts = t.split("/");
            if (ts.length != 2)
                throw new KlabRuntimeException("wrong table accessor format: " + definition);

            ITable table = tableset.getTable(ts[0]);
            if (table == null)
                return false;

            IColumn column = table.getColumn(ts[1]);
            if (column == null)
                return false;

            _columns.add(column);
        }

        return true;
    }

    /**
     * Apply the implemented retrieval strategy to the tableset. Return
     * the result, or null if not available.
     * 
     * @param key
     * @return the retrieved object, or null
     */
    public Object get(Object key) {

        if (key == null)
            if (key instanceof Double && Double.isNaN((Double) key))
                return key;

        if (key instanceof Double) {
            // we don't use double keys, turn into integer
            // TODO check if this works - may need a string
            key = ((Double) key).intValue();
        }

        if (_columns.size() < 1)
            return null;

        Object ret = key;
        Iterator<IColumn> it = _columns.iterator();

        while (ret != null && it.hasNext()) {
            ret = it.next().getValue(ret);
        }

        return ret;
    }

}
