/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.data.IColumn;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.monitoring.IMonitor;

public class LookupTable implements ITable /* , IMonitorable */ {

    // IMonitor _monitor;
    String name = "";

    protected ArrayList<IClassifier[]> rows    = new ArrayList<>();
    protected List<String>             headers = null;

    @Override
    public List<Object> lookup(int columnIndex, Object... values) {

        for (IClassifier[] row : rows) {
            boolean ok = true;
            for (int i = 0; i < values.length; i++) {
                int cind = i < columnIndex ? i : i + 1;
                if (!row[cind].classify(values[i])) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                return Collections.singletonList(row[columnIndex].asValue());
            }
        }

        return null;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Collection<IColumn> getColumns() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String sanitizeKey(Object key) {
        return AbstractTableSet.sanitizeKey(key);
    }

    @Override
    public IColumn getColumn(String columnName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Object> lookup(String columnId, IExpression values, Map<String, Object> parameters, IMonitor monitor) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Map<String, Object>> lookup(IExpression expression) {
        // TODO Auto-generated method stub
        return null;
    }

    // @Override
    // public void setMonitor(IMonitor monitor) {
    // // TODO Auto-generated method stub
    //
    // }

    @Override
    public Map<String, Object> getMapping(String keyColumnName, String valueColumnName) {
        // TODO Auto-generated method stub
        return null;
    }

}
