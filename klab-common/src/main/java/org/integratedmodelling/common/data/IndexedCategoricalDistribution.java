/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Random;

import org.integratedmodelling.api.data.IProbabilityDistribution;

/*
 * nothing but a double array with appropriate hash methods
 */
public class IndexedCategoricalDistribution implements Comparable<IndexedCategoricalDistribution>, IProbabilityDistribution {

		static Random rnGenerator = null;
    public double[] data = null;

    /*
     * either of these should be non-null: if ranges, this is discretizing a continuous
     * distribution and the mean will be computed by using the ranges as values for each
     * category. If classes, this is a probabilistic classification outcome and the "mean"
     * will be the most likely class.
     */
    private double[] ranges = null;

    public IndexedCategoricalDistribution(int n) {
        data = new double[n];
    }

    /**
     * Create from existing data. Note: will store the array, not copy it, so
     * don't reuse the same array to create different objects.
     * @param data
     * @param ranges 
     */
    public IndexedCategoricalDistribution(double[] data, double[] ranges) {
        this.data = data;
        this.ranges = ranges;
    }
    
    public double draw() {
    	
    	if (ranges == null) {
    		return 0;
    	}
    	
    	if (rnGenerator == null) {
    		rnGenerator = new Random();
    	}
    	double d = rnGenerator.nextDouble();
    	int bin = (int)((ranges.length - 1) * d);
    	return d*(ranges[bin+1] - ranges[bin]) + ranges[bin];
    }

    public IndexedCategoricalDistribution(double[] data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(data);
        return result;
    }

    @Override
    public boolean equals(Object obj) {

        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof IndexedCategoricalDistribution)) {
            return false;
        }
        IndexedCategoricalDistribution other = (IndexedCategoricalDistribution) obj;
        if (!Arrays.equals(data, other.data) || !Arrays.equals(ranges, other.ranges)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "[m=" + NumberFormat.getInstance().format(getMean()) + "]";
    }

    public void setRanges(double[] distributionBreakpoints) {
        this.ranges = distributionBreakpoints;
    }

    @Override
    public double[] getData() {
        return this.data;
    }

    @Override
    public double[] getRanges() {
        return this.ranges;
    }

    /**
     * Only returns a proper mean if ranges isn't null; otherwise the value returned is the
     * index of the most abundant class.
     * @return the mean
     */
    @Override
    public double getMean() {

        double ret = 0.0;
        if (ranges != null) {
            for (int i = 0; i < data.length; i++) {
                double midpoint = (this.ranges[i] + (this.ranges[i + 1] - this.ranges[i]) / 2);
                ret += midpoint * data[i];
            }
        } else {

            int idx = 0;
            double dd = data[0];

            /*
             * index of the most likely class
             */
            for (int i = 1; i < data.length; i++) {
                if (data[i] > dd)
                    idx = i;
            }

            ret = idx;
        }
        return ret;
    }

    @Override
    public int compareTo(IndexedCategoricalDistribution o) {
        return ((Double) getMean()).compareTo(o.getMean());
    }

    /*
     * return the coefficient of variation if we have ranges, or the shannon index if
     * we don't.
     */
    @Override
    public double getUncertainty() {

        double ret = 0.0;
        if (ranges != null) {
            double mu = 0.0, mu2 = 0.0;
            for (int i = 0; i < data.length; i++) {
                double midpoint = (this.ranges[i] + (this.ranges[i + 1] - this.ranges[i]) / 2);
                mu += midpoint * data[i];
                mu2 += midpoint * midpoint * data[i];
            }
            ret = Math.sqrt(mu2 - (mu * mu));
            ret = mu == 0.0 ? Double.NaN : (ret / mu);
        } else {
            double sh = 0.0;
            int nst = 0;
            for (int i = 0; i < data.length; i++) {
                sh += data[i] * Math.log(data[i]);
                nst++;
            }
            ret = (sh / Math.log(nst)) * -1.0;
        }
        return ret;
    }

    /*
     * these enable lots of Groovy niceties.
     * TODO see if we can integrate Gary's package to handle actual distributions and
     * return distributions when the operands are both distributions.
     */
    public double asDouble() {
        return getMean();
    }

    public float asFloat() {
        return (float) getMean();
    }

    public Object plus(Object o) {

        if (o instanceof IndexedCategoricalDistribution) {
            return getMean() + ((IndexedCategoricalDistribution) o).getMean();
        } else if (o instanceof Number) {
            return getMean() + ((Number) o).doubleValue();
        }
        return getMean();
    }

    public Object minus(Object o) {

        if (o instanceof IndexedCategoricalDistribution) {
            return getMean() - ((IndexedCategoricalDistribution) o).getMean();
        } else if (o instanceof Number) {
            return getMean() - ((Number) o).doubleValue();
        }
        return getMean();
    }

    public Object multipy(Object o) {

        if (o instanceof IndexedCategoricalDistribution) {
            return getMean() * ((IndexedCategoricalDistribution) o).getMean();
        } else if (o instanceof Number) {
            return getMean() * ((Number) o).doubleValue();
        }
        return getMean();
    }

    public Object div(Object o) {

        if (o instanceof IndexedCategoricalDistribution) {
            return getMean() / ((IndexedCategoricalDistribution) o).getMean();
        } else if (o instanceof Number) {
            return getMean() / ((Number) o).doubleValue();
        }
        return getMean();
    }
}
