package org.integratedmodelling.common.data;

import java.util.List;

import org.integratedmodelling.api.data.IDataService;
import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * Bean implementing the IDataService interface.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class DataService implements IDataService, IModelBean {
    String       name;
    String       key;
    String       type;
    String       url;
    List<String> authenticationCredentials;
}
