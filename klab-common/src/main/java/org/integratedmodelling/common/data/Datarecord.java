/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.integratedmodelling.api.data.IDataAsset;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.DataSource;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Used to store data source access details at the server side, and cached at
 * the client side. Allows using a simple URI to refer to a data source without
 * having to consider the type and protocol of access, and allowing access
 * control at the server side (all sources can only be cached at the client side
 * if they are accessible to the client user).
 * 
 * 
 * Bears much relationship with the LSID concept, but no need to adopt a more
 * complex and deprecated standard right now.
 * 
 * @author Ferd
 *
 */
public class Datarecord implements IDataAsset, NetworkDeserializable, NetworkSerializable {

	private String id;
	private String creator;
	private Set<String> types = new HashSet<>();
	private Set<String> allowedGroups = new HashSet<>();
	private DataSource bean;

	@Override
	public String toString() {
		String ret = "[DataRecord type=" + StringUtils.join(types, ",") + ", " + id + " bean = <"
				+ (bean == null ? "NULL" : bean.toString()) + ">]";
		return ret;
	}

	@Override
	public void deserialize(IModelBean object) {
		if (!(object instanceof DataSource)) {
			throw new KlabRuntimeException(
					"cannot deserialize a " + object.getClass().getCanonicalName() + " into a Datarecord");
		}

		DataSource bean = (DataSource) object;

		this.bean = bean;
		this.types.add(bean.getType());
		this.id = bean.getId();
		this.creator = bean.getCreator();
	}

	@Override
	public String getCreatorUsername() {
		return creator;
	}

	@Override
	public Collection<String> getAllowedGroups() {
		return allowedGroups;
	}

	@Override
	public URL getResourceUrl(String serviceType) {
		return bean.getUrl();
	}

	@Override
	public String getResourceId() {
		return bean.getName();
	}

	@Override
	public String getResourceNamespace() {
		return bean.getNamespace();
	}

	@Override
	public Collection<String> getResourceTypes() {
		return types;
	}

	@Override
	public String getServiceKey() {
		return bean.getServerKey();
	}

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {
        return (T) bean;
    }

}
