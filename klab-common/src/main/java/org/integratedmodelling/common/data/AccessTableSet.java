/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.data.IColumn;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.data.ITableSet;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.KlabIOException;

import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Row;

/**
 * Wraps a MS ACCESS file to provide access to its tables. Only supports Access 2000-2007 due
 * to restrictions in the underlying library (Apache Jackcess). 
 * 
 * @author Ferd
 *
 */
public class AccessTableSet extends AbstractTableSet implements ITableSet {

    File     _file;
    Database _db;
    String   _name;

    public AccessTableSet(File file) {
        super(file);
        _file = file;
        _name = MiscUtilities.getFileBaseName(file.toString());
    }

    class Table implements ITable {

        String                                 _name;
        com.healthmarketscience.jackcess.Table _table;

        class Column implements IColumn {

            String                  _name;
            // TODO substitute with a configurable cache; if persistent, load on
            // creation unless file has changed.
            HashMap<Object, Object> _cache = new HashMap<Object, Object>();

            Column(String name) {
                _name = name;
            }

            @Override
            public String getName() {
                return _name;
            }

            @Override
            public int getValueCount() {
                try {
                    return getTable().getRowCount();
                } catch (KlabIOException e) {
                    // shouldn't happen
                }
                return 0;
            }

            @Override
            public Iterable<Object> getValues() {

                ArrayList<Object> ret = new ArrayList<Object>();
                for (Row row : _table) {
                    ret.add(row.get(_name));
                }
                return ret;
            }

            @Override
            public Object getValue(Object index) {

                if (!_cache.containsKey(_name + "_" + index)) {
                    cacheValues();
                }
                return _cache.get(_name + "_" + index);
            }

            private void cacheValues() {
                for (Row row : _table) {
                    for (String s : row.keySet()) {
                        _cache.put(_name + "_" + s, row.get(s));
                    }
                }
            }
        }

        public Table(String s) {
            _name = s;
        }

        @Override
        public String getName() {
            return _name;
        }

        @Override
        public Collection<IColumn> getColumns() {

            ArrayList<IColumn> ret = new ArrayList<IColumn>();
            try {
                for (com.healthmarketscience.jackcess.Column cc : getTable().getColumns()) {
                    ret.add(new Column(cc.getName()));
                }
            } catch (KlabIOException e) {
                ret.clear();
            }
            return ret;
        }

        @Override
        public IColumn getColumn(String columnName) {

            boolean ok = false;
            try {
                for (com.healthmarketscience.jackcess.Column cc : getTable().getColumns()) {
                    if (cc.getName().equals(columnName)) {
                        ok = true;
                        break;
                    }
                }
            } catch (KlabIOException e) {
            }
            return ok ? new Column(columnName) : null;
        }

        private com.healthmarketscience.jackcess.Table getTable() throws KlabIOException {

            if (_table == null) {
                try {
                    _table = getDb().getTable(_name);
                } catch (IOException e) {
                    throw new KlabIOException(e);
                }
            }
            return _table;
        }

        @Override
        public List<Object> lookup(int columnIndex, Object... values) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<Object> lookup(String columnId, IExpression match, Map<String, Object> parameters, IMonitor monitor) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public List<Map<String, Object>> lookup(IExpression expression) {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<String, Object> getMapping(String keyColumnName, String valueColumnName)
                throws KlabIOException {

            if (!hasMappingFor(_name, keyColumnName, valueColumnName)) {

                Map<String, Object> newmap = getMappingFor(_name, keyColumnName, valueColumnName);

                KLAB.info("caching values for mapping " + keyColumnName + " -> " + valueColumnName
                        + " in table " + _name);

                for (Row row : getTable()) {
                    Object key = row.get(keyColumnName);
                    Object val = row.get(valueColumnName);
                    if (key != null && val != null) {
                        newmap.put(key.toString(), val);
                    }
                }
                db.commit();
                return newmap;
            }
            return getMappingFor(_name, keyColumnName, valueColumnName);
        }

        @Override
        public String sanitizeKey(Object key) {
            return AbstractTableSet.sanitizeKey(key);
        }

    }

    @Override
    public Collection<ITable> getTables() {

        ArrayList<ITable> ret = new ArrayList<ITable>();

        try {
            for (String s : getDb().getTableNames()) {
                ret.add(new Table(s));
            }
        } catch (Exception e) {
            ret.clear();
        }
        return ret;
    }

    @Override
    public ITable getTable(String tableName) {
        return new Table(tableName);
    }

    @Override
    public String getName() {
        return _name;
    }

    private Database getDb() throws KlabIOException {

        if (_db == null) {
            try {
                _db = DatabaseBuilder.open(_file);
                Runtime.getRuntime().addShutdownHook(new Thread() {
                    @Override
                    public void run() {
                        try {
                            if (_db != null) {
                                _db.close();
                            }
                        } catch (IOException e) {
                        }
                    }
                });
            } catch (IOException e) {
                throw new KlabIOException(e);
            }
        }
        return _db;
    }

    public static void main(String[] args) throws Exception {

        AccessTableSet set = new AccessTableSet(new File("C:\\HWSD.mdb"));

        for (ITable table : set.getTables()) {
            System.out.println(table.getName());
            for (IColumn column : table.getColumns()) {
                System.out.println("\t" + column.getName() + " [" + column.getValueCount() + " values]");
                // int i = 0;
                // for (Object o : column.getValues()) {
                // System.out.println(column.getName() + "[" + (i++) + "] = " + o);
                // }
            }
        }

        /**
         * try out a cached mapping.
         */
        ITable hwsd_data = set.getTable("HWSD_DATA");
        Map<String, Object> vals = hwsd_data.getMapping("ID", "SU_SYM85");
        for (String id : vals.keySet()) {
            System.out.println("   " + id + " -> " + vals.get(id));
        }
    }
}
