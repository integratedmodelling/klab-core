package org.integratedmodelling.common.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.utils.MiscUtilities;

/**
 * Class for future use that collects info about all data encountered, including errors
 * and locations in projects. Also provide utilities for import, organization,
 * (supervised) annotation and creation of .k sidecar files for the future semantic
 * crawler.
 * 
 * @author fvilla
 *
 */
public class DataManager {

    static public File UNKNOWN_FILE = new File("unknown.unk");

    /*
     * TODO add excel and more raster/vector formats
     */
    static Set<String> RECOGNIZED_EXTENSIONS;

    static {
        RECOGNIZED_EXTENSIONS = new HashSet<>();
        RECOGNIZED_EXTENSIONS.add("shp");
        RECOGNIZED_EXTENSIONS.add("tif");
        RECOGNIZED_EXTENSIONS.add("tiff");
        RECOGNIZED_EXTENSIONS.add("csv");
    }

    /**
     * Arrange a collection of files into known dataset descriptors, returning for each
     * recognized dataset a pair including the main file (e.g. the .shp for a set of
     * shapefile-related files) and the full set of "sidecar" files (including the main
     * one).
     * 
     * Any files that are not recognized as data will be returned in a collection
     * associated to the {@link #UNKNOWN_FILE} main file, which should be checked for in
     * the returned output. If existing, the unknown file will always the first element in
     * the returned list, so comparing it as follows:
     * 
     * <pre>
        List<Pair<File, Collection<File>>> payload = DataManager.arrangeFiles(files);
        if (payload.isEmpty()) {
            return;
        }
        if (payload.get(0).getFirst().equals(DataManager.UNKNOWN_FILE)) {
            // error, unknown files
            return;
        }
     * </pre>
     * 
     * will suffice.
     * 
     * For now just limited to recognizing sidecar files (with the same name and different
     * extension), which works for all the cases I know of.
     * 
     * @param files
     * @return
     */
    public static List<Pair<File, Collection<File>>> arrangeFiles(Collection<File> files) {

        List<Pair<File, Collection<File>>> ret = new ArrayList<>();
        Set<File> known = new HashSet<>();

        /*
         * get the mains first, add empty collection
         */
        Pair<File, Collection<File>> unknown = null;
        for (File f : files) {
            if (RECOGNIZED_EXTENSIONS.contains(MiscUtilities.getFileExtension(f))) {
                ret.add(new Pair<>(f, new ArrayList<>()));
                known.add(f);
            }
        }

        /*
         * add all sidecars of main including main itself
         */
        for (Pair<File, Collection<File>> p : ret) {
            String name = MiscUtilities.getFileBaseName(p.getFirst());
            for (File f : files) {
                String fname = MiscUtilities.getFileBaseName(f);
                // second check catches things like file.shp.xml
                if (fname.equals(name) || fname.startsWith(name + ".")) {
                    p.getSecond().add(f);
                    known.add(f);
                }
            }
        }

        /*
         * record all unknowns
         */
        for (File f : files) {
            if (!known.contains(f)) {
                if (unknown == null) {
                    unknown = new Pair<>(UNKNOWN_FILE, new ArrayList<>());
                }
                unknown.getSecond().add(f);
            }
        }

        if (unknown != null) {
            ret.add(0, unknown);
        }

        return ret;
    }

}
