/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// author:  Robert Keller
// purpose: Polylist enumeration class of polya package

package org.integratedmodelling.common.data.lists;
/**
  *  PolylistEnum is an enumeration class for the class Polylist.  It 
  *  implements the interface java.util.Enumeration, i.e. the methods:
  *  hasMoreElements() and nextElement().  
 **/

public class PolylistEnum implements java.util.Enumeration<Object> {
    PolyList L; // current list

    /**
      *  PolylistEnum constructs a PolylistEnum from a Polylist.
     **/

    public PolylistEnum(PolyList L) // constructor
    {
        this.L = L;
    }

    /**
      *  hasMoreElements() indicates whether there are more elements left in 
      *  the enumeration.
     **/

    @Override
    public boolean hasMoreElements() {
        return L.nonEmpty();
    }

    /**
      *  nextElement returns the next element in the enumeration.
     **/
    @Override
    public Object nextElement() {
        if (L.isEmpty())
            throw new java.util.NoSuchElementException("No next element in Polylist");

        Object result = L.first();
        L = (PolyList) L.rest();
        return result;
    }
}
