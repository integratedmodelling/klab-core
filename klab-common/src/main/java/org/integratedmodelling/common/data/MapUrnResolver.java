package org.integratedmodelling.common.data;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.data.IDataAsset;
import org.integratedmodelling.common.beans.DataSource;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.UrnResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The simplest possible URN resolver, to be used when we don't have anything better. Scan
 * this package to inject this one into the API.GET_RESOURCE handler.
 * 
 * If a datadir/data/datasources.json file is found when initialize() is called, this will
 * read it and load the contents.
 * 
 * Ignores security, too.
 * 
 * @author ferdinando.villa
 *
 */
@Component
public class MapUrnResolver implements UrnResolver {

    @Autowired
    ObjectMapper objectMapper;

    boolean initialized = false;

    private Map<String, IDataAsset> assets = new HashMap<>();

    /**
     * Add a data resource to be resolved later.
     * 
     * @param urn
     * @param data
     */
    public void add(String urn, IDataAsset data) {
        assets.put(urn, data);
    }

    @Override
    public IDataAsset resolveUrn(String urn, ConnectionAuthorization authorization, IResourceConfiguration resourceConfiguration) {

        /*
         * painful but necessary, to avoid Spring boot calling this too early.
         */
        initialize();

        IDataAsset ret = assets.get(urn);

        if (ret != null) {
            if (ret.getAllowedGroups().size() > 0) {
                if (authorization == null) {
                    ret = null;
                } else {
                    Set<String> ugr = new HashSet<>(ret.getAllowedGroups());
                    ugr.retainAll(authorization.getUserGroups());
                    if (ugr.size() == 0) {
                        ret = null;
                    }
                }
            }
        }
        return ret;
    }

    /**
     * If a JSON file is present with initial content, load it.
     */
    public void initialize() {

        if (!initialized) {
            initialized = true;
            if (objectMapper != null) {
                File source = new File(KLAB.CONFIG.getDataPath() + File.separator + "data" + File.separator
                        + "datasources.json");
                if (source.exists()) {
                    try {
                        Data data = objectMapper.readValue(source, Data.class);
                        for (String urn : data.keySet()) {
                            assets.put(urn, KLAB.MFACTORY.adapt(data.get(urn), Datarecord.class));
                        }
                    } catch (IOException e) {
                        KLAB.error(e);
                    }
                }
            }

        }

    }

    static public class Data extends HashMap<String, DataSource> {
    }

}
