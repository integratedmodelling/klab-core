/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.data;

import java.io.IOException;
import java.io.StreamTokenizer;
import java.io.StringReader;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.kim.expr.GroovyExpression;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * A numeric interval parsed from conventional syntax (e.g. "[12 34)" ). Can be
 * set to be open or close on each end and check if a number is contained in it.
 * 
 * @author Ferdinando Villa
 *
 */
public class NumericInterval implements IParseable, ITopologicallyComparable<NumericInterval>, NetworkSerializable, NetworkDeserializable {

	double lowerBound = 0.0;
	double upperBound = 0.0;
	boolean lowerOpen = false;
	boolean upperOpen = false;
	boolean leftInfinite = true;
	boolean rightInfinite = true;

	/*
	 * for the semantic instantiator
	 */
	public NumericInterval() {
	}

	public NumericInterval(String intvs) {
		parse(intvs);
	}

	public NumericInterval(Double left, Double right, boolean leftOpen, boolean rightOpen) {

		if (!(leftInfinite = (left == null)))
			lowerBound = left;
		if (!(rightInfinite = (right == null)))
			upperBound = right;

		lowerOpen = leftOpen;
		upperOpen = rightOpen;
	}

	public boolean isLowerOpen() {
		return lowerOpen;
	}

	public void setLowerOpen(boolean b) {
		this.lowerOpen = b;
	}

	public void parse(String s) {

		StreamTokenizer scanner = new StreamTokenizer(new StringReader(s));
		int token = 0;
		double high = 0.0, low = 0.0;
		int nnums = 0;
		boolean lowdef = false, highdef = false;

		while (true) {

			try {
				token = scanner.nextToken();
			} catch (IOException e) {
				throw new KlabRuntimeException("invalid interval syntax: " + s);
			}

			if (token == StreamTokenizer.TT_NUMBER) {

				if (nnums > 0) {
					high = scanner.nval;
				} else {
					low = scanner.nval;
				}
				nnums++;

			} else if (token == StreamTokenizer.TT_EOF || token == StreamTokenizer.TT_EOL) {
				break;
			} else if (token == '(') {
				if (nnums > 0)
					throw new KlabRuntimeException("invalid interval syntax: " + s);
				lowdef = true;
				lowerOpen = true;
			} else if (token == '[') {
				if (nnums > 0)
					throw new KlabRuntimeException("invalid interval syntax: " + s);
				lowdef = true;
				lowerOpen = false;
			} else if (token == ')') {
				if (nnums == 0)
					throw new KlabRuntimeException("invalid interval syntax: " + s);
				highdef = true;
				upperOpen = true;
			} else if (token == ']') {
				if (nnums == 0)
					throw new KlabRuntimeException("invalid interval syntax: " + s);
				highdef = true;
				upperOpen = false;
			} else if (token == ',') {
				/* accept and move on */
			} else {
				throw new KlabRuntimeException("invalid interval syntax: " + s);
			}
		}

		/*
		 * all read, assemble interval info
		 */
		if (lowdef && highdef && nnums == 2) {
			leftInfinite = rightInfinite = false;
			lowerBound = low;
			upperBound = high;
		} else if (lowdef && !highdef && nnums == 1) {
			leftInfinite = false;
			lowerBound = low;
		} else if (highdef && !lowdef && nnums == 1) {
			rightInfinite = false;
			upperBound = low;
		} else {
			throw new KlabRuntimeException("invalid interval syntax: " + s);
		}
	}

	public int compare(NumericInterval i) {

		if (leftInfinite == i.leftInfinite && lowerOpen == i.lowerOpen && rightInfinite == i.rightInfinite
				&& upperOpen == i.upperOpen && lowerBound == i.lowerBound && upperBound == i.upperBound)
			return 0;

		if (this.upperBound <= i.lowerBound)
			return -1;

		if (this.lowerBound >= i.upperBound)
			return 1;

		throw new KlabRuntimeException("error: trying to sort overlapping numeric intervals");

	}

	public boolean isUpperOpen() {
		return upperOpen;
	}

	public void setUpperOpen(boolean upperOpen) {
		this.upperOpen = upperOpen;
	}

	public void setLowerBound(double lowerBound) {
		this.lowerBound = lowerBound;
	}

	public void setUpperBound(double upperBound) {
		this.upperBound = upperBound;
	}

	public void setLeftInfinite(boolean leftInfinite) {
		this.leftInfinite = leftInfinite;
	}

	public void setRightInfinite(boolean rightInfinite) {
		this.rightInfinite = rightInfinite;
	}

	public boolean isRightInfinite() {
		return rightInfinite;
	}

	public boolean isLeftInfinite() {
		return leftInfinite;
	}

	/**
	 * true if the upper boundary is closed, i.e. includes the limit
	 * 
	 * @return true if upper boundary is closed
	 */
	public boolean isRightBounded() {
		return !upperOpen;
	}

	/**
	 * true if the lower boundary is closed, i.e. includes the limit
	 * 
	 * @return true if lower bounday is closed
	 */
	public boolean isLeftBounded() {
		return !lowerOpen;
	}

	public double getLowerBound() {
		return lowerBound;
	}

	public double getUpperBound() {
		return upperBound;
	}

	public boolean contains(double d) {

		if (leftInfinite)
			return (upperOpen ? d < upperBound : d <= upperBound);
		else if (rightInfinite)
			return (lowerOpen ? d > lowerBound : d >= lowerBound);
		else
			return (upperOpen ? d < upperBound : d <= upperBound) && (lowerOpen ? d > lowerBound : d >= lowerBound);
	}

	@Override
	public String toString() {

		String ret = "";

		if (!leftInfinite) {
			ret += lowerOpen ? "(" : "[";
			ret += lowerBound;
		}
		if (!rightInfinite) {
			if (!leftInfinite)
				ret += " ";
			ret += upperBound;
			ret += upperOpen ? ")" : "]";
		}

		return ret;
	}

	@Override
	public String asText() {
		return toString();
	}

	@Override
	public boolean contains(NumericInterval o) throws KlabException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean overlaps(NumericInterval o) throws KlabException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean intersects(NumericInterval o) throws KlabException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public ITopologicallyComparable<NumericInterval> union(ITopologicallyComparable<?> other) throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ITopologicallyComparable<NumericInterval> intersection(ITopologicallyComparable<?> other)
			throws KlabException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public double getCoveredExtent() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Interval)) {
            throw new KlabRuntimeException("cannot deserialize a NumericInterval from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Interval bean = (org.integratedmodelling.common.beans.Interval) object;
        
        leftInfinite = bean.isLeftInfinite();
        lowerBound = bean.getLowerBound();
        upperBound = bean.getUpperBound();
        lowerOpen = bean.isLowerOpen();
        rightInfinite = bean.isRightInfinite();
        upperOpen = bean.isUpperOpen();		

    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Interval.class)) {
            throw new KlabRuntimeException("cannot serialize a NumericInterval to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Interval ret = new org.integratedmodelling.common.beans.Interval();

        ret.setLeftInfinite(leftInfinite);
        ret.setLowerBound(lowerBound);
        ret.setLowerOpen(lowerOpen);
        ret.setRightInfinite(rightInfinite);
        ret.setUpperBound(upperBound);
        ret.setUpperOpen(upperOpen);
        
        return (T) ret;
    }
	
}
