/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.states;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;

/**
 * A state that merges other states that address only a part of the context.
 * 
 * @author ferd
 *
 */
public class MergedState extends State {

    Collection<IState>    originalStates;

    /**
     * Mediators for all extents that need mediators, in reverse order
     * vs. the extents in the scale. Applied in the list order when
     * mediating.
     */
    List<IState.Mediator> mediators;

    public MergedState(IObservableSemantics observable, IScale scale,
            Collection<IState> original,
            List<IState.Mediator> mediators, IDirectObservation context) {
        super(observable, scale, null, original.iterator().next().getStorage().isDynamic(), original
                .iterator().next().getStorage().isProbabilistic(), context);
        this.originalStates = original;
        this.mediators = mediators;
        
        /*
         * build spatial index if we have space; if resolution is 
         * different, prepare for resampling and subscribe to 
         * change notifications.
         */
        
        /*
         * build poor-man temporal index if we have time; if resolution is 
         * different, prepare for resampling and subscribe to 
         * change notifications.
         */
        
    }

    public Object mediateValueTo(Object value, int offset) {
        for (IState.Mediator m : mediators) {
            value = m.mediateTo(value, offset);
        }
        return value;
    }

    @Override
    public Object getValue(int index) {

        Queue<IState.Mediator> meds = new LinkedList<>(mediators);
        List<IScale.Locator> locs = new LinkedList<>();

        return map(index, meds, locs);
    }

    private Object map(int index, Queue<Mediator> meds, List<Locator> locs) {
        // TODO Auto-generated method stub
        return null;
    }


}
