package org.integratedmodelling.common.states;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Utility functions to operate on observed values and collections thereof. 
 * 
 * @author ferdinando.villa
 *
 */
public class Values {

    public static Object sum(Collection<Object> values) {
        double ret = 0;
        for (Object o : values) {
            if (o != null) {
                if (!(o instanceof Number)) {
                    return average(values);
                }
                if (!Double.isNaN(((Number)o).doubleValue())) {
                    ret += ((Number)o).doubleValue();
                }
            }
        }
        return ret;
    }

    public static Object average(Collection<Object> values) {
        
        double ret = 0;
        int n = 0;
        Map<IConcept, Integer> occurrences = null;
        int[] bcounts = null;
        
        for (Object o : values) {
            if (o != null) {
                if (o instanceof Number && !Double.isNaN(((Number)o).doubleValue())) {
                    ret += ((Number)o).doubleValue();
                    n ++;
                } else if (o instanceof IConcept) {
                    if (occurrences == null) {
                        occurrences = new HashMap<>();
                    }
                    int num = occurrences.containsKey(o) ? (occurrences.get(o) + 1) : 1; 
                    occurrences.put((IConcept)o, num);
                } else if (o instanceof Boolean) {
                    if (bcounts == null) {
                        bcounts = new int[2];
                    }
                    bcounts[((Boolean) o) ? 0 : 1] ++;
                }
            }
        }
        
        int types = 0;
        if (n > 0) types++;
        if (bcounts != null) types++;
        if (occurrences != null) types++;
        
        if (types > 1) {
            throw new KlabRuntimeException("cannot average values of different types");
        }
        
        if (types == 0) {
            return null;
        }
        
       if (bcounts != null) {
            return bcounts[0] >= bcounts[1];
        } else if (occurrences != null) {
            int nmax = 0;
            IConcept rt = null;
            for (IConcept cc : occurrences.keySet()) {
                int cn = occurrences.get(cc);
                if (cn > nmax) {
                    nmax = cn;
                    rt = cc;
                }
            }
            return rt;
        }
        
        return ret/n;
    }

}
