package org.integratedmodelling.common.states;

import java.util.Iterator;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservable;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IMediatingObserver;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.runtime.IActiveObserver;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.beans.Observation;
import org.integratedmodelling.common.knowledge.Observable;
import org.integratedmodelling.common.model.runtime.AbstractMediator;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.exceptions.KlabException;

/**
 * A mediated state wraps another state and filters all the {@link IState#getValue(int)}
 * calls through a mediating observer. To access the unmediated value, use
 * {@link #getUnmediatedValue(int)}.
 * 
 * If mediation is not necessary or not applicable (i.e. the observer(s) are not mediating
 * observers) this simply acts as a proxy to the unmodified state.
 * 
 * Note that {@link #getValue()} does not mediate at all (returns the unmodified storage)
 * and all the set functions expect <b>unmediated</b> values.
 * 
 * @author ferdinando.villa
 *
 */
public class MediatedState extends State implements IState {

    State                    state;
    private AbstractMediator mediator;
    private IObserver        observer;
    private IMonitor monitor;
    
    boolean errorsPresent = false;

    public MediatedState(State state, IObserver finalSemantics, IMonitor monitor) throws KlabException {
        this.state = state;
        this.observer = finalSemantics;
        this.monitor = monitor;
        this.observable = new Observable((Observable) state.getObservable());
        ((ObservableSemantics) this.observable).setObserver(finalSemantics);
        if (finalSemantics instanceof IMediatingObserver
                && state.getObserver() instanceof IMediatingObserver) {
            this.mediator = (AbstractMediator) ((IActiveObserver) state.getObserver())
                    .getMediator(finalSemantics, monitor);
        }
    }

    /**
     * 
     */
    @Override
    public Object getValue(int index) {
        Object ret = state.getValue(index);
        if (mediator != null) {
            try {
                ret = mediator.mediate(ret);
            } catch (KlabException e) {
                if (!errorsPresent) {
                    monitor.error("mediation error for mediated state: " + e.getMessage());
                    errorsPresent = true;
                }
            }
        }
        return ret;
    }

    @Override
    public IObservable getObservable() {
        return observable;
    }

    @Override
    public IObserver getObserver() {
        return this.observer;
    }

    /**
     * 
     * @param index
     * @return
     */
    public Object getUnmediatedValue(int index) {
        return state.getValue(index);
    }

    /*
     * delegate methods
     */

    @Override
    public boolean equals(Object obj) {
        return state.equals(obj);
    }

    @Override
    public int hashCode() {
        return state.hashCode();
    }

    @Override
    public String getInternalId() {
        return state.getInternalId();
    }

    @Override
    public IDirectObservation getContextObservation() {
        return state.getContextObservation();
    }

    @Override
    public void deserialize(Observation bean) {
        state.deserialize(bean);
    }

    @Override
    public void serialize(Observation ret) {
        state.serialize(ret);
    }

    @Override
    public String toString() {
        return state.toString();
    }

    @Override
    public void setParentId(String id) {
        state.setParentId(id);
    }

    @Override
    public IConcept getType() {
        return state.getType();
    }

    @Override
    public IObservation find(String id) {
        return state.find(id);
    }

    @Override
    public void append(IObservation observation) {
        state.append(observation);
    }

    @Override
    public final void setContext(IContext context) {
        state.setContext(context);
    }

    @Override
    public IContext getContext() {
        return state.getContext();
    }

    @Override
    public IMetadata getMetadata() {
        return state.getMetadata();
    }

    @Override
    public IScale getScale() {
        return state.getScale();
    }

    @Override
    public long getValueCount() {
        return state.getValueCount();
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return state.isSpatiallyDistributed();
    }

    @Override
    public boolean isTemporallyDistributed() {
        return state.isTemporallyDistributed();
    }

    @Override
    public boolean isTemporal() {
        return state.isTemporal();
    }

    @Override
    public boolean isSpatial() {
        return state.isSpatial();
    }

    @Override
    public ISpatialExtent getSpace() {
        return state.getSpace();
    }

    @Override
    public ITemporalExtent getTime() {
        return state.getTime();
    }

    @Override
    public Iterator<?> iterator(Index index) {
        return state.iterator(index);
    }

    @Override
    public IStorage<?> getStorage() {
        return state.getStorage();
    }

    @Override
    public Object getValue() {
        return state.getValue();
    }

    @Override
    public boolean isConstant() {
        return state.isConstant();
    }

    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {
        return state.serialize(desiredClass);
    }

    @Override
    public boolean isDynamic() {
        return state.isDynamic();
    }

}
