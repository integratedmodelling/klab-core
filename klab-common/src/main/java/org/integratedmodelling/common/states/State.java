/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.states;

import java.util.Collection;
import java.util.Iterator;

import org.integratedmodelling.api.data.IProbabilityDistribution;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.IValueMediatingObserver;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.storage.ConstStorage;
import org.integratedmodelling.common.storage.MemoryDataset;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Operational state implementation. The one in the client (
 * {@link org.integratedmodelling.common.model.runtime.State}) is a no-storage version for
 * client use only.
 * 
 * @author ferdinando.villa
 *
 */
public class State extends Observation implements IState, NetworkSerializable {

    IStorage<?> storage;
    private boolean   copyStateAtTransitions = false;
    ITransition timePointer = ITransition.INITIALIZATION;

    /*
     * only for MediatedState, which is simply a delegate wrapper but needs to extend
     * State for compatibility with methods in States.
     */
    protected State() {
    }

    /**
     * Create a state with memory storage.
     * 
     * @param observable must have an observer defined
     * @param scale
     * @param isDynamic
     * @param context
     */
    public State(IObservableSemantics observable, IScale scale, boolean isDynamic,
            boolean isProbabilistic, IDirectObservation context) {
        this(observable, scale, null, isDynamic, isProbabilistic, context);
    }

    @Override
    public String toString() {
        return "ST/" + getObservable();
    }
    
    /**
     * Call this one before get() or set() involving a transition, to ensure that 
     * the state is copied to the next step when configured.
     * 
     * @param tr
     */
    public void setTimePointer(ITransition tr) {
        if (tr != null) {
            if (timePointer == null || tr.getTimeIndex() > timePointer.getTimeIndex()) {
                if (copyStateAtTransitions) {
                    States.copyStateToNextTransition(this, timePointer);
                }
                timePointer = tr;
            }
        }
    }
    
    public void setCopyStateAtTransitions(boolean b) {
        this.copyStateAtTransitions = b;
    }
    
    public boolean isCopyStateAtTransitions() {
        return this.copyStateAtTransitions;
    }

//    /**
//     * Create a state potentially with dataset-backed storage, taking data from an
//     * existing object.
//     * @param data
//     * 
//     * @param observable must have an observer defined
//     * @param scale
//     * @param dataset
//     * @param isDynamic
//     * @param context
//     */
//    public State(Object data, IObservableSemantics observable, IScale scale, IDataset dataset,
//            boolean isDynamic,
//            IDirectObservation context) {
//
//        this.parentId = ((Observation) context).getInternalId();
//        setScale(scale, isDynamic);
//        setObservable(observable, context.getContext(), observable.getFormalName());
//        if (isScalar(data)) {
//            this.storage = new ConstStorage(data, scale);
//        } else {
//            if (dataset instanceof MemoryDataset) {
//                this.storage = dataset.getStorage(observable, isDynamic, isProbabilistic(data));
//            } else {
//                this.storage = KLAB.MFACTORY
//                        .createStorage(observable
//                                .getObserver(), scale, dataset, isDynamic, isProbabilistic(data));
//            }
//            this.storage.set(data);
//        }
//    }

    /**
     * Create a state with dataset-backed storage.
     * 
     * @param observable must have an observer defined
     * @param scale
     * @param dataset
     * @param isDynamic
     * @param context
     */
    public State(IObservableSemantics observable, IScale scale, IDataset dataset, boolean isDynamic,
            boolean isProbabilistic,
            IDirectObservation context) {

        this.parentId = context == null ? null : ((Observation) context).getInternalId();
        this.parent = context;
        setObservable(observable, context == null ? null : context.getContext(), id);
        setScale(scale, isDynamic);
        if (dataset instanceof MemoryDataset) {
            this.storage = dataset.getStorage(observable, isDynamic, isProbabilistic);
        } else {
            this.storage = KLAB.MFACTORY
                    .createStorage(observable.getObserver(), scale, dataset, isDynamic, isProbabilistic);
        }
    }
    
    public State(IObservableSemantics observable, IScale scale, IDataset dataset, boolean isDynamic,
            boolean isProbabilistic,
            IContext context) {

        setObservable(observable, context, id);
        setScale(scale, isDynamic);
        if (dataset instanceof MemoryDataset) {
            this.storage = dataset.getStorage(observable, isDynamic, isProbabilistic);
        } else {
            this.storage = KLAB.MFACTORY
                    .createStorage(observable.getObserver(), scale, dataset, isDynamic, isProbabilistic);
        }
    }

    /**
     * Create a state to only serve one object without any waste of memory. If the object
     * is not a scalar, try to build appropriate storage based on the scale.
     * @param data
     * @param observable must have an observer defined
     * @param scale
     * @param isDynamic
     */
    public State(Object data, IObservableSemantics observable, IDirectObservation context, boolean isDynamic,
            boolean isProbabilistic) {

        setObservable(observable, context.getContext(), id);
        setScale(context.getScale(), isDynamic);
        this.parent = context;
        
        if (isScalar(data)) {
            this.storage = new ConstStorage(data, scale);
        } else {
            this.storage = KLAB.MFACTORY.createStorage(observable
                    .getObserver(), scale, null, isDynamic, data instanceof IProbabilityDistribution);
            this.storage.set(data);
        }
    }

    @Override
    public void setContext(IContext context) {
        this.context = context;
    }

    private void setScale(IScale scale, boolean isDynamic) {
        // not dynamic: remove time from scale if it's there.
        if (!isDynamic && scale.getTime() != null && scale.getTime().isTemporallyDistributed()) {
            scale = scale.getSubscale(KLAB.c(NS.TIME_DOMAIN), -1);
        }
        this.scale = scale;
    }

    /*
     * non-public API. These could all be metadata without specialized methods, although
     * using them is way messier, so they stand for now.
     */

    @SuppressWarnings("javadoc")
    public static boolean isScalar(Object data) {
        return !(data.getClass().isArray() || data instanceof Collection);
    }

    @SuppressWarnings("javadoc")
    public static boolean isProbabilistic(Object data) {
        if (isScalar(data)) {
            return data instanceof IProbabilityDistribution;
        }
        if (data.getClass().isArray()) {
            // TODO unlikely to happen, but I don't know how to do it.
        }
        if (data instanceof Collection) {
            for (Object o : (Collection<?>) data) {
                if (o != null) {
                    return o instanceof IProbabilityDistribution;
                }
            }
        }
        return false;
    }
//
//    @Override
//    public IObservable getObservable() {
//        return observable;
//    }
//
//    @Override
//    public IScale getScale() {
//        return scale;
//    }

    @Override
    public long getValueCount() {
        return scale.getMultiplicity();
    }

    @Override
    public IObserver getObserver() {
        return observable.getSemantics().getObserver();
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return scale.getSpace() != null && scale.getSpace().getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return scale.getTime() != null && scale.getTime().getMultiplicity() > 1;
    }

    @Override
    public boolean isTemporal() {
        return scale.getTime() != null;
    }

    @Override
    public boolean isSpatial() {
        return scale.getSpace() != null;
    }

    @Override
    public ISpatialExtent getSpace() {
        return scale.getSpace();
    }

    @Override
    public ITemporalExtent getTime() {
        return scale.getTime();
    }

    @Override
    public Object getValue(int index) {
        return storage.get(index);
    }
    
    @Override
    public Iterator<?> iterator(Index index) {
        return new It(index.iterator());
    }

    @Override
    public IStorage<?> getStorage() {
        return storage;
    }

    /*
     * used in Groovy code to get the value if possible. TODO use some meaningful
     * on-demand aggregation for multiple values; see what to do with categories and
     * distributions.
     */
    @SuppressWarnings("javadoc")
    public Object getValue() {
        return getValueCount() == 1 ? getValue(0) : "[multiple values]";
    }

    // / ===================================

    class It implements Iterator<Object> {

        Iterator<Integer> _sit;

        It(Iterator<Integer> sit) {
            _sit = sit;
        }

        @Override
        public boolean hasNext() {
            return _sit.hasNext();
        }

        @Override
        public Object next() {
            return getValue(_sit.next());
        }

        @Override
        public void remove() {
        }

    }

    @Override
    public boolean isConstant() {
        return storage instanceof ConstStorage;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.State.class)) {
            throw new KlabRuntimeException("cannot serialize a State to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.State ret = new org.integratedmodelling.common.beans.State();
        super.serialize(ret);

        ret.setObserver(KLAB.MFACTORY
                .adapt(getObserver(), org.integratedmodelling.common.beans.Observer.class));
        ret.setDynamic(storage.isDynamic());
        ret.setConstant(isConstant());
        if (isConstant()) {
            ret.setConstantData(getValue());
        }
        if (getScale().getMultiplicity() == 1) {
            ret.setScalarData(getValue(0));
        }

        return (T) ret;
    }

    @Override
    public boolean isDynamic() {
        return storage.isDynamic();
    }

    public void flushStorage(ITransition transition) {
        // KLAB.info("FLUSHING " + this + " before " + transition.getTimeIndex());
        storage.flush(transition);
    }

    @Override
    public void addChangeListener(ChangeListener listener) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public IState as(IObserver observer) {
        // TODO Auto-generated method stub
        if (observer instanceof IValueMediatingObserver) {
            
        }
        
        return this;
    }

}
