/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.states;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.collections.Pair;

public class StateView extends State {

    IState                originalState;
    IState                mediatedState;

    /**
     * Mediators for all extents that need mediators, in reverse order vs. the extents in
     * the scale. Applied in the list order when mediating.
     */
    List<IState.Mediator> mediators;

    public StateView(IObservableSemantics observable, IScale scale, IState original, IDataset dataset,
            List<IState.Mediator> mediators, IDirectObservation context) {
        super(observable, scale, dataset, original.getStorage().isDynamic(), original.getStorage()
                .isProbabilistic(), context);
        this.originalState = original;
        this.mediators = mediators;
    }

    public Object mediateValueTo(Object value, int offset) {
        for (IState.Mediator m : mediators) {
            value = m.mediateTo(value, offset);
        }
        return value;
    }

    @Override
    public Object getValue(int index) {

        /*
         * mediated state may be covered while this isn't, so
         * check.
         */
        if (scale.isCovered(index)) {
            Queue<IState.Mediator> meds = new LinkedList<>(mediators);
            List<IScale.Locator> locs = new LinkedList<>();
            return reduce(index, meds, locs);
        }
        return null;
    }

    private Object reduce(int index, Queue<Mediator> meds, List<Locator> locs) {

        IState.Mediator m = meds.remove();
        ArrayList<Pair<Object, Double>> toReduce = new ArrayList<>();
        
        /**
         * FIXME the index passed to each mediator should probably be relative to its own
         * dimension.
         */
        for (IScale.Locator locator : m.getLocators(index)) {

            ArrayList<IScale.Locator> floc = new ArrayList<>(locs);
            floc.add(locator);

            if (meds.isEmpty()) {
                Object val = originalState.getValue((int) originalState.getScale()
                        .locate(floc.toArray(new Locator[floc.size()])));
                toReduce.add(new Pair<Object, Double>(val, locator.getWeight()));
            } else {
                toReduce.add(new Pair<Object, Double>(reduce(index, meds, floc), locator.getWeight()));
            }
        }

        return m.reduce(toReduce, getMetadata());

    }

}
