/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.states;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.IValueMediator;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.classification.Classifier;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.NumericInterval;
import org.integratedmodelling.common.storage.AbstractStorage;
import org.integratedmodelling.common.storage.ConstStorage;
import org.integratedmodelling.common.utils.NumberUtils;
import org.integratedmodelling.common.visualization.VisualizationFactory;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Traits;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Utilities to create and handle states, to be used in component development from
 * contextualizers. The types of state created are specific of one transition, meant to be
 * marshalled around a contextualization process, not for final storage of results.
 * 
 * @author Ferd
 *
 */
public class States {

    public static Object get(IState s, int offset) {
        return getStraight(s, offset);
    }

    /**
     * Find a state in the context that has the passed base observable and all of the
     * passed traits.
     * 
     * @param context
     * @param observable
     * @param traits
     * @return the state with these attributes
     */
    public static IState findStateWith(IDirectObservation context, IConcept observable, IConcept... traits) {

        if (NS.isTrait(observable)) {
            observable = Observables.makeTypeFor(observable);
        }

        for (IState state : context.getStates()) {
            if (state.getObservable().getSemantics().is(observable)) {
                /*
                 * match traits
                 */
                boolean ok = true;
                if (traits != null) {
                    for (IConcept trait : traits) {
                        if (!Traits.hasTrait(state.getObservable().getSemantics().getType(), trait)) {
                            ok = false;
                        }
                    }
                }
                if (ok) {
                    return state;
                }
            }
        }

        return null;
    }

    /**
     * Find a state in the context that has the passed base observable and none of the
     * passed traits.
     * 
     * @param context
     * @param observable
     * @param traits
     * @return state without the attributes
     */
    public static IState findStateWithout(IDirectObservation context, IConcept observable, IConcept... traits) {

        if (NS.isTrait(observable)) {
            observable = Observables.makeTypeFor(observable);
        }

        for (IState state : context.getStates()) {
            if (state.getObservable().getSemantics().is(observable)) {
                /*
                 * match traits
                 */
                boolean ok = true;
                if (traits != null) {
                    for (IConcept trait : traits) {
                        if (Traits.hasTrait(state.getObservable().getSemantics().getType(), trait)) {
                            ok = false;
                        }
                    }
                }
                if (ok) {
                    return state;
                }
            }
        }

        return null;
    }

    /**
     * Find a state with the given observable of the passed context.
     * 
     * @param context
     * @param observable
     * @return state with given observable
     */
    public static IState findState(IDirectObservation context, IConcept observable) {

        if (NS.isTrait(observable)) {
            observable = Observables.makeTypeFor(observable);
        }
        for (IState state : context.getStates()) {
            if (state.getObservable().getSemantics().is(observable)) {
                return state;
            }
        }

        return null;
    }

    /**
     * Find a state with the given observable of the passed context, ensuring its
     * observation semantics matches the passed mediator (unit, ranking scale or
     * currency).
     * 
     * TODO unimplemented yet.
     * 
     * @param context
     * @param observable
     * @return state with given context
     */
    public static IState findState(IDirectObservation context, IConcept observable, IValueMediator mediator) {

        if (NS.isTrait(observable)) {
            observable = Observables.makeTypeFor(observable);
        }

        for (IState state : context.getStates()) {
            if (state.getObservable().getSemantics().is(observable)) {
                return state;
            }
        }

        return null;
    }

    /**
     * Find all states whose observable inherits a given trait.
     * 
     * @param context
     * @param trait
     * @return all states with trait
     */
    public static Collection<IState> findStates(IDirectObservation context, IConcept trait) {

        ArrayList<IState> ret = new ArrayList<>();
        for (IState state : context.getStates()) {
            if (Traits.hasTrait(state, trait)) {
                ret.add(state);
            }
        }

        return ret;
    }
    
    public static Object getStraight(IState s, int offset) {

        if (s.isConstant()) {
            return ((ConstStorage) s.getStorage()).get(offset);
        }

        AbstractStorage<?> st = (AbstractStorage<?>) s.getStorage();
        if (s instanceof StateView) {
            return s.getValue(offset);
        }
        return ((IStorage<?>) st).get(offset);
    }
    /**
     * Get the value at the passed offset after moving the time to the passed transition,
     * every other extent offset remaining the same. Up to you to not ask to predict the
     * future unless someone has implemented it.
     * 
     * @param s
     * @param offset
     * @param transition
     * @return value at offset
     */
    public static Object get(IState s, int offset, ITransition transition) {

        if (transition != null) {
            ((State)s).setTimePointer(transition);
        }
        
        if (s.isConstant()) {
            return ((ConstStorage) s.getStorage()).get(offset);
        }

        AbstractStorage<?> st = (AbstractStorage<?>) s.getStorage();
        if (s instanceof StateView) {
            return s.getValue(st.getTemporalOffsetAt(offset, transition));
        }
        return ((IStorage<?>) st).get(st.getTemporalOffsetAt(offset, transition));
    }

    public static double getDouble(IState s, int offset, ITransition transition) {
        return toDouble(get(s, offset, transition));
    }

    public static double getDouble(IState s, int offset) {
        return toDouble(get(s, offset));
    }
    
    public static int getInteger(IState s, int offset, ITransition transition, int nodataValue) {
        return toInteger(get(s, offset, transition), nodataValue);
    }
    
    public static int getInteger(IState s, int offset, int nodataValue) {
        return toInteger(get(s, offset), nodataValue);
    }

    public static double toDouble(Object o) {
        if (o == null) {
            return Double.NaN;
        }
        if (!(o instanceof Number)) {
            throw new KlabRuntimeException("non-number returned by a state accessed using getDouble: "
                    + o);
        }
        return ((Number) o).doubleValue();
    }

    public static int toInteger(Object o, int nodataValue) {
        if (o == null) {
            return nodataValue;
        }
        if (!(o instanceof Number)) {
            throw new KlabRuntimeException("non-number returned by a state accessed using getInteger: "
                    + o);
        }
        return ((Number) o).intValue();
    }
    
    public static boolean toBoolean(Object o) {
        if (o == null) {
            return false;
        }
        if (!(o instanceof Boolean)) {
            throw new KlabRuntimeException("non-boolean returned by a state accessed using getBoolean: "
                    + o);
        }
        return (Boolean) o;
    }

    public static boolean getBoolean(IState s, int offset, ITransition transition) {
        return toBoolean(get(s, offset, transition));
    }

    public static boolean getBoolean(IState s, int offset) {
        return getBoolean(s, offset, null);
    }

    /**
     * Set the initial state of the passed state to the passed object at the passed
     * offset. NOTE: Only use this one to set values. Directly calling IStorage.set will
     * only work on states that do not mediate others.
     * 
     * @param s
     * @param value
     * @param offset
     */
    public static synchronized void set(IState s, Object value, int offset) {

        if (!s.isConstant()) {
            AbstractStorage<?> st = (AbstractStorage<?>) s.getStorage();
            if (s instanceof StateView) {
                ((IStorage<?>) st).set(offset, ((StateView) s).mediateValueTo(value, offset));
            } else {
                ((IStorage<?>) st).set(offset, value);
            }
        }
    }

    /**
     * TBD
     * 
     * @param s
     * @param value
     * @param offset
     */
    public static synchronized void set(IState s, Object value, ITransition transition) {

        if (transition != null) {
            ((State)s).setTimePointer(transition);
        }

        if (!s.isConstant()) {
            AbstractStorage<?> st = (AbstractStorage<?>) s.getStorage();
            for (int n : s.getScale().getIndex(transition)) {
                if (s instanceof StateView) {
                    ((IStorage<?>) st).set(n, ((StateView) s).mediateValueTo(value, n));
                } else {
                    ((IStorage<?>) st).set(n, value);
                }
            }
        }
    }
    
    /**
     * Copy the state from a "current" transition to the one after that, assumed to exist.
     * 
     * @param s
     * @param transition
     */
    public static synchronized void copyStateToNextTransition(IState s, ITransition transition) {
        if (!s.isConstant()) {
            Object[] values = new Object[(int)(s.getScale().getMultiplicity()/s.getScale().getTime().getMultiplicity())];
            int ii = 0;
            for (int n : s.getScale().getIndex(transition)) {
                values[ii++] = get(s, n);
            }
            ii = 0;
            ITransition next = transition == null ? (s.getScale().getTime().getTransition(0)) : transition.next();
            for (int n : s.getScale().getIndex(next)) {
                set(s, values[ii++], n);
            }
        }
    }

    /**
     * Set state to value. Intelligently analyzes the value and behaves appropriately with
     * scalar or non-scalar values.
     * 
     * TODO unimplemented as stated - for now just sets state at offset 0.
     * 
     * @param s
     * @param value
     */
    public static synchronized void set(IState s, Object value) {

        if (!s.isConstant()) {
            AbstractStorage<?> st = (AbstractStorage<?>) s.getStorage();
            if (s instanceof StateView) {
                ((IStorage<?>) st).set(0, ((StateView) s).mediateValueTo(value, 0));
            } else {
                ((IStorage<?>) st).set(0, value);
            }
        }
    }

    /**
     * Create a state for the passed observable (must be a quality) within the context of
     * the given observation (must be a process or subject) for the passed transition. The
     * transition may be null, in which case the state holds the initial values. Meant to
     * create states that get passed back to the workflow and merged into the
     * contextualization process, used in models that do not keep history (e.g. weather).
     * 
     * Handles almost everything automatically, including using backing datasets if
     * available. The generated state will have enough states to hold one timeslice of the
     * overall scale.
     * 
     * @param observable
     * @param context
     * @param transition
     * @return new state
     */
    public static IState create(IObservableSemantics observable, IObservation context, ITransition transition) {
        IDataset dataset = context instanceof IActiveSubject ? ((IActiveSubject) context).getBackingDataset()
                : null;
        return new State(observable, transition == null ? KLAB.MFACTORY.copyScale(context.getScale())
                : KLAB.MFACTORY.copyScale(transition), dataset, true, false, (IDirectObservation) context);
    }
    
    public static IState createWithScale(IObservableSemantics observable, IObservation context, IScale scale) {
        IDataset dataset = context instanceof IActiveSubject ? ((IActiveSubject) context).getBackingDataset()
                : null;
        return new State(observable, scale, dataset, true, false, (IDirectObservation) context);
    }

    private static IState createStatic(IObservableSemantics observable, IObservation context, ITransition transition) {
        IDataset dataset = context instanceof IActiveSubject ? ((IActiveSubject) context).getBackingDataset()
                : null;
        return new State(observable, transition == null ? KLAB.MFACTORY.copyScale(context.getScale())
                : KLAB.MFACTORY.copyScale(transition), dataset, false, false, (IDirectObservation) context);
    }

    public static IState createStatic(IObservableSemantics observable, IScale scale, IContext context) {
        return new State(observable, KLAB.MFACTORY.copyScale(scale), null, false, false, context);
    }

    /**
     * Create a state for the passed observable (must be a quality) within the context of
     * the given observation (must be a process or subject) for the passed transition. The
     * transition may be null, in which case the state holds the initial values. Meant to
     * create states that get passed back to the workflow and merged into the
     * contextualization process, used in models that do not keep history (e.g. weather).
     * 
     * Handles almost everything automatically, including using backing datasets if
     * available. The generated state will have enough states to hold one timeslice of the
     * overall scale.
     * 
     * @param observable
     * @param context
     * @param transition
     * @return new state
     */
    public static IState createProbabilistic(IObservableSemantics observable, IObservation context, ITransition transition) {
        IDataset dataset = context instanceof IActiveSubject ? ((IActiveSubject) context).getBackingDataset()
                : null;
        return new State(observable, transition == null ? KLAB.MFACTORY.copyScale(context.getScale())
                : KLAB.MFACTORY.copyScale(transition), dataset, true, true, (IDirectObservation) context);
    }

    public static IState createProbabilisticWithScale(IObservableSemantics observable, IObservation context, IScale scale) {
        IDataset dataset = context instanceof IActiveSubject ? ((IActiveSubject) context).getBackingDataset()
                : null;
        return new State(observable, scale, dataset, true, true, (IDirectObservation) context);
    }
    
    private static IState createProbabilisticStatic(IObservableSemantics observable, IObservation context, ITransition transition) {
        IDataset dataset = context instanceof IActiveSubject ? ((IActiveSubject) context).getBackingDataset()
                : null;
        return new State(observable, transition == null ? KLAB.MFACTORY.copyScale(context.getScale())
                : KLAB.MFACTORY.copyScale(transition), dataset, false, true, (IDirectObservation) context);
    }

    /**
     * Calls create(observable, context, null) - just a more fluent API for the cases when
     * the transition does not apply (initialization or no time in scale).
     * 
     * @param observable
     * @param context
     * @return new state
     */
    public static IState create(IObservableSemantics observable, IObservation context) {
        return ((ObservableSemantics) observable).isProbabilistic()
                ? createProbabilistic(observable, context)
                : create(observable, context, (ITransition)null);
    }
    
    /**
     * Calls create(observable, context, null) passing a specified scale (should be conformant
     * with self's).
     * 
     * @param observable
     * @param context
     * @return new state
     */
    public static IState create(IObservableSemantics observable, IObservation context, IScale scale) {
        return ((ObservableSemantics) observable).isProbabilistic()
                ? createProbabilisticWithScale(observable, context, scale)
                : createWithScale(observable, context, scale);
    }
    
    /**
     * Calls create(observable, context, null) - just a more fluent API for the cases when
     * the transition does not apply (initialization or no time in scale).
     * 
     * @param observable
     * @param context
     * @return new state
     */
    public static IState create(IObservableSemantics observable, IObservation context, IConcept...collapseExtents) {
        return ((ObservableSemantics) observable).isProbabilistic()
                ? createProbabilistic(observable, context)
                : create(observable, context, (ITransition) null);
    }

    /**
     * Calls create(observable, context, null) for a static state which won't contain
     * temporal structure.
     * 
     * @param observable
     * @param context
     * @return new static state
     */
    public static IState createStatic(IObservableSemantics observable, IObservation context) {
        return ((ObservableSemantics) observable).isProbabilistic()
                ? createProbabilisticStatic(observable, context)
                : createStatic(observable, context, null);
    }

    /**
     * Calls create(observable, context, null) - just a more fluent API for the cases when
     * the transition does not apply (initialization or no time in scale). Creates a
     * probabilistic state.
     * 
     * @param observable
     * @param context
     * @return new state
     */
    public static IState createProbabilistic(IObservableSemantics observable, IObservation context) {
        return createProbabilistic(observable, context, null);
    }

    /**
     * Calls create(observable, context, null) for a static state which won't contain
     * temporal structure. Creates a probabilistic state.
     * 
     * @param observable
     * @param context
     * @return new static state
     */
    public static IState createProbabilisticStatic(IObservableSemantics observable, IObservation context) {
        return createProbabilisticStatic(observable, context, null);
    }

    public static Map<String, IState> matchStatesToInputs(IDirectObservation context, Map<String, IObservableSemantics> expectedInputs) {

        Map<String, IState> ret = new HashMap<>();
        for (String id : expectedInputs.keySet()) {
            for (IState state : context.getStates()) {
                if (state.getObservable().getSemantics().is(expectedInputs.get(id))) {
                    ret.put(id, state);
                    break;
                }
            }
        }
        return ret;
    }

    public static void setChanged(IState state, boolean b) {
        state.getStorage().setChanged(b);
    }

    public static boolean hasChanged(IState state) {
        return state.getStorage().hasChanged();
    }

    /**
     * True if state has multiple values in each state of the passed extent type. Do not
     * confuse with state.isConstant(), for which this may return true.
     * 
     * @param extentType
     */
    public static boolean isDistributedOutside(IState state, IConcept extentType) {

        long mul = state.getScale().getMultiplicity();
        IExtent ext = state.getScale().getExtent(extentType);
        long nex = ext == null ? 1 : ext.getMultiplicity();
        return (mul / nex) > 1;
    }

    public static IConcept getDataReductionTrait(IObservableSemantics originalObservable, IObservableSemantics finalObservable) {
        return null;
    }

    /**
     * Return a state that provides a merged view of a set of other states. Changes in any
     * of the merged states will be reflected in the values. Not meant for overlapping
     * states, nor (for now) for states with extents that require mediation beyond
     * relocation of uniform grids.
     * 
     * @param states
     * @param scale
     * @return
     */
    public static IState getMerge(Collection<IState> states, IScale scale) {
        return new MergedState(states.iterator().next().getObservable()
                .getSemantics(), scale, states, null, null);
    }

    /**
     * Find a state in a context subject (or its parents) that is suitable to create a
     * view for the passed observable in a dependent subject. This may involve parsing
     * data reduction traits from the observable, matching to models with compatible
     * traits, and configuring the view to express the requested ones. If a suitable view
     * is found, create it and return it.
     * 
     * @param contextSubject
     * @param observable
     * @param context
     * @return new view
     * @throws KlabException
     */
    public static IState findView(ISubject contextSubject, IObservableSemantics observable, IActiveSubject context)
            throws KlabException {

        Pair<IState, IConcept> original = findStateInSubject(observable, contextSubject);
        if (original == null) {
            return null;
        }

        ArrayList<IState.Mediator> mediators = new ArrayList<>();
        for (IExtent originalExtent : contextSubject.getScale()) {
            IExtent targetExtent = context.getScale().getExtent(originalExtent.getDomainConcept());
            if (targetExtent != null) {
                IState.Mediator mediator = originalExtent.getMediator(targetExtent, observable, original
                        .getSecond());
                if (mediator == null) {
                    /*
                     * can't do it - give up to tell resolver to try a first-hand
                     * observation.
                     */
                    return null;
                }
                mediators.add(mediator);
            }
        }
        return new StateView(observable, context.getScale(), original.getFirst(), context
                .getBackingDataset(), mediators, context);
    }
    /**
     * Return a state that represents a view of another according to a subject. The state
     * is not automatically assigned to the subject.
     * 
     * @param state
     *            the original state
     * @param viewer
     *            the subject that will view its values through its own scale.
     * 
     * @return new view
     */
    public static IState getView(IState state, IDirectObservation viewer) {
        return getView(state, viewer, null);
    }
    
    /**
     * Return a state that represents a view of another according to a subject. The state
     * is not automatically assigned to the subject.
     * 
     * @param state
     *            the original state
     * @param viewer
     *            the subject that will view its values through its own scale.
     * 
     * @return new view
     */
    public static IState getView(IState state, IDirectObservation viewer, IConcept dataReduction) {

        ArrayList<IState.Mediator> mediators = new ArrayList<>();
        for (IExtent originalExtent : state.getScale()) {
            IExtent targetExtent = viewer.getScale().getExtent(originalExtent.getDomainConcept());
            if (targetExtent != null) {
                IState.Mediator mediator = originalExtent
                        .getMediator(targetExtent, state.getObservable().getSemantics(), dataReduction);
                if (mediator == null) {
                    return null;
                }
                mediators.add(mediator);
            }
        }
        return new StateView(state.getObservable().getSemantics(), viewer
                .getScale(), state, null, mediators, ((State) state)
                        .getContextObservation());
    }

    /**
     * Return a state that represents a view of another according to a scale.
     * 
     * @param state
     *            the original state
     * @param targetScale
     *            the scale for the target state.
     * 
     * @return new view
     */
    public static IState getView(IState state, IScale targetScale) {
        return getView(state, targetScale, null);
    }
    
    /**
     * Return a state that represents a view of another according to a scale.
     * 
     * @param state
     *            the original state
     * @param targetScale
     *            the scale for the target state.
     * 
     * @return new view
     */
    public static IState getView(IState state, IScale targetScale, IConcept dataReduction) {

        ArrayList<IState.Mediator> mediators = new ArrayList<>();
        for (IExtent originalExtent : state.getScale()) {
            IExtent targetExtent = targetScale.getExtent(originalExtent.getDomainConcept());
            if (targetExtent != null) {
                IState.Mediator mediator = originalExtent
                        .getMediator(targetExtent, state.getObservable().getSemantics(), dataReduction);
                if (mediator == null) {
                    return null;
                }
                mediators.add(mediator);
            }
        }
        return new StateView(state.getObservable()
                .getSemantics(), targetScale, state, null, mediators, ((State) state)
                        .getContextObservation());
    }

    private static Pair<IState, IConcept> findStateInSubject(IObservableSemantics observable, ISubject contextSubject) {

        IState original = null;
        for (IState state : contextSubject.getStates()) {

            if (state.getObservable().getSemantics().is(observable)) {
                original = state;
                break;
            }
        }

        IConcept trait = null;
        if (original == null) {

            if (!NS.synchronize()) {
                return null;
            }

            trait = Traits.getTrait(observable.getType(), NS.DATA_REDUCTION_TRAIT);
            if (trait == null) {
                return null;
            }

            IKnowledge obs;
            try {
                obs = Traits.removeTrait(observable.getType(), NS.DATA_REDUCTION_TRAIT);
            } catch (KlabValidationException e) {
                return null;
            }

            for (IState state : contextSubject.getStates()) {

                if (state.getObservable().getSemantics().is(obs)) {
                    original = state;
                    break;
                }
            }

        }
        return original == null ? null : new Pair<>(original, trait);
    }

    /**
     * Produce a classification that discretizes the range of the passed numeric state. If
     * the state is all no-data, return null without error.
     * 
     * @param s
     * @param maxBins
     * @param locators
     * @return discretization of range
     */
    public static IClassification discretize(IState s, int maxBins, IScale.Locator... locators) {

        /*
         * establish boundaries
         */
        double min = Double.NaN;
        double max = Double.NaN;
        for (int offset : s.getScale().getIndex(locators)) {

            if (!s.getScale().isCovered(offset)) {
                continue;
            }

            double val = getDouble(s, offset);
            if (!Double.isNaN(val)) {
                if (Double.isNaN(min) || min > val) {
                    min = val;
                }
                if (Double.isNaN(max) || max < val) {
                    max = val;
                }
            }
        }

        if (Double.isNaN(min) || Double.isNaN(max)) {
            return null;
        }

        /*
         * create ranges.
         */
        List<IConcept> levels = NS.getLevels(maxBins);
        List<Pair<IClassifier, IConcept>> classifiers = new ArrayList<>();

        double ist = min;
        double istep = (max - min) / maxBins;
        for (int i = 0; i < maxBins; i++) {

            double ien = ist + istep;
            boolean closeEnd = i == (maxBins - 1);
            if (closeEnd && ien < max) {
                ien = max;
            }
            classifiers.add(new Pair<IClassifier, IConcept>(Classifier
                    .RangeMatcher(new NumericInterval(ist, ien, false, !closeEnd)), levels.get(i)));

            ist += istep;
        }

        try {
            return new Classification(NS.getUserOrdering(), classifiers);
        } catch (KlabValidationException e) {
            throw new KlabRuntimeException(e);
        }
    }

    static public Object aggregate(IState state, IScale.Locator... locators) {

        Object ret = null;

        ITransition transition = getTransition(locators);
        if (transition != null) {
                ((State)state).setTimePointer(transition);
        }
        
        if (state.getObserver() instanceof INumericObserver) {
            double[] data = VisualizationFactory.getStateDataAsNumbers(state, Arrays.asList(locators));
            ret = state.getObserver().isExtensive(getExtensionConcept(state.getScale(), locators))
                    ? NumberUtils.sumWithoutNan(data) : NumberUtils.averageWithoutNan(data);
        } else {
            List<Pair<Integer, Object>> histogram = VisualizationFactory.getStateHistogram(state, Arrays.asList(locators));
            ret = getMostFrequentClass(histogram);
        }

        return ret;
    }

    private static ITransition getTransition(Locator[] locators) {
        for (Locator locator : locators) {
            if (locator instanceof ITransition) {
                return (ITransition)locator;
            }
        }
        return null;
    }

    private static Object getMostFrequentClass(List<Pair<Integer, Object>> histogram) {
        int n = -1;
        Object ret = null;
        for (Pair<Integer, Object> kvp : histogram) {
            if (kvp.getFirst() > n) {
                n = kvp.getFirst();
                ret = kvp.getSecond();
            }
        }
        return ret;
    }

    /**
     * Get the extent concept that we're not fixing through the locators.
     * 
     * FIXME there may be more than one extension concepts in the future
     */
    private static IConcept getExtensionConcept(IScale scale, Locator[] locators) {
        IConcept ret = null;
        for (IExtent extent : scale) {
            boolean found = false;
            for (Locator l : locators) {
                if (extent.getDomainConcept().equals(l == ITransition.INITIALIZATION ? KLAB.c(NS.TIME_DOMAIN) : l.getDomainConcept())) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return extent.getDomainConcept();
            }
        }
        return null;
    }

}
