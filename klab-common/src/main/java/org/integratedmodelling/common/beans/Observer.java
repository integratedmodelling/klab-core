/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.beans;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * We use one observer bean for all of them, to avoid an excess of beans with very similar
 * interfaces.
 * 
 * @author Ferd
 *
 */
public @Data class Observer implements IModelBean {
    /*
     * this is used to switch when constructing the deserialized object, and is set to the
     * actual class name of the object - a KIMxxxx observer.
     */
    String         observerClass;
    Classification classification;
    double         minimumValue;
    double         maximumValue;
    String         unitOrCurrency;
    Observable     observable;
    String         comparisonKnowledge;
    boolean        discretization;
    boolean        indirect;
    boolean        proportion;
    String         metadataProperty;
    RankingScale   rankingScale;
    boolean        integer;
}
