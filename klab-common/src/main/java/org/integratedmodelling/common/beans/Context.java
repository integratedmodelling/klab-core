/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.beans;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class Context implements IModelBean {

    private String     id;
    private Subject    subject;
    private String     description;
    private double     coverage;
    private boolean    finished;

    /*
     * a bit painful compared to List<Observation>, but this is a bean and doesn't play
     * well with polimorphism.
     */
    List<Subject>      deltaSubjects;
    List<State>        deltaStates;
    List<Process>      deltaProcesses;
    List<Event>        deltaEvents;
    List<Relationship> deltaRelationships;

    /**
     * @param s
     */
    public void addEvent(Event s) {
        if (deltaEvents == null) {
            deltaEvents = new ArrayList<>();
        }
        deltaEvents.add(s);
    }

    /**
     * @param s
     */
    public void addRelationship(Relationship s) {
        if (deltaRelationships == null) {
            deltaRelationships = new ArrayList<>();
        }
        deltaRelationships.add(s);
    }

    /**
     * @param s
     */
    public void addState(State s) {
        if (deltaStates == null) {
            deltaStates = new ArrayList<>();
        }
        deltaStates.add(s);
    }

    /**
     * @param s
     */
    public void addProcess(Process s) {
        if (deltaProcesses == null) {
            deltaProcesses = new ArrayList<>();
        }
        deltaProcesses.add(s);
    }

    /**
     * @param s
     */
    public void addSubject(Subject s) {
        if (deltaSubjects == null) {
            deltaSubjects = new ArrayList<>();
        }
        deltaSubjects.add(s);
    }

}
