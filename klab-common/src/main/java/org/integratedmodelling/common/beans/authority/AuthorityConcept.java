package org.integratedmodelling.common.beans.authority;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.owl.Knowledge;

import lombok.Data;

public @Data class AuthorityConcept implements IModelBean, Serializable {

    private static final long serialVersionUID = 4403390190869584633L;

    /**
     * ID suitable for a concept name
     */
    private String id;
    
    /**
     * label for display; if null, definition is used
     */
    private String label;
    
    /**
     * definition from which the authority can reconstruct the concept
     */
    private String definition;
    
    /**
     * description of concept, in format specified by format field.
     */
    private String description;
    
    /**
     * description format may be html, markdown or plain. Could use MIME here. If html,
     * should contain links to images or other links as needed and should be
     * self-contained for display. HTML responses with rich content and links are
     * encouraged.
     */
    private String descriptionFormat;

    /**
     * Definition to be used with {@link Knowledge#createConcept}
     */
    private List<String> conceptDefinition = new ArrayList<>();
    
    /**
     * If this isn't null the concept should not be used. Should report
     * wrong usage of IDs, not system errors.
     */
    private String error;
}
