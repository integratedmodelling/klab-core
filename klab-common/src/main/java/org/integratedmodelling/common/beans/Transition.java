package org.integratedmodelling.common.beans;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class Transition implements IModelBean  {
	private long startTime;
	private long endTime;
	private int timeIndex;
}
