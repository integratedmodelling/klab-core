package org.integratedmodelling.common.beans.responses;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class StateData implements IModelBean {
    private String label;
    private long[] times;
    private double[] vdata;
}
