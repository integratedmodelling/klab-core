package org.integratedmodelling.common.beans.requests;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * Messages that get sent from the Javascript side in the dataviewer
 * to the engine. They include the session ID so that any response from
 * processing them can be sent back to JS on the appropriate channel.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class ViewerNotification implements IModelBean {
    private String       sessionId;
    private String       command;
    private List<Object> arguments = new ArrayList<>();
}
