/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// - BEGIN LICENSE: -3034066901776921602 -
//
// Copyright (C) 2014-2015 by:
// - J. Luke Scott <luke@cron.works>
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - any other authors listed in the various @author annotations
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.common.beans.responses;

import java.util.Collection;
import java.util.Properties;
import java.util.UUID;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.auth.Role;
import org.integratedmodelling.common.beans.auth.UserProfile;

import lombok.Data;

/**
 * the result sent to the client in the HTTP RESPONSE for an authentication request.
 * Compatible with AuthenticationResultResource from the collaboration codebase, in case
 * the two get merged (except the latter has Mongo annotations that I really can't have
 * here).
 */
public @Data class AuthorizationResponse implements IModelBean {
    String           username;
    String           authToken;
    String           expiration;
    UserProfile      profile;
    Collection<Role> roles;

    public static AuthorizationResponse newFromCertificateContents(Properties identity) {

        AuthorizationResponse ret = new AuthorizationResponse();

        ret.authToken = UUID.randomUUID().toString();
        ret.profile = new UserProfile();
        ret.username = identity.getProperty("user");
        ret.profile.setUsername(identity.getProperty("user"));
        ret.profile.setEmail(identity.getProperty("email"));

        return ret;
    }
}
