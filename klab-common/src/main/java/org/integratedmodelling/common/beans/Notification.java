/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.integratedmodelling.api.modelling.IModelBean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Notifications are either global (sessionId == null) or session-specific;
 * they may be about a specific context or task (contextId != 0 and/or taskId != 0)
 * or carry a NEW context or task to be notified at the client end.
 * 
 * The notification body may be null in "system" notifications, where what counts is
 * the notificationClass and the payload.
 * 
 * @author Ferd
 *
 */
public @Data class Notification implements IModelBean {
	
    private String       sessionId;
    private String       userId;
    private String       contextId;
    private String       taskId;
    private String       body;
    private String       notificationClass;
    private String       exceptionClass;
    private String       levelValue;
    
    private Context      context;
    private Task         task;
    private List<Object> arguments = new ArrayList<>();

    /**
     * Remove when I figure out how to configure Jackson to do this.
     * @return the logging level correspondent to bean contents.
     */
    public Level loggingLevel() {
        return Level.parse(levelValue);
    }

    /**
     * Remove when I figure out how to configure Jackson to do this.
     * @param level
     */
    public void defineLevel(Level level) {
        levelValue = level.getName();
    }

}
