/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.beans;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.space.IGeometricShape;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

/**
 * Contains only the bare essentials for ranking and retrieving a model. These are
 * intended as query results or storable data, so there is one Model bean per observable
 * (a model with multiple outputs generates multiple Model beans, plus any inferred ones).
 * For the same reason, the observableConcept field is transient (observable contains the
 * definition of the single observable).
 * 
 * @author Ferd
 *
 */
public @Data class Model implements IModelBean, IModelMetadata {

    /**
     * Mediation type necessary for the model represented to observe the given observable.
     * 
     * @author Ferd
     *
     */
    public static enum Mediation {

        /**
         * Directly observes the stated observable
         */
        NONE,
        /**
         * Must contextualize direct observables and extract a quality (or presence)
         */
        DEREIFY_QUALITY,
        /**
         * Must extract observed trait from classified type
         */
        EXTRACT_TRAIT,
        /**
         * Infers the presence of the inherent observable from the value of its observed
         * quality
         */
        INFER_INHERENT_PRESENCE,

    }

    private String              id;
    private String              name;
    private String              serverId;
    private String              projectUrn;
    private String              projectId;
    private boolean             privateModel;
    private String              namespaceId;
    private boolean             inScenario;
    private boolean             reification;
    private boolean             hasDirectData;
    // private boolean computed;
    private boolean             hasDirectObjects;
    private boolean             spatial;
    private boolean             temporal;
    private boolean             resolved;
    private boolean             primaryObservable;
    private long                timeMultiplicity;
    private long                spaceMultiplicity;
    private long                scaleMultiplicity;
    private String              dereifyingAttribute;
    private String              observable;
    private String              observationType;
    private Metadata            metadata;
    private Mediation           mediation             = Mediation.NONE;
    private long                timeStart             = -1;
    private long                timeEnd               = -1;
    private Set<String>         neededCapabilities    = new HashSet<>();
    private Map<String, Object> ranks;
    private boolean             abstractObservable;
    private int                 minSpatialScaleFactor = ISpatialExtent.MIN_SCALE_RANK;
    private int                 maxSpatialScaleFactor = ISpatialExtent.MAX_SCALE_RANK;
    private int                 minTimeScaleFactor    = ITemporalExtent.MIN_SCALE_RANK;
    private int                 maxTimeScaleFactor    = ITemporalExtent.MAX_SCALE_RANK;

    @JsonIgnore
    transient private IConcept  observableConcept;
    @JsonIgnore
    transient private IConcept  observationConcept;
    @JsonIgnore
    transient IGeometricShape   shape;

    /*
     * Lombock should really have something with less side-effects than
     * 
     * @builder for this.
     */
    public Model copy() {
        Model ret = new Model();
        ret.id = id;
        ret.name = name;
        ret.serverId = serverId;
        ret.projectId = projectId;
        ret.projectUrn = projectUrn;
        ret.privateModel = privateModel;
        ret.namespaceId = namespaceId;
        ret.inScenario = inScenario;
        ret.reification = reification;
        ret.hasDirectData = hasDirectData;
        ret.hasDirectObjects = hasDirectObjects;
        ret.spatial = spatial;
        ret.temporal = temporal;
        ret.resolved = resolved;
        ret.primaryObservable = primaryObservable;
        ret.timeMultiplicity = timeMultiplicity;
        ret.spaceMultiplicity = spaceMultiplicity;
        ret.scaleMultiplicity = scaleMultiplicity;
        ret.dereifyingAttribute = dereifyingAttribute;
        ret.observable = observable;
        ret.observationType = observationType;
        ret.metadata = metadata == null ? null : metadata.copy();
        ret.mediation = mediation;
        ret.timeStart = timeStart;
        ret.timeEnd = timeEnd;
        ret.neededCapabilities = neededCapabilities == null ? null : new HashSet<>(neededCapabilities);
        ret.ranks = ranks == null ? null : new HashMap<>(ranks);
        ret.minSpatialScaleFactor = minSpatialScaleFactor;
        ret.maxSpatialScaleFactor = maxSpatialScaleFactor;
        ret.minTimeScaleFactor = minTimeScaleFactor;
        ret.maxTimeScaleFactor = maxTimeScaleFactor;
        ret.observableConcept = observableConcept;
        ret.observationConcept = observationConcept;
        ret.shape = shape;

        return ret;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Model) {
            return (name == null && ((Model) o).getName() == null) ||
                    (name != null &&
                            ((Model) o).getName() != null &&
                            name.equals(((Model) o).getName()));
        }
        return false;
    }

    @Override
    public int hashCode() {
        return name == null ? 0 : name.hashCode();
    }
}
