package org.integratedmodelling.common.beans;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class Interval implements IModelBean {
	private double lowerBound = 0.0;
	private double upperBound = 0.0;
	private boolean lowerOpen = false;
	private boolean upperOpen = false;
	private boolean leftInfinite = true;
	private boolean rightInfinite = true;
}
