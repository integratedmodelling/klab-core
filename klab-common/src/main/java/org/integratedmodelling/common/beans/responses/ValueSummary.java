package org.integratedmodelling.common.beans.responses;

import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class ValueSummary implements IModelBean {
    private String       description;
    private List<Double> distribution;
    private List<Double> ranges;
    private double       uncertainty;
    private String       mostLikelyClass;
}
