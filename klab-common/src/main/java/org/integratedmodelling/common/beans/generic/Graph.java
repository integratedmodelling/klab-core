package org.integratedmodelling.common.beans.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.Metadata;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.NameGenerator;
import org.jgrapht.DirectedGraph;

import lombok.Data;

/**
 * For now only strings, later metadata.
 * 
 * @author Ferd
 *
 */
public @Data class Graph implements IModelBean {

    /**
     * Descriptor class to provide labels, types and metadata for nodes and edges when
     * using adapt().
     * 
     * @author Ferd
     *
     */
    public static interface Identifier {

        /**
         * Label for a node or edge, shown in diagram.
         * 
         * @param object
         * @return
         */
        String getLabel(Object object);

        /**
         * Metadata for nodes or edges, used in visualization.
         * 
         * @param object
         * @return
         */
        IMetadata getMetadata(Object object);

        /**
         * Type, used in visualization (e.g. to choose icons or display style).
         * 
         * @param object
         * @return
         */
        String getType(Object object);

        /**
         * Label and type of a top node, used to connect all the root nodes found to.
         * Return null if not wanted (in which case a graph with one node may not be
         * visualizable).
         * 
         * @return
         */
        Pair<String, String> getTopNode();
    }

    /*
     * Used at receiving end to define which graph this is in case there are doubts. Could
     * have graph-level metadata, too.
     */
    String type;

    /*
     * each element: ID|label|type
     */
    Set<String> nodes = new HashSet<>();

    /*
     * each element: ID|sourceID|targetID|label
     */
    Set<String> relationships = new HashSet<>();

    /*
     * if objects had metadata, metadata are here, indexed by ID.
     */
    Map<String, Metadata> metadata = new HashMap<>();

    /**
     * Pass any JGrapht directed graph and an object to describe it; get back a graph
     * bean.
     * 
     * @param graph
     * @param identifier
     * @return
     */
    public static <T1, T2> Graph adapt(DirectedGraph<T1, T2> graph, Identifier identifier) {

        Graph ret = new Graph();

        Map<Object, String> ids = new HashMap<>();
        List<T1> topNodes = new ArrayList<>();

        for (T1 o : graph.vertexSet()) {
            String id = NameGenerator.shortUUID();
            String label = identifier.getLabel(o);
            String type = identifier.getType(o);
            ret.nodes.add(id + "|" + label + "|" + type);
            IMetadata md = identifier.getMetadata(o);
            if (md != null) {
                ret.metadata.put(id, KLAB.MFACTORY.adapt(md, Metadata.class));
            }
            ids.put(o, id);
            if (graph.outgoingEdgesOf(o).isEmpty()) {
                topNodes.add(o);
            }
        }

        for (T2 o : graph.edgeSet()) {
            String id = NameGenerator.shortUUID();
            T1 s = graph.getEdgeSource(o);
            T1 t = graph.getEdgeTarget(o);
            String label = identifier.getLabel(o);
            if (label == null) {
                label = "_N_";
            }
            String sid = ids.get(s);
            String tid = ids.get(t);
            IMetadata md = identifier.getMetadata(o);
            if (md != null) {
                ret.metadata.put(id, KLAB.MFACTORY.adapt(md, Metadata.class));
            }
            ret.relationships.add(id + "|" + sid + "|" + tid + "|" + label);
        }
        
        Pair<String, String> topDesc = identifier.getTopNode();
        if (topDesc != null && !topNodes.isEmpty() && graph.vertexSet().size() == 1) {
            ret.nodes.add("__topn_|" + topDesc.getFirst() + "|" + topDesc.getSecond());
            int i = 0;
            for (T1 tn : topNodes) {
                ret.relationships.add(("__topr_" + i++) + "|__topn_|" + ids.get(tn) + "| ");
            }
        }

        return ret;
    }

}
