package org.integratedmodelling.common.beans.requests;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.common.beans.responses.RunScriptResponse;

import lombok.Data;

/**
 * Request to run a script, possibly with user-specified parameters and/or within an
 * observation's context. Only accepted by local engines or with admin key.
 * 
 * If asynchronous, returns a task, otherwise return a {@link RunScriptResponse}.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class RunScriptRequest {
    private String              script;
    private Map<String, Object> parameters = new HashMap<>();
    private String              observationPath;
    private String              contextId;
    private boolean             asynchronous;
}
