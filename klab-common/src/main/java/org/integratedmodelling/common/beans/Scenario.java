package org.integratedmodelling.common.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.knowledge.Definition;
import org.integratedmodelling.common.model.runtime.AbstractContext;
import org.integratedmodelling.exceptions.KlabValidationException;

import lombok.Data;

/**
 * A scenario is specific to a context. It encodes user-supplied information that will be
 * converted into models and observations before contextualization.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class Scenario {

    private List<Observation> observations     = new ArrayList<>();
    private Set<String>       roleAttributions = new HashSet<>();

    /**
     * Register a new role for the passed target observation. 
     * 
     * @param target
     * @param role
     */
    public void addRoleAttribution(IObservation target, IConcept role) {
        roleAttributions.add(((org.integratedmodelling.common.knowledge.Observation) target).getInternalId()
                + "|" + role.getDefinition());
    }
   
    /**
     * Reconstruct the specification for a given context. Just return the
     * resolved components without actually creating the roles. Any errors
     * will just ignore the specification, allowing unknown concepts and
     * observations without error.
     * 
     * @param context
     * @return
     */
    public List<Pair<IObservation, IConcept>> findRoleAttributions(IContext context) {
        List<Pair<IObservation, IConcept>> ret = new ArrayList<>();
        for (String s : roleAttributions) {
            String[] ss = s.split("\\|");
            if (ss.length == 2) {
                try {
                    IConcept c = Definition.parse(ss[1]).reconstruct();
                    IObservation obs = ((AbstractContext)context).findWithId(ss[0]);
                    if (obs != null && c != null) { 
                        ret.add(new Pair<>(obs, c));
                    }
                } catch (KlabValidationException e) {
                    // just continue
                }
            }
        }
        return ret;
    }

    public Collection<IConcept> getRolesFor(IObservation observation) {
        
        Set<IConcept> ret = new HashSet<>();
        
        for (String s : roleAttributions) {
            String[] ss = s.split("\\|");
            if (ss.length == 2 && ss[0].equals(((org.integratedmodelling.common.knowledge.Observation)observation).getInternalId())) {
                try {
                    ret.add(Definition.parse(ss[1]).reconstruct());
                } catch (KlabValidationException e) {
                    // just continue
                }
            }
        }
        return ret;
    }
}
