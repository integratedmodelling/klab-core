package org.integratedmodelling.common.beans.responses;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.Model;

import lombok.Data;

public @Data class ModelQueryResponse implements IModelBean {

    private List<Model> models = new ArrayList<>();
    
}
