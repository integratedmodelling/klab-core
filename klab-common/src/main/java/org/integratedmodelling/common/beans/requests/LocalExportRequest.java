package org.integratedmodelling.common.beans.requests;

import java.io.File;

import org.integratedmodelling.api.data.IExport;
import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class LocalExportRequest implements IModelBean, IExport {

	private String observationId;
	private Format format;
	private Aggregation aggregation;
	private String locators;
	private File outputFile;
	
	/*
	 * type of sub-observable to collect into folder
	 */
	private String collectObservable;
}
