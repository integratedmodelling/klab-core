package org.integratedmodelling.common.beans;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class Metadata implements IModelBean {

    Map<String,Object> data = new HashMap<>();

    public Metadata() {}
    
    public Metadata(Map<String, Double> criteria) {
        data.putAll(criteria);
    }

    public Metadata copy() {
        Metadata ret = new Metadata();
        ret.data.putAll(data);
        return ret;
    }
    
}
