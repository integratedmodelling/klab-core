package org.integratedmodelling.common.beans.responses;

import lombok.Data;

/**
 * This is not really useful as a Boolean would suffice, but if we pass an
 * object we can use error management much more reliably.
 * 
 * @author Ferd
 *
 */
public @Data class LockResponse {
	private boolean locked;
	private boolean wasLocked;
	private String lockingUser;
}
