/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// - BEGIN LICENSE: -3034066901776921602 -
// 
// Copyright (C) 2014-2015 by:
// - J. Luke Scott <luke@cron.works>
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - any other authors listed in the various @author annotations
// 
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License 
// Version 3 or any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// Affero General Public License for more details.
// 
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
// 
// - END LICENSE -
package org.integratedmodelling.common.beans.requests;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * A client will authenticate using the certfile, then pass around this
 * to servers so that the same authentication can be used to obtain user
 * details and privileges without having to further reauthenticate.
 */
public @Data class SessionRequest implements IModelBean {

    String  certificate;
    String  primaryServerUrl;
    boolean requestingExclusive;
    boolean reopenExisting = true;
    String  sessionId;            // not null if we're requesting to reopen a specific session
    // compared at client side to establish if the filesystem is the same
    String  clientSignature;

    public SessionRequest() {
    }

    /*
     * FIXME when servers are integrated, remove the authEndpoint parameter and use user.getServerUrl().
     * All nodes should be able to authenticate, either autonomously or using an authoritative IM node.
     */
    public SessionRequest(String certificate, String primaryServer, boolean needExclusive, String clientSignature) {
        this.certificate = certificate;
        this.requestingExclusive = needExclusive;
        this.primaryServerUrl = primaryServer;
        this.clientSignature = clientSignature;
    }

}
