package org.integratedmodelling.common.beans.responses;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.beans.auth.UserProfile;

import lombok.Data;

/**
 * The response to a {@link API#CONNECT} request, which profiles a user from a certfile, advertises the user
 * to the network, and collects the view of the network that the user can access.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class UserView implements IModelBean {

    String      authToken;
    UserProfile userProfile;
    Network     network;

}
