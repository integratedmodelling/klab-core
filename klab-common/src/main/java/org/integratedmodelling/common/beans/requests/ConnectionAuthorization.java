package org.integratedmodelling.common.beans.requests;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.auth.UserProfile;

import lombok.Data;

/**
 * Swapped around by the network structure request, which is a depth-first search across
 * remote nodes.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class ConnectionAuthorization implements IModelBean, Serializable {

    private static final long serialVersionUID = 7113022566182954205L;

    Set<String> traversedNodes = new HashSet<>();
    String      username;
    Set<String> userGroups     = new HashSet<>();
    String      requestingIP;

    public ConnectionAuthorization() {
    }

    /*
     * This is what get sent to secondary servers to communicate that a user has been
     * successfully connected to us. It also tells them to store the user token and
     * connect it to this set of privileges.
     * 
     * Copy user data and add all local nodes in the hash, so that the downstream nodes
     * will not call them in case they also have them, to avoid infinite recursion.
     * 
     */
    public ConnectionAuthorization(ConnectionAuthorization request) {
        this.traversedNodes.addAll(request.traversedNodes);
        this.username = request.username;
        this.userGroups.addAll(request.userGroups);
        this.requestingIP = request.requestingIP;
    }

    public ConnectionAuthorization(UserProfile profile, String requestingIp) {
        this.username = profile.getUsername();
        this.userGroups.addAll(profile.getGroups());
        this.requestingIP = requestingIp;
    }

    public ConnectionAuthorization(IUser user) {
        this.username = user.getUsername();
        this.userGroups.addAll(user.getGroups());
    }

    /**
     * Equality only uses the authorization fields.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof ConnectionAuthorization))
            return false;
        ConnectionAuthorization other = (ConnectionAuthorization) obj;
        if (requestingIP == null) {
            if (other.requestingIP != null)
                return false;
        } else if (!requestingIP.equals(other.requestingIP))
            return false;
        if (userGroups == null) {
            if (other.userGroups != null)
                return false;
        } else if (!userGroups.equals(other.userGroups))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((requestingIP == null) ? 0 : requestingIP.hashCode());
        result = prime * result + ((userGroups == null) ? 0 : userGroups.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

}
