/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
/**
 * This package contains the common beans for k.LAB 1.0 REST communication. These are built at the
 * server side and reconstructed by Jackson ObjectMapper at the client.
 * 
 * All implement the IModelBean tag interface for type checking. As passing the actual objects is in most cases
 * impossible due to necessary implementation differences between client and server, I prefer to
 * have a uniform strategy to keep code clean.
 * 
 * The Model factory (accessible to all through KLAB.MFACTORY) has adapter method to/from objects 
 * that can be used at client or server side to produce the corresponding versions of the actual
 * model objects.
 */
package org.integratedmodelling.common.beans;
