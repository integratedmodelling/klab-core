package org.integratedmodelling.common.beans;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class ObservationData implements IModelBean {
    public String name;
    public String id;
    public String namespaceId;
    public String nodeId;
    public String observableName;
    public String description;
    public String geometryWKB;
    public Long   start;
    public Long   end;
    public Long   step;
    public Map<String, Object> attributes = new HashMap<>();
}
