// - BEGIN LICENSE: -3034066901776921602 -
//
// Copyright (C) 2014-2015 by:
// - J. Luke Scott <luke@cron.works>
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - any other authors listed in the various @author annotations
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.common.beans;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * Data source bean - fields are same as collaboration codebase, but less. We also add the
 * URN although that's not required from the server end (for testing only).
 * 
 * @author Ferd
 *
 */
public @Data class DataSource implements IModelBean {

    private String      urn;
    private String      id;
    private String      uuid;
    private String      name;
    private String      namespace;
    private String      theme;
    private String      type;
    private Set<String> permittedGroups = new HashSet<>();
    private boolean     publicDatasource        = false;
    private String      serverId;
    private String      createdDate;
    private String      creator;
    private String      descriptionBrief;
    private boolean     inDb;
    private URL         url;
    private String      continentCode;
    private String      countryCode;
    private String      regionCode;
    private String      projection;
    private String      serverKey;
}
