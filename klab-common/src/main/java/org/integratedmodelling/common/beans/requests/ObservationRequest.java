/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.beans.requests;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.common.beans.Scenario;
import org.integratedmodelling.common.beans.Space;
import org.integratedmodelling.common.beans.Time;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Knowledge;

import lombok.Data;

/**
 * What we send to an engine when we want to make an observation.
 * 
 * @author ferdinando.villa
 */
public @Data class ObservationRequest implements IModelBean {

	String observable;
	List<String> scenarios = new ArrayList<>();
	Space spaceForcing = null;
	Time timeForcing = null;
	String context;
	String focusObservation;
	boolean interactive;
	Scenario userScenario;

	/**
	 * If set, this is the region of interest for observables that do not have a
	 * context already assigned. It is the callee's responsibility to choose how
	 * to respond.
	 */
	double[] regionOfInterest;

	private static final String CPREFIX = "[CONCEPT]";
	
	/**
	 * Set the observable field based on the nature of the passed observable. Because
	 * since 0.9.10 compound concepts may be passed, we need a way to distinguish those,
	 * so always use parseObservable() to get the object back.
	 * 
	 * @param obs
	 */
	public void defineObservable(Object obs) {

		if (obs instanceof IModelObject) {
			if (obs instanceof IKnowledgeObject && ((IKnowledgeObject) obs).getConcept() != null) {
				obs = ((IKnowledgeObject) obs).getConcept();
			}
		}

		if (obs instanceof IConcept) {
			observable = CPREFIX + ((IConcept) obs).getDefinition();
		} else if (obs instanceof IModelObject) {
            observable = ((IModelObject)obs).getName();
        } else {
			observable = obs.toString();
		}
	}
	
	public Object parseObservable() {
		Object ret = null;
		if (observable != null) {
			if (observable.startsWith(CPREFIX)) {
				ret = Knowledge.parse(observable.substring(CPREFIX.length()));
			} else {
				// let the observing context or session figure it out.
				ret = observable;
			}
		}
		return ret;
	}

	/**
	 * Create actual forcings as an array from typed beans.
	 * 
	 * @return the forcings stated
	 */
	public Collection<IExtent> adaptForcings() {
		List<IExtent> ret = new ArrayList<>();
		if (spaceForcing != null) {
			ret.add(KLAB.MFACTORY.adapt(spaceForcing, org.integratedmodelling.common.model.runtime.Space.class));
		}
		if (timeForcing != null) {
			ret.add(KLAB.MFACTORY.adapt(timeForcing, org.integratedmodelling.common.model.runtime.Time.class));
		}
		return ret;
	}
}
