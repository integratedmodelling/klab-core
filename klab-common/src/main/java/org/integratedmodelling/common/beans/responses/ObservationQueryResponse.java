package org.integratedmodelling.common.beans.responses;

import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.Observation;

import lombok.Data;

public @Data class ObservationQueryResponse implements IModelBean {

    private List<Observation> observations;
}
