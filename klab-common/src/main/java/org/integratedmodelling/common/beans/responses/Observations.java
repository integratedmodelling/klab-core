package org.integratedmodelling.common.beans.responses;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.ObservationData;

import lombok.Data;

/**
 * Response from an observation query or an import request.
 * 
 * @author Ferd
 *
 */
public @Data class Observations implements IModelBean {
	List<ObservationData> observations = new ArrayList<>();
}
