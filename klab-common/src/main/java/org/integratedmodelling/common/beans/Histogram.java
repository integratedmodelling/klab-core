package org.integratedmodelling.common.beans;

import java.util.HashMap;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;
import lombok.Data;

public @Data class Histogram implements IModelBean {

    private String           description;
    private List<Integer>    bins;
    private List<Double>     boundaries;
    private List<String>     binLegends;
    boolean                  nodata          = false;
    long                     nodataCount     = 0;
    double                   aggregatedMean  = 0;
    double                   aggregatedTotal = Double.NaN;
    HashMap<String, Integer> occurrences;
}
