/*******************************************************************************
 * Copyright (C) 2007, 2016:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
// - BEGIN LICENSE: -3034066901776921602 -
//
// Copyright (C) 2014-2015 by:
// - J. Luke Scott <luke@cron.works>
// - Ferdinando Villa <ferdinando.villa@bc3research.org>
// - any other authors listed in the various @author annotations
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the Affero General Public License
// Version 3 or any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// Affero General Public License for more details.
//
// You should have received a copy of the Affero General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
// The license is also available at: https://www.gnu.org/licenses/agpl.html
//
// - END LICENSE -
package org.integratedmodelling.common.beans.auth;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * A convenience class which identifies the "publicly accessible" fields. The fields'
 * names and types should match their counterparts in UserData, with the exception of
 * password vs. passwordHash.
 *
 */
public @Data class UserProfile implements IModelBean, Serializable {

    private static final long serialVersionUID = 1623268053281922514L;

    public enum AccountStatus {
        active,
        locked,
        deleted,
        expired,
        pendingActivation,
    };
    
    private String        username;
    private String        email;
    private String        serverUrl;
    private String        firstName;
    private String        lastName;
    private String        initials;
    private String        address;
    private String        jobTitle;
    private String        phone;
    private String        affiliation;
    private List<Role>    roles  = new ArrayList<>(); // LDAP security roles
    private List<String>  groups = new ArrayList<>(); // research groups, etc. in web tool
    private boolean       sendUpdates;
    private String        comments;
    private String        registrationDate;
    private String        lastLogin;
    private String        lastEngineConnection;
    private AccountStatus accountStatus;

}
