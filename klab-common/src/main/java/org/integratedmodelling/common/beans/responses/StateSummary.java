package org.integratedmodelling.common.beans.responses;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.Colormap;
import org.integratedmodelling.common.beans.Histogram;

import lombok.Data;

public @Data class StateSummary implements IModelBean {
	private Colormap colormap;
	private Histogram histogram;
}
