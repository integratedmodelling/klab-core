package org.integratedmodelling.common.beans.responses;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * Exports may contain files (requested or not), model statements, datasets etc. If a
 * model statement is returned, any files created will be referred to in the model using
 * the corresponding local paths specified in fileReferences.
 * 
 * @author Ferd
 */
public @Data class LocalExportResponse implements IModelBean {

    private String       artifactName;
    private List<String> files = new ArrayList<>();
    private String       relativeExportPath;
    private String       modelStatement;
    private boolean      model;
    private boolean      dataset;
}
