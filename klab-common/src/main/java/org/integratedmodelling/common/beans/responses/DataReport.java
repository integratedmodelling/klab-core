package org.integratedmodelling.common.beans.responses;

import org.integratedmodelling.api.monitoring.IMonitor;

import lombok.Data;

/**
 * Report on data encountered at engine side. Filled through
 * {@link IMonitor#send(Object)} and instantiated through a KlabDataException
 * (to be written).
 * 
 * @author fvilla
 *
 */
public @Data class DataReport {

	private String urn;
	private boolean error;
	private String erroDescription;
	private String projectId;
	private String namespaceId;
	private int lineNumber;
}
