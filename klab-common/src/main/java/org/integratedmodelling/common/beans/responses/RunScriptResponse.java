package org.integratedmodelling.common.beans.responses;

import org.integratedmodelling.common.beans.Task;

import lombok.Data;

public @Data class RunScriptResponse {
    private Task task;
    private long timeElapsed;
    private Object result;
    private String exception;
}
