package org.integratedmodelling.common.beans.requests;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

/**
 * Local deploy request for a resource that is already in the engine's
 * filesystem. Used for different deploys - projects and importing
 * observations.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class DeployRequest implements IModelBean {
	String path;
	String id;
	long timestamp;
}
