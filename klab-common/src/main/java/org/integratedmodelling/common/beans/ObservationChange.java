package org.integratedmodelling.common.beans;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class ObservationChange implements IModelBean {
    private String observationId;
}
