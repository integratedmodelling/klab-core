package org.integratedmodelling.common.beans;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.modelling.IModelBean;

public class ModelBean implements IModelBean {

    public Version version;
    
    protected ModelBean(String version) {
        this.version = Version.parse(version);
    }
    
}
