/*******************************************************************************
 *  Copyright (C) 2007, 2016:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.beans.responses;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.Service;
import org.integratedmodelling.common.beans.auth.UserProfile;
import org.integratedmodelling.common.beans.authority.Authority;
import org.integratedmodelling.common.client.palette.Palette;

import lombok.Data;

/**
 * In a modeling engine, the capabilities are those corresponding to the certificate it
 * runs with. In a k.LAB node, the capabilities are filtered according to the user that
 * requests them. If a node is requesting, the capabilities are unfiltered.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class Capabilities implements IModelBean {

    String          name;
    String          version;
    String          build;
    String          worldview;
    List<Service>   services                = new ArrayList<>();
    List<Service>   functions               = new ArrayList<>();
    List<String>    componentUrns           = new ArrayList<>();
    List<String>    synchronizedProjectUrns = new ArrayList<>();
    List<String>    staticResourceIds       = new ArrayList<>();
    List<Authority> authorities             = new ArrayList<>();
    List<Palette>   toolkits                = new ArrayList<>();
    long            observationCount;
    long            modelCount;
    String          bootMode;
    UserProfile     engineUser;
    String          communicationChannel;
}
