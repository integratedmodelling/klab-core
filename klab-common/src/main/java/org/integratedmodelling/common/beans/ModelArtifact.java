package org.integratedmodelling.common.beans;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.monitoring.IMonitor;

import lombok.Data;

/**
 * Sent through {@link IMonitor#send(Object)} when a contextualizer makes an artifact 
 * available in its context observation.
 * 
 * @author Ferd
 *
 */
public @Data class ModelArtifact implements IModelBean {

    private String artifactType;
    private String observationId;
    private String artifactName;
    
    public ModelArtifact() {}
    public ModelArtifact(String artifactName, String artifactType, String observationId) {
        this.artifactName = artifactName;
        this.artifactType = artifactType;
        this.observationId = observationId;
    }
}
