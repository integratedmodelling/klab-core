package org.integratedmodelling.common.beans.requests;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * Messages that get sent from the modeler to the Javascript side in the dataviewer.
 * 
 * @author ferdinando.villa
 *
 */
public @Data class ViewerCommand {

    private String       command;
    private List<Object> arguments = new ArrayList<>();

    @SuppressWarnings("javadoc")
    public ViewerCommand() {
    }

    /**
     * Easy constructor.
     * 
     * @param command
     * @param arguments
     */
    public ViewerCommand(String command, Object... arguments) {
        this.command = command;
        if (arguments != null) {
            for (Object arg : arguments) {
                this.arguments.add(arg);
            }
        }
        
        /* remove trailing nulls */
        int cutat = -1;
        for (int i = this.arguments.size() - 1; i >= 0; i--) {
            if (this.arguments.get(i) != null) {
                break;
            }
            cutat = i;
        }
        
        if (cutat >=0) {
            List<Object> narguments = new ArrayList<>();
            for (int i = 0; i < cutat; i++) {
                narguments.add(this.arguments.get(i));
            }
            this.arguments = narguments;
        }
    }
}
