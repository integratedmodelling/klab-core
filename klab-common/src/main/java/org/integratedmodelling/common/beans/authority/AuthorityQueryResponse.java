package org.integratedmodelling.common.beans.authority;

import java.util.ArrayList;
import java.util.List;

import org.integratedmodelling.api.metadata.ISearchResult;
import org.integratedmodelling.api.modelling.IModelBean;

public class AuthorityQueryResponse implements ISearchResult<AuthorityConcept>, IModelBean {
    
    /**
     * set to -1 if result is not paged
     */
    private int page;
    
    /**
     * set to true if there are (or there may be) more
     * results in a subsequent page. Not checked if 
     * pageNumber == -1.
     */
    private boolean moreMatches;

    /**
     * The results themselves
     */
    private List<AuthorityConcept> matches = new ArrayList<>();
    
    /**
     * If not null, there was an error
     */
    private String error;

    @Override
    public List<AuthorityConcept> getMatches() {
        return matches;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public boolean isMoreMatches() {
        return moreMatches;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setMoreMatches(boolean moreMatches) {
        this.moreMatches = moreMatches;
    }

    public void setMatches(List<AuthorityConcept> matches) {
        this.matches = matches;
    }
    
    public String getError() {
        return error;
    }
    
    public void setError(String error) {
        this.error = error;
    }
    
}
