package org.integratedmodelling.common.beans.authority;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.owl.Knowledge;

import lombok.Data;

/**
 * Structure communicated by an engine to instruct the receiving end on how to create an
 * authority. Authorities are communicated as part of an engine's capabilities.
 * 
 * Authorities can include one or more different ones, handling different identities.
 * These are presented with a path-like name, such as GBIF.SPECIES or GBIF.FAMILY.
 * 
 * For each authority we create one ontology, with an initial content specified by
 * {@link #initialConcepts} and {@link #initialProperties}. These are created using
 * {@link Knowledge#createConcept(String, org.integratedmodelling.api.knowledge.IOntology)}.
 * 
 * Authorities are specific of a worldview, and won't be used unless the worldview in the
 * user's certificate is the same.
 * 
 * @author Ferd
 *
 */
public @Data class Authority implements IModelBean {

    private String       name;
    String               version;
    String               overallDescription;
    String               worldview;
    boolean              searchable;
    private List<String> authorities           = new ArrayList<>();
    private List<String> authorityDescriptions = new ArrayList<>();
    String               ontologyId;
    List<String>         initialConcepts       = new ArrayList<>();
    List<String>         initialProperties     = new ArrayList<>();
    Map<String, String>  coreConcepts          = new HashMap<>();
}
