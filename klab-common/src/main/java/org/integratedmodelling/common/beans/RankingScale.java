package org.integratedmodelling.common.beans;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class RankingScale implements IModelBean {

    double  lowerBound;
    double  upperBound;
    boolean integerScale;
    boolean bounded;
}
