package org.integratedmodelling.common.beans;

import java.util.List;

import org.integratedmodelling.api.modelling.IModelBean;

import lombok.Data;

public @Data class Colormap implements IModelBean {
    int        levels;
    boolean    transparentZero;
    List<Byte> red;
    List<Byte> green;
    List<Byte> blue;
    List<Byte> alpha;
}
