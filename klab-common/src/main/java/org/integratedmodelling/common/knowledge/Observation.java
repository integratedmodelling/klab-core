/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.knowledge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservable;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IProcess;
import org.integratedmodelling.api.modelling.IRelationship;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.beans.Metadata;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.runtime.AbstractContext;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.owl.Individual;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;

/**
 * Base observation.
 * 
 * @author ferdinando.villa
 */
public abstract class Observation extends Individual implements IObservation {

    public static interface ObservationVisitor {

        void visitObservation(Observation observation);

        void visitState(IState observation);

        void visitSubject(ISubject observation);

        void visitEvent(IEvent observation);

        void visitRelationship(IRelationship observation);

        void visitProcess(IProcess observation);

    }

    // API here is a bit primitive. May want to unify with explicitRoles, which
    // basically act as a faster way to check for a <role, null, null> triple.
    public static class ContextualRole {
        public ContextualRole(IConcept role, IConcept target, IConcept context) {
            this.role = role;
            this.target = target;
            this.context = context;
        }

        public IConcept role;
        public IConcept target;
        public IConcept context;
    }

    protected IObservable          observable;
    protected IScale               scale;
    protected String               id;
    protected String               parentId;
    protected IActuator            actuator;

    // used only for special situations when we need a parent but don't want to
    // link the observation up to it
    protected IDirectObservation   parent;
    protected IModel               model           = null;
    protected Set<IConcept>        explicitRoles   = new HashSet<>();
    protected List<ContextualRole> contextualRoles = new ArrayList<>();
    // protected org.integratedmodelling.common.metadata.Metadata metadata;
    protected boolean              initialized     = false;

    // these 2 are just a cache
    private boolean                osChecked;
    private ISubject               observingSubject;
    private int                    priorityOrder   = 0;

    /**
     * Checked before any modification is sent out
     */
    private boolean seenByClient = false;
    
    public Observation() {
        this.id = NameGenerator.shortUUID();
    }

    /*
     * used by contextualizers
     */
    public void setInitialized(boolean b) {
        this.initialized = b;
    }

    public void visit(ObservationVisitor visitor) {

        visitor.visitObservation(this);

        if (this instanceof IDirectObservation) {

            for (IState s : ((IDirectObservation) this).getStates()) {
                ((Observation) s).visit(visitor);
            }

            if (this instanceof IProcess) {
                visitor.visitProcess((IProcess) this);
            } else if (this instanceof IEvent) {
                visitor.visitEvent((IEvent) this);
            } else if (this instanceof IRelationship) {
                visitor.visitRelationship((IRelationship) this);
            } else if (this instanceof ISubject) {
                visitor.visitSubject((ISubject) this);
                for (IProcess s : ((ISubject) this).getProcesses()) {
                    ((Observation) s).visit(visitor);
                }
                for (IEvent s : ((ISubject) this).getEvents()) {
                    ((Observation) s).visit(visitor);
                }
                for (ISubject s : ((ISubject) this).getSubjects()) {
                    ((Observation) s).visit(visitor);
                }
                for (IRelationship s : ((ISubject) this).getStructure()
                        .getRelationships()) {
                    ((Observation) s).visit(visitor);
                }
            }
        } else if (this instanceof IState) {
            visitor.visitState((IState) this);
        }
    }

    public void resetExplicitRoles() {

        explicitRoles.clear();

        if (this instanceof IDirectObservation) {
            for (IState s : ((IDirectObservation) this).getStates()) {
                ((Observation) s).resetExplicitRoles();
            }
            if (this instanceof ISubject) {
                for (IProcess s : ((ISubject) this).getProcesses()) {
                    ((Observation) s).resetExplicitRoles();
                }
                for (IEvent s : ((ISubject) this).getEvents()) {
                    ((Observation) s).resetExplicitRoles();
                }
                for (ISubject s : ((ISubject) this).getSubjects()) {
                    ((Observation) s).resetExplicitRoles();
                }
                for (IRelationship s : ((ISubject) this).getStructure()
                        .getRelationships()) {
                    ((Observation) s).resetExplicitRoles();
                }
            }
        }
    }

    /**
     * Called after the subject is resolved and a resolution scope exists.
     */
    public void defineObservable() {
        ((Observable) this.observable).setTraitsAndRoles();
    }

    public void addRole(IConcept role) {
        explicitRoles.add(role);
    }

    public void addRole(IConcept role, IConcept target, IConcept context) {
        contextualRoles.add(new ContextualRole(role, target, context));
    }

    public Collection<IConcept> getExplicitRoles() {
        return explicitRoles;
    }

    public Collection<ContextualRole> getContextualRoles() {
        return contextualRoles;
    }

    protected void setObservable(IObservableSemantics observableSemantics, IContext context, String name) {

        this.context = context;
        define(observableSemantics.getObservationType(), id, context);
        this.observable = new Observable(observableSemantics, this, name, context);
        IProperty link = KLAB.p(NS.CONTEXTUALIZES);
        ((Ontology) ((AbstractContext) context).getOntology())
                .linkIndividuals(this, this.observable, link);
    }

    @Override
    public IObservable getObservable() {
        return observable;
    }

    @Override
    public IScale getScale() {
        return scale;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Observation && id.equals(((Observation) obj).id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public void setParent(IDirectObservation parent) {
        this.parent = parent;
    }

    /**
     * Get the internal ID, which forms the path. Identical to the one given in the
     * engine.
     * 
     * @return internal ID of the observation.
     */
    public String getInternalId() {
        return id;
    }

    @Override
    public IDirectObservation getContextObservation() {
        return parent == null ? (parentId == null || context == null ? null
                : (IDirectObservation) ((AbstractContext) context).findWithId(parentId))
                : parent;
    }

    @SuppressWarnings("javadoc")
    public void deserialize(org.integratedmodelling.common.beans.Observation bean) {
        this.scale = KLAB.MFACTORY.adapt(bean.getScale(), Scale.class);
        this.id = bean.getId();
        this.parentId = bean.getParentId();
        this.observable = new Observable(KLAB.MFACTORY
                .adapt(bean
                        .getObservable(), ObservableSemantics.class), this, id, getContext());
        if (bean.getMetadata() != null) {
            this.metadata = KLAB.MFACTORY
                    .adapt(bean
                            .getMetadata(), org.integratedmodelling.common.metadata.Metadata.class);
        }
    }

    @SuppressWarnings("javadoc")
    public void serialize(org.integratedmodelling.common.beans.Observation ret) {

        ret.setObservation(this);
        ret.setObservable(KLAB.MFACTORY.adapt(getObservable()
                .getSemantics(), org.integratedmodelling.common.beans.Observable.class));
        ret.setScale(KLAB.MFACTORY
                .adapt(getScale(), org.integratedmodelling.common.beans.Scale.class));
        ret.setId(getInternalId());
        ret.setParentId(parentId);
        ret.setMetadata(KLAB.MFACTORY.adapt(getMetadata(), Metadata.class));
    }

    @SuppressWarnings("javadoc")
    public void setParentId(String id) {
        this.parentId = id;
    }

    /**
     * Find observation with passed ID. Real implementation in subclasses.
     * 
     * @param id
     * @return observation with that ID, or null
     */
    public IObservation find(String id) {
        return this.id.equals(id) ? this : null;
    }

    @SuppressWarnings("javadoc")
    public void append(IObservation observation) {
    }

    @SuppressWarnings("javadoc")
    public void setContext(IContext context) {
        this.context = context;
    }

    /**
     * @return the context of the observation, if "alive"
     */
    @Override
    public IContext getContext() {
        return this.context;
    }

    public void setActuator(IActuator actuator) {
        this.actuator = actuator;
    }

    public IActuator getActuator() {
        return this.actuator;
    }

    public String getParentId() {
        return parentId;
    }

    @Override
    public ISubject getObservingSubject() {
        if (!osChecked) {
            String id = ((ObservableSemantics) getObservable().getSemantics())
                    .getObservingSubjectId();
            observingSubject = id == null ? null
                    : (ISubject) ((AbstractContext) getContext()).findWithId(id);
            osChecked = true;
        }
        return observingSubject;
    }

    public Collection<IObservation> select(Object... args) {
        return retrieve(new ArrayList<>(), false, true, args);
    }

    public Collection<IObservation> siblings(Object... args) {
        return retrieve(new ArrayList<>(), false, false, args);
    }

    public Collection<IObservation> selectAll(Object... args) {
        return retrieve(new ArrayList<>(), true, true, args);
    }

    /**
     * Return every child in our context, optionally digging down into the direct
     * observations.
     * 
     * @param recursive
     * @return
     */
    public Collection<IObservation> retrieveAll(boolean recursive) {
        List<IObservation> ret = new ArrayList<>();
        return retrieveAll(ret, recursive);
    }

    private Collection<IObservation> retrieveAll(List<IObservation> ret, boolean recursive) {

        if (this instanceof IDirectObservation)
            for (IObservation obs : ((IDirectObservation) this).getStates()) {
                ret.add(obs);
            }

        if (this instanceof ISubject) {
            for (IObservation obs : ((ISubject) this).getSubjects()) {
                if (!((IActiveDirectObservation) obs).isActive()) {
                    continue;
                }
                ret.add(obs);
                if (recursive) {
                    ((Observation) obs).retrieveAll(ret, recursive);
                }
            }
            for (IObservation obs : ((ISubject) this).getEvents()) {
                if (!((IActiveDirectObservation) obs).isActive()) {
                    continue;
                }
                ret.add(obs);
                if (recursive) {
                    ((Observation) obs).retrieveAll(ret, recursive);
                }
            }
            for (IObservation obs : ((ISubject) this).getProcesses()) {
                if (!((IActiveDirectObservation) obs).isActive()) {
                    continue;
                }
                ret.add(obs);
                if (recursive) {
                    ((Observation) obs).retrieveAll(ret, recursive);
                }
            }
            for (IObservation obs : ((ISubject) this).getStructure().getRelationships()) {
                if (!((IActiveDirectObservation) obs).isActive()) {
                    continue;
                }
                ret.add(obs);
                if (recursive) {
                    ((Observation) obs).retrieveAll(ret, recursive);
                }
            }
        }
        return ret;
    }

    public Collection<IObservation> retrieve(List<IObservation> result, boolean recursive, boolean useChildren, Object... args) {

        if (!(this instanceof IDirectObservation)) {
            return result;
        }

        IDirectObservation target = (IDirectObservation) this;
        if (!useChildren) {
            // siblings
            target = this.getContextObservation();
            if (target == null) {
                return result;
            }
        }
        for (Object arg : args) {

            if (arg instanceof IConcept) {

                if (NS.isQuality((IConcept) arg)) {
                    for (IObservation obs : target.getStates()) {
                        if (obs.getObservable().getType().is((IConcept) arg)) {
                            result.add(obs);
                        }
                    }
                } else if (target instanceof ISubject) {
                    if (NS.isThing((IConcept) arg)) {
                        for (IObservation obs : ((ISubject) target).getSubjects()) {

                            if (!((IActiveDirectObservation) obs).isActive() || obs.equals(this)) {
                                // when looking for siblings, this can cause infinite
                                // recursion.
                                continue;
                            }

                            if (obs.getObservable().getType().is((IConcept) arg)) {
                                result.add(obs);
                            }
                            if (recursive) {
                                ((Observation) obs)
                                        .retrieve(result, recursive, useChildren, args);
                            }
                        }

                    } else if (NS.isEvent((IConcept) arg)) {

                        for (IObservation obs : ((ISubject) target).getEvents()) {
                            if (((IActiveDirectObservation) obs).isActive()
                                    && obs.getObservable().getType().is((IConcept) arg)) {
                                result.add(obs);
                            }
                        }

                    } else if (NS.isRelationship((IConcept) arg)) {

                        for (IObservation obs : ((ISubject) target).getStructure()
                                .getRelationships()) {
                            if (((IActiveDirectObservation) obs).isActive()
                                    && obs.getObservable().getType().is((IConcept) arg)) {
                                result.add(obs);
                            }
                        }
                    }
                } else if (NS.isTrait((IConcept) arg)) {
                    /*
                     * TODO check every single child
                     */
                } else if (NS.isRole((IConcept) arg)) {
                    /*
                     * TODO check every single child
                     */
                }

            } else if (arg instanceof String) {
                /*
                 * TODO check all direct observables for (simple) name match
                 */
            }
        }
        return result;
    }

    /**
     * Return our priority in the dataflow we're in, if any. If this is 0, we can
     * contextualize concurrently with others having 0 order.
     * 
     * @return
     */
    public int getPriorityOrder() {
        return this.priorityOrder;
    }

    public void setPriorityOrder(int n) {
        this.priorityOrder = n;
    }

    public void setSeenByClient(boolean b) {
        seenByClient = b;
    }
}
