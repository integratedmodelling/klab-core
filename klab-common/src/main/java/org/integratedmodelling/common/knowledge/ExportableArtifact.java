package org.integratedmodelling.common.knowledge;

import java.util.Collection;
import java.util.HashSet;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IIndividual;
import org.integratedmodelling.api.knowledge.IObservable;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.utils.NameGenerator;

/**
 * Peer for an exportable artifact linked to a direct observation. It's an observation for
 * tangential reasons (must fit in a tree of observations). For now these can only be
 * models or datasets - later we may want to use an enum for types. At the client side,
 * all we need is a parent and an ID so we can ask for all the export artifacts to the
 * engine.
 * 
 * @author Ferd
 *
 */
public class ExportableArtifact implements IObservation {

    String             id;
    String             name;
    boolean            isRelationship = false;
    IMetadata          metadata       = new Metadata();
    IDirectObservation parent;
    boolean            isModel;
    String             label;

    public ExportableArtifact(String name, IDirectObservation parent, boolean isModel) {
        this.id = NameGenerator.shortUUID();
        if (name.contains("|")) {
            String[] ss = name.split("\\|");
            this.name = ss[0];
            this.label = ss[1];
        } else {
            this.name = name;
        }
        this.parent = parent;
        this.isModel = isModel;
    }

    @Override
    public IObservable getObservable() {
        return null;
    }

    public boolean isRelationshipFolder() {
        return isRelationship;
    }

    public boolean isModel() {
        return isModel;
    }

    public String getLabel() {
        return label == null ? (isModel ? "model" : "dataset") : label;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public IScale getScale() {
        return parent.getScale();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ExportableArtifact && ((ExportableArtifact) o).id.equals(id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public IConcept getType() {
        return null;
    }

    @Override
    public IDirectObservation getContextObservation() {
        return parent;
    }

    @Override
    public Collection<IIndividual> getIndividuals(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<Object> getData(IProperty property) {
        return new HashSet<>();
    }

    @Override
    public Collection<IProperty> getObjectRelationships() {
        return new HashSet<>();
    }

    @Override
    public Collection<IProperty> getDataRelationships() {
        return new HashSet<>();
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    public IDirectObservation getParent() {
        return parent;
    }

    @Override
    public boolean is(ISemantic type) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IContext getContext() {
        return parent.getContext();
    }

    @Override
    public ISubject getObservingSubject() {
        // TODO Auto-generated method stub
        return null;
    }
}
