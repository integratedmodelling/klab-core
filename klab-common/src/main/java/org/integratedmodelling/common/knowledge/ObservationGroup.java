package org.integratedmodelling.common.knowledge;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IIndividual;
import org.integratedmodelling.api.knowledge.IObservable;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.NS;

/**
 * Used to group together observations of the same type for export and display.
 * 
 * @author Ferd
 *
 */
public class ObservationGroup extends ArrayList<IObservation> implements IObservation {

    private static final long serialVersionUID = -6948404167166780198L;
    IObservable               observable;
    String                    id;
    boolean                   isRelationship   = false;
    IMetadata                 metadata         = new Metadata();
    ISubject                  parent;

    public ObservationGroup(IConcept observable, ISubject parent) {
        this.id = NameGenerator.shortUUID();
        if (observable == null) {
            this.isRelationship = true;
        } else {
            this.observable = new Observable(observable, id, parent.getContext());
        }
        this.parent = parent;
    }

    @Override
    public IObservable getObservable() {
        return observable;
    }

    public boolean isRelationshipFolder() {
        return isRelationship;
    }

    public String getId() {
        return id;
    }

    @Override
    public IScale getScale() {
        return parent.getScale();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof ObservationGroup && ((ObservationGroup) o).id.equals(id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public IConcept getType() {
        return observable.getType();
    }

    @Override
    public IDirectObservation getContextObservation() {
        return parent;
    }

    @Override
    public Collection<IIndividual> getIndividuals(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<Object> getData(IProperty property) {
        return new HashSet<Object>();
    }

    @Override
    public Collection<IProperty> getObjectRelationships() {
        return new HashSet<IProperty>();
    }

    @Override
    public Collection<IProperty> getDataRelationships() {
        return new HashSet<IProperty>();
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public boolean is(ISemantic type) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IContext getContext() {
        return parent.getContext();
    }

    @Override
    public ISubject getObservingSubject() {
        // TODO Auto-generated method stub
        return null;
    }

    public void collectObservations() {
        if (NS.isThing(getType())) {
            for (ISubject s : parent.getSubjects()) {
                if (s.getObservable().getType().equals(getType())) {
                    add(s);
                }
            }
        } else if (NS.isEvent(getType())) {
            for (IEvent s : parent.getEvents()) {
                if (s.getObservable().getType().equals(getType())) {
                    add(s);
                }
            }
        }
    }
}
