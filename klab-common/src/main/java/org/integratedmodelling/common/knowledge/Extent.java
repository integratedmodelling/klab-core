package org.integratedmodelling.common.knowledge;

import java.util.Iterator;

import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IScale;

public abstract class Extent extends Observation implements IExtent {

    IScale scale;
    
    @Override
    public Iterator<IExtent> iterator() {
        return new Iterator<IExtent>() {

            int i = 0;
            
            @Override
            public boolean hasNext() {
                return i < (getMultiplicity() - 1);
            }

            @Override
            public IExtent next() {
                return getExtent(i++);
            }
            
        };
    }
    
    @Override
    public IScale getScale() {
        return scale;
    }
    
    public void setScale(IScale scale) {
        this.scale = scale;
    }
   
}
