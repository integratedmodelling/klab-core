package org.integratedmodelling.common.knowledge;

import java.util.Collection;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservable;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Individual;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Roles;
import org.integratedmodelling.common.vocabulary.Traits;

public class Observable extends Individual implements IObservable {

    private IObservableSemantics semantics;
    private IObservation         observation;

    /**
     * Simple constructor that just makes a simple individual. The usual way to create an
     * observable is through the ObservableSemantics instantiate() method. setTraits should
     * always be true unless we're creating a dummy observable that won't be used.
     * 
     * @param observable
     */
    public Observable(IConcept observable, String name, IContext context, boolean setTraits) {
        super(observable, name, context);
        this.semantics = new ObservableSemantics(observable);
        if (setTraits) {
            setTraitsAndRoles();
        }
    }
    
    /**
     * Simple constructor that just makes a simple individual. The usual way to create an
     * observable is through the ObservableSemantics instantiate() method.
     * 
     * @param observable
     */
    public Observable(IConcept observable, String name, IContext context) {
        this(observable, name, context, true);
    }

    public Observable(IObservableSemantics observable, IObservation observation, String name,
            IContext context) {
        super(observable.getType(), name, context);
        this.semantics = observable;
        this.observation = observation;
        setTraitsAndRoles();
    }

    void setTraitsAndRoles() {

        /*
         * incarnate all traits; if abstract, resolve in context.
         */
        for (IConcept t : Traits.getTraits(getType())) {

            if (t.isAbstract()) {
                /*
                 * TODO resolve to concrete trait
                 */
            }

            boolean done = false;
            IConcept base = NS.getBaseParentTrait(t);
            if (base != null) {
                String prop = base.getMetadata().getString(NS.TRAIT_RESTRICTING_PROPERTY);
                if (prop != null) {
                    IProperty p = KLAB.KM.getProperty(prop);
                    if (p != null && this.context != null) {
                        ((Ontology) getOntology()).linkIndividuals(this, ((Ontology) t.getOntology())
                                .getSingletonIndividual(t), p);
                        done = true;
                    }
                }
                if (!done) {
                    KLAB.warn("internal: cannot attribute trait " + t + " to individual " + this
                            + " for lack of trait metadata");
                }
            }
        }

        /*
         * incarnate all roles; if abstract, resolve in context.
         */
        if (context != null && semantics != null) {
            for (IConcept r : Roles.getRoles(this.semantics, context.getScope())) {

                if (r.isAbstract()) {
                    /*
                     * TODO resolve to concrete role - for now just attribute
                     */
                }

                IProperty p = KLAB.KM.getProperty(NS.HAS_ROLE_PROPERTY);
                if (p != null && this.context != null) {
                    ((Ontology) getOntology())
                            .linkIndividuals(this, ((Ontology) r.getOntology()).getSingletonIndividual(r), p);
                    
                    System.out.println("ROLE " + r + " ADDED TO " + semantics);
                }
            }
        }
    }

    @Override
    public IConcept getType() {
        IConcept ret = super.getType();
        if (ret == null && semantics != null) {
            ret = semantics.getType();
        }
        return ret;
    }

    /**
     * Cloning constructor
     * 
     * @param observable
     */
    public Observable(Observable observable) {
        super(observable);
        this.semantics = observable.getSemantics();
        this.observation = observable.observation;
    }

    @Override
    public IObservableSemantics getSemantics() {
        return semantics;
    }

    @Override
    public IObservation getObservation() {
        return observation;
    }

    @Override
    public IObservable getContext() {
        return observation == null ? null
                : (observation.getContextObservation() == null ? null
                        : observation.getContextObservation().getObservable());
    }

    @Override
    public boolean hasTrait(IConcept trait) {
        Collection<IConcept> roles = Traits.getTraits(getType());
        if (roles.contains(trait)) {
            return true;
        }
        for (IConcept r : roles) {
            if (r.is(trait)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "[O " + getSemantics() + "]";
    }

    @Override
    public boolean hasAttribute(IConcept attribute) {
        Collection<IConcept> roles = Traits.getAttributes(getType());
        if (roles.contains(attribute)) {
            return true;
        }
        for (IConcept r : roles) {
            if (r.is(attribute)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasRealm(IConcept realm) {
        Collection<IConcept> roles = Traits.getRealms(getType());
        if (roles.contains(realm)) {
            return true;
        }
        for (IConcept r : roles) {
            if (r.is(realm)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean hasIdentity(IConcept identity) {
        Collection<IConcept> roles = Traits.getIdentities(getType());
        if (roles.contains(identity)) {
            return true;
        }
        for (IConcept r : roles) {
            if (r.is(identity)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isCompatible(ISemantic obs) {
        return Observables.isCompatible(getType(), obs.getType());
    }

}
