package org.integratedmodelling.common.knowledge;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.jtopas.ReaderSource;
import org.integratedmodelling.common.utils.jtopas.StandardTokenizer;
import org.integratedmodelling.common.utils.jtopas.StandardTokenizerProperties;
import org.integratedmodelling.common.utils.jtopas.Token;
import org.integratedmodelling.common.utils.jtopas.Tokenizer;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Observables.Qualities;
import org.integratedmodelling.common.vocabulary.Traits;
import org.integratedmodelling.common.vocabulary.authority.AuthorityFactory;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

import com.google.common.base.CharMatcher;

/**
 * A definition is a logical formula that creates a derived concept from stated ones. The
 * concept can be composed with traits, contexts, inherent types, "by" types for
 * conditional classification of qualities, and restricted closures for "down to"
 * specifications. It also admits modifiers such as negation, observability, or any of the
 * observers that create new observables from existing ones, including the definition of
 * secondary concepts when necessary (e.g. ratios).
 * 
 * A definition will never contain another derived concept; only stated concepts are
 * admitted throughout.
 * 
 * Passed around in queries so that it can be reconstructed at an end that has not seen
 * the same usage.
 * 
 * All "composed" concepts carry their definition with them in the klab:conceptDefinition
 * annotation property.
 * 
 * The modifier O allows to specify the ontology where any new concept must be created. If
 * that is absent, it defaults to the ontology hosting the main concept.
 * 
 * Definition are created from a string using {@link #parse(String)} and each concept can
 * return its definition with {@link IConcept#getDefinition()}. Definitions are not built
 * by users, but are set into derived concepts by the functions that create them.
 * 
 * Grammar for the definition is:
 * 
 * <pre> Definition: Modifier?
 * (NAMESPACE:ID)|'('Definition')')(,(NAMESPACE:ID)|'('Definition')')? ('+' Component*)?
 * Component: [T|C|I|O|R|B|D] '(' Definition (',' Definition)* ')' Modifier:
 * not|observability|observable|ObserverType ObserverType:
 * count|distance|presence|probability|proportion|ratio|type|uncertainty|value </pre>
 * 
 * As a special case, the definition admits 'distance #x#y where y is a namespace and x
 * something that the concept should express the distance from.
 * 
 * If the group O(ontologyId) is also given after +, the concept is created in the
 * specified ontology; otherwise it will be created in the ontology that contains the main
 * concept. The O group can only appear in concepts that have components.
 * 
 * Example:
 * 
 * onto:BaseConcept+T(onto:Trait1,onto:Trait2),I(onto:InherentConcept+C(onto:
 * ContextConcept))
 * 
 * @author ferdinando.villa
 *
 */
public class Definition {

    Definition       mainConcept;
    String           baseConcept;
    String           ontology;
    String           distanceSpecs;
    List<Definition> traits;
    List<Definition> roles;
    Definition       context;
    Definition       inherent;
    Definition       otherConcept;
    Definition       byTrait;
    Definition       downTo;

    String           authority;
    String           authorityTerm;

    /*
     * modifiers
     */
    boolean          denied;
    boolean          observability;
    boolean          observable;
    Qualities        observer;

    private Definition() {
    }

    boolean isTrivial() {
        return baseConcept != null && !denied && !observability && !observable
                && traits == null
                && context == null && inherent == null && observer == null
                && otherConcept == null &&
                distanceSpecs == null && roles == null && byTrait == null;
    }

    public static Definition parse(String s) throws KlabValidationException {

        /*
         * remove dot from properties so that we can capture ontology names with dots in
         * them. FIXME does not seem to work. earth:Region gives earth, ':', then 'R' as
         * separator.
         */
        StandardTokenizerProperties properties = new StandardTokenizerProperties();
        properties.setSeparators("(),+:");
        properties.addString("\"", "\"", "\\");

        try (Tokenizer tokenizer = new StandardTokenizer(properties)) {

            tokenizer.setSource(new ReaderSource(new StringReader(s)));
            Definition ret = new Definition();
            if (tokenizer.hasMoreTokens()) {
                readDefinition(tokenizer, tokenizer.nextToken(), ret);
                return ret;
            }
        }
        return null;
    }

    private static Token readDefinition(Tokenizer tokenizer, Token token, Definition ret)
            throws KlabValidationException {

        /*
         * modifiers first - only one allowed.
         */
        boolean admitOther = false;

        if (token.getImage().equals("not")) {
            ret.denied = true;
            token = tokenizer.nextToken();
        } else if (token.getImage().equals("authority")) {
            token = tokenizer.nextToken();
            ret.authority = tokenizer.expect(token, Token.NORMAL);
            token = tokenizer.nextToken();
            ret.authorityTerm = CharMatcher.is('\"').trimFrom(tokenizer.expect(token, Token.STRING));
            // authority reference ends here, nothing else admitted
            return tokenizer.nextToken();
        } else if (token.getImage().equals("observability")) {
            ret.observability = true;
            token = tokenizer.nextToken();
        } else if (token.getImage().equals("observable")) {
            ret.observable = true;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.COUNT.name().toLowerCase())) {
            ret.observer = Observables.Qualities.COUNT;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.ASSESSMENT.name().toLowerCase())) {
            ret.observer = Observables.Qualities.ASSESSMENT;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.DISTANCE.name().toLowerCase())) {
            ret.observer = Observables.Qualities.DISTANCE;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.PRESENCE.name().toLowerCase())) {
            ret.observer = Observables.Qualities.PRESENCE;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.PROBABILITY.name().toLowerCase())) {
            ret.observer = Observables.Qualities.PROBABILITY;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.OCCURRENCE.name().toLowerCase())) {
            ret.observer = Observables.Qualities.OCCURRENCE;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.PROPORTION.name().toLowerCase())) {
            ret.observer = Observables.Qualities.PROPORTION;
            admitOther = true;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.TYPE.name().toLowerCase())) {
            ret.observer = Observables.Qualities.TYPE;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.UNCERTAINTY.name().toLowerCase())) {
            ret.observer = Observables.Qualities.UNCERTAINTY;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.VALUE.name().toLowerCase())) {
            ret.observer = Observables.Qualities.VALUE;
            admitOther = true;
            token = tokenizer.nextToken();
        } else if (token.getImage()
                .equals(Observables.Qualities.RATIO.name().toLowerCase())) {
            ret.observer = Observables.Qualities.RATIO;
            admitOther = true;
            token = tokenizer.nextToken();
        }

        /*
         * main may be another definition or a simple concept
         */
        if (token.getImage().equals("(")) {
            Definition main = new Definition();
            token = readDefinition(tokenizer, tokenizer.nextToken(), main);
            ret.mainConcept = main;
            token = tokenizer.nextToken();

        } else if (tokenizer.hasMoreTokens()) {

            ret.baseConcept = tokenizer.expect(token, Token.NORMAL);

            if (ret.baseConcept.startsWith("#")) {
                ret.distanceSpecs = ret.baseConcept;
                ret.baseConcept = null;
            } else {
                ret.baseConcept += tokenizer.expect(Token.SEPARATOR, ":")
                        + tokenizer.expect(Token.NORMAL);
            }

            token = tokenizer.nextToken();
        }

        /*
         * if modifier was encountered that admits a secondary concept, look for secondary
         * definition after comma
         */
        if (admitOther && token.getImage().equals(",")) {
            token = tokenizer.nextToken();
            if (token.getImage().equals("(")) {
                Definition other = new Definition();
                token = readDefinition(tokenizer, tokenizer.nextToken(), other);
                ret.otherConcept = other;
                token = tokenizer.nextToken();

            } else {
                String other = tokenizer.expect(token, Token.NORMAL)
                        + tokenizer.expect(Token.SEPARATOR, ":")
                        + tokenizer.expect(Token.NORMAL);
                ret.otherConcept = new Definition();
                ret.otherConcept.baseConcept = other;
                token = tokenizer.nextToken();
            }

        }

        if (token.getImage().equals("+")) {
            while (true) {
                token = readComponent(tokenizer, tokenizer.nextToken(), ret);
                if (token == null || !token.getImage().equals(",")) {
                    break;
                }
            }
        }

        return token;

    }

    private static Token readComponent(Tokenizer tokenizer, Token token, Definition ret)
            throws KlabValidationException {

        String type = tokenizer
                .expect(token, Token.NORMAL, "T", "C", "I", "O", "R", "B", "D");
        tokenizer.expect(Token.SEPARATOR, "(");

        while (true) {

            token = tokenizer.nextToken();

            if (token.getImage().equals(")")) {
                return tokenizer.nextToken();
            }

            Definition definition = new Definition();

            if (type.equals("O")) {
                ret.ontology = token.getImage();
                tokenizer.expect(Token.SEPARATOR, ")");
            } else {
                token = readDefinition(tokenizer, token, definition);
            }

            switch (type) {
            case "T":
                if (ret.traits == null) {
                    ret.traits = new ArrayList<>();
                }
                ret.traits.add(definition);
                break;
            case "R":
                if (ret.roles == null) {
                    ret.roles = new ArrayList<>();
                }
                ret.roles.add(definition);
                break;
            case "C":
                if (ret.context != null) {
                    throw new KlabValidationException("no more than one context type is allowed in a concept definition");
                }
                ret.context = definition;
                break;
            case "I":
                if (ret.inherent != null) {
                    throw new KlabValidationException("no more than one inherent type is allowed in a concept definition");
                }
                ret.inherent = definition;
                break;
            case "B":
                if (ret.byTrait != null) {
                    throw new KlabValidationException("no more than one 'by' type is allowed in a concept definition");
                }
                ret.byTrait = definition;
                break;
            case "D":
                if (ret.downTo != null) {
                    throw new KlabValidationException("no more than one 'down to' type is allowed in a concept definition");
                }
                ret.downTo = definition;
                break;
            }

            if (!token.getImage().equals(",")) {
                break;
            }
        }

        return tokenizer.nextToken();
    }

    /**
     * Reconstruct the concept from the definition, or return null if any of the
     * components cannot be found in the knowledge base.
     * 
     * @return the reconstructed concept, or null if it is unsatisfiable in the current
     * knowledge environment. Use same ontologies as original concept, which are only
     * guaranteed to exist in the same runtime environment where the definition was
     * created.
     * 
     * @throws KlabValidationException
     */
    public IConcept reconstruct() throws KlabValidationException {
        return reconstruct(null);
    }

    public String getDeclaration() {
        return getDeclaration(null);
    }

    public String getDeclaration(IProject project) {

        String ret = "";
        String premove = project != null && project.getUserKnowledge() != null
                ? project.getUserKnowledge().getId() + ":" : null;

        if (traits != null) {
            for (Definition d : traits) {
                ret += removePrefix(d.getDeclaration(project), premove) + " ";
            }
        }

        ret += baseConcept == null ? removePrefix(mainConcept.getDeclaration(project), premove)
                : removePrefix(baseConcept, premove);

        if (inherent != null) {
            ret += " of " + removePrefix(inherent.getDeclaration(project), premove);
        }

        if (context != null) {
            ret += " within " + removePrefix(context.getDeclaration(project), premove);
        }

        return ret;
    }

    private String removePrefix(String declaration, String premove) {
        return premove == null ? declaration
                : (declaration.startsWith(premove) ? declaration.substring(premove.length()) : declaration);
    }

    /**
     * Reconstruct in specified ontology.
     * 
     * @param ontology
     * @return
     * @throws KlabValidationException
     */
    public IConcept reconstruct(IOntology ontology) throws KlabValidationException {

        if (authority != null && authorityTerm != null) {
            IAuthority<?> auth = AuthorityFactory.get().getAuthorityView(authority);
            if (auth == null) {
                throw new KlabValidationException("authority " + authority + " unrecognized");
            }
            return auth.getIdentity(authorityTerm, authority);
        }

        IConcept main = baseConcept == null
                ? (distanceSpecs == null ? mainConcept.reconstruct(ontology)
                        : reconstructDistance())
                : KLAB.KM.getConcept(baseConcept);

        if (main == null
                || (observer == null && traits == null && context == null
                        && inherent == null && roles == null
                        && downTo == null && byTrait == null && !denied && !observability
                        && !observable)) {
            return main;
        }

        IOntology onto = (ontology == null ? (this.ontology == null ? main.getOntology()
                : KLAB.KM.requireOntology(this.ontology)) : ontology);

        Collection<IConcept> trs = new HashSet<>();
        Collection<IConcept> rls = new HashSet<>();
        IConcept ctx = null;
        IConcept inh = null;
        IConcept byt = null;
        IConcept dnt = null;

        if (traits != null) {
            for (Definition d : traits) {
                IConcept t = d.reconstruct(ontology);
                if (t == null) {
                    return null;
                }
                trs.add(t);
            }
        }
        if (roles != null) {
            for (Definition d : roles) {
                IConcept t = d.reconstruct(ontology);
                if (t == null) {
                    return null;
                }
                rls.add(t);
            }
        }
        if (context != null) {
            ctx = context.reconstruct(ontology);
            if (ctx == null) {
                return null;
            }
        }
        if (inherent != null) {
            inh = inherent.reconstruct(ontology);
            if (inh == null) {
                return null;
            }
        }
        if (byTrait != null) {
            byt = byTrait.reconstruct(ontology);
            if (byt == null) {
                return null;
            }
        }
        if (downTo != null) {
            dnt = downTo.reconstruct(ontology);
            if (dnt == null) {
                return null;
            }
        }
        IConcept ret = Observables
                .declareObservable(main, trs, ctx, inh, rls, byt, dnt, onto);
        if (observability || observable) {
            ret = Traits.getObservabilityOf(ret, observability);
        } else if (denied) {
            ret = Traits.getNegation(ret);
        }

        if (observer != null) {

            IConcept other = otherConcept == null ? null
                    : otherConcept.reconstruct(ontology);

            switch (observer) {
            case COUNT:
                return Observables.makeCount(ret);
            case DISTANCE:
                return Observables.makeDistance(ret);
            case PRESENCE:
                return Observables.makePresence(ret);
            case PROBABILITY:
                return Observables.makeProbability(ret);
            case OCCURRENCE:
                return Observables.makeOccurrence(ret);
            case PROPORTION:
                return Observables.makeProportion(ret, other);
            case RATIO:
                return Observables.makeRatio(ret, other);
            case TYPE:
                return Observables.makeTypeFor(ret);
            case UNCERTAINTY:
                return Observables.makeUncertainty(ret);
            case VALUE:
                return Observables.makeValue(ret, other);
            case ASSESSMENT:
                return Observables.makeAssessment(ret);
            default:
                break;
            }
        }

        return ret;
    }

    private IConcept reconstructDistance() {

        IConcept ret = null;
        if (observer == Qualities.DISTANCE) {
            String[] qdist = distanceSpecs.substring(1).split("#");
            if (qdist.length == 2) {
                INamespace ns = KLAB.MMANAGER.getNamespace(qdist[1]);
                if (ns != null) {
                    ret = Observables.makeDistanceTo(ns, qdist[0]);
                }
            }
        }
        return ret;
    }

    @Override
    public String toString() {

        if (authority != null && authorityTerm != null) {
            /*
             * return right here as we can't have anything else.
             */
            return "authority " + authority + " \"" + authorityTerm + "\"";
        }

        String ret = "";
        if (denied) {
            ret = "not ";
        } else if (observability) {
            ret = "observability ";
        } else if (observable) {
            ret = "observable ";
        } else if (observer != null) {
            ret = observer.name().toLowerCase() + " ";
        }

        ret += mainConcept == null ? baseConcept : ("(" + mainConcept + ")");

        if (otherConcept != null) {
            boolean trivial = otherConcept.isTrivial();
            ret += "," + (trivial ? "" : "(") + otherConcept + (trivial ? "" : ")");
        }

        if (traits != null || context != null || inherent != null || roles != null) {
            ret += "+";
        }

        if (traits != null) {
            String tr = "";
            for (Definition d : traits) {
                tr += (tr.isEmpty() ? "" : ",") + d;
            }
            ret += "T(" + tr + ")";
        }

        if (roles != null) {
            String rl = "";
            for (Definition d : roles) {
                rl += (rl.isEmpty() ? "" : ",") + d;
            }
            ret += "R(" + rl + ")";
        }

        if (context != null) {
            ret += (traits == null ? "" : ",") + "C(" + context + ")";
        }

        if (inherent != null) {
            ret += (traits == null && context == null ? "" : ",") + "I(" + inherent + ")";
        }

        if (ontology != null) {
            ret += (traits == null && context == null && inherent == null ? "" : ",")
                    + "O(" + ontology + ")";
        }

        return ret;

    }

    static public void main(String[] args) throws KlabValidationException {
        Definition diocan = Definition
                .parse("(onto:BaseConcept)+T(onto:Trait1,onto:Trait2+T(zio:Cane),O(merda)),I(not onto:InherentConcept+C(onto:ContextConcept)),O(ontol2)");
        System.out.println(diocan);
        diocan = Definition
                .parse("observability onto:BaseConcept+T(onto:Trait1,onto:Trait2),I(onto:InherentConcept+C(onto:ContextConcept))");
        System.out.println(diocan);
        diocan = Definition
                .parse("not (onto:BaseConcept+I(onto2:Inherent2))+T(onto:Trait1,onto:Trait2),I(onto:InherentConcept+C(onto:ContextConcept))");
        System.out.println(diocan);
        diocan = Definition
                .parse("not (onto:BaseConcept+I(onto2:Inherent2))");
        System.out.println(diocan);
        diocan = Definition
                .parse("presence earth:Stream+I(onto2:Inherent2)");
        System.out.println(diocan);
        diocan = Definition
                .parse("ratio earth:Stream,earth:Diocane+I(onto2:Inherent2)");
        System.out.println(diocan);
        diocan = Definition
                .parse("not (ratio earth:Stream,(not earth:Diocane))+I(onto2:Inherent2)");
        System.out.println(diocan);
        diocan = Definition
                .parse("authority SOIL.WRB \"Haplic Vertisols (Eutric, Endoskeletic)\"");
        System.out.println(diocan);
        diocan = Definition
                .parse("count (biology:Individual+T(authority AGROVOC \"c_1931\"),C(earth:Region))");
        System.out.println(diocan);
    }

    /*
     * produce a pre-0.9.10 definition for compatibility with old servers.
     */
    public static String downgrade(String type) {

        try {

            Definition def = parse(type);

            IConcept main = KLAB.c(def.baseConcept);
            List<IConcept> traits = new ArrayList<>();
            ArrayList<String> tids = new ArrayList<>();

            /*
             * up to 0.9.9, traits were observed directly.
             */
            if (type.startsWith("type ")) {
                type = def.baseConcept;
            }

            String ret = def.baseConcept;
            if (def.traits != null) {
                for (Definition t : def.traits) {
                    IConcept trait = KLAB.c(t.baseConcept);
                    traits.add(trait);
                    tids.add(trait.getLocalName());
                }
            }

            Collections.sort(tids);

            String cId = "";
            for (String s : tids) {
                cId += s;
            }
            cId += main.getLocalName();

            ret = main.getOntology().getConceptSpace() + ":" + cId;

            if (traits.size() > 0) {

                Collections.sort(traits, new Comparator<IConcept>() {
                    @Override
                    public int compare(IConcept arg0, IConcept arg1) {
                        return arg0.toString().compareTo(arg1.toString());
                    }
                });

                ret += "=" + def.baseConcept;
                for (IConcept trait : traits) {
                    ret += "+" + trait.toString();
                }
                type = ret;
            }

        } catch (KlabValidationException e) {
            KLAB.warn("error reconstructing backwards-compatible definition for " + type);
        }
        return type;
    }

    public void removeOntology() {

        this.ontology = null;
        if (mainConcept != null) {
            mainConcept.removeOntology();
        }
        if (inherent != null) {
            inherent.removeOntology();
        }
        if (context != null) {
            context.removeOntology();
        }
        if (byTrait != null) {
            byTrait.removeOntology();
        }
        if (otherConcept != null) {
            otherConcept.removeOntology();
        }
        if (downTo != null) {
            downTo.removeOntology();
        }
        if (roles != null) {
            for (Definition d : roles) {
                d.removeOntology();
            }
        }
        if (traits != null) {
            for (Definition d : traits) {
                d.removeOntology();
            }
        }
    }

    public static String removeOntology(String definition) {
        Definition def;
        try {
            def = parse(definition);
        } catch (KlabValidationException e) {
            throw new KlabRuntimeException(e);
        }
        def.removeOntology();
        return def.toString();
    }

    public static IConcept createIn(String definition, IOntology ontology) {
        try {
            Definition def = parse(definition);
            return def.reconstruct(ontology);
        } catch (KlabValidationException e) {
            return null;
        }
    }

    /**
     * Retrieve only the core observable, with any necessary transformation.
     * 
     * @return
     * @throws KlabValidationException
     */
    public IConcept getCoreType() throws KlabValidationException {

        if (baseConcept == null) {
            return reconstruct();
        }

        IConcept ret = KLAB.c(baseConcept);
        if (ret == null) {
            return null;
        }
        if (observer != null) {

            IConcept other = null;
            if (otherConcept != null) {
                other = otherConcept.reconstruct();
            }

            switch (observer) {
            case COUNT:
                ret = Observables.makeCount(ret);
                break;
            case DISTANCE:
                ret = Observables.makeDistance(ret);
                break;
            case PRESENCE:
                ret = Observables.makePresence(ret);
                break;
            case PROBABILITY:
                ret = Observables.makeProbability(ret);
                break;
            case OCCURRENCE:
                ret = Observables.makeOccurrence(ret);
                break;
            case PROPORTION:
                ret = Observables.makeProportion(ret, other);
                break;
            case RATIO:
                ret = Observables.makeRatio(ret, other);
                break;
            case TYPE:
                ret = Observables.makeTypeFor(ret);
                break;
            case UNCERTAINTY:
                ret = Observables.makeUncertainty(ret);
                break;
            case VALUE:
                ret = Observables.makeValue(ret, other);
                break;
            default:
                break;
            }
        }
        return ret;
    }

}
