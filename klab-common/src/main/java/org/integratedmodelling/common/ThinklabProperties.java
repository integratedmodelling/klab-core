/*******************************************************************************
 *  Copyright (C) 2007, 2014:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
//package org.integratedmodelling.common;
//
//import java.util.Properties;
//
//import org.integratedmodelling.api.configuration.IConfiguration;
//import org.integratedmodelling.api.project.IProject;
//
///**
// * A way to access both the Java system properties and the user properties in
// * .thinklab/thinklab.properties. The latter can only be set manually from the
// * outside, and are expected to be used only for advanced applications and not
// * be relied upon for regular functioning. Calling boot() on this class will
// * merge the system properties with the Thinklab ones (unless there are already
// * properties with the same name in system) so that System.getProperties will
// * also return the thinklab properties. This is used in applications that are
// * agnostic about the Thinklab environment.
// *  
// * @author Ferd
// *
// */
//public class ThinklabProperties {
//
//    public static void boot(IConfiguration cnf) {
//
//        Properties properties = System.getProperties();
//        Properties tlprops = cnf.getProperties();
//
//        for (Object key : tlprops.keySet()) {
//            if (properties.get(key) == null) {
//                properties.put(key, tlprops.get(key));
//            }
//        }
//
//        System.setProperties(properties);
//    }
//
//    /**
//     * Return a property value as an array of strings: if there is
//     * no property return an empty array, if the property has one 
//     * value return a 1-element array, and if there are multiple
//     * values separated by whitespace return all of them as independent
//     * elements.
//     * 
//     * @param property
//     * @return
//     */
//    public static String[] getPropertyAsArray(String property) {
//
//        String val = System.getProperty(property);
//        if (val != null) {
//            return val.trim().split("\\s+");
//        }
//        return new String[0];
//    }
//
//    public static String[] getPropertyAsArray(String property, IProject project) {
//
//        String val = null;
//        if (project != null) {
//            val = project.getProperties().getProperty(property);
//        }
//        if (val != null) {
//            return val.trim().split("\\s+");
//        }
//        return getPropertyAsArray(property);
//    }
// }
