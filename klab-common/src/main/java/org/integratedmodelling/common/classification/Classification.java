/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.classification;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IParseable;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.NumericInterval;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Traits;
import org.integratedmodelling.common.vocabulary.Types;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Reference implementation for IClassification. Also holds the global catalog of
 * user-defined orderings taken from project properties, which needs to be initialized on
 * a project-to-project basis by passing the properties to loadPredefinedOrdering() using
 * a project action.
 * 
 * @author Ferd
 */
public class Classification
        implements IClassification, NetworkSerializable, NetworkDeserializable {

    private IConcept                  cSpace           = null;
    private String                    id               = NameGenerator.shortUUID();

    List<Pair<IClassifier, IConcept>> classifiers      = new ArrayList<>();

    private IConcept                  nilClassifier    = null;
    private boolean                   hasZeroCategory  = false;
    private Type                      type             = null;
    private boolean                   isDiscretization;
    private boolean                   isBinaryClassification;
    private boolean                   initialized;
    private List<IConcept>            conceptOrder     = new ArrayList<>();
    private List<String>              codeConceptOrder = new ArrayList<>();
    private double[]                  distributionBreakpoints;
    private Map<IConcept, Double>     numCodes         = null;
    private Map<IConcept, Integer>    conceptIndexes;
    private Map<Integer, IConcept>    conceptOrderRank;
    /*
     * only filled in when a "down to" restricted concept space is set. If so,
     * no classifier is allowed that is not in this set.
     */
    Set<IConcept>                     allowedChildren  = new HashSet<>();

    /*
     * these are recognized as ordinal prefixes and match the lexicographical ordering of
     * corresponding traits in im. TODO probably obsolete now. Check.
     */
    static String[]                   orderNarrative   = {
            "^No[A-Z].*",
            "^Not[A-Z].*",
            "^Minimal[A-Z].*",
            "^ExtremelyLow[A-Z].*",
            "^ExtremelySmall[A-Z].*",
            "^VeryLow[A-Z].*",
            "^VerySmall[A-Z].*",
            "^Low[A-Z].*",
            "^Small[A-Z].*",
            "^Medium[A-Z].*",
            "^Moderate[A-Z].*",
            "^Partial[A-Z].*",
            "^ModeratelyHigh[A-Z].*",
            "^MediumHigh[A-Z].*",
            "^ModeratelyLarge[A-Z].*",
            "^MediumLarge[A-Z].*",
            "^High[A-Z].*",
            "^Large[A-Z].*",
            "^Full[A-Z].*",
            "^VeryHigh[A-Z].*",
            "^VeryLarge[A-Z].*",
            "^Extreme[A-Z].*",
            "^ExtremelyHigh[A-Z].*",
            "^ExtremelyLarge[A-Z].*",
            "^Maximal[A-Z].*" };

    /*
     * only recognize the "no" case, the rest is a yes case
     */
    static String[]                   booleanNarrative = {
            "^No[A-Z].*",
            "^Not[A-Z].*",
            ".*Absent.*",
            ".*NotPresent.*" };

    public void reset() {
        for (Pair<IClassifier, IConcept> zz : this.classifiers) {
            ((Classifier) (zz.getFirst())).reset();
        }
    }

    /*
     * for automatic constructors
     */
    public Classification() {
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Classification)) {
            throw new KlabRuntimeException("cannot deserialize a Classification from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Classification bean = (org.integratedmodelling.common.beans.Classification) object;

        setConceptSpace((IConcept) Knowledge.parse(bean.getConceptSpace()));
        ArrayList<Pair<IClassifier, IConcept>> cls = new ArrayList<>();
        for (int i = 0; i < bean.getClassifiers().size(); i++) {
            IClassifier classifier = KLAB.MFACTORY
                    .adapt(bean.getClassifiers().get(i), Classifier.class);
            IConcept concept = (IConcept) Knowledge.parse(bean.getConcepts().get(i));
            cls.add(new Pair<>(classifier, concept));
        }
        this.classifiers = cls;
        initialize();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass
                .isAssignableFrom(org.integratedmodelling.common.beans.Classification.class)) {
            throw new KlabRuntimeException("cannot serialize a Classification to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Classification ret = new org.integratedmodelling.common.beans.Classification();

        ret.setConceptSpace(((Knowledge) cSpace).asText());
        for (IConcept c : getConcepts()) {
            ret.getConcepts().add(((Knowledge) c).asText());
        }
        for (IClassifier cls : getClassifiers()) {
            ret.getClassifiers()
                    .add(KLAB.MFACTORY
                            .adapt(cls, org.integratedmodelling.common.beans.Classifier.class));
        }

        return (T) ret;
    }

    public Classification(IConcept conceptSpace) {
        this(conceptSpace, false);
    }

    public Classification(IConcept conceptSpace, boolean createClassifiers) {

        setConceptSpace(conceptSpace);
        if (createClassifiers) {
            ArrayList<Pair<IClassifier, IConcept>> cls = new ArrayList<>();
            for (IConcept c : cSpace.getChildren()) {
                cls.add(new Pair<IClassifier, IConcept>(Classifier
                        .ConceptMatcher(c), c));
            }
            this.classifiers = cls;
            initialize();
        }
    }

    /*
     * for API construction
     */
    public Classification(IConcept conceptSpace,
            List<Pair<IClassifier, IConcept>> classifiers)
            throws KlabValidationException {
        setConceptSpace(conceptSpace);
        for (Pair<IClassifier, IConcept> classifier : classifiers) {
            addClassifier(classifier.getFirst(), classifier.getSecond());
        }
        initialize();
    }

    /*
     * for the remote serializer
     */
    public Classification(String string) {
        String[] def = string.split("#");
        setConceptSpace((IConcept) Knowledge.parse(def[0]));
        // TODO classifiers and
    }

    public static String asString(IClassification classification) {
        String ret = ((IParseable) classification.getConceptSpace()).asText();
        // TODO the rest
        return ret;
    }

    public void initialize() {

        if (initialized)
            return;

        initialized = true;

        /*
         * we have no guarantee that the universal classifier, if there, will be last,
         * given that it may come from a definition where the ordering isn't guaranteed.
         * scan the classifiers and if we have a universal classifier make sure it's the
         * last one, to avoid problems.
         */
        int unidx = -1;
        int iz = 0;
        for (Pair<IClassifier, IConcept> cls : this.classifiers) {
            if (cls.getFirst().isUniversal()) {
                unidx = iz;
            }
            iz++;
        }

        if (unidx >= 0 && unidx < this.classifiers.size() - 1) {
            ArrayList<Pair<IClassifier, IConcept>> nc = new ArrayList<>();
            for (iz = 0; iz < this.classifiers.size(); iz++) {
                if (iz != unidx)
                    nc.add(this.classifiers.get(iz));
            }
            nc.add(this.classifiers.get(unidx));
            this.classifiers = nc;
        }

        /*
         * check if we have a nil classifier; if we don't we don't bother classifying
         * nulls and save some work.
         */
        for (Pair<IClassifier, IConcept> cl : this.classifiers) {
            if (cl.getFirst().isNil()) {
                this.nilClassifier = cl.getSecond();
                break;
            }
        }

        /*
         * remap the values to ranks and determine how to rewire the input if necessary,
         * use classifiers instead of lexicographic order to infer the appropriate concept
         * order
         */
        ArrayList<Classifier> cla = new ArrayList<>();
        ArrayList<IConcept> con = new ArrayList<>();
        for (Pair<IClassifier, IConcept> op : this.classifiers) {
            cla.add((Classifier) op.getFirst());
            con.add(op.getSecond());
        }

        this.distributionBreakpoints = computeDistributionBreakpoints(cSpace, cla, con);

        /*
         * check the rankings, establish whether we have zero categories, and if boolean
         * ensure our zero is the first concept.
         */
        checkRanking();

        /*
         * if we couldn't sort the concepts according to semantics, use the declaration
         * order in the definition.
         */
        if (this.conceptOrder.size() == 0 && this.codeConceptOrder.size() > 0) {
            for (String cn : codeConceptOrder)
                this.conceptOrder.add(KLAB.KM.getConcept(cn));
        }

        if (this.conceptOrder.size() /* still */ == 0) {
            for (Pair<IClassifier, IConcept> cls : this.classifiers) {
                conceptOrder.add(cls.getSecond());
            }
        }

    }

    public void addClassifier(IClassifier classifier, IConcept concept)
            throws KlabValidationException {

        /*
         * validate classifier: must be a child of the concept space, and
         * if the closure is restricted, be part of that closure.
         */
        if (!concept.is(cSpace)) {
            throw new KlabValidationException("cannot classify "
                    + NS.getDisplayName(cSpace)
                    + " to unrelated concept " + concept);
        }

        if (!allowedChildren.isEmpty() && !allowedChildren.contains(concept)) {
            throw new KlabValidationException("cannot use concept "
                    + NS.getDisplayName(concept)
                    + " because of 'down to' restrictions in the observable");
        }

        this.classifiers.add(new Pair<>(classifier, concept));
        codeConceptOrder.add(concept.toString());
    }

    @Override
    public IConcept classify(Object o) {

        if (o instanceof Number && Double.isNaN(((Number) o).doubleValue())) {
            o = null;
        }

        if (o == null && nilClassifier != null) {
            return nilClassifier;
        }

        for (Pair<IClassifier, IConcept> p : this.classifiers) {
            if (p.getFirst().classify(o)) {
                return p.getSecond();
            }
        }

        return null;
    }

    @Override
    public double[] getDistributionBreakpoints() {
        return this.distributionBreakpoints;
    }

    @Override
    public List<IClassifier> getClassifiers() {

        ArrayList<IClassifier> ret = new ArrayList<>();
        for (Pair<IClassifier, IConcept> p : this.classifiers) {
            ret.add(p.getFirst());
        }
        return ret;
    }

    /*
     * the list of concepts in the same order as the list of the corresponding
     * classifiers. May differ from the concept order.
     */
    public List<IConcept> getConcepts() {

        ArrayList<IConcept> ret = new ArrayList<>();
        for (Pair<IClassifier, IConcept> p : this.classifiers) {
            ret.add(p.getSecond());
        }
        return ret;
    }

    @Override
    public List<IConcept> getConceptOrder() {
        return conceptOrder;
    }

    // --- tough stuff below
    // ----------------------------------------------------

    /**
     * This one checks if all classifiers are the discretization of a continuous
     * distribution. If so, it ranks them in order and returns an array of breakpoints
     * that define the continuous distribution they represent. If the classifiers are not
     * like that, it returns null.
     * This does not touch or rank the concepts. If the concepts have a ranking (such as
     * the lexicographic ranking found in Metadata.rankConcepts() it is the user's
     * responsibility that the concepts and the ranges make sense together. We do,
     * however, enforce that continuous ranges are propertly defined if the observable is
     * the discretization of a continuous range.
     * 
     * @return null if we don't encode a continuous discretization; otherwise a pair
     *         containing the breakpoints as a double[] (n+1) and a vector of concepts in
     *         the order defined by the intervals (size n). If the concept list was not
     *         passed, the concept array will be filled with nulls.
     * @throws KlabValidationException
     *             if the observable is a continuous range mapping but the classification
     *             has disjoint intervals.
     */
    double[] computeDistributionBreakpoints(IConcept observable, Collection<Classifier> cls, List<IConcept> classes) {

        if (cls.size() < 1)
            return null;

        double[] ret = null;

        ArrayList<Triple<Double, Double, IConcept>> ranges = new ArrayList<>();

        int i = 0;
        for (Classifier c : cls) {
            if (!c.isInterval())
                return null;
            NumericInterval iv = c.getInterval();
            IConcept concept = classes == null ? null : classes.get(i++);
            double d1 = iv.isLeftInfinite() ? Double.NEGATIVE_INFINITY : iv
                    .getLowerBound();
            double d2 = iv.isRightInfinite() ? Double.POSITIVE_INFINITY : iv
                    .getUpperBound();
            ranges.add(new Triple<>(d1, d2, concept));
        }

        /*
         * sort ranges so that they appear in ascending order
         */
        Collections.sort(ranges, new Comparator<Triple<Double, Double, IConcept>>() {

            @Override
            public int compare(Triple<Double, Double, IConcept> o1, Triple<Double, Double, IConcept> o2) {

                if (Double.compare(o1.getFirst(), o2.getFirst()) == 0
                        && Double.compare(o1.getSecond(), o2.getSecond()) == 0)
                    return 0;

                return o2.getFirst() >= o1.getSecond() ? -1 : 1;
            }
        });

        /*
         * sorted vector of concepts
         */
        IConcept[] cret = new IConcept[ranges.size()];
        for (int jc = 0; jc < ranges.size(); jc++)
            cret[jc] = ranges.get(jc).getThird();

        /*
         * build vector from sorted array
         */
        boolean isContinuous = true;
        ret = new double[ranges.size() + 1];
        i = 0;
        double last = 0.0;
        ret[i++] = ranges.get(0).getFirst();
        last = ranges.get(0).getSecond();
        for (int n = 1; n < ranges.size(); n++) {

            Triple<Double, Double, IConcept> pd = ranges.get(n);

            /*
             * we don't allow ordered range mappings to have disjoint intervals
             */
            if (Double.compare(pd.getFirst(), last) != 0) {
                isContinuous = false;
            }
            ret[i++] = pd.getFirst();
            last = pd.getSecond();
            if (n == ranges.size() - 1)
                ret[i++] = last;
        }

        /*
         * ret != null so we are continuous and sortable. redefine the order of the
         * concepts (whether we are continuous or not).
         */
        conceptOrder.clear();
        for (IConcept c : cret) {
            conceptOrder.add(c);
        }

        return isContinuous ? ret : null;
    }

    /**
     * Produce the lexical ranking of the concept passed, using a ranking method that
     * depends on the concept (or the type hint passed if any). If we are told to order
     * the concepts and we have no help from the classifiers, try using lexicographic
     * ranking. Honor any configuration for specific concepts that may specify or override
     * the "natural" ordering.
     */
    public void checkRanking() {

        ArrayList<Pair<IConcept, Integer>> lexicalRank = new ArrayList<>();

        boolean gotNo = false;
        boolean isBoolean = false;

        IConcept trueCategory = null;

        /*
         * if presence-absence, map the "No*" or "notpresent" to 0 and the other to 1,
         * then return. Must be two concepts at most.
         */

        for (IConcept c : conceptOrder) {

            if (c.isAbstract())
                continue;

            int i = 0;
            for (String rx : booleanNarrative) {
                if (c.getLocalName().matches(rx)) {
                    lexicalRank.add(new Pair<>(c, i));
                    gotNo = true;
                    break;
                }
                i++;
            }

            // wasn't a no, insert as a higher value.
            if (i == booleanNarrative.length) {
                lexicalRank.add(new Pair<>(c, i + 1));
                trueCategory = c;
            }
        }

        if (isBinaryClassification) {

            /*
             * sort concepts according to rank in lexical array to linearize rank
             */
            Collections.sort(lexicalRank, new Comparator<Pair<IConcept, Integer>>() {

                @Override
                public int compare(Pair<IConcept, Integer> o1, Pair<IConcept, Integer> o2) {
                    return o1.getSecond().compareTo(o2.getSecond());
                }
            });

            isBoolean = true;

            conceptOrder.clear();
            for (Pair<IConcept, Integer> p : lexicalRank) {
                conceptOrder.add(p.getFirst());
            }
        }

        this.hasZeroCategory = gotNo
                || (lexicalRank.size() > 0 && lexicalRank.get(0).getFirst()
                        .getLocalName().startsWith("No"));

        if (isBoolean) {
            this.type = Type.BOOLEAN_RANKING;
        }

    }

    @Override
    public IConcept getConceptSpace() {
        return cSpace;
    }

    // @Override
    public void addClassifier(IConcept concept, IClassifier classifier)
            throws KlabException {
        addClassifier(classifier, concept);
    }

    public void setConceptSpace(IConcept concept) {

        // avoid NPEs after parsing syntax errors.
        if (concept == null) {
            return;
        }

        if (Types.isDelegate(concept)) {
            concept = Types.getExposedTraits(concept).iterator().next();
        }
        if (NS.isObservability(concept)) {
            concept = Traits.getImpliedObservable(concept);
        }

        IConcept traitType = Observables.getByType(concept);
        if (traitType != null) {
            concept = traitType;
        }

        this.allowedChildren.addAll(Observables.getRestrictedClosure(concept));
        this.cSpace = concept;
    }

    @Override
    public boolean hasZeroRank() {
        return hasZeroCategory;
    }

    @Override
    public boolean isCategorical() {
        return type.equals(Type.UNORDERED);
    }

    @Override
    public boolean isContiguousAndFinite() {

        if (distributionBreakpoints == null)
            return false;

        return !(Double.isInfinite(distributionBreakpoints[0]) || Double
                .isInfinite(distributionBreakpoints[distributionBreakpoints.length - 1]));
    }

    @Override
    public boolean isIdentical(IClassification classification) {
        
        /*
         * same object
         */
        if (((Classification)classification).id.equals(id)) {
            return true;
        }
        
        /*
         * TODO
         * same contents
         */
        
        return false;
    }

    public void dumpIndented(PrintStream out, int indent) {

        String spaces = MiscUtilities.spaces(indent);
        for (Pair<IClassifier, IConcept> cl : this.classifiers) {
            out.println(spaces + cl.getSecond() + " if "
                    + ((Classifier) cl.getFirst()).dumpCode());
        }
    }

    @Override
    public boolean isDiscretization() {
        return isDiscretization;
    }

    // @Override
    public void setDiscretization(boolean isDiscretization) {
        this.isDiscretization = isDiscretization;
    }

    @Override
    public Type getType() {
        return type;
    }

    // @Override
    public void setBooleanRanking(boolean b) {
        isBinaryClassification = b;
    }

    public Number getRank(IConcept object) {

        for (int i = 0; i < conceptOrder.size(); i++) {
            if (conceptOrder.get(i).equals(object)) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public double getNumericCode(IConcept c) {
        if (numCodes == null) {
            numCodes = new HashMap<>();
            if (distributionBreakpoints != null) {
                for (int i = 0; i < conceptOrder.size(); i++) {
                    double val = distributionBreakpoints[i]
                            + ((distributionBreakpoints[i + 1]
                                    - distributionBreakpoints[i]) / 2.0);
                    numCodes.put(conceptOrder.get(i), val);
                }
            } else {
                for (int i = 0; i < conceptOrder.size(); i++) {
                    numCodes.put(conceptOrder.get(i), (double) (i + 1));
                }
            }
        }
        return numCodes.get(c);
    }

    /**
     * Create a classification in two levels, the first for false, the second for true.
     * 
     * @return the binary classification
     */
    public static IClassification createBinary() {
        List<Pair<IClassifier, IConcept>> classifiers = new ArrayList<>();
        List<IConcept> levels = NS.getLevels(2);
        classifiers.add(new Pair<IClassifier, IConcept>(Classifier
                .BooleanMatcher(false), levels.get(0)));
        classifiers.add(new Pair<IClassifier, IConcept>(Classifier
                .BooleanMatcher(true), levels.get(1)));
        try {
            return new Classification(NS.getUserOrdering(), classifiers);
        } catch (KlabValidationException e) {
            throw new KlabRuntimeException(e);
        }
    }

    @Override
    public int classifyToIndex(Object o) {

        if (conceptIndexes == null) {
            conceptIndexes = new HashMap<>();
            for (int i = 0; i < conceptOrder.size(); i++) {
                conceptIndexes.put(conceptOrder.get(i), i);
            }
        }
        IConcept c = classify(o);
        if (c == null) {
            return -1;
        }
        Integer ret = conceptIndexes.get(c);
        return ret == null ? -1 : ret;
    }

    /**
     * Return most close rank of classifier using concept order instead of concept.
     * 
     * @param integer
     * @return numeric rank of concept
     */
    public double getNumericCode(Integer integer) {
        if (conceptOrderRank == null) {
            conceptOrderRank = new HashMap<>();
            for (int i = 0; i < conceptOrder.size(); i++) {
                conceptOrderRank.put(i, conceptOrder.get(i));
            }
        }
        return getNumericCode(conceptOrderRank.get(integer));
    }

    @Override
    public double undiscretize(IConcept object) {
        double ret = Double.NaN;
        if (distributionBreakpoints != null
                && distributionBreakpoints.length == (getConceptOrder().size() + 1)) {
            for (int i = 0; i < getConceptOrder().size(); i++) {
                if (object.equals(getConceptOrder().get(i))) {
                    ret = distributionBreakpoints[i] + ((distributionBreakpoints[i + 1]
                            - distributionBreakpoints[1]) / 2.0);
                    break;
                }
            }
        }
        return ret;
    }
}
