/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.classification;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.integratedmodelling.api.data.IList;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.Interval;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.NumericInterval;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.kim.expr.GroovyExpression;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.NumberUtils;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * A classifier of objects for general use.
 */
public class Classifier implements IClassifier, NetworkSerializable, NetworkDeserializable {

	/*
	 * leave these protected, so I can define them in k.IM without having to
	 * write a zillion set methods.
	 */
	protected ArrayList<Classifier> classifierMatches = null;
	protected Double numberMatch = null;
	protected NumericInterval intervalMatch = null;
	protected IConcept conceptMatch = null;
	protected String stringMatch = null;
	protected Integer booleanMatch = null;
	protected boolean negated = false;
	protected IExpression expressionMatch = null;
	protected boolean anythingMatch = false;
	
	// each sublist is in AND, each concept in each list is in OR
	protected List<List<IConcept>> conceptMatches = null;
	protected HashMap<String, Boolean> _reasonCache;

	// if not null, we're a classifier for a particularly inherited trait, which
	// enables
	// different optimizations.
	IConcept trait = null;

	/*
	 * if true, this is an :otherwise classifier, that needs to be known by the
	 * classification
	 */
	protected boolean catchAll = false;

	/*
	 * if true, this is a classifier specifically meant to reclassify nil/null;
	 * normally, nil does not reclassify unless there is one of these in a
	 * classification.
	 */
	protected boolean nullMatch = false;

	public Classifier() {
	}

	@Override
	public void deserialize(IModelBean object) {

		if (!(object instanceof org.integratedmodelling.common.beans.Classifier)) {
			throw new KlabRuntimeException(
					"cannot deserialize a Classifier from a " + object.getClass().getCanonicalName());
		}
		org.integratedmodelling.common.beans.Classifier bean = (org.integratedmodelling.common.beans.Classifier) object;

		if (bean.getIntervalMatch() != null) {
			this.intervalMatch = KLAB.MFACTORY.adapt(bean.getIntervalMatch(), NumericInterval.class);
		}
		if (bean.getNumberMatch() != null) {
			this.numberMatch = bean.getNumberMatch();
		}
		if (bean.getStringMatch() != null) {
			this.stringMatch = bean.getStringMatch();
		}
		if (bean.getConceptMatch() != null) {
			this.conceptMatch = (IConcept) Knowledge.parse(bean.getConceptMatch());
		}
		if (bean.getTrait() != null) {
			this.trait = (IConcept) Knowledge.parse(bean.getTrait());
		}
		if (bean.getBooleanMatch() != null) {
			this.booleanMatch = bean.getBooleanMatch();
		}
		if (bean.getExpressionMatch() != null) {
			this.expressionMatch = new GroovyExpression(expressionMatch.toString());
		}
		if (bean.getConceptMatches() != null) {
			// TODO
		}

		this.nullMatch = bean.isNullMatch();
		this.negated = bean.isNegated();
		this.catchAll = bean.isCatchAll();

	}

	@SuppressWarnings("unchecked")
	@Override
	public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

		if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Classifier.class)) {
			throw new KlabRuntimeException("cannot serialize a Classifier to a " + desiredClass.getCanonicalName());
		}

		org.integratedmodelling.common.beans.Classifier ret = new org.integratedmodelling.common.beans.Classifier();

		if (numberMatch != null) {
			ret.setNumberMatch(numberMatch);
		}
		if (stringMatch != null) {
			ret.setStringMatch(stringMatch);
		}
		if (conceptMatch != null) {
			ret.setConceptMatch(((Knowledge) conceptMatch).asText());
		}
		if (trait != null) {
			ret.setTrait(((Knowledge) trait).asText());
		}
		if (intervalMatch != null) {
			ret.setIntervalMatch(KLAB.MFACTORY.adapt(intervalMatch, Interval.class));
		}
		if (booleanMatch != null) {
			ret.setBooleanMatch(booleanMatch);
		}
		if (expressionMatch != null) {
			ret.setExpressionMatch(expressionMatch.toString());
		}
		if (conceptMatches != null) {
			// TODO
		}

		ret.setNullMatch(nullMatch);
		ret.setNegated(negated);
		ret.setCatchAll(catchAll);

		return (T) ret;
	}

	public Classifier(Object o) {

		if (o instanceof Number) {
			numberMatch = ((Number) o).doubleValue();
		} else if (o instanceof String) {
			stringMatch = (String) o;
		} else if (o instanceof IConcept) {
			conceptMatch = (IConcept) o;
		} else if (o instanceof NumericInterval) {
			intervalMatch = (NumericInterval) o;
		} else if (o == null) {
			nullMatch = true;
		} else {
			throw new KlabRuntimeException("cannot create classifier to match unsupported object type: " + o);
		}
	}

	@Override
	public boolean isUniversal() {
		return catchAll;
	}

	// called by the classification
	void reset() {
		_reasonCache = null;
	}

	@Override
	public boolean classify(Object o) {

		if (anythingMatch) {
			return true;
		}
		
		if (catchAll && o != null && !(o instanceof Double && Double.isNaN((Double) o))) {
			return true;
		}

		if (o == null)
			return negated ? !nullMatch : nullMatch;

		if (numberMatch != null) {

			Number n = asNumber(o);
			if (n == null) {
				return false;
			}
			return negated ? !NumberUtils.equal(numberMatch, asNumber(o)) : NumberUtils.equal(numberMatch, asNumber(o));

		} else if (booleanMatch != null) {

			return negated ? asBoolean(o) != (booleanMatch > 0) : asBoolean(o) == (booleanMatch > 0);

		} else if (classifierMatches != null) {

			for (Classifier cl : classifierMatches) {
				if (cl.classify(o))
					return true;
			}

		} else if (intervalMatch != null) {

			Double d = asNumber(o);
			if (d != null)
				return negated ? !intervalMatch.contains(d) : intervalMatch.contains(d);

		} else if (conceptMatch != null) {

			return negated ? !is(asConcept(o), conceptMatch) : is(asConcept(o), conceptMatch);

		} else if (stringMatch != null) {

			return negated ? !stringMatch.equals(o.toString()) : stringMatch.equals(o.toString());

		} else if (expressionMatch != null) {

			try {
				/*
				 * TODO find an elegant way to communicate external parameter
				 * maps, and set :self = o in it.
				 */
				HashMap<String, Object> parms = new HashMap<String, Object>();
				parms.put("self", o);
				// FIXME pass a proper monitor
				return negated ? !(Boolean) expressionMatch.eval(parms, null)
						: (Boolean) expressionMatch.eval(parms, null);

			} catch (Exception e) {
				throw new KlabRuntimeException(e);
			}
		} else if (conceptMatches != null) {

			IConcept cc = asConcept(o);
			for (List<IConcept> or : conceptMatches) {
				boolean oneOk = false;
				for (IConcept oc : or) {
					if (negated ? !is(cc, conceptMatch) : is(cc, conceptMatch)) {
						oneOk = true;
						break;
					}
					if (!oneOk) {
						return false;
					}
				}
			}
			return true;
		}

		return false;
	}

	/**
	 * Calls is() on the concepts, but caches the result so that the same
	 * invocation will be cheap. We're going to call this hundreds of thousands
	 * of times, and it's safe to assume that the ontology won't change between
	 * invocations.
	 * 
	 * TODO this should be reset by the classification before a cycle
	 * 
	 * 
	 * @param asConcept
	 * @param conceptMatch2
	 * @return
	 */
	private boolean is(IConcept c1, IConcept c2) {

		String key = c1 + "#" + c2;
		Boolean ret = null;
		if (_reasonCache != null)
			ret = _reasonCache.get(key);
		if (ret == null) {
			if (_reasonCache == null) {
				_reasonCache = new HashMap<String, Boolean>();
			}
			ret = c1.is(c2);
			_reasonCache.put(key, ret);
		}
		return ret;
	}

	private IConcept asConcept(Object o) {

		if (o instanceof IConcept)
			return (IConcept) o;

		/*
		 * TODO no provision for parsing from string
		 */

		return null;
	}

	private Double asNumber(Object o) {

		Double ret = null;
		if (o instanceof Number) {
			ret = ((Number) o).doubleValue();
		}
		return ret;
	}

	private boolean asBoolean(Object o) {

		Boolean ret = false;
		if (o instanceof Boolean) {
			ret = ((Boolean) o);
		}
		return ret;
	}

	public void addClassifier(Classifier c) {
		if (classifierMatches == null)
			classifierMatches = new ArrayList<Classifier>();
		classifierMatches.add(c);
	}

	public void setConcept(IConcept c) {
		conceptMatch = c;
	}

	public void setInterval(NumericInterval interval) {
		this.intervalMatch = interval;
	}

	public void setNumber(Object classifier) {
		numberMatch = asNumber(classifier);
	}

	@Override
	public String toString() {
		String ret = null;
		if (classifierMatches != null) {
			ret = "mul:";
			for (Classifier c : classifierMatches) {
				ret += "[" + c + "]";
			}
		} else if (numberMatch != null) {
			ret = "num:" + numberMatch;
		} else if (intervalMatch != null) {
			ret = "int:" + intervalMatch;
		} else if (conceptMatch != null) {
			ret = "con:" + conceptMatch;
		} else if (stringMatch != null) {
			ret = "str:" + stringMatch;
		} else if (catchAll) {
			ret = "tru:true";
		} else if (nullMatch) {
			ret = "nil:true";
		}
		return ret;
	}

	public void setCatchAll() {
		this.catchAll = true;
	}

	public void setString(String classifier) {
		this.stringMatch = classifier;
	}

	public void setNil() {
		this.nullMatch = true;
	}

	@Override
	public boolean isInterval() {
		return intervalMatch != null;
	}

	public NumericInterval getInterval() {
		return intervalMatch;
	}

	@Override
	public boolean isNil() {
		return this.nullMatch;
	}

	public void setExpression(IExpression e) {
		this.expressionMatch = e;
	}

	public static Classifier NumberMatcher(Number n) {
		Classifier ret = new Classifier();
		ret.numberMatch = n.doubleValue();
		return ret;
	}

	public static Classifier BooleanMatcher(boolean n) {
		Classifier ret = new Classifier();
		ret.booleanMatch = n ? 1 : 0;
		return ret;
	}

	public static Classifier RangeMatcher(NumericInterval interval) {
		Classifier ret = new Classifier();
		ret.intervalMatch = interval;
		return ret;
	}

	public static Classifier ConceptMatcher(IConcept concept) {
		Classifier ret = new Classifier();
		ret.conceptMatch = concept;
		return ret;
	}

	public static Classifier Multiple(Classifier... classifiers) {
		Classifier ret = new Classifier();
		for (Classifier c : classifiers)
			ret.addClassifier(c);
		return ret;
	}

	public static Classifier StringMatcher(String string) {
		Classifier ret = new Classifier();
		ret.stringMatch = string;
		return ret;
	}

	public static Classifier Universal() {
		Classifier ret = new Classifier();
		ret.catchAll = true;
		return ret;
	}

	public static Classifier NullMatcher() {
		Classifier ret = new Classifier();
		ret.nullMatch = true;
		return ret;
	}

	public void negate() {
		negated = true;
	}

	/**
	 * Create a classifier that will match any of a set of literals. Admitted
	 * literals are numbers, strings, concepts, null and nested lists, which are
	 * all in OR.
	 * 
	 * @param set
	 * @return multiple classifier
	 */
	public static IClassifier Multiple(IList set) {

		Classifier ret = new Classifier();
		for (Object o : set) {
			if (o instanceof Number) {
				ret.addClassifier(NumberMatcher((Number) o));
			} else if (o instanceof String) {
				ret.addClassifier(StringMatcher((String) o));
			} else if (o instanceof IConcept) {
				ret.addClassifier(ConceptMatcher((IConcept) o));
			} else if (o == null) {
				ret.addClassifier(NullMatcher());
			} else if (o instanceof IList) {
				ret.addClassifier((Classifier) Multiple((IList) o));
			}
		}
		return ret;
	}

	public String dumpCode() {

		/*
		 * TODO provisional
		 */
		return toString();
	}

	@Override
	public Object asValue() {

		if (numberMatch != null) {
			return numberMatch;
		} else if (booleanMatch != null) {
			return booleanMatch;
		} else if (intervalMatch != null) {

			return intervalMatch.getLowerBound()
					+ (new Random().nextDouble() * (intervalMatch.getUpperBound() - intervalMatch.getLowerBound()));

		} else if (conceptMatch != null) {

			return conceptMatch;

		} else if (stringMatch != null) {

			return stringMatch;

		} else if (expressionMatch != null) {

			return expressionMatch;
		}
		return null;
	}

	// may be useful (or not) but requires a knowledge manager for parsing
	// concept names.
	// public void parse(String s) throws ThinklabException {
	//
	// String selector = s.substring(0,4);
	// String def = s.substring(4);
	//
	// if (selector.equals("num:")) {
	// number = Double.parseDouble(def);
	// } else if (selector.equals("int:")) {
	// interval = new NumericInterval(def);
	// } else if (selector.equals("con:")) {
	// concept = Thinklab.c(def);
	// } else if (selector.equals("mul:")) {
	// parseMultiple(def);
	// } else if (selector.equals("str:")) {
	// string = def;
	// } else if (selector.equals("tru:")) {
	// catchAll = true;
	// } else if (selector.equals("nil:")) {
	// isNil = true;
	// }
	// }
	//
	// private void parseMultiple(String def) throws ThinklabException {
	//
	// /*
	// * first character must be a square bracket; read up until matching
	// closing bracket
	// */
	// if (def.charAt(0) != '[') {
	// throw new
	// ThinklabValidationException("syntax error in multiple classifier:
	// classifiers must appear in square
	// brackets");
	// }
	//
	// int level = 0;
	// int len = def.length();
	//
	// StringBuffer buf = new StringBuffer(len);
	// for (int i = 0; i < len; i++) {
	// char c = def.charAt(i);
	// if (c == '[') {
	// if (level > 0) {
	// buf.append(c);
	// }
	// level++;
	// } else if (c == ']') {
	// level--;
	// if (level == 0) {
	// addClassifier(new Classifier(buf.toString()));
	// buf = new StringBuffer(len);
	// } else {
	// buf.append(c);
	// }
	// } else {
	// buf.append(c);
	// }
	// }
	// }

}
