package org.integratedmodelling.common.resources;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.network.INodeNetwork;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.beans.Component;
import org.integratedmodelling.common.beans.Project;
import org.integratedmodelling.common.beans.responses.Directory;
import org.integratedmodelling.common.beans.responses.FileResource;
import org.integratedmodelling.common.client.KlabClient;
import org.integratedmodelling.common.client.RestTemplateHelper;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.DirectResourceService;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.common.utils.URLUtils;
import org.integratedmodelling.common.vocabulary.KlabUrn;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Static methods to access resources from URNs and obtain them locally with the minimum
 * required work.
 * 
 * @author ferdinando.villa
 *
 */
public class ResourceFactory {

    static RestTemplateHelper template;
    static Set<String>        exclusions = new HashSet<>();

    static {
        exclusions.add(".git");
        exclusions.add("filelist.txt");
    }

    public static Directory populateDirectory(String urn, File directory, Directory bean) {
        File list = new File(directory + File.separator + "filelist.txt");
        if (list.exists()) {

        }
        if (!list.exists() && directory.canWrite()) {
            // TODO create hash as we go
            // bean.setFilelist(urn + "#" + "filelist.txt");
        }
        populateSubdirectory(urn, "", directory, bean);
        return bean;
    }

    private static void populateSubdirectory(String urn, String pathPrefix, File subdirectory, Directory bean) {

        for (File f : subdirectory.listFiles()) {
            String fname = MiscUtilities.getFileName(f.toString());
            if (exclusions.contains(fname)) {
                continue;
            }
            if (f.isDirectory()) {
                populateSubdirectory(urn, (pathPrefix.isEmpty() ? "" : (pathPrefix + "/")) + fname, f, bean);
            } else {
                bean.getResourceUrns()
                        .add(urn + "#" + ((pathPrefix.isEmpty() ? "" : (pathPrefix + "/"))) + fname);
            }
        }
    }

    /**
     * Collects annotated resource service handlers from the classpath. These are used to
     * resolve specific service URNs in networked engines.
     */
    public static Map<String, Class<? extends DirectResourceService>> serviceRegistry = new HashMap<>();

    /**
     * Extract a physical directory from a server and a directory service URN. TODO handle
     * fileList for sync without copying.
     * 
     * @param serverUrl
     * @param service
     *            the service we're using (could be directory, project, component)
     * @param urn
     * @param topDirectory
     * @param user
     * @param beanClass
     * @return the directory after it has been synchronized.
     * @throws KlabIOException
     */
    public static File getDirectoryFromURN(String serverUrl, String service, String urn, File topDirectory, IUser user, Class<? extends Directory> beanClass)
            throws KlabException {

        if (template == null) {
            template = RestTemplateHelper.newTemplate();
        }

        topDirectory.mkdirs();
        String token = getUrnAuthorization(urn);
        Map map = template.with(token).get(serverUrl + API.getResource(service, urn), Map.class);
        Directory directory = template.with(token).get(serverUrl + API.getResource(service, urn), beanClass);
        /*
         * record the originating node in .node
         */
        KlabUrn kurn = new KlabUrn(urn);
        File nodeFile = new File(topDirectory + File.separator + ".node");
        try {
            FileUtils.writeStringToFile(nodeFile, kurn.getNodeName());
        } catch (IOException e) {
            throw new KlabRuntimeException(e);
        }

        if (directory != null) {
            for (String file : directory.getResourceUrns()) {
                // TODO retrieve and check md5 hash if present.
                retrieveFile(serverUrl, service, file, topDirectory, user);
            }
        } else {
            // FIXME uncomment when engine is always set.
            // KLAB.ENGINE.getMonitor().error("could not retrieve
            // directory from URN " + urn);
        }
        return topDirectory;
    }

    /**
     * Get the appropriate authentication token to request resources through the
     * {@link API#GET_RESOURCE} service of the node mentioned in the URN. This is quite
     * involved as it depends on both where we are and where we go, so better to not
     * improvise.
     * 
     * @param urn
     * @return an authentication token, or null.
     */
    public static String getUrnAuthorization(String urn) {

        KlabUrn kurn = new KlabUrn(urn);
        String ret = null;

        /*
         * we're a node and the URN points to ourselves.
         */
        if (KLAB.ENGINE.getNetwork() instanceof INodeNetwork
                && kurn.getNodeName().equals(KLAB.ENGINE.getName())) {
            return ((INodeNetwork) KLAB.ENGINE.getNetwork()).getKey();
        }

        /*
         * The call goes to the node in the URN; we authenticate with the auth token of
         * the node this goes to if coming from a node, or the token of the primary engine
         * if it comes from an engine, which should have negotiated access with all nodes
         * at API.CONNECT.
         */
        IServer node = KLAB.ENGINE.getNetwork().getNode(kurn.getNodeName());
        if (node == null) {

            /*
             * Not a recognized network node, must be self if we're a client using the
             * engine, in which case the authorization credentials are linked to the user
             * that owns the engine. User will be null before we initialize, which is OK
             * as we'll only access public resources at that point.
             */
            if (KLAB.ENGINE instanceof IModelingEngine && ((IModelingEngine) KLAB.ENGINE).getUser() != null
                    && kurn.getNodeName().equals(((IModelingEngine) KLAB.ENGINE).getUser().getUsername())) {
                return ((IModelingEngine) KLAB.ENGINE).getUser().getSecurityKey();
            }

            /*
             * if we get here the URN points to an unknown node, and we can't do much with
             * it, or it's a universal URN and no authentication is OK.
             */
            return null;
        }

        /*
         * if we're a client, we only deal with the engine
         */
        if (KLAB.ENGINE instanceof KlabClient) {
            ret = null;// ((ClientNetwork) KLAB.ENGINE.getNetwork()).//?????;
        } else {
            /*
             * known node: the key in the node is the result of previously negotiated
             * access credentials.
             */
            ret = node.getKey();
        }

        return ret;
    }

    /**
     * Return the URL on which we should call the {@link API#GET_RESOURCE} service to
     * retrieve the passed URN.
     * 
     * @param urn
     * @return a valid URL string, or null.
     */
    public static String getNodeURLForUrn(String urn) {

        KlabUrn kurn = new KlabUrn(urn);

        if (KLAB.ENGINE.getNetwork() instanceof INodeNetwork
                && kurn.getNodeName().equals(KLAB.ENGINE.getName())) {
            return ((INodeNetwork) KLAB.ENGINE.getNetwork()).getUrl();
        }

        /*
         * The call goes to the node in the URN; we authenticate with the auth token of
         * the node this goes to if coming from a node, or the token of the primary engine
         * if it comes from an engine, which should have negotiated access with all nodes
         * at API.CONNECT.
         */
        IServer node = KLAB.ENGINE.getNetwork().getNode(kurn.getNodeName());
        if (node == null) {
            return null;
        }
        return node.getUrl();
    }

    /**
     * Extract a physical file from a server and a file resource.
     * 
     * @param serverUrl
     * @param file
     * @param directory
     * @param user
     * @return the file
     * @throws KlabIOException
     */
    public static File retrieveFile(String serverUrl, FileResource file, File directory, IUser user)
            throws KlabIOException {
        try {
            URL url = new URL(serverUrl + API.getResource("file", file.getUrn()));
            File out = new File(directory + File.separator + file.getPath());
            String authentication = getUrnAuthorization(file.getUrn());
            URLConnection connection = url.openConnection();
            if (authentication != null) {
                connection.setRequestProperty(API.AUTHENTICATION_HEADER, authentication);
            }
            /*
             * set configured timeout
             */
            if (KLAB.CONFIG.getProperties().containsKey(IConfiguration.KLAB_CONNECTION_TIMEOUT)) {
                int timeout = 1000 * Integer.parseInt(KLAB.CONFIG.getProperties()
                        .getProperty(IConfiguration.KLAB_CONNECTION_TIMEOUT, "10"));
                connection.setConnectTimeout(timeout);
                connection.setReadTimeout(timeout);
            }

            FileUtils.makeDirsUpToFile(out);
            FileUtils.copyInputStreamToFile(connection.getInputStream(), out);
            URLUtils.copy(url, out);
            return out;
        } catch (Exception e) {
            throw new KlabIOException(e);
        }
    }

    /**
     * Retrieve a file from a directory URN with file fragment
     * 
     * @param serverUrl
     * @param service
     * @param fileUrn
     * @param directory
     * @param user
     * @return the file
     * @throws KlabIOException
     */
    public static File retrieveFile(String serverUrl, String service, String fileUrn, File directory, IUser user)
            throws KlabIOException {
        try {

            String[] ss = fileUrn.split("#");
            String urn = ss[0];
            String filePath = ss[1];
            String authentication = getUrnAuthorization(fileUrn);
            URL url = new URL(serverUrl + API.getResource(service, urn) + "?element=" + filePath);

            /*
             * set configured timeout FIXME this mofo never works - URLConnection simply
             * ignores timeout on windows. Must refactor everywhere to use HttpClient/get
             * and set the timeout there.
             */
            URLConnection connection = url.openConnection();
            if (authentication != null) {
                connection.setRequestProperty(API.AUTHENTICATION_HEADER, authentication);
            }

            if (KLAB.CONFIG.getProperties().containsKey(IConfiguration.KLAB_CONNECTION_TIMEOUT)) {
                int timeout = 1000 * Integer.parseInt(KLAB.CONFIG.getProperties()
                        .getProperty(IConfiguration.KLAB_CONNECTION_TIMEOUT, "10"));
                connection.setConnectTimeout(timeout);
                connection.setReadTimeout(timeout);
            }

            File out = new File(directory + File.separator + filePath);
            FileUtils.makeDirsUpToFile(out);
            FileUtils.copyInputStreamToFile(connection.getInputStream(), out);
            return out;
        } catch (Exception e) {
            throw new KlabIOException(e);
        }
    }

    /**
     * Retrieve a project or component from a server, write to a specified directory,
     * register with project manager and return the corresponding (unloaded) project.
     * 
     * @param nodeUrl
     * @param urn
     * @param nodeName
     *            the node name that is set in the project so we can know it's remote.
     *            Pass null for local engines.
     * @param directory
     * @param user
     * @return the registered project after synchronization
     * @throws KlabException
     */
    public static IProject getProjectFromURN(String nodeUrl, String urn, /* @Nullable */ String nodeName, File directory, IUser user)
            throws KlabException {
        String projectId = Path.getLast(urn, ':');
        File pdir = new File(directory + File.separator + projectId);
        KLAB.info("attempting to retrieve project from URN " + urn);
        File dir = getDirectoryFromURN(nodeUrl, "project", urn, pdir, user, Project.class);

        if (ProjectManager.isKlabComponent(dir)) {

            IComponent component = KLAB.PMANAGER.registerComponent(dir);
            KLAB.info("retrieval of remote component " + projectId + " successful");
            return component;

        } else if (ProjectManager.isKlabProject(dir)) {

            KLAB.PMANAGER.registerProject(pdir);
            IProject project = KLAB.PMANAGER.getProject(projectId);
            KLAB.info("retrieval of remote project " + projectId + " successful");
            if (nodeName != null) {
                ((org.integratedmodelling.common.project.Project) project).setOriginatingNodeId(nodeName);
            }
            return project;
        }
        return null;
    }

    /**
     * Retrieve a component from a node and return the directory where it's been
     * synchronized. No registration is done.
     * 
     * @param node
     * @param componentId
     * @param destinationDirectory
     * @return the directory where the component was synchronized
     * 
     * @throws KlabException
     */
    public static File synchronizeComponent(IServer node, String componentId, File destinationDirectory)
            throws KlabException {

        File pdir = new File(destinationDirectory + File.separator + componentId);
        String urn = getComponentUrn(node, componentId);
        KLAB.info("attempting to retrieve project from URN " + urn);

        return getDirectoryFromURN(node
                .getUrl(), "component", urn, pdir, KLAB.ENGINE instanceof IModelingEngine
                        ? ((IModelingEngine) KLAB.ENGINE).getUser() : null, Component.class);
    }

    public static String getProjectUrn(IProject project) {
        return KLAB.ENGINE.getName() + ":project:" + project.getId();
    }

    public static String getComponentUrn(IComponent project) {
        return KLAB.ENGINE.getName() + ":component:" + project.getId();
    }

    public static String getComponentUrn(IServer node, String componentId) {
        return node.getName() + ":component:" + componentId;
    }

    public static String getProjectIdFromUrn(String urn) {
        return Path.getLast(urn, ':');
    }

}
