package org.integratedmodelling.common.configuration.services;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.configuration.IServiceConfiguration;
import org.integratedmodelling.api.data.IDataService;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The simplest possible service configuration, reading from a JSON file, if any, in
 * datadir/services/services.json.
 * 
 * Returns the first service listed as default.
 * 
 * @author ferdinando.villa
 *
 */
@Component
public class MapServiceConfiguration implements IServiceConfiguration {

    @Autowired
    ObjectMapper objectMapper;
    
    IDataService defaultService = null;
    
    boolean initialized = false;
    
    private Map<String, IDataService> assets = new HashMap<>();

    /**
     * Add a data resource to be resolved later.
     * 
     * @param urn
     * @param data
     */
    public void add(String urn, IDataService data) {
        assets.put(urn, data);
    }

    @Override
    public IDataService getDataService(String urn) {

        /*
         * painful but necessary, to avoid Spring boot calling this too early.
         */
        initialize();

        return assets.get(urn);
    }

    /**
     * If a JSON file is present with initial content, load it.
     */
    public void initialize() {

        if (!initialized) {
        	
            initialized = true;
            if (objectMapper != null) {
                File source = new File(KLAB.CONFIG.getDataPath() + File.separator + "services"
                        + File.separator
                        + "services.json");
                if (source.exists()) {
                    try {
                        Data data = objectMapper.readValue(source, Data.class);
                        for (String urn : data.keySet()) {
                            DataService service = data.get(urn);
                            service.setKey(urn);
                            if (defaultService == null && service.getType().equals("geoserver")) {
                                defaultService = service;
                            }
                            assets.put(urn, service);
                        }
                    } catch (IOException e) {
                        KLAB.error(e);
                    }
                }
            }
        }
    }

    @SuppressWarnings("serial")
	static public class Data extends HashMap<String, DataService> {
    }

    @Override
    public IDataService getDefaultService() {
        initialize();
        return defaultService;
    }

}
