/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.configuration;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;
import java.util.logging.Level;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.integratedmodelling.Version;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.collections.OS;
import org.integratedmodelling.common.project.Project;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Global Thinklab configuration. Thinklab proxies to one instance
 * of this.
 * 
 * Global Thinklab properties are by default in ${user.home}/.thinklab/thinklab.properties.
 * 
 * @author Ferd
 *
 */
public class Configuration implements IConfiguration {


    /**
     * Name of the work directory, set to .klab or .klab_dev in development distributions.
     */
    public static String KLAB_RELATIVE_WORK_PATH = ".klab";
    
    static OS os = null;

    /**
     * Used by modeler implementations to advertise themselves on the network so that
     * clients can discover them. Also determines the context path of the application,
     * so don't change it.
     * 
     * FIXME make the context path a variable in the broadcasted info.
     */
    public static final String MODELER_APPLICATION_ID = "kmodeler";

    static {
        if ((Version.VERSION_BRANCH != null && Version.VERSION_BRANCH.equals("develop")) ||
                System.getProperty(KLAB_ENGINE_USE_DEVELOPER_NETWORK, "false").equals("true")) {
            KLAB_RELATIVE_WORK_PATH = ".klab_dev";
        }
    }
    
    /*
     * all configurable paths; others are derived from them.
     */
    File          scratchPath;
    File          dataPath;
    Properties    properties;
    private Level notificationLevel;

    public Configuration(boolean setupLogging) {

        if (KLAB.getLogger() != null && setupLogging) {
            KLAB.setLogger(Logger.getLogger("k.lab"));
        }

        if (System.getProperty(KLAB_DATA_DIRECTORY) != null) {

            this.dataPath = new File(System.getProperty(KLAB_DATA_DIRECTORY));
            this.scratchPath = new File(this.dataPath + File.separator + ".scratch");

        } else {
            String home = System.getProperty("user.home");
            if (System.getProperty(KLAB_WORK_DIRECTORY) != null) {
                KLAB_RELATIVE_WORK_PATH = System.getProperty(KLAB_WORK_DIRECTORY);
            }
            this.dataPath = new File(home + File.separator + KLAB_RELATIVE_WORK_PATH);
            this.scratchPath = new File(this.dataPath + File.separator + ".scratch");

            /*
             * make sure it's  available for substitution in property files etc.
             */
            System.setProperty(KLAB_DATA_DIRECTORY, this.dataPath.toString());
        }

        this.dataPath.mkdirs();
        this.scratchPath.mkdirs();

        KLAB.info("k.LAB data directory set to " + dataPath);

        notificationLevel = Level.INFO;
        if (getProperties().containsKey(KLAB_CLIENT_DEBUG)) {
            if (getProperties().getProperty(KLAB_CLIENT_DEBUG).equals("on")) {
                notificationLevel = Level.FINEST;
            }
        }
    }

    @Override
    public Properties getProperties() {

        if (this.properties == null) {
            /*
             * load or create thinklab system properties
             */
            this.properties = new Properties();
            File pFile = new File(dataPath + File.separator + "klab.properties");
            if (!pFile.exists()) {
            	pFile = new File(dataPath + File.separator + "thinklab.properties");
            }
            try {
                if (pFile.exists()) {
                    FileInputStream input;

                    input = new FileInputStream(pFile);
                    this.properties.load(input);
                    input.close();
                } else {
                	pFile = new File(dataPath + File.separator + "klab.properties");
                    FileUtils.touch(pFile);
                }
            } catch (Exception e) {
                throw new KlabRuntimeException(e);
            }
        }
        return this.properties;
    }

    @Override
    public void save() {
        
        File td = new File(dataPath + File.separator + "klab.properties");

        String[] doNotPersist = new String[] { Project.ORIGINATING_NODE_PROPERTY };
        
        Properties p =  new Properties();
        p.putAll(getProperties());
        
        for (String dn : doNotPersist) {
            p.remove(dn);
        }
        
        try {
            p.store(new FileOutputStream(td), null);
        } catch (Exception e) {
            throw new KlabRuntimeException(e);
        }

    }

    @Override
    public File getDataPath(String subspace) {
        File ret = new File(dataPath + File.separator + subspace);
        ret.mkdirs();
        return ret;
    }

    @Override
    public File getScratchArea() {
        return scratchPath;
    }

    @Override
    public File getScratchArea(String subArea) {
        File ret = new File(scratchPath + File.separator + subArea);
        ret.mkdirs();
        return ret;
    }

    @Override
    public File getTempArea(String subArea) {

        File ret = new File(scratchPath + File.separator + "tmp");
        if (subArea != null) {
            ret = new File(ret + File.separator + subArea);
        }
        boolean exists = ret.exists();
        ret.mkdirs();

        if (!exists) {
            ret.deleteOnExit();
        }

        return ret;
    }

    @Override
    public String getWorkDirectoryName() {
        return KLAB_RELATIVE_WORK_PATH;
    }

    @Override
    public File getDataPath() {
        return dataPath;
    }

    /**
     * Quickly and unreliably retrieve the class of OS we're running on.
     * @return the OS
     */
    @Override
    public OS getOS() {

        if (os == null) {

            String osd = System.getProperty("os.name").toLowerCase();

            // TODO ALL these checks need careful checking
            if (osd.contains("windows")) {
                os = OS.WIN;
            } else if (osd.contains("mac")) {
                os = OS.MACOS;
            } else if (osd.contains("linux") || osd.contains("unix")) {
                os = OS.UNIX;
            }
        }

        return os;
    }

    @Override
    public boolean isDebug() {
        return getProperties().getProperty(KLAB_CLIENT_DEBUG, "off").equals("on");
    }

    @Override
    public boolean isOffline() {
        return getProperties().getProperty(KLAB_OFFLINE, "off").equals("on");
    }
    
    @Override
    public boolean useContextQualities() {
        return getProperties().getProperty(KLAB_USE_CONTEXT_QUALITIES, "on").equals("on");
    }

    @Override
    public boolean useDeveloperNetwork() {
        
        /*
         * development build is always develop no matter what the user chooses.
         */
        if (Version.VERSION_BRANCH != null && Version.VERSION_BRANCH.equals("develop")) {
            return true;
        }
        
        /*
         * system property overrides setting. Used in engine.
         */
        if (System.getProperty(KLAB_ENGINE_USE_DEVELOPER_NETWORK) != null) {
            return System.getProperty(KLAB_ENGINE_USE_DEVELOPER_NETWORK).equals("true");
        }
        return getProperties().getProperty(KLAB_ENGINE_USE_DEVELOPER_NETWORK, "false").equals("true");
    }
    
    @Override
    public boolean useReasoner() {
        /*
         * system property overrides setting. Used in engine.
         */
        if (System.getProperty(KLAB_REASONING) != null) {
            return System.getProperty(KLAB_REASONING).equals("true");
        }
        return getProperties().getProperty(KLAB_REASONING, "false").equals("true");
    }

    @Override
    public File getDefaultExportPath() {
        if (getProperties().containsKey(KLAB_EXPORT_PATH)) {
            return new File(getProperties().getProperty(KLAB_EXPORT_PATH));
        }
        return null;
    }

    @Override
    public String getDeveloperNetworkURLPostfix() {
        return "_dev";
    }

}
