package org.integratedmodelling.common.configuration;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * General class providing read/write access to 
 * @author ferdinando.villa
 *
 */
public class EditableProperties extends PropertiesConfiguration {

    /**
     * If file contains path, read from full path, otherwise find it in KLAB.CONFIG.getDataPath(). 
     * Always create the file if not available. If a prototype URL is passed, copy that instead 
     * of creating an empty file.
     * 
     * @param file
     * @param prototype 
     */
    public EditableProperties(String file, URL prototype) {

        String finp = file;
        if (!finp.contains(File.separator)) {
            finp = KLAB.CONFIG.getDataPath() + File.separator + file;
        }
        File in = new File(finp);
        if (!in.exists() && prototype != null) {
            try {
                InputStream is = prototype.openStream();
                FileUtils.copyInputStreamToFile(is, in);
                is.close();
            } catch (IOException e) {
                // not there
            }
        }

        if (/* still */ !in.exists()) {
            try {
                FileUtils.touch(in);
            } catch (IOException e) {
                throw new KlabRuntimeException(e);
            }
        }

        try {
            this.load(in);
        } catch (ConfigurationException e) {
            throw new KlabRuntimeException(e);
        }
    }
}
