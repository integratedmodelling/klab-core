/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.configuration;

import java.io.PrintStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.log4j.Logger;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.factories.IKnowledgeFactory;
import org.integratedmodelling.api.factories.IModelFactory;
import org.integratedmodelling.api.factories.IModelManager;
import org.integratedmodelling.api.factories.IProjectFactory;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.runtime.IWorkspace;
import org.integratedmodelling.api.services.IBatchRunner;
import org.integratedmodelling.common.kim.KIMModelManager;
import org.integratedmodelling.common.kim.ModelFactory;
import org.integratedmodelling.common.owl.KnowledgeManager;
import org.integratedmodelling.common.project.ProjectManager;
import org.integratedmodelling.common.utils.LoggerStream;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.vocabulary.KLABReasoner;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Used to provide defaults for common variables and to allow implementation to set a
 * knowledge manager to resolve knowledge independent of whether a server or a client is
 * being used. Also servers as a place where to put a configuration object for the same
 * reasons.
 * 
 * @author Ferd
 *
 */
public class KLAB {

    /**
     * Used in engines to explicitly define the expected behavior at boot.
     * 
     * @author ferdinando.villa
     *
     */
    public static enum BootMode {
        /**
         * Authenticate using a user or an institutional certificate; maintain a workspace
         * and get online through the primary server in the certificate.
         */
        MODELER,

        /**
         * Authenticate through server certificate and connect only to nodes we share
         * certificates with. Distribute project knowledge either directly or through
         * individual observations and models.
         */
        NODE
    }

    // for ease of access. Some of these may be null according to who runs this.
    public static IKnowledgeFactory                          KM;
    public static IConfiguration                             CONFIG                         = null;
    public static IModelManager                              MMANAGER;
    public static IProjectFactory                            PMANAGER;
    public static IWorkspace                                 WORKSPACE;
    private static Log                                       clogger                        = null;
    private static Logger                                    logger                         = null;
    public static IEngine                                    ENGINE;
    // public static INetwork NETWORK;
    // public static IClient CLIENT;
    public static String                                     NAME;
    public static IModelFactory                              MFACTORY;
    public static KLABReasoner                               REASONER;

    private static String                                    worldview;

    public static Map<String, Class<? extends IBatchRunner>> batchRunners                   = new HashMap<>();
    private static Set<IProject>                             worldviewProjects              = new HashSet<>();

    // public static ComponentManager CMANAGER;

    /*
     * where to find thinklab. Must be defined.
     */
    public static final String                               THINKLAB_HOME_PROPERTY         = "thinklab.home";

    /**
     * defaults to ${HOME}/.thinklab
     */
    public static final String                               ENV_THINKLAB_DATA_DIR          = "THINKLAB_DATA_DIR";

    /**
     * defaults to ${HOME}/thinklab
     */
    public static final String                               ENV_THINKLAB_WORKSPACE         = "THINKLAB_WORKSPACE_DIR";

    /**
     * defaults to THINKLAB_DATA_DIR/.scratch
     */
    public static final String                               ENV_THINKLAB_SCRATCH_DIR       = "THINKLAB_SCRATCH_DIR";

    /**
     * defaults to THINKLAB_HOME/plugins TODO revise and clean up.
     */
    public static final String                               ENV_THINKLAB_PLUGIN_DIR        = "THINKLAB_PLUGIN_DIR";
    public static final String                               PROJECTS_WORKSPACE             = "projects";
    public static final String                               THINKLAB_WEBAPP_DIR_PROPERTY   = "thinklab.webapp.dir";
    public static final String                               THINKLAB_JRE_PROPERTY          = "thinklab.jre";
    public static final String                               THINKLAB_SERVERLESS_PROPERTY   = "thinklab.serverless";
    public static final String                               THINKLAB_RESOURCE_DIRECTORY    = "thinklab.resource.dir";
    public static final String                               THINKLAB_MEMORY_LIMIT          = "thinklab.memory.limit";
    public static final String                               THINKLAB_REST_CONTEXT_PROPERTY = "THINKLAB_REST_CONTEXT";
    public static final String                               THINKLAB_REST_PORT_PROPERTY    = "THINKLAB_REST_PORT";
    public static final String                               THINKLAB_WORKSPACE_PROPERTY    = "thinklab.client.workspace";

    public static IConcept c(String s) {

        if (s == null || s.isEmpty() || KM == null) {
            return null;
        }

        IConcept ret = KM.getConcept(s);
        if (ret == null) {
            throw new KlabRuntimeException("cannot resolve concept " + s);
        }
        return ret;

    }

    public static IProperty p(String s) {

        if (s == null || s.isEmpty()) {
            return null;
        }

        IProperty ret = KM.getProperty(s);
        if (ret == null) {
            throw new KlabRuntimeException("cannot find property " + s);
        }
        return ret;

    }

    public static IKnowledge k(String s) {

        if (s == null || s.isEmpty()) {
            return null;
        }

        IKnowledge ret = KM.getConcept(s);
        if (ret == null) {
            ret = KM.getProperty(s);
        }
        if (ret == null) {
            throw new KlabRuntimeException("cannot find identifier " + s + " in knowledge base");
        }
        return ret;
    }

    public static boolean getBooleanProperty(String property, boolean def) {

        String p = CONFIG.getProperties().getProperty(property);
        return p == null ? def : Boolean.parseBoolean(p);
    }

    public static int getIntProperty(String property, int def) {

        String p = CONFIG.getProperties().getProperty(property);
        return p == null ? def : Integer.parseInt(p);
    }

    public static String getStringProperty(String property, String def) {

        String p = CONFIG.getProperties().getProperty(property);
        return p == null ? def : p;
    }

    public static String[] getPropertyAsArray(String property) {

        String val = System.getProperty(property);
        if (val != null) {
            return val.trim().split("\\s+");
        }
        return new String[0];
    }

    public static String[] getPropertyAsArray(String property, IProject project) {

        String val = null;
        if (project != null) {
            val = project.getProperties().getProperty(property);
        }
        if (val != null) {
            return val.trim().split("\\s+");
        }
        return getPropertyAsArray(property);
    }

    public static void bootMinimal() {

        MFACTORY = new ModelFactory();
        PMANAGER = new ProjectManager();
        MMANAGER = new KIMModelManager();
        try {
            KM = new KnowledgeManager();
        } catch (KlabException e) {
            throw new KlabRuntimeException(e);
        }
    }

    public static void info(Object o) {
        if (clogger != null) {
            clogger.info(o);
        } else if (logger != null) {
            logger.info(o);
        } else {
            System.err.println("INFO: " + o);
        }
    }

    public static void warn(Object o) {
        if (clogger != null) {
            clogger.warn(o);
        } else if (logger != null) {
            logger.warn(o);
        } else {
            System.err.println("WARN: " + o);
        }
    }

    public static void error(Object o) {
        if (clogger != null) {
            clogger.error(o);
        } else if (logger != null) {
            logger.error(o);
        } else {
            System.err.println("ERROR: " + o);
        }
    }

    public static void debug(Object o) {
        if (CONFIG.isDebug()) {
            if (clogger != null) {
                clogger.debug(o);
            } else if (logger != null) {
                logger.debug(o);
            } else {
                System.err.println("DEBUG: " + o);
            }
        }
    }

    public static void warn(String string, Throwable e) {
        if (clogger != null) {
            clogger.warn(string, e);
        } else if (logger != null) {
            logger.warn(string, e);
        } else {
            System.err.println("WARN: " + MiscUtilities.throwableToString(e));
        }
    }

    public static void error(String string, Throwable e) {
        if (clogger != null) {
            clogger.error(string, e);
        } else if (logger != null) {
            logger.error(string, e);
        } else {
            System.err.println("ERROR: " + MiscUtilities.throwableToString(e));
        }
    }

    public static void setLogger(Logger logger2) {
        logger = logger2;
    }

    public static void setLogger(Log logger2) {
        clogger = logger2;
    }

    public static PrintStream getLoggerStream() {
        // TODO may be given different types of logger; behave accordingly.
        return new LoggerStream(logger);
    }

    /*
     * used to check for existence of logger only.
     */
    public static Object getLogger() {
        if (clogger != null) {
            return clogger;
        }
        return logger;
    }

    public static void registerRunnerClass(String id, Class<? extends IBatchRunner> cls) {
        batchRunners.put(id, cls);
    }

    public static Class<? extends IBatchRunner> getBatchRunner(String id) {
        return batchRunners.get(id);
    }

    public static String getWorldview() {
        return worldview;
    }

    public static void setWorldview(String id) {
        worldview = id;
    }

    public static void notifyWorldviewProject(IProject project) {
        worldviewProjects.add(project);
    }

    public static Collection<IProject> getWorldviewProjects() {
        return worldviewProjects;
    }
}
