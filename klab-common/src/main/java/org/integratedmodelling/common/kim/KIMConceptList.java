/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.kim.kim.ConceptDeclaration;
import org.integratedmodelling.kim.kim.ConceptList;
import org.integratedmodelling.kim.kim.ConceptStatement;

/**
 * Parses a list of declaration, using the context for validation of allowed knowledge.
 * 
 * @author ferdinando.villa
 *
 */
public class KIMConceptList extends KIMModelObject implements List<IKnowledgeObject> {

    List<IKnowledgeObject> concepts = new ArrayList<>();

    public KIMConceptList(KIMScope context, ConceptList statement, KIMModelObject parent) {

        super(context, statement, parent);

        for (ConceptDeclaration declaration : statement.getConcept()) {

            if (declaration.isParent()) {

                /*
                 * parent concept must be an ordering and we must be in a child list
                 */

                /*
                 * record position for posterity and continue
                 */

                continue;
            }

            IKnowledgeObject k = new KIMKnowledge(context, declaration, parent);
            if (!k.isNothing()) {
                concepts.add(k);
            }
        }
        for (ConceptStatement declaration : statement.getDefinitions()) {
            IKnowledgeObject k = new KIMKnowledge(context, declaration, this, declaration.getAnnotations());
            if (!k.isNothing()) {
                concepts.add(k);
            }
        }

        /*
         * if we had a 'parent' keyword, reconstruct ordering with knowledge manager.
         */
    }

    @Override
    public boolean add(IKnowledgeObject arg0) {
        return concepts.add(arg0);
    }

    @Override
    public void add(int arg0, IKnowledgeObject arg1) {
        concepts.add(arg0, arg1);
    }

    @Override
    public boolean addAll(Collection<? extends IKnowledgeObject> arg0) {
        return concepts.addAll(arg0);
    }

    @Override
    public boolean addAll(int arg0, Collection<? extends IKnowledgeObject> arg1) {
        return concepts.addAll(arg0, arg1);
    }

    @Override
    public void clear() {
        concepts.clear();
    }

    @Override
    public boolean contains(Object arg0) {
        return concepts.contains(arg0);
    }

    @Override
    public boolean containsAll(Collection<?> arg0) {
        return concepts.containsAll(arg0);
    }

    @Override
    public IKnowledgeObject get(int arg0) {
        return concepts.get(arg0);
    }

    @Override
    public int indexOf(Object arg0) {
        return concepts.indexOf(arg0);
    }

    @Override
    public boolean isEmpty() {
        return concepts.isEmpty();
    }

    @Override
    public Iterator<IKnowledgeObject> iterator() {
        return concepts.iterator();
    }

    @Override
    public int lastIndexOf(Object arg0) {
        return concepts.lastIndexOf(arg0);
    }

    @Override
    public ListIterator<IKnowledgeObject> listIterator() {
        return concepts.listIterator();
    }

    @Override
    public ListIterator<IKnowledgeObject> listIterator(int arg0) {
        return concepts.listIterator(arg0);
    }

    @Override
    public boolean remove(Object arg0) {
        return concepts.remove(arg0);
    }

    @Override
    public IKnowledgeObject remove(int arg0) {
        return concepts.remove(arg0);
    }

    @Override
    public boolean removeAll(Collection<?> arg0) {
        return concepts.removeAll(arg0);
    }

    @Override
    public boolean retainAll(Collection<?> arg0) {
        return concepts.retainAll(arg0);
    }

    @Override
    public IKnowledgeObject set(int arg0, IKnowledgeObject arg1) {
        return concepts.set(arg0, arg1);
    }

    @Override
    public int size() {
        return concepts.size();
    }

    @Override
    public List<IKnowledgeObject> subList(int arg0, int arg1) {
        return concepts.subList(arg0, arg1);
    }

    @Override
    public Object[] toArray() {
        return concepts.toArray();
    }

    @Override
    public <T> T[] toArray(T[] arg0) {
        return concepts.toArray(arg0);
    }

}
