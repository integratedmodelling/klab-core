/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.ICurrency;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.IValueMediator;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.Currency;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMValuationObserver extends KIMNumericObserver implements IValuingObserver {

    ICurrency  currency;
    IKnowledge valuedObservable;
    IUnit      unit;

    class ValueMediator extends NumericMediator {

        private IValuingObserver other;
        private boolean          convertCurrencies;

        protected ValueMediator(IValuingObserver other, IMonitor monitor) {
            super(monitor);
            // TODO handle distribution unit
            this.other = other;
            this.convertCurrencies = currency != null && other.getCurrency() != null
                    && !currency.equals(other.getCurrency());
        }

        @Override
        public Object mediate(Object object) throws KlabException {

            // TODO handle distribution unit
            Number val = getValueAsNumber(object, other);
            if (!Double.isNaN(val.doubleValue()) && convertCurrencies) {
                val = currency.convert(val, other.getCurrency());
            }
            return super.mediate(val);
        }

        @Override
        public String getLabel() {
            return (currency.equals(other.getCurrency()) ? ""
                    : "convert " + other.getCurrency() + " to " + currency)
                    + (discretization == null ? ""
                            : ((currency.equals(other.getCurrency()) ? "" : "->") + "discretize"));
        }
    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return unit == null || !unit.isDensity(extent);
    }

    public KIMValuationObserver(KIMScope context, KIMModel model, Observer statement) {

        super(context, model, statement);
        this.minimumValue = 0;
        double from = Double.NaN, to = Double.NaN;

        if (statement.getFrom() != null) {
            from = KIM.processNumber(statement.getFrom()).doubleValue();
            to = KIM.processNumber(statement.getTo()).doubleValue();
        }

        if (statement.getCurrency() != null || statement.getFrom() != null) {
            this.currency = new KIMCurrency(context.get(KIMScope.Type.CURRENCY), statement
                    .getCurrency(), from, to);
        } else {
            context.error("either a currency ('in ....') or a range of values must be specified", getFirstLineNumber());
        }

        if (statement.getUnit() != null) {
            this.unit = new KIMUnit(context.get(KIMScope.Type.UNIT), statement.getUnit());
        }

        validate(context);
    }

    public KIMValuationObserver(IObserver observer) {
        super(observer);
        this.minimumValue = 0;
        if (!(observer instanceof IValuingObserver)) {
            throw new KlabRuntimeException("cannot initialize a valuing observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.currency = ((IValuingObserver) observer).getCurrency();
        this.unit = ((IValuingObserver) observer).getUnit();
    }

    public KIMValuationObserver(String string, IObservableSemantics obs) {
        String[] defs = string.split("\\|");
        currency = new Currency(defs[1]);
        if (defs.length > 2) {
            discretization = new org.integratedmodelling.common.classification.Classification(defs[2]);
        }
        observable = obs;
        this.minimumValue = 0;
    }

    /**
     * Only for the deserializer
     */
    public KIMValuationObserver() {
        this.minimumValue = 0;
    }

    @Override
    public IObserver copy() {
        return new KIMValuationObserver(this);
    }

    KIMValuationObserver(IConcept observable, ICurrency currency2) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
        this.currency = currency2;
        this.minimumValue = 0;
    }

    @Override
    public ICurrency getCurrency() {
        return currency;
    }

    @Override
    public IObserver getMediatedObserver() {
        // TODO Auto-generated method stub
        return mediated;
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException {

        observer = getRepresentativeObserver(observer);

        if (!(observer instanceof IValuingObserver))
            throw new KlabValidationException("valuations can only mediate other valuations");

        if (discretization != null) {
            if (((IValuingObserver) observer).getDiscretization() != null) {
                monitor.warn(getObservable().getType()
                        + ": discretized values should not mediate other discretized values: value will be undiscretized, then discretized again");
            }
            return new ValueMediator((IValuingObserver) observer, monitor);
        }

        if (((IValuingObserver) observer).getCurrency().equals(getCurrency())) {
            return null;
        }

        return new ValueMediator((IValuingObserver) observer, monitor);
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.VALUE_OBSERVATION);
    }

    @Override
    public IKnowledge getValuedObservable() {
        return valuedObservable;
    }

    @Override
    public IKnowledge getComparisonObservable() {
        return this.comparisonConcept;
    }

    @Override
    public boolean isPairwise() {
        return this.comparisonConcept != null;
    }

    @Override
    protected IConcept getObservedType(KIMScope context, IConcept knowledge) {
        valuedObservable = knowledge;
        return Observables.makeValue(knowledge, this.comparisonConcept);
    }

    public static String asString(IValuingObserver observer) {
        String ret = "oVL|" + observer.getCurrency();
        if (observer.getDiscretization() != null) {
            ret += "|" + org.integratedmodelling.common.classification.Classification
                    .asString(observer.getDiscretization());
        }
        return ret;
    }

    @Override
    public String toString() {
        return "VAL/" + getObservable() + " (" + currency + ")";
    }

    @Override
    public String getDefinition() {
        return "value " + (isIndirect ? "of " : "")
                + Observables.getDeclaration((IConcept) valuedObservable, getNamespace().getProject())
                + (currency == null ? "" : (" in " + currency))
                + (unit == null ? "" : (" in " + currency));
    }

    @Override
    public IUnit getUnit() {
        return this.unit;
    }

    @Override
    public IValueMediator getValueMediator() {
        return currency;
    }

}
