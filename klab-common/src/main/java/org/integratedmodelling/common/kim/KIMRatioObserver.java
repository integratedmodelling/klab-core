/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IRatioObserver;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.model.Coverage;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMRatioObserver extends KIMNumericObserver
        implements IRatioObserver, NetworkSerializable, NetworkDeserializable {

    public class RatioMediator extends NumericMediator {

        INumericObserver otherObserver;

        protected RatioMediator(INumericObserver other, IMonitor monitor) {
            super(monitor);
            this.otherObserver = other;
        }

        @Override
        public Object mediate(Object object) throws KlabException {
            Number val = getValueAsNumber(object, otherObserver);
            return super.mediate(val);
        }

        @Override
        public String getLabel() {
            return discretization == null ? null : "discretize";
        }
    }

    public KIMRatioObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        validate(context);
    }

    public KIMRatioObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof KIMRatioObserver)) {
            throw new KlabRuntimeException("cannot initialize a ratio observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.comparisonConcept = ((KIMRatioObserver) observer).comparisonConcept;
    }

    @Override
    public IObserver copy() {
        return new KIMRatioObserver(this);
    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return false;
    }

    KIMRatioObserver(IConcept observable) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
    }

    KIMRatioObserver(IConcept observable, ISemantic compareTo) {
        this(observable);
        this.comparisonConcept = compareTo.getType();
    }

    /**
     * Only for the deserializer
     */
    public KIMRatioObserver() {
    }

    @Override
    public IConcept getComparisonConcept() {
        return comparisonConcept;
    }

    @Override
    public boolean isIndirect() {
        return comparisonConcept != null;
    }

    @Override
    public List<IObservableSemantics> getAlternativeObservables() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.RATIO_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMScope context, IConcept concept) {

        IConcept ret = concept;

        if (comparisonConcept != null) {
            ret = Observables.makeRatio(concept, comparisonConcept);
            if (ret == null) {
                context.error(concept
                        + ": ratios must compare two related observables.", getFirstLineNumber());
                return concept;
            }
        } else if (!concept.is(KLAB.c(NS.CORE_RATIO))) {
            context.error(concept
                    + ": the observable in this statement is not a ratio. Use the indirect form ('of' ... 'to' ...) to annotate ratios explicity.", getFirstLineNumber());
        }
        return ret;
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor)
            throws KlabException {
        observer = getRepresentativeObserver(observer);

        if (!(observer instanceof IRatioObserver))
            throw new KlabValidationException("measurements can only mediate other measurements");

        if (discretization != null) {
            if (((IRatioObserver) observer).getDiscretization() != null) {
                monitor.warn(getObservable().getType()
                        + ": discretized ratios should not mediate other discretized ratios: value will be undiscretized, then discretized again");
            }
            return new RatioMediator((INumericObserver) observer, monitor);
        }

        return null;
    }

    @Override
    public String toString() {
        return "RTO/" + getObservable();
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        isIndirect = bean.isIndirect();
        if (bean.getComparisonKnowledge() != null) {
            comparisonConcept = Knowledge.parse(bean.getComparisonKnowledge());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass
                .isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        ret.setIndirect(isIndirect);
        if (comparisonConcept != null) {
            ret.setComparisonKnowledge(((Knowledge) comparisonConcept).asText());
        }
        return (T) ret;
    }

    @Override
    public ICoverage acceptAlternativeContextualizer(IContextualizer contextualizer, ICoverage coverage) {
        // TODO Auto-generated method stub
        return Coverage.EMPTY;
    }

    @Override
    public String getDefinition() {
        // TODO Auto-generated method stub
        return "ratio " + (isIndirect ? "of " : "")
                + Observables.getDeclaration(getObservable().getType(), getNamespace().getProject())
                + (isIndirect ? "of " : "")
                + Observables.getDeclaration(getComparisonConcept(), getNamespace().getProject());
    }

}
