/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.beans.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.model.runtime.AbstractMediator;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public abstract class KIMNumericObserver extends KIMObserver implements INumericObserver {

	protected IClassification discretization;
	protected double minimumValue = Double.NEGATIVE_INFINITY;
	protected double maximumValue = Double.POSITIVE_INFINITY;

	KIMNumericObserver() {
	}

	public KIMNumericObserver(KIMScope context, IModel model, Observer statement) {
		super(context, model, statement);
		if (statement.getDiscretization() != null) {
			discretization = new KIMClassification(context.get(getDiscretizationContext()),
					statement.getDiscretization(), this.getObservable());
		}
	}

	public KIMNumericObserver(IObserver observer) {
		super(observer);
		if (!(observer instanceof INumericObserver)) {
			throw new KlabRuntimeException(
					"cannot initialize a numeric observer from a " + observer.getClass().getCanonicalName());
		}
		this.discretization = ((INumericObserver) observer).getDiscretization();
		this.maximumValue = ((INumericObserver) observer).getMaximumValue();
		this.minimumValue = ((INumericObserver) observer).getMinimumValue();
	}

	protected KIMScope.Type getDiscretizationContext() {
		return KIMScope.Type.NUMERIC_DISCRETIZATION;
	}

	@Override
	public IClassification getDiscretization() {
		return discretization;
	}

	@Override
	public double getMinimumValue() {
		return minimumValue;
	}

	@Override
	public double getMaximumValue() {
		return maximumValue;
	}

	/**
	 * Base numeric mediator/processor checks values and discretizes.
	 * 
	 * @author Ferd
	 *
	 */
	class NumericMediator extends AbstractMediator {

		protected boolean errorsPresent = false;

		protected NumericMediator(IMonitor monitor) {
			super(monitor);
		}

		@Override
		public String getLabel() {
			return discretization == null ? null : "discretize";
		}

		protected Number getValueAsNumber(Object object, INumericObserver otherObserver) {

			Number val = Double.NaN;

			if (object == null || (object instanceof Number && Double.isNaN(((Number) object).doubleValue()))) {
				return val;
			}

			if (object instanceof IndexedCategoricalDistribution) {
				val = ((IndexedCategoricalDistribution) object).getMean();
			} else if (object instanceof Number) {
				val = ((Number) object).doubleValue();
			} else if (object instanceof IConcept && otherObserver.getDiscretization() != null) {

				/*
				 * undiscretize the fucker, we have been warned before.
				 */
				val = otherObserver.getDiscretization().undiscretize((IConcept) object);

			} else {
				try {
					val = object.toString().equals("NaN") ? Double.NaN : Double.parseDouble(object.toString());
				} catch (Exception e) {
					if (!errorsPresent) {
						monitor.error("cannot interpret value: " + object);
						errorsPresent = true;
					}
					return Double.NaN;
				}
			}

			return val;
		}

		@Override
		public Object mediate(Object value) throws KlabException {

			if (isValidating()) {
				if (value instanceof Number) {

					if (!Double.isNaN(((Number) value).doubleValue())) {

						if (Double.isFinite(minimumValue) && ((Number) value).doubleValue() < minimumValue) {
							throw new KlabValidationException("illegal input value " + value + " for " + getId()
									+ ": must not be less than " + minimumValue);
						}
						if (Double.isFinite(maximumValue) && ((Number) value).doubleValue() > maximumValue) {
							throw new KlabValidationException("illegal input value " + value + " for " + getId()
									+ ": must not exceed " + maximumValue);
						}
					}

				} else if (value != null) {
					throw new KlabValidationException("illegal non-numeric input value " + value + " for " + getId());
				}
			}
			return discretization == null ? value : discretization.classify(value);
		}

		@Override
        public String toString() {
			return (isValidating() ? "check [" + minimumValue + "," + maximumValue + "]" : "")
					+ (discretization == null ? "" : " -> discretize");
		}
	};

	/*
	 * Default for numeric observers lets data through but ensures they're not
	 * illegal or outside stated boundaries, and handles discretization if
	 * necessary. (non-Javadoc)
	 * 
	 * @see org.integratedmodelling.api.modelling.runtime.IActiveObserver#
	 * getDataProcessor(org. integratedmodelling.api.modelling.IObserver,
	 * org.integratedmodelling.api.monitoring.IMonitor)
	 */
	@Override
	public IStateContextualizer getDataProcessor(IMonitor monitor) throws KlabException {
		return new NumericMediator(monitor);
	}

	@Override
	protected void serialize(org.integratedmodelling.common.beans.Observer bean) {
		super.serialize(bean);
		if (discretization != null) {
			bean.setClassification(KLAB.MFACTORY.adapt(getDiscretization(), Classification.class));
		}
		bean.setMaximumValue(maximumValue);
		bean.setMinimumValue(minimumValue);
	}

	@Override
	protected void deserialize(org.integratedmodelling.common.beans.Observer bean) {
		super.deserialize(bean);
		if (bean.getClassification() != null) {
			discretization = KLAB.MFACTORY.adapt(bean.getClassification(),
					org.integratedmodelling.common.classification.Classification.class);
		}
		maximumValue = bean.getMaximumValue();
		minimumValue = bean.getMinimumValue();
	}

    @Override
    public IConcept getUndiscretizedObservable() {
        // TODO Auto-generated method stub
        return discretization == null ? getObservable().getType() : null;
    }
	
	

}
