/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.exceptions.KlabInternalRuntimeException;
import org.integratedmodelling.kim.kim.Literal;
import org.integratedmodelling.kim.kim.Metadata;

public class KIMMetadata extends KIMModelObject implements IMetadata, NetworkSerializable {

    protected Map<String, Object> _data = new HashMap<>();

    public KIMMetadata(KIMScope context, Metadata statement, IModelObject parent) {
        super(context, statement, parent);
        for (int i = 0; i < statement.getIds().size(); i++) {

            String id = statement.getIds().get(i);
            EObject vl = statement.getValues().get(i);

            if (vl instanceof Literal) {
                Object o = KIM.processLiteral((Literal) vl);
                if (o != null) {
                    _data.put(id, o);
                } else {
                    context.warning("literal value evaluates to nothing", lineNumber(vl));
                }
            } else if (vl instanceof Metadata) {
                _data.put(id, new KIMMetadata(context.get(KIMScope.Type.METADATA), (Metadata) vl, this));
            }

        }

    }

    @Override
    public void put(String id, Object value) {
        _data.put(id, value);
    }

    @Override
    public Collection<String> getKeys() {
        return _data.keySet();
    }

    @Override
    public Object get(String string) {
        return _data.get(string);
    }

    @Override
    public void merge(IMetadata md, boolean overwriteExisting) {
        if (overwriteExisting) {
            _data.putAll(((KIMMetadata) md)._data);
        } else {
            for (String key : md.getKeys()) {
                if (!_data.containsKey(key))
                    _data.put(key, md.get(key));
            }
        }
    }

    @Override
    public String getString(String field) {
        Object o = _data.get(field);
        return o != null ? o.toString() : null;
    }

    @Override
    public Integer getInt(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).intValue() : null;
    }

    @Override
    public Long getLong(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).longValue() : null;
    }

    @Override
    public Double getDouble(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).doubleValue() : null;
    }

    @Override
    public Float getFloat(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).floatValue() : null;
    }

    @Override
    public Boolean getBoolean(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof Boolean ? (Boolean) o : null;
    }

    @Override
    public IConcept getConcept(String field) {
        Object o = _data.get(field);
        return o != null && o instanceof IConcept ? (IConcept) o : null;
    }

    @Override
    public String getString(String field, String def) {
        Object o = _data.get(field);
        return o != null ? o.toString() : def;
    }

    @Override
    public int getInt(String field, int def) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).intValue() : def;
    }

    @Override
    public long getLong(String field, long def) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).longValue() : def;
    }

    @Override
    public double getDouble(String field, double def) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).doubleValue() : def;
    }

    @Override
    public float getFloat(String field, float def) {
        Object o = _data.get(field);
        return o != null && o instanceof Number ? ((Number) o).floatValue() : def;
    }

    @Override
    public boolean getBoolean(String field, boolean def) {
        Object o = _data.get(field);
        return o != null && o instanceof Boolean ? (Boolean) o : def;
    }

    @Override
    public IConcept getConcept(String field, IConcept def) {
        Object o = _data.get(field);
        return o != null && o instanceof IConcept ? (IConcept) o : def;
    }

    @Override
    public Collection<Object> getValues() {
        return _data.values();
    }

    public void putAll(IMetadata metadata) {
        _data.putAll(((KIMMetadata) metadata)._data);
    }

    @Override
    public void remove(String key) {
        _data.remove(key);
    }

    @Override
    public Map<? extends String, ? extends Object> getDataAsMap() {
        return _data;
    }

    @Override
    public boolean contains(String key) {
        return _data.containsKey(key);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Metadata.class)) {
            throw new KlabInternalRuntimeException("cannot adapt metadata to "
                    + desiredClass.getCanonicalName());
        }
        org.integratedmodelling.common.beans.Metadata ret = new org.integratedmodelling.common.beans.Metadata();
        ret.getData().putAll(_data);
        return (T) ret;
    }

    @Override
    public boolean isEmpty() {
        return _data.isEmpty();
    }

}
