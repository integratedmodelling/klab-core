/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ICondition;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.contextualization.ISwitchingContextualizer;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.kim.expr.GroovyExpression;
import org.integratedmodelling.common.model.runtime.AbstractStateContextualizer;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.ObservationGeneratorSwitch;

public class KIMConditional extends KIMObserver implements IConditionalObserver, NetworkSerializable {

    List<Pair<IModel, IExpression>> models   = new ArrayList<>();
    boolean                         reported = false;

    public class ConditionalProcessor extends AbstractStateContextualizer
            implements ISwitchingContextualizer {

        boolean preprocessed = false;

        protected ConditionalProcessor(IMonitor monitor) {
            super(monitor);
        }

        @Override
        public boolean getConditionValue(int condition, Map<String, Object> inputs) throws KlabException {

            if (!preprocessed) {
                preprocess();
            }

            IExpression cond = models.get(condition).getSecond();
            try {
                Object iif = cond.eval(inputs, monitor);
                if (!(iif instanceof Boolean)) {
                    monitor.error("condition in observer does not evaluate to true or false");
                }
                if (cond instanceof ICondition && ((ICondition) cond).isNegated()) {
                    iif = !((Boolean) iif);
                }
                return (Boolean) iif;
            } catch (Exception e) {
                if (!reported) {
                    reported = true;
                    throw new KlabValidationException(e);
                }
            }
            return false;
        }

        private synchronized void preprocess() {
            preprocessed = true;
            for (Pair<IModel, IExpression> zio : models) {
                if (zio.getSecond() instanceof GroovyExpression) {
                    ((GroovyExpression) zio.getSecond()).initialize(getInputObservers(), getOutputObservers());
                }
            }
        }

        @Override
        public int getConditionsCount() {
            return models.size();
        }

        @Override
        public boolean isNullCondition(int condition) {
            return models.get(condition).getSecond() == null;
        }

        @Override
        public String toString() {
            return "switch " + getName();
        }

        @Override
        public Map<String, Object> initialize(int index, Map<String, Object> inputs) throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
                throws KlabException {
            // TODO Auto-generated method stub
            return null;
        }

        @Override
        public boolean isProbabilistic() {
            // TODO Auto-generated method stub
            return false;
        }

        @Override
        public void notifyConditionalContextualizer(IStateContextualizer actuator, int conditionIndex) {
            // TODO Auto-generated method stub

        }

        @Override
        public String getLabel() {
            return "switch";
        }

    }

    // public class ConditionalActuator extends StateActuator implements
    // ISwitchingActuator {
    //
    // public ConditionalActuator(List<IAction> actions, IMonitor monitor) {
    // super(actions, monitor);
    // }
    //
    // boolean preprocessed = false;
    //
    // @Override
    // public void processState(int stateIndex, ITransition transition) throws
    // KlabException {
    // }
    //
    // @Override
    // public boolean getConditionValue(int condition) throws KlabException {
    //
    // if (!preprocessed) {
    // preprocess();
    // }
    //
    // IExpression cond = models.get(condition).getSecond();
    // try {
    // Object iif = cond.eval(this.parameters, monitor);
    // if (!(iif instanceof Boolean)) {
    // monitor.error(new KlabValidationException("condition in observer does not evaluate
    // to true or false"));
    // }
    // if (cond instanceof ICondition && ((ICondition) cond).isNegated()) {
    // iif = !((Boolean) iif);
    // }
    // return (Boolean) iif;
    // } catch (Exception e) {
    // if (!reported) {
    // reported = true;
    // throw new KlabValidationException(e);
    // }
    // }
    // return false;
    // }
    //
    // private synchronized void preprocess() {
    // preprocessed = true;
    // for (Pair<IModel, IExpression> zio : models) {
    // if (zio.getSecond() instanceof GroovyExpression) {
    // ((GroovyExpression) zio.getSecond()).initialize(this.inputs, this.outputs);
    // }
    // }
    // }
    //
    // @Override
    // public int getConditionsCount() {
    // return models.size();
    // }
    //
    // @Override
    // public boolean isNullCondition(int condition) {
    // return models.get(condition).getSecond() == null;
    // }
    //
    // @Override
    // public String toString() {
    // return "switch " + getName();
    // }
    //
    // @Override
    // public void notifyConditionalAccessor(IActuator actuator, int conditionIndex) {
    // }
    //
    // }

    public KIMConditional(KIMScope context, KIMModel model,
            List<Pair<IObserver, IExpression>> observers, ObservationGeneratorSwitch statement) {
        super(context, model, statement);
        IObserver std = null;
        for (Pair<IObserver, IExpression> oe : observers) {
            if (std == null) {
                std = oe.getFirst();
            } else {
                if (!((KIMObserver) std).isCompatible(oe.getFirst())) {
                    context.error("observer is incompatible with previously defined one", oe.getFirst()
                            .getFirstLineNumber());
                    continue;
                }
            }
            models.add(new Pair<IModel, IExpression>(new KIMModel((KIMObserver) oe.getFirst()), oe
                    .getSecond()));
        }
    }

    @Override
    public List<Pair<IModel, IExpression>> getModels() {
        return models;
    }

    @Override
    public IObserver getRepresentativeObserver() {
        return models.size() == 0 ? null : models.get(0).getFirst().getObserver();
    }

    @Override
    public IConcept getObservationType() {
        return models.size() == 0 ? null : getRepresentativeObserver().getObservationType();
    }

    @Override
    public IObservableSemantics getObservable() {
        return models.size() == 0 ? null : getRepresentativeObserver().getObservable();
    }

    @Override
    public void name(String id) {
        super.name(id);
        for (Pair<IModel, IExpression> zio : models) {
            ((KIMObservingObject) zio.getFirst()).name(id);
        }
    }

    @Override
    public IObserver copy() {
        // TODO for now - don't think this will be called, but in case, let it throw an
        // exception so we know.
        throw new KlabRuntimeException("internal: copy() unimplemented in conditional observer: please report this to developers");
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException {
        return null;
    }


    @Override
    public String toString() {
        return "COND/" + getRepresentativeObserver();
    }

    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {
        return ((NetworkSerializable) getRepresentativeObserver()).serialize(desiredClass);
    }

    @Override
    public IStateContextualizer getDataProcessor(IMonitor monitor)
            throws KlabValidationException {
        return new ConditionalProcessor(monitor);
    }

    @Override
    public String getDefinition() {
        // TODO Auto-generated method stub
        return null;
    }
    
	@Override
	public boolean isExtensive(IConcept extent) {
		return getRepresentativeObserver().isExtensive(extent);
	}

}
