/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IConcreteObserver;
import org.integratedmodelling.api.modelling.IConditionalObserver;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IMediatingObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IValueResolver;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.runtime.IActiveObserver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.model.actuators.LookupTableContextualizer;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabResourceNotFoundException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.ConceptDeclaration;
import org.integratedmodelling.kim.kim.LookupFunction;
import org.integratedmodelling.kim.kim.Observer;

public abstract class KIMObserver extends KIMObservingObject
        implements IObserver, IActiveObserver {

    protected IModel               model;
    protected IObservableSemantics observable;
    protected IObserver            mediated;

    /*
     * these may be defined only in observers that allow direct observations instead of
     * concepts (implementing IConcreteObserver)
     */
    protected List<String>         fromObject    = new ArrayList<>();
    private List<IDirectObserver>  directObjects = null;
    protected boolean              isDiscretized;
    protected boolean              isIndirect;
    protected IConcept             comparisonConcept;

    public KIMObserver() {
        super((KIMScope) null, null, null);
    }

    public KIMObserver(IObserver observer) {
        super((KIMObserver) observer);
        this.model = ((KIMObserver) observer).model;
        this.observable = ((KIMObserver) observer).observable;
        this.mediated = ((KIMObserver) observer).mediated;
    }

    @Override
    public boolean needsResolution() {

        /*
         * TODO check if there are init-scoped set actions and no change actions
         */
        boolean hasSet = false, hasChange = false;
        for (IAction action : getActions()) {
            if (action.getDomain().isEmpty() && action.getType() == IAction.Type.SET) {
                hasSet = true;
            }
            if (action.getDomain().isEmpty() && action.getType() == IAction.Type.CHANGE) {
                hasChange = true;
            }
        }

        boolean needsRes = !(hasSet && !hasChange);

        /*
         * check if there is a datasource
         */
        if (this.model != null && ((KIMModel) this.model).dataSourceGenerator != null) {
            return false;
        }
        /*
         * check if there is a contextualizer and it's a IValueResolver
         */
        if (contextualizerCall != null) {
            Class<?> cclass = KLAB.MFACTORY.getContextualizerClass(contextualizerCall);
            if (cclass != null && IValueResolver.class.isAssignableFrom(cclass)) {
                return false;
            }
        }

        return needsRes;

    }

    public KIMObserver(KIMScope context, IModel model, EObject statement) {

        super(context, statement, model);
        this.model = model;
        /*
         * statement may be an observer or a conditional
         */
        if (statement instanceof Observer) {

            Observer ostatement = (Observer) statement;

            this.isDiscretized = ostatement.getDiscretization() != null;
            this.isIndirect = ostatement.isDerived();

            if (ostatement.getOther() != null) {
                this.comparisonConcept = new KIMKnowledge(context
                        .get(KIMScope.Type.COMPARISON_OBSERVABLE), ostatement
                                .getOther(), null).getConcept();
            }

            if (ostatement.getConcept() != null || (ostatement.getObservable() != null
                    && ostatement.getObservable().getConcept() != null)) {

                ConceptDeclaration declaration = ostatement.getConcept() == null ? ostatement.getObservable()
                        .getConcept() : ostatement.getConcept();
                KIMKnowledge k = new KIMKnowledge(context
                        .get(KIMScope.Type.OBSERVER_OBSERVABLE), declaration, null, this instanceof IConcreteObserver);

                if (k.isNothing()) {

                    /*
                     * TODO may have an object if observer allows it (in this case it
                     * would be distance only).
                     */
                    if (this instanceof IConcreteObserver && k.getObjectDeclarations().size() > 0) {
                        fromObject.addAll(k.getObjectDeclarations());
                    } else {
                        return;
                    }
                }

                this.observable = new KIMObservableSemantics(context
                        .get(KIMScope.Type.OBSERVER_OBSERVABLE), k);
                ((ObservableSemantics) observable).setObservationType(this.getObservationType());
                ((ObservableSemantics) observable).setObserver(this);
                ((ObservableSemantics) this.observable)
                        .setType(interpretType(context, ostatement, this.observable.getType()));

            } else if (ostatement.getObservable() != null
                    && ostatement.getObservable().getConceptStatement() != null) {

                this.observable = new KIMObservableSemantics(context
                        .get(KIMScope.Type.OBSERVER_OBSERVABLE), new KIMKnowledge(context
                                .get(KIMScope.Type.OBSERVER_OBSERVABLE), ostatement.getObservable()
                                        .getConceptStatement(), this));

                try {
                    ((ObservableSemantics) observable).setObservationType(this.getObservationType());
                } catch (KlabRuntimeException e) {
                    context.error(e
                            .getMessage(), lineNumber(ostatement.getObservable().getConceptStatement()));
                }
                ((ObservableSemantics) observable).setObserver(this);
                ((ObservableSemantics) this.observable)
                        .setType(interpretType(context, ostatement, this.observable.getType()));

            } else if (ostatement.getObservable() != null
                    && ostatement.getObservable().getMediated() != null) {

                if (!(this instanceof IMediatingObserver)) {
                    context.error("this observer cannot mediate another", lineNumber(ostatement
                            .getObservable().getMediated()));
                }

                this.mediated = defineObserver(context, (KIMModel) model, ostatement.getObservable()
                        .getMediated());

                if (this.mediated.getObservable() != null) {
                    try {
                        this.observable = new ObservableSemantics(interpretType(context, ostatement, this.mediated
                                .getObservable()
                                .getType()), this
                                        .getObservationType(), this.mediated.getObservable().getFormalName());
                        ((ObservableSemantics) observable).setObserver(this);
                    } catch (KlabRuntimeException e) {
                        context.error(e.getMessage(), getFirstLineNumber());
                    }
                }
            }

            defineDependencies(context, ostatement.getDependencies());

            if (ostatement.getAccessor() != null) {
                this.contextualizerCall = new KIMFunctionCall(context
                        .get(KIMScope.Type.ACTUATOR_FUNCTION_CALL), ostatement
                                .getAccessor());
                if (!context.suppressContextualizerValidation()) {	
                	validateFunctionCall(context, this.contextualizerCall, KLAB.c(NS.STATE_CONTEXTUALIZER));
                }
            } else if (ostatement.getLookup() != null) {

                /*
                 * parse a lookup table and ask the resolver for the accessor that will
                 * lookup data from it.
                 */
                LookupFunction lfunc = ostatement.getLookup();
//                ITable lookupTable = null;
                if (lfunc.getRef() != null) {
                    Object table = context.getNamespace().getSymbolTable().get(lfunc.getRef());
                    if (!(table instanceof ITable)) {
                        context.error("object identified by "
                                + lfunc.getRef()
                                + " is unknown or is not a lookup table", lineNumber(lfunc));
                    } else {
                        this.lookupTable = (ITable) table;
                    }
                } else if (lfunc.getTable() != null) {
                    this.lookupTable = new KIMLookupTable(context.get(KIMScope.Type.LOOKUP_TABLE), lfunc.getTable());
                }
                this.contextualizerCall = new LookupTableContextualizer(this.lookupTable, context
                        .getNamespace(), lfunc
                                .getArgs());
            }

            if (ostatement.getContextualizers() != null) {

                Pair<List<IFunctionCall>, List<IAction>> zio = defineContextActions(context, this, ostatement
                        .getContextualizers());

                /*
                 * float the scale specs if any
                 */
                if (scaleGenerators.size() > 0) {
                    ((KIMModel) model).scaleGenerators.addAll(scaleGenerators);
                }

                for (IFunctionCall function : zio.getFirst()) {
                    scaleGenerators.add(function);
                }

                for (IAction action : zio.getSecond()) {
                    actions.add(action);
                }
            }
            
//            if (ostatement.getLookup() != null) {
//                this.lookupTable = new KIMLookupTable(context, ostatement.getLookup().getTable());
//            }
//            
            if (ostatement.getName() != null) {
                name(ostatement.getName());
            }
        }
    }

    /**
     * If there is a "by" or "down to" clause external to the declaration (this can only
     * happen in discretizations and classifications, and must happen before
     * getObservedType() is called), reinterpret accordingly; then defer to the specific
     * observer for any semantics reinterpretation necessary.
     * 
     * @param context
     * @param ostatement
     * @param statedType
     * @return
     */
    protected IConcept interpretType(KIMScope context, Observer ostatement, IConcept statedType) {

        String downToId = null;
        String byTraitId = null;

        try {

            /*
             * we turn concepts into appropriate observables before discretization if any;
             * if we're dealing with a classified type, getObservedType() is called last
             * to turn the concept into a trait.
             */
            if (!(this instanceof IClassifyingObserver)) {
                statedType = getObservedType(context, statedType);
            }

            if (ostatement.getTrait() != null) {
                // discretized numeric observer
                byTraitId = ostatement.getTrait().getId();
                downToId = ostatement.getTrait().getDownTo();
            } else if (ostatement.getObservable() != null
                    && ostatement.getObservable().getByTraits() != null) {
                // classification observer
                byTraitId = ostatement.getObservable().getByTraits();
                downToId = ostatement.getObservable().getDownToId();
            }

            if (byTraitId != null || downToId != null) {
                IConcept byTrait = null;
                IConcept downTo = null;
                if (byTraitId != null) {
                    byTrait = KLAB.c(byTraitId);
                    if (byTrait == null || !NS.isTrait(byTrait)) {
                        context.error("cannot use non-trait " + ostatement.getObservable().getByTraits()
                                + " in a 'by' clause", lineNumber(ostatement.getObservable()));
                    }
                }
                if (downToId != null) {
                    downTo = KLAB.c(downToId);
                    if (downTo == null || !downTo.is(byTrait)) {
                        context.error("cannot use " + ostatement.getObservable().getDownToId()
                                + " as a resolution specification for "
                                + (byTrait == null ? "no trait" : byTrait), lineNumber(ostatement
                                        .getObservable()));
                    }
                }

                try {
                    statedType = Observables
                            .declareObservable(statedType, null, null, null, null, byTrait, downTo, KLAB.REASONER
                                    .getOntology());
                } catch (KlabValidationException e) {
                    context.error(e.getMessage(), lineNumber(ostatement.getObservable()));
                }

            }

            if (this instanceof IClassifyingObserver) {
                return getObservedType(context, statedType);
            }
//            else if (NS.isTrait(statedType)) {
//                /*
//                 * resulting from discretization - if not, the previously called
//                 * getObservedType will have signaled an error.
//                 */
//                return Observables.makeTypeFor(statedType);
//            }
        } catch (KlabRuntimeException e) {
            context.error(e.getMessage(), lineNumber(ostatement.getObservable() == null ? ostatement : ostatement.getObservable()));
        }
        
        return statedType;
    }

    public abstract IObserver copy();

    public IModel getTopLevelModel() {
        return getTopLevelModel(this);
    }

    @Override
    public IStateContextualizer getContextualizer(IResolutionScope scope, IProvenance.Artifact provenance, IMonitor monitor)
            throws KlabException {
        IContextualizer ret = super.getContextualizer(scope, provenance, monitor);
        if (ret != null && !(ret instanceof IStateContextualizer)) {
            throw new KlabValidationException("contextualizer returned by observer " + getName()
                    + " is not a state contextualizer");
        }
        return (IStateContextualizer) ret;
    }

    protected IModel getTopLevelModel(KIMModelObject object) {
        if (object instanceof IModel && object.parent == null) {
            return (IModel) object;
        }
        return object.parent == null ? null : getTopLevelModel((KIMModelObject) object.parent);
    }

    /**
     * Check compatibility; used within a set of conditional observers.
     * 
     * @param observer
     * @return
     */
    boolean isCompatible(IObserver observer) {
        // TODO check the units etc
        return this.getClass().equals(observer.getClass());
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    protected void name(String id) {
        super.name(id);
        if (observable != null) {
            ((ObservableSemantics) observable).setFormalName(id);
        }
        if (mediated != null) {
            ((KIMObserver) mediated).name(id);
        }
    }

    /**
     * Filter the observable type, if necessary, to make it match the result of this
     * observation made on the stated type.
     * 
     * @param knowledge
     * @return
     */
    protected IConcept getObservedType(KIMScope context, IConcept knowledge) {
        return knowledge;
    }

    @Override
    public IObservableSemantics getObservable() {
        return observable;
    }

    @Override
    public IConcept getType() {
        return observable.getType();
    }

    public IObserver getRepresentativeObserver() {
        return (this instanceof KIMConditional) ? getRepresentativeObserver() : this;
    }

    /**
     * utility to check observers in mediators - gives us the observer that has the
     * semantics of the data, being smart about conditional observers.
     * 
     * @param observer
     * @return an observer that represents our semantics.
     */
    static public IObserver getRepresentativeObserver(IObserver observer) {
        return (observer instanceof IConditionalObserver) ? ((KIMConditional) observer)
                .getRepresentativeObserver() : observer;
    }

    public boolean isCompatibleWith(IKnowledge type) {
        return type.is(getObservable().getType());
    }

    protected Collection<IDirectObserver> resolveConcreteIds() throws KlabException {
        if (directObjects == null) {
            directObjects = new ArrayList<>();
            for (String s : fromObject) {
                IDirectObserver obs = KLAB.ENGINE.retrieveObservation(s, null);
                if (obs == null) {
                    throw new KlabResourceNotFoundException("observation " + s
                            + " was not found either locally or remotely");
                }
                directObjects.add(obs);
            }
        }
        return directObjects;
    }

    protected IConcept getObservableConcept(KIMScope context, KIMKnowledge observed) {
        return null;
    }

    /*
     * call from the legitimate serialize() to set the class name.
     */
    protected void serialize(org.integratedmodelling.common.beans.Observer bean) {
        bean.setObserverClass(this.getClass().getCanonicalName());
        bean.setObservable(KLAB.MFACTORY
                .adapt(getObservable(), org.integratedmodelling.common.beans.Observable.class));
    }

    protected void deserialize(org.integratedmodelling.common.beans.Observer bean) {
        this.observable = KLAB.MFACTORY.adapt(bean.getObservable(), ObservableSemantics.class);
    }

    /**
     * Called when this observer is resolved by another - e.g. when data sources are
     * matched to dependencies). This allows the resolved observer to customize itself
     * after resolution if necessary. When this is called, mediation and other
     * communication has been assessed already.
     * 
     * NOTE: if any modifications are made, the modified observer must be a NEW one. Use
     * copy() - if that isn't done, concurrent use will be disrupted.
     * 
     * @param observer
     */
    public IActiveObserver notifyResolution(IActiveObserver observer) {
        return this;
    }

    /**
     * Reconstruct our k.IM definition.
     * 
     * @return
     */
    abstract public String getDefinition();
 
}
