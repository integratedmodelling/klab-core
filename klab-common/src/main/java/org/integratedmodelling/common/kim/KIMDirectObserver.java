/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.Observable;
import org.integratedmodelling.common.beans.Observation;
import org.integratedmodelling.common.beans.Observer;
import org.integratedmodelling.common.beans.Scale;
import org.integratedmodelling.common.beans.Subject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.kim.kim.Annotation;
import org.integratedmodelling.kim.kim.ObserveStatement;
import org.integratedmodelling.kim.kim.RoleStatement;
import org.integratedmodelling.kim.kim.State;

/**
 * Same bean for all direct observations. The model factory will discriminate and decide
 * what to build.
 * 
 * Serializes to a Subject bean for storage in ObservationKbox (version 2). Should only be
 * serialized within an engine (otherwise the scale will not be produced).
 * 
 * @author ferdinando.villa
 *
 */
public class KIMDirectObserver extends KIMObservingObject implements IDirectObserver, NetworkSerializable {

	// not at API level for now. Observers can only have stated roles
	// within scenarios.
    public static class RoleDescriptor {
        public List<IConcept> roles    = new ArrayList<>();
        public List<IConcept> targets  = new ArrayList<>();
        public List<IConcept> contexts = new ArrayList<>();
    }

    IObservableSemantics          observable;
    List<Pair<IObserver, Object>> literalStates = new ArrayList<>();
    List<IDirectObserver>         secondary     = new ArrayList<>();
    List<RoleDescriptor>          roles         = new ArrayList<>();

    public KIMDirectObserver(KIMScope context, ObserveStatement statement, IModelObject parent,
            List<Annotation> annotations) {

        super(context, statement, parent, annotations);

        this.id = statement.getName();

        KIMKnowledge k = new KIMKnowledge(context
                .get(KIMScope.Type.OBSERVER_OBSERVABLE), statement.getConcept(), null);

        this.observable = new ObservableSemantics(k.getType());

        if (this.observable.getType() != null && this.observable.getType().isAbstract()) {
            context.error("the type for an observation cannot be abstract: abstract types are only "
                    + "admitted in dependencies or 'resolve' statements", lineNumber(statement
                            .getConcept()));
        }

        if (!NS.isDirect(this.observable.getType())) {
            context.error("only direct observables are admitted in observation statements", lineNumber(statement
                    .getConcept()));
        }

        if (statement.getContextualizers() != null) {
            Pair<List<IFunctionCall>, List<IAction>> zio = KIMObservingObject
                    .defineContextActions(context, this, statement
                            .getContextualizers());

            for (IFunctionCall function : zio.getFirst()) {
                scaleGenerators.add(function);
            }

            for (IAction action : zio.getSecond()) {
                actions.add(action);
            }
        }

        for (State state : statement.getStates()) {
            if (state.getObserver() != null) {
                IObserver observer = KIMObservingObject.defineObserver(context
                        .get(KIMScope.Type.OBSERVATION_STATE_OBSERVER), null, state.getObserver());
                Object value = null;
                if (state.getLiteral() != null) {
                    value = KIM.processLiteral(state.getLiteral());
                } else if (state.getFunction() != null) {
                    value = new KIMFunctionCall(context.get(KIMScope.Type.FUNCTIONCALL), state.getFunction());
                }

                literalStates.add(new Pair<>(observer, value));

            } else if (state.getObservation() != null) {
                children.add(new KIMDirectObserver(context.get(KIMScope.Type.OBSERVATION), state
                        .getObservation(), this, new ArrayList<Annotation>()));
            }
        }

        if (statement.getRoles() != null && !statement.getRoles().isEmpty()) {

            if (!context.getNamespace().isScenario()) {
                context.error("role attribution in observation is only allowed in scenarios", lineNumber(statement.getRoles().iterator().next()));
            }
            
            for (RoleStatement role : statement.getRoles()) {

                RoleDescriptor rd = new RoleDescriptor();

                for (IKnowledgeObject co : new KIMConceptList(context
                        .get(KIMScope.Type.ROLE_DESCRIPTION_LIST), role.getRole(), null)) {
                    if (!co.isConcept() || !NS.isRole(co.getConcept())) {
                        context.error("the role statement does not mention a role", co.getFirstLineNumber());
                    }
                    rd.roles.add(co.getConcept());
                }

                for (IKnowledgeObject co : new KIMConceptList(context
                        .get(KIMScope.Type.ROLE_DESCRIPTION_LIST), role.getRestrictedObservable(), null)) {
                    if (!co.isConcept()) {
                        context.error("unknown concept in role target", co.getFirstLineNumber());
                    }
                    rd.targets.add(co.getConcept());
                }

                for (IKnowledgeObject co : new KIMConceptList(context
                        .get(KIMScope.Type.ROLE_DESCRIPTION_LIST), role.getRestrictedObservable(), null)) {
                    if (!co.isConcept() /* || !NS.isProcess(co.getConcept()? */) {
                        context.error("unknown concept for role context", co.getFirstLineNumber());
                    }
                    rd.contexts.add(co.getConcept());
                }

                roles.add(rd);
            }
        }
    }

    /**
     * Creates a new object with the same exact data using the installed model factory.
     * Will sanitize scales etc. to ensure they're created by the currently active
     * factory, so it's good to turn a client implementation into an engine one and the
     * other way around.
     * 
     * TODO doesn't work YET!
     * 
     * @param observer
     */
    public KIMDirectObserver(KIMDirectObserver observer) {
        super(KLAB.MMANAGER
                .getNamespace(observer.getNamespace().getId()), observer
                        .getId());
        this.observable = observer.getObservable();
    }

    public KIMDirectObserver(INamespace namespace, IConcept observable, String id, IScale scale) {
        super(namespace, id);
        this.observable = new ObservableSemantics(observable);
        this.scale = scale;
    }

    public KIMDirectObserver(IConcept observable, String id, IScale scale) {
        super(KLAB.MMANAGER.getLocalNamespace(), id);
        this.observable = new ObservableSemantics(observable);
        this.scale = scale;
    }

    @Override
    public IConcept getType() {
        return observable.getType();
    }

    @Override
    public IObservableSemantics getObservable() {
        return observable;
    }

    @Override
    public List<Pair<IObserver, Object>> getStatesDefinition() {
        return literalStates;
    }

    @Override
    public String toString() {
        return "O/" + getName() + "/" + getObservable();
    }

    @Override
    public List<IDirectObserver> getChildrenDefinition() {
        return secondary;
    }

    public Collection<RoleDescriptor> getStatedRoles() {
    	return roles;
    }
    
    /**
     * Scale serialization will only work properly in an engine context.
     */
    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(Observation.class)) {
            throw new KlabRuntimeException("cannot serialize a IDirectObserver to a "
                    + desiredClass.getCanonicalName());
        }

        Subject ret = new Subject();

        ret.setObservable(KLAB.MFACTORY.adapt(this.observable, Observable.class));
        ret.setName(this.getName());
        ret.setNamespace(this.getNamespace().getId());
        ret.setId(this.getId());

        IScale scale = this.getCoverage(KLAB.ENGINE.getMonitor());
        if (scale != null) {
            ret.setScale(KLAB.MFACTORY.adapt(scale, Scale.class));
        }

        for (Pair<IObserver, Object> state : literalStates) {

            org.integratedmodelling.common.beans.State sbean = new org.integratedmodelling.common.beans.State();
            sbean.setConstantData(state.getSecond());
            sbean.setObserver(KLAB.MFACTORY.adapt(state.getFirst(), Observer.class));
            sbean.setObservable(KLAB.MFACTORY.adapt(state.getFirst().getObservable(), Observable.class));

            ret.getStates().add(sbean);
        }

        for (IDirectObserver child : secondary) {
            ret.getChildSubjects().add(((KIMDirectObserver) child).serialize(Subject.class));
        }

        return (T) ret;
    }

}
