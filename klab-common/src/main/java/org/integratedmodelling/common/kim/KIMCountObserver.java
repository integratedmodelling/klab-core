/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICountingObserver;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.IValuingObserver;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.contextualization.IDirectInstantiator;
import org.integratedmodelling.api.modelling.contextualization.IEventInstantiator;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.contextualization.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.model.Coverage;
import org.integratedmodelling.common.model.runtime.AbstractStateContextualizer;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabUnsupportedOperationException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMCountObserver extends KIMNumericObserver
        implements ICountingObserver, NetworkSerializable, NetworkDeserializable {

    IUnit                       unit;
    Double                      cellAreaConversion = null;
    private IDirectInstantiator objectContextualizer;
    private IConcept            originalConcept;

    /**
     * TODO this is still a presence calculator
     * 
     * @author ferdinando.villa
     *
     */
    class DensityComputer extends AbstractStateContextualizer {

        int[]              counts = null;
        boolean            inited = false;
        IDirectObservation context;
        private boolean    exists;
        List<IShape>       shapes;

        protected DensityComputer(IMonitor monitor) {
            super(monitor);
        }

        @Override
        public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance)
                throws KlabValidationException {
            if (objectContextualizer != null) {
                objectContextualizer.setContext(parameters, model, project, provenance);
            }
            super.setContext(parameters, model, project, provenance);
        }

        @Override
        public Map<String, IObservation> define(String name, IObserver observer, IActiveDirectObservation contextSubject, IResolutionScope context, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, boolean isLastInChain, IMonitor monitor)
                throws KlabException {

            if (objectContextualizer != null) {
                objectContextualizer
                        .initialize((IActiveSubject) contextSubject, context, observer
                                .getModel(), expectedInputs, expectedOutputs, monitor);
            }

            this.context = contextSubject;

            return super.define(name, observer, contextSubject, context, expectedInputs, expectedOutputs, isLastInChain, monitor);
        }

        @Override
        public Map<String, Object> initialize(int index, Map<String, Object> inputs)
                throws KlabException {
            return MapUtils
                    .ofWithNull(getStateName(), processState(index, ITransition.INITIALIZATION));
        }

        private Object processState(int stateIndex, ITransition transition)
                throws KlabException {

            IGrid grid = context.getScale().isSpatiallyDistributed()
                    ? context.getScale().getSpace().getGrid() : null;

            if (!inited) {

                Map<String, IObservation> objects = null;
                this.exists = false;

                IConcept originalConcept = Observables
                        .getInherentType(getObservable().getType());

                if (originalConcept != null) {

                    if (getContextObservation()
                            .getResolvedCoverage(originalConcept) != null
                            && getContextObservation() instanceof ISubject) {

                        if (NS.isThing(originalConcept)) {
                            for (ISubject s : ((ISubject) getContextObservation())
                                    .getSubjects()) {
                                if (s.getObservable().getType().is(originalConcept)) {
                                    if (objects == null) {
                                        objects = new HashMap<>();
                                    }
                                    objects.put(s.getName(), s);
                                }
                            }
                        } else if (NS.isEvent(originalConcept)) {
                            for (IEvent s : ((ISubject) getContextObservation())
                                    .getEvents()) {
                                if (s.getObservable().getType().is(originalConcept)) {
                                    if (objects == null) {
                                        objects = new HashMap<>();
                                    }
                                    objects.put(s.getName(), s);
                                }
                            }

                        }
                    }

                } else {
                    if (objectContextualizer != null) {
                        if (objectContextualizer instanceof ISubjectInstantiator) {
                            objects = ((ISubjectInstantiator) objectContextualizer)
                                    .createSubjects((IActiveSubject) getContextObservation(), transition, new HashMap<>());
                        } else if (objectContextualizer instanceof IEventInstantiator) {
                            objects = ((IEventInstantiator) objectContextualizer)
                                    .createEvents(transition, new HashMap<>());
                        }
                    }
                }

                if (objects != null && objects.size() > 0) {

                    shapes = new ArrayList<>();

                    for (IObservation object : objects.values()) {

                        /*
                         * TODO attach a listener to the object to set inited to false if
                         * the object goes away - in that case, it needs to rescan all the
                         * objects from the main subject, keeping a list of the IDs. For
                         * now, new things come in but old things do not disappear.
                         */
                        if (object.getScale().getSpace() != null
                                && context.getScale().getSpace() != null) {

                            IShape shape = object.getScale().getSpace().getShape();
                            if (shape != null) {
                                shapes.add(shape);
                            }
                        }
                    }
                }

                if (grid != null) {
                    this.counts = KLAB.MFACTORY.countShapes(shapes, grid);
                } else if (context.getScale().isSpatiallyDistributed()) {
                    throw new KlabUnsupportedOperationException("density calculations unsupported in non-grid extents unsupported");
                }

                inited = true;
            }

            if (counts != null) {

                int sfs = getScale().getExtentOffset(getScale().getSpace(), stateIndex);
                return adjustForCellSize(counts[sfs], grid);

            }

            return Boolean.FALSE;
        }

        private Object adjustForCellSize(int n, IGrid grid) {
            double ret = n;
            if (unit != null) {
                if (cellAreaConversion == null) {
                    double cellarea = grid.getCellArea(true);
                    cellAreaConversion = unit.convert(cellarea, new Unit("m^2")).doubleValue();
                }
                ret = ret / cellAreaConversion;
            }
            return ret;
        }

        @Override
        public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
                throws KlabException {
            return MapUtils.ofWithNull(getStateName(), processState(index, transition));
        }

        @Override
        public boolean isProbabilistic() {
            return false;
        }

        @Override
        public String getLabel() {
            return "density calculation";
        }

    }

    @Override
    public IStateContextualizer getDataProcessor(IMonitor monitor)
            throws KlabException {
        return objectContextualizer == null ? super.getDataProcessor(monitor)
                : new DensityComputer(monitor);
    }

    class CountMediator extends NumericMediator {

        protected CountMediator(ICountingObserver other, IMonitor monitor) {
            super(monitor);
            this.other = other;
            this.convertUnits = unit != null && other.getUnit() != null && !unit.equals(other.getUnit());
        }

        ICountingObserver other;
        boolean           convertUnits = false;

        @Override
        public Object mediate(Object object) throws KlabException {

            Number val = getValueAsNumber(object, other);

            if (!Double.isNaN(val.doubleValue()) && convertUnits) {
                val = unit.convert(val, other.getUnit());
            }

            return super.mediate(val);

        }

        @Override
        public String getLabel() {
            return (unit.equals(other.getUnit()) ? "" : "convert " + other.getUnit() + " to " + unit) +
                    (discretization == null ? ""
                            : ((unit.equals(other.getUnit()) ? "" : "->") + "discretize"));
        }

    }

    public KIMCountObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        minimumValue = 0;
        if (statement.getUnit() != null) {
            unit = new KIMUnit(context.get(KIMScope.Type.UNIT), statement.getUnit());
            /*
             * TODO check must be distributional only
             */
        }
        // if (observable != null) {
        // ((ObservableSemantics) observable)
        // .setType(this.getObservedType(context, this.observable.getType()));
        // }

        validate(context);
    }

    public KIMCountObserver(IObserver observer) {
        super(observer);
        minimumValue = 0;
        if (!(observer instanceof ICountingObserver)) {
            throw new KlabRuntimeException("cannot initialize a counting observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.unit = ((ICountingObserver) observer).getUnit();
    }

    /**
     * Only for the deserializer
     */
    public KIMCountObserver() {
        minimumValue = 0;
    }

    @Override
    public IObserver copy() {
        return new KIMCountObserver(this);
    }

    KIMCountObserver(IConcept observable, String unit2) {
        minimumValue = 0;
        this.observable = new KIMObservableSemantics(observable);
        if (unit2 != null) {
            this.unit = new Unit(unit2);
        }
    }

    KIMCountObserver(IConcept observable) {
        minimumValue = 0;
        this.observable = new KIMObservableSemantics(observable);
    }

    @Override
    public IUnit getUnit() {
        return unit;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.COUNT_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMScope context, IConcept concept) {

        this.originalConcept = concept;

        IConcept ret = Observables.makeCount(concept);
        if (ret == null) {
            context.error("only things or agents can be counted", getFirstLineNumber());
            return concept;
        }
        return ret;
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException {

        observer = getRepresentativeObserver(observer);

        if (observer instanceof ICountingObserver) {

            if (!observer.getObservable().is(getObservable()))
                throw new KlabValidationException("counts can only mediate other counts of the same thing");

            if ((((ICountingObserver) observer).getUnit() == null && unit != null)
                    || (((ICountingObserver) observer).getUnit() != null && unit == null))
                throw new KlabValidationException("counts with units can only mediate other counts with units");

            if (discretization != null) {
                if (((IValuingObserver) observer).getDiscretization() != null) {
                    monitor.warn(getObservable().getType()
                            + ": discretized counts should not mediate other discretized counts: value will be undiscretized, then discretized again");
                }
                return new CountMediator((ICountingObserver) observer, monitor);
            }

            if (((((ICountingObserver) observer).getUnit() == null && unit == null)
                    || ((ICountingObserver) observer)
                            .getUnit().equals(getUnit()))) {
                return null;
            }
        }

        return new CountMediator((ICountingObserver) observer, monitor);
    }

    @Override
    public String toString() {
        return "CNT/" + getObservable() + (unit == null ? "" : (" (" + unit + ")"));
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        if (bean.getUnitOrCurrency() != null) {
            unit = new Unit(bean.getUnitOrCurrency());
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        if (unit != null) {
            ret.setUnitOrCurrency(unit.asText());
        }

        return (T) ret;
    }

    @Override
    public String getDefinition() {
        return "count " + Observables.getDeclaration(originalConcept, getNamespace().getProject())
                + (unit == null ? "" : (" per " + unit));
    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return unit == null || !unit.isDensity(extent);
    }

    @Override
    public List<IObservableSemantics> getAlternativeObservables() {
        IConcept c = Observables.getInherentType(getObservable().getType());
        if (c != null && NS.isObservable(c)) {
            return Collections.singletonList(new ObservableSemantics(c));
        }
        return null;
    }

    @Override
    public ICoverage acceptAlternativeContextualizer(IContextualizer contextualizer, ICoverage coverage) {
        if (contextualizer instanceof IDirectInstantiator) {
            objectContextualizer = (IDirectInstantiator) contextualizer;
            return Coverage.FULL(scale);
        }
        return Coverage.EMPTY;
    }

}
