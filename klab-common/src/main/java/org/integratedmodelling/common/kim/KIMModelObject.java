/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.integratedmodelling.api.data.IList;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.lists.PolyList;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.kim.kim.Annotation;
import org.integratedmodelling.kim.kim.Literal;
import org.integratedmodelling.kim.kim.Value;

public class KIMModelObject extends KIMLanguageObject implements IModelObject {

	String id;
	INamespace namespace;
	IModelObject parent;
	List<IAnnotation> annotations = new ArrayList<>();
	List<IModelObject> children = new ArrayList<>();
	IMetadata metadata = new Metadata();
	boolean firstClass;
	boolean isDeprecated;
	boolean isComputed;

	int errorCount = 0;
	int warningCount = 0;

	KIMModelObject(KIMModelObject object) {
		super(object);
		if (object != null) {
			this.id = object.id;
			this.namespace = object.namespace;
		}
	}

	public KIMModelObject(INamespace namespace, String id) {
		super((EObject) null);
		this.id = id;
		this.namespace = namespace;
	}

	public KIMModelObject(KIMScope context, EObject statement, IModelObject parent, List<Annotation> annotations) {

		super(statement);
		setParent(parent, false);
		context.accept(this);
		this.namespace = context.getNamespace();
		for (Annotation a : annotations) {
			IAnnotation annotation = new KIMAnnotation(context.get(KIMScope.Type.ANNOTATION), a, this);
			this.annotations.add(annotation);
			if (annotation.getId().equals(IAnnotation.DEPRECATED)) {
				isDeprecated = true;
			}
		}
		firstClass = context.firstClass;
	}

	protected void setParent(IModelObject parent2, boolean resetChildren) {
		this.parent = parent2;
		if (parent2 != null) {
			if (resetChildren) {
				((KIMModelObject) parent2).children.clear();
			}
			((KIMModelObject) parent2).children.add(this);
		}
	}

    protected void processMetadata(org.integratedmodelling.kim.kim.Metadata metadata) {
        // TODO Auto-generated method stub
        
    }
	
	public KIMModelObject(KIMScope context, EObject statement, IModelObject parent) {
		super(statement);
		setParent(parent, false);
		if (context != null) {
			context.accept(this);
			this.namespace = context.getNamespace();
		}
	}

	@Override
	public INamespace getNamespace() {
		return namespace;
	}

	@Override
	public String getName() {
		return namespace.getId() + "." + getId();
	}

	@Override
	public IMetadata getMetadata() {
		return metadata;
	}

	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public boolean isPrivate() {
		return namespace.isPrivate();
	}

	@Override
	public boolean isInactive() {
		return namespace.isInactive();
	}

	@Override
	public Collection<IAnnotation> getAnnotations() {
		return annotations;
	}

	public IAnnotation getAnnotation(String id) {
	    for (IAnnotation a : annotations) {
	        if (a.getId().equals(id)) {
	            return a;
	        }
	    }
	    return null;
	}
	
	@Override
	public int getErrorCount() {
		return errorCount;
	}

	@Override
	public int getWarningCount() {
		return warningCount;
	}

	@Override
	public boolean isFirstClass() {
		return firstClass;
	}

	@Override
	public boolean isDeprecated() {
		KIMModelObject mo = this;
		while (mo != null) {
			if (mo.isDeprecated) {
				return true;
			}
			mo = (KIMModelObject) mo.parent;
		}
		return false;
	}

	@Override
	public Collection<IModelObject> getChildren() {
		return children;
	}

	@Override
	public IModelObject getParent() {
		return parent;
	}

	public static Object defineValue(KIMScope context, Value value) {

		Object ret = null;

		if (value.getId() != null) {
			ret = context.getNamespace().getSymbolTable().get(value.getId());
			if (ret == null) {
				context.error("identifier " + value.getId() + " has not been defined or imported", lineNumber(value));
			}
		} else if (value.getLiteral() != null) {
			ret = KIM.processLiteral(value.getLiteral());
		} else if (value.getExpr() != null) {
			ret = new KIMExpression(context.get(KIMScope.Type.FUNCTIONCALL), value);
		} else if (value.getFunction() != null) {
			ret = new KIMFunctionCall(context.get(KIMScope.Type.FUNCTIONCALL), value.getFunction());
		} else if (value.getList() != null) {
			ret = defineList(context.get(KIMScope.Type.LIST_LITERAL), value.getList());
		}

		return ret;
	}

	private static IList defineList(KIMScope context, org.integratedmodelling.kim.kim.List list) {

		IList ret = null;
		if (list != null) {

			ArrayList<Object> ct = new ArrayList<>();

			for (EObject zio : list.getContents()) {
				if (zio instanceof org.integratedmodelling.kim.kim.List)
					ct.add(defineList(context.get(KIMScope.Type.LIST_LITERAL), (org.integratedmodelling.kim.kim.List) zio));
				else if (zio instanceof Literal) {
					// treat commas inside lists as whitespace. TODO we may use
					// them for
					// grouping at some point.
					if (!((Literal) zio).isComma()) {
						ct.add(KIM.processLiteral((Literal) zio));
					}
				}
			}
			ret = PolyList.fromCollection(ct);
		}

		return ret;
	}

	/**
	 * Validate a function call to return a value that is the specified concept.
	 * Returns the actually returned concept if valid; null otherwise. Uses
	 * context to report any errors and returns null also if the prototype for
	 * the function cannot be found - using info if no engine is active and
	 * error if the function is indeed unknown.
	 * 
	 * @param context
	 * @param call
	 * @param concept
	 * @return the actual concept returned
	 */
	public static IConcept validateFunctionCall(KIMScope context, IFunctionCall call, IConcept... concept) {

		if (call.getPrototype() == null) {
			if (KLAB.ENGINE != null) {
				context.error(call.getId() + ": this function is unknown in the current execution environment",
						call.getFirstLineNumber());
			} else {
				context.info(
						"cannot establish return type for " + call.getId()
								+ (KLAB.ENGINE == null ? ": function unknown" : ": engine is stopped"),
						call.getFirstLineNumber());
			}
		} else {

			IPrototype prot = call.getPrototype();

			String s = "";
			for (IConcept c : concept) {
				for (IConcept p : prot.getReturnTypes()) {
					if (p.is(c)) {
						return p;
					}
				}
				s += (s.isEmpty() ? "" : ", ") + CamelCase.toLowerCase(c.getLocalName(), ' ');
			}
			context.error(call.getId() + ": wrong return type for function: expecting " + s, call.getFirstLineNumber());
		}

		return null;
	}

	@Override
	public void setNamespace(INamespace namespace) {
		this.namespace = namespace;
	}

	public void setDeprecated(boolean b) {
		this.isDeprecated = b;
	}
}
