/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.List;

import org.integratedmodelling.api.data.IRankingScale;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IRankingObserver;
import org.integratedmodelling.api.modelling.IValueMediator;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.RankingScale;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMRankingObserver extends KIMNumericObserver
        implements IRankingObserver, NetworkSerializable, NetworkDeserializable {

    IRankingScale rankingScale;
    boolean       integer;

    public class RankingMediator extends NumericMediator {

        private IRankingObserver other;
        private boolean          convertScale;

        protected RankingMediator(IRankingObserver other, IMonitor monitor) {
            super(monitor);
            this.other = other;
            this.convertScale = scale != null && other.getRankingScale() != null
                    && !scale.equals(other.getRankingScale());
        }

        @Override
        public Object mediate(Object object) throws KlabException {

            Number val = getValueAsNumber(object, other);
            if (!Double.isNaN(val.doubleValue()) && convertScale) {
                val = rankingScale.convert(val, other.getRankingScale());
            }
            return super.mediate(val);
        }

        @Override
        public String getLabel() {
            return (rankingScale == null || other.getRankingScale() == null
                    || rankingScale.equals(other.getRankingScale()) ? ""
                            : "rescale " + other.getRankingScale() + " to " + rankingScale)
                    +
                    (discretization == null ? ""
                            : ((rankingScale == null || other.getRankingScale() == null
                                    || rankingScale.equals(other.getRankingScale()) ? "" : "->")
                                    + "discretize"));
        }
    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return false;
    }

    public KIMRankingObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        if (statement.getFrom() != null || statement.getTo() != null) {
            rankingScale = new RankingScale(KIM.processNumber(statement.getFrom()), KIM
                    .processNumber(statement.getTo()));
        }

        if (mediated != null) {
            /*
             * TODO must be another ranking and both must have a scale
             */
        }

        this.integer = statement.isInteger();

        if (rankingScale != null) {
            minimumValue = rankingScale.getRange().getFirst().doubleValue();
            maximumValue = rankingScale.getRange().getSecond().doubleValue();
        }

        validate(context);
    }

    public KIMRankingObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof KIMRankingObserver)) {
            throw new KlabRuntimeException("cannot initialize a ranking observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.rankingScale = ((KIMRankingObserver) observer).rankingScale;
        if (rankingScale != null) {
            minimumValue = rankingScale.getRange().getFirst().doubleValue();
            maximumValue = rankingScale.getRange().getSecond().doubleValue();
        }
        this.integer = ((KIMRankingObserver) observer).isInteger();
    }

    /**
     * Only for the deserializer
     */
    public KIMRankingObserver() {
    }

    @Override
    protected IConcept getObservedType(KIMScope context, IConcept knowledge) {
        if (!NS.isQuality(knowledge)) {
            context.error("rankings must observe qualities", this.getFirstLineNumber());
        }
        return super.getObservedType(context, knowledge);
    }

    @Override
    public IObserver copy() {
        return new KIMRankingObserver(this);
    }

    KIMRankingObserver(IConcept observable) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
        // TODO observable may have data properties for range
    }

    KIMRankingObserver(IConcept observable, List<Integer> scale) {
        this(observable);
    }

    @Override
    public IRankingScale getRankingScale() {
        return rankingScale;
    }

    @Override
    public IObserver getMediatedObserver() {
        return mediated;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.RANKING);
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException {

        if (!(observer instanceof IRankingObserver)) {
            throw new KlabValidationException("ranking observers can only mediate other ranking observers");
        }

        IRankingObserver other = (IRankingObserver) observer;

        if (discretization != null) {
            if (((IRankingObserver) observer).getDiscretization() != null) {
                monitor.warn(getObservable().getType()
                        + ": discretized rankings should not mediate other discretized rankings: value will be undiscretized, then discretized again");
            }
            return new RankingMediator((IRankingObserver) observer, monitor);
        }

        if ((other.getRankingScale() == null && rankingScale == null) || (rankingScale != null
                && other.getRankingScale() != null && rankingScale.equals(other.getRankingScale()))) {
            return null;
        }

        return new RankingMediator(other, monitor);
    }

    @Override
    public String toString() {
        return "RNK/" + getObservable();
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize an Observer from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        if (bean.getRankingScale() != null) {
            rankingScale = KLAB.MFACTORY.adapt(bean.getRankingScale(), RankingScale.class);
        }
        this.integer = bean.isInteger();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize an Observer to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        if (rankingScale != null) {
            ret.setRankingScale(KLAB.MFACTORY
                    .adapt(rankingScale, org.integratedmodelling.common.beans.RankingScale.class));
        }

        ret.setInteger(integer);

        return (T) ret;
    }

    @Override
    public boolean isInteger() {
        return integer;
    }

    @Override
    public String getDefinition() {
        // TODO Auto-generated method stub
        String ret = "rank "
                + Observables.getDeclaration(getObservable().getType(), getNamespace().getProject());
        if (rankingScale != null) {
            ret += " " + ((RankingScale) rankingScale).asText();
        }
        return ret;
    }

    @Override
    protected void validate(KIMScope context) {
        if (this.getObservable() != null && this.getObservable().getType() != null) {
            if (this.getObservable().getType().is(KLAB.c(NS.CORE_PHYSICAL_PROPERTY))) {
                context.warning("a physical property should be measured, not ranked. This will not match the measured observable.", getFirstLineNumber());
            }
        }
        super.validate(context);
    }

    @Override
    public IValueMediator getValueMediator() {
        return rankingScale;
    }

}
