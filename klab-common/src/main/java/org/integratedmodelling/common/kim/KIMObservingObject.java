/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.integratedmodelling.api.data.ITable;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IAction.Trigger;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.collections.Triple;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.OWL;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Roles;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.kim.kim.Action;
import org.integratedmodelling.kim.kim.Annotation;
import org.integratedmodelling.kim.kim.Contextualization;
import org.integratedmodelling.kim.kim.Dependency;
import org.integratedmodelling.kim.kim.FunctionOrID;
import org.integratedmodelling.kim.kim.ObservationGenerator;
import org.integratedmodelling.kim.kim.ObservationGeneratorConditional;
import org.integratedmodelling.kim.kim.ObservationGeneratorSwitch;

public abstract class KIMObservingObject extends KIMModelObject implements IObservingObject {

    IFunctionCall                 contextualizerCall;
    protected ITable              lookupTable;

    protected List<IFunctionCall> scaleGenerators = new ArrayList<>();
    protected List<IAction>       actions         = new ArrayList<>();
    protected List<IDependency>   dependencies    = new ArrayList<>();

    /*
     * created from scale generators on demand.
     */
    IScale                        scale           = null;

    public KIMObservingObject(KIMScope context, EObject statement, IModelObject parent,
            List<Annotation> annotations) {
        super(context, statement, parent, annotations);
        // observers and models start existing after the worldview has been
        // read, so
        // ensuring
        // worldview synch before the first is created is the best strategy.
        NS.synchronize();
    }

    public KIMObservingObject(KIMScope context, EObject statement, IModelObject parent) {
        super(context, statement, parent);
        // observers and models start existing after the worldview has been
        // read, so
        // ensuring
        // worldview synch before the first is created is the best strategy.
        NS.synchronize();
    }

    protected KIMObservingObject(KIMObserver observer) {
        super(observer);
    }

    public KIMObservingObject(INamespace namespace, String id) {
        super(namespace, id);
        this.id = id;
    }

    /**
     * Called at end to check for common issues
     * 
     * @param context
     */
    protected void validate(KIMScope context) {

        /*
         * check for input = output thing, which people seem to love doing.
         */
        if (this.getObservable() != null && this.getObservable().getType() != null) {
            if (hasDependencyFor(this.getObservable().getType())) {
                context.error("observed concept is also a dependency: "
                        + this.getObservable().getType(), getFirstLineNumber());
            }
        }

    }

    protected boolean hasDependencyFor(IConcept observable) {
        for (IDependency d : getDependencies()) {
            if (d.getObservable().getType().equals(observable)) {
                return true;
            }
        }
        return false;
    }

    protected void defineDependencies(KIMScope context, EList<Dependency> dependencies) {

        if (getObservable() == null || getObservable().getType() == null) {
            return;
        }

        IConcept refContext = (NS.isCountable(getObservable())
                && !(this instanceof KIMModel && ((KIMModel) this).isInstantiator))
                        ? getObservable().getType()
                        : Observables.getContextType(getObservable().getType());

        for (Dependency dependency : dependencies) {

            KIMDependency d = new KIMDependency(context.get(KIMScope.Type.DEPENDENCY), dependency, this);
            if (!d.isNothing()) {
                this.dependencies.add(d);
            }

            IObservableSemantics ko = d.getObservable();

            if (ko != null && refContext != null && !NS.isDirect(ko)) {

                /*
                 * check compatible context
                 */
                IConcept ctx = Observables.getContextType(ko.getType());
                if (ctx != null) {

                    if (!Observables.isCompatible(refContext, ctx, Observables.ACCEPT_REALM_DIFFERENCES)) {

                        context.error("observable " + NS.getDisplayName(ko.getType())
                                + " has incompatible context ("
                                + NS.getDisplayName(ctx) + ") with the overall model context ("
                                + NS.getDisplayName(refContext) + ")", this.getFirstLineNumber());
                    }
                } /*
                   * else { try { IConcept fixedObs = Observables.declareObservable(ko
                   * .getType(), null, refContext, null); ((ObservableSemantics)
                   * ko).setType(fixedObs); } catch (KlabValidationException e) {
                   * context.error(e.getMessage(), this.getFirstLineNumber()); } }
                   */
            }
        }

        /*
         * ensure that no naming conflicts exist and name any unnamed dependencies.
         */
        Set<String> depNames = new HashSet<>();
        for (IDependency dependency : this.dependencies) {
            if (dependency.getFormalName() == null) {
                String name = ((KIMDependency) dependency).name();
                if (name != null) {
                    if (depNames.contains(name)) {
                        context.error("dependency default name '" + name
                                + " cannot be used as it's ambiguous; please name this dependency explicitly", dependency
                                        .getFirstLineNumber());
                    } else if (!context.hasErrors()) {
                        // context.info("dependency was named " + name
                        // + " from its observable; use 'named' to explicitly
                        // set the
                        // name", dependency
                        // .getFirstLineNumber());
                    }
                    depNames.add(name);
                }
            } else {
                if (depNames.contains(dependency.getFormalName())) {
                    context.error("dependency cannot be named '" + dependency.getFormalName()
                            + "': name is ambiguous", dependency.getFirstLineNumber());
                }
                depNames.add(dependency.getFormalName());
            }
        }
    }

    static IObserver defineObserver(KIMScope context, KIMModel model, ObservationGeneratorSwitch statement) {

        if (statement.getMediated() != null) {
            if (statement.getMediated().size() == 1 && statement.getMediated().get(0).getWhen() == null) {
                return defineObserver(context.get(KIMScope.Type.OBSERVER), model, statement.getMediated()
                        .get(0).getObservable());
            } else {
                List<Pair<IObserver, IExpression>> observers = new ArrayList<>();
                for (ObservationGeneratorConditional oc : statement.getMediated()) {
                    IObserver cobs = defineObserver(context.get(KIMScope.Type.OBSERVER), model, oc
                            .getObservable());

                    if (cobs.getObservable() != null && !(NS.isQuality(cobs.getObservable()))) {
                        context.error("the observable in any observer must be a quality", lineNumber(statement));
                    }

                    IExpression cexp = new KIMExpression(context
                            .get(KIMScope.Type.OBSERVER_CONDITIONAL_EXPRESSION), model, cobs, oc.getWhen());
                    observers.add(new Pair<>(cobs, cexp));
                }
                return new KIMConditional(context
                        .get(KIMScope.Type.OBSERVER_CONDITIONAL), model, observers, statement);
            }
        }
        return null;
    }

    static IObserver defineObserver(KIMScope context, KIMModel model, ObservationGenerator statement) {

        if (statement.getClassification() != null) {
            return new KIMClassificationObserver(context, model, statement.getClassification());
        } else if (statement.getCount() != null) {
            return new KIMCountObserver(context, model, statement.getCount());
        } else if (statement.getDistance() != null) {
            return new KIMDistanceObserver(context, model, statement.getDistance());
        } else if (statement.getMeasurement() != null) {
            return new KIMMeasurementObserver(context, model, statement.getMeasurement());
        } else if (statement.getPresence() != null) {
            return new KIMPresenceObserver(context, model, statement.getPresence());
        } else if (statement.getProbability() != null) {
            return new KIMProbabilityObserver(context, model, statement.getProbability());
        } else if (statement.getProportion() != null) {
            return new KIMProportionObserver(context, model, statement.getProportion());
        } else if (statement.getRanking() != null) {
            return new KIMRankingObserver(context, model, statement.getRanking());
        } else if (statement.getRatio() != null) {
            return new KIMRatioObserver(context, model, statement.getRatio());
        } else if (statement.getUncertainty() != null) {
            return new KIMUncertaintyObserver(context, model, statement.getUncertainty());
        } else if (statement.getValuation() != null) {
            return new KIMValuationObserver(context, model, statement.getValuation());
        }
        return null;
    }

    static Pair<List<IFunctionCall>, List<IAction>> defineContextActions(KIMScope context, KIMModelObject observer, List<Contextualization> statements) {

        List<IFunctionCall> functions = new ArrayList<>();
        List<IAction> actions = new ArrayList<>();

        for (Contextualization c : statements) {

            Set<IConcept> domains = new HashSet<>();

            IAction.Trigger trigger = null;
            if (c.isInitialization()) {
                trigger = Trigger.DEFINITION;
            } else if (c.isResolution()) {
                trigger = Trigger.RESOLUTION;
            } else if (c.isTermination()) {
                trigger = Trigger.TERMINATION;
            } else if (c.isInstantiation()) {
                trigger = Trigger.INSTANTIATION;
            } else if (c.isStateInitialization()) {
                trigger = Trigger.STATE_INITIALIZATION;
            }
            ;
            IConcept onEvent = null;
            if (c.getEvent() != null) {
                KIMKnowledge k = new KIMKnowledge(context.get(KIMScope.Type.EVENT_CONTEXTUALIZATION), c
                        .getEvent());
                if (k == null || !k.isConcept() || k.isNothing() || !NS.isEvent(k.getConcept())) {
                    context.error("only known event concepts can be used to trigger actions", lineNumber(c));
                }
                domains.add(k.getConcept());
                trigger = Trigger.EVENT;
            }

            boolean ok = true;

            if (c.getDomain() != null) {

                for (FunctionOrID fid : c.getDomain()) {

                    IFunctionCall function = null;
                    if (fid.getFunctionId() != null) {
                        function = new KIMFunctionCall(context.get(KIMScope.Type.COVERAGE_FUNCTION_CALL), fid
                                .getFunctionId());
                    } else if (fid.getFunction() != null) {
                        function = new KIMFunctionCall(context.get(KIMScope.Type.COVERAGE_FUNCTION_CALL), fid
                                .getFunction());
                    }

                    IConcept domain = validateFunctionCall(context, function, KLAB.c(NS.EXTENT));
                    if (domain != null) {
                        domains.add(domain);
                        functions.add(function);
                        if (domain.is(KLAB.c(NS.TIME_DOMAIN))) {
                            trigger = Trigger.TRANSITION;
                        }
                    } else {
                        ok = false;
                    }

                    /**
                     * Function defines the scale even if no actions are specified.
                     */
                    // if (function != null && observer instanceof
                    // KIMObservingObject) {
                    // ((KIMObservingObject)
                    // observer).scaleGenerators.add(function);
                    // }
                }
            }

            if (trigger == null) {
                if (c.getDomain() == null) {
                    context.warning("cannot determine trigger for actions", lineNumber(statements.iterator()
                            .next()));
                }
            } else {

                for (Action action : c.getActions()) {
                    if (ok) {
                        KIMAction a = new KIMAction(context
                                .get(KIMScope.Type.CONTEXTUALIZATION_ACTION), observer, domains, action, trigger, c);
                        actions.add(a);
                    } else {
                        context.warning("cannot determine domain: ignoring action", lineNumber(action));
                    }
                }
            }
        }

        return new Pair<>(functions, actions);

    }

    @Override
    public List<IDependency> getDependencies() {
        return dependencies;
    }

    @Override
    public boolean hasActionsFor(IConcept observable, IConcept domainConcept) {

        for (IAction a : actions) {
            for (IConcept d : a.getDomain()) {
                if (d.is(domainConcept)) {

                    /*
                     * TODO check observable
                     */
                    return true;
                }
            }
        }

        return false;
    }

    public boolean hasActionsFor(IAction.Trigger trigger) {

        for (IAction a : actions) {
            if (a.getTrigger() == trigger) {
                return true;
            }
        }

        return false;
    }

    public boolean hasActionsFor(IAction.Trigger trigger, IAction.Type type) {

        for (IAction a : actions) {
            if (a.getTrigger() == trigger && a.getType() == type) {
                return true;
            }
        }

        return false;
    }

    
    @Override
    public IScale getCoverage(IMonitor monitor) {
        try {
            return makeScale(monitor);
        } catch (Exception e) {
            monitor.error(e.getMessage());
            throw new KlabRuntimeException(e);
        }
    }

    protected IScale makeScale(IMonitor monitor) throws KlabException {
        if (scale == null) {
            scale = KLAB.MFACTORY.createScale(scaleGenerators, monitor);
        }
        return scale;
    }

    @Override
    public List<IAction> getActions() {
        return actions;
    }

    protected void name(String id) {
        this.id = id;
    }

    /**
     * Make the model the (quality) observable implies.
     * 
     * @param context
     * @param observable
     * @param lineNumber
     * @return
     */
    IModel makeObservableModel(KIMScope context, IKnowledge observable, int lineNumber, boolean complain) {

        /**
         * make a model instead - warn about default observer only if overall model is not
         * abstract.
         */
        IObserver observer = ((ModelFactory) KLAB.MFACTORY).getObserverFor((IConcept) observable);
        if (observer == null) {
            context.error("bare quality dependency cannot be assigned a default observer "
                    + observable, lineNumber);
        } else if (this instanceof IModel && !observable.isAbstract() && !((KIMModel) this).isAbstract()) {
            // TODO info/warn of default interpretation - unsure of how to say it.
        }

        KIMModel ret = null;
        if (observer != null) {
            ((KIMModelObject) observer).setLineNumbers(lineNumber, lineNumber);
            ret = new KIMModel((KIMObserver) observer);
            ret.setLineNumbers(lineNumber, lineNumber);
        }

        return ret;
    }

    /**
     * Lookup a model in the namespace
     * 
     * @param context
     * @param c
     * @param lineNumber
     * @return
     */
    static IModel findObservableModel(KIMScope context, IKnowledge c, int lineNumber, boolean complain) {

        IModel ret = null;

        /**
         * TODO make a model instead - warn about default observer only if not abstract.
         */

        /*
         * we should find one and only one model in the namespace defined before this to
         * model this observable.
         */
        ArrayList<IModel> candidates = new ArrayList<>();
        for (IModelObject o : context.getNamespace().getModelObjects()) {
            if (!o.isInactive() && o instanceof IModel) {

                if (o.getErrorCount() == 0 && ((IModel) o).getObservable().is(c))
                    candidates.add((IModel) o);
            }
        }

        if (candidates.size() == 0 && complain) {
            context.error("bare quality dependencies need a model to interpret them in the namespace; please define one for "
                    + c + "  before this line", lineNumber);
        } else if (candidates.size() > 1) {
            context.error("found more than one model in the namespace to interpret " + c
                    + ": please indicate one explicitly or define an observer here", lineNumber);

        } else if (candidates.size() > 0) {
            ret = candidates.get(0);
        }

        return ret;
    }

    public Class<?> getContextualizerClass() {
        if (contextualizerCall != null) {
            return KLAB.MFACTORY.getContextualizerClass(contextualizerCall);
        }
        return null;
    }

    @Override
    public IContextualizer getContextualizer(IResolutionScope scope, IProvenance.Artifact provenance, IMonitor monitor)
            throws KlabException {

        IContextualizer ret = null;
        if (hasContextualizer()) {
            ret = KLAB.MFACTORY.getContextualizer(contextualizerCall, this, scope, provenance, monitor);
        }
        return ret;
    }

    public boolean hasContextualizer() {
        return contextualizerCall != null;
    }

    private IModel asModel() {
        return this instanceof IModel ? (IModel) this : ((IObserver) this).getModel();
    }

    /**
     * Return all inferred dependencies. Called only at resolution. Return the concept we
     * need to observe and whether the dependency should be optional (true) or mandatory.
     * For now only addresses dependencies determined by roles stated in the model, but
     * will also check those coming from other fundamental restrictions. Will NOT return
     * dependencies that are already represented in the observer/model.
     * 
     * @param context
     * @throws KlabException
     */
    public List<Pair<IConcept, Boolean>> getInferredDependencies(IMonitor monitor, IResolutionScope context)
            throws KlabException {

        List<Pair<IConcept, Boolean>> ret = new ArrayList<>();

        /*
         * roles are contextual to the ACTUAL observable being resolved, which may not be
         * the same as the model (as abstract models can be picked to resolve it).
         */
        Collection<IConcept> roles = Roles.getRoles(context.getObservableBeingResolved().getType());

        /*
         * add source and destination if it's a relationship and we're instantiating
         */
        if (NS.isRelationship(context.getObservableBeingResolved()) && this instanceof IModel
                && ((IModel) this).isInstantiator()) {
            for (IConcept role : roles) {
                IConcept source = Observables.getApplicableSource(role);
                IConcept destination = Observables.getApplicableDestination(role);
                if (source != null) {
                    addRoleImpliedDependency(role, source, ret);
                }
                if (destination != null) {
                    addRoleImpliedDependency(role, destination, ret);
                }
            }
        }

        /*
         * Otherwise, dependencies are obviously added only to explanation models
         */
        if (!(this instanceof IModel && ((IModel) this).isInstantiator())) {
            for (IConcept role : roles) {
                for (IConcept impliedRole : Roles.getImpliedObservableRoles(role)) {
                    addRoleImpliedDependency(role, impliedRole, ret);
                }
            }
        }

        return ret;
    }

    private void addRoleImpliedDependency(IConcept implyingRole, IConcept impliedRole, List<Pair<IConcept, Boolean>> ret)
            throws KlabException {

        /*
         * Concrete ones generate a dependency on their applicable observable with the
         * role.
         */
        if (!impliedRole.isAbstract() && !includesRole(impliedRole)) {

            if (OWL.isRestrictionDenied(getObservable().getType(), impliedRole)) {
                return;
            }

            IConcept contextType = Observables.getContextType(getObservable().getType());
            IConcept toobs = Roles.getObservableWithRole(impliedRole, contextType);

            /*
             * TODO check - only adding the dep if it's DIRECT or a non-abstract
             * non-countable. Definitely shouldn't add an abstract state dep, but who
             * knows.
             */
            if (toobs != null && (NS.isDirect(toobs) || !toobs.isAbstract())) {
                boolean optional = OWL.isRestrictionOptional(implyingRole, impliedRole);
                ret.add(new Pair<>(toobs, optional));
            }
        }
    }

    protected boolean includesRole(IConcept observableRole) {

        for (IDependency d : getDependencies()) {
            if ((((KIMDependency) d).interpretAs != null
                    && ((KIMDependency) d).interpretAs.is(observableRole))
                    || NS.contains(observableRole, Roles.getRoles(d))) {
                return true;
            }
        }

        for (IObservableSemantics obs : this instanceof IModel ? ((IModel) this).getObservables()
                : Collections.singletonList(getObservable())) {
            /*
             * an abstract observable role fulfills the requested role if the requested
             * role specializes the provided (adding it means that the model will check the
             * concrete role and provide it as requested). For a concrete one the model
             * must have a role that specializes the requested.
             */
            if (obs.getType().isAbstract()) {
                for (IConcept abrole : Roles.getRoles(obs.getType())) {
                    if (observableRole.is(abrole)) {
                        return true;
                    }
                }
            } else if (NS.contains(observableRole, Roles.getRoles(obs.getType()))) {
                return true;
            }
        }

        return false;
    }

    /**
     * This (non-API for the time being) will pair the stated dependencies with any other
     * coming from the semantics. Some observers may have additional requirements that
     * will also end up here through getInferredDependencies().
     * 
     * @return all the dependencies, resolved
     * @throws KlabException
     */
    public List<IDependency> getAllDependencies(IMonitor monitor, IResolutionScope context)
            throws KlabException {

        /*
         * a model created as part of a dependency must NOT be treated this well.
         */
        if (!this.isFirstClass()) {
            return dependencies;
        }

        /**
         * original minus those that apply to a scale we don't have.
         */
        List<IDependency> ret = selectApplicableDependencies(dependencies, context);
        
        for (Pair<IConcept, Boolean> c : getInferredDependencies(monitor, context)) {
            // if (!haveDep(ret, c)) {
            ObservableSemantics obs = new ObservableSemantics(c.getFirst());
            if (NS.isCountable(c.getFirst())) {
                obs.setInstantiator(true);
            }
            ret.add(new KIMDependency(obs, obs.getFormalName(), c.getSecond(), context));
            // }
        }

        /**
         * Add whatever is specified by the semantics and is not already in the stated
         * dependencies. Only for direct observations - indirect are "data" and they
         * naturally can't have anything playing a role in their semantics, only models
         * can define that.
         */
        if (NS.isDirect(getObservable())) {

            for (IConcept c : Roles.getRoles(this.getObservable(), context)) {
                if (!haveDep(ret, c)) {
                    String fname = CamelCase.toLowerCase(c.getLocalName(), '-');
                    // TODO depends on the restriction and the property!
                    boolean isOptional = true;
                    IDependency ndep = new KIMDependency(new ObservableSemantics(c, NS
                            .getObservationTypeFor(c), fname), fname, null, isOptional, null, context);
                    ret.add(ndep);
                }
            }

            for (Triple<IConcept, IConcept, IProperty> sd : NS.getSemanticDependencies(getObservable())) {
                IConcept dep = sd.getFirst();
                String fname = CamelCase.toLowerCase(dep.getLocalName(), '-');
                if (!haveDep(ret, dep)) {
                    // TODO depends on the restriction and the property!
                    boolean isOptional = true;
                    IDependency ndep = new KIMDependency(new ObservableSemantics(dep, NS
                            .getObservationTypeFor(dep), fname), fname, sd
                                    .getThird(), isOptional, null, context);
                    ret.add(ndep);
                }
            }
        }

        return ret;
    }

    private List<IDependency> selectApplicableDependencies(List<IDependency> dependencies, IResolutionScope context) {
        List<IDependency> ret = new ArrayList<>();
        for (IDependency dep : dependencies) {
            if (!((KIMDependency)dep).resolutionContext.isEmpty()) {
                boolean ok = true;
                for (IConcept ext : ((KIMDependency)dep).resolutionContext) {
                    if (context.getScale().getExtent(ext) == null) {
                        ok = false;
                        break;
                    }
                }
                if (!ok) {
                    continue;
                }
            }
            ret.add(dep);
        }
        return ret;
    }

    private boolean haveDep(List<IDependency> ret, IKnowledge c) {
        for (IDependency d : ret) {
            if (d.getType().is(c)) {
                return true;
            }
        }
        return false;
    }

}
