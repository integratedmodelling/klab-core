/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IMediatingObserver;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.runtime.IActiveObserver;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.model.runtime.AbstractMediator;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Types;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMClassificationObserver extends KIMObserver
        implements IMediatingObserver, IClassifyingObserver, NetworkSerializable,
        NetworkDeserializable {

    IClassification classification;
    boolean         isDiscretization;
    String          metadataProperty;
    String          authority;

    public class ClassificationMediator extends AbstractMediator {

        int                     convertToTrait         = -1;
        int                     maxLevel               = -1;
        IClassification         mediatedClassification = null;
        IKnowledge              baseClass              = null;
        IConcept                baseTrait              = null;
        Map<IConcept, IConcept> traitCache             = new HashMap<>();
        Map<IConcept, IConcept> levelCache             = new HashMap<>();

        protected ClassificationMediator(IObserver observer, IMonitor monitor)
                throws KlabValidationException {

            super(monitor);

            /*
             * get the classification from the main observer
             */
            mediatedClassification = classification;

            if (mediatedClassification != null) {
                ((org.integratedmodelling.common.classification.Classification) classification)
                        .reset();
            } else {
                /*
                 * EHM FIXME not sure what this does.
                 */
                mediatedClassification = classification = new Classification(getObservable()
                        .getType());
            }

            if (!observer.getType().equals(getType())) {

                if (NS.isClass(observer)) {

                    // List<IObservableSemantics> etypes = NS
                    // .getExposedTraitsForType((IConcept) observer.getType());
                    // for (int i = 0; i < etypes.size(); i++) {
                    // if (etypes.get(i).getType().equals(getType())) {
                    // convertToTrait = i;
                    // baseClass = observer.getType();
                    // baseTrait = etypes.get(i).getTypeAsConcept();
                    // break;
                    // }
                    // }
                }
                // else {
                // throw new KlabValidationException("wrong operation in class mediation:
                // conversion between incompatible types "
                // + observer.getType() + " and " + getType());
                // }
            }

            // if (observer.getObservable().getDetailLevel() !=
            // getObservable().getDetailLevel()
            // && getObservable().getDetailLevel() > 0) {
            // maxLevel = getObservable().getDetailLevel();
            // }

        }

        @Override
        public Object mediate(Object object) {

            if (object == null) {
                return null;
            }

            if (object instanceof String) {
                if (authority != null) {

                } else if (classification == null && mediatedClassification == null) {
                    return Knowledge.parse((String) object);
                }
            }

            // Object ret = null;
            //
            // if (object instanceof IConcept) {
            // // if (convertToTrait >= 0) {
            // // ret = traitCache.get(object);
            // // if (ret == null) {
            // // List<IObservableSemantics> tr = NS
            // // .getAdoptedTraits((IConcept) baseClass, (IConcept) object);
            // // if (tr != null) {
            // // ret = tr.get(convertToTrait).getType();
            // // traitCache.put((IConcept) object, (IConcept) ret);
            // // }
            // // }
            // // }
            // //
            // // if (ret != null && maxLevel > 0) {
            // // IConcept r = levelCache.get(ret);
            // // if (r == null) {
            // // IConcept zc = (IConcept) ret;
            // // ret = NS.getParentAtLevel(baseTrait, (IConcept) ret, maxLevel);
            // // if (ret != null) {
            // // levelCache.put(zc, (IConcept) ret);
            // // }
            // // }
            // // }
            // }
            //
            // if (ret != null) {
            // return ret;
            // }
            // FIXME something screwy left in this logics - should probably be just pass
            // object through in the second instance.
            return mediatedClassification != null
                    ? mediatedClassification.classify(object)
                    : (object instanceof IConcept ? object
                            : classification.classify(object));
        }

        @Override
        public String getLabel() {
            return "classify " + NS.getDisplayName(classification.getConceptSpace());
        }
    }

    public KIMClassificationObserver(KIMScope context, KIMModel model,
            Observer statement) {

        super(context, model, statement);

        isDiscretization = statement.isDiscretizer();
        metadataProperty = statement.getMetadataProperty();
        authority = statement.getAuthority();
        
        if (observable != null) {

            // ((ObservableSemantics) observable)
            // .setType(this.getObservedType(context, this.observable.getType()));

            if (metadataProperty != null) {
                try {
                    classification = Types
                            .createClassificationFromMetadata(getObservable()
                                    .getType(), metadataProperty);
                } catch (KlabValidationException e) {
                    context.error(e.getMessage(), lineNumber(statement));
                }
            } else if (statement.getClassification() != null) {
                classification = new KIMClassification(context
                        .get(KIMScope.Type.CLASSIFICATION), statement
                                .getClassification(), this.observable);
            }
        }

        if (observable != null && NS.isObject(observable.getType())) {
            context.error("cannot classify an object unless it's 'by' a trait", getFirstLineNumber());
        }
        
        validate(context);
    }

    public KIMClassificationObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IClassifyingObserver)) {
            throw new KlabRuntimeException("cannot initialize a classification observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.classification = ((IClassifyingObserver) observer).getClassification();
    }
    
    KIMClassificationObserver(IConcept observable) {
        // NO ERROR CHECKING - USED INTERNALLy
        observable = Observables.makeTypeFor(observable);
        this.observable = new KIMObservableSemantics(observable);
    }

    @Override
    public KIMClassificationObserver copy() {
        return new KIMClassificationObserver(this);
    }

    @Override
    public IClassification getClassification() {
        return classification;
    }

    @Override
    public IObserver getMediatedObserver() {
        return mediated;
    }

    @Override
    public IConcept getObservationType() {

        /*
         * TODO take the Classification type and restrict it to represent the particular
         * concept space or mediator we're handling.
         */
        return KLAB.c(NS.CLASSIFICATION);
    }

    @Override
    public IConcept getObservedType(KIMScope context, IConcept concept) {
        try {
            return Types.getClassificationType(concept);
        } catch (KlabValidationException e) {
            context.error(e.getMessage(), this.getFirstLineNumber());
        }
        return concept;
    }

    /**
     * Only for the deserializer
     */
    public KIMClassificationObserver() {
    }

    KIMClassificationObserver(IObservableSemantics observable) {

        if (!NS.isClass(observable)) {
            throw new KlabRuntimeException("cannot create a classification observer from non-class knowledge");
        }
        this.observable = observable;
        classification = new Classification(observable.getType());
    }

    public static String asString(IClassifyingObserver cl) {
        return "oCL" + "|" + Classification.asString(cl.getClassification());
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor)
            throws KlabException {

        /*
         * true mediation only required if the other is a classification and the
         * classifications exist and are different.
         */
        observer = getRepresentativeObserver(observer);
        IClassification otherClassification = observer instanceof KIMClassificationObserver
                ? ((IClassifyingObserver) observer).getClassification() : null;

        if (otherClassification != null && classification != null) {

            if (otherClassification.isIdentical(classification)) {
                return null;
            }

            if (observer.getType().equals(this.getType())) {
                // || observer.getObservable().getDetailLevel() !=
                // this.getObservable().getDetailLevel()
                // || observer.getObservable().getTraitDetailLevel() !=
                // getObservable().getTraitDetailLevel()) {
                return new ClassificationMediator(observer, monitor);
            }
        }

        /*
         * if the other is numeric, we definitely need to classify it.
         */
        return observer instanceof IClassifyingObserver ? null
                : new ClassificationMediator(observer, monitor);
    }

    @Override
    public boolean isCompatibleWith(IKnowledge type) {
        return NS.isClass(type);
    }

    @Override
    public String toString() {
        return "CLS/" + getObservable();
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        classification = KLAB.MFACTORY
                .adapt(bean.getClassification(), Classification.class);
        isDiscretization = bean.isDiscretization();
        metadataProperty = bean.getMetadataProperty();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass
                .isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        ret.setClassification(KLAB.MFACTORY
                .adapt(getClassification(), org.integratedmodelling.common.beans.Classification.class));
        ret.setMetadataProperty(metadataProperty);
        ret.setDiscretization(isDiscretization);
        return (T) ret;
    }

    @Override
    public IStateContextualizer getDataProcessor(IMonitor monitor)
            throws KlabValidationException {
        return new AbstractMediator(monitor) {

            @Override
            public Object mediate(Object value) throws KlabException {
                return classification == null ? value : classification.classify(value);
            }

            @Override
            public String getLabel() {
                return "classify";
            }

            @Override
            public String toString() {
                return "[classifier" + (classification == null ? "<empty>"
                        : ("[" + classification.getClassifiers().size()
                                + " classifiers]"))
                        + "]";
            }

        };
    }

    @Override
    public IActiveObserver notifyResolution(IActiveObserver observer) {

        /*
         * use the resolution's classifier. Happens with the 'classify' dependencies
         * without classification. FIXME this is specific of a resolution - should only
         * happen within dependencies, but it does modify the observer, which is supposed
         * to be immutable.
         */
        if (classification == null && observer instanceof IClassifyingObserver) {

            /*
             * FIXME! at the moment doing the right thing (commented out) breaks dataflow
             * compilation.
             */
            classification = ((IClassifyingObserver) observer).getClassification();
            // KIMClassificationObserver ret = copy();
            // ret.copyId(this);
            // ret.classification =
            // ((IClassifyingObserver)observer).getClassification();
            // return ret;
        }

        return this;
    }

    @Override
    public String getDefinition() {
        // TODO Auto-generated method stub
        return "classify " + Observables.getDeclaration(getObservable().getType(), getNamespace().getProject());
    }

	@Override
	public boolean isExtensive(IConcept extent) {
		return false;
	}
}
