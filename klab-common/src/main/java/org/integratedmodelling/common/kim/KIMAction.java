/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IDocumentation;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IActiveEvent;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.expr.GroovyExpression;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.kim.kim.Action;
import org.integratedmodelling.kim.kim.Contextualization;

public class KIMAction extends KIMModelObject implements IAction {

    // Used with event triggers to specify context and in EventBus cache for quick
    // matching.
    public static class EventContext {
        public IConcept event;                // the event type we subscribe to
        public IConcept within;             // explicit context for events
        public IConcept related;          // type of relations we consider when
                                          // subscribing
        public boolean  useRelated; // may be true with related == null

        public EventContext(EventContext ctx) {
            this.event = ctx.event;
            this.related = ctx.related;
            this.useRelated = ctx.useRelated;
            this.within = ctx.within;
        }

        EventContext() {
        }
    }

    Set<IConcept>       domain             = new HashSet<>();
    Type                type               = Type.SET;
    Trigger             trigger;
    IExpression         action;
    IExpression         condition;
    Object              whereTo;
    String              target;
    IModel              model;
    EventContext        eventContext;                         // not null if trigger ==
                                                              // Trigger.EVENT
    Map<String, Object> namespaceVariables = new HashMap<>();

    public KIMAction(KIMScope context, KIMModelObject observer, Set<IConcept> domain,
            Action statement, Trigger trigger, Contextualization contextualization) {

        super(context, statement, observer);
        this.domain.addAll(domain);
        this.trigger = trigger;
        this.model = observer instanceof IModel ? (IModel) observer : ((IObserver) observer).getModel();

        if (trigger == Trigger.EVENT) {
            this.eventContext = new EventContext();
            KIMKnowledge k = new KIMKnowledge(context
                    .get(KIMScope.Type.EVENT_CONTEXTUALIZATION), contextualization.getEvent());
            if (k.getConcept() == null || !NS.isEvent(k.getConcept())) {
                context.error("cannot add a trigger on anything but an event concept", lineNumber(contextualization));
            }
            this.eventContext.event = k.getConcept();
            if ((this.eventContext.useRelated = contextualization.isRelatedEventContext())) {
                if (contextualization.getEventContext() != null) {
                    k = new KIMKnowledge(context.get(KIMScope.Type.EVENT_CONTEXTUALIZATION), contextualization
                            .getEventContext());
                    if (k.getConcept() == null || !NS.isThing(k.getConcept())) {
                        context.error("any related concept must identify a subject", lineNumber(contextualization));
                    }
                    this.eventContext.related = k.getConcept();
                }
            } else if (contextualization.getEventContext() != null) {
                k = new KIMKnowledge(context.get(KIMScope.Type.EVENT_CONTEXTUALIZATION), contextualization
                        .getEventContext());
                if (k.getConcept() == null || !NS.isDirect(k.getConcept())) {
                    context.error("the context for the event must be a direct observable", lineNumber(contextualization));
                }
                this.eventContext.within = k.getConcept();
            }
        }

        if (statement == null) {
            type = Type.DO;
        } else {
            if (statement.isSet()) {
                type = Type.SET;
            } else if (statement.isChange()) {
                type = Type.CHANGE;
            } else if (statement.isDo()) {
                type = Type.DO;
            } else if (statement.isIntegrate()) {
                type = Type.INTEGRATE;
            } else if (statement.isMove()) {

                if (statement.isAway()) {
                    type = Type.DESTROY;
                    /*
                     * TODO add object disposal for language adapter.
                     */
                } else {
                    type = Type.MOVE;
                    whereTo = defineValue(context.get(KIMScope.Type.VALUE), statement
                            .getWhere());
                }
            }
        }

        if (statement == null || statement.getValue() != null) {

            String postfix = null;
            IDocumentation documentation = observer instanceof IModel ? ((IModel) observer).getDocumentation()
                    : ((IObserver) observer).getModel().getDocumentation();
            if (documentation != null) {
                IDocumentation.Template template = documentation.get(trigger);
                if (template != null) {
                    postfix = template.getActionCode();
                }
            }

            action = new KIMExpression(context, (statement == null ? null
                    : statement.getValue()), "wrap();\n", postfix);
        }

        if (statement != null) {
            if (statement.getCondition() != null) {
                condition = new KIMExpression(context, statement.getCondition(), "wrap();\n");
            }

            this.target = statement.getChanged();
        }
        
        namespaceVariables.putAll(context.getNamespace().getSymbolTable());
    }

    public EventContext getEventContext() {
        return this.eventContext;
    }

    @Override
    public Trigger getTrigger() {
        return this.trigger;
    }

    public KIMAction(KIMAction kimAction, Map<String, IObserver> inputs,
            Map<String, IObserver> outputs,
            IConcept... domain) {

        super(null);

        this.target = kimAction.target;
        this.type = kimAction.type;
        this.whereTo = kimAction.whereTo;
        this.trigger = kimAction.trigger;
        this.model = kimAction.model;
        if (kimAction.eventContext != null) {
            this.eventContext = new EventContext(kimAction.eventContext);
        }

        if (domain != null) {
            for (IConcept c : domain) {
                this.domain.add(c);
            }
        }

        try {
            this.condition = kimAction.condition == null ? null
                    : new GroovyExpression(kimAction.condition.toString()
                            .trim(), inputs, outputs, domain);
            this.action = kimAction.action == null ? null
                    : new GroovyExpression(kimAction.action.toString()
                            .trim(), inputs, outputs, domain);
        } catch (Throwable e) {
            // TODO add artifact
            throw new KlabRuntimeException(e.getMessage() + " [in " + model.getName() + "]");
        }
    }

    @Override
    public Set<IConcept> getDomain() {
        return domain;
    }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String getTargetStateId() {
        return target;
    }

    public void setTargetStateId(String name) {
        target = name;
    }

    @Override
    public IExpression getAction() {
        return action;
    }

    @Override
    public IExpression getCondition() {
        return condition;
    }

    /**
     * FIXME this must be const - return a new Action with the preprocessed expressions
     * for these inputs.
     * 
     * @param inputs
     * @param outputs
     */
    public void preprocess(Map<String, IObserver> inputs, HashMap<String, IObserver> outputs) {

        if (action instanceof GroovyExpression) {
            ((GroovyExpression) action).initialize(inputs, outputs);
        }
        if (condition instanceof GroovyExpression) {
            ((GroovyExpression) condition).initialize(inputs, outputs);
        }
    }

    @Override
    public String toString() {
        return "A/[" + action + "]" + (condition == null ? "" : ("/[" + condition + "]"));
    }

    /**
     * Called directly by KIMModel when actions are executed.
     * 
     * @param context
     * @param subject
     * @param transition
     * @param monitor
     * @return result of execution
     * @throws KlabException
     */
    public Object execute(IDirectObservation context, IObservation subject, ITransition transition, IProvenance.Artifact provenance, IMonitor monitor)
            throws KlabException {
        return execute(context, subject, transition, provenance, null, monitor);
    }

    /**
     * Called directly by KIMModel when actions are executed.
     * 
     * @param context
     * @param subject
     * @param transition
     * @param monitor
     * @return result of execution
     * @throws KlabException
     */
    public Object execute(IDirectObservation context, IObservation subject, ITransition transition, IProvenance.Artifact provenance, IActiveEvent event, IMonitor monitor)
            throws KlabException {

        /*
         * prepare environment. TODO the rest
         */
        Map<String, Object> args = new HashMap<>();

        args.putAll(namespaceVariables);

        /*
         * system parameters - including task and session
         */
        args.put("_monitor", monitor);
        args.put("_provenance", provenance);

        /*
         * user variables
         */
        args.put("_context", context);
        args.put("_self", subject);
        args.put("_model", this.model);

        if (event != null) {
            args.put("_event", event);
        }

        if (transition != null) {
            args.put("_transition", transition);
        }

        if (condition != null) {
            Object cret = condition.eval(args, monitor);
            if (!(cret instanceof Boolean && (Boolean) cret)) {
                return null;
            }
        }

        if (action != null) {
            return action.eval(args, monitor);
        }

        return null;
    }

    /*
     * Clone action and preprocess/compile for the passed observation
     */
    public IAction compileFor(IScale scale, IModel model) {

        List<IConcept> doms = new ArrayList<>();
        for (IExtent e : scale) {
            doms.add(e.getDomainConcept());
        }

        Map<String, IObserver> inputs = new HashMap<>();
        Map<String, IObserver> outputs = new HashMap<>();

        if (model != null) {
            for (int n = 1; n < model.getObservables().size(); n++) {
                if (model.getObservables().get(n).getObserver() != null) {
                    outputs.put(model.getObservables().get(n).getFormalName(), model
                            .getObservables().get(n)
                            .getObserver());
                }
            }
            for (IDependency d : model.getDependencies()) {
                if (d.getObservable().getObserver() != null) {
                    inputs.put(d.getFormalName(), d.getObservable().getObserver());
                }
            }
        }

        KIMAction ret = new KIMAction(this, inputs, outputs, doms
                .toArray(new IConcept[doms.size()]));
        return ret;
    }

    @Override
    public Collection<IConcept> getTriggeringEvents() {
        Set<IConcept> ret = new HashSet<>();
        if (eventContext != null) {
            ret.add(eventContext.event);
        }
        return ret;
    }

    public boolean isTemporalHandler() {
        return domain.contains(KLAB.c(NS.TIME_DOMAIN));
    }

}
