/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IProportionObserver;
import org.integratedmodelling.api.modelling.IRatioObserver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMProportionObserver extends KIMNumericObserver
        implements IProportionObserver, NetworkSerializable, NetworkDeserializable {

    boolean          isProportion;
    private IConcept originalConcept;

    public class ProportionMediator extends NumericMediator {

        private IProportionObserver other;

        protected ProportionMediator(IProportionObserver other, IMonitor monitor) {
            super(monitor);
            this.other = other;
        }

        @Override
        public Object mediate(Object object) throws KlabException {
            Number val = getValueAsNumber(object, other);

            if (other.isProportion() && !isProportion()) {
                val = val.doubleValue() * 100.0;
            } else if (isProportion() && !other.isProportion()) {
                val = val.doubleValue() / 100.0;
            }

            return super.mediate(val);
        }

        @Override
        public String getLabel() {
            String desc = "";
            if (other.isProportion() && !isProportion()) {
                desc = "convert to percentage ";
            } else if (isProportion() && !other.isProportion()) {
                desc = "convert to proportion ";
            }
            return desc + (discretization == null ? null : "discretize");
        }

    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return false;
    }

    public KIMProportionObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        // if (statement.getOther() != null) {
        // comparisonConcept = new
        // KIMKnowledge(context.get(KIMScope.Type.COMPARISON_OBSERVABLE), statement
        // .getOther(), this).getConcept();
        // }
        // if (observable != null) {
        // ((ObservableSemantics) observable)
        // .setType(this.getObservedType(context, this.observable.getType()));
        // }
        isProportion = statement.getType().equals("proportion");
        minimumValue = 0.0;
        maximumValue = isProportion ? 1.0 : 100.0;
    }

    public KIMProportionObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IProportionObserver)) {
            throw new KlabRuntimeException("cannot initialize a proportion observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.comparisonConcept = ((KIMProportionObserver) observer).comparisonConcept;
        minimumValue = 0.0;
        maximumValue = isProportion ? 1.0 : 100.0;
    }

    /**
     * Only for the deserializer
     */
    public KIMProportionObserver() {
    }

    @Override
    public IObserver copy() {
        return new KIMProportionObserver(this);
    }

    KIMProportionObserver(IConcept observable) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
    }

    KIMProportionObserver(IConcept observable, ISemantic comparison,
            boolean makePercentage) {
        observable = Observables.makeProportion(observable, comparison.getType());
        this.observable = new KIMObservableSemantics(observable);
    }

    @Override
    public IConcept getComparisonConcept() {
        return comparisonConcept;
    }

    @Override
    public boolean isIndirect() {
        return comparisonConcept != null;
    }

    @Override
    protected KIMScope.Type getDiscretizationContext() {
        return KIMScope.Type.PROPORTION_DISCRETIZATION;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.PROPORTION_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMScope context, IConcept concept) {

        if (!concept.is(KLAB.c(NS.CORE_PROPORTION))) {
            // save for later serialization
            this.originalConcept = concept;
        }

        IConcept ret = concept;
        if (comparisonConcept != null) {
            ret = Observables.makeProportion(concept, comparisonConcept);
            if (ret == null) {
                context.error(concept
                        + ": proportions must compare two related observables, differing by a trait or by inheritance.", getFirstLineNumber());
                return concept;
            }
        } else if (!concept.is(KLAB.c(NS.CORE_PROPORTION))) {
            context.error(concept
                    + ": the observable in this statement must be a proportion or percentage. Use the indirect form (with 'of') to annotate explicit comparisons.", getFirstLineNumber());
        }
        return ret;
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor)
            throws KlabException {

        if (!(observer instanceof IProportionObserver)) {
            throw new KlabValidationException("proportion observers can only mediate other proportion observers");
        }

        IProportionObserver other = (IProportionObserver) observer;

        if (discretization != null) {
            if (((IRatioObserver) observer).getDiscretization() != null) {
                monitor.warn(getObservable().getType()
                        + ": discretized ratios should not mediate other discretized ratios: value will be undiscretized, then discretized again");
            }
            return new ProportionMediator(other, monitor);
        }

        if ((other.isProportion() && isProportion())
                || (!other.isProportion() && !isProportion())) {
            return null;
        }

        return new ProportionMediator(other, monitor);
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        isIndirect = bean.isIndirect();
        isProportion = bean.isProportion();
        if (bean.getComparisonKnowledge() != null) {
            comparisonConcept = Knowledge.parse(bean.getComparisonKnowledge());
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass
                .isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        ret.setIndirect(isIndirect);
        ret.setProportion(isProportion);
        if (comparisonConcept != null) {
            ret.setComparisonKnowledge(((Knowledge) comparisonConcept).asText());
        }
        minimumValue = 0.0;
        maximumValue = isProportion ? 1.0 : 100.0;

        return (T) ret;
    }

    @Override
    public boolean isProportion() {
        return isProportion;
    }

    @Override
    public String toString() {
        return "PRC/" + getObservable();
    }

    @Override
    public String getDefinition() {
        return (isProportion ? "proportion " : "percentage ") + (isIndirect ? "of " : "")
                + (originalConcept == null
                        ? Observables.getDeclaration(getObservable().getType(), getNamespace().getProject())
                        : Observables.getDeclaration(originalConcept, getNamespace().getProject()))
                + (isIndirect
                        ? (" in "
                                + Observables.getDeclaration(comparisonConcept, getNamespace().getProject()))
                        : "");
    }

}
