/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.integratedmodelling.api.modelling.IClassifier;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.common.classification.Classification;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Classifier;

public class KIMClassification extends Classification implements ILanguageObject {

    int firstLine, lastLine;

    public KIMClassification(KIMScope context, org.integratedmodelling.kim.kim.Classification statement,
            IObservableSemantics observable) {

        firstLine = NodeModelUtils.getNode(statement).getStartLine();
        lastLine = NodeModelUtils.getNode(statement).getEndLine();

        setConceptSpace(observable.getType());

        for (Classifier cls : statement.getClassifiers()) {
            KIMKnowledge knowledge = new KIMKnowledge(context.get(KIMScope.Type.CLASSIFIER_CONCEPT), cls
                    .getDeclaration(), null);
            IClassifier classifier = cls.isOtherwise()
                    ? org.integratedmodelling.common.classification.Classifier.Universal()
                    : new KIMClassifier(context.get(KIMScope.Type.CLASSIFIER), cls
                            .getClassifier(), knowledge, cls.isNegated());

            if (!knowledge.isNothing()) {
                try {
                    addClassifier(classifier, knowledge.getConcept());
                } catch (KlabValidationException e) {
                    context.error(e.getMessage(), KIMLanguageObject.lineNumber(cls));
                }
            }
        }
        initialize();
    }

    @Override
    public int getFirstLineNumber() {
        return firstLine;
    }

    @Override
    public int getLastLineNumber() {
        return lastLine;
    }

}
