/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim.expr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.codehaus.groovy.control.CompilerConfiguration;
import org.codehaus.groovy.control.customizers.ImportCustomizer;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ICondition;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.errormanagement.CompileError;
import org.integratedmodelling.common.utils.Path;
import org.integratedmodelling.exceptions.KlabException;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.MissingPropertyException;
import groovy.lang.Script;

public class GroovyExpression extends CodeExpression implements ICondition {

    protected String              code;
    protected boolean             negated        = false;
    protected Object              object;
    protected IFunctionCall       functionCall;
    protected IModel              model;
    protected boolean             isNull         = false;
    protected boolean             isTrue         = false;
    private boolean               initialized    = false;
    private Set<String>           defineIfAbsent = new HashSet<>();

    Script                        script;
    Set<IConcept>                 domain;
    INamespace                    namespace;

    private List<CompileError>    errors         = new ArrayList<>();
    private CompilerConfiguration compiler       = new CompilerConfiguration();
    private GroovyShell           shell;
    private String                preprocessed   = null;

    /*
     * used by k.LAB to instantiate Groovy expressions. Will automatically add imports for
     * any KimImport class.
     */
    public GroovyExpression() {

        ImportCustomizer customizer = new ImportCustomizer();
        for (Class<?> cls : KLAB.MMANAGER.getKimImports()) {
            customizer.addImport(Path.getLast(cls.getCanonicalName(), '.'), cls.getCanonicalName());
        }
        compiler.addCompilationCustomizers(customizer);
    }

    public boolean hasErrors() {
        return errors.size() > 0;
    }

    public List<CompileError> getErrors() {
        return errors;
    }

    /*
     * used by Thinklab - when using the API use the String constructor. MUST be called in
     * all cases.
     */
    public void initialize(Map<String, IObserver> inputs, Map<String, IObserver> outputs) {
        compile(preprocess(code, inputs, outputs));
        initialized = true;
    }

    /**
     * Simple expression without context or receivers. NOT PREPROCESSED in the context
     * it's in.
     *
     * @param code
     */
    public GroovyExpression(String code, Map<String, IObserver> inputs,
            Map<String, IObserver> outputs,
            IConcept... domain) {

        ImportCustomizer customizer = new ImportCustomizer();
        for (Class<?> cls : KLAB.MMANAGER.getKimImports()) {
            customizer.addImport(Path.getLast(cls.getCanonicalName(), '.'), cls.getCanonicalName());
        }
        compiler.addCompilationCustomizers(customizer);

        this.code = (code.startsWith("wrap()") ?  code : ("wrap();\n\n" + code));
        this.domain = new HashSet<>();
        if (domain != null) {
            for (IConcept c : domain) {
                this.domain.add(c);
            }
        }
        this.compiler.setScriptBaseClass(getBaseClass());
        initialize(inputs, outputs);
    }

    /**
     * Preprocess with the dependencies of the passed model preset in symbol table.
     * 
     * @param code
     * @param model
     */
    public GroovyExpression(String code, IObservingObject model) {

        ImportCustomizer customizer = new ImportCustomizer();
        for (Class<?> cls : KLAB.MMANAGER.getKimImports()) {
            customizer.addImport(Path.getLast(cls.getCanonicalName(), '.'), cls.getCanonicalName());
        }
        compiler.addCompilationCustomizers(customizer);

        this.code = (code.startsWith("wrap()") ?  code : ("wrap();\n\n" + code));
        Map<String, IObserver> inputs = new HashMap<>();
        for (IDependency d : model.getDependencies()) {
            if (d.getObservable().getObserver() != null) {
                inputs.put(d.getFormalName(), d.getObservable().getObserver());
            }
        }
        initialize(inputs, null);
    }

    public GroovyExpression(String code, INamespace namespace, Set<IConcept> domain) {

        ImportCustomizer customizer = new ImportCustomizer();
        for (Class<?> cls : KLAB.MMANAGER.getKimImports()) {
            customizer.addImport(Path.getLast(cls.getCanonicalName(), '.'), cls.getCanonicalName());
        }
        compiler.addCompilationCustomizers(customizer);

        this.namespace = namespace;
        this.code = (code.startsWith("wrap()") ?  code : ("wrap();\n\n" + code));
        this.domain.addAll(domain);
    }

    public GroovyExpression(String code) {

        ImportCustomizer customizer = new ImportCustomizer();
        for (Class<?> cls : KLAB.MMANAGER.getKimImports()) {
            customizer.addImport(Path.getLast(cls.getCanonicalName(), '.'), cls.getCanonicalName());
        }
        compiler.addCompilationCustomizers(customizer);

        this.code = (code.startsWith("wrap()") ?  code : ("wrap();\n\n" + code));
        this.domain = new HashSet<>();
    }

    private void compile(String code) {
        this.compiler.setScriptBaseClass(getBaseClass());
        this.shell = new GroovyShell(this.getClass()
                .getClassLoader(), new Binding(), compiler);
        this.script = shell.parse(code);
    }

    protected String getBaseClass() {

        /*
         * choose proper class according to domains so that the appropriate functions are
         * supported.
         */
        // if (domain != null) {
        //
        // if (domain.contains(KLAB.c(NS.SPACE_DOMAIN)) &&
        // domain.contains(KLAB.c(NS.TIME_DOMAIN))) {
        // return "org.integratedmodelling.thinklab.actions.SpatioTemporalActionScript";
        // } else if (domain.contains(KLAB.c(NS.SPACE_DOMAIN))) {
        // return "org.integratedmodelling.thinklab.actions.SpatialActionScript";
        // } else if (domain.contains(KLAB.c(NS.TIME_DOMAIN))) {
        // return "org.integratedmodelling.thinklab.actions.TemporalActionScript";
        // }
        // }
        return "org.integratedmodelling.thinklab.actions.ActionScript";
    }

    @Override
    public Object eval(Map<String, Object> parameters, IMonitor monitor, IConcept... context)
            throws KlabException {

        if (isTrue) {
            return true;
        }

        if (isNull) {
            return null;
        }

        if (code != null) {

            if (!initialized) {
                initialize(new HashMap<>(), new HashMap<>());
            }

            try {
                setBindings(script.getBinding(), monitor, parameters);
                return script.run();
            } catch (MissingPropertyException e) {
                String property = e.getProperty();
                monitor.warn("variable " + property
                        + " undefined: check naming. Adding as no-data for future evaluations.");
                defineIfAbsent.add(property);
            } catch (Throwable t) {
                throw new KlabException(t);
            }
        } else if (object != null) {
            return object;
        } else if (functionCall != null) {
            return KLAB.MFACTORY.callFunction(functionCall, monitor, model, context);
        }
        return null;
    }

    private void setBindings(Binding binding, IMonitor monitor, Map<String, Object> parameters) {

        for (String key : parameters.keySet()) {
            binding.setVariable(key, parameters.get(key));
        }
        for (String v : defineIfAbsent) {
            if (!binding.hasVariable(v)) {
                binding.setVariable(v, null);
            }
        }

        binding.setVariable("_p", parameters);
        binding.setVariable("_ns", namespace);
        binding.setVariable("_mmanager", KLAB.MMANAGER);
        binding.setVariable("_engine", KLAB.ENGINE);
        binding.setVariable("_pmanager", KLAB.PMANAGER);
        binding.setVariable("_kmanager", KLAB.KM);
        binding.setVariable("_config", KLAB.CONFIG);
        binding.setVariable("_network", KLAB.ENGINE.getNetwork());
        binding.setVariable("_monitor", monitor);
    }

    private String preprocess(String code, Map<String, IObserver> inputs, Map<String, IObserver> outputs) {

        if (this.preprocessed != null) {
            return this.preprocessed;
        }

        Set<String> knownKeys = new HashSet<>();
        if (inputs != null) {
            knownKeys.addAll(inputs.keySet());
        }
        if (outputs != null) {
            knownKeys.addAll(outputs.keySet());
        }
        GroovyExpressionPreprocessor processor = new GroovyExpressionPreprocessor(namespace, knownKeys, domain);
        this.preprocessed = processor.process(code);
        this.errors.addAll(processor.getErrors());
        return this.preprocessed;
    }

    @Override
    public String toString() {
        return code;
    }

    @Override
    public void setNegated(boolean negate) {
        negated = negate;
    }

    @Override
    public boolean isNegated() {
        return negated;
    }

}
