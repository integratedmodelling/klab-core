/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.errormanagement.ICompileInfo;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.lang.IParsingScope;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.errormanagement.CompileError;
import org.integratedmodelling.common.errormanagement.CompileInfo;
import org.integratedmodelling.common.errormanagement.CompileWarning;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

public class KIMScope implements IParsingScope {

    private KIMScope              parent                           = null;
    Type                          type                             = Type.ROOT;

    /*
     * we save errors and warnings in two lists, one that is reset for each project and
     * another that remains through the whole root context.
     */
    private List<ICompileError>   contextErrors                    = new ArrayList<>();
    private List<ICompileWarning> contextWarnings                  = new ArrayList<>();
    private List<ICompileError>   projectErrors                    = new ArrayList<>();
    private List<ICompileWarning> projectWarnings                  = new ArrayList<>();
    private List<ICompileInfo>    info                             = new ArrayList<>();
    private IProject              project;
    private KIMNamespace          namespace;
    private KIMModelObject        object;
    private File                  resource;
    private String                namespaceId;
    private boolean               mustClose;
    private InputStream           input;
    private long                  timestamp;
    private IConcept              coreConcept;
    private Set<String>           beingParsed                      = new HashSet<>();

    private IMonitor              monitor;

    boolean                       firstClass;
    boolean                       suppressContextualizerValidation = false;

    public KIMScope() {
    }

    protected KIMScope newInstance() {
        return new KIMScope();
    }

    @Override
    public String toString() {
        return "{SCOPE " + type + " NS=" + namespaceId + " MO="
                + (object == null ? "null" : object.toString()) + "}";
    }

    /*
     * create a copy of the same type with us as parents. Redefine if necessary to
     * transmit more objects.
     */
    protected KIMScope getChild() {

        try {
            KIMScope ret = newInstance();

            ret.parent = this;
            ret.projectErrors = this.projectErrors;
            ret.projectWarnings = this.projectWarnings;
            ret.contextErrors = this.contextErrors;
            ret.contextWarnings = this.contextWarnings;
            ret.info = this.info;
            ret.namespace = this.namespace;
            ret.project = this.project;
            ret.namespaceId = this.namespaceId;
            ret.resource = this.resource;
            ret.timestamp = this.timestamp;
            ret.beingParsed = this.beingParsed;
            ret.suppressContextualizerValidation = this.suppressContextualizerValidation;
            
            /*
             * don't transmit the inputstream
             */
            return ret;

        } catch (Throwable t) {
            throw new KlabRuntimeException(t);
        }
    }

    public boolean suppressContextualizerValidation() {
        return suppressContextualizerValidation;
    }

    public KIMScope suppressContextualizerValidation(boolean b) {
        this.suppressContextualizerValidation = b;
        return this;
    }

    @Override
    public KIMScope get(IProject project) {
        KIMScope ret = getChild();
        ret.namespace = null;
        ret.namespaceId = null;
        ret.resource = null;
        ret.project = project;
        ret.type = Type.PROJECT;
        ret.projectErrors = new ArrayList<>();
        ret.projectWarnings = new ArrayList<>();
        ret.info = new ArrayList<>();
        return ret;
    }

    @Override
    public KIMScope get(Type type) {
        KIMScope ret = getChild();
        ret.type = type;
        return ret;
    }

    @Override
    public long getTimestamp() {
        return timestamp;
    }

    @Override
    public KIMNamespace getNamespace() {
        return namespace != null ? namespace : (parent == null ? null : parent.getNamespace());
    }

    @Override
    public IParsingScope withMonitor(IMonitor monitor) {
        KIMScope ret = getChild();
        ret.monitor = monitor;
        return ret;
    }

    @Override
    public IProject getProject() {
        return project != null ? project : (parent == null ? null : parent.getProject());
    }

    @SuppressWarnings("unchecked")
    public <T extends ILanguageObject> T get(Class<? extends T> cls) {
        return (object != null && cls.isAssignableFrom(object.getClass())) ? (T) object
                : (parent == null ? null : parent.get(cls));
    }

    /**
     * Find the innermost concept statement and return the core concept that has been
     * declared for it.
     * 
     * @return core concept in scope
     */
    public IConcept getCoreConcept() {
        return coreConcept != null ? coreConcept : (parent == null ? null : parent.getCoreConcept());
    }

    @Override
    public boolean isInScope(Type type) {
        return this.type == type || (parent != null && parent.isInScope(type));
    }

    @Override
    public void error(String message, int line) {

        KIMModelObject obj = getFirstClassObject();
        if (obj != null) {
            obj.errorCount++;

            /*
             * if object is void, turn errors into warnings with prefix
             */
            if (obj instanceof KIMModel && ((KIMModel) obj).isInactive()) {
                warning("(fatal error): " + message, line);
                return;
            }
        }
        ICompileError error = new CompileError(namespaceId, message, line);
        if (getNamespace() != null) {
            namespace.errors.add(error);
        }
        projectErrors.add(error);
        contextErrors.add(error);
        onError(error);
    }

    public IMonitor getMonitor() {
        return monitor == null ? KLAB.ENGINE.getMonitor() : monitor;
    }

    protected void onError(ICompileError error) {
        System.err.println("" + error);
    }

    protected void onWarning(ICompileWarning error) {
        System.err.println("" + error);
    }

    protected void onInfo(ICompileInfo error) {
        // System.err.println("" + error);
    }

    private KIMModelObject getFirstClassObject() {
        return firstClass ? object : (parent == null ? null : parent.getFirstClassObject());
    }

    @Override
    public void info(String message, int line) {
        ICompileInfo info = new CompileInfo(namespaceId, message, line);
        if (getNamespace() != null) {
            namespace.info.add(info);
        }
        this.info.add(info);
        onInfo(info);
    }

    @Override
    public void warning(String message, int line) {

        KIMModelObject obj = getFirstClassObject();
        if (obj != null) {
            obj.warningCount++;
        }
        ICompileWarning error = new CompileWarning(namespaceId, message, line);
        if (getNamespace() != null) {
            namespace.warnings.add(error);
        }
        projectWarnings.add(error);
        contextWarnings.add(error);
        onWarning(error);
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof KIMScope && ((KIMScope) o).type == type;
    }

    @Override
    public int hashCode() {
        return type.ordinal();
    }

    /**
     * Return the ID attributed to the closest named context.
     * 
     * @return ID of namespace in scope.
     */
    public String getId() {
        return namespaceId;
    }

    public void accept(Object kimObject) {
        if (kimObject instanceof KIMNamespace) {
            this.namespace = (KIMNamespace) kimObject;
            beingParsed.add(namespaceId);
        } /*
           * else if (kimObject instanceof KIMModel) { this.model = (KIMModel) kimObject;
           * }
           */else if (kimObject instanceof KIMModelObject) {
            this.object = (KIMModelObject) kimObject;
        }
    }

    @Override
    public boolean hasSeen(String namespaceId) {
        return beingParsed.contains(namespaceId);
    }

    @Override
    public KIMScope forNamespace(String namespaceId) {
        KIMScope ret = getChild();
        ret.type = Type.NAMESPACE;
        ret.namespaceId = namespaceId;
        return ret;
    }

    /**
     * Given an object passed to parse(), set up the environment for parsing the namespace
     * identified by the object, setting the namespaceId, the resource, and the
     * inputStream field to valid values. If resource cannot be opened, just leave
     * everything null in the returned context.
     * 
     * @param resource
     * @return scope appropriate to parse the resource
     * @throws KlabException
     */
    @Override
    public KIMScope forResource(Object resource) throws KlabException {

        KIMScope ret = getChild();

        if (resource instanceof InputStream) {
            if (getId() == null) {
                throw new KlabValidationException("cannot parse a KIM input stream in a context that does not define a namespace ID");
            }
            ret.input = (InputStream) resource;
        } else {
            File file = null;
            if (resource instanceof File) {
                if (ret.namespaceId == null) {
                    ret.namespaceId = MiscUtilities.getFileBaseName(resource.toString());
                }
                file = (File) resource;

            } else if (resource instanceof URL) {
                if (ret.namespaceId == null) {
                    ret.namespaceId = MiscUtilities.getFileBaseName(((URL) resource).getPath());
                }
                try {
                    file = File.createTempFile(namespaceId, KIM.DEFAULT_FILE_EXTENSION);
                } catch (IOException e) {
                    throw new KlabIOException(e);
                }
            } else if (resource instanceof String) {

                /*
                 * if we have a project, look it up in there first
                 */
                IProject p = getProject();
                if (p != null) {
                    file = p.findResourceForNamespace(resource.toString(), false);
                }
            }

            if (file != null) {
                ret.resource = file;
                ret.timestamp = file.lastModified();
                try {
                    ret.input = new FileInputStream(file);
                    ret.mustClose = true;
                } catch (FileNotFoundException e) {
                    throw new KlabIOException(e);
                }
            }
        }

        return ret;
    }

    public InputStream getInputStream() {
        return input;
    }

    public void close() throws KlabException {
        if (mustClose) {
            try {
                input.close();
            } catch (Exception e) {
                throw new KlabIOException(e);
            }
        }
    }

    public static KIMScope forProject(IProject project) {
        KIMScope ret = new KIMScope();
        ret.project = project;
        ret.type = Type.PROJECT;
        return ret;
    }

    public File getResource() {
        return resource;
    }

    public void synchronize() {
        KIMNamespace ns = getNamespace();
        if (ns != null) {
            try {
                ns.synchronizeKnowledge();
            } catch (Throwable e) {
                error("internal error: " + e.getMessage(), this.object == null ? 0
                        : this.object.getFirstLineNumber());
            }
        }
    }

    public void setCoreConcept(IConcept c) {
        coreConcept = c;
    }

    public KIMScope firstClass() {
        firstClass = true;
        return this;
    }

    public boolean is(Type type) {
        return this.type == type;
    }

    public boolean hasErrors() {
        return projectErrors.size() > 0;
    }

    @Override
    public int getErrorCount() {
        return contextErrors.size();
    }

    @Override
    public int getWarningCount() {
        return contextWarnings.size();
    }

    public IModelObject getObjectInScope(Type type) {
        return (this.type == type) ? object : (parent == null ? null : parent.getObjectInScope(type));
    }

}
