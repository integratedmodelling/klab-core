/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IDistanceObserver;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.contextualization.IDirectInstantiator;
import org.integratedmodelling.api.modelling.contextualization.IEventInstantiator;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.contextualization.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.model.Coverage;
import org.integratedmodelling.common.model.runtime.AbstractStateContextualizer;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMDistanceObserver extends KIMNumericObserver
        implements IDistanceObserver, NetworkSerializable, NetworkDeserializable {

    IUnit               unit;
    IConcept            originalConcept;
    IDirectInstantiator objectContextualizer;

    public class DistanceComputer extends AbstractStateContextualizer {

        ISpatialIndex  index;
        ISpatialExtent extent;
        int            lastTransition = -2;
        Unit           meters         = new Unit("m");

        public DistanceComputer(IMonitor monitor) {
            super(monitor);
        }

        @Override
        public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance)
                throws KlabValidationException {
            if (objectContextualizer != null) {
                objectContextualizer.setContext(parameters, model, project, provenance);
            }
            super.setContext(parameters, model, project, provenance);
        }

        @Override
        public Map<String, IObservation> define(String name, IObserver observer, IActiveDirectObservation contextSubject, IResolutionScope context, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, boolean isLastInChain, IMonitor monitor)
                throws KlabException {

            if (objectContextualizer != null) {
                objectContextualizer.initialize((IActiveSubject) contextSubject, context, observer
                        .getModel(), expectedInputs, expectedOutputs, monitor);
            }
            return super.define(name, observer, contextSubject, context, expectedInputs, expectedOutputs, isLastInChain, monitor);
        }

        protected Object processState(int stateIndex, ITransition transition) throws KlabException {

            Double val = Double.NaN;

            if (index == null) {

                if (extent == null) {
                    if (getScale().getSpace() == null) {
                        throw new KlabValidationException("distance can not be computed in non-spatial contexts");
                    }
                    extent = getScale().getSpace();
                }
                index = extent.getIndex(true);

                if (fromObject.size() > 0) {

                    monitor.info("computing distance from "
                            + StringUtils.joinCollection(fromObject, ','), Messages.INFOCLASS_MODEL);

                    for (IDirectObserver o : getConcreteObjects()) {
                        index.add(o.getCoverage(monitor).getSpace(), o.getName());
                    }
                }

            }

            /*
             * subjects and events may be added at any time, so we should check for new
             * objects at each new transition
             */
            if (originalConcept != null && getContextObservation() instanceof ISubject
                    && fromObject.size() == 0) {

                int transitionIndex = transition == null ? -1 : transition.getTimeIndex();

                if (transitionIndex != lastTransition) {

                    lastTransition = transitionIndex;

                    monitor.info("computing distance from all " + NS.getDisplayName(originalConcept), null);

                    addObjects(transition);

                    for (ISubject s : ((ISubject) getContextObservation()).getSubjects()) {
                        if (s.getType().is(originalConcept) && s.getScale().getSpace() != null
                                && !index.contains(s.getName())) {
                            index.add(s.getScale().getSpace(), s.getName());
                        }
                    }
                    for (IEvent s : ((ISubject) getContextObservation()).getEvents()) {
                        if (s.getType().is(originalConcept) && s.getScale().getSpace() != null
                                && !index.contains(s.getName())) {
                            index.add(s.getScale().getSpace(), s.getName());
                        }
                    }
                }
            }

            /*
             * process distance for state
             */
            if (index != null && index.size() > 0) {

                int sfs = getScale()
                        .getExtentOffset(getScale().getExtent(KLAB.c(NS.SPACE_DOMAIN)), stateIndex);
                val = index.distanceToNearestObjectFrom(sfs);
                if (!Double.isNaN(val) && !unit.equals(meters)) {
                    val = (Double) unit.convert(val, meters);
                }
            }

            Object ret = val;
            if (getDiscretization() != null) {
                ret = getDiscretization().classify(val);
            }

            return ret;
        }

        public void addObjects(ITransition transition) throws KlabException {

            /*
             * if we were given a contextualizer, run it to ensure we have the objects and
             * add them to the index.
             */
            Map<String, IObservation> objects = null;

            if (getContextObservation().getResolvedCoverage(originalConcept) != null && getContextObservation() instanceof ISubject) {

                for (ISubject s : ((ISubject)getContextObservation()).getSubjects()) {
                    if (s.getObservable().getType().is(originalConcept)) {
                        if (objects == null) {
                            objects = new HashMap<>();
                        }
                        objects.put(s.getName(), s);
                    }
                }
                
            } else {

                if (objectContextualizer != null) {
                    if (objectContextualizer instanceof ISubjectInstantiator) {
                        objects = ((ISubjectInstantiator) objectContextualizer)
                                .createSubjects((IActiveSubject) getContextObservation(), transition, new HashMap<>());
                    } else if (objectContextualizer instanceof IEventInstantiator) {
                        objects = ((IEventInstantiator) objectContextualizer)
                                .createEvents(transition, new HashMap<>());
                    }
                }
            }

            if (objects != null && index != null) {
                for (IObservation object : objects.values()) {
                    ISpatialExtent ospace = object.getScale().getSpace();
                    if (ospace != null) {
                        index.add(ospace, ((IDirectObservation) object).getName());
                    }
                }
            }
        }

        @Override
        public Map<String, Object> initialize(int index, Map<String, Object> inputs) throws KlabException {
            return MapUtils.ofWithNull(getStateName(), processState(index, ITransition.INITIALIZATION));
        }

        @Override
        public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
                throws KlabException {
            return MapUtils.ofWithNull(getStateName(), processState(index, transition));
        }

        @Override
        public boolean isProbabilistic() {
            return false;
        }

        @Override
        public String getLabel() {
            // TODO add source
            return "distance calculation";
        }
    }

    public class DistanceMediator extends NumericMediator {

        IDistanceObserver other;
        boolean           convertUnits = false;

        protected DistanceMediator(IDistanceObserver other, IMonitor monitor) {
            super(monitor);
            this.other = other;
            this.convertUnits = !unit.equals(other.getUnit());
        }

        @Override
        public Object mediate(Object object) throws KlabException {

            Number val = getValueAsNumber(object, other);

            if (!Double.isNaN(val.doubleValue()) && convertUnits) {
                val = unit.convert(val, other.getUnit());
            }

            return super.mediate(val);
        }

        @Override
        public String getLabel() {
            return (unit.equals(other.getUnit()) ? "" : "convert " + other.getUnit() + " to " + unit)
                    + (discretization == null ? ""
                            : ((unit.equals(other.getUnit()) ? "" : " ->") + "discretize"));
        }
    }

    @Override
    public IStateContextualizer getDataProcessor(IMonitor monitor) throws KlabException {
        return (objectContextualizer == null && fromObject.size() == 0 && getConcreteObjects().size() == 0)
                ? null
                : new DistanceComputer(monitor);
    }

    public KIMDistanceObserver() {
        minimumValue = 0;
    }

    KIMDistanceObserver(IConcept observable, String unit) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
        this.unit = new Unit(unit);
    }
    
    public KIMDistanceObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        minimumValue = 0;
        unit = new KIMUnit(context.get(KIMScope.Type.UNIT), statement.getUnit());
        // TODO check it's a length
        // try {
        // if (observable != null) {
        // ((ObservableSemantics) observable).setType(this.getObservedType(context,
        // this.observable.getType()));
        // }
        // } catch (KlabRuntimeException e) {
        // context.error(e.getMessage(), getFirstLineNumber());
        // }
        validate(context);
    }

    public KIMDistanceObserver(IObserver observer) {
        super(observer);
        minimumValue = 0;
        if (!(observer instanceof IDistanceObserver)) {
            throw new KlabRuntimeException("cannot initialize a distance observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.unit = ((IDistanceObserver) observer).getUnit();
    }

    public KIMDistanceObserver(String string, IObservableSemantics obs) {
        minimumValue = 0;
        String[] defs = string.split("\\|");
        unit = new Unit(defs[1]);
        if (defs.length > 2) {
            discretization = new org.integratedmodelling.common.classification.Classification(defs[2]);
        }
        observable = obs;
    }

    @Override
    public IObserver copy() {
        return new KIMDistanceObserver(this);
    }

    @Override
    public List<IObservableSemantics> getAlternativeObservables() {
        List<IObservableSemantics> ret = new ArrayList<>();
        if (originalConcept != null) {
            /*
             * these are all direct observations
             */
            ret.add(new ObservableSemantics(originalConcept));
        }
        return ret;
    }

    @Override
    public IUnit getUnit() {
        return unit;
    }

    @Override
    public IObserver getMediatedObserver() {
        return mediated;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.DISTANCE_OBSERVATION);
    }

    @Override
    protected IConcept getObservedType(KIMScope context, IConcept knowledge) {

        NS.synchronize();

        if (fromObject.size() == 0 && !knowledge.is(NS.DISTANCE)) {
            originalConcept = knowledge;
        }

        return Observables.makeDistance(knowledge);
    }

    public static String asString(IDistanceObserver m) {
        String ret = "oDS|" + m.getUnit();
        if (m.getDiscretization() != null) {
            ret += "|" + org.integratedmodelling.common.classification.Classification
                    .asString(m.getDiscretization());
        }
        return ret;
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException {

        observer = getRepresentativeObserver(observer);

        if (!(observer instanceof IDistanceObserver))
            throw new KlabValidationException("distances can only mediate other distances");

        if (((IDistanceObserver) observer).getUnit().equals(getUnit())) {
            return null;
        }

        return new DistanceMediator((IDistanceObserver) observer, monitor);
    }

    @Override
    public String toString() {
        return "DST/" + getObservable();
    }

    @Override
    public Collection<IDirectObserver> getConcreteObjects() throws KlabException {
        return resolveConcreteIds();
    }

    @Override
    protected IConcept getObservableConcept(KIMScope context, KIMKnowledge observed) {

        String cns = "";
        if (fromObject.size() > 0) {
            for (String s : fromObject) {
                cns += StringUtils.capitalize(Path.getLast(s, '.'));
            }
        }
        return Observables.makeDistanceTo(context.getNamespace(), cns);
    }

    @Override
    public boolean needsResolution() {

        /*
         * return false if we produce our data; the required info come from this being a
         * IDerivedObserver.
         */
        try {
            if (fromObject.size() != 0 || getConcreteObjects().size() != 0 /*
                                                                            * ||
                                                                            * objectContextualizer
                                                                            * != null
                                                                            */
                    || !super.needsResolution()) {
                return false;
            }
        } catch (KlabException e) {
            throw new KlabRuntimeException(e);
        }
        return super.needsResolution();
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        unit = new Unit(bean.getUnitOrCurrency());
        if (bean.getComparisonKnowledge() != null) {
            originalConcept = Knowledge.parse(bean.getComparisonKnowledge());
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        ret.setUnitOrCurrency(unit.asText());
        if (originalConcept != null) {
            ret.setComparisonKnowledge(((Knowledge) originalConcept).asText());
        }

        return (T) ret;
    }

    @Override
    public ICoverage acceptAlternativeContextualizer(IContextualizer contextualizer, ICoverage coverage) {
        if (contextualizer instanceof IDirectInstantiator) {
            objectContextualizer = (IDirectInstantiator) contextualizer;
            return Coverage.FULL(scale);
        }
        return Coverage.EMPTY;
    }
    

    @Override
    public String getDefinition() {
        // TODO Auto-generated method stub
        return "distance to " + Observables.getDeclaration(getObservable().getType(), getNamespace().getProject());
    }
    

	@Override
	public boolean isExtensive(IConcept extent) {
		return false;
	}
}
