/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.Date;

import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.Currency;
import org.integratedmodelling.common.vocabulary.NS;

public class KIMCurrency extends Currency implements ILanguageObject {

	private int startLine;
	private int endLine;

	public KIMCurrency(KIMScope context, org.integratedmodelling.kim.kim.Currency statement, double from, double to) {

		int lineN = statement == null ? context.get(IObserver.class).getFirstLineNumber()
				: NodeModelUtils.getNode(statement).getStartLine();

		if (statement != null) {
			this.startLine = NodeModelUtils.getNode(statement).getStartLine();
			this.endLine = NodeModelUtils.getNode(statement).getEndLine();
		}

		/*
		 * FIXME the ID/YEAR do not come out but the string comes in as
		 * conceptId. This should not be necessary.
		 */
		String currencyId = null;
		int year = new Date().getYear();
		
		if (statement != null) {
			currencyId = statement.getId();
			year = statement.getYear();

			if (currencyId == null && statement.getConcept() != null && statement.getConcept().contains("@")) {
				String[] ids = statement.getConcept().split("@");
				currencyId = ids[0];
				year = Integer.parseInt(ids[1]);
			}
		}

		if (statement != null && currencyId != null) {
			setMonetary(currencyId, year);

		} else {

			IKnowledgeObject k = null;

			if (statement != null && statement.getConcept() != null) {
				k = new KIMKnowledge(context.get(KIMScope.Type.CURRENCY_CONCEPT), statement.getConcept(), false, lineN);
			}

			if (Double.isNaN(from) || Double.isNaN(to)) {
				context.error("currencies stating simply a concept must define a range of values", lineN);
			}

			IConcept c = KLAB.c(NS.CORE_VALUE);
			if (k == null) {
				// maybe not
				// context.warning("non-monetary currencies should specify a
				// concept for the value (after
				// 'in')", lineN);
			} else {
				c = k.getConcept();
			}

			/*
			 * TODO check concept
			 */
			setConcept(c, from, to);
		}
	}

	@Override
	public int getFirstLineNumber() {
		return startLine;
	}

	@Override
	public int getLastLineNumber() {
		return endLine;
	}

}
