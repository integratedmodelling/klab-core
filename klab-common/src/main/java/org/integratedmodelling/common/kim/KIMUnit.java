/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.kim.kim.NUMBER;
import org.integratedmodelling.kim.kim.UnitElement;
import org.integratedmodelling.kim.kim.UnitOp;

public class KIMUnit extends Unit implements ILanguageObject {

    private int startLine;
    private int endLine;

    public KIMUnit(KIMScope context, org.integratedmodelling.kim.kim.Unit statement) {
        this.startLine = NodeModelUtils.getNode(statement).getStartLine();
        this.endLine = NodeModelUtils.getNode(statement).getEndLine();
        String utext = reassembleUnit(statement);
        try {
            parse(utext);
        } catch (Throwable e) {
            context.error("unit " + utext + " was not recognized", startLine);
        }
    }

    @Override
    public int getFirstLineNumber() {
        return startLine;
    }

    @Override
    public int getLastLineNumber() {
        return endLine;
    }

    private String reassembleUnit(org.integratedmodelling.kim.kim.Unit unit) {

        String ret = processUnitElement(unit.getRoot());
        int i = 0;
        for (UnitOp u : unit.getConnectors()) {
            ret += u.toString();
            ret += processUnitElement(unit.getUnits().get(i++));
        }

        return ret;
    }

    private String processUnitElement(UnitElement root) {

        if (root == null)
            return "";

        if (root.getId() != null)
            return root.getId();
        else if (root.getNum() != null)
            return numberToString(root.getNum());
        else
            return "(" + reassembleUnit(root.getUnit()) + ")";
    }

    private String numberToString(NUMBER num) {
        if (num.getFloat() != null)
            return num.getFloat();
        if (num.getSint() != null)
            return num.getSint();
        return Integer.toString(num.getInt());
    }

}
