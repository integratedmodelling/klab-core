/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IUnit;
import org.integratedmodelling.api.modelling.IValueMediator;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Unit;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMMeasurementObserver extends KIMNumericObserver
        implements IMeasuringObserver, NetworkSerializable, NetworkDeserializable {

    IUnit unit;

    public class MeasurementMediator extends NumericMediator {

        private IMeasuringObserver other;
        boolean                    convertUnits = false;

        protected MeasurementMediator(IMeasuringObserver other, IMonitor monitor) {
            super(monitor);
            this.other = other;
            this.convertUnits = !unit.equals(other.getUnit());
        }

        @Override
        public Object mediate(Object object) throws KlabException {

            Number val = getValueAsNumber(object, other);

            if (!Double.isNaN(val.doubleValue()) && convertUnits) {
                val = unit.convert(val, other.getUnit());
            }

            return super.mediate(val);
        }

        @Override
        public String toString() {
            return "[" + getLabel() + "]";
        }

        @Override
        public String getLabel() {
            return (unit.equals(other.getUnit()) ? "" : "convert " + other.getUnit() + " to " + unit) +
                    (discretization == null ? ""
                            : ((unit.equals(other.getUnit()) ? "" : "->") + "discretize"));
        }
    }

    @Override
    protected IConcept getObservedType(KIMScope context, IConcept knowledge) {
        // level ratios can be measured in decibels, although allowing ratios is a bit of
        // a distorsion.
        if (!knowledge.is(KLAB.c(NS.CORE_PHYSICAL_PROPERTY)) && !knowledge.is(KLAB.c(NS.CORE_RATIO))) {
            context.error("measurements can only be of physical properties", this.getFirstLineNumber());
        }
        return super.getObservedType(context, knowledge);
    }

    public KIMMeasurementObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        unit = new KIMUnit(context.get(KIMScope.Type.UNIT), statement.getUnit());
        validate(context);
    }

    KIMMeasurementObserver(ISemantic observable, String unit2) {
        if (observable instanceof IObservableSemantics) {
            this.observable = (IObservableSemantics) observable;
        } else {
            this.observable = new ObservableSemantics(observable.getType(), KLAB.c(NS.MEASUREMENT), CamelCase
                    .toLowerCase(observable.getType().getLocalName(), '-'));
            ((ObservableSemantics) this.observable).setObserver(this);
        }
        this.unit = new Unit(unit2);
    }

    public KIMMeasurementObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof IMeasuringObserver)) {
            throw new KlabRuntimeException("cannot initialize a measuring observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.unit = ((IMeasuringObserver) observer).getUnit();
    }

    public KIMMeasurementObserver(String string, IObservableSemantics obs) {
        String[] defs = string.split("\\|");
        unit = new Unit(defs[1]);
        if (defs.length > 2) {
            discretization = new org.integratedmodelling.common.classification.Classification(defs[2]);
        }
        observable = obs;
    }

    /**
     * Only for the deserializer
     */
    public KIMMeasurementObserver() {
    }

    @Override
    public IObserver copy() {
        return new KIMMeasurementObserver(this);
    }

    @Override
    public IUnit getUnit() {
        return unit;
    }

    public void validateUnit(KIMScope context) {
        if (observable != null) {
            String siUnit = NS.getMetadata(this.getObservable().getType(), NS.SI_UNIT_PROPERTY);
            boolean isExtensive = observable.is(KLAB.c(NS.CORE_EXTENSIVE_PHYSICAL_PROPERTY));
            if (siUnit == null) {
                context.warning("cannot establish base unit for checking: please check type metadata", getFirstLineNumber());
            } else  /*if ( FIXME! must check with actual extents implied.  !isExtensive)*/ {
                IUnit refUnit = new Unit(siUnit);
                if (isExtensive) {
                    // TODO not implemented
                    refUnit = Unit.addExtents(refUnit, ((KIMModel) model).getExtentDimensions());
                }
                if (!unit.isCompatible(refUnit)) {
                    context.error("unit " + unit + " is incompatible with base SI unit " + refUnit
                            + "  for type: "
                            + siUnit, getFirstLineNumber());
                }
            }
        }
    }

    @Override
    public IObserver getMediatedObserver() {
        return mediated;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.MEASUREMENT);
    }

    public static String asString(IMeasuringObserver m) {
        String ret = "oMS|" + m.getUnit();
        if (m.getDiscretization() != null) {
            ret += "|"
                    + org.integratedmodelling.common.classification.Classification.asString(m
                            .getDiscretization());
        }
        return ret;
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException {

        observer = getRepresentativeObserver(observer);

        if (!(observer instanceof IMeasuringObserver))
            throw new KlabValidationException("measurements can only mediate other measurements");

        if (discretization != null) {
            if (((IMeasuringObserver) observer).getDiscretization() != null) {
                monitor.warn(getObservable().getType()
                        + ": discretized measurements should not mediate other discretized measurements: value will be undiscretized, then discretized again");
            }
            return new MeasurementMediator((IMeasuringObserver) observer, monitor);
        }

        if (((IMeasuringObserver) observer).getUnit().equals(getUnit())) {
            return null;
        }

        return new MeasurementMediator((IMeasuringObserver) observer, monitor);
    }
    //
    // @Override
    // public IStateActuator getActuator(IMonitor monitor) {
    // return new MeasurementActuator(getActions(), monitor);
    // }

    @Override
    public String toString() {
        return "MEA/" + getObservable() + " (" + unit + ")";
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        unit = new Unit(bean.getUnitOrCurrency());

    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        ret.setUnitOrCurrency(unit.asText());

        return (T) ret;
    }

    @Override
    public String getDefinition() {
        // TODO Auto-generated method stub
        return "measure " + Observables.getDeclaration(getObservable().getType(), getNamespace().getProject()) + " in " + unit.asText();
    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return getObservable().getType().is(KLAB.c(NS.CORE_EXTENSIVE_PHYSICAL_PROPERTY)) &&
                !unit.isDensity(extent);
    }

    @Override
    public IValueMediator getValueMediator() {
        return unit;
    }
}
