/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.metadata.IDocumentation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.metadata.IModelMetadata;
import org.integratedmodelling.api.modelling.ExtentDimension;
import org.integratedmodelling.api.modelling.IAction;
import org.integratedmodelling.api.modelling.IClassifyingObserver;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IMeasuringObserver;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.contextualization.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.space.IAreal;
import org.integratedmodelling.api.space.ILineal;
import org.integratedmodelling.api.space.IPuntal;
import org.integratedmodelling.api.space.IVolumetric;
import org.integratedmodelling.api.time.ITemporal;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.reporting.Documentation;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Roles;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Annotation;
import org.integratedmodelling.kim.kim.ConceptDeclaration;
import org.integratedmodelling.kim.kim.ModelObservable;
import org.integratedmodelling.kim.kim.ModelStatement;
import org.integratedmodelling.lang.Axiom;
import org.integratedmodelling.lang.LogicalConnector;

public class KIMModel extends KIMObservingObject implements IModel {

    boolean                                    isPrivate;
    boolean                                    isInactive;
    boolean                                    isInstantiator;
    boolean                                    isPrimary;
    boolean                                    isLearning;
    boolean                                    isAssessment;
    boolean                                    isAdditional;

    private boolean                            metadataMerged       = false;
    IFunctionCall                              dataSourceGenerator;
    IFunctionCall                              objectSourceGenerator;
    IObserver                                  observer;
    Object                                     inlineValue;

    List<IObservableSemantics>                 observables          = new ArrayList<>();
    List<AttributeTranslator>                  attributeTranslators = new ArrayList<>();
    private IContextualizer                    predefinedContextualizer;
    private IConcept                           reinterpretAs        = null;
    private IDocumentation                     documentation;

    List<Pair<IObservableSemantics, IConcept>> localRoles           = new ArrayList<>();
    private IObservableSemantics               uninterpretedObservable;

    /*
     * data structure below support reification models. If the model is a reification
     * model, the _datasource will be a source of objects and the attribute table below is
     * used to associate properties to each object based on their attributes.
     */
    static public class AttributeTranslator {

        public String    attribute;
        public IObserver observer;
        public IProperty property;
    }

    public KIMModel(IObservableSemantics observable, IContextualizer contextualizer,
            IResolutionScope scope) {
        super(null);
        ObservableSemantics obs = new ObservableSemantics((ObservableSemantics) observable);
        obs.setModel(this);
        this.observables.add(obs);
        this.predefinedContextualizer = contextualizer;
        this.namespace = scope.getResolutionNamespace();
        name(observable.getFormalName());
    }

    /*
     * Wrap an observer.
     */
    public KIMModel(KIMObserver observer) {
        super(observer);
        this.observer = observer.copy();
        setParent(this.observer.getParent(), false);
        ((KIMObserver) this.observer).setParent(this, true);
        if (this.observer.getObservable() != null) {
            this.observables.add(this.observer.getObservable());
        }
        this.namespace = this.observer.getNamespace();
        if (this.id == null && this.observer.getObservable() != null) {
            name(this.observer.getObservable().getFormalName());
        }
    }

    /*
     * Wrap an observer and use the object source from a model to produce its data source
     * generator function.
     */
    public KIMModel(KIMObserver observer, KIMModel ds, String attribute,
            IMonitor monitor) {

        super(observer);
        observer = (KIMObserver) observer.copy();
        observer.model = this;
        try {
            ((ObservableSemantics) observer.getObservable()).setType(Observables
                    .contextualizeTo(observer.getObservable().getType(), ds.getObservable().getType()));
        } catch (KlabException e1) {
            // should not happen
            throw new KlabRuntimeException(e1);
        }
        // jeez
        ((ObservableSemantics) observer.getObservable()).setModel(this);
        ((ObservableSemantics) observer.getObservable()).setObserver(observer);
        observer.setParent(this, false);
        // observer.setParent(this, true);
        if (observer.getObservable() != null) {
            ObservableSemantics obs = new ObservableSemantics((ObservableSemantics) observer
                    .getObservable());
            obs.setModel(this);
            obs.setObserver(observer);
            this.observables.add(obs);
        }
        this.observer = observer;
        this.namespace = observer.getNamespace();
        if (this.id == null && observer.getObservable() != null) {
            name(observer.getObservable().getFormalName());
        }

        if (ds.hasObjectSource()) {
            IObjectSource os;
            try {
                os = ds.getObjectSource(monitor);
                this.dataSourceGenerator = os
                        .getDatasourceCallFor(attribute, getNamespace());
            } catch (KlabException e) {
                throw new KlabRuntimeException(e.getMessage());
            }
        } else if (ds.hasContextualizer()) {

            /*
             * TODO must use contextualizer as a datasource after instantiation and
             * dereification. Guess that needs to happen using a dedicated StateActuator.
             */
            // this.dataSourceGenerator = DereifyingStateActuator.getCallFor(ds,
            // attribute, getNamespace());
            throw new KlabRuntimeException("unimplemented dereification from instantiator in model "
                    + ds.getName());

        } else {
            throw new KlabRuntimeException("model " + ds.getName()
                    + " has no object source");
        }
    }

    /*
     * Wrap an observer with inline value.
     */
    KIMModel(Object literalValue, KIMObserver observer) {
        super(observer);
        inlineValue = literalValue;
        setParent(observer.getParent(), false);
        observer.setParent(this, true);
        this.observables.add(observer.getObservable());
        this.observer = observer;
    }

    /*
     * only for direct observers. Use makeModel in general
     */
    KIMModel(INamespace namespace, ObservableSemantics obs) {
        super(namespace, obs.getFormalName());
        this.observables.add(obs);
        isInstantiator = true;
        if (this.id == null) {
            name(obs.getFormalName());
        }
    }

    /*
     * Wrap an observer using a given observable; does not ensure the observable is OK or
     * make it so.
     */
    KIMModel(IConcept observable, IObserver observer, String formalName) {
        super((KIMObserver) observer);
        setParent(observer.getParent(), false);
        ((KIMObserver) observer).setParent(this, true);
        this.observer = observer;
        ((KIMObserver) observer).model = this;
        ObservableSemantics observ = new ObservableSemantics(observable, observer
                .getObservationType(), formalName);
        observ.setObserver(observer);
        this.observables.add(observ);
        name(formalName);
    }

    public KIMModel(KIMScope context, ModelStatement statement, IModelObject parent,
            List<Annotation> annotations) {

        super(context, statement, parent, annotations);

        if (statement.getMetadata() != null) {
            this.metadata = new KIMMetadata(context.get(KIMScope.Type.METADATA), statement
                    .getMetadata(), null);
        }

        if (statement.getDocumentation() != null) {

            this.documentation = new Documentation(new KIMMetadata(context
                    .get(KIMScope.Type.MODEL_DOCUMENTATION), statement
                            .getDocumentation(), null));

            for (String e : this.documentation.getErrors()) {
                context.error(e, lineNumber(statement.getDocumentation()));
            }

            if (context.getNamespace().getDocumentation() != null) {
                ((Documentation) this.documentation).merge(context.getNamespace().getDocumentation());
            }
        }

        this.isPrivate = statement.isPrivate() || getNamespace().isPrivate();
        this.isInactive = statement.isInactive() || getNamespace().isInactive();
        this.isAdditional = statement.isAdditional();

        if (statement.getInterpretAsRole() != null) {
            this.reinterpretAs = KLAB.c(statement.getInterpretAsRole());
            if (this.reinterpretAs == null || !NS.isRole(this.reinterpretAs)) {
                context.error("cannot reinterpret observable using " + statement.getInterpretAsRole()
                        + ": not a role", lineNumber(statement));
            }
        }

        // record whether this is a model that comes from a user statement. Used
        // to establish
        // namespace of reference for a context.
        this.isPrimary = true;

        boolean isOk = false;

        if (statement.isReification()) {
            isOk = defineInstantiatorModel(context, statement);
        } else {
            isOk = defineExplanationModel(context, statement);
        }

        if (this.reinterpretAs != null) {

            /*
             * save the original observable
             */
            int obspos = (isAssessment || isLearning) ? 1 : 0;
            this.uninterpretedObservable = observables.get(obspos);

            /*
             * add the role
             */
            KIMObservableSemantics interpreted = new KIMObservableSemantics(this.uninterpretedObservable);
            try {
                interpreted.setType(Observables
                        .declareObservable(interpreted.getType(), null, null, null, Collections
                                .singletonList(this.reinterpretAs), null, null, KLAB.REASONER
                                        .getOntology(), 0));
            } catch (KlabValidationException e) {
                context.error(e.getMessage(), interpreted.getFirstLineNumber());
            }
            observables.add(obspos, interpreted);
        }

        defineDependencies(context, statement.getDependencies());

        if (statement.getAccessor() != null) {
            this.contextualizerCall = new KIMFunctionCall(context
                    .get(KIMScope.Type.ACTUATOR_FUNCTION_CALL), statement.getAccessor());
        }
        if (this.contextualizerCall != null) {
            IConcept expected = getExpectedDirectContextualizer();
            if (expected == null) {
                context.error("cannot attribute the expected contextualizer type", this.contextualizerCall
                        .getFirstLineNumber());
            } else {
                validateFunctionCall(context, this.contextualizerCall, expected);
            }
        }

        if (statement.getContextualizers() != null) {

            Pair<List<IFunctionCall>, List<IAction>> zio = defineContextActions(context, this, statement
                    .getContextualizers());

            for (IFunctionCall function : zio.getFirst()) {
                scaleGenerators.add(function);
            }

            for (IAction action : zio.getSecond()) {
                actions.add(action);
            }
        }

        if (statement.getLookup() != null) {
            this.lookupTable = new KIMLookupTable(context, statement.getLookup().getTable());
        }
        
        /*
         * if we have documentation for actions that are not defined, create empty actions
         */
        if (documentation != null) {
            for (IAction.Trigger trigger : documentation.getTriggers()) {
                if (!hasActionsFor(trigger)) {
                    actions.add(new KIMAction(context
                            .get(KIMScope.Type.CONTEXTUALIZATION_ACTION), this, new HashSet<>(), null, trigger, null));
                }
            }
        }

        if (!isOk) {
            this.isInactive = true;
        }

        /*
         * Quality models: local concepts must be validated for inconsistency and aligned
         * with the observer if necessary
         */
        if (observables.size() > 0 && observables.get(0) != null && observer != null) {

            IKnowledge obb = observables.get(0).getType();

            if (NS.isObservable(obb)) {
                /*
                 * check core concepts are same unless we are classifying
                 */
                IKnowledge c1 = NS.getCoreType(obb);
                IKnowledge c2 = NS.getCoreType(observer.getObservable());
                if (NS.isTrait(obb) && !(observer instanceof IClassifyingObserver)) {

                    context.error("inconsistent semantics: " + NS.getDisplayName(obb)
                            + ": traits can only be observed through a classify statement", lineNumber(statement));
                } else if (!(NS.isTrait(obb) && observer instanceof IClassifyingObserver)
                        && !c1.equals(c2)) {
                    context.error("wrong observables: cannot mix a " + c1 + " with a "
                            + c2, lineNumber(statement));
                }

            } else if (obb.getConceptSpace().equals(context.getNamespace().getId())) {
                /*
                 * make NS
                 */
                if (NS.isClass(obb) && !(observer instanceof IClassifyingObserver)) {

                    context.error("inconsistent semantics for local concept: "
                            + NS.getDisplayName(obb)
                            + " is a type and the observer is not a classification", lineNumber(statement));

                } else if (observer.getObservable() != null) {

                    context.getNamespace().addAxiom(Axiom
                            .SubClass(observer.getObservable().getType().toString(), obb
                                    .getLocalName()));
                    context.synchronize();
                }

            } else {
                /*
                 * TODO non-observable
                 */
                context.error("inconsistent semantics for external concept: "
                        + NS.getDisplayName(obb)
                        + " is not observable", lineNumber(statement));
            }
        }

        if (getObservable() != null && getObservable().getType() != null
                && (NS.isQuality(getObservable()) || NS.isTrait(getObservable()))
                && observer == null) {
            context.error("quality models must have an observer", lineNumber(statement));
        }

        for (IObservableSemantics obs : observables) {
            if (obs == null) {
                continue;
            }
            if (obs.getType() != null && obs.getType().isAbstract()) {
                if (!NS.isClass(obs)) {
                    // context.error("the observed type for a model cannot be abstract:
                    // abstract types are only "
                    // + "admitted for classifications and in dependencies", obs
                    // instanceof KIMObservableSemantics ? ((KIMObservableSemantics) obs)
                    // .getFirstLineNumber() : lineNumber(statement));
                    ((ObservableSemantics) obs).setAbstract(true);
                }
            }
        }

        for (IObservableSemantics o : getObservables()) {
            /*
             * TODO more semantic validation
             */
            if (o != null && o.getType() != null) {
                if (!KLAB.REASONER.isSatisfiable(o.getType())) {
                    context.error("observable " + NS.getDisplayName(o.getType())
                            + " is not consistent. Please check semantics.", lineNumber(statement));
                }

                validateRoles(context);
            }
        }

        validate(context);
        /*
         * TODO consider making observers and actions children of the model when we have
         * the silly icons for them.
         */
    }

    @Override
    protected void validate(KIMScope context) {

        /*
         * check for input = output thing, which people seem to love doing.
         */
        for (IObservableSemantics o : getObservables()) {
            if (o != null && o.getType() != null) {
                if (hasDependencyFor(o.getType())) {
                    context.error("observed concept is also a dependency: "
                            + o.getType(), getFirstLineNumber());
                }
            }
        }

        if (observer instanceof IMeasuringObserver) {
            ((KIMMeasurementObserver) observer).validateUnit(context);
        }
    }

    @Override
    public IMetadata getMetadata() {
        if (!metadataMerged && observer != null) {
            this.metadata.merge(observer.getMetadata(), false);
        }
        metadataMerged = true;
        return this.metadata;
    }

    /**
     * 
     */
    private void validateRoles(KIMScope context) {
        Collection<IConcept> roles = Roles.getRoles(getObservable().getType());
        if (!this.isInstantiator()) {
            for (IConcept role : roles) {
                for (IConcept observableRole : Roles.getImpliedObservableRoles(role)) {
                    /*
                     * abstract roles must be represented in the model. Concrete ones
                     * generate dependencies so they are observed anyway.
                     */
                    if (observableRole.isAbstract() && !isAbstract() && !includesRole(observableRole)) {
                        context.error("model must reference the role " + observableRole
                                + " implied by "
                                + role, this.getFirstLineNumber());
                    }
                }
            }
        }
    }

    /*
     * true if at least one dependency or output have the passed role
     */
    @Override
    protected boolean includesRole(IConcept observableRole) {

        for (int i = 1; i < observables.size(); i++) {

            if (((KIMObservableSemantics) observables.get(i)).interpretAs != null
                    && ((KIMObservableSemantics) observables.get(i)).interpretAs
                            .is(observableRole)) {
                return true;
            }

            if (NS.contains(observableRole, Roles.getRoles(observables.get(i)))) {
                return true;
            }
        }

        if (super.includesRole(observableRole)) {
            return true;
        }

        if (observer != null) {
            if (((KIMObserver) observer).includesRole(observableRole)) {
                return true;
            }
        }
        return false;
    }

    private IConcept getExpectedDirectContextualizer() {

        if (observables.size() < 1 || observables.get(0).getType() == null) {
            return null;
        }
        if (NS.isThing(getObservable())) {
            return isInstantiator ? KLAB.c(NS.SUBJECT_INSTANTIATOR)
                    : KLAB.c(NS.SUBJECT_CONTEXTUALIZER);
        } else if (NS.isProcess(getObservable())) {
            return KLAB.c(NS.PROCESS_CONTEXTUALIZER);
        } else if (NS.isEvent(getObservable())) {
            return isInstantiator ? KLAB.c(NS.EVENT_INSTANTIATOR)
                    : KLAB.c(NS.EVENT_CONTEXTUALIZER);
        } else if (isInstantiator && NS.isRelationship(getObservable())) {
            return KLAB.c(NS.RELATIONSHIP_INSTANTIATOR);
        } else if (NS.isFunctionalRelationship(getObservable())) {
            return KLAB.c(NS.FUNCTIONAL_RELATIONSHIP_CONTEXTUALIZER);
        } else if (NS.isStructuralRelationship(getObservable())) {
            return KLAB.c(NS.STRUCTURAL_RELATIONSHIP_CONTEXTUALIZER);
        }
        return null;
    }

    private boolean defineExplanationModel(KIMScope context, ModelStatement statement) {

        boolean isNothing = false;
        IFunctionCall functionCall = null;
        IConcept refContext = null;

        /*
         * observable(s)
         */
        int obsIdx = 0;
        for (ModelObservable observable : statement.getObservables()) {

            KIMObservableSemantics ko = null;
            IConcept localConcept = null;

            if (observable.getDeclaration() != null) {

                KIMKnowledge kn = new KIMKnowledge(context
                        .get(KIMScope.Type.MODEL_OBSERVABLE), observable
                                .getDeclaration(), null);

                if (kn.isNothing()) {
                    isNothing = true;
                } else if (NS.isRole(kn.getConcept()) || NS.isQuality(kn.getConcept())
                        || NS.isTrait(kn.getConcept())) {

                    IConcept cn = kn.getConcept();
                    if (NS.isRole(cn)) {
                        try {
                            cn = Roles.getObservableWithRole(cn, null);
                        } catch (KlabException e) {
                            context.error(e.getMessage(), lineNumber(observable));
                        }
                    }

                    // TODO warn
                    IModel model = makeObservableModel(context
                            .get(KIMScope.Type.MODEL_OBSERVABLE), cn, lineNumber(observable
                                    .getDeclaration()), true);
                    ko = new KIMObservableSemantics(model.getObservable());

                } else {

                    ko = new KIMObservableSemantics(context, kn);
                    if (ko.getObservationType() == null) {
                        context.error("cannot attribute observation type for "
                                + kn.getConcept(), lineNumber(observable));
                    }

                }

                if (ko != null && observable.getAsrole() != null) {
                    IConcept role = KLAB.c(observable.getAsrole());
                    if (role == null || !NS.isRole(role)) {
                        context.error("concept " + observable.getAsrole()
                                + " mentioned after 'as' does not exist or is not a role", lineNumber(observable));
                    }

                    try {

                        IConcept roled = Observables
                                .declareObservable(kn.getConcept(), null, null, null, Collections
                                        .singleton(role), null, null, KLAB.REASONER.getOntology());
                        ko.setType(roled);

                    } catch (KlabValidationException e) {
                        context.error(e.getMessage(), lineNumber(observable));
                    }
                }

                // ko.setInherentTypes(context, observable.getDeclaration());
                observables.add(ko);
                ko.setOptional(observable.isOptional());

            } else if (observable.getConceptStatement() != null) {

                ko = new KIMObservableSemantics(context, new KIMKnowledge(context
                        .get(KIMScope.Type.MODEL_OBSERVABLE), observable
                                .getConceptStatement(), this));
                if (ko.isNothing()) {
                    isNothing = true;
                }

                ko.setOptional(observable.isOptional());
                observables.add(ko);

            } else if (observable.getFunction() != null) {

                if (obsIdx > 0) {
                    context.error("cannot have a data source function as a secondary observable", lineNumber(observable
                            .getFunction()));
                }
                functionCall = new KIMFunctionCall(context
                        .get(KIMScope.Type.OBSERVED_FUNCTIONCALL), observable
                                .getFunction());

            } else if (observable.getLiteral() != null) {

                // TODO!
                inlineValue = KIM.processLiteral(observable.getLiteral());

            } else if (observable.getInlineModel() != null) {

                IObserver observ = KIMObservingObject
                        .defineObserver(context, null, observable.getInlineModel()
                                .getObserver());

                if (observable.getInlineModel().getValue() != null) {

                    Object literalValue = KIM
                            .processLiteral(observable.getInlineModel().getValue());
                    ko = new KIMObservableSemantics(literalValue, observ);
                    ko.setOptional(observable.isOptional());

                } else if (observable.getInlineModel().getConceptId() != null) {

                    String localId = null;

                    if (observable.getInlineModel().getConceptId() != null) {
                        localId = observable.getInlineModel().getConceptId();
                        localConcept = context.getNamespace().getOntology()
                                .getConcept(localId);
                    }

                    observ = KIMObservingObject
                            .defineObserver(context, null, observable.getInlineModel()
                                    .getObserver());
                    IConcept oc = observ.getObservable().getType();

                    if (localId != null) {
                        if (localConcept != null && !localConcept.is(oc)) {
                            context.error("local concept " + localId
                                    + " is defined in this namespace with an incompatible type", lineNumber(observable
                                            .getInlineModel()));
                        } else {
                            List<IAxiom> ax = new ArrayList<>();
                            ax.add(Axiom.ClassAssertion(localId));
                            ax.add(Axiom.SubClass(oc.toString(), localId));
                            context.getNamespace().getOntology().define(ax);
                            localConcept = context.getNamespace().getOntology()
                                    .getConcept(localId);
                        }
                    }

                    ko = new KIMObservableSemantics(localConcept == null ? oc
                            : localConcept, observ);
                    ko.setOptional(observable.isOptional());

                } else {

                    ko = new KIMObservableSemantics(observ);
                    ko.setOptional(observable.isOptional());
                }

                if (ko != null) {

                    if (observable.getRole() != null) {
                        String roleid = observable.getRole();
                        if (!roleid.contains(":")) {
                            roleid = context.getNamespace().getId() + ":" + roleid;
                        }
                        IConcept role = KLAB.KM.getConcept(observable.getRole());
                        if (role == null) {
                            context.error("cannot understand " + observable.getRole()
                                    + " as a role", lineNumber(statement));
                        } else if (!NS.isRole(role)) {
                            context.error(observable.getRole()
                                    + " does not specify a role", lineNumber(statement));
                        } /*
                           * else if (role.isAbstract()) { context.error("role " +
                           * observable.getRole() +
                           * " is abstract; only concrete roles can be attributed to observables"
                           * , lineNumber(statement)); }
                           */ else {
                            ko.interpretAs = role;
                            localRoles.add(new Pair<>(ko, role));
                        }
                    }

                    if (observable.getObservableName() != null) {
                        ko.setFormalName(observable.getObservableName());
                    }
                    observables.add(ko);
                }

            }

            if (ko != null && !ko.isNothing()) {

                if (obsIdx == 0) {

                    refContext = (NS.isCountable(ko) && !isInstantiator) ? ko.getType()
                            : Observables.getContextType(ko.getType());

                } else if (refContext != null && !NS.isDirect(ko)) {

                    /*
                     * check compatible context
                     */

                    IConcept ctx = Observables.getContextType(ko.getType());
                    if (ctx != null) {
                        if (!Observables
                                .isCompatible(refContext, ctx, Observables.ACCEPT_REALM_DIFFERENCES)) {
                            context.error("observable " + NS.getDisplayName(ko.getType())
                                    + " has incompatible context ("
                                    + NS.getDisplayName(ctx)
                                    + ") with the overall model context ("
                                    + NS.getDisplayName(refContext)
                                    + ")", lineNumber(observable));
                        }
                    } else {
                        try {
                            IConcept fixedObs = Observables
                                    .declareObservable(ko
                                            .getType(), null, refContext, null);
                            ko.setType(fixedObs);
                        } catch (KlabValidationException e) {
                            context.error(e.getMessage(), lineNumber(observable));
                        }
                    }
                }
            }

            obsIdx++;
        }

        if (isNothing
                || (functionCall == null && inlineValue == null && observables.size() < 1
                        && !(statement.isInterpreter() || statement.isLearner()
                                || statement.isAssessor()))) {
            return false;
        }

        /*
         * observers
         */
        if (statement.getObservers() != null) {
            this.observer = defineObserver(context
                    .suppressContextualizerValidation(statement.isAssessor()
                            || statement.isLearner()), this, statement.getObservers());
        }

        /*
         * if we have a datasource, inline value, or are simply interpreting the observer,
         * our observable is the observer's.
         */
        if (observer != null && (functionCall != null || inlineValue != null
                || statement.isInterpreter()
                || statement.isLearner() || statement.isAssessor())) {

            // quality goes in first, becomes secondary if necessary
            observables.add(0, observer.getObservable());

            if (statement.isLearner() || statement.isAssessor()) {

                /*
                 * create process
                 */
                IConcept assessment = Observables
                        .makeAssessment(observer.getObservable().getType());

                /*
                 * if learner, add roles
                 */
                if (statement.isLearner()) {

                    this.isLearning = true;

                    try {
                        assessment = Observables
                                .declareObservable(assessment, null, null, null, Collections
                                        .singleton(NS.LEARNING_PROCESS_ROLE), null, null, KLAB.REASONER
                                                .getOntology());
                    } catch (KlabValidationException e) {
                        context.error(e.getMessage(), lineNumber(statement));
                    }
                    ((KIMObservableSemantics) observer
                            .getObservable()).interpretAs = NS.LEARNED_QUALITY_ROLE;

                    /*
                     * TODO the archetype dependency should be a countable and become
                     * <learnedquality> of <archetype>
                     */

                } else {
                    this.isAssessment = true;
                }

                /*
                 * transfer the dependencies and contextualizer to the model
                 */
                for (IDependency dependency : observer.getDependencies()) {

                    /*
                     * if dependency is archetype and model is a learning process, check
                     * dependency and turn it into what it needs to be.
                     */
                    if (this.isLearning
                            && ((KIMDependency) dependency).interpretAs != null
                            && ((KIMDependency) dependency).interpretAs
                                    .is(NS.ARCHETYPE_ROLE)) {

                        /*
                         * the learned quality in our observables is still the first
                         * observable.
                         */
                        IObservableSemantics learned = observables.size() > 0
                                ? observables.get(0) : null;

                        /*
                         * ensure it's a subject or event
                         */
                        if (!NS.isCountable(dependency.getObservable())) {

                            // TODO must check that archetype is the learned quality IN
                            // some other countable, or complain.
                            // This should be the way to specify the model when the
                            // archetypes are not known and controlled.

                            if (dependency.getObservable().getType().equals(learned.getType())) {
                                IConcept ainherent = Observables
                                        .getInherentType(dependency.getObservable().getType());
                                IConcept linherent = Observables.getContextType(learned.getType());

                                if (ainherent == null || (ainherent == null && linherent == null)
                                        || (ainherent != null && linherent != null
                                                && linherent.equals(ainherent))) {
                                    context.error("quality archetypes in learning processes must be inherent to subjects or events different from the model's context", dependency
                                            .getFirstLineNumber());
                                }
                            } else {
                                context.error("archetypes in learning processes must be subjects, events, or the learned quality with different inherency", dependency
                                        .getFirstLineNumber());
                            }
                        } else {

                            // TODO remove this and just trust that the modeler knows
                            // there are
                            // countables with the learned quality; the contextualizer
                            // will do
                            // the rest.

                            /*
                             * redefine observable in dependency to be the learned quality
                             * contextualized to the archetype
                             */
                            // if (learned != null) {
                            // IConcept type;
                            // try {
                            // type = Observables.contextualizeTo(learned
                            // .getType(), dependency
                            // .getObservable().getType());
                            // ((ObservableSemantics) dependency.getObservable())
                            // .setType(type);
                            // notifyRole(dependency.getObservable(), NS.ARCHETYPE_ROLE);
                            //
                            // } catch (KlabException e) {
                            // context.error(e.getMessage(), dependency
                            // .getFirstLineNumber());
                            // }
                            // }
                        }
                    }

                    this.dependencies.add(dependency);
                }
                this.contextualizerCall = ((KIMObserver) observer).contextualizerCall;
                this.observer.getDependencies().clear();
                ((KIMObserver) observer).contextualizerCall = null;

                /*
                 * transfer actions to model
                 */
                this.actions.addAll(observer.getActions());
                ((KIMObserver) observer).actions.clear();

                /*
                 * remove the observer, which remains in the second output
                 */
                this.observer = null;

                /*
                 * set process as main observable, pushing down the original quality
                 */
                observables.add(0, new ObservableSemantics(assessment));
            }
        }

        /*
         * if the observable is a new concept we need to make it a child of the same
         * observable we have in the observer.
         */
        if (observables.size() > 0 && observables.get(0) != null
                && observables.get(0).getType() != null) {
            
            IConcept type = observables.get(0).getType();

            if (!NS.isObservable(type)) {
                if (type.getConceptSpace().equals(getNamespace().getId())) {
                    if (observer != null && observer.getObservable() != null
                            && observer.getObservable().getType() != null) {
                        context.getNamespace().getOntology()
                                .define(Collections
                                        .singleton(Axiom.SubClass(observer.getObservable()
                                                .getType()
                                                .toString(), type.getLocalName())));
                        type = KLAB.c(type.toString());
                        /*
                         * reset the observable
                         */
                        ((ObservableSemantics) observables.get(0))
                                .setObservationType(null);
                        ((ObservableSemantics) observables.get(0))
                                .setType(new KIMKnowledge(context, type), context);
                        ((ObservableSemantics) observables.get(0)).setObserver(observer);
                    } else {
                        context.error("undeclared direct observable "
                                + type, lineNumber(statement));
                    }
                } else {
                    context.error("external concept " + type
                            + " is not observable", lineNumber(statement));
                }
            } else {
                if (observer != null) {
                    if (!((KIMObserver) observer).isCompatibleWith(type)) {
                        context.error(type
                                + " is not compatible with the observer's type "
                                + observer.getObservable()
                                        .getType(), lineNumber(statement));
                    }
                }
            }
        }

        /*
         * name self and every observer in cascade. Bit complicated to accommodate
         * redundancy for fluency in interpreter models.
         */
        String name = statement.getName();
        if (name == null) {
            name = statement.getName2();
        } else if (statement.getName2() != null) {
            context.error("cannot use 'named' twice: please give this model a unique name", lineNumber(statement));
        }
        if (name /* still */ == null) {
            name = context.getNamespace().getNewObjectName(this);
        }

        name(name);

        if (functionCall != null) {

            /*
             * observables are still undefined. Must keep it, load the concept later and
             * postpone validation.
             */
            IKnowledge match = null;
            if (this.observer != null && observer.getObservable() != null
                    && observer.getObservable().getType() != null) {
                match = observer.getObservable().getType();
                // if (observer.getObservable().getTraitType() != null) {
                // match = observer.getObservable().getTraitType();
                // }
            }

            if (match == null) {
                // ziocan
                KLAB.error("XIOCAN no match in ds");
                return false;
            }

            if (NS.isQuality(match)) {
                if (validateFunctionCall(context, functionCall, KLAB
                        .c(NS.DATASOURCE)) != null) {
                    dataSourceGenerator = functionCall;
                }
            } else if (NS.isThing(match)) {
                if (validateFunctionCall(context, functionCall, KLAB
                        .c(NS.OBJECTSOURCE)) != null) {
                    objectSourceGenerator = functionCall;
                }
            }
        }

        if (observables.size() > 0 && observables.get(0) != null
                && observables.get(0).getObservationType() == null) {
            if (observer != null) {
                ((ObservableSemantics) observables.get(0))
                        .setObservationType(observer.getObservationType());
                ((ObservableSemantics) observables.get(0)).setObserver(observer);
            } else if (NS.isDirect(observables.get(0).getType())) {
                ((ObservableSemantics) observables.get(0))
                        .setObservationType(KLAB.c(NS.DIRECT_OBSERVATION));
            }
        }

        return true;

    }

    /**
     * If the passed observable is in the outputs, return whether it is not tagged
     * 'optional'. Return false also if the output is not declared by the model.
     * 
     * @param observable
     * @return
     */
    public boolean isOutputRequired(IObservableSemantics observable) {
        for (int i = 1; i < observables.size(); i++) {
            KIMObservableSemantics obs = (KIMObservableSemantics) observables.get(i);
            if (obs.equals(observable)) {
                return !obs.isOptional();
            }
        }
        return false;
    }

    @Override
    protected IScale makeScale(IMonitor monitor) throws KlabException {
        if (this.scale == null) {
            IScale defined = KLAB.MFACTORY.createScale(getScaleGenerators(), monitor);
            if (defined != null && !defined.isEmpty()) {
                this.scale = defined;// .merge(this.scale,
                                     // LogicalConnector.INTERSECTION, true);
            }

            IScale dscale = null;
            if (dataSourceGenerator != null) {
                dscale = getDatasource(monitor).getCoverage();
            } else if (objectSourceGenerator != null) {
                dscale = getObjectSource(KLAB.ENGINE.getMonitor()).getCoverage();
            }

            if (dscale != null) {
                this.scale = this.scale == null ? dscale
                        : this.scale.merge(dscale, LogicalConnector.INTERSECTION, true);
            }
        }
        return scale;
    }

    private Collection<IFunctionCall> getScaleGenerators() {
        if (scaleGenerators.isEmpty() && observer != null
                && !((KIMObservingObject) observer).scaleGenerators.isEmpty()) {
            return ((KIMObservingObject) observer).scaleGenerators;
        }
        return scaleGenerators;
    }

    @Override
    public Class<?> getContextualizerClass() {
        return predefinedContextualizer == null
                ? (objectSourceGenerator == null ? super.getContextualizerClass()
                        : ISubjectInstantiator.class)
                : predefinedContextualizer.getClass();
    }

    @Override
    public IContextualizer getContextualizer(IResolutionScope scope, IProvenance.Artifact provenance, IMonitor monitor)
            throws KlabException {
        return predefinedContextualizer == null ? (objectSourceGenerator == null
                ? super.getContextualizer(scope, provenance, monitor)
                : getObjectSource(monitor).getInstantiator(getActions()))
                : predefinedContextualizer;
    }

    @Override
    public boolean hasContextualizer() {
        return predefinedContextualizer != null || objectSourceGenerator != null
                || super.hasContextualizer();
    }

    private boolean defineInstantiatorModel(KIMScope context, ModelStatement statement) {

        isInstantiator = true;

        for (ConceptDeclaration observable : statement.getObservable()) {
            KIMKnowledge ko = new KIMKnowledge(context
                    .get(KIMScope.Type.MODEL_OBSERVABLE), observable, null);
            if (!ko.isNothing()) {
                if (!NS.isObject(ko) && !NS.isRelationship(ko)) {
                    context.error("observables of instantiator models must be direct: subjects, events or processes", ko
                            .getFirstLineNumber());
                } else {
                    observables.add(new ObservableSemantics(ko.getType(), KLAB
                            .c(NS.DIRECT_OBSERVATION), id));
                }
            }
        }

        name(statement.getName() == null ? context.getNamespace().getNewObjectName(this)
                : statement.getName());

        if (statement.getAgentSource() != null) {
            objectSourceGenerator = new KIMFunctionCall(context
                    .get(KIMScope.Type.REIFICATION_FUNCTION_CALL), statement
                            .getAgentSource());
            validateFunctionCall(context, objectSourceGenerator, KLAB.c(NS.OBJECTSOURCE));
        }

        if (statement.getAttributeTranslators() != null) {
            for (org.integratedmodelling.kim.kim.AttributeTranslator at : statement
                    .getAttributeTranslators()) {

                IObserver obser = null;
                if (at.getObservers() != null) {
                    obser = defineObserver(context
                            .get(KIMScope.Type.OBSERVER_ATTRIBUTE), this, at
                                    .getObservers());
                    ((ObservableSemantics) obser.getObservable()).setObserver(obser);
                }

                if (at.getProperty() != null
                        && KLAB.KM.getProperty(at.getProperty()) == null) {
                    KLAB.error("can't translate property " + at.getProperty());
                    continue;
                }

                AttributeTranslator att = new AttributeTranslator();
                att.observer = obser;
                att.attribute = at.getAttributeId();
                att.property = at.getProperty() == null ? null : KLAB.p(at.getProperty());

                if (obser != null) {
                    /*
                     * FIXME/CHECK: Should these be contextualized to the type of the
                     * instantiated observable? This would prevent simple reference to
                     * states from within actions. Not doing it is an
                     * "open world assumption" thing. Still, must ensure that the derived
                     * qualities ARE contextual.
                     */
                    // try {
                    // Observables.contextualizeTo(obser.getObservable()
                    // .getType(), getObservable().getType());
                    // } catch (KlabException e) {
                    // context.error(e.getMessage(), lineNumber(at));
                    // }
                }

                attributeTranslators.add(att);

            }
        }

        return true;
    }

    @Override
    public List<IObservableSemantics> getObservables() {
        return observables;
    }

    @Override
    public IDataSource getDatasource(IMonitor monitor) throws KlabException {

        if (inlineValue != null) {
            return KLAB.MFACTORY.createConstantDataSource(inlineValue);
        }
        return dataSourceGenerator == null ? null
                : KLAB.MFACTORY.createDatasource(dataSourceGenerator, monitor);
    }

    @Override
    public IObjectSource getObjectSource(IMonitor monitor) throws KlabException {
        return objectSourceGenerator == null ? null
                : KLAB.MFACTORY.createObjectsource(objectSourceGenerator, monitor);
    }

    @Override
    public Collection<Pair<String, IObserver>> getAttributeObservers(boolean addInherency) {

        List<Pair<String, IObserver>> ret = new ArrayList<>();

        // if (NS.isCountable(getObservable())) {
        // ret.add(new Pair<>(IModelMetadata.PRESENCE_ATTRIBUTE, new
        // KIMPresenceObserver(getObservable())));
        // }

        for (AttributeTranslator t : attributeTranslators) {
            if (t.observer != null) {
                ret.add(new Pair<>(t.attribute, addInherency ? makeInherentObserver(t.observer) : t.observer));
            }
        }

        return ret;
    }

    private IObserver makeInherentObserver(IObserver observer2) {

        IObserver obs = ((KIMObserver) observer2).copy();
        ObservableSemantics obsm = new ObservableSemantics((ObservableSemantics) obs
                .getObservable());
        IConcept newtype;
        try {
            newtype = Observables.contextualizeTo(obsm.getType(), this.getType());
            obsm.setType(newtype);
            ((KIMObserver) obs).observable = obsm;
        } catch (KlabException e) {
            // should never happen as this is not called on models with errors.
        }

        return obs;
    }

    @Override
    public Collection<Pair<String, IProperty>> getAttributeMetadata() {

        List<Pair<String, IProperty>> ret = new ArrayList<>();

        for (AttributeTranslator t : attributeTranslators) {
            if (t.observer == null) {
                ret.add(new Pair<>(t.attribute, t.property));
            }
        }

        return ret;

    }

    @Override
    public IObserver getObserver() {
        return observer;
    }

    @Override
    public boolean isResolved() {
        return (hasDatasource() || hasObjectSource() || inlineValue != null) && dependencies.size() == 0
                && (observer == null || observer.getDependencies().size() == 0);
    }

    /**
     * Like isResolved() but admits having dependencies. Used to establish whether a
     * reinterpreting model should resolve its uninterpreted observable.
     * 
     * @return
     */
    public boolean hasData() {
        return hasDatasource() || hasObjectSource() || inlineValue != null || hasContextualizer()
                || hasActionsFor(IAction.Trigger.DEFINITION, IAction.Type.SET);
    }

    @Override
    public boolean isInstantiator() {
        return isInstantiator;
    }

    @Override
    public IObservableSemantics getObservable() {
        return observables.size() > 0 ? observables.get(0) : null;
    }

    @Override
    public String toString() {
        return "M/" + getName() + "/" + getObservable();
    }

    @Override
    public IConcept getType() {
        return getObservable().getType();
    }

    @Override
    public boolean isPrivate() {
        return isPrivate || super.isPrivate();
    }

    @Override
    public boolean isInactive() {
        return isInactive || super.isInactive();
    }

    @Override
    public void name(String id) {
        super.name(id);
        if (observer != null) {
            ((KIMObserver) observer).name(id);
        }
        if (getObservable() != null) {
            ((ObservableSemantics) getObservable()).setFormalName(id);
        }
    }

    @Override
    public Object getInlineValue() {
        return inlineValue;
    }

    public boolean hasDatasource() {
        return dataSourceGenerator != null;
    }

    public boolean hasObjectSource() {
        return objectSourceGenerator != null;
    }

    /**
     * Make a default model from an observable. Only works for class/trait and for direct
     * observers. If direct, select an instantiator.
     * 
     * @param context
     * @param oob
     * @return a new model
     */
    public static IModel makeModel(KIMScope context, KIMObservableSemantics oob) {

        if (NS.isDirect(oob)) {

            return new KIMModel(context.getNamespace(), oob);

        } else if (NS.isClass(oob)) {

            KIMClassificationObserver co = new KIMClassificationObserver(oob);
            oob.setObserver(co);
            co.namespace = context.getNamespace();
            return new KIMModel(co);

        } else {
            context.error("models can only be created without specifications for direct observables or classes/traits", oob
                    .getFirstLineNumber());
        }
        return null;
    }

    public IObserver getAttributeObserver(String dereifyingAttribute) {

        if (dereifyingAttribute.equals(IModelMetadata.PRESENCE_ATTRIBUTE)) {
            IObserver ret = new KIMPresenceObserver(this.getObservable());
            ret.setNamespace(getNamespace());
            return ret;
        }

        for (AttributeTranslator z : attributeTranslators) {
            if (z.attribute.equals(dereifyingAttribute)) {
                return z.observer;
            }
        }
        return null;
    }

    @Override
    public boolean isAvailable() {

        if (hasDatasource()) {
            try {
                return getDatasource(KLAB.ENGINE.getMonitor()).isAvailable();
            } catch (KlabException e) {
                return false;
            }
        } else if (hasObjectSource()) {
            try {
                return getObjectSource(KLAB.ENGINE.getMonitor()).isAvailable();
            } catch (KlabException e) {
                return false;
            }
        }

        return true;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public boolean isAbstract() {
        return getObservable() != null && ((ObservableSemantics) getObservable()).isAbstract();
    }

    @Override
    public boolean isReinterpreter() {
        return reinterpretAs != null;
    }

    public boolean acceptContextualizer(IContextualizer instantiator) {

        /*
         * we don't want another
         */
        if (hasContextualizer()) {
            return false;
        }

        /*
         * must fit our type
         */
        if (NS.isThing(getObservable())) {

        }

        predefinedContextualizer = instantiator;

        return true;
    }

    /**
     * Notify that the passed observable will have the passed role (or trait) in this
     * model.
     * 
     * @param observable
     * @param role
     */
    public void notifyRole(IObservableSemantics observable, IConcept role) {
        localRoles.add(new Pair<>(observable, role));
    }

    public Collection<? extends Pair<IObservableSemantics, IConcept>> getLocalRoles() {
        return localRoles;
    }

    @Override
    public String getNameFor(IObservableSemantics observable) {

        for (IObservableSemantics out : getObservables()) {
            if (out.getType().equals(observable.getType())) {
                return out.getFormalName();
            }
        }

        if (observer != null) {
            for (IDependency dep : observer.getDependencies()) {
                if (dep.getObservable().getType().equals(observable.getType())) {
                    return dep.getObservable().getFormalName();
                }
            }
        }

        for (IDependency dep : getDependencies()) {
            if (dep.getObservable().getType().equals(observable.getType())) {
                return dep.getObservable().getFormalName();
            }
        }

        return observable.getFormalName();
    }

    @Override
    public IDocumentation getDocumentation() {
        return documentation;
    }

    /**
     * Return all extent dimensions implied by the datasources and contextualization
     * statements. Must be called at the end of parsing.
     * 
     * @return
     */
    public Collection<ExtentDimension> getExtentDimensions() {

        Set<ExtentDimension> ret = new HashSet<>();

        checkFunctionExtent(dataSourceGenerator, ret);
        checkFunctionExtent(objectSourceGenerator, ret);
        for (IFunctionCall extent : scaleGenerators) {
            checkFunctionExtent(extent, ret);
        }
        if (observer != null) {
            for (IFunctionCall extent : ((KIMObserver) observer).scaleGenerators) {
                checkFunctionExtent(extent, ret);
            }
        }

        return ret;
    }

    private void checkFunctionExtent(IFunctionCall call, Set<ExtentDimension> ret) {

        if (call == null || call.getPrototype().getExecutorClass() == null) {
            return;
        }

        boolean candidate = true;
        if (!call.getPrototype().getExtentParameters().isEmpty()) {
            candidate = false;
            for (String p : call.getPrototype().getExtentParameters()) {
                if (call.getParameters().containsKey(p)) {
                    candidate = true;
                    break;
                }
            }
        }

        if (!candidate) {
            return;
        }

        if (IAreal.class.isAssignableFrom(call.getPrototype().getExecutorClass())) {
            ret.add(ExtentDimension.AREAL);
        } else if (ITemporal.class.isAssignableFrom(call.getPrototype().getExecutorClass())) {
            ret.add(ExtentDimension.TEMPORAL);
        } else if (IPuntal.class.isAssignableFrom(call.getPrototype().getExecutorClass())) {
            ret.add(ExtentDimension.PUNTAL);
        } else if (ILineal.class.isAssignableFrom(call.getPrototype().getExecutorClass())) {
            ret.add(ExtentDimension.LINEAL);
        } else if (IVolumetric.class.isAssignableFrom(call.getPrototype().getExecutorClass())) {
            ret.add(ExtentDimension.VOLUMETRIC);
        }
    }

    /**
     * Return the uninterpreted observable in any model that reinterprets through a role.
     * 
     * @return
     */
    public IObservableSemantics getUninterpretedObservable() {
        return uninterpretedObservable;
    }

    public IConcept getAddedRole() {
        return reinterpretAs;
    }

}
