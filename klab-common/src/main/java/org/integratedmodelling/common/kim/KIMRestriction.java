/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.OWL;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.kim.kim.RestrictionDefinition;
import org.integratedmodelling.lang.Axiom;

public class KIMRestriction extends KIMModelObject {

    public KIMRestriction(KIMScope context, RestrictionDefinition statement, IModelObject parent) {

        super(context, statement, parent);

        String type = getRelationshipType(context);
        boolean functional = false;

        if (type.equals("requires")) {
            type = "has";
            functional = true;
        }

        IKnowledgeObject rootConcept = (IKnowledgeObject) context.get(IKnowledgeObject.class);
        String id = rootConcept.getId();

        IKnowledgeObject property = null;
        IKnowledgeObject concept = null;
        IConcept forconcept = null;
        String pcand = null;

        /*
         * may be a concept (which needs to exist) or a property (lowercase first letter
         * if not existing, or existing). If concept, create property based on concept
         * type.
         */
        String pp = statement.getSource();

        /*
         * if it's not a suitable concept ID, skip this step.
         */
        if (isSuitableConceptId(pp)) {
            concept = new KIMKnowledge(context.get(KIMScope.Type.CONCEPT), pp, false, getFirstLineNumber());
        } else {
            pcand = pp;
        }

        if (statement.getSubject() != null && isSuitableConceptId(statement.getSubject())) {
            IKnowledgeObject fc = new KIMKnowledge(context.get(KIMScope.Type.CONCEPT), statement
                    .getSubject(), false, getFirstLineNumber());
            if (!fc.isNothing()) {
                forconcept = (IConcept) fc.getType();
            }
        } else if (statement.getSubject() != null && isSuitablePropertyId(statement.getSubject())) {
            IKnowledgeObject fc = new KIMKnowledge(context.get(KIMScope.Type.PROPERTY), statement
                    .getSubject(), true, getFirstLineNumber());
            if (!fc.isNothing()) {
                property = fc;
            }
        }

        if (concept != null && property == null && !concept.isNothing()) {

            IConcept main = context.getNamespace().getOntology().getConcept(id);
            IProperty prop = null;
            // name of property if we have to create it.
            String propName = type + concept.getId();

            /*
             * find property. Check if we're specializing an existing restriction first.
             */
            if (forconcept != null) {
                prop = OWL.getRestrictingProperty(main, forconcept);
                if (prop != null) {
                    property = new KIMKnowledge(context, prop);
                }
            } else {
                prop = context.getNamespace().getOntology().getProperty(propName);
            }

            if (prop == null) {

                /*
                 * create property, using concept type and relationship type as a guide
                 * for inheritance. Only creating object restrictions. Use an appropriate
                 * super property so we can distinguish these.
                 */
                context.getNamespace().addAxiom(Axiom.ObjectPropertyAssertion(propName));
                context.getNamespace().addAxiom(Axiom
                        .SubObjectProperty(getRelationshipSupertype(context).toString(), propName));
                context.getNamespace()
                        .addAxiom(Axiom.ObjectPropertyDomain(propName, rootConcept.getName()));
                context.getNamespace()
                        .addAxiom(Axiom.ObjectPropertyRange(propName, concept.getId()));

                if (functional) {
                    context.getNamespace()
                            .addAxiom(Axiom.FunctionalObjectProperty(propName));
                }

                // it's an inverse part-of or an inherency.
                if (type.equals("contains")) {
                    //
                } else {
                    //
                }

                context.synchronize();
                
                prop = context.getNamespace().getOntology().getProperty(propName);
                property = new KIMKnowledge(context, prop);

            }

            if (property == null) {
                IKnowledgeObject fc = new KIMKnowledge(context
                        .get(KIMScope.Type.PROPERTY), propName, false, getFirstLineNumber());
                if (!fc.isNothing()) {
                    property = fc;
                }
            }
        }

        if (property /* still */ == null && isSuitablePropertyId(pcand)) {
            IKnowledgeObject fc = new KIMKnowledge(context
                    .get(KIMScope.Type.PROPERTY), pcand, false, getFirstLineNumber());
            if (!fc.isNothing()) {
                property = fc;
            }
        }

        if (property == null || property.isNothing() || concept == null || concept.isNothing()) {
            context.error("restriction uses unknown concepts or properties", lineNumber(statement));
            return;
        }

        if (statement.isAtLeast()) {

            context.getNamespace()
                    .addAxiom(Axiom.AtLeastNValuesFrom(id, property.getName(), concept
                            .getName(), statement.getHowmany().getInt()));

        } else if (statement.isAtMost()) {

            context.getNamespace()
                    .addAxiom(Axiom.AtMostNValuesFrom(id, property.getName(), concept
                            .getName(), statement.getHowmany().getInt()));

        } else if (statement.isExactly()) {

            context.getNamespace()
                    .addAxiom(Axiom.ExactlyNValuesFrom(id, property.getName(), concept
                            .getName(), statement.getHowmany().getInt()));

        } else if (statement.isNone()) {

            context.getNamespace()
                    .addAxiom(Axiom.NoValuesFrom(id, property.getName(), concept
                            .getName()));

        } else if (statement.isOnly()) {

            /*
             * all values from
             */
            context.getNamespace().addAxiom(Axiom.AllValuesFrom(id, property.getName(), concept
                    .getName()));

        } else {

            /*
             * some values from if not quantified in other ways
             */
            context.getNamespace()
                    .addAxiom(Axiom.SomeValuesFrom(id, property.getName(), concept
                            .getName()));
        }

        context.synchronize();
    }

    private String getRelationshipType(KIMScope context) {
        if (context.isInScope(KIMScope.Type.USES_RELATIONSHIP)) {
            return "uses";
        }
        if (context.isInScope(KIMScope.Type.HAS_RELATIONSHIP)) {
            return "has";
        }
        if (context.isInScope(KIMScope.Type.CONTAINS_RELATIONSHIP)) {
            return "contains";
        }
        if (context.isInScope(KIMScope.Type.IMPLIES_RELATIONSHIP)) {
            return "implies";
        }
        return "with";
    }

    private IProperty getRelationshipSupertype(KIMScope context) {

        if (context.isInScope(KIMScope.Type.USES_RELATIONSHIP)) {
            return KLAB.p(NS.DEPENDS_ON_PROPERTY);
        }
        if (context.isInScope(KIMScope.Type.HAS_RELATIONSHIP)) {
            return KLAB.p(NS.RELATES_TO_PROPERTY);
        }
        if (context.isInScope(KIMScope.Type.CONTAINS_RELATIONSHIP)) {
            /*
             * TODO if objects are spatial, use spatial counterpart.
             */
            return KLAB.p(NS.CONTAINS_PART_PROPERTY);
        }
        if (context.isInScope(KIMScope.Type.IMPLIES_RELATIONSHIP)) {
            return KLAB.p(NS.IMPLIES_ROLE_PROPERTY);
        }
        return KLAB.p(NS.RELATES_TO_PROPERTY);
    }

}
