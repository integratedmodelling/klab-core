/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.lang.IParsingScope;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.kim.expr.Validator;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.common.vocabulary.Roles;
import org.integratedmodelling.common.vocabulary.Traits;
import org.integratedmodelling.common.vocabulary.Types;
import org.integratedmodelling.common.vocabulary.authority.AuthorityFactory;
import org.integratedmodelling.common.vocabulary.authority.BaseAuthority;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Annotation;
import org.integratedmodelling.kim.kim.ConceptDeclaration;
import org.integratedmodelling.kim.kim.ConceptIdentifier;
import org.integratedmodelling.kim.kim.ConceptStatement;
import org.integratedmodelling.kim.kim.PropertyStatement;
import org.integratedmodelling.kim.kim.RestrictionDefinition;
import org.integratedmodelling.kim.kim.RestrictionStatement;
import org.integratedmodelling.lang.Axiom;

public class KIMKnowledge extends KIMModelObject implements IKnowledgeObject {

    // knowledge has errors or inconsistencies
    boolean            isNothing;

    // knowledge has been explicitly declared to be owl:Nothing
    boolean            isOwlNothing;

    IConcept           knowledge;
    /*
     * only instantiated when concept is a relationship
     */
    IProperty          property;
    boolean            isNegated;
    boolean            isDeniable;
    IModel             model;
    // int detailLevel = -1;
    IDirectObserver    directObserver;
    IConcept           inherentType;
    IConcept           contextType;
    IConcept           byTrait;
    IConcept           downTo;
    boolean            isWorldviewCompatible = false;

    /*
     * if allowed to (by using a specific constructor), this will contain a list of
     * unresolved object names.
     */
    List<String>       objectDeclarations    = new ArrayList<>();

    static Set<String> traitKeywords         = new HashSet<>();

    static {
        traitKeywords.add(KIM.ATTRIBUTE_CONCEPT);
        traitKeywords.add(KIM.REALM_CONCEPT);
        traitKeywords.add(KIM.IDENTITY_CONCEPT);
    }

    /**
     * This creates references to knowledge, potentially external. The namespace for the
     * model object is the one being parsed even if knowledge comes from outside.
     * 
     * @param context
     * @param knowledge
     */
    public KIMKnowledge(KIMScope context, IConcept knowledge) {
        super(context, null, null);
        this.knowledge = knowledge;
    }

    /**
     * This creates references to knowledge, potentially external. The namespace for the
     * model object is the one being parsed even if knowledge comes from outside.
     * 
     * @param context
     * @param knowledge
     */
    public KIMKnowledge(KIMScope context, IProperty knowledge) {
        super(context, null, null);
        this.knowledge = knowledge.getType();
        this.property = knowledge;
    }

    /**
     * This creates references to knowledge, potentially external. The namespace for the
     * model object is the one being parsed even if knowledge comes from outside.
     * 
     * @param context
     * @param knowledge
     */
    public KIMKnowledge(KIMScope context, IConcept knowledge, IModelObject parent,
            int lineNumber) {
        super(context, null, parent);
        this.knowledge = knowledge;
        this.property = KLAB.KM.getProperty(knowledge);
    }

    /**
     * Matches ID or optionally creates in namespace. Context to define what is wanted -
     * ensure it's either CONCEPT or PROPERTY.
     * 
     * @param context
     * @param id
     * @param createIfAbsent
     * @param lineNumber
     */
    public KIMKnowledge(KIMScope context, String id, boolean createIfAbsent,
            int lineNumber) {

        super(context, null, null);

        boolean external = id.contains(":");
        IKnowledge existing = null;
        if (external) {
            existing = KLAB.KM.getKnowledge(id);
        } else {
            if (context.is(KIMScope.Type.PROPERTY)) {
                existing = context.getNamespace().getOntology().getProperty(id);
            } else if (context.is(KIMScope.Type.CONCEPT)) {
                existing = context.getNamespace().getOntology().getConcept(id);
            }
        }

        if (external && existing == null) {
            isNothing = true;
            return;
        }

        if (external) {
            context.getNamespace().checkImported(context, existing, lineNumber);
        }

        if (context.is(KIMScope.Type.PROPERTY)) {

            if (existing instanceof IProperty) {
                property = (IProperty) existing;
            } else if (existing == null && createIfAbsent) {
                property = ((Ontology) context.getNamespace().getOntology())
                        .createProperty(id, false);
            } else {
                isNothing = true;
            }
        } else if (context.is(KIMScope.Type.CONCEPT)) {

            if (existing instanceof IConcept) {
                knowledge = (IConcept) existing;
            } else if (existing == null && createIfAbsent) {
                ((Ontology) context.getNamespace().getOntology()).createConcept(id);
            } else {
                isNothing = true;
            }
        } else {
            isNothing = true;
        }

    }

    public KIMKnowledge(KIMScope context, PropertyStatement propertyStatement,
            IModelObject parent,
            List<Annotation> annotations) {

        /**
         * TODO FIXME implement properly
         */

        super(context, propertyStatement, parent, annotations);

        String id = propertyStatement.getId();

        if (id == null) {
            return;
        }

        boolean isObject = !(propertyStatement.isData()
                || propertyStatement.isAnnotation());
        knowledge = declare(context, propertyStatement, propertyStatement.getId());

        // if (!id.contains(":")) {
        //
        // if (propertyStatement.getModifiers() != null) {
        //
        // for (PropertyModifier pm : propertyStatement.getModifiers()
        // .getModifier()) {
        //
        // /*
        // * TODO these are subproperties!
        // */
        // if (pm.getValue() == PropertyModifier.FUNCTIONAL_VALUE) {
        // context.getNamespace()
        // .addAxiom(isObject ? Axiom.FunctionalObjectProperty(id)
        // : Axiom.FunctionalDataProperty(id));
        // }
        // /*
        // * TODO the rest
        // */
        // }
        // }
        //
        // } else {
        // if (propertyStatement.getModifiers() != null) {
        // context.error("cannot apply modifiers to an external property",
        // lineNumber(propertyStatement));
        // }
        // }
        //
        if (propertyStatement.isAbstract()) {
            context.getNamespace()
                    .addAxiom(Axiom.AnnotationAssertion(id, NS.IS_ABSTRACT, "true"));
        }

        if (propertyStatement.getParents() != null) {
            for (IKnowledgeObject co : new KIMPropertyList(context
                    .get(KIMScope.Type.PARENT_LIST), propertyStatement, propertyStatement
                            .getParents(), null)) {
                context.getNamespace()
                        .addAxiom(isObject ? Axiom.SubObjectProperty(co.getName(), id)
                                : Axiom.SubDataProperty(co.getName(), id));
            }
        }

        /*
         * children
         */
        if (propertyStatement.getChildren() != null) {
            ArrayList<String> children = new ArrayList<>();
            for (IKnowledgeObject co : new KIMPropertyList(context
                    .get(KIMScope.Type.CHILD_LIST), propertyStatement, propertyStatement
                            .getChildren(), null)) {
                if (co != null) {

                    context.getNamespace()
                            .addAxiom(isObject ? Axiom.SubObjectProperty(id, co.getId())
                                    : Axiom.SubDataProperty(id, co.getId()));
                    children.add(co.getId());

                    /*
                     * record declaration hierarchy FIXME redundant with new constructors
                     * - check behavior.
                     */
                    ((KIMKnowledge) co).parent = this;
                    this.children.add(co);
                }
            }
        }

        if (propertyStatement.getDomain() != null) {
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMScope.Type.PROPERTY_DOMAIN), propertyStatement
                            .getDomain(), null)) {
                context.getNamespace()
                        .addAxiom(Axiom.ObjectPropertyDomain(id, co.getName()));
            }
        }

        if (propertyStatement.getRange() != null) {
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMScope.Type.PROPERTY_RANGE), propertyStatement
                            .getRange(), null)) {
                context.getNamespace()
                        .addAxiom(Axiom.ObjectPropertyRange(id, co.getName()));
            }
        }

        /*
         * TODO the rest
         */
    }

    public KIMKnowledge(KIMScope context, ConceptDeclaration statement) {
        this(context, statement, null);
    }

    public KIMKnowledge(KIMScope context, ConceptDeclaration statement,
            IModelObject parent) {
        this(context, statement, parent, false);
    }

    /**
     * If allowObjects is true, won't complain for anything undefined and just set a list
     * of names declared.
     * 
     * @param context
     * @param statement
     * @param parent
     * @param allowObjects
     */
    public KIMKnowledge(KIMScope context, ConceptDeclaration statement,
            IModelObject parent, boolean allowObjects) {

        super(context, statement, parent, new ArrayList<Annotation>());

        this.knowledge = declare(context, statement, allowObjects);

        if (objectDeclarations.size() > 0) {
            return;
        }

    }

    public KIMKnowledge(KIMScope context, ConceptStatement statement,
            IModelObject parent) {
        this(context, statement, parent, new ArrayList<Annotation>());
    }

    public KIMKnowledge(KIMScope context, PropertyStatement statement,
            IModelObject parent) {
        this(context, statement, parent, new ArrayList<Annotation>());
    }

    public KIMKnowledge(KIMScope context, ConceptStatement statement, IModelObject parent,
            List<Annotation> annotations) {

        super(context, statement, parent, annotations);

        if (statement.getConcept() != null) {
            context.setCoreConcept(KIM.getCoreConceptFor(statement.getConcept()));
        }

        IConcept coreConcept = context.isInScope(KIMScope.Type.TRAIT_LIST)
                ? KLAB.c(NS.CORE_TRAIT)
                : context.getCoreConcept();

        // happens in error
        if (statement == null
                || (!statement.isRoot() && statement.getDeclaration() == null))
            return;

        /*
         * this gets filled if we encounter an 'identified as' and we're not an identity
         * used as an alias.
         */
        Set<IConcept> identities = new HashSet<>();

        if (statement.getAuthority() != null) {

            boolean stop = false;

            /*
             * if it's an authority-identify concept, we ensure it's an identity (or
             * whatever the authority requires) and alias it in the ontology so that any
             * reasoning involving the concept is actually done on the stable identity.
             */
            if (statement.getAuthority() != null && coreConcept != null) {

                IAuthority<?> auth = null;
                try {
                    auth = AuthorityFactory.get().getAuthorityView(statement.getAuthority());
                } catch (KlabValidationException e1) {
                    context.error(e1.getMessage(), lineNumber(statement));
                }
                if (auth == null) {
                    context.error("no authority named " + statement.getAuthority()
                            + " has been configured", lineNumber(statement));
                } else {
                    try {

                        String error = auth
                                .validateIdentifiedConcept(coreConcept, statement.getIdentifier());

                        if (error != null) {

                            context.error(error, lineNumber(statement));

                        } else {

                            IConcept identity = auth
                                    .getIdentity(statement.getIdentifier(), statement.getAuthority());
                            if (identity != null && identity
                                    .getLocalName() != null /*
                                                             * the latter may happen when
                                                             * the ID is screwed up, the
                                                             * user should know
                                                             */) {

                                /*
                                 * ARGH! If we're DECLARING THE IDENTITY, this is an
                                 * alias; otherwise, it's using the identity as a TRAIT
                                 * for the main concept.
                                 */
                                if (NS.isIdentity(coreConcept)) {
                                    context.getNamespace().registerAuthority(identity);
                                    ((Ontology) context.getNamespace().getOntology())
                                            .addDelegateConcept(statement
                                                    .getDeclaration(), context
                                                            .getNamespace(), identity);
                                    knowledge = identity;
                                    stop = true;
                                } else {
                                    identities.add(identity);
                                }

                            } else {

                                context.error("authority " + auth.getAuthorityId()
                                        + " does not recognize identifier "
                                        + statement.getIdentifier(), lineNumber(statement));
                            }
                        }
                    } catch (Exception e) {
                        context.error(e.getMessage(), lineNumber(statement));
                    }
                }

                if (stop) {
                    return;
                }
            }
        }

        if (statement.isRoot()) {
            knowledge = KLAB.c(NS.CORE_DOMAIN);
        } else if (statement.isRedeclaration()) {
            knowledge = new KIMKnowledge(context, statement.getRedeclared())
                    .getType();
        } else {
            knowledge = declare(context, Collections
                    .singleton(statement.getDeclaration()), lineNumber(statement));
        }

        if (knowledge == null) {
            return;
        }

        if (statement.isNothing()) {
            // knowledge = Env.KM.getNothing();
            this.isOwlNothing = true;
        }

        boolean isOrdering = false;
        boolean isSubjective = false;
        boolean isFunctional = false;
        boolean isStructural = false;
        boolean isUnidirectional = false;
        boolean isBidirectional = false;
        boolean needsSuperclass = false;
        boolean isPrimary = statement.getConcept() != null;

        IConcept cconcept = null;

        if (statement.isRoot()) {
            if (!statement.getConcept().equals("domain")) {
                context.error("root is only allowed in core domain specifications: "
                        + knowledge, lineNumber(statement));
                return;
            }
            if (context.getNamespace().getDomain() == null
                    || !context.getNamespace().getDomain().is(KLAB.c(NS.CORE_DOMAIN))) {
                context.error("domain specifications are only allowed in the core domain: "
                        + knowledge, lineNumber(statement));
                return;
            }

            cconcept = coreConcept = KLAB.c(NS.CORE_DOMAIN);

        } else {
            cconcept = this.getConcept();
        }

        String id = knowledge.toString();
        if (id.startsWith(context.getNamespace().getId() + ":")) {
            id = knowledge.getLocalName();
        }

        boolean isExternal = !statement.isRoot() && id.contains(":") && !isOwlNothing;

        if (isExternal && cconcept == null) {
            context.error("cannot redefine unknown external concept "
                    + knowledge, lineNumber(statement));
            return;
        }

        if (coreConcept == null) {

            if (isExternal) {
                coreConcept = (IConcept) NS.getCoreType(cconcept);
            } else {

                String key = statement.getConcept();
                if (key == null) {
                    context.error("internal: semantic key expected", lineNumber(statement));
                } else {
                    coreConcept = KIM.getCoreConceptFor(key);
                }
                if (coreConcept == null) {
                    context.error("internal: cannot resolve core concept at "
                            + key, lineNumber(statement));
                }
            }
        }

        /*
         * add to ontology
         */
        if (!isExternal) {

            /*
             * set as child of whatever concept is implied by the keyword used to declare
             * ACHTUNG: this depends on the keys being the same as the language keywords
             * in IResolver.
             */
            String key = statement.getConcept();
            String mod = statement.getAgentSpecifier();
            List<String> pmo = statement.getPropertySpecifiers();
            /*
             * Modify according to modifiers, for now only supported for agent types.
             */
            if (key != null) {

                isOrdering = key.equals(KIM.ORDERING_CONCEPT);
                isSubjective = mod != null && mod.equals(KIM.SUBJECTIVE_SPECIFIER);

                isFunctional = pmo != null && pmo.contains(KIM.FUNCTIONAL_SPECIFIER);
                isBidirectional = pmo != null
                        && pmo.contains(KIM.BIDIRECTIONAL_SPECIFIER);
                isUnidirectional = pmo != null
                        && pmo.contains(KIM.UNIDIRECTIONAL_SPECIFIER);
                isStructural = pmo != null && pmo.contains(KIM.STRUCTURAL_SPECIFIER);

                // simplify handling below
                if (isSubjective) {
                    if (!(key.equals(KIM.ATTRIBUTE_CONCEPT)
                            || key.equals(KIM.ORDERING_CONCEPT))) {
                        context.error("only attributes and ordering traits can use the 'subjective' modifier", lineNumber(statement));
                    }
                    mod = null;
                    key = KIM.SUBJECTIVE_SPECIFIER;
                }

                if (mod != null && !key.equals(KIM.AGENT_CONCEPT)) {
                    context.error("only agent concepts can use the modifier "
                            + mod, lineNumber(statement));
                } else if (mod != null) {
                    key = mod + "-" + key;
                }

                if (isFunctional || isBidirectional || isUnidirectional || isStructural) {
                    if (!key.equals(KIM.RELATIONSHIP_CONCEPT)) {
                        context.error("only relationship concepts can use modifier "
                                + org.integratedmodelling.common.utils.StringUtils
                                        .joinCollection(pmo, ','), lineNumber(statement));
                    }
                    if (isFunctional) {
                        key = "functional-" + key;
                        coreConcept = KLAB.c(NS.CORE_FUNCTIONAL_RELATIONSHIP);
                    } else if (isStructural) {
                        key = "structural-" + key;
                        coreConcept = KLAB.c(NS.CORE_STRUCTURAL_RELATIONSHIP);
                    }
                }

                if (coreConcept != null && cconcept != null
                        && !cconcept.is(coreConcept)) {

                    /*
                     * ensure it isn't redeclaring a different kind of observable.
                     */
                    KlabException exc = Validator.ensureCompatible(cconcept, coreConcept);
                    if (exc != null) {
                        context.error(exc.getMessage(), lineNumber(statement));
                    }

                    needsSuperclass = true;

                }

                if (!statement.isRoot()) {

                    context.getNamespace().addAxiom(Axiom
                            .AnnotationAssertion(id, NS.BASE_DECLARATION, "true"));

                    /*
                     * if trait, create the appropriate subproperty to restrict when used.
                     * Subjective traits always use NS.HAS_SUBJECTIVE_TRAIT_PROPERTY.
                     */
                    if (isPrimary && coreConcept != null) {
                        String pName = null;
                        String pProp = null;
                        if (coreConcept.equals(KLAB.c(NS.ATTRIBUTE_TRAIT))) {
                            // hasX
                            pName = "has" + id;
                            pProp = NS.HAS_ATTRIBUTE_PROPERTY;
                        } else if (coreConcept.equals(KLAB.c(NS.CORE_REALM_TRAIT))) {
                            // inX
                            pName = "in" + id;
                            pProp = NS.HAS_REALM_PROPERTY;
                        } else if (coreConcept.equals(KLAB.c(NS.CORE_IDENTITY_TRAIT))) {
                            // isX
                            pName = "is" + id;
                            pProp = NS.HAS_IDENTITY_PROPERTY;
                        }
                        if (pName != null) {
                            context.getNamespace()
                                    .addAxiom(Axiom.ObjectPropertyAssertion(pName));
                            context.getNamespace()
                                    .addAxiom(Axiom.ObjectPropertyRange(pName, id));
                            context.getNamespace()
                                    .addAxiom(Axiom.SubObjectProperty(pProp, pName));
                            context.getNamespace().addAxiom(Axiom
                                    .AnnotationAssertion(id, NS.TRAIT_RESTRICTING_PROPERTY, context
                                            .getNamespace().getId() + ":" + pName));
                            context.synchronize();
                        }
                    }

                }

            }
        } else {

            KlabException exc = Validator.ensureCompatible(cconcept, coreConcept);
            if (exc != null) {
                context.error(exc.getMessage(), lineNumber(statement));
            }
            context.getNamespace()
                    .checkImported(context, knowledge, lineNumber(statement));
        }

        /*
         * modifiers
         */
        if (statement.isAbstract()) {

            if (isExternal) {
                context.error("modifiers are illegal on external concepts", lineNumber(statement));
                return;
            }
            context.getNamespace()
                    .addAxiom(Axiom.AnnotationAssertion(id, NS.IS_ABSTRACT, "true"));
        }

        context.synchronize();

        /*
         * parents
         */
        if (statement.getParents() != null) {

            if (isExternal) {
                context.error("parents are illegal on external concepts", lineNumber(statement));
                return;
            }

            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMScope.Type.PARENT_LIST), statement.getParents(), null)) {

                if (NS.isNothing(co)) {

                    if (knowledge != null) {
                        NS.registerNothingConcept(knowledge.toString());
                    }
                    if (statement.getDocstring() == null) {
                        context.getNamespace().addAxiom(Axiom
                                .AnnotationAssertion(id, IMetadata.DC_COMMENT, "this concept derives from an inconsistent ancestor and cannot be used."));
                    }
                }

                /*
                 * check for proper inheritance
                 */
                IConcept c = co.getConcept();
                IConcept core = (IConcept) NS.getCoreType(c);
                if (!Validator.checkParent(core, coreConcept)) {
                    context.error("cannot make "
                            + (core == null ? id : ("a " + coreConcept)) + " a child of "
                            + (core == null ? ("the partially defined concept " + c)
                                    : core.toString()), lineNumber(statement
                                            .getParents()));
                } else {

                    context.getNamespace().addAxiom(Axiom.SubClass(co.getName(), id));

                    /*
                     * don't add the core superclass if any of the parents already has it.
                     */
                    if (c.is(coreConcept)) {
                        needsSuperclass = false;
                    }

                    /*
                     * if this comes from an authority and we provide the alias for it, we
                     * want the user alias as an annotation for display.
                     */
                    if (c.getMetadata().contains(NS.AUTHORITY_ID_PROPERTY)) {
                        c.getOntology().define(Collections.singletonList(Axiom
                                .AnnotationAssertion(c.getLocalName(), NS.DISPLAY_LABEL_PROPERTY, id)));
                    }
                }
            }
        }

        if (needsSuperclass && coreConcept != null) {
            context.getNamespace().addAxiom(Axiom.SubClass(coreConcept.toString(), id));
        }

        if (isSubjective) {
            context.getNamespace()
                    .addAxiom(Axiom.AnnotationAssertion(id, NS.IS_SUBJECTIVE, "true"));
        }

        context.synchronize();

        boolean isRelationship = coreConcept != null
                && coreConcept.is(KLAB.c(NS.CORE_RELATIONSHIP));
        int conTraits = 0;

        if (statement.getRange() != null) {

            if (!isRelationship) {
                context.error("only relationships can specify range concepts", lineNumber(statement
                        .getRange()));
            }
        }
        if (statement.getDomain() != null) {
            if (!isRelationship) {
                context.error("only relationships can specify domain concepts", lineNumber(statement
                        .getDomain()));
            }
        }
        if (statement.getInverse() != null) {
            if (!isRelationship) {
                context.error("only relationships can specify their inverse", lineNumber(statement));
            }
        }

        /*
         * contextualized traits - only for classes and allowed only on the primary
         * concept spec.
         */
        if (statement.getContextualizedTraits() != null) {

            if (isExternal) {
                context.error("exposing traits is illegal on external concepts", lineNumber(statement
                        .getContextualizedTraits()));
                return;
            }

            if (coreConcept == null || !coreConcept.is(KLAB.c(NS.TYPE))) {
                context.error("only classes can use the 'exposing/exposes' notation", lineNumber(statement
                        .getContextualizedTraits()));
                return;
            }

            // /*
            // * check that no ancestors expose any traits.
            // */
            // IConcept anc = NS
            // .getRootType(context.getNamespace().getOntology().getConcept(id));
            // if (!statement.isSpecific() && anc != null
            // && !anc.equals(context.getNamespace().getOntology().getConcept(id))) {
            // context.error("traits are already exposed by parent concept " + anc
            // + ": only one class in a hierarchy can expose traits", lineNumber(statement
            // .getContextualizedTraits()));
            // return;
            // }
            //
            // /**
            // * reset static contextualized trait index in reasoner
            // */
            // NS.resetContextualizedTraits(context.getNamespace().getOntology()
            // .getConcept(id));

            List<IConcept> exposedTraits = null;
            List<IConcept> adoptedTraits = null;

            if (statement.isSpecific()) {
                exposedTraits = new ArrayList<>(Types
                        .getExposedTraits(knowledge));
                if (exposedTraits.isEmpty()) {
                    context.error("no traits to adopt have been declared in parent class (using an 'exposes' statement)", lineNumber(statement
                            .getContextualizedTraits()));
                    return;
                }
                adoptedTraits = new ArrayList<>();
            } else {
                exposedTraits = new ArrayList<>();
            }

            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(statement.isSpecific() ? KIMScope.Type.ADOPTED_EXPOSED_TRAIT_LIST
                            : KIMScope.Type.EXPOSED_TRAIT_LIST), statement
                                    .getContextualizedTraits(), null)) {

                IConcept t = co.getType();

                if (t == null) {
                    continue;
                }

                if (NS.isObservable(t)) {

                    /*
                     * turn observable to trait...
                     */
                    t = Traits.getTraitFor(t);

                    if (statement.isSpecific()) {
                        // FIXME something sketchy in the reassignment to t
                        t = Traits.getConcreteObservabilityTrait(t, co
                                .isNegated());
                    } else if (co.isNegated()) {
                        // don't think the syntax allows this anyway
                        context.error(co.getId()
                                + ": cannot negate an abstract observability trait: negations can only be used in concrete subclasses", co
                                        .getFirstLineNumber());
                    }

                    // set back into knowledge object because we'll build an
                    // observable
                    // with the rest of the
                    // info.
                    ((KIMKnowledge) co).knowledge = t;

                } else if (!NS.isTrait(t)) {
                    context.error(co.getId()
                            + ": only traits or observables are allowed here", co
                                    .getFirstLineNumber());
                    continue;
                } else {
                    if (co.isNegated()) {
                        try {
                            t = Traits.getNegation(t);
                        } catch (KlabValidationException e) {
                            context.error(e.getMessage(), co.getFirstLineNumber());
                            continue;
                        }
                    }
                }

                if (!statement.isSpecific()) {

                    if (!t.isAbstract()) {
                        context.error(co.getId()
                                + ": the traits exposed by a classification must be abstract", co
                                        .getFirstLineNumber());
                        continue;
                    }

                    exposedTraits.add(t);

                } else {

                    if (!((KIMKnowledge) co).knowledge.isAbstract() && t.isAbstract()) {
                        context.error(co.getId()
                                + ": the traits adopted by a concrete class must be concrete", co
                                        .getFirstLineNumber());
                        continue;
                    }

                    int nMatch = 0;
                    int i = 0;
                    int thm = -1;
                    String mtchdes = "";
                    IConcept thr = null;
                    for (IConcept o : exposedTraits) {
                        if (Types.isExposedAs(t, o)) {
                            nMatch++;
                            thr = o;
                            thm = i;
                            mtchdes += (mtchdes.isEmpty() ? "" : ", ") + o;
                        }
                        i++;
                    }

                    if (nMatch == 0) {
                        context.error("no matching exposed trait for "
                                + co.getConcept(), co
                                        .getFirstLineNumber());
                        continue;
                    } else if (nMatch > 1) {
                        context.error("ambiguous exposed trait for " + co.getConcept()
                                + ": can match "
                                + mtchdes, co.getFirstLineNumber());
                        continue;
                    }

                    /*
                     * negation may be not null. Must use an observable
                     */
                    adoptedTraits.add(t);
                }
            }

            /*
             * don't reverse this - exposed traits are non null also when adopted traits
             * are defined.
             */
            if (adoptedTraits != null) {
                Types.setExposedTraits(knowledge, adoptedTraits);
            } else if (exposedTraits != null) {
                Types.setExposedTraits(knowledge, exposedTraits);
            }

        } else if (!isExternal && coreConcept != null && coreConcept.is(KLAB.c(NS.TYPE))
                && !knowledge.isAbstract()) {
            context.error("concrete class concepts must adopt ('with') one or more traits exposed ('exposes') by a superclass", lineNumber(statement));
        }

        context.synchronize();

        List<IConcept> applicables = new ArrayList<>();

        /**
         * Must be a trait: defines which (existing) observables it applies to.
         */
        if (statement.getTraitTargets() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getTraitTargets()));
                return;
            }

            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMScope.Type.TARGET_TRAIT_LIST), statement
                            .getTraitTargets(), null)) {
                applicables.add(co.getConcept());
            }

            try {
                Observables
                        .setApplicableObservables(knowledge, applicables);
            } catch (KlabValidationException e) {
                context.error(e
                        .getMessage(), lineNumber(statement.getTraitTargets()));
            }

            /*
             * these are only allowed for relationships
             */
            IConcept linkFrom = statement.getLinkFrom() == null ? null
                    : new KIMKnowledge(context, statement.getLinkFrom())
                            .getType();

            IConcept linkTo = statement.getLinkFrom() == null ? null
                    : new KIMKnowledge(context, statement.getLinkTo())
                            .getType();

            if (linkFrom != null && linkTo != null) {
                if (coreConcept == null || !NS.isRole(coreConcept) || !NS.isRelationship(applicables)) {

                    context.error("relationship source/target restrictions are only allowed in roles that apply to relationships", lineNumber(statement
                            .getLinkFrom()));
                }

                try {
                    Observables
                            .setApplicableSource(knowledge, linkFrom);

                } catch (KlabValidationException e) {
                    context.error(e
                            .getMessage(), lineNumber(statement.getLinkFrom()));
                }
                try {
                    Observables
                            .setApplicableDestination(knowledge, linkTo);
                } catch (KlabValidationException e) {
                    context.error(e
                            .getMessage(), lineNumber(statement.getLinkTo()));
                }

            }

        } else if (coreConcept != null && NS.isConfiguration(coreConcept) && isPrimary) {
            context.error("configurations must specify a subject type in the 'applies to' clause", lineNumber(statement));
        }

        /**
         * Must be a process: (existing) qualities that it can affect
         */
        if (statement.getQualitiesAffected() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getQualitiesAffected()));
                return;
            }

            if (coreConcept != null && !NS.isProcess(coreConcept)) {
                context.error("only processes can use the 'affects' clause, to define the qualities they modify in their context", lineNumber(statement));
            }

            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMScope.Type.AFFECTED_QUALITIES_LIST), statement
                            .getQualitiesAffected(), null)) {

                /*
                 * TODO do something
                 */
            }
        }

        /**
         * Must be a process: (existing) traits it will confer to its participants.
         */
        if (statement.getConferredTraits() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getConferredTraits()));
                return;
            }

            /*
             * TODO two different restrictions for permanent confer or temporary
             */

            if (coreConcept != null
                    && !(NS.isProcess(coreConcept) || NS.isEvent(coreConcept))) {
                context.error("only processes can use the 'confers' clause, to define traits they confer to their context subjects", lineNumber(statement));
            }

            List<IConcept> conferred = new ArrayList<>();
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMScope.Type.CONFERRED_TRAIT_LIST), statement
                            .getConferredTraits(), null)) {
                conferred.add(co.getConcept());
            }
            try {
                Observables.setConferredTraits(knowledge, conferred);
            } catch (KlabValidationException e) {
                context.error(e.getMessage(), lineNumber(statement.getConferredTraits()));
            }

        }

        /**
         * Must be a trait: (existing) traits it will confer to its participants.
         */
        if (statement.getDescribedQuality() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getDescribedQuality()));
                return;
            }

            if (coreConcept != null && NS.isQuality(coreConcept)) {

                IKnowledgeObject ko = new KIMKnowledge(context
                        .get(KIMScope.Type.QUALITY_DESCRIBED_CONFIGURATION), statement
                                .getDescribedQuality(), null);

                if (!NS.isConfiguration(ko.getConcept())) {
                    context.error("the concept described by a quality must be a configuration", lineNumber(statement));
                }

                try {
                    Observables.setDescribedConfiguration(knowledge, ko.getConcept());
                } catch (KlabValidationException e) {
                    context.error(e.getMessage(), ko.getFirstLineNumber());
                }

            } else {

                if (coreConcept != null
                        && !(NS.isTrait(coreConcept) || NS.isClass(coreConcept))) {
                    context.error("only traits, classes and qualities can use the 'describes' clause, to define qualities they summarize or their context of application", lineNumber(statement));
                }

                IKnowledgeObject ko = new KIMKnowledge(context
                        .get(KIMScope.Type.TRAIT_DESCRIBED_QUALITY), statement
                                .getDescribedQuality(), null);
                if (!ko.isNothing() && NS.isConfiguration(ko.getConcept())) {
                    try {
                        Observables.setDescribedConfiguration(knowledge, ko.getConcept());
                    } catch (KlabValidationException e) {
                        context.error(e.getMessage(), ko.getFirstLineNumber());
                    }
                } else if (!NS.isQuality(ko.getConcept())) {
                    context.error("the concept described by a trait must be a quality", lineNumber(statement));
                }

                try {
                    Observables.setDescribedQuality(knowledge, ko.getConcept());
                } catch (KlabValidationException e) {
                    context.error(e.getMessage(), ko.getFirstLineNumber());
                }
            }
        }

        /**
         * Set constraint on required traits
         */
        if (statement.getRequirement() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getRequirement()));
                return;
            }

            // TODO
        }

        int observableOptions = getObservableOptions(context);

        if (statement.getActuallyInheritedTraits() != null) {

            if (isExternal) {
                context.error("this specification is illegal on external concepts", lineNumber(statement
                        .getActuallyInheritedTraits()));
                return;
            }

            if (statement.getConcept() != null
                    && traitKeywords.contains(statement.getConcept())) {
                context.error("traits cannot use 'inherits' - use 'is' to derive from other traits", lineNumber(statement
                        .getActuallyInheritedTraits()));
            } else {

                for (IKnowledgeObject co : new KIMConceptList(context
                        .get(KIMScope.Type.INHERITED_TRAIT_LIST), statement
                                .getActuallyInheritedTraits(), null)) {

                    IConcept trait = co.getConcept();
                    if (!NS.isTrait(trait, false) || NS.isObservable(trait)) {
                        context.error("only traits are allowed in an 'observing' list", lineNumber(statement
                                .getActuallyInheritedTraits()));
                    } else {
                        try {
                            for (ICompileWarning warning : Traits
                                    .addTraitsToDeclaredConcept(knowledge, Collections
                                            .singleton(trait), null, null, observableOptions)) {
                                context.warning(warning.getMessage(), co
                                        .getFirstLineNumber());
                            }
                        } catch (KlabValidationException e) {
                            context.error(e.getMessage(), co.getFirstLineNumber());
                        }
                    }

                }
            }
        }

        context.synchronize();

        if (identities.size() > 0 && !isNothing) {
            try {
                for (ICompileWarning warning : Traits
                        .addTraitsToDeclaredConcept(knowledge, identities, null, null, observableOptions)) {
                    context.warning(warning.getMessage(), getFirstLineNumber());
                }
            } catch (KlabValidationException e) {
                context.error(e.getMessage(), getFirstLineNumber());
            }
        }

        if (statement.getRoles() != null) {

            if (statement.getConcept() != null
                    && traitKeywords.contains(statement.getConcept())) {
                context.error("traits cannot have roles", lineNumber(statement
                        .getRoles()));
            } else {

                for (IKnowledgeObject co : new KIMConceptList(context
                        .get(KIMScope.Type.ROLE_LIST), statement.getRoles(), null)) {

                    IConcept trait = KLAB.c(co.getName());
                    if (!NS.isRole(trait) || NS.isObservable(trait)) {
                        context.error(trait
                                + " is not a role", lineNumber(statement.getRoles()));
                    } else {

                        IConcept main = getConcept();

                        List<IKnowledgeObject> targets = new ArrayList<>();
                        if (statement.getTargetObservable() != null) {
                            for (IKnowledgeObject rob : new KIMConceptList(context
                                    .get(KIMScope.Type.ROLE_TARGET_OBSERVABLE_LIST), statement
                                            .getTargetObservable(), null)) {
                                targets.add(rob);
                            }
                        }
                        if (targets.isEmpty()) {
                            targets.add(null);
                        }

                        for (IKnowledgeObject target : targets) {

                            IConcept trg = target == null ? null : target.getConcept();

                            if (trg != null && !NS.isCountable(trg)) {
                                context.error("the target context ('for') of a role must be a subject or an event", lineNumber(statement
                                        .getTargetObservable()));
                            }

                            for (IKnowledgeObject rob : new KIMConceptList(context
                                    .get(KIMScope.Type.ROLE_RESTRICTED_OBSERVABLE_LIST), statement
                                            .getRestrictedObservable(), null)) {

                                IConcept restricted = KLAB.c(rob.getName());

                                if (restricted == null || !(NS.isCountable(restricted)
                                        || NS.isProcess(restricted))) {
                                    context.error("the target scenario ('in') of a role must be a process, subject or event", lineNumber(statement
                                            .getRestrictedObservable()));
                                }

                                Roles.addRole(trait, main, trg, restricted, /* prop, */ context
                                        .getNamespace());

                            }
                        }

                    }

                }
            }
        }

        context.synchronize();

        /*
         * authority base trait definition - we allow the worldview (and the worldview
         * only) to set the base trait for authorities.
         */
        if (statement.getDefinedAuthority() != null) {

            if (context.getProject() == null || context.getProject().getWorldview() == null) {
                context.error("only worldview projects can define base identities for authorities", lineNumber(statement));
            } else {

                try {
                    IAuthority<?> authority = AuthorityFactory.get()
                            .getAuthorityView(statement.getDefinedAuthority());
                    if (authority != null) {
                        ((BaseAuthority) authority)
                                .setBaseIdentity(knowledge, statement.getDefinedAuthority());
                    }
                } catch (KlabValidationException e) {
                    context.error(e.getMessage(), lineNumber(statement));
                }
            }

        }

        /*
         * applies to
         */

        if (isRelationship) {
            /*
             * TODO create the associated property
             */
            String propertyId = "p" + getConcept().getLocalName();
            this.property = getNamespace().getOntology().getProperty(propertyId);

            if (this.property != null) {
                /*
                 * ehm. Can this happen?
                 */
            } else {

                List<IAxiom> ax = new ArrayList<>();
                ax.add(Axiom.ObjectPropertyAssertion(propertyId));

            }

            /*
             * TODO handle applies to in a rel
             */

            // if (statement.getApplyRange() != null) {
            // // TODO
            // }

            if (!applicables.isEmpty()) {
                /*
                 * TODO
                 */
            }
        }

        /*
         * Equivalence: OK with externals
         */
        if (statement.getEquivalences() != null) {
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMScope.Type.EQUIVALENT_CONCEPT_LIST), statement
                            .getEquivalences(), null)) {

                String coc = co.getNamespace().getId()
                        .equals(context.getNamespace().getId()) ? co.getId()
                                : co.getName();
                context.getNamespace().addAxiom(Axiom.EquivalentClasses(coc, id));
            }
            context.synchronize();
        }

        /*
         * children
         */
        if (statement.getChildren() != null) {
            ArrayList<String> children = new ArrayList<>();
            int nC = 1;
            for (IKnowledgeObject co : new KIMConceptList(context
                    .get(KIMScope.Type.CHILD_LIST), statement.getChildren(), this)) {

                if (co != null) {
                    context.getNamespace().addAxiom(Axiom.SubClass(id, co.getId()));
                    children.add(co.getId());
                    if (isOrdering) {
                        context.getNamespace()
                                .addAxiom(Axiom.AnnotationAssertion(co
                                        .getId(), NS.ORDER_PROPERTY, "" + nC));
                    }

                    /*
                     * record declaration hierarchy FIXME redundant with new constructors
                     * - check behavior. Or don't.
                     */
                    ((KIMKnowledge) co).parent = this;
                    this.children.add(co);

                    nC++;
                }
                
                /**
                 * Concepts with children are not worldview-only.
                 */
                this.isWorldviewCompatible = false;
                
            }
            if (statement.isDisjoint() && children.size() > 1) {
                context.getNamespace().addAxiom(Axiom
                        .DisjointClasses(children.toArray(new String[children.size()])));
            }
        }

        context.synchronize();

        if (statement.getRestrictions() != null
                && statement.getRestrictions().size() > 0) {

            if (isExternal) {
                context.error("cannot restrict external concepts", lineNumber(statement));
                return;
            }

            for (RestrictionStatement rs : statement.getRestrictions()) {

                if (rs.getRelDefs() != null) {
                    for (RestrictionDefinition rd : rs.getRelDefs().getDefinitions()) {
                        new KIMRestriction(context.get(getContextForRelationship(rs
                                .getRelType())), rd, this);
                    }
                } else if (rs.getAuthorities() != null) {

                    if (coreConcept == null || !coreConcept.is(KLAB.c(NS.CORE_DOMAIN))) {
                        context.error("only domains can restrict authorities. Use 'requires authority' on trait definitions", lineNumber(rs));
                    }

                    /*
                     * in domains, the annotation means "allowed authority" and can have
                     * multiple comma-separated specs. TODO check if we want to use data
                     * properties and reasoning to enforce this. Authority IDs are paths
                     * and this allows all the sub-authorities, so for now this is much
                     * simpler.
                     */
                    context.getNamespace()
                            .addAxiom(Axiom
                                    .AnnotationAssertion(id, NS.AUTHORITY_ID_PROPERTY, StringUtils
                                            .join(rs.getAuthorities(), ',')));

                }
            }
        }

        if (statement.getMetadata() != null) {

            if (isExternal) {
                context.error("cannot add metadata to external concepts", lineNumber(statement
                        .getMetadata()));
                return;
            }

            metadata = new KIMMetadata(context, statement.getMetadata(), this);

            /*
             * assert annotations from metadata
             */
            for (String key : metadata.getKeys()) {
                /*
                 * TODO check that key is a valid property; use a catalog key -> property
                 * if one exists
                 */
                Object o = metadata.get(key);
                context.getNamespace().addAxiom(Axiom.AnnotationAssertion(id, key, o));
            }

            context.synchronize();
        }

        if (statement.getDocstring() != null) {

            if (isExternal) {
                context.error("cannot add descriptions to external concepts", lineNumber(statement));
                return;
            }

            if (!statement.isRoot()) {
                context.getNamespace()
                        .addAxiom(Axiom
                                .AnnotationAssertion(id, IMetadata.DC_COMMENT, statement
                                        .getDocstring()));
                try {
                    context.synchronize();
                } catch (Throwable e) {
                    context.error("internal error: "
                            + e.getMessage(), lineNumber(statement));
                }
            }
        }

        /*
         * TODO/NOW gather and validate constraints on required traits.
         */

        /*
         * annotations in inner objects are only allowed on dependent concept and property
         * definitions. Just check that there aren't - they have been processed already.
         */
        if (statement.getAnnotations() != null && statement.getAnnotations().size() > 0) {

            if (isExternal) {
                context.error("cannot add annotations to external concepts", lineNumber(statement
                        .getAnnotations().get(0)));
                return;
            }
        }

        /**
         * We only warn about lack of domains for concepts that are not roles, as roles
         * are orthogonal semantics, scenario-driven and scoped.
         */
        if (isPrimary && NS.getDomain(knowledge) == null && !NS.isDomain(knowledge)
                && !NS.isRole(knowledge)) {
            if (NS.getAnnotations(this, "internal").isEmpty()) {
                context.warning("The concept does not belong to a domain. This will prevent this namespace from being published. Please ensure its inheritance is complete or mark it internal.", lineNumber(statement));
            }
        }

        context.synchronize();

        if (statement.isDeniable()) {

            if (isExternal) {
                context.error("modifiers are illegal on external concepts", lineNumber(statement));
                return;
            }

            if (knowledge != null
                    && !((coreConcept != null && NS.isAttribute(coreConcept))
                            || NS.isAttribute(knowledge))) {
                context.error("only attributes can be declared deniable", lineNumber(statement));
            }

            context.getNamespace().addAxiom(Axiom
                    .AnnotationAssertion(id, NS.DENIABILITY_PROPERTY, "true"));

            context.synchronize();

            isDeniable = true;
        }

        if (isOwlNothing) {
            isNothing = true;
            if (knowledge != null) {
                NS.registerNothingConcept(knowledge.toString());
            }
        }

        /*
         * we allow more slack for internal concepts
         */
        boolean isInternal = getAnnotation("internal") != null;

        if (knowledge != null && !isNothing && !knowledge.isAbstract()
                && NS.isQuality(knowledge)) {
            IConcept ctx = Observables.getContextType(knowledge);
            if (ctx == null && !isInternal) {
                context.error("any non-abstract quality must identify a context", getFirstLineNumber());
            }
        }

        if (knowledge != null && !isNothing && !knowledge.isAbstract()
                && NS.isTrait(knowledge)
                && context.getNamespace().getProject().getWorldview() == null) {
            IConcept ctx = Observables.getContextType(knowledge);
            if (ctx == null && !isInternal) {
                context.error("non-abstract traits without a context (stated in 'describes' or 'applies to') are only allowed in worldviews", getFirstLineNumber());
            }
        }

    }

    private KIMScope.Type getContextForRelationship(String relType) {
        switch (relType) {
        case "uses":
            return KIMScope.Type.USES_RELATIONSHIP;
        case "contains":
            return KIMScope.Type.CONTAINS_RELATIONSHIP;
        case "has":
            return KIMScope.Type.HAS_RELATIONSHIP;
        case "implies":
            return KIMScope.Type.IMPLIES_RELATIONSHIP;
        }
        throw new KlabRuntimeException("unsupported relationship type " + relType);
    }

    static IConcept declare(KIMScope context, PropertyStatement statement, String id) {

        /*
         * FIXME TODO create property and masking concept; return concept.
         */
        boolean isRoot = statement.getId().equals(id);
        boolean mustExist = !context.isInScope(KIMScope.Type.CHILD_LIST) && !isRoot;
        IProperty pret = null;
        IConcept ret = null;
        boolean isObject = !(statement.isData() || statement.isAnnotation());

        if (id.contains(":")) {
            pret = KLAB.KM.getProperty(id);
            if (pret == null) {
                context.error("cannot resolve external property "
                        + id, lineNumber(statement));
            } else {
                context.getNamespace().checkImported(context, ret, lineNumber(statement));
                ret = pret.getType();
            }

        } else {

            pret = context.getNamespace().getOntology().getProperty(id);

            if (pret == null) {
                if (mustExist) {
                    context.error("knowledge " + statement.getId()
                            + " is unknown: new declarations are not allowed in this context", lineNumber(statement));
                } else {

                    // TODO now this can only be an annotation property
                    context.getNamespace().addAxiom(statement.isAnnotation()
                            ? Axiom.AnnotationPropertyAssertion(id)
                            : (isObject ? Axiom.ObjectPropertyAssertion(id)
                                    : Axiom.DataPropertyAssertion(id)));

                    context.getNamespace().synchronizeKnowledge();

                    pret = context.getNamespace().getOntology().getProperty(id);
                    ret = pret.getType();
                }
            }
        }

        return ret;
    }

    private IConcept declare(KIMScope context, ConceptDeclaration statement, boolean allowObjects) {

        boolean propertyOK = context.isInScope(KIMScope.Type.PROPERTY)
                || context.isInScope(KIMScope.Type.MODEL_OBSERVABLE); // TODO
        // link
        // to
        // context
        boolean mustExist = !context.isInScope(KIMScope.Type.CHILD_LIST); // TODO
        // link
        // to

        boolean mustBeObservable = context.isInScope(KIMScope.Type.OBSERVER)
                || context.isInScope(KIMScope.Type.MODEL); // TODO
        // link
        // to
        // context

        if (statement == null || statement.getIds() == null
                || statement.getIds().isEmpty()) {
            this.isNothing = true;
            return null;
        }

        if (allowObjects) {
            List<String> objIds = new ArrayList<>();
            for (ConceptIdentifier cid : statement.getIds()) {

                // TODO support the rest
                String string = cid.getId();

                IKnowledge existing = handleConceptIdentifier(context, cid);

                if (existing == null) {

                    if (KLAB.KM.getConcept(string) != null) {
                        break;
                    }

                    String thisC = context.getNamespace().getId() + ":" + string;
                    if (KLAB.KM.getConcept(thisC) != null) {
                        break;
                    }

                    if (context.getProject().getWorldview() == null
                            && !context.getNamespace().isProjectKnowledge()) {
                        if (context.getProject().getUserKnowledge().getOntology()
                                .getConcept(string) != null) {
                            string = context.getProject().getUserKnowledge().getId() + ":" + string;
                            break;
                        }
                    }

                    if (!string.contains(".")) {
                        string = context.getNamespace().getId() + "." + string;
                    }
                    objIds.add(string);

                } else if (existing.equals(KLAB.KM.getNothing())) {
                    this.isNothing = true;
                    return null;
                } else {
                    string = existing.toString();
                }

            }

            /*
             * only proceed with objects if ALL are potential object IDs.
             */
            if (objIds.size() == statement.getIds().size()) {
                this.objectDeclarations.addAll(objIds);
                return null;
            }

        }

        this.isNegated = statement.isNegated();

        IConcept main = null;
        ArrayList<IConcept> roles = new ArrayList<>();
        ArrayList<IConcept> traits = new ArrayList<>();
        IConcept existing = null;
        boolean singleConcept = statement.getIds().size() == 1;

        for (ConceptIdentifier cid : statement.getIds()) {

            String string = cid.getId();
            existing = handleConceptIdentifier(context, cid);

            if (existing == null) {

                if (string.contains(":")) {

                    if (KLAB.KM.getConcept(string) == null
                            && (!propertyOK || KLAB.KM.getProperty(string) == null)
                            && mustExist) {
                        context.error("knowledge " + string
                                + " is unknown: new declarations are not allowed in this context", lineNumber(statement));
                    }

                    // bit of a hack here, but should work.
                    if (propertyOK && KLAB.KM.getProperty(string) != null) {
                        existing = KLAB.KM.getProperty(string).getType();
                    }

                    if (existing == null) {
                        existing = KLAB.KM.getConcept(string);
                    }

                    if (existing != null) {
                        context.getNamespace()
                                .checkImported(context, existing, lineNumber(statement));
                    }

                } else {

                    boolean isProjectKnowledge = false;

                    if (propertyOK && KLAB.KM.getProperty(string) != null) {
                        existing = namespace.getOntology().getProperty(string).getType();
                    }

                    if (context.getProject().getWorldview() == null
                            && !context.getNamespace().isProjectKnowledge()) {
                        existing = context.getProject().getUserKnowledge().getOntology().getConcept(string);
                        isProjectKnowledge = existing != null;
                    }

                    if (existing == null) {

                        existing = ((Ontology) namespace.getOntology())
                                .createConcept(string);

                        /*
                         * New local knowledge is child of any model where it's been
                         * generated. Good for display and debugging. Does not happen in
                         * other knowledgeobjects.
                         */
                        IModelObject model = context.get(IModel.class);
                        if (model != null) {
                            ((KIMModelObject) model).children.add(this);
                            this.parent = model;
                        }
                    }

                    if (namespace.getOntology().getConcept(string) == null
                            && (!propertyOK || namespace.getOntology()
                                    .getProperty(string) == null)
                            && mustExist
                            && !isProjectKnowledge) {
                        context.error("knowledge " + string
                                + " is unknown: new declarations are not allowed in this context", lineNumber(statement));
                    }

                    if (!(existing instanceof IProperty)
                            && Character.isLowerCase(string.charAt(0)) && !propertyOK) {
                        context.error("concept names start with an uppercase letter: "
                                + string, lineNumber(statement));
                    }
                }
            } else if (existing.equals(KLAB.KM.getNothing())) {
                isNothing = true;
                return null;
            }

            /*
             * if it's a trait or a role, add it;
             */
            if (existing != null) {

                if (cid.isNegated()) {
                    if (!NS.isTrait(existing)) {
                        context.error("only traits can be negated", lineNumber(cid));
                    }
                    try {
                        existing = Traits.getNegation(existing);
                    } catch (KlabValidationException e) {
                        context.error(e.getMessage(), lineNumber(cid));
                    }
                }

                if (NS.isTrait(existing, false)) {

                    traits.add(existing);

                } else if (NS.isRole(existing)) {

                    roles.add(existing);

                } else {
                    if (main != null && !main.equals(existing)) {
                        context.error("both " + main + " and " + existing
                                + " are observables: only one concept can be observable, all others must be traits", lineNumber(statement));
                    }
                    main = existing;
                }

                /*
                 * If it comes from an authority, register the authority with the
                 * namespace
                 */
                String auth = existing.getMetadata().getString(NS.AUTHORITY_ID_PROPERTY);
                if (auth != null) {
                    context.getNamespace().registerAuthority(existing);
                }
            }
        }

        boolean traitOnly = false;

        if (main != null && roles.size() > 0) {

            if (!context.isInScope(IParsingScope.Type.MODEL)) {
                context.error("role attribution is only allowed in model specifications", lineNumber(statement));
            }
            for (IConcept role : roles) {
                // if (role.isAbstract()) {
                // context.error(role
                // + " is an abstract role: only concrete roles can be attributed to
                // observables", lineNumber(statement));
                // }
            }
        }

        if (main != null && statement.getAuthIdentifier() != null) {

            int i = 0;

            for (String authId : statement.getAuthIdentifier()) {

                String authority = statement.getAuthority().get(i);

                IAuthority<?> auth = null;
                try {
                    auth = AuthorityFactory.get().getAuthorityView(authority);
                } catch (KlabValidationException e1) {
                    context.error(e1.getMessage(), lineNumber(statement));
                }
                if (auth == null) {
                    context.error("no authority named " + statement.getAuthority()
                            + " has been configured", lineNumber(statement));
                } else {
                    try {

                        String error = auth.validateIdentifiedConcept(main, authId);

                        if (error != null) {

                            context.error(error, lineNumber(statement));

                        } else {

                            IConcept identity = auth.getIdentity(authId, authority);
                            if (identity != null && identity.getLocalName() != null) {
                                traits.add(identity);
                            } else {
                                context.error("authority " + authority
                                        + " does not recognize identifier "
                                        + authId, lineNumber(statement));
                            }
                        }
                    } catch (Exception e) {
                        context.error(e.getMessage(), lineNumber(statement));
                    }
                }
                i++;
            }
        }

        if (!mustBeObservable && main == null && roles.size() == 0) {

            if (statement.getIds().size() > 1) {

                /*
                 * compose traits with trait. Will be the last trait adopting all others.
                 */
                main = traits.get(traits.size() - 1);
                for (int i = 0; i < traits.size() - 1; i++) {
                    IConcept t = traits.get(i);
                    if (!main.is(t)) {
                        context.getNamespace().getOntology()
                                .define(Collections.singleton(Axiom
                                        .SubClass(t.toString(), main.toString())));
                    }
                }

            } else {
                main = existing;
            }
            traitOnly = true;
        }

        if (main == null) {

            /*
             * role trumps trait
             */
            if (roles.size() > 0) {
                if (roles.size() == 1) {
                    main = roles.get(0);
                    roles.clear();
                } else {
                    context.error("more than one role are only allowed when annotating an observable", lineNumber(statement));
                }
            }

            if (main == null && traits.size() == 1) {
                main = KLAB.c(traits.get(0).toString());
                traits.clear();
            }
            if (main == null && propertyOK && traits.size() > 0) {
                main = KLAB.p(traits.get(0).toString()).getType();
            }
            if (main == null) {
                context.error("no valid observable: at least one observable concept is mandatory", lineNumber(statement));
            }

        }

        /*
         * these must be done outside of declare() after the concept is clarified.
         */
        if (main != null && ((statement.getInherentIds() != null
                && statement.getInherentIds().size() > 0)
                || statement.getInherent() != null)) {

            int line = lineNumber(statement);

            // if (!NS.isQuality(main) && !NS.isTrait(main) && !NS.isConfiguration(main))
            // {
            // context.error("inherent types ('of') can only be specified for qualities,
            // traits and configurations", lineNumber(statement));
            // }
            if (statement.getInherent() != null) {
                inherentType = declare(context, statement.getInherent(), false);
                line = lineNumber(statement.getInherent());
            } else {
                inherentType = declare(context, statement
                        .getInherentIds(), lineNumber(statement));
            }

            if (inherentType != null
                    && !(NS.isDirect(inherentType) || NS.isConfiguration(inherentType))) {

                if (!((NS.isQuality(inherentType) || NS.isTrait(inherentType))
                        && NS.isRelativeQuality(main))) {
                    context.error("the inherent type ('of') must be a subject, process, event, configuration or relationship"
                            + (NS.isQuality(main)
                                    ? ": this relative quality can also name a compatible quality or trait"
                                    : ""), line);
                }
            }
        }

        if (main != null && (statement.getDownTo() != null
                || (statement.getDownToIds() != null
                        && statement.getDownToIds().size() > 0))) {
            if (statement.getDownTo() != null) {
                downTo = declare(context, statement.getDownTo(), false);
            } else {
                downTo = declare(context, statement
                        .getDownToIds(), lineNumber(statement));
            }
        }

        if (main != null && (statement.getByTrait() != null
                || (statement.getByTraitIds() != null
                        && statement.getByTraitIds().size() > 0))) {
            if (statement.getByTrait() != null) {
                byTrait = declare(context, statement.getByTrait(), false);
            } else {
                byTrait = declare(context, statement
                        .getByTraitIds(), lineNumber(statement));
            }
        }

        if (main != null && ((statement.getOuterContextIds() != null
                && statement.getOuterContextIds().size() > 0)
                | statement.getOuterContext() != null)) {

            if (statement.getOuterContext() != null) {
                contextType = declare(context, statement.getOuterContext(), false);
            } else {
                contextType = declare(context, statement
                        .getOuterContextIds(), lineNumber(statement));
            }

            if (contextType == null) {
                context.error("context type ('within') is unknown", lineNumber(statement));
            } else if (main != null && !NS.isDirect(contextType)) {
                boolean ok = false;
                IModelObject mainobs = context
                        .getObjectInScope(IParsingScope.Type.CONCEPT);
                if (mainobs instanceof IKnowledgeObject
                        && ((IKnowledgeObject) mainobs).getConcept() != null
                        && NS.isRole(((IKnowledgeObject) mainobs).getConcept())) {
                    for (IConcept app : Observables
                            .getApplicableObservables(contextType)) {
                        if (NS.isThing(app)) {
                            ok = true;
                            break;
                        }
                    }
                }
                if (!ok) {
                    context.error("the context type ('within') must be a direct observable"
                            + (NS.isRole(main) ? " or a role that applies to one"
                                    : ""), lineNumber(statement));
                } else {

                    /*
                     * if this has been declared to "apply to" something, the context must
                     * be compatible.
                     */
                    Collection<IConcept> applicable = Observables
                            .getApplicableObservables(main);
                    if (!applicable.isEmpty()) {
                        ok = false;
                        for (IConcept c : applicable) {
                            if (contextType.is(c)) {
                                ok = true;
                            }
                        }
                        if (!ok) {
                            context.error("the context type ('within') is not compatible with the previously declared 'applies to' clause", lineNumber(statement));
                        }
                    }
                }
            }
        }

        if (main != null && ((traits.size() > 0 && !traitOnly) || contextType != null
                || inherentType != null || !roles.isEmpty()) || downTo != null
                || byTrait != null) {
            try {

                int observableOptions = getObservableOptions(context);

                main = Observables
                        .declareObservable(main, traits, contextType, inherentType, roles, byTrait, downTo, KLAB.REASONER
                                .getOntology(), observableOptions);
            } catch (KlabValidationException e) {
                context.error(e.getMessage(), lineNumber(statement));
            }
        }
        if (main != null && isNegated) {
            try {
                main = Traits.getNegation(main);
            } catch (KlabValidationException e) {
                context.error(e.getMessage(), lineNumber(statement));
            }
        }

        return main;
    }

    private int getObservableOptions(KIMScope context) {
        int ret = 0;

        IModel model = context.get(IModel.class);
        if (context.isInScope(IParsingScope.Type.DEPENDENCY) || (model != null && model.isPrivate())) {
            ret |= Observables.ACCEPT_SUBJECTIVE_OBSERVABLES;
        }

        return ret;
    }

    /*
     * Find the knowledge e typesresponding to this id, which must be known. reminds of
     * something
     */
    public static IConcept getKnownConcept(KIMScope context, ConceptIdentifier cid, boolean primary) {

        int line = lineNumber(cid);
        String id = primary ? cid.getId() : cid.getId2();
        ConceptDeclaration declaration = primary ? cid.getDeclaration() : cid.getDeclaration2();

        IConcept ret = null;
        // boolean isLocal = id.contains(":");

        if (id != null) {
            if (!id.contains(":")) {
                id = context.getNamespace().getId() + ":" + id;
            }

            if (KLAB.KM.getProperty(id) != null) {
                context.error("knowledge " + id
                        + " is a property: properties are not allowed in this context", line);
            }

            ret = KLAB.KM.getConcept(id);

        } else if (declaration != null) {

            KIMKnowledge c = new KIMKnowledge(context, declaration);
            if (c.isProperty()) {
                context.error("properties are not allowed in this context", line);
            }
            if (!c.isNothing() && c.getConcept() != null) {
                ret = c.getConcept();
            }

        }
        return ret;
    }

    public List<String> getObjectDeclarations() {
        return objectDeclarations;
    }

    /**
     * This will handle all the syntax for concept identification, returning null only
     * when no special identifiers have been given (in which case the default ID treatment
     * should take over). Any concept identification must refer to compatible and existing
     * concepts. Returns Owl:Nothing in error.
     * 
     * @param context
     * @param cid
     * @return knowledge, null, or nothing.
     */
    public static IConcept handleConceptIdentifier(KIMScope context, ConceptIdentifier cid) {

        IConcept ret = null;
        IConcept other = null;

        if (cid.isCount()) {

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_COUNT))) {
                context.error(cid.getId()
                        + " is already a count: you cannot declare a count of a count", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!NS.isCountable(ret)) {
                context.error(cid.getId()
                        + " is not a countable concept: you cannot count this", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = Observables.makeCount(ret);

        } else if (cid.isDistance()) {

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_DISTANCE))) {
                context.error(cid.getId()
                        + " is already a distance: you cannot declare the distance to a distance", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!NS.isObject(ret)) {
                context.error(cid.getId()
                        + " is not an object: you can only observe the distance to an object", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = Observables.makeDistance(ret);

        } else if (cid.isPresence()) {

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_PRESENCE))) {
                context.error(cid.getId()
                        + " is already a presence: you cannot declare the presence of a presence", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            try {
                ret = Observables.makePresence(ret);
            } catch (KlabRuntimeException e) {
                context.error(e.getMessage(), lineNumber(cid));
            }

        } else if (cid.isProbability()) {

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_PROBABILITY))) {
                context.error(cid.getId()
                        + " is already a probability: you cannot declare the probability of a probability", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!NS.isEvent(ret)) {
                context.error(cid.getId()
                        + " is not an event: you can only observe the probability of an event", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = Observables.makeProbability(ret);

        } else if (cid.isProportion()) {

            if (cid.getId2() != null || cid.getDeclaration2() != null) {
                if ((other = getKnownConcept(context, cid, false)) == null) {
                    return KLAB.KM.getNothing();
                }
            }

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_PROPORTION))) {
                context.error(cid.getId()
                        + " is already a proportion: you cannot declare the proportion of a proportion", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!(NS.isQuality(ret) || NS.isTrait(ret)) && !NS.isQuality(other)) {
                context.error(cid.getId()
                        + " proportions are of qualities in qualities or traits in qualities", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = Observables.makeProportion(ret, other);

        } else if (cid.isRatio()) {

            if (cid.getId2() != null || cid.getDeclaration2() != null) {
                if ((other = getKnownConcept(context, cid, false)) == null) {
                    return KLAB.KM.getNothing();
                }
            }

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_RATIO))) {
                context.error(cid.getId()
                        + " is already a ratio: you cannot declare the ratio of a ratio", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!(NS.isQuality(ret) || NS.isTrait(ret)) && !NS.isQuality(other)) {
                context.error(cid.getId()
                        + " ratios are of qualities over qualities or traits over qualities", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = Observables.makeRatio(ret, other);

        } else if (cid.isUncertainty()) {

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_UNCERTAINTY))) {
                context.error(cid.getId()
                        + " is already an uncertainty: you cannot declare the uncertainty of an uncertainty", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = Observables.makeUncertainty(ret);

        } else if (cid.isValue()) {

            if (cid.getId2() != null || cid.getDeclaration2() != null) {
                if ((other = getKnownConcept(context, cid, false)) == null) {
                    return KLAB.KM.getNothing();
                }
            }

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_VALUE))) {
                context.error(cid.getId()
                        + " is already a value: you cannot declare the value of a value", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = Observables.makeValue(ret, other);

        } else if (cid.isAssessment()) {

            if ((ret = getKnownConcept(context, cid, true)) == null) {
                return KLAB.KM.getNothing();
            }

            if (ret.is(KLAB.c(NS.CORE_ASSESSMENT))) {
                context.error(cid.getId()
                        + " is already an assessment: you cannot declare the assessment of an assessment", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            if (!NS.isQuality(ret)) {
                context.error(cid.getId()
                        + ": only qualities can be assessed", lineNumber(cid));
                return KLAB.KM.getNothing();
            }

            ret = Observables.makeAssessment(ret);

        }

        return ret;
    }

    public static IConcept declare(KIMScope context, List<ConceptIdentifier> ids, int lineNumber) {
        ArrayList<String> idds = new ArrayList<>();
        for (ConceptIdentifier cd : ids) {

            String id = cd.getId();

            if (cd.isNegated()) {
                id = negate(cd.getId(), context, lineNumber(cd));
            }

            idds.add(id);
        }

        return declare(context, idds, lineNumber);
    }

    private static String negate(String id, KIMScope context, int line) {

        /*
         * ensure id is known
         */
        IConcept original = null;
        if (id.contains(":")) {
            original = KLAB.KM.getConcept(id);
        } else {
            original = context.getNamespace().getOntology().getConcept(id);
        }

        if (original == null) {
            context.error("concept " + id + " is unknown", line);
        } else {
            try {
                original = Traits.getNegation(original);
            } catch (KlabValidationException e) {
                context.error(e.getMessage(), line);
            }
        }

        return original == null ? null : original.toString();
    }

    public static IConcept declare(KIMScope context, Collection<String> ids, int lineNumber) {

        boolean propertyOK = context.isInScope(KIMScope.Type.PROPERTY)
                || context.isInScope(KIMScope.Type.MODEL_OBSERVABLE); // TODO
        // link
        // to
        // context
        boolean mustExist = !context.isInScope(KIMScope.Type.CHILD_LIST); // TODO
        // link
        // to
        // context
        boolean mustBeObservable = context.isInScope(KIMScope.Type.OBSERVER)
                || context.isInScope(KIMScope.Type.MODEL); // TODO
        // link
        // to
        // context
        IConcept rootConcept = null; // TODO link to context

        if (ids == null || ids.isEmpty()) {
            return null;
        }

        IConcept main = null;
        ArrayList<IConcept> traits = new ArrayList<>();
        IConcept existing = null;
        for (String string : ids) {

            existing = null;

            if (string.contains(":")) {

                if (KLAB.KM.getConcept(string) == null
                        && (!propertyOK || KLAB.KM.getProperty(string) == null)
                        && mustExist) {
                    context.error("knowledge " + string
                            + " is unknown: new declarations are not allowed in this context", lineNumber);
                }

                // bit of a hack here, but should work.
                if (propertyOK && KLAB.KM.getProperty(string) != null) {
                    existing = KLAB.KM.getProperty(string).getType();
                }

                if (existing == null) {
                    existing = KLAB.KM.getConcept(string);
                }

                if (existing != null) {
                    context.getNamespace().checkImported(context, existing, lineNumber);
                }

            } else {

                boolean isProjectKnowledge = false;

                if (propertyOK && KLAB.KM.getProperty(string) != null) {
                    existing = context.getNamespace().getOntology().getProperty(string)
                            .getType();
                }

                if (context.getProject().getWorldview() == null
                        && !context.getNamespace().isProjectKnowledge()) {
                    existing = context.getProject().getUserKnowledge().getOntology().getConcept(string);
                    isProjectKnowledge = existing != null;
                }

                if (existing == null) {
                    existing = ((Ontology) context.getNamespace().getOntology())
                            .createConcept(string);
                }

                if (context.getNamespace().getOntology().getConcept(string) == null
                        && (!propertyOK || context.getNamespace().getOntology()
                                .getProperty(string) == null)
                        && mustExist
                        && !isProjectKnowledge) {
                    context.error("knowledge " + string
                            + " is unknown: new declarations are not allowed in this context", lineNumber);
                }

                if (!(existing instanceof IProperty)
                        && Character.isLowerCase(string.charAt(0)) && !propertyOK) {
                    context.error("concept names start with an uppercase letter: "
                            + string, lineNumber);
                }
            }

            /*
             * if it's a trait (alone), add it;
             */
            if (existing != null) {

                if (NS.isTrait(existing, false) && !NS.isObservable(existing)) {
                    traits.add(existing);
                } else {
                    if (main != null && !main.equals(existing)) {
                        context.error("both " + main + " and " + existing
                                + " are observables: only one concept can be observable, all others must be traits", lineNumber);
                    }
                    main = existing;
                }
            }
        }

        boolean traitOnly = false;
        if (!mustBeObservable && main == null) {
            if (ids.size() > 1) {

                /*
                 * compose multiple traits into one; use the last specified as root.
                 */
                Set<IConcept> trs = new HashSet<>();
                for (int i = 0; i < traits.size() - 1; i++) {
                    trs.add(traits.get(i));
                }
                IConcept cnc;
                if (traits.size() > 0) {
                    try {
                        cnc = Observables
                                .declareObservable(traits.get(traits.size() - 1), trs);
                        main = KLAB.KM.getConcept(cnc.toString());
                    } catch (KlabValidationException e) {
                        context.error(e.getMessage(), lineNumber);
                    }
                }

            } else {
                main = existing;
            }
            traitOnly = true;
        }

        if (main == null && rootConcept == null) {

            if (traits.size() == 1) {
                main = KLAB.c(traits.get(0).toString());
            }

            if (propertyOK && traits.size() > 0) {
                main = KLAB.p(traits.get(0).toString()).getType();
            }
            if (main == null) {
                context.error("no valid observable: at least one observable concept is mandatory", lineNumber);
            }
        } else if (traits.size() > 0 && !traitOnly) {
            try {
                main = Observables
                        .declareObservable(main == null ? rootConcept
                                : main, traits, null, null, null, null, null, KLAB.REASONER
                                        .getOntology());
            } catch (KlabValidationException e) {
                context.error(e.getMessage(), lineNumber);
            }
        }

        return main;
    }

    @Override
    public boolean isConcept() {
        return this.knowledge != null;
    }

    @Override
    public boolean isProperty() {
        return this.property != null;
    }

    @Override
    public boolean isNegated() {
        return isNegated;
    }

    @Override
    public String getId() {
        return knowledge == null ? (property == null ? null : property.getLocalName())
                : knowledge.getLocalName();
    }

    @Override
    public String getName() {
        return knowledge == null ? (property == null ? null : property.toString())
                : knowledge.toString();
    }

    @Override
    public IConcept getConcept() {
        return knowledge;
    }

    @Override
    public IProperty getProperty() {
        return property;
    }

    @Override
    public IKnowledge getInherentType() {
        return inherentType;
    }

    @Override
    public IKnowledge getContextType() {
        return contextType;
    }

    @Override
    public boolean isNothing() {
        return knowledge == null && property == null;
    }

    @Override
    public String toString() {
        return "K/" + (knowledge == null
                ? (property == null ? "(nothing)" : property.toString())
                : knowledge.toString());
    }

    @Override
    public IConcept getType() {
        return knowledge;
    }

    @Override
    public IModel getModel() {
        return model;
    }

    @Override
    public IDirectObserver getDirectObserver() {
        return directObserver;
    }

    @Override
    public IKnowledge getDetailLevel() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isWorldviewCompatible() {
        return isWorldviewCompatible;
    }
}
