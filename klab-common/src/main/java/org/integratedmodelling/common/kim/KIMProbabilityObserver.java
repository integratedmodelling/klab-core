/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.INumericObserver;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IProbabilityObserver;
import org.integratedmodelling.api.modelling.IRatioObserver;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMProbabilityObserver extends KIMNumericObserver
        implements IProbabilityObserver, NetworkSerializable, NetworkDeserializable {

    boolean          occurrence = false;
    private IConcept originalConcept;

    public class ProbabilityMediator extends NumericMediator {

        INumericObserver otherObserver;

        protected ProbabilityMediator(INumericObserver other, IMonitor monitor) {
            super(monitor);
            this.otherObserver = other;
        }

        @Override
        public Object mediate(Object object) throws KlabException {
            Number val = getValueAsNumber(object, otherObserver);
            return super.mediate(val);
        }

        @Override
        public String getLabel() {
            return (discretization == null ? null : "discretize");
        }
    }

    public KIMProbabilityObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        minimumValue = 0.0;
        maximumValue = 1.0;
        this.occurrence = statement.getType() != null && statement.getType().equals("occurrence");
        // if (observable != null) {
        // ((ObservableSemantics) observable)
        // .setType(this.getObservedType(context, this.observable.getType()));
        // }
        validate(context);
    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return false;
    }

    public KIMProbabilityObserver(IObserver observer) {
        super(observer);
        minimumValue = 0.0;
        maximumValue = 1.0;
        if (!(observer instanceof IProbabilityObserver)) {
            throw new KlabRuntimeException("cannot initialize a probability observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.isIndirect = ((KIMProbabilityObserver) observer).isIndirect;
        this.occurrence = ((KIMProbabilityObserver) observer).occurrence;
    }

    @Override
    public String toString() {
        return "PRB/" + getObservable();
    }

    /**
     * Only for the deserializer
     */
    public KIMProbabilityObserver() {
        minimumValue = 0.0;
        maximumValue = 1.0;
    }

    @Override
    public IObserver copy() {
        return new KIMProbabilityObserver(this);
    }

    KIMProbabilityObserver(IObservableSemantics observable) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
        minimumValue = 0.0;
        maximumValue = 1.0;
    }

    KIMProbabilityObserver(IConcept observable) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
        minimumValue = 0.0;
        maximumValue = 1.0;
    }

    @Override
    protected KIMScope.Type getDiscretizationContext() {
        return KIMScope.Type.PROPORTION_DISCRETIZATION;
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.PROBABILITY_OBSERVATION);
    }

    @Override
    protected IConcept interpretType(KIMScope context, Observer ostatement, IConcept statedType) {
        this.occurrence = ostatement.getType() != null && ostatement.getType().equals("occurrence");
        return super.interpretType(context, ostatement, statedType);
    }

    @Override
    public IConcept getObservedType(KIMScope context, IConcept concept) {

        if (!concept.is(KLAB.c(NS.CORE_PROBABILITY))) {
            this.originalConcept = concept;
        }

        IConcept ret = concept;
        if (isIndirect) {
            ret = occurrence ? Observables.makeOccurrence(concept) : Observables.makeProbability(concept);
            if (ret == null) {
                context.error(concept
                        + ": probabilities are assigned only to events or processes. Use the direct form (without 'of') for observables that are already probabilities.", getFirstLineNumber());
                return concept;
            }
        } else if (!concept.is(KLAB.c(NS.CORE_PROBABILITY))) {
            context.error(concept
                    + ": the observable in this statement must be a probability. Use the indirect form (with 'of') for events or processes.", getFirstLineNumber());
        }
        return ret;
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException {
        observer = getRepresentativeObserver(observer);

        if (!(observer instanceof IProbabilityObserver))
            throw new KlabValidationException("probabilities can only mediate other probabilities");

        if (discretization != null) {
            if (((IRatioObserver) observer).getDiscretization() != null) {
                monitor.warn(getObservable().getType()
                        + ": discretized probabilities should not mediate other discretized probabilities: value will be undiscretized, then discretized again");
            }
            return new ProbabilityMediator((INumericObserver) observer, monitor);
        }

        return null;
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        isIndirect = bean.isIndirect();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        ret.setIndirect(isIndirect);
        return (T) ret;
    }

    @Override
    public IConcept getEventType() {
        return Observables.getInherentType(getObservable().getType());
    }

    @Override
    public boolean isOccurrence() {
        return occurrence;
    }

    @Override
    public String getDefinition() {

        return "probability " + (isIndirect ? "of " : "")
                + (originalConcept == null
                        ? Observables.getDeclaration(getObservable().getType(), getNamespace().getProject())
                        : Observables.getDeclaration(originalConcept, getNamespace().getProject()));
    }

}
