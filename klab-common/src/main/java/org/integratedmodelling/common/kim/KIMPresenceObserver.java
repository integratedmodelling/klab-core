/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.IActiveSubject;
import org.integratedmodelling.api.modelling.ICoverage;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IEvent;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IPresenceObserver;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.contextualization.IDirectInstantiator;
import org.integratedmodelling.api.modelling.contextualization.IEventInstantiator;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.modelling.contextualization.ISubjectInstantiator;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IGridMask;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.model.Coverage;
import org.integratedmodelling.common.model.runtime.AbstractMediator;
import org.integratedmodelling.common.model.runtime.AbstractStateContextualizer;
import org.integratedmodelling.common.utils.MapUtils;
import org.integratedmodelling.common.utils.NumberUtils;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabUnsupportedOperationException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMPresenceObserver extends KIMObserver
        implements IPresenceObserver, NetworkSerializable, NetworkDeserializable {

    private IDirectInstantiator objectContextualizer;
    private IConcept            originalConcept;

    class PresenceComputer extends AbstractStateContextualizer {

        BitSet             index  = null;
        IGridMask          mask   = null;
        boolean            inited = false;
        IDirectObservation context;
        private boolean    exists;
        List<IShape>       shapes;

        protected PresenceComputer(IMonitor monitor) {
            super(monitor);
        }

        @Override
        public void setContext(Map<String, Object> parameters, IModel model, IProject project, IProvenance.Artifact provenance)
                throws KlabValidationException {
            if (objectContextualizer != null) {
                objectContextualizer.setContext(parameters, model, project, provenance);
            }
            super.setContext(parameters, model, project, provenance);
        }

        @Override
        public Map<String, IObservation> define(String name, IObserver observer, IActiveDirectObservation contextSubject, IResolutionScope context, Map<String, IObservableSemantics> expectedInputs, Map<String, IObservableSemantics> expectedOutputs, boolean isLastInChain, IMonitor monitor)
                throws KlabException {

            if (objectContextualizer != null) {
                objectContextualizer
                        .initialize((IActiveSubject) contextSubject, context, observer
                                .getModel(), expectedInputs, expectedOutputs, monitor);
            }

            this.context = contextSubject;

            return super.define(name, observer, contextSubject, context, expectedInputs, expectedOutputs, isLastInChain, monitor);
        }

        @Override
        public Map<String, Object> initialize(int index, Map<String, Object> inputs)
                throws KlabException {
            return MapUtils
                    .ofWithNull(getStateName(), processState(index, ITransition.INITIALIZATION));
        }

        private Object processState(int stateIndex, ITransition transition)
                throws KlabException {

            if (!inited) {

                Map<String, IObservation> objects = null;
                this.exists = false;

                IConcept originalConcept = Observables
                        .getInherentType(getObservable().getType());

                if (originalConcept != null) {

                    if (getContextObservation()
                            .getResolvedCoverage(originalConcept) != null
                            && getContextObservation() instanceof ISubject) {

                        if (NS.isThing(originalConcept)) {
                            for (ISubject s : ((ISubject) getContextObservation())
                                    .getSubjects()) {
                                if (s.getObservable().getType().is(originalConcept)) {
                                    if (objects == null) {
                                        objects = new HashMap<>();
                                    }
                                    objects.put(s.getName(), s);
                                }
                            }
                        } else if (NS.isEvent(originalConcept)) {
                            for (IEvent s : ((ISubject) getContextObservation())
                                    .getEvents()) {
                                if (s.getObservable().getType().is(originalConcept)) {
                                    if (objects == null) {
                                        objects = new HashMap<>();
                                    }
                                    objects.put(s.getName(), s);
                                }
                            }

                        }

                    }

                } else {
                    if (objectContextualizer != null) {
                        if (objectContextualizer instanceof ISubjectInstantiator) {
                            objects = ((ISubjectInstantiator) objectContextualizer)
                                    .createSubjects((IActiveSubject) getContextObservation(), transition, new HashMap<>());
                        } else if (objectContextualizer instanceof IEventInstantiator) {
                            objects = ((IEventInstantiator) objectContextualizer)
                                    .createEvents(transition, new HashMap<>());
                        }
                    }
                }

                IGrid grid = context.getScale().isSpatiallyDistributed()
                        ? context.getScale().getSpace().getGrid() : null;

                if (objects != null && objects.size() > 0) {

                    shapes = new ArrayList<>();

                    for (IObservation object : objects.values()) {

                        /*
                         * TODO attach a listener to the object to set inited to false if
                         * the object goes away - in that case, it needs to rescan all the
                         * objects from the main subject, keeping a list of the IDs. For
                         * now, new things come in but old things do not disappear.
                         */
                        if (object.getScale().getSpace() != null
                                && context.getScale().getSpace() != null) {

                            IShape shape = object.getScale().getSpace().getShape();
                            if (shape != null) {
                                shapes.add(shape);
                            }
                        } else {
                            this.exists = true;
                            break;
                        }
                    }
                }

                if (grid != null) {
                    this.mask = KLAB.MFACTORY.createMask(shapes, grid);
                } else if (context.getScale().isSpatiallyDistributed()) {
                    this.index = new BitSet((int) context.getScale().getSpace()
                            .getMultiplicity());
                }

                inited = true;
            }

            if (this.exists) {
                return Boolean.TRUE;
            } else if (mask != null) {

                int sfs = getScale().getExtentOffset(getScale().getSpace(), stateIndex);
                return mask.isActive(sfs);

            } else if (index != null) {
                int sfs = getScale().getExtentOffset(getScale().getSpace(), stateIndex);
                ISpatialExtent ext = getScale().getSpace().getExtent(sfs);
                if (shapes != null && !shapes.isEmpty()) {
                    for (IShape s : shapes) {
                        // TODO need simple shape intersection in spatial extent
                        throw new KlabUnsupportedOperationException("intersection of non-grid extents unsupported (ask for it, small task)");
                        // if (ext.intersects(s)) {
                        // return true;
                        // }
                    }
                }
                return false;
            }

            return Boolean.FALSE;
        }

        @Override
        public Map<String, Object> compute(int index, ITransition transition, Map<String, Object> inputs)
                throws KlabException {
            return MapUtils.ofWithNull(getStateName(), processState(index, transition));
        }

        @Override
        public boolean isProbabilistic() {
            return false;
        }

        @Override
        public String getLabel() {
            return "presence calculation";
        }

    }

    public class PresenceMediator extends AbstractMediator {

        boolean errorsPresent;

        protected PresenceMediator(IMonitor monitor) {
            super(monitor);
        }

        @Override
        public Object mediate(Object object) {

            Object val = null;

            // not much to say
            if (object instanceof Boolean) {
                return object;
            }

            if (object == null
                    || (object instanceof Number
                            && Double.isNaN(((Number) object).doubleValue()))) {
                return null;
            }

            if (object instanceof IndexedCategoricalDistribution) {

                /*
                 * TODO try to mediate distributions
                 */
                return !(NumberUtils
                        .equal(((IndexedCategoricalDistribution) object).getMean(), 0));

            } else if (object instanceof Number) {
                return !(NumberUtils.equal(((Number) object).doubleValue(), 0));
            } else {
                try {
                    val = object.toString().equals("NaN") ? Double.NaN : Double
                            .parseDouble(object.toString());
                    return !(NumberUtils.equal(((Number) val).doubleValue(), 0));
                } catch (Exception e) {
                    if (!errorsPresent) {
                        monitor.error("cannot interpret value: " + object
                                + " as a number");
                        errorsPresent = true;
                    }
                    return null;
                }
            }
        }

        @Override
        public String getLabel() {
            return "convert to presence";
        }

    }

    public KIMPresenceObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        validate(context);
    }

    public KIMPresenceObserver(IObservableSemantics observable) {
        IConcept oob = Observables.makePresence(observable.getType());
        this.observable = new ObservableSemantics(oob, KLAB
                .c(NS.PRESENCE_OBSERVATION), observable
                        .getFormalName());
    }

    KIMPresenceObserver(IConcept observable) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
    }

    public KIMPresenceObserver(IObserver observer) {
        super(observer);
        IConcept oob = Observables.makePresence(observable.getType());
        this.observable = new ObservableSemantics(oob, KLAB
                .c(NS.PRESENCE_OBSERVATION), observable
                        .getFormalName());
        if (observer instanceof KIMPresenceObserver) {
            isIndirect = ((KIMPresenceObserver) observer).isIndirect;
        }
    }

    /**
     * Only for the deserializer
     */
    public KIMPresenceObserver() {
    }

    @Override
    public IConcept getObservationType() {
        return KLAB.c(NS.PRESENCE_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMScope context, IConcept concept) {

        if (!concept.is(KLAB.c(NS.CORE_PRESENCE))) {
            this.originalConcept = concept;
        }

        IConcept ret = concept;
        if (isIndirect) {
            try {
                ret = Observables.makePresence(concept);
            } catch (Throwable e) {
                // will be null and caught later.
                ret = null;
            }
            if (ret == null) {
                context.error(concept
                        + ": presence is only assessed for things or agents. Use direct form (without 'of') for observables that are presence qualities.", getFirstLineNumber());
                return concept;
            }
        } else if (!concept.is(KLAB.c(NS.CORE_PRESENCE))) {
            context.error(concept
                    + ": the observable in this statement must be a presence already. Use the indirect form (with 'of') to annotate as the presence of subject observables.", getFirstLineNumber());
        }
        return ret;
    }

    public static String asString(IPresenceObserver observer) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IObserver copy() {
        return new KIMPresenceObserver(this);
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor)
            throws KlabException {
        return observer instanceof IPresenceObserver ? null
                : new PresenceMediator(monitor);
    }

    @Override
    public String toString() {
        return "PRS/" + getObservable();
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        isIndirect = bean.isIndirect();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass
                .isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        ret.setIndirect(isIndirect);
        return (T) ret;
    }

    @Override
    public IStateContextualizer getDataProcessor(IMonitor monitor)
            throws KlabValidationException {
        return objectContextualizer == null ? new PresenceMediator(monitor)
                : new PresenceComputer(monitor);
    }

    @Override
    public List<IObservableSemantics> getAlternativeObservables() {
        IConcept c = Observables.getInherentType(getObservable().getType());
        if (c != null && NS.isObservable(c)) {
            return Collections.singletonList(new ObservableSemantics(c));
        }
        return null;
    }

    @Override
    public ICoverage acceptAlternativeContextualizer(IContextualizer contextualizer, ICoverage coverage) {
        if (contextualizer instanceof IDirectInstantiator) {
            objectContextualizer = (IDirectInstantiator) contextualizer;
            return Coverage.FULL(scale);
        }
        return Coverage.EMPTY;

    }

    @Override
    public String getDefinition() {
        return "presence " + (isIndirect ? "of " : "")
                + (originalConcept == null
                        ? Observables.getDeclaration(getObservable().getType(), getNamespace().getProject())
                        : Observables.getDeclaration(originalConcept, getNamespace().getProject()));
    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return false;
    }

}
