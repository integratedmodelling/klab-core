/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;

public class KIMObservableSemantics extends ObservableSemantics implements ILanguageObject {

    int      firstLine, lastLine;
    boolean  isNothing;
    // not part of the IObservable model - used for convenience to store optional
    // status in model observables.
    boolean  optional;

    /*
     * holds the "as" role in model observables, because we don't know where else to put
     * it. Only used internally.
     */
    IConcept interpretAs;

    public KIMObservableSemantics(KIMScope context, KIMKnowledge concept) {

        firstLine = concept.getFirstLineNumber();
        lastLine = concept.getLastLineNumber();
        this.setType(concept, context);
        isNothing = concept.isNothing();

        if (isNothing && concept.getObjectDeclarations().size() > 0) {
            /*
             * ASK OBSERVER TO CREATE TYPE
             */
            IObserver oo = context.get(IObserver.class);
            IConcept obser = null;
            if (oo == null || (obser = ((KIMObserver) oo).getObservableConcept(context, concept)) == null) {
                context.error("using direct objects as observables is not supported here", firstLine);
            } else {
                this.setType(obser);
            }
        }

        // I know
        if (!concept.isNothing() && NS.isNothing(concept)) {
            context.error(concept + " has been declared as 'nothing' and cannot be observed", firstLine);
        }

        if (concept.isNothing()) {
            context.error("cannot use this concept", firstLine);
        } else if (this.getObservationType() == null && NS.isDirect(concept.getConcept())) {
            this.setObservationType(KLAB.c(NS.DIRECT_OBSERVATION));
        }
    }

    public KIMObservableSemantics(IObserver observer) {
        super((ObservableSemantics) observer.getObservable());
        this.setObserver(observer);
    }

    public KIMObservableSemantics(Object literal, IObserver observer) {
        super((ObservableSemantics) observer.getObservable());
        this.setObserver(observer);
        this.setModel(new KIMModel(literal, (KIMObserver) observer));
    }

    public KIMObservableSemantics(IKnowledgeObject knowledge, IObserver observer, KIMScope context) {
        this(observer);
        this.setType(knowledge, context);
    }

    public KIMObservableSemantics(IObservableSemantics semantics) {
        super((ObservableSemantics)semantics);
        if (semantics instanceof KIMObservableSemantics) {
            firstLine = ((KIMObservableSemantics)semantics).firstLine;
            lastLine = ((KIMObservableSemantics)semantics).lastLine;
        }
    }
    
    public KIMObservableSemantics(IConcept observable) {
        super(observable.getType(), NS.getObservationTypeFor(observable.getType()), CamelCase
                .toLowerCamelCase(NS.getDisplayName(observable.getType()), '_'));
    }

    @Override
    public int getFirstLineNumber() {
        return firstLine;
    }

    @Override
    public int getLastLineNumber() {
        return lastLine;
    }

    public boolean isNothing() {
        return isNothing;
    }

    public IConcept getStatedRole() {
        return interpretAs;
    }

    /**
     * This is only relevant in model output observables, which may be tagged optional by
     * the user. Not nice to put it in a higher-level object, but it's the economical way
     * to do it.
     */
    public boolean isOptional() {
        return optional;
    }

    public void setOptional(boolean optional) {
        this.optional = optional;
    }

}
