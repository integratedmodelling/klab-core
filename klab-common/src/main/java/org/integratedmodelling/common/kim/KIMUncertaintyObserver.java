/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.integratedmodelling.api.data.IRankingScale;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IUncertaintyObserver;
import org.integratedmodelling.api.modelling.IValueMediator;
import org.integratedmodelling.api.modelling.contextualization.IStateContextualizer;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.RankingScale;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;
import org.integratedmodelling.kim.kim.Observer;

public class KIMUncertaintyObserver extends KIMNumericObserver
        implements IUncertaintyObserver, NetworkSerializable, NetworkDeserializable {

    IConcept      originalConcept;
    IRankingScale rankingScale = new RankingScale();

    public class UncertaintyMediator extends NumericMediator {

        private IUncertaintyObserver other;
        private boolean              convertScale;

        protected UncertaintyMediator(IUncertaintyObserver other, IMonitor monitor) {
            super(monitor);
            this.other = other;
            this.convertScale = scale != null && other.getRankingScale() != null
                    && !scale.equals(other.getRankingScale());
        }

        @Override
        public Object mediate(Object object) throws KlabException {

            Number val = getValueAsNumber(object, other);
            if (!Double.isNaN(val.doubleValue()) && convertScale) {
                val = rankingScale.convert(val, other.getRankingScale());
            }
            return super.mediate(val);
        }

        @Override
        public String getLabel() {
            return (rankingScale.equals(other.getRankingScale()) ? ""
                    : "rescale " + other.getRankingScale() + " to " + rankingScale) +
                    (discretization == null ? ""
                            : ((rankingScale.equals(other.getRankingScale()) ? "" : "->") + "discretize"));
        }
    }

    @Override
    public boolean isExtensive(IConcept extent) {
        return false;
    }

    public KIMUncertaintyObserver(KIMScope context, KIMModel model, Observer statement) {
        super(context, model, statement);
        // if (observable != null) {
        // ((ObservableSemantics) observable)
        // .setType(this.getObservedType(context, this.observable.getType()));
        // }
        validate(context);
    }

    KIMUncertaintyObserver(IConcept observable) {
        // NO ERROR CHECKING - USED INTERNALLY
        this.observable = new KIMObservableSemantics(observable);
    }

    /**
     * Only for the deserializer
     */
    public KIMUncertaintyObserver() {
    }

    public KIMUncertaintyObserver(IObserver observer) {
        super(observer);
        if (!(observer instanceof KIMUncertaintyObserver)) {
            throw new KlabRuntimeException("cannot initialize an uncertainty observer from a "
                    + observer.getClass().getCanonicalName());
        }
        this.isIndirect = ((KIMUncertaintyObserver) observer).isIndirect;
        this.originalConcept = ((KIMUncertaintyObserver) observer).originalConcept;
    }

    @Override
    public IObserver copy() {
        return new KIMUncertaintyObserver(this);
    }

    @Override
    public IKnowledge getOriginalConcept() {
        return originalConcept;
    }

    @Override
    public IConcept getObservationType() {
        // TODO restrict?
        return KLAB.c(NS.UNCERTAINTY_OBSERVATION);
    }

    @Override
    public IConcept getObservedType(KIMScope context, IConcept concept) {

        IConcept ret = concept;

        if (isIndirect) {
            originalConcept = concept;
            ret = Observables.makeUncertainty(concept);
            if (ret == null) {
                context.error(concept
                        + ": uncertainties are assigned to observables. Use the direct form (without 'of') for observables that are already probabilities.", getFirstLineNumber());
                return concept;
            }
        } else if (!concept.is(KLAB.c(NS.CORE_UNCERTAINTY))) {
            context.error(concept
                    + ": the observable in this statement must be an uncertainty. Use the indirect form (with 'of') for the uncertainty of another observable.", getFirstLineNumber());
        }
        return ret;
    }

    @Override
    public IStateContextualizer getMediator(IObserver observer, IMonitor monitor) throws KlabException {

        if (!(observer instanceof IUncertaintyObserver)) {
            throw new KlabValidationException("uncertainty observers can only mediate other uncertainty observers");
        }

        IUncertaintyObserver other = (IUncertaintyObserver) observer;

        if (discretization != null) {
            if (((IUncertaintyObserver) observer).getDiscretization() != null) {
                monitor.warn(getObservable().getType()
                        + ": discretized uncertainties should not mediate other discretized uncertainties: value will be undiscretized, then discretized again");
            }
            return new UncertaintyMediator((IUncertaintyObserver) observer, monitor);
        }

        if ((other.getRankingScale() == null && rankingScale == null) || (rankingScale != null
                && other.getRankingScale() != null && rankingScale.equals(other.getRankingScale())))

        {
            return null;
        }

        return new UncertaintyMediator(other, monitor);

    }

    @Override
    public String toString() {
        return "UNC/" + getObservable();
    }

    @Override
    public void deserialize(IModelBean object) {

        if (!(object instanceof org.integratedmodelling.common.beans.Observer)) {
            throw new KlabRuntimeException("cannot deserialize a Prototype from a "
                    + object.getClass().getCanonicalName());
        }
        org.integratedmodelling.common.beans.Observer bean = (org.integratedmodelling.common.beans.Observer) object;
        super.deserialize(bean);
        if (bean.getComparisonKnowledge() != null) {
            originalConcept = Knowledge.parse(bean.getComparisonKnowledge());
        }
        isIndirect = bean.isIndirect();
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        if (!desiredClass.isAssignableFrom(org.integratedmodelling.common.beans.Observer.class)) {
            throw new KlabRuntimeException("cannot serialize a Prototype to a "
                    + desiredClass.getCanonicalName());
        }

        org.integratedmodelling.common.beans.Observer ret = new org.integratedmodelling.common.beans.Observer();

        super.serialize(ret);
        if (originalConcept != null) {
            ret.setComparisonKnowledge(((Knowledge) originalConcept).asText());
        }
        ret.setIndirect(isIndirect);

        return (T) ret;
    }

    @Override
    public IRankingScale getRankingScale() {
        return rankingScale;
    }

    @Override
    public String getDefinition() {
        // TODO Auto-generated method stub
        return "uncertainty " + (isIndirect ? "of " : "")
                + (originalConcept == null
                        ? Observables.getDeclaration(getObservable().getType(), getNamespace().getProject())
                        : Observables.getDeclaration(originalConcept, getNamespace().getProject()));
    }

    @Override
    public IValueMediator getValueMediator() {
        return rankingScale;
    }

}
