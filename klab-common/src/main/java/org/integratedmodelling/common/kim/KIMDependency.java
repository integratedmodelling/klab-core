/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.kim.kim.Dependency;
import org.integratedmodelling.lang.Axiom;

public class KIMDependency extends KIMModelObject implements IDependency {

    boolean              isNothing;
    IObservableSemantics observable;
    String               formalName;
    IProperty            property;
    boolean              isOptional;
    boolean              isDistributing;
    boolean              isGeneric;
    Object               contextModel;
    IConcept             interpretAs;
    IExpression          whereCondition;
    IConcept             distributionContext;
    Set<IConcept>        resolutionContext = new HashSet<>();

    public KIMDependency(KIMDependency other, IObservableSemantics observable, String formalName) {
        super(other);
        this.isNothing = other.isNothing;
        this.property = other.property;
        this.isOptional = other.isOptional;
        this.isDistributing = other.isDistributing;
        this.isGeneric = other.isGeneric;
        this.interpretAs = other.interpretAs;
        this.whereCondition = other.whereCondition;
        this.distributionContext = other.distributionContext;
        this.observable = observable;
        this.formalName = formalName;
        this.resolutionContext.addAll(other.resolutionContext);
    }

    public KIMDependency(ObservableSemantics observable, String fname, IProperty property, boolean isOptional,
            Object contextModel, IResolutionScope scope) {
        super(null);
        this.formalName = fname;
        this.observable = observable;
        this.property = property;
        this.isOptional = isOptional;
        this.contextModel = contextModel;

        // if (NS.isObject(this.observable)) {
        // Observable oob = new Observable(observable);
        // IModel depModel = new KIMModel(scope.getResolutionNamespace(), oob);
        // this.observable = new Observable(depModel, fname);
        // }
    }

    public KIMDependency(ObservableSemantics observable, String fname, boolean isOptional,
            IResolutionScope scope) {
        super(null);
        this.formalName = fname;
        this.observable = observable;
        // this.property = property;
        this.isOptional = isOptional;
        // this.contextModel = contextModel;

        // if (NS.isObject(this.observable)) {
        // Observable oob = new Observable(observable);
        // IModel depModel = new KIMModel(scope.getResolutionNamespace(), oob);
        // this.observable = new Observable(depModel, fname);
        // }
    }

    public KIMDependency(KIMScope context, Dependency statement, KIMObservingObject observer) {
        super(context, statement, observer);

        /**
         * TODO see logics in old alignDependencies()
         */

        IKnowledgeObject obs = null;
        IObserver observ = null;
        Object literalValue = null;
        boolean bareObservable = false;
        IModel depModel = null;
        IConcept localConcept = null;

        this.isGeneric = statement.isGeneric();

        if (statement.getRequiredDomains() != null && !statement.getRequiredDomains().isEmpty()) {
            for (String s : statement.getRequiredDomains()) {
                IPrototype fun = KLAB.ENGINE.getFunctionPrototype(s);
                boolean found = false;
                if (fun != null) {
                    for (IConcept c : fun.getReturnTypes()) {
                        if (c.is(KLAB.c(NS.EXTENT))) {
                            this.resolutionContext.add(c);
                            found = true;
                        }
                    }
                }
                if (!found) {
                    context.error(s + " does not specify a valid extent function", lineNumber(statement));
                }
            }
        }

        this.isOptional = statement.getOptional() != null && statement.getOptional().equals("optional");
        if (statement.getConcept() != null) {

            bareObservable = true;

            obs = new KIMKnowledge(context.get(KIMScope.Type.DEPENDENCY_OBSERVABLE), statement
                    .getConcept(), null);

            if (obs.isNothing()) {
                return;
            }

            /*
             * must be direct, class (then check for trait, otherwise trait is an error)
             * or be a quality and have a model in the ns.
             */
            if (NS.isQuality(obs) || NS.isTrait(obs)) {

                /*
                 * needs a unique model in namespace; this will automatically complain if
                 * not found
                 */
                depModel = observer
                        .makeObservableModel(context, obs
                                .getType(), lineNumber(statement.getConcept()), NS.isQuality(obs));

            } else if (!(NS.isClass(obs) || NS.isTrait(obs)) && !NS.isDirect(obs)) {

                /*
                 * bitch about it
                 */
                context.error("bare concept dependencies must be classifications, direct observables (subjects, "
                        + "processes, events) or qualities previously observed by a model in this namespace", lineNumber(statement
                                .getConcept()));

                return;
            }

            /*
             * if we still have no model and no error, must be a legal class/trait or
             * direct: make an observable from obs
             */
            if (depModel == null) {

                KIMObservableSemantics oob = new KIMObservableSemantics(context
                        .get(KIMScope.Type.DEPENDENCY_OBSERVABLE), (KIMKnowledge) obs);

                depModel = KIMModel.makeModel(context, oob);
            }

        } else if (statement.getInlineModel() != null) {

            String localId = null;

            if (statement.getInlineModel().getValue() != null) {
                literalValue = KIM.processLiteral(statement.getInlineModel().getValue());
            } else if (statement.getInlineModel().getConceptId() != null) {
                localId = statement.getInlineModel().getConceptId();
                localConcept = context.getNamespace().getOntology().getConcept(localId);
            }

            observ = KIMObservingObject.defineObserver(context, null, statement.getInlineModel()
                    .getObserver());

            if (observ != null && observ.getObservable() != null) {

                obs = new KIMKnowledge(context.get(KIMScope.Type.DEPENDENCY_OBSERVABLE), observ
                        .getObservable()
                        .getType());

                if (localId != null) {
                    if (localConcept != null && !localConcept.is(obs.getConcept())) {
                        context.error("local concept " + localId
                                + " is defined in this namespace with an incompatible type", lineNumber(statement
                                        .getInlineModel()));
                    } else {
                        List<IAxiom> ax = new ArrayList<>();
                        ax.add(Axiom.ClassAssertion(localId));
                        ax.add(Axiom.SubClass(obs.getConcept().toString(), localId));
                        context.getNamespace().getOntology().define(ax);
                        localConcept = context.getNamespace().getOntology().getConcept(localId);
                    }
                }
            }
        }

        /**
         * user specified an observer
         */
        if (!bareObservable && observ != null) {

            if (obs != null && !obs.isNothing()) {

                depModel = new KIMModel(obs.getType(), observ, statement.getFormalname());

                if (literalValue != null) {

                    /*
                     * set depModel to literal model with observ
                     */
                    ((KIMModel) depModel).inlineValue = literalValue;
                }
            }
        }

        if (depModel != null) {
            this.observable = new ObservableSemantics(depModel, statement.getFormalname());
            if (this.observable.getType() == null) {
                // BS concepts, pre-existing error, stop here
                return;
            }
            // hm - if we leave it, it will screw up resolution. Must review all this.
            if (NS.isObject(this.observable)) {
                ((ObservableSemantics) this.observable).setModel(null);
            }

            if (localConcept != null) {
                ((ObservableSemantics) this.observable).setLocalConcept(localConcept);
                if (depModel.getObserver() != null) {
                    /*
                     * store in observer's observable - dataflows will only use the
                     * observer now so we need to save it there.
                     */
                    ((ObservableSemantics) depModel.getObservable()).setLocalConcept(localConcept);
                    ((ObservableSemantics) depModel.getObserver().getObservable())
                            .setLocalConcept(localConcept);
                }
            }

        } else {

            if (obs == null) {
                return;
            }

            // everything else should have been dealt with
            this.observable = new ObservableSemantics(obs.getType(), KLAB
                    .c(NS.DIRECT_OBSERVATION), statement
                            .getFormalname());

            if (localConcept != null) {
                ((ObservableSemantics) this.observable).setType(localConcept);
            }
        }

        if (statement.getTraitConferred() != null) {
            IConcept trait = KLAB.KM.getConcept(statement.getTraitConferred());
            if (trait == null) {
                context.error("unknown concept " + statement.getTraitConferred()
                        + ": must be a role or trait", lineNumber(statement));
            } else if (!NS.isTrait(trait) && !NS.isRole(trait)) {
                context.error("concept " + statement.getTraitConferred()
                        + " is not a role or trait", lineNumber(statement));
            } else {
                this.interpretAs = trait;
            }
        }

        if (statement.getDcontext() != null) {
            IKnowledgeObject dctx = new KIMKnowledge(context
                    .get(KIMScope.Type.DEPENDENCY_OBSERVABLE), statement
                            .getDcontext(), null);
            if (!((statement.isEach() && NS.isCountable(dctx))
                    || (statement.isEach() && NS.isProcess(dctx)))) {
                context.error("distributing dependencies must be on countable observables (subjects or events) if 'each' is given, or on processes if not", lineNumber(statement
                        .getDcontext()));
            } else {
                isDistributing = true;
                distributionContext = dctx.getConcept();
                if (statement.getWhereCondition() != null) {
                    whereCondition = new KIMExpression(context
                            .get(KIMScope.Type.DISTRIBUTION_DEPENDENCY_CONDITION), statement
                                    .getWhereCondition());
                }
            }
        }

        isOptional = statement.getOptional() != null && statement.getOptional().equals("optional");
        if (statement.getFormalname() != null) {
            formalName = statement.getFormalname();
        }

        if (isGeneric && observable != null && observable.getType() != null) {
            if (!NS.isCountable(observable.getType()) || observable.getType().isAbstract()) {
                context.error("generic dependencies ('every') are only allowed with non-abstract, countable observables", lineNumber(statement));
            }
        }

    }

    @Override
    public IObservableSemantics getObservable() {
        return observable;
    }

    @Override
    public boolean isOptional() {
        return isOptional;
    }

    @Override
    public IProperty getProperty() {
        return property;
    }

    @Override
    public IConcept reinterpretAs() {
        return interpretAs;
    }

    @Override
    public Object getContextModel() {
        return contextModel;
    }

    @Override
    public String getFormalName() {
        return formalName;
    }

    public boolean isNothing() {
        return isNothing;
    }

    public String name() {
        if (observable != null && observable.getType() != null) {
            if (observable.getFormalName() != null) {
                formalName = observable.getFormalName();
            } else {
                ((ObservableSemantics) observable)
                        .setFormalName(CamelCase.toLowerCase(NS.getDisplayName(observable.getType()), '-'));
                formalName = observable.getFormalName();
            }
            if (observable.getModel() != null) {
                ((KIMModel) observable.getModel()).name(formalName);
            }
        } else {
            formalName = "error";
        }

        return formalName;
    }

    @Override
    public boolean isDistributing() {
        return isDistributing;
    }

    @Override
    public IExpression getWhereCondition() {
        return whereCondition;
    }

    @Override
    public IConcept getDistributionContext() {
        return distributionContext;
    }

    @Override
    public IConcept getType() {
        return observable.getType();
    }

    @Override
    public String toString() {
        return "D/" + observable;
    }

    @Override
    public boolean isGeneric() {
        return isGeneric;
    }
}
