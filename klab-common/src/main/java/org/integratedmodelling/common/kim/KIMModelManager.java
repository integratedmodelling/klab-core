/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.factories.IModelManager;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.runtime.ISession;
import org.integratedmodelling.collections.Path;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.data.IntelligentMap;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.exceptions.KlabException;

public class KIMModelManager implements IModelManager {

    Map<String, INamespace>                   namespaces        = new HashMap<>();
    IntelligentMap<Class<? extends ISubject>> subjectClasses    = new IntelligentMap<>();
    Map<String, IKnowledge>                   exportedKnowledge = new HashMap<>();
    List<Class<?>>                            kimImports        = new ArrayList<>();

    public void addNamespace(INamespace ns) {
        namespaces.put(ns.getId(), ns);
    }

    public void addExports(Map<String, IKnowledge> map) {
        exportedKnowledge.putAll(map);
    }

    @Override
    public INamespace getNamespace(String ns) {
        return namespaces.get(ns);
    }

    @Override
    public void releaseNamespace(String namespaceId) {

        try {
            KLAB.MFACTORY.cleanNamespaceArtifacts(namespaceId);
        } catch (KlabException e) {
            KLAB.error("error cleaning persistent artifacts from " + namespaceId + ": " + e.getMessage());
        }

        synchronized (namespaces) {
            INamespace ns = namespaces.get(namespaceId);
            if (ns != null) {
                if (ns.getOntology() != null) {
                    KLAB.KM.releaseOntology(ns.getOntology().getConceptSpace());
                }
                namespaces.remove(namespaceId);
            }
        }
    }

    @Override
    public Collection<INamespace> getNamespaces() {
        return namespaces.values();
    }

    @Override
    public List<INamespace> getScenarios() {

        List<INamespace> ret = new ArrayList<>();
        for (INamespace ns : namespaces.values()) {
            if (ns.isScenario()) {
                ret.add(ns);
            }
        }
        return ret;
    }

    @Override
    public IModelObject findModelObject(String name) {
        String ns = Path.getLeading(name, name.contains(":") ? ':' : '.');
        if (ns == null) {
            return null;
        }
        INamespace namespace = namespaces.get(ns);
        return namespace == null ? null
                : namespace.getModelObject(Path.getLast(name, name.contains(":") ? ':' : '.'));
    }

    @Override
    public boolean isModelFile(File f) {
        return KIM.FILE_EXTENSIONS.contains(MiscUtilities.getFileExtension(f.toString()));
    }

    @Override
    public INamespace getLocalNamespace() {
        return requireNamespace("local." + KLAB.NAME);
    }

    public INamespace requireNamespace(String id) {
        INamespace ret = namespaces.get(id);
        if (ret == null) {
            namespaces.put(id, ret = new KIMNamespace(id));
        }
        return ret;
    }

    static int                  counter                  = 1;
    private static final String OBSERVATION_NAMESPACE_ID = "imobs";

    public INamespace getUserNamespace(ISession session) throws KlabException {

        /*
         * FIXME use user data
         */
        String key = session.getId();
        if (namespaces.containsKey(key)) {
            return namespaces.get(key);
        }

        INamespace ret = new KIMNamespace(key);
        namespaces.put(key, ret);
        return ret;
    }

//    /**
//     * Called only when the project is established to contain a namespace for knowledge,
//     * i.e. it's not a worldview. Won't check, so don't call outside the current logics.
//     * 
//     * @param project
//     * @return
//     * @throws KlabException
//     */
//    public INamespace getProjectNamespace(IProject project) throws KlabException {
//
//        INamespace ret = ((Project) project).getProjectNamespace();
//
//
//
//        return ret;
//    }

    @Override
    public INamespace getObservationNamespace() {
        INamespace ret = getNamespace(OBSERVATION_NAMESPACE_ID);
        if (ret == null) {
            ret = new KIMNamespace(OBSERVATION_NAMESPACE_ID);
            namespaces.put(OBSERVATION_NAMESPACE_ID, ret);
        }
        return ret;
    }

    @Override
    public void registerSubjectClass(String concept, Class<? extends ISubject> cls) {
        if (!subjectClasses.containsKey(concept)) {
            subjectClasses.put(concept, cls);
        }
    }

    @Override
    public IKnowledge getExportedKnowledge(String id) {
        return exportedKnowledge.get(id);
    }

    @Override
    public Class<? extends ISubject> getSubjectClass(IConcept type) {
        Class<? extends ISubject> ret = subjectClasses.get(type);
        return ret;
    }

    @Override
    public void releaseAll() {
        ArrayList<String> nss = new ArrayList<>(namespaces.keySet());
        for (String id : nss) {
            INamespace ns = namespaces.get(id);
            if (!((Ontology) ns.getOntology()).isInternal()) {
                releaseNamespace(id);
            }
        }
    }

    /**
     * Dump a set of CSV-compatible strings with all the exported names.
     * 
     * @return all exported knowledge concept-ID pairs in comma-separated format
     */
    public List<String> dumpExports() {

        List<String> ret = new ArrayList<>();
        List<String> nms = new ArrayList<>(exportedKnowledge.keySet());

        Collections.sort(nms);
        for (String id : nms) {
            ret.add(id + "," + exportedKnowledge.get(id));
        }

        return ret;
    }

    public void registerNamespace(IOntology ontology, String id) {
        addNamespace(new KIMNamespace(id, null, ontology));
    }

    @Override
    public void registerKimImportClass(Class<?> cls) {
        kimImports.add(cls);
    }

    @Override
    public Collection<Class<?>> getKimImports() {
        return kimImports;
    }

}
