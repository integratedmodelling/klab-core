/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.integratedmodelling.api.data.IList;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.errormanagement.ICompileError;
import org.integratedmodelling.api.errormanagement.ICompileInfo;
import org.integratedmodelling.api.errormanagement.ICompileNotification;
import org.integratedmodelling.api.errormanagement.ICompileWarning;
import org.integratedmodelling.api.knowledge.IAxiom;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.IOntology;
import org.integratedmodelling.api.metadata.IDocumentation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IKnowledgeObject;
import org.integratedmodelling.api.modelling.IModelObject;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.ui.IBookmarkManager;
import org.integratedmodelling.common.client.referencing.BookmarkManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.errormanagement.CompileError;
import org.integratedmodelling.common.errormanagement.CompileInfo;
import org.integratedmodelling.common.errormanagement.CompileWarning;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.owl.OWL;
import org.integratedmodelling.common.owl.Ontology;
import org.integratedmodelling.common.reporting.Documentation;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.utils.MiscUtilities;
import org.integratedmodelling.common.utils.NameGenerator;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Roles;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.kim.kim.Annotation;
import org.integratedmodelling.kim.kim.Coverage;
import org.integratedmodelling.kim.kim.Import;
import org.integratedmodelling.kim.kim.Namespace;

public class KIMNamespace extends KIMLanguageObject implements INamespace {

    String                       id;
    public ArrayList<IAxiom>     axioms              = new ArrayList<>();
    public HashSet<IAxiom>       axiomCatalog        = new HashSet<>();
    List<INamespace>             imported            = new ArrayList<>();
    Set<String>                  importedIds         = new HashSet<>();
    Set<String>                  authorityNamespaces = new HashSet<>();
    List<String>                 disjoint            = new ArrayList<>();
    List<IModelObject>           modelObjects        = new ArrayList<>();
    Map<String, Object>          symbolTable         = new HashMap<>();
    Map<String, IModelObject>    modelObjectsById    = new HashMap<>();
    List<IFunctionCall>          extentFunctions     = new ArrayList<>();
    List<ICompileError>          errors              = new ArrayList<>();
    List<ICompileInfo>           info                = new ArrayList<>();
    List<ICompileWarning>        warnings            = new ArrayList<>();
    IMetadata                    metadata            = new Metadata();
    IOntology                    ontology;
    boolean                      isScenario;
    boolean                      isPrivate;
    boolean                      isInactive;
    boolean                      isTainted           = false;
    // milliseconds it took to read this up
    public long                  elapsedMs           = 0;

    private Map<String, Integer> modelNameCounts     = new HashMap<>();

    /**
     * true if the namespace represents a core ontology not read from a KIM file.
     */
    boolean                      isInternal;
    String                       description;
    IMetadata                    resolutionCriteria;
    IConcept                     domain;
    long                         timestamp;
    File                         resource;
    IProject                     project;
    int                          lastAxiom           = 0;

    /*
     * concept aliases exported to components - must have correspondent @export
     * annotations in namespace.
     */
    Map<String, IKnowledge>      exported            = new HashMap<>();

    // this and the next are only instantiated if the namespace is in a sidecar kim file
    // harvested by a crawler alongside a directory or file of interest.
    private String               sidecarFileName;
    private String               worldviewName;
    private Documentation        documentation;
    private boolean              isProjectKnowledge  = false;

    public KIMNamespace(String pth, File f, IOntology o) {
        super((EObject) null);
        id = pth;
        resource = f;
        ontology = o;
    }

    public boolean isSidecarFile() {
        return this.sidecarFileName != null;
    }

    public KIMNamespace(Namespace statement, List<Annotation> annotations, KIMScope ctx) {

        super(statement);
        ctx.accept(this);

        if (statement == null) {
            return;
        }

        this.isProjectKnowledge = ctx.isInScope(KIMScope.Type.PROJECT_DEFAULT_KNOWLEDGE);

        if (statement.isWorldviewBound()) {

            this.worldviewName = statement.getName();
            this.id = NameGenerator.newName("ksf");
            this.sidecarFileName = MiscUtilities.getFileName(ctx.getResource());
            if (!this.worldviewName.equals(KLAB.getWorldview())) {
                KLAB.warn("deactivating sidecar file to " + this.sidecarFileName
                        + " due to incompatible worldviews");
                this.isInactive = true;
            }

        } else {
            this.id = statement.getName();
        }

        if (id == null) {
            id = "ERROR";
        }

        this.timestamp = ctx.getTimestamp();
        this.resource = ctx.getResource();
        this.project = ctx.getProject();

        if (!this.isSidecarFile() && !this.isProjectKnowledge && !this.id.equals(ctx.getId())) {
            ctx.error("namespace id " + statement.getName()
                    + " does not correspond to its file location: expected id is "
                    + ctx.getId(), getFirstLineNumber());
        }

        this.isPrivate = statement.isPrivate();
        this.isInactive = statement.isInactive();
        this.isScenario = statement.isScenario();

        this.resolutionCriteria = statement.getMetadata() == null ? null
                : new KIMMetadata(ctx.get(KIMScope.Type.METADATA), statement.getMetadata(), null); // TODO

        if (statement.getDisjointNamespaces() != null) {
            disjoint.addAll(statement.getDisjointNamespaces().getNames());
        }

        if (statement.getDocumentation() != null) {

            this.documentation = new Documentation(new KIMMetadata(ctx
                    .get(KIMScope.Type.NAMESPACE_DOCUMENTATION), statement
                            .getDocumentation(), null));

            for (String e : this.documentation.getErrors()) {
                ctx.error(e, lineNumber(statement.getDocumentation()));
            }
        }

        if (statement.getImportList() != null) {
            for (Import imp : statement.getImportList().getImports()) {

                INamespace ins = null;

                if (ctx.getProject() == null && KLAB.MMANAGER != null) {
                    // just check if it was loaded before
                    ins = KLAB.MMANAGER.getNamespace(imp.getImported());
                } else {
                    try {
                        ins = ctx.getProject().findNamespaceForImport(imp.getImported(), ctx);
                    } catch (KlabException e) {
                        // just fail later
                    }
                }

                if (ins == null) {
                    ctx.error("imported namespace "
                            + imp.getImported()
                            + " not available for " + id
                            + (ctx.getProject() == null ? ": please load the namespace manually"
                                    : ": please add a dependency on a project that provides it"), lineNumber(imp));
                    continue;
                }

                imported.add(ins);
                importedIds.add(ins.getId());

                ((Ontology) getOntology()).addImport(ins.getOntology());

                /*
                 * add imported symbols as required, ensuring no conflicts arise. We
                 * import all symbols if we have a star, named symbols if we have a list,
                 * and just record the dependency for abstract knowledge if neither is
                 * true.
                 */
                if (imp.getImports() != null) {

                    IList objs = KIM.nodeToList(imp.getImports());

                    Object[] oos = objs.toArray();
                    for (int i = 0; i < oos.length; i++) {

                        String mod = oos[i].toString();
                        String fnm = oos[i].toString();

                        /*
                         * check for formal name
                         */
                        if (i < (oos.length - 1) && oos[i + 1].toString().equals("as")) {
                            fnm = oos[i + 2].toString();
                            i += 2;
                        }

                        Object mo = ins.getSymbolTable().get(mod);
                        if (mo == null) {
                            ctx.error("imported namespace "
                                    + imp.getImported()
                                    + " does not define a value for " + mod, lineNumber(imp));
                        } else {
                            symbolTable.put(fnm, mo);
                        }
                    }
                } else if (imp.isStar()) {

                    for (String mod : ins.getSymbolTable().keySet()) {
                        symbolTable.put(mod, ins.getSymbolTable().get(mod));
                    }
                }

            }
        }

        if (statement.isRootDomain()) {
            this.domain = KLAB.c(NS.CORE_DOMAIN);

        } else if (statement.getDomainConcept() != null) {

            this.domain = KLAB.KM.getConcept(statement.getDomainConcept());
            if (domain == null) {
                ctx.error("cannot find domain concept " + statement.getDomainConcept(), getFirstLineNumber());
            }
        }

        /*
         * process any constraining extents - may be variables (unimplemented) or
         * functions
         */
        if (statement.getCoverageList() != null) {
            for (Coverage cl : statement.getCoverageList().getCoverage()) {

                if (cl.getId() != null) {

                    Object o = symbolTable.get(cl.getId());
                    if (!(o instanceof IFunctionCall) /* TODO check return value */) {
                        ctx.error("symbol " + cl.getId()
                                + " does not resolve to an extent function", lineNumber(cl));
                    } else {
                        extentFunctions.add((IFunctionCall) o);
                    }

                } else if (cl.getFunction() != null) {

                    IFunctionCall function = new KIMFunctionCall(ctx.get(KIMScope.Type.FUNCTIONCALL), cl
                            .getFunction());
                    if (!function.returns(KLAB.c(NS.EXTENT))) {
                        ctx.error("function " + function.getId()
                                + " does not return an extent", lineNumber(cl
                                        .getFunction()));
                    } else {
                        extentFunctions.add(function);
                    }
                }
            }
        }

        /*
         * reset all roles that may previously have been attributed from this namespace.
         */
        Roles.removeRolesFor(this);

    }

    public KIMNamespace(String key) {
        super((EObject) null);
        id = key;
        try {

            ontology = KLAB.KM.createOntology(key, OWL.INTERNAL_ONTOLOGY_PREFIX);

        } catch (KlabException e) {
            throw new KlabRuntimeException(e);
        }
    }

    public KIMNamespace(String key, File file, IProject project) {
        this(key);
        try {

            ontology = KLAB.KM.createOntology(key, OWL.INTERNAL_ONTOLOGY_PREFIX);

        } catch (KlabException e) {
            throw new KlabRuntimeException(e);
        }
        this.resource = file;
        this.timestamp = file.lastModified();
        this.project = project;
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public long getTimeStamp() {
        return timestamp;
    }

    @Override
    public IConcept getDomain() {
        return domain;
    }

    @Override
    public List<IModelObject> getModelObjects() {
        return modelObjects;
    }

    /**
     * Return all first-class model objects and their first-class children (i.e. only
     * top-level models, direct observers, plus all knowledge WITH its children in a
     * flattened list).
     */
    @Override
    public List<IModelObject> getAllModelObjects() {

        List<IModelObject> ret = new ArrayList<>();
        for (IModelObject o : modelObjects) {
            storeObject(o, ret);
        }
        return ret;
    }

    private static void storeObject(IModelObject o, List<IModelObject> ret) {
        ret.add(o);
        if (o instanceof IKnowledgeObject) {
            for (IModelObject oo : o.getChildren()) {
                storeObject(oo, ret);
            }
        }
    }

    @Override
    public IModelObject getModelObject(String mod) {
        return modelObjectsById.get(mod);
    }

    @Override
    public IProject getProject() {
        return project;
    }

    @Override
    public Collection<INamespace> getImportedNamespaces() {
        return imported;
    }

    @Override
    public List<String> getTrainingNamespaces() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getLookupNamespaces() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale getCoverage(IMonitor monitor) {
        if (extentFunctions.size() > 0) {
            return KLAB.MFACTORY.createScale(extentFunctions, monitor);
        }
        return null;
    }

    @Override
    public boolean hasErrors() {
        return errors.size() > 0;
    }

    @Override
    public boolean hasWarnings() {
        return warnings.size() > 0;
    }

    @Override
    public Collection<ICompileNotification> getCodeAnnotations() {
        List<ICompileNotification> ret = new ArrayList<>();
        ret.addAll(errors);
        ret.addAll(warnings);
        ret.addAll(info);
        return ret;
    }

    @Override
    public IOntology getOntology() {
        if (ontology == null) {

            ontology = KLAB.KM.requireOntology(id);

        }
        return ontology;
    }

    @Override
    public Map<String, Object> getSymbolTable() {
        return symbolTable;
    }

    @Override
    public boolean isScenario() {
        return isScenario;
    }

    @Override
    public IMetadata getResolutionCriteria() {
        return resolutionCriteria;
    }

    @Override
    public Collection<String> getDisjointNamespaces() {
        return disjoint;
    }

    @Override
    public File getLocalFile() {
        return resource;
    }

    @Override
    public boolean isPrivate() {
        return isPrivate;
    }

    @Override
    public boolean isInactive() {
        return isInactive;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public String toString() {
        return "NS/" + getId();
    }

    public void addAxiom(IAxiom axiom) {
        if (!axiomCatalog.contains(axiom)) {
            axiomCatalog.add(axiom);
            axioms.add(axiom);
        }
    }

    /**
     * Add model object. Use for models, whose children (observers etc) shouldn't be
     * indexed.
     * 
     * @param ret
     */
    public void addModelObject(IModelObject ret) {
        modelObjects.add(ret);
        if (ret.getId() != null) {
            modelObjectsById.put(ret.getId(), ret);
        }
    }

    /**
     * Add model object and all children. Use for concepts and properties.
     * 
     * @param ret
     */
    public void addModelObjects(IModelObject ret) {
        modelObjects.add(ret);
        if (ret.getId() != null) {
            modelObjectsById.put(ret.getId(), ret);
        }
        for (IModelObject o : ret.getChildren()) {
            addModelObjects(o);
        }
    }

    public void synchronizeKnowledge() {

        if (axioms.size() > 0) {
            Collection<IAxiom> ax = new ArrayList<>();
            for (int i = lastAxiom; i < axioms.size(); i++)
                ax.add(axioms.get(i));
            for (String error : getOntology().define(ax)) {

                // /*
                // * TODO should pass a different line number
                // */
                // if (resolver != null)
                // resolver.onException(new ThinklabValidationException(error), 1);
                // addError(0, error, 1);
            }
            lastAxiom = axioms.size();
        }
    }

    public void setInternal(boolean b) {
        isInternal = b;
    }

    public void setResourceUrl(String resource2) {
        resource = new File(resource2);
    }

    public void setId(String namespace) {
        id = namespace;
    }

    public String getNewObjectName(KIMModel kimModel) {
        String prefix = kimModel.getObservable() == null ? "unnamed"
                : CamelCase.toLowerCase(NS.getDisplayName(kimModel.getObservable().getType()), '-');
        String ret = prefix + (kimModel.isInstantiator ? "-instantiator" : "-contextualizer");
        if (kimModel.isReinterpreter()) {
            // ACHTUNG: this only works with the current parser, as the observable is
            // still the original one when this is called.
            String oconc = CamelCase.toLowerCase(NS.getDisplayName(kimModel.getObservable().getType()), '-');
            String orole = CamelCase.toLowerCase(NS.getDisplayName(kimModel.getAddedRole()), '-');
            ret = oconc + "-as-" + orole + "-interpreter";
        }
        int n = getCountAndIncrement(ret);
        if (n > 0) {
            ret += "-" + n;
        }
        return ret;
    }

    private int getCountAndIncrement(String name) {
        int ret = 0;
        if (modelNameCounts.containsKey(name)) {
            ret = modelNameCounts.get(name);
        }
        modelNameCounts.put(name, ret + 1);
        return ret;
    }

    public void addWarning(CompileWarning compileWarning) {
        warnings.add(compileWarning);
    }

    public void addError(CompileError compileWarning) {
        errors.add(compileWarning);
    }

    public void addInfo(CompileInfo compileWarning) {
        info.add(compileWarning);
    }

    public void setResolutionCriteria(IMetadata criteria) {
        resolutionCriteria = criteria;
    }

    public void processAnnotations(KIMScope context, Namespace statement) {

        Set<String> elist = new HashSet<>();
        Set<String> decla = new HashSet<>();
        if (statement.getExportList() != null) {
            elist.addAll(statement.getExportList());
        }

        /*
         * check export, deprecation etc.
         */
        for (IModelObject o : getAllModelObjects()) {
            for (IAnnotation a : o.getAnnotations()) {
                if (a.getId().equals("export")) {
                    if (!elist.contains(a.getParameters().get(IAnnotation.DEFAULT_PARAMETER_NAME))) {
                        context.error("exported object was not declared in an 'export' statement: ", a
                                .getFirstLineNumber());
                    } else {
                        decla.add(a.getParameters().get(IAnnotation.DEFAULT_PARAMETER_NAME).toString());
                        if (o instanceof IKnowledgeObject) {
                            exported.put(a.getParameters()
                                    .get(IAnnotation.DEFAULT_PARAMETER_NAME)
                                    .toString(), ((IKnowledgeObject) o)
                                            .getType());
                        } else {
                            context.warning("exporting anything other than knowledge is not currently useful", o
                                    .getFirstLineNumber());
                        }
                    }
                } else if (a.getId().equals("deprecated")) {
                    ((KIMModelObject) o).setDeprecated(true);
                }
            }

            /*
             * have the bookmark manager scan for bookmarks and issues
             */
            IBookmarkManager bookmarkManager = KLAB.KM.getBookmarkManager();
            if (bookmarkManager != null) {
                ((BookmarkManager) bookmarkManager)
                        .processAnnotations(o, KLAB.ENGINE instanceof IModelingEngine
                                ? ((IModelingEngine) KLAB.ENGINE).getUser() : null);
            }
        }

        String dangling = "";
        for (String d : elist) {
            if (!decla.contains(d)) {
                dangling += (dangling.isEmpty() ? "" : ", ") + d;
            }
        }

        if (!dangling.isEmpty()) {
            context.error("declared exports are not defined in the code: please add the @export annotations for "
                    + dangling, lineNumber(statement));
        }

    }

    public Map<String, IKnowledge> getExportedKnowledgeMap() {
        return exported;
    }

    public void checkImported(KIMScope context, IKnowledge existing, int lineNumber) {

        String ns = existing.getConceptSpace();
        if (authorityNamespaces.contains(ns)) {
            return;
        } else if (existing instanceof IConcept && KLAB.KM.getAuthorityFor((IConcept) existing) != null) {
            registerAuthority((IConcept) existing);
            return;
        }

        if (KLAB.KM.getCoreNamespace(ns) != null) {
            /*
             * If core knowledge, it's OK as long as we are a worldview - only worldview
             * projects can refer to core knowledge.
             */
            if (!(project != null && project.getWorldview() != null)) {
                // FIXME this should become an error, but for now the mechanism to link
                // core types to the worldview is
                // incomplete
                context.warning("core types can only be referred to within worldview projects. THIS WARNING WILL BECOME AN ERROR!", lineNumber);
            }
            return;
        }
        if (KLAB.MMANAGER.getNamespace(ns) == null && !authorityNamespaces.contains(ns)) {
            context.error("the namespace " + ns + " containing " + existing
                    + " has not been loaded: please declare it in the import list or add a project that provides it", lineNumber);

        } else if (!importedIds.contains(ns) && !authorityNamespaces.contains(ns)) {
            if (ns.equals(this.getId())) {
                context.warning("concepts in the same namespace being declared should not indicate a namespace", lineNumber);
            } else {
                context.warning("the namespace " + ns + " containing " + existing
                        + " exists but is not in the import list: please declare it in the import list", lineNumber);
            }
        }
    }

    public void registerAuthority(IConcept concept) {
        authorityNamespaces.add(concept.getConceptSpace());
        ((Ontology) ontology).addImport(concept.getOntology());
    }

    public void setVoid(boolean b) {
        this.isInactive = b;
    }

    public void setProject(IProject project) {
        this.project = project;
    }

    @Override
    public IDocumentation getDocumentation() {
        return documentation;
    }

    public boolean isProjectKnowledge() {
        return isProjectKnowledge;
    }

    public void setProjectKnowledge(boolean b) {
        this.isProjectKnowledge = b;
    }
    
    @Override
    public boolean isTainted() {
        return isTainted;
    }

    public void setTainted(boolean b) {
        this.isTainted = b;
    }

}
