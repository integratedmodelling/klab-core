/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.nodemodel.util.NodeModelUtils;
import org.integratedmodelling.api.modelling.ILanguageObject;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.common.kim.expr.GroovyExpression;
import org.integratedmodelling.kim.kim.Condition;
import org.integratedmodelling.kim.kim.Value;
import org.integratedmodelling.kim.kim.WhenExpression;

public class KIMExpression extends GroovyExpression implements ILanguageObject {

    int firstLine, lastLine;

    public KIMExpression(KIMScope context, KIMModel model, IObserver cobs, WhenExpression statement) {
        firstLine = NodeModelUtils.getNode(statement).getStartLine();
        lastLine = NodeModelUtils.getNode(statement).getEndLine();
        processLiteralToExpression(context, statement.isOtherwise() ? null
                : statement.getCondition().getExpression(), model, statement
                        .getCondition() == null ? false : statement.getCondition().isNegated(), null, null);
    }

    public KIMExpression(KIMScope context, Value value) {
        this(context, value, null);
    }

    public KIMExpression(KIMScope context, Condition condition) {
        this(context, condition, null);
    }

    public KIMExpression(KIMScope context, Value value, String prefix, String postfix) {
        firstLine = value == null ? 0 : NodeModelUtils.getNode(value).getStartLine();
        lastLine = value == null ? 0 : NodeModelUtils.getNode(value).getEndLine();
        processLiteralToExpression(context, value, null, false, prefix, postfix);
    }

    public KIMExpression(KIMScope context, Value value, String prefix) {
        firstLine = NodeModelUtils.getNode(value).getStartLine();
        lastLine = NodeModelUtils.getNode(value).getEndLine();
        processLiteralToExpression(context, value, null, false, prefix, null);
    }

    public KIMExpression(KIMScope context, Condition condition, String prefix) {
        firstLine = NodeModelUtils.getNode(condition).getStartLine();
        lastLine = NodeModelUtils.getNode(condition).getEndLine();
        processLiteralToExpression(context, condition.getExpression(), null, condition
                .isNegated(), prefix, null);
    }

    public KIMExpression(KIMScope context, EObject kim, String code) {
        firstLine = kim == null ? 0 : NodeModelUtils.getNode(kim).getStartLine();
        lastLine = kim == null ? 0 : NodeModelUtils.getNode(kim).getEndLine();
        this.code = processExpressionCode(code, "", "");
    }

    @Override
    public int getFirstLineNumber() {
        return firstLine;
    }

    @Override
    public int getLastLineNumber() {
        return lastLine;
    }

    /*
     * process the passed value so if it's an expression, return it compiled according to
     * the language being supported, otherwise turn any literal into an expression that
     * returns it.
     */
    private void processLiteralToExpression(KIMScope context, Value value, IModel model, boolean negate, String prefix, String postfix) {

        if (value == null) {
            if (prefix != null || postfix != null) {
                this.code = processExpressionCode("", prefix, postfix);
            } else {
                this.isTrue = true;
            }
        } else if (value.getFunction() != null) {
            this.functionCall = new KIMFunctionCall(context, value.getFunction());
        } else if (value.getLiteral() != null) {
            this.object = KIM.processLiteral(value.getLiteral());
        } else if (value.getId() != null) {
            this.object = context.getNamespace().getSymbolTable().get(value.getId());
            if (this.object == null) {
                context.error("unknown symbol in expression: "
                        + value.getId(), KIMLanguageObject.lineNumber(value));
            }
        } else if (value.getExpr() != null) {
            this.code = processExpressionCode(value.getExpr(), prefix, postfix);
        } else if (value.isNull()) {
            this.isNull = true;
        }

        this.model = model;

        setNegated(negate);
    }

    private String processExpressionCode(String expr, String prefix, String postfix) {

        if (expr.startsWith("[")) {
            expr = expr.substring(1);
        }
        if (expr.endsWith("]")) {
            expr = expr.substring(0, expr.length() - 1);
        }
        if (prefix != null) {
            expr = prefix + expr;
        }
        if (postfix != null) {
            expr = expr + "\n" + postfix + "\n";
        }
        return expr;
    }
}
