/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.modelling.IAnnotation;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.kim.kim.Annotation;
import org.integratedmodelling.kim.kim.KeyValuePair;

public class KIMAnnotation extends KIMModelObject implements IAnnotation {

    Map<String, Object> parameters = new HashMap<String, Object>();

    /**
     * Inline constructor for use outside k.IM
     * 
     * @param id
     * @param parameters
     */
    public KIMAnnotation(String id, Object ...parameters) {
    	super(null);
    	this.id = id;
    	for (int i = 0; i < parameters.length; i++) {
    		this.parameters.put(parameters[i].toString(), parameters[++i]);
    	}
    }
    
    public KIMAnnotation(KIMScope context, Annotation statement, KIMModelObject parent) {
        super(context, statement, parent);

        this.id = statement.getId().substring(1);

        if (statement.getParameters() != null) {
            if (statement.getParameters().getSingleValue() != null) {
                parameters
                        .put(IAnnotation.DEFAULT_PARAMETER_NAME, KIM.processLiteral(statement.getParameters()
                                .getSingleValue().getLiteral()));
            }
            for (KeyValuePair kp : statement.getParameters().getPairs()) {
                parameters.put(kp.getKey(), KIM.processLiteral(context, kp.getValue()));
            }
        }

    }

    @Override
    public Map<String, Object> getParameters() {
        return parameters;
    }

    @Override
    public IPrototype getPrototype() {
        return KLAB.ENGINE.getFunctionPrototype(getId());
    }

    @Override
    public String toString() {

        String ret = id;
        if (parameters != null && parameters.size() > 0) {
            ret += "(";
            for (String k : parameters.keySet()) {
                ret += k + " = " + parameters.get(k) + " ";
            }
            ret += ")";
        }

        return ret;
    }
}
