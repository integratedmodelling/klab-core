/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.kim;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.api.factories.IModelFactory;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IExpression;
import org.integratedmodelling.api.knowledge.IKnowledge;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.lang.INamespaceQualified;
import org.integratedmodelling.api.lang.IParsingScope;
import org.integratedmodelling.api.modelling.IActiveDirectObservation;
import org.integratedmodelling.api.modelling.ICurrency;
import org.integratedmodelling.api.modelling.IDataSource;
import org.integratedmodelling.api.modelling.IDirectObserver;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IFunctionCall;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.INamespace;
import org.integratedmodelling.api.modelling.IObjectSource;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IObservingObject;
import org.integratedmodelling.api.modelling.IRemoteServiceCall;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.contextualization.IContextualizer;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.monitoring.IKnowledgeLifecycleListener;
import org.integratedmodelling.api.monitoring.IMonitor;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.api.space.IGrid;
import org.integratedmodelling.api.space.IGridMask;
import org.integratedmodelling.api.space.IShape;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.space.ISpatialIndex;
import org.integratedmodelling.common.beans.Observer;
import org.integratedmodelling.common.command.ServiceManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.Monitorable;
import org.integratedmodelling.common.interfaces.NetworkDeserializable;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.common.model.runtime.Scale;
import org.integratedmodelling.common.model.runtime.Space;
import org.integratedmodelling.common.model.runtime.Time;
import org.integratedmodelling.common.space.ShapeValue;
import org.integratedmodelling.common.utils.CamelCase;
import org.integratedmodelling.common.vocabulary.Currency;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.Observables;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.integratedmodelling.exceptions.KlabValidationException;

import com.vividsolutions.jts.geom.Geometry;

/**
 * Basic model factory, not functional for an engine but OK for a client or testing.
 * 
 * @author Ferd
 *
 */
public class ModelFactory implements IModelFactory {

    List<IKnowledgeLifecycleListener> knowledgeListeners = new ArrayList<>();

    public IObserver getObserverFor(IConcept concept) {
        
        if (NS.isTrait(concept) || NS.isClass(concept)) {
            return new KIMClassificationObserver(concept);
        }
        if (concept.is(KLAB.c(NS.CORE_DISTANCE))) {
            return new KIMDistanceObserver(concept, "m");
        }
        if (concept.is(KLAB.c(NS.CORE_PHYSICAL_PROPERTY))) {
            String unit = NS.getMetadata(concept, NS.SI_UNIT_PROPERTY);
            return new KIMMeasurementObserver(concept, unit);
        }
        if (concept.is(KLAB.c(NS.CORE_PREFERENCE_VALUE))) {
            IConcept currency = Observables.getInherentType(concept);
            return new KIMValuationObserver(concept, new Currency(currency));
        }
        if (concept.is(KLAB.c(NS.CORE_RATIO))) {
            return new KIMRatioObserver(concept, null);
        }
        if (concept.is(KLAB.c(NS.CORE_PROPORTION))) {
            return new KIMProportionObserver(concept);
        }
        if (concept.is(KLAB.c(NS.CORE_PRESENCE))) {
            return new KIMPresenceObserver(concept);
        }
        if (concept.is(KLAB.c(NS.CORE_MONETARY_VALUE))) {
            return new KIMValuationObserver(concept, new Currency("EUR@" + Calendar.getInstance().get(Calendar.YEAR)));
        }
        if (concept.is(KLAB.c(NS.CORE_COUNT))) {
            return new KIMCountObserver(concept, null);
        }
        if (concept.is(KLAB.c(NS.CORE_PROBABILITY))) {
            return new KIMProbabilityObserver(concept);
        }
        if (concept.is(KLAB.c(NS.CORE_UNCERTAINTY))) {
            return new KIMUncertaintyObserver(concept);
        }
        if (concept.is(KLAB.c(NS.CORE_NUMERIC_QUANTITY))) {
            return new KIMRankingObserver(concept);
        }
        return null;
    }
    
    @Override
    public IParsingScope getRootParsingContext() {
        return new KIMScope();
    }

    @Override
    public IStorage<?> createStorage(IObserver observer, IScale scale, IDataset dataset, boolean isDynamic, boolean isProbabilistic) {
        return null;
    }

    @Override
    public IActiveDirectObservation createSubject(IDirectObserver observer, IContext context, IActiveDirectObservation contextObservation, IMonitor monitor)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale createScale(Collection<IFunctionCall> definition, IMonitor monitor) {
        try {
            List<IExtent> exts = new ArrayList<>();
            for (IFunctionCall f : definition) {
                Object o = callFunction(f, monitor, null);
                if (!(o instanceof IExtent)) {
                    throw new KlabRuntimeException("function call " + f + " did not produce an extent");
                }
                exts.add((IExtent) o);
            }
            return Scale.create(exts.toArray(new IExtent[definition.size()]));
        } catch (KlabException e) {
            throw new KlabRuntimeException(e);
        }
    }

    public static IObserver measureObserver(IConcept observable, String unit) throws KlabException {
        return new KIMMeasurementObserver(observable, unit);
    }

    public static IObserver countObserver(IConcept observable, String unit) throws KlabException {
        return new KIMCountObserver(observable, unit);
    }

    public static IModel count(IConcept observable, String unit) throws KlabException {
        return new KIMModel((KIMObserver) countObserver(observable, unit));
    }

    public static IObserver rankObserver(IConcept observable, List<Integer> scale) {
        return new KIMRankingObserver(observable, scale);
    }

    public static IModel rank(IConcept observable) {
        return new KIMModel((KIMObserver) rankObserver(observable, null));
    }

    public static IObserver presenceObserver(ISemantic observable) {
        IObservableSemantics obs = new ObservableSemantics(Observables
                .makePresence(observable.getType()), KLAB.c(NS.PRESENCE_OBSERVATION), CamelCase
                        .toLowerCase(observable.getType().getLocalName(), '-'));
        return new KIMPresenceObserver(obs);
    }

    public static IObserver ratioObserver(IConcept observable, ISemantic compareTo) {
        return new KIMRatioObserver(observable, compareTo);
    }

    public static IObserver valueObserver(IConcept observable, ICurrency currency) {
        return new KIMValuationObserver(observable, currency);
    }

    public static IObserver proportionObserver(IConcept observable, ISemantic comparison, boolean makePercentage) {
        return new KIMProportionObserver(observable, comparison, makePercentage);
    }

    // public static IObserver classifyObserver(ISemantic observable, Object...
    // classifiers) {
    // return new KIMClassificationObserver(observable, classifiers);
    // }

    public static IModel measure(IConcept observable, String unit) throws KlabException {
        return new KIMModel((KIMObserver) measureObserver(observable, unit));
    }

    public static IModel presence(IConcept observable) {
        return new KIMModel((KIMObserver) presenceObserver(observable));
    }

    // public static IModel classify(ISemantic observable, Object...
    // classifiers) {
    // return new KIMModel((KIMObserver) classifyObserver(observable,
    // classifiers));
    // }

    public static IModel probability(IConcept observable) {
        return new KIMModel((KIMObserver) probabilityObserver(observable));
    }

    public static IObserver probabilityObserver(IConcept observable) {
        return new KIMProbabilityObserver(observable);
    }

    public static IModel percentage(IConcept observable, ISemantic generic) {
        return new KIMModel((KIMObserver) proportionObserver(observable, generic, true));
    }

    public static IModel proportion(IConcept observable, ISemantic generic) {
        return new KIMModel((KIMObserver) proportionObserver(observable, generic, false));
    }

    public static IModel ratio(IConcept observable, ISemantic compareTo) {
        return new KIMModel((KIMObserver) ratioObserver(observable, compareTo));
    }

    public static IModel value(IConcept observable, ICurrency currency) {
        return new KIMModel((KIMObserver) valueObserver(observable, currency));
    }

    public static IModel uncertainty(IConcept observable) {
        return new KIMModel((KIMObserver) uncertaintyObserver(observable));
    }

    public static IObserver uncertaintyObserver(IConcept observable) {
        return new KIMUncertaintyObserver(observable);
    }

    @Override
    public void addKnowledgeLifecycleListener(IKnowledgeLifecycleListener listener) {
        knowledgeListeners.add(listener);
    }

    public static IDirectObserver createDirectObserver(IConcept concept, String name, INamespace namespace, IMonitor monitor, Object... extents)
            throws KlabException {

        boolean isLocal = false;
        IDirectObserver ret = null;
        if (namespace == null) {
            namespace = KLAB.MMANAGER.getLocalNamespace();
            isLocal = true;
        }
        if (extents != null && extents.length == 1 && extents[0] instanceof IScale) {
            ret = new KIMDirectObserver(namespace, concept, name, (IScale) extents[0]);
        } else {
            ret = new KIMDirectObserver(namespace, concept, name, KLAB.MFACTORY
                    .createScale(monitor, extents));
        }

        if (ret != null && isLocal) {
            ((KIMNamespace) KLAB.MMANAGER.getLocalNamespace()).addModelObject(ret);
        }

        return ret;
    }

    private IUser getUser() {
        return KLAB.ENGINE instanceof IModelingEngine ? ((IModelingEngine) KLAB.ENGINE).getUser() : null;
    }

    @Override
    public Object callFunction(IFunctionCall call, IMonitor monitor, IModel model, IConcept... context)
            throws KlabException {

        /*
         * 0. if the call is itself an expression, just call it with its own arguments.
         */
        if (call instanceof IExpression) {
            return ((IExpression) call).eval(call.getParameters(), monitor, context);
        }

        /*
         * 1. get prototype
         */
        IPrototype p = KLAB.ENGINE.getFunctionPrototype(call.getId());

        if (p == null) {
            throw new KlabValidationException("cannot find function " + call.getId());
        }

        /*
         * 1.1: the prototype comes from a component. Use the project manager to ensure
         * the component is deployed - which will throw an exception in any error
         * situation.
         */
        if (p.getComponentId() != null) {
            KLAB.PMANAGER.getDeployedComponent(p.getComponentId());
            // reload prototype as the previous operation may have reset its
            // executor.
            p = KLAB.ENGINE.getFunctionPrototype(call.getId());
        }

        /*
         * 2. validate args
         */
        ServiceManager.parseCall(call.getId(), call.getParameters(), null, null);

        /*
         * create accessor. If that's for a remotely available service, see if we should
         * use the remote service instead.
         */
        Object ret = null;
        if (IContextualizer.class.isAssignableFrom(p.getExecutorClass())) {
            try {
                ret = p.getExecutorClass().newInstance();
                if (ret instanceof INamespaceQualified) {
                    ((INamespaceQualified) ret).setNamespace(((KIMFunctionCall) call).getNamespace());
                }
            } catch (Exception e) {
                //
            }
        }

        if (ret instanceof IContextualizer.Remote) {

            boolean useLocally = false;

            if (ServiceManager.get().isServiceAvailableRemotely(call.getId())) {
                p = ServiceManager.get().getRemoteService(call.getId());
                if (IRemoteServiceCall.class.isAssignableFrom(p.getExecutorClass())) {
                    try {
                        Constructor<?> constructor = p.getExecutorClass().getConstructor(IPrototype.class);
                        if (constructor != null) {
                            Object prev = ret;
                            if (((IContextualizer.Remote) prev).useRemote(getUser())) {
                                monitor.info("using remote service for "
                                        + call.getId(), Messages.INFOCLASS_MODEL);
                                ret = constructor.newInstance(p);
                            }
                        } else {
                            useLocally = true;
                        }
                    } catch (Exception e) {
                        // keep using the local contextualizer if allowed
                        useLocally = true;
                    }
                }
            }

            if (ret /* still */ instanceof IContextualizer.Remote
                    && !((IContextualizer.Remote) ret).canRunLocally()) {
                monitor.error(call.getId() + " remote service not available; service cannot run locally");
                ret = null;
            }

            if (ret != null && useLocally) {
                monitor.info("using local version of remote-capable service "
                        + call.getId(), Messages.INFOCLASS_MODEL);
            }
        }

        /*
         * 4. return an instance of either the annotated class if an IExpression or a
         * wrapper if not.
         */
        if (ret == null) {
            if (IExpression.class.isAssignableFrom(p.getExecutorClass())) {
                try {
                    ret = p.getExecutorClass().newInstance();
                    if (ret instanceof INamespaceQualified) {
                        ((INamespaceQualified) ret).setNamespace(call.getNamespace());
                    }

                } catch (Exception e) {
                    KLAB.error(e);
                }
                if (ret instanceof IExpression) {
                    ret = ((IExpression) ret).eval(call.getParameters(), monitor, context);
                }
            } else if (IContextualizer.class.isAssignableFrom(p.getExecutorClass())) {
                try {
                    ret = p.getExecutorClass().newInstance();
                    if (ret instanceof INamespaceQualified) {
                        ((INamespaceQualified) ret).setNamespace(((KIMFunctionCall) call).getNamespace());
                    }
                } catch (Exception e) {
                    KLAB.error(e);
                }
            }
        }

        if (ret == null) {
            monitor.error("error calling " + call.getId());
            throw new KlabValidationException("error when calling function " + call.getId());
        }

        if (ret instanceof Monitorable) {
            ((Monitorable) ret).setMonitor(monitor);
        }

        if (ret instanceof IContextualizer) {
            ((IContextualizer) ret)
                    .setContext(call.getParameters(), model, model.getNamespace().getProject(), /* TODO - shit */ null);
        }

        return ret;
    }

    @Override
    public IContextualizer getContextualizer(IFunctionCall call, IObservingObject observer, IResolutionScope scope, IProvenance.Artifact provenance, IMonitor monitor)
            throws KlabException {

        IContextualizer ret = null;
        if (call != null) {

            if (scope.isInteractive()) {
                call = getUserChoices(call, observer);
            }

            IModel model = observer instanceof IModel ? (IModel) observer : ((IObserver) observer).getModel();

            Object ds = callFunction(call, monitor, model, chooseDirectAccessorType(observer.getObservable()
                    .getType(), model.isInstantiator()));

            if (!(ds instanceof IContextualizer)) {
                throw new KlabValidationException("function " + call.getId()
                        + " does not return a contextualizer");
            }

            ret = (IContextualizer) ds;
        }

        if (ret instanceof INamespaceQualified) {
            ((INamespaceQualified) ret).setNamespace(call.getNamespace());
        }

        return ret;
    }

    private IFunctionCall getUserChoices(IFunctionCall call, IObservingObject observer) {
        // TODO redefine in engine.
        // FIXME this strategy really doesn't get us any optional outputs, which
        // depend on
        // the resolution scope and the object being resolved.
        return call;
    }

    private IConcept chooseDirectAccessorType(IKnowledge ob, boolean isInstantiator)
            throws KlabValidationException {

        if (isInstantiator) {
            if (NS.isThing(ob)) {
                return KLAB.c(NS.SUBJECT_INSTANTIATOR);
            } else if (NS.isEvent(ob)) {
                return KLAB.c(NS.EVENT_INSTANTIATOR);
            } else if (NS.isRelationship(ob)) {
                return KLAB.c(NS.RELATIONSHIP_INSTANTIATOR);
            } 
        } else {
            if (NS.isQuality(ob)) {
                return KLAB.c(NS.STATE_CONTEXTUALIZER);
            } else if (NS.isThing(ob)) {
                return KLAB.c(NS.SUBJECT_CONTEXTUALIZER);
            } else if (NS.isProcess(ob)) {
                return KLAB.c(NS.PROCESS_CONTEXTUALIZER);
            } else if (NS.isEvent(ob)) {
                return KLAB.c(NS.EVENT_CONTEXTUALIZER);
            } else if (NS.isFunctionalRelationship(ob)) {
                return KLAB.c(NS.FUNCTIONAL_RELATIONSHIP_CONTEXTUALIZER);
            }  else if (NS.isStructuralRelationship(ob)) {
                return KLAB.c(NS.STRUCTURAL_RELATIONSHIP_CONTEXTUALIZER);
            }
        }

        throw new KlabValidationException("subject model has observable that doesn't match any accessor type");

    }

    @Override
    public IDataSource createDatasource(IFunctionCall call, IMonitor monitor) throws KlabException {
        Object ret = callFunction(call, monitor, null);
        if (!(ret instanceof IDataSource)) {
            throw new KlabValidationException("function " + call.getId() + " did not return a datasource");
        }
        if (ret instanceof INamespaceQualified) {
            ((INamespaceQualified) ret).setNamespace(((KIMFunctionCall) call).getNamespace());
        }
        return (IDataSource) ret;
    }

    @Override
    public IObjectSource createObjectsource(IFunctionCall call, IMonitor monitor) throws KlabException {
        /*
         * FIXME this is called twice or more - should cache function and parameters and
         * return existing sources.
         */
        Object ret = callFunction(call, monitor, null);
        if (!(ret instanceof IObjectSource)) {
            throw new KlabValidationException("function " + call.getId()
                    + " did not return an object source");
        }
        if (ret instanceof INamespaceQualified) {
            ((INamespaceQualified) ret).setNamespace(((KIMFunctionCall) call).getNamespace());
        }
        return (IObjectSource) ret;
    }

    @Override
    public IScale createScale(IMonitor monitor, Object... objects) {

        List<IExtent> extents = new ArrayList<>();

        for (Object o : objects) {
            if (o == null) {
                continue;
            }
            if (o instanceof ShapeValue) {
                extents.add(new Space(((ShapeValue) o)));
            } else if (o instanceof IShape) {
                ShapeValue shp = new ShapeValue(((IShape) o).asText());
                extents.add(new Space(shp));
            } else if (o instanceof Geometry) {
                ShapeValue shp = new ShapeValue((Geometry) o);
                extents.add(new Space(shp));
            } else if (o instanceof Long) {
                extents.add(new Time((Long) o));
            } else if (o instanceof String) {
                ShapeValue shp = new ShapeValue((String) o);
                extents.add(new Space(shp));
            } else if (o instanceof IExtent) {
                extents.add((IExtent) o);
            } else if (o instanceof IScale) {
                for (IExtent e : ((IScale) o)) {
                    extents.add(e);
                }
            } else {
                throw new KlabRuntimeException("internal: cannot create a scale from a "
                        + (o == null ? "null" : o.getClass().getCanonicalName()));
            }
        }

        return createScaleFromExtents(extents);
    }

    private IScale createScaleFromExtents(List<IExtent> extents) {
        return Scale.create(extents.toArray(new IExtent[extents.size()]));
    }

    @Override
    public IDataSource createConstantDataSource(Object inlineValue) {
        return null;
    }

    // @Override
    // public Object wrapForAction(Object object) {
    // return object;
    // }

    @Override
    public ISpatialIndex getSpatialIndex(ISpatialExtent space) {
        // TODO Auto-generated method stub
        return null;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T adapt(Object toAdapt, Class<? extends T> desiredClass) {

        /*
         * only thing that remains difficult is deserializing the observer, as providing
         * polymorphism for it is really too much, so we use a single bean for all
         * observers and intercept its type here. So just call adapt(observerBean,
         * IObserver.class) and we do the rest.
         */
        if (desiredClass.isAssignableFrom(IObserver.class) && toAdapt instanceof Observer) {
            try {
                Class<?> cls = Class.forName(((Observer) toAdapt).getObserverClass());
                if (cls != null && KIMObserver.class.isAssignableFrom(cls)) {
                    Object o = cls.newInstance();
                    ((NetworkDeserializable) o).deserialize((IModelBean) toAdapt);
                    return (T) o;
                }
            } catch (Exception e) {
                throw new KlabRuntimeException(e);
            }
        }

        return defaultAdapt(toAdapt, desiredClass);
    }

    /**
     * Default adaptation strategy uses interfaces. Call this in the overridden adapt()
     * after processing anything that must use specific objects for the implementation.
     * 
     * @param toAdapt
     * @param desiredClass
     * @return
     */
    @SuppressWarnings("unchecked")
    protected <T> T defaultAdapt(Object toAdapt, Class<? extends T> desiredClass) {

        if (IModelBean.class.isAssignableFrom(desiredClass) && toAdapt instanceof NetworkSerializable) {
            return ((NetworkSerializable) toAdapt).serialize((Class<? extends IModelBean>) desiredClass);
        }
        if (NetworkDeserializable.class.isAssignableFrom(desiredClass) && toAdapt instanceof IModelBean) {
            try {
                T ret = desiredClass.newInstance();
                ((NetworkDeserializable) ret).deserialize((IModelBean) toAdapt);
                return ret;
            } catch (InstantiationException | IllegalAccessException e) {
                KLAB.error(e);
                return null;
            }
        }
        return null;
    }

    @Override
    public void cleanNamespaceArtifacts(String namespaceId) throws KlabException {
        // client version does nothing.
    }

    @Override
    public IExtent sanitizeExtent(IExtent extent) throws KlabException {
        // just fine as is for client purposes.
        return extent;
    }

    @Override
    public Class<?> getContextualizerClass(IFunctionCall call) {

        IPrototype p = KLAB.ENGINE.getFunctionPrototype(call.getId());

        if (p.getComponentId() != null) {
            try {
                KLAB.PMANAGER.getDeployedComponent(p.getComponentId());
            } catch (KlabException e) {
                KLAB.warn("error obtaining component " + p.getComponentId() + " to resolve call to "
                        + call.getId());
                return null;
            }
            // reload prototype as the previous operation may have reset its
            // executor.
            p = KLAB.ENGINE.getFunctionPrototype(call.getId());
        }

        return p == null ? null : p.getExecutorClass();
    }

    @Override
    public int[] countShapes(Collection<IShape> shape, IGrid grid) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public IGridMask createMask(Collection<IShape> shape, IGrid grid) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IGridMask createMask(IShape shape, IGrid grid) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IGridMask addToMask(IShape shape, IGridMask mask) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale copyScale(IScale scale)  {
        // TODO Auto-generated method stub
        return scale;
    }

}
