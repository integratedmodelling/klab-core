/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.time;

import java.util.ArrayList;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.LocalDate;

/**
 * Odd temporal functions. 
 * TODO move the temporal namespace here from the engine package, have the engine derive
 * from this.
 * 
 * @author ferdinando.villa
 *
 */
public class Time {

    static final int FEB29 = 60;

    static final public long MS_IN_A_DAY = 86400000l;

    public static enum Frequency {
        HOURLY,
        DAILY,
        WEEKLY,
        MONTHLY,
        YEARLY
    }

    /**
     * Return true if the passed day/year is a time point at the specified frequency. Use
     * the end of the month and Sunday for week.
     * @param day
     * @param year
     * @param frequency
     * @return true if passed day/year clicks at given frequency
     */
    public static boolean isTimePoint(int day, int year, Frequency frequency) {
        // TODO finish
        switch (frequency) {
        case DAILY:
        case HOURLY:
            return true;
        case MONTHLY:
            // TODO day is last of month
            return false;
        case WEEKLY:
            // TODO day is sunday
            return false;
        case YEARLY:
            return day == daysInYear(year);
        }
        return false;
    }

    /**
     * Return all the years overlapping the period between the two times passed.
     * @param start
     * @param end
     * @return all years between the two dates
     */
    public static int[] yearsBetween(long start, long end) {

        DateTime ds = new DateTime(start);
        DateTime de = new DateTime(end);

        int ny = de.getYear() - ds.getYear() + 1;
        int[] ret = new int[ny];

        int i = 0;
        for (int y = ds.getYear(); y <= de.getYear(); y++) {
            ret[i++] = y;
        }
        return ret;
    }

    public static DateTime dateAt(int year, int dayInYear) {
        DateTime ret = new DateTime(year, 1, 1, 0, 0);
        return ret.plusDays(dayInYear);
    }

    /**
     * Return an iterable of the days (indexed within the year starting a 0) that overlap the passed
     * interval in the given year. 
     * 
     * @param start
     * @param end
     * @param year
     * @return all days between dates
     */
    public static Iterable<Integer> daysBetween(long start, long end, int year) {

        ArrayList<Integer> ret = new ArrayList<>(366);
        DateTime ds = new DateTime(start);
        ds = new DateTime(ds.getYear(), ds.getMonthOfYear(), ds.getDayOfMonth(), 0, 0);
        DateTime de = new DateTime(end);
        de = new DateTime(de.getYear(), de.getMonthOfYear(), de.getDayOfMonth(), 23, 59);

        for (DateTime d = ds; d.compareTo(de) <= 0; d = d.plusDays(1)) {
            if (d.getYear() > year) {
                break;
            }
            if (d.getYear() == year) {
                ret.add(d.getDayOfYear() - 1);
            }
        }

        return ret;
    }

    public static int daysInYear(int year) {
        LocalDate ld = new LocalDate(year, 1, 1);
        return Days.daysBetween(ld, ld.plusYears(1)).getDays();
    }

    /**
     * Adjust the passed array describing a daily value for olderYear to describe a value for oldYear, duplicating
     * or removing values as required.
     * 
     * @param data
     * @param year the year we adapt to
     * @param olderYear the year represented in the data
     * @return adjusted array
     */
    public static double[] adjustLeapDays(double[] data, int year, int olderYear) {

        double[] ret = data;
        if (data != null) {

            int cdays = daysInYear(year);
            int odays = daysInYear(olderYear);

            if (odays > cdays) {
                ret = new double[cdays];
                // just remove the day
                for (int i = 0; i < cdays; i++) {
                    ret[i] = i < FEB29 ? data[i] : data[i + 1];
                }
            } else if (odays < cdays) {
                // add a day
                ret = new double[cdays];
                for (int i = 0; i < cdays; i++) {
                    ret[i] = i < FEB29 ? data[i] : data[i - 1];
                }
            }

        }
        return ret;
    }

    public static int getYear(long start) {
        DateTime ds = new DateTime(start);
        return ds.getYear();
    }

}
