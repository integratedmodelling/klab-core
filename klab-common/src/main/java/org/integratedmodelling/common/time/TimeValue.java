/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.time;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IIndividual;
import org.integratedmodelling.api.knowledge.IObservable;
import org.integratedmodelling.api.knowledge.IProperty;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IObserver;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.IScale.Index;
import org.integratedmodelling.api.modelling.IScale.Locator;
import org.integratedmodelling.api.modelling.IState;
import org.integratedmodelling.api.modelling.ISubject;
import org.integratedmodelling.api.modelling.ITopologicallyComparable;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IStorage;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.api.time.ITimeDuration;
import org.integratedmodelling.api.time.ITimeInstant;
import org.integratedmodelling.api.time.ITimePeriod;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.model.runtime.AbstractLocator;
import org.integratedmodelling.exceptions.KlabException;
import org.joda.time.DateTime;

/**
 * A parsed literal representing one point in time, parsed from a string that satisfies
 * the JODA Time parser. The typical input is an ISO8601 date/time string, such as
 * "2006-12-13T21:39:45.618-08:00". Linked to the DateTimeValue type as an extended
 * literal. The same syntax can be used to define an entire time observation of type
 * TemporalRecord using a literal.
 *
 * TODO parser should support yyyy, mm-yyyy and dd-mm-yyyy by simple pattern recognition,
 * and set correspondent PRECISION values (YEAR, MONTH, DAY). Whatever doesn't match
 * should be given to Yoda for ISO parsing and assumed MILLISECOND precision.
 *
 * @author Ferdinando Villa
 * 
 *         FIXME resolve ambiguity with "semantic literal" version in engine package.
 *
 */
public class TimeValue extends AbstractLocator implements ITimeInstant, IScale.Locator, ITemporalExtent {

    int                  precision         = TemporalPrecision.MILLISECOND;
    DateTime             value;
    private int          scaleRank;
    public static String matchYear         = "[0-2][0-9][0-9][0-9]";
    public static String matchMonthYear    = "[0-1][0-9]-[0-2][0-9][0-9][0-9]";
    public static String matchDayMonthYear = "[0-3][0-9]-[0-1][0-9]-[0-2][0-9][0-9][0-9]";

    public TimeValue(long d) {
        this(new DateTime(d));
    }

    public TimeValue(DateTime dateTime) {
        value = dateTime;
    }

    @Override
    public int getScaleRank() {
        return scaleRank;
    }
    
    @Override
    public IState as(IObserver observer) {
        return this;
    }

    public int getValuesCount() {
        return 1;
    }

    public DateTime getTimeData() {
        return value;
    }

    @Override
    public IConcept getType() {
        return getObservable().getType();
    }

    public int getPrecision() {
        return precision;
    }

    /**
     * Compare at the resolution intrinsic in the date. Code should read easily.
     *
     * TODO no normalization of time zones is done - we should ensure both TZ are the same
     * before comparing.
     *
     * @param v1
     * @return true if comparable
     */
    public boolean comparable(TimeValue v1) {

        if (precision == TemporalPrecision.YEAR) {
            return value.year().getMaximumValue() == v1.value.year().getMaximumValue();

        } else if (precision == TemporalPrecision.MONTH) {

            return value.year().equals(v1.value.year()) && value.monthOfYear().equals(v1.value.monthOfYear());

        } else if (precision == TemporalPrecision.DAY) {
            return value.year().equals(v1.value.year()) && value.monthOfYear().equals(v1.value.monthOfYear())
                    && value.dayOfMonth().equals(v1.value.dayOfMonth());
        }

        return v1.value.equals(value);
    }

    @Override
    public String toString() {

        if (precision == TemporalPrecision.YEAR) {
            return value.toString("yyyy");
        } else if (precision == TemporalPrecision.MONTH) {
            return value.toString("MM-yyyy");
        } else if (precision == TemporalPrecision.DAY) {
            return value.toString("dd-MM-yyyy");
        }
        return value.toString();
    }

    public boolean isIdentical(TimeValue obj) {

        return (precision == obj.precision) && value.equals(obj.value);

    }

    public TimeValue leastPrecise(TimeValue v) {

        if (comparable(v)) {
            return new Integer(precision).compareTo(v.precision) > 0 ? this : v;
        }
        return null;
    }

    public TimeValue mostPrecise(TimeValue v) {

        if (comparable(v)) {
            return new Integer(precision).compareTo(v.precision) > 0 ? v : this;
        }
        return null;
    }

    public int getDayOfMonth() {
        return value.getDayOfMonth();
    }

    public int getJulianDay() {
        return value.getDayOfYear();
    }

    public int getMonth() {
        return value.getMonthOfYear();
    }

    public int getYear() {
        return value.getYear();
    }

    public int getNDaysInMonth() {
        return value.dayOfMonth().withMaximumValue().getDayOfMonth();
    }

    @Override
    public boolean equals(Object other) {

        boolean ret = (other instanceof TimeValue);
        if (ret) {
            ret = precision == ((TimeValue) other).precision;
        }
        if (ret) {
            ret = value.equals(((TimeValue) other).value);
        }
        return ret;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * Return the end of the implied extent according to the precision in the generating
     * string.
     *
     * @return total extent
     */
    public TimeValue getEndOfImpliedExtent() {

        TimeValue end = null;
        switch (precision) {

        case TemporalPrecision.MILLISECOND:
            end = new TimeValue(value.plus(1));
            break;
        case TemporalPrecision.SECOND:
            end = new TimeValue(value.plusSeconds(1));
            break;
        case TemporalPrecision.MINUTE:
            end = new TimeValue(value.plusMinutes(1));
            break;
        case TemporalPrecision.HOUR:
            end = new TimeValue(value.plusHours(1));
            break;
        case TemporalPrecision.DAY:
            end = new TimeValue(value.plusDays(1));
            break;
        case TemporalPrecision.MONTH:
            end = new TimeValue(value.plusMonths(1));
            break;
        case TemporalPrecision.YEAR:
            end = new TimeValue(value.plusYears(1));
            break;
        }

        return end;

    }

    public int compareTo(TimeValue other) {
        return this.value.compareTo(other.value);
    }

    /**
     * Overridden DateTime.compareTo() function. Extracting milliseconds from two
     * DateTime/ITimeInstant objects is probably more efficient than creating a new
     * DateTime from other.getMillis() just for one comparison.
     */
    @Override
    public int compareTo(ITimeInstant other) {
        long millis = value.getMillis();
        long otherMillis = other.getMillis();
        if (millis == otherMillis) {
            return 0;
        }
        if (millis < otherMillis) {
            return -1;
        }
        return 1;
    }

    @Override
    public long getMillis() {
        return value.getMillis();
    }

    @Override
    public int getDimensionCount() {
        return 1;
    }

    @Override
    public boolean isAll() {
        return false;
    }

    @Override
    public String asText() {
        return toString();
    }

    @Override
    public IConcept getDomainConcept() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent getExtent() {
        // TODO Auto-generated method stub
        return this;
    }

    @Override
    public boolean isCovered(int stateIndex) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IExtent merge(IExtent extent, boolean force) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Pair<ITopologicallyComparable<?>, Double> checkCoverage(ITopologicallyComparable<?> obj)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConsistent() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public int[] getDimensionSizes() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int[] getDimensionOffsets(int linearOffset, boolean rowFirst) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int locate(Locator locator) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public long getValueCount() {
        return 1;
    }

    @Override
    public IObserver getObserver() {
        return null;
    }

    @Override
    public boolean isSpatiallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporallyDistributed() {
        return false;
    }

    @Override
    public boolean isTemporal() {
        return true;
    }

    @Override
    public boolean isSpatial() {
        return false;
    }

    @Override
    public ISpatialExtent getSpace() {
        return null;
    }

    @Override
    public ITemporalExtent getTime() {
        return this;
    }

    @Override
    public Object getValue(int index) {
        return this;
    }

    @Override
    public Iterator<?> iterator(Index index) {
        return null;
    }

    @Override
    public IStorage<?> getStorage() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IMetadata getMetadata() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IScale getScale() {
        // TODO Auto-generated method stub
        return null;
    }

    // @Override
    // public IConcept getDirectType() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public INamespace getNamespace() {
    // // TODO Auto-generated method stub
    // return null;
    // }
    //
    // @Override
    // public boolean is(Object other) {
    // // TODO Auto-generated method stub
    // return false;
    // }

    @Override
    public long getMultiplicity() {
        // TODO Auto-generated method stub
        return 1;
    }

    @Override
    public IExtent union(IExtent other) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean contains(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean overlaps(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean intersects(IExtent o) throws KlabException {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public ITopologicallyComparable<IExtent> union(ITopologicallyComparable<?> other)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITopologicallyComparable<IExtent> intersection(ITopologicallyComparable<?> other)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public double getCoveredExtent() {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public ITemporalExtent getExtent(int stateIndex) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimePeriod collapse() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITemporalExtent intersection(IExtent other) throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITimeInstant getStart() {
        // TODO Auto-generated method stub
        return this;
    }

    @Override
    public ITimeInstant getEnd() {
        // TODO Auto-generated method stub
        return this;
    }

    @Override
    public ITimeDuration getStep() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Mediator getMediator(IExtent extent, IObservableSemantics observable, IConcept trait) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isConstant() {
        return getMultiplicity() == 1;
    }

    @Override
    public IDirectObservation getContextObservation() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean isDynamic() {
        return false;
    }

    @Override
    public IObservable getObservable() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IIndividual> getIndividuals(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<Object> getData(IProperty property) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IProperty> getObjectRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Collection<IProperty> getDataRelationships() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean is(ISemantic type) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public IContext getContext() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ISubject getObservingSubject() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void addChangeListener(ChangeListener listener) {
        // TODO Auto-generated method stub

    }

    @Override
    public Iterator<IExtent> iterator() {
        return Collections.singletonList((IExtent)this).iterator();
    }

    @Override
    public ITransition getTransition(int i) {
        // TODO Auto-generated method stub
        return null;
    }

}
