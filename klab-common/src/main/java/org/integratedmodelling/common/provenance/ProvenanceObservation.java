package org.integratedmodelling.common.provenance;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IObservableSemantics;

/**
 * The only Artifact in the k.LAB provenance model.
 * 
 * @author ferdinando.villa
 *
 */
public class ProvenanceObservation extends ProvenanceArtifact {
    
    public ProvenanceObservation(IDirectObservation observation) {
        super(observation.getName());
        this.observation = observation;
    }

    @Override
    public ISemantic getArtifact() {
        return observation;
    }

    @Override
    public String toString() {
        // TODO
        return "< S " + observation  + " >";
    }
    
    @Override
    public IObservation getObservation() {
        return observation == null ? super.getObservation() : observation;
    }

    @Override
    public IConcept getObservable() {
        return observation.getObservable().getSemantics().getType();
    }

    @Override
    public IObservableSemantics getObservableSemantics() {
        return observation.getObservable().getSemantics();
    }
    
}
