package org.integratedmodelling.common.provenance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModel;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.provenance.IProvenance.Artifact;
import org.integratedmodelling.api.space.ISpatialExtent;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.knowledge.Observation;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.Roles;
import org.integratedmodelling.common.vocabulary.Traits;

public abstract class ProvenanceArtifact extends ProvenanceNode implements IProvenance.Artifact {

    protected IProvenance.Agent owner;
    protected IProvenance.Agent consumer;
    protected IObservation      observation;

    protected ProvenanceArtifact(String name) {
        super(name);
    }

    @Override
    public IModel getModel() {
        return actuator == null ? null : actuator.getModel();
    }

    @Override
    public Collection<Artifact> collect(IConcept concept) {

        Set<Artifact> ret = new HashSet<>();
        collect(concept, null, ret);
        return ret;
    }

    public Collection<Artifact> collect(IConcept concept, IDirectObservation context) {

        Set<Artifact> ret = new HashSet<>();
        collect(concept, context, ret);
        return ret;
    }

    public void collect(IConcept concept, IDirectObservation context, Set<Artifact> result) {
        if (matchConcept(concept, context)) {
            result.add(this);
        }
        for (ProvenanceAction up : getGraph().incomingEdgesOf(this)) {
            ((ProvenanceArtifact) getGraph().getEdgeSource(up)).collect(concept, context, result);
        }
    }

    @Override
    public Artifact trace(IConcept concept) {
        return trace(concept, null);
    }

    public Artifact trace(IConcept concept, IDirectObservation context) {
        if (matchConcept(concept, context)) {
            return this;
        }
        for (ProvenanceAction up : getGraph().incomingEdgesOf(this)) {
            Artifact ret = ((ProvenanceArtifact) getGraph().getEdgeSource(up)).trace(concept, context);
            if (ret != null) {
                return ret;
            }
        }
        return null;
    }

    private boolean matchConcept(IConcept concept, IDirectObservation context) {
        if (NS.isRole(concept)) {
            if (context != null && observation != null) {
                if (NS.contains(concept, context.getRolesFor(observation))) {
                    return true;
                }
            }
            if (observation != null && ((Observation) observation).getExplicitRoles().contains(concept)) {
                return true;
            }
            if (getObservable() != null && Roles.hasRole(getObservable(), concept)) {
                return true;
            }
        } else if (NS.isTrait(concept)) {
            if (getObservable() != null && Traits.hasTrait(getObservable(), concept)) {
                return true;
            }
        } else {
            if (getObservable() != null && getObservable().is(concept)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public IProvenance.Agent getOwner() {
        return owner;
    }

    public void setOwner(IProvenance.Agent owner) {
        this.owner = owner;
    }

    @Override
    public IProvenance.Agent getConsumer() {
        return consumer;
    }

    public void setConsumer(IProvenance.Agent consumer) {
        this.consumer = consumer;
    }

    public void setObservation(IObservation observation) {
        this.observation = observation;
    }

    @Override
    public Collection<Artifact> getAntecedents() {
        List<Artifact> ret = new ArrayList<>();
        for (ProvenanceAction edge : getGraph().incomingEdgesOf(this)) {
            ret.add((ProvenanceArtifact) getGraph().getEdgeSource(edge));
        }
        return ret;
    }

    @Override
    public Collection<Artifact> getConsequents() {
        List<Artifact> ret = new ArrayList<>();
        for (ProvenanceAction edge : getGraph().outgoingEdgesOf(this)) {
            ret.add((ProvenanceArtifact) getGraph().getEdgeTarget(edge));
        }
        return ret;
    }


    @Override
    public ITemporalExtent getTime() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ISpatialExtent getSpace() {
        // TODO Auto-generated method stub
        return null;
    }
}
