package org.integratedmodelling.common.provenance;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.provenance.IProvenance.Agent;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.utils.NameGenerator;
import org.jgrapht.graph.DefaultEdge;

/**
 * Base class for an action (made by an actor and producing an artifact). OPM uses the
 * inverse relationships - so we create OPM graphs by reversing these, but the API for
 * provenance uses the direct action as it's more in tune with what actually happens.
 * 
 * @author Ferd
 *
 */
public abstract class ProvenanceAction extends DefaultEdge implements IProvenance.Action {

    private static final long serialVersionUID = 432701225015693782L;
    
    String                    id               = NameGenerator.shortUUID();
    long                      timestamp        = System.currentTimeMillis();
    IMetadata                 metadata         = new Metadata();

    ProvenanceAction          cause            = null;
    Agent                     agent;

    /*
     * this is 0 in actions that are the consequence of others, and sequentially numbered
     * from 1 in chronological order in primary actions. Usually those will be user
     * actions.
     */
    int                       sequenceIndex    = 0;

    public ProvenanceAction(Agent agent) {
        this.agent = agent;
    }

    @Override
    public IProvenance.Action getCause() {
        return cause;
    }

    public void setCause(IProvenance.Action cause) {
        this.cause = (ProvenanceAction) cause;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProvenanceAction other = (ProvenanceAction) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    public void setSequence(int i) {
        this.sequenceIndex = i;
    }

    @Override
    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
    
    @Override
    public String toString() {
        // TODO
        return ">A<";
    }
}
