package org.integratedmodelling.common.provenance;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.knowledge.ISemantic;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.modelling.IObservableSemantics;

/**
 * The only Artifact in the k.LAB provenance model along with ProvenanceObservation. Used
 * when we are observing anything except an observation from the kBox.
 * 
 * @author ferdinando.villa
 *
 */
public class ProvenanceObservable extends ProvenanceArtifact {

    IObservableSemantics observable;
    
    public ProvenanceObservable(IObservableSemantics observable) {
        super(observable.getFormalName());
        this.observable = observable;
    }

    public ProvenanceObservable(IDependency dependency) {
        super(dependency.getFormalName());
        this.observable = dependency.getObservable();
    }

    @Override
    public ISemantic getArtifact() {
        return observable;
    }
    
    @Override
    public String toString() {
        // TODO
        return "< O " + observable  + " >";
    }

    @Override
    public IConcept getObservable() {
        return observable.getType();
    }

    @Override
    public IObservableSemantics getObservableSemantics() {
        return observable;
    }

}
