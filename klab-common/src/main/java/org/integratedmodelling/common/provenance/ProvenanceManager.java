package org.integratedmodelling.common.provenance;

import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.common.configuration.KLAB;

public class ProvenanceManager {

    static Provenance.Agent userAgent;
    static Provenance.Agent engineAgent;
    
    public static Provenance.Agent getUserAgent() {
        if (userAgent == null) {
            userAgent = new ProvenanceAgent(((IModelingEngine)KLAB.ENGINE).getUser());
        }
        return userAgent;
    }

    public static Provenance.Agent getEngineAgent() {
        if (engineAgent == null) {
            engineAgent = new ProvenanceAgent(KLAB.ENGINE);
        }
        return engineAgent;
    }

    
}
