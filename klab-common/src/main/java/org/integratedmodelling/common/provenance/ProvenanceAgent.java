package org.integratedmodelling.common.provenance;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IEngine;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IDependency;
import org.integratedmodelling.api.provenance.IProvenance;

public class ProvenanceAgent extends ProvenanceNode implements IProvenance.Agent {

    static enum Type {
        USER,
        ENGINE,
        DEPENDENCY
    }

    IUser    user;
    IEngine  engine;
    IConcept dependency;
    Type     type;

    public ProvenanceAgent(IUser user) {
        super(user.getUsername());
        this.user = user;
        this.type = Type.USER;
    }

    public ProvenanceAgent(IEngine engine) {
        super(engine.getName());
        this.engine = engine;
        this.type = Type.ENGINE;
    }

    public ProvenanceAgent(IDependency dependency) {
        super(dependency.getFormalName());
        this.dependency = dependency.getObservable().getType();
        this.type = Type.DEPENDENCY;
    }

}
