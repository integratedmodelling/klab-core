package org.integratedmodelling.common.provenance;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.lang.IMetadataHolder;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.runtime.IActuator;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.common.interfaces.actuators.IDirectActuator;
import org.integratedmodelling.common.interfaces.actuators.IStateActuator;
import org.integratedmodelling.common.metadata.Metadata;
import org.integratedmodelling.common.model.actuators.Actuator;
import org.integratedmodelling.common.utils.NameGenerator;

public class ProvenanceNode implements IProvenance.Node, IMetadataHolder {

    IMetadata          metadata  = new Metadata();
    String             id        = NameGenerator.shortUUID();
    long               timestamp = System.currentTimeMillis();
    IActuator          actuator  = null;

    String             name;
    private Provenance graph;

    protected ProvenanceNode(String name) {
        this.name = name;
    }

    @Override
    public IMetadata getMetadata() {
        return metadata;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setActuator(IActuator actuator) {
        this.actuator = actuator;
        if (this instanceof ProvenanceArtifact) { 
        	((ProvenanceArtifact)this).setObservation(((Actuator)actuator).getObservation());
        }
    }

    @Override
    public IObservation getObservation() {
        return (actuator instanceof IStateActuator ? ((IStateActuator) actuator).getState()
                : (actuator == null ? null : ((IDirectActuator<?>) actuator).getObservation()));
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProvenanceNode other = (ProvenanceNode) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    public void setGraph(Provenance provenance) {
        this.graph = provenance;
    }

    public Provenance getGraph() {
        return this.graph;
    }
    

}
