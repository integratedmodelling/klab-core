package org.integratedmodelling.common.provenance;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IObservation;
import org.integratedmodelling.api.metadata.IMetadata;
import org.integratedmodelling.api.modelling.IDirectObservation;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.monitoring.Messages;
import org.integratedmodelling.api.provenance.IProvenance;
import org.integratedmodelling.api.runtime.IContext;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.beans.generic.Graph;
import org.integratedmodelling.common.interfaces.NetworkSerializable;
import org.integratedmodelling.exceptions.KlabRuntimeException;
import org.jgrapht.graph.DefaultDirectedGraph;

public class Provenance extends DefaultDirectedGraph<ProvenanceNode, ProvenanceAction>
        implements IProvenance, NetworkSerializable {

    private static final long                serialVersionUID = -4570058469739662229L;

    IContext                                 context;

    /*
     * we number all PRIMARY actions starting at 1.
     */
    int                                      nextActionId;

    ProvenanceArtifact                       root;

    /*
     * observations are "final" artifacts
     */
    Map<IObservation, ProvenanceObservation> obsArtifacts     = new HashMap<>();

    /**
     * There is always a root artifact (the root observation);
     * 
     * @return
     */
    @Override
    public Artifact getRootArtifact() {
        return root;
    }

    /*
     * Observables are goals and also appear once
     */
    Map<IObservableSemantics, ProvenanceObservable> obsGoals = new HashMap<>();

    public ProvenanceNode get(Object agent) {
        ProvenanceNode ret = null;
        if (agent instanceof IObservation) {
            ret = obsArtifacts.get((IObservation) agent);
            if (ret == null) {
                ret = new ProvenanceObservation((IDirectObservation) agent);
                obsArtifacts.put((IObservation) agent, (ProvenanceObservation) ret);
                addVertex(ret);
            }
        } else if (agent instanceof IObservableSemantics) {
            ret = obsGoals.get((IObservableSemantics) agent);
            if (ret == null) {
                ret = new ProvenanceObservable((IObservableSemantics) agent);
                obsGoals.put((IObservableSemantics) agent, (ProvenanceObservable) ret);
                addVertex(ret);
            }
        } else {
            throw new KlabRuntimeException("ahi - unimplemented object in provenance");
        }

        return ret;
    }

    public Provenance(IContext context) {
        super(ProvenanceAction.class);
        this.context = context;
    }

    /**
     * Use to add a direct observation that is made by the user.
     * 
     * @param observation
     * @return
     */
    public ProvenanceArtifact add(IDirectObservation observation) {
        ProvenanceObservation ret = new ProvenanceObservation(observation);
        obsArtifacts.put(observation, ret);
        addVertex(ret);
        if (root == null) {
            root = ret;
        } else {
            addEdge(root, ret, new ObservationAction(ProvenanceManager.getUserAgent()));
        }
        return ret;
    }
    
    public ProvenanceArtifact add(IObservableSemantics observable) {
        ProvenanceObservable ret = new ProvenanceObservable(observable);
        obsGoals.put(observable, ret);
        addVertex(ret);
        if (root == null) {
            root = ret;
        } else {
            addEdge(root, ret, new ObservationAction(ProvenanceManager.getUserAgent()));
        }
        return ret;
    }

    @Override
    public Action add(IProvenance.Node actor, IProvenance.Action action, IProvenance.Node result) {
        addVertex((ProvenanceNode) actor);
        addVertex((ProvenanceNode) result);
        addEdge((ProvenanceNode) actor, (ProvenanceNode) result, (ProvenanceAction) action);
        ((ProvenanceAction) action).setSequence(nextActionId++);
        return action;
    }

    @Override
    public Action add(IProvenance.Node actor, IProvenance.Action action, IProvenance.Node result, IProvenance.Action cause) {
        add(actor, action, result);
        ((ProvenanceAction) action).setCause((ProvenanceAction) cause);
        return action;
    }

    @Override
    public boolean isEmpty() {
        return vertexSet().isEmpty();
    }

    @Override
    public IMetadata collectMetadata(Object node) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Action> getPrimaryActions() {

        List<Action> ret = new ArrayList<>();

        for (ProvenanceAction a : edgeSet()) {
            if (a.sequenceIndex > 0) {
                ret.add(a);
            }
        }

        Collections.sort(ret, new Comparator<Action>() {

            @Override
            public int compare(Action arg0, Action arg1) {
                return Integer
                        .compare(((ProvenanceAction) arg0).sequenceIndex, ((ProvenanceAction) arg1).sequenceIndex);
            }

        });

        return ret;
    }

    @Override
    public boolean addVertex(ProvenanceNode node) {
        node.setGraph(this);
        return super.addVertex(node);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends IModelBean> T serialize(Class<? extends IModelBean> desiredClass) {

        Graph ret = Graph.adapt(this, new Graph.Identifier() {

            @Override
            public String getLabel(Object object) {

                String id = "";

                if (object instanceof ProvenanceNode) {

                    ProvenanceNode o = (ProvenanceNode) object;
                    id = o.getName();

                } else if (object instanceof ProvenanceAction) {
                    // id = ((ProvenanceAction) object).describeType();
                }
                return id;
            }

            @Override
            public IMetadata getMetadata(Object o) {
                return o instanceof ProvenanceNode ? ((ProvenanceNode) o).getMetadata()
                        : (o instanceof ProvenanceAction ? ((ProvenanceAction) o).getMetadata() : null);
            }

            @Override
            public String getType(Object object) {
                if (object instanceof ProvenanceNode) {
                    // ProvenanceNode o = (ProvenanceNode) object;
                    // if (o.model != null) {
                    // return o.model.getObserver() == null ? "amodel" : "dmodel";
                    // } else if (o.observer != null) {
                    // return "observer";
                    // } else if (o.datasource != null) {
                    // return "datasource";
                    // } else if (o.state != null) {
                    // return "state";
                    // }
                }
                return "node";
            }

            @Override
            public Pair<String, String> getTopNode() {
                return null;
            }
        });

        ret.setType(Messages.GRAPH_PROVENANCE);

        return (T) ret;
    }

    @Override
    public Collection<Artifact> getArtifacts() {
        List<Artifact> ret = new ArrayList<>();
        for (org.integratedmodelling.api.provenance.IProvenance.Node n : this.vertexSet()) {
            if (n instanceof Artifact) {
                ret.add((Artifact)n);
            }
        }
        return ret;
    }   
}
