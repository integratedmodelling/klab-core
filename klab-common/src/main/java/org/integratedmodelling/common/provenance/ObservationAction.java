package org.integratedmodelling.common.provenance;

import org.integratedmodelling.api.provenance.IProvenance.Agent;

public class ObservationAction extends ProvenanceAction {
    
    public ObservationAction(Agent agent) {
        super(agent);
    }

    private static final long serialVersionUID = 1067919908958043109L;

}
