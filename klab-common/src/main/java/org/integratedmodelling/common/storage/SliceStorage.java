/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.storage;

import org.apache.commons.lang.mutable.MutableBoolean;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.time.ITemporalExtent;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.exceptions.KlabRuntimeException;

/**
 * Uses a scale to implement storage of a single timeslice (full data if the scale has no
 * time). Storage is done using a PODStorage object. Expects to be written in sequential
 * and uninterrupted time order.
 * 
 * @author ferdinando.villa
 */
public abstract class SliceStorage {

    protected IScale     scale;
    protected int        timeslice                      = -1;
    protected PODStorage storage;
    protected int        dimensionCount                 = 0;
    protected boolean    isDynamic;
    protected boolean    containsMultipleTemporalValues = false;

    public SliceStorage(IScale scale, PODStorage storage, boolean isDynamic) {
        this.isDynamic = isDynamic;
        setScale(isDynamic ? scale : scale.getSubscale(KLAB.c(NS.TIME_DOMAIN), -1));
        this.storage = storage;
    }

    /*
     * must set the storage later if used
     */
    public SliceStorage(IScale scale, boolean isDynamic) {
        this.isDynamic = isDynamic;
        setScale(isDynamic ? scale : scale.getSubscale(KLAB.c(NS.TIME_DOMAIN), -1));
    }

    private void setScale(IScale scale) {
        this.scale = scale;
        if (scale.getTime() != null) {
            timeslice = 0;
            containsMultipleTemporalValues = (scale.isTemporallyDistributed() && scale.getMultiplicity() == scale.getTime().getMultiplicity());
        }
        for (IExtent extent : scale) {
            dimensionCount += extent.getDimensionSizes().length;
        }
    }

    /**
     * Set value at passed overall offset. If the offset identifies a timeslice beyond the
     * current one, storage is flushed. It is a runtime exception to address a timeslice
     * earlier of the current or not immediately following it.
     * 
     * @param offset
     * @param value
     */
    public void set(int offset, Object value) {
        if (timeslice < 0) {
            storage.set(offset, value);
        } else {
            int tslice = getTimeslice(offset);
            if (tslice < timeslice) {
                throw new KlabRuntimeException("write access to history in temporal dataset is not allowed");
            } else if (tslice > timeslice) {
                if (tslice > (timeslice + 1)) {
                    throw new KlabRuntimeException("non-sequential write access to temporal dataset");
                } else {
                    flush(storage, timeslice, tslice);
                    timeslice = tslice;
                    storage.set(containsMultipleTemporalValues ? offset
                            : getNonTemporalOffset(offset), value);
                }
            } else {
                storage.set(containsMultipleTemporalValues ? offset : getNonTemporalOffset(offset), value);
            }
        }
    }

    /**
     * Return a value based on the passed overall offset. If the timeslice is not current,
     * the result will depend on the implemented getNonCurrentValue().
     * 
     * @param offset
     */
    public Object getAt(int offset) {
        
        if (containsMultipleTemporalValues || timeslice < 0) {
            return storage.get(offset);
        }
        int tslice = getTimeslice(offset);
        if (tslice != timeslice) {
            return getNonCurrentValue(offset, tslice, getNonTemporalOffset(offset));
        }
        return storage.get(getNonTemporalOffset(offset));
    }

    /**
     * This is called whenever a set operation requires to flush the current slice.
     * Writing sequentially guarantees that only the minimum number of flushes happens,
     * otherwise things may get really slow and wasteful. By default the data are not
     * zeroed when a slice is flushed, so the previous values remain in the storage. That
     * works OK for temporally based processes where this is normally used. Call zero() on
     * the storage if that's not OK.
     * 
     * @param storage
     * @param currentTimeslice
     * @param nextTimeslice
     */
    public abstract void flush(PODStorage storage, int currentTimeslice, int nextTimeslice);

    /**
     * Called when a get() request identifies an object in a different timeslice than the
     * current one.
     * 
     * @param fullOffset
     * @param timeslice
     * @param nonTemporalOffset
     * @return value in different timeslice
     */
    public abstract Object getNonCurrentValue(int fullOffset, int timeslice, int nonTemporalOffset);

    protected int setTimeslice(int timeslice) {
        int n = (int) scale.getMultiplicity();
        if (scale.getTime() != null) {
            n = (int) (scale.getMultiplicity() / scale.getTime().getMultiplicity());
        }
        this.timeslice = timeslice;
        return n;
    }

    protected int getTimeslice(int offset) {
        if (scale.getTime() == null) {
            return 0;
        }
        return scale.getCursor().getElementIndexes(offset)[0];
    }

    /**
     * Return the linear offset into a single slice of time for the full scale offset
     * passed.
     * 
     * @param offset
     * @return value at offset not considering time
     */
    public int getNonTemporalOffset(int offset) {
        if (scale.getTime() == null) {
            return offset;
        }
        int[] dd = scale.getCursor().getElementIndexes(offset);
        dd[0] = 0;
        return scale.getCursor().getElementOffset(dd);
    }

    /**
     * Return the full scale offset for the passed non-temporal offset in the current
     * timeslice .
     * 
     * @param offset
     * @return full temporal offset for non-temporal input
     */
    public int getTemporalOffset(int offset) {
        if (scale.getTime() == null) {
            return offset;
        }
        int[] dd = scale.getCursor().getElementIndexes(offset);
        dd[0] = timeslice;
        return scale.getCursor().getElementOffset(dd);
    }

    /**
     * Return the full scale offset for the passed non-temporal offset in the passed time.
     * 
     * @param offset
     * @return offset adjusted for time
     */
    public int getTemporalOffsetAt(int offset, ITransition transition) {
        if (scale.getTime() == null) {
            return offset;
        }
        int[] dd = scale.getCursor().getElementIndexes(offset);
        if (transition != null) {
            dd[0] = transition.getTimeIndex();
        }
        return scale.getCursor().getElementOffset(dd);
    }

    /**
     * Get the dimensional offsets for the passed non-temporal offset in the current
     * timeslice.
     * 
     * @param nonTemporalOffset
     * @return
     */
    protected int[] getDimensionOffsets(int nonTemporalOffset, MutableBoolean nodata) {

        int[] extentOffsets = scale.getCursor()
                .getElementIndexes(getTemporalOffset(nonTemporalOffset));
        int[] dimensionOffsets = new int[dimensionCount];

        int eIdx = 0, dIdx = 0;
        nodata.setValue(false);
        for (IExtent ext : scale) {
            for (int o : ext.getDimensionOffsets(extentOffsets[eIdx], true)) {
                dimensionOffsets[dIdx++] = o;
            }
            if (!nodata.booleanValue()) {
                nodata.setValue(!ext.isCovered(extentOffsets[eIdx]));
            }
            eIdx++;
        }
        return dimensionOffsets;
    }

    /**
     * Get the dimensional offsets for the passed full offset.
     * 
     * @param nonTemporalOffset
     * @return
     */
    protected int[] getOffsets(int offset, MutableBoolean nodata) {

        int[] extentOffsets = scale.getCursor().getElementIndexes(offset);
        int[] dimensionOffsets = new int[dimensionCount];

        int eIdx = 0, dIdx = 0;
        nodata.setValue(false);
        for (IExtent ext : scale) {
            for (int o : ext.getDimensionOffsets(extentOffsets[eIdx], true)) {
                dimensionOffsets[dIdx++] = o;
            }
            if (!nodata.booleanValue()) {
                nodata.setValue(!ext.isCovered(extentOffsets[eIdx]));
            }
            eIdx++;
        }
        return dimensionOffsets;
    }

    protected int[] getSliceDimensions() {
        int[] ret = new int[dimensionCount];
        int i = 0;
        for (IExtent extent : scale) {
            if (extent instanceof ITemporalExtent) {
                ret[i++] = 1;
            } else {
                // reverse order - only matters for space, and this thing wants lat/lon
                int[] dims = extent.getDimensionSizes();
                for (int ii = dims.length - 1; ii >= 0; ii--) {
                    ret[i++] = dims[ii];
                }
            }
        }
        return ret;
    }

    protected int getSliceMultiplicity() {
        boolean isTemporalOnly = scale.isTemporallyDistributed()
                && scale.getMultiplicity() == scale.getTime().getMultiplicity();
        return (int) (scale.getMultiplicity()
                / ((timeslice < 0 || isTemporalOnly) ? 1L : scale.getTime().getMultiplicity()));
    }

}
