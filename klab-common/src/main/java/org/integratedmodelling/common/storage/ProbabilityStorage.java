/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.storage;

import org.integratedmodelling.api.data.IProbabilityDistribution;
import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IClassification;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.IScale;
import org.integratedmodelling.api.modelling.scheduling.ITransition;
import org.integratedmodelling.api.modelling.storage.IDataset;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;

public class ProbabilityStorage extends AbstractStorage<IProbabilityDistribution> {

    IClassification discretization = null;

    public ProbabilityStorage(IObservableSemantics observable, IScale scale, IDataset dataset, boolean isDynamic,
            IClassification classification) {

        super(scale, isDynamic);
        this.discretization = classification;
        if (dataset == null) {
            storage = new PODStorage(getSliceMultiplicity(), PODStorage.Type.DISTRIBUTION);
        } else {
            dStore = dataset.getStorage(observable, isDynamic, true);
        }
    }

    @Override
    public IProbabilityDistribution get(int index) {
        Object o = dStore == null ? getAt(index) : fromStorage(dStore.get(index));
        return (o instanceof IProbabilityDistribution) ? (IProbabilityDistribution) o : null;
    }
    
    /**
     * Return the most likely concept from the discretization at the given point, or
     * null if there are no data or there is no discretization.
     * 
     * @param index
     * @return
     */
    public IConcept getMostLikelyClass(IProbabilityDistribution distribution) {
        
        IConcept ret = null;
        if (discretization != null && distribution instanceof IndexedCategoricalDistribution) {
            int i = 0;
            IndexedCategoricalDistribution dist = (IndexedCategoricalDistribution)distribution;
            double max = Double.NEGATIVE_INFINITY;
            for (IConcept c : discretization.getConceptOrder()) {
                if (dist.getData()[i] > max) {
                    max = dist.getData()[i];
                    ret = c;
                }
                i++;
            }
        }
        
        return ret;
    }

    @Override
    public Class<?> getDataClass() {
        return IProbabilityDistribution.class;
    }

    @Override
    public void flush(ITransition incomingTransition) {
        if (isDynamic && dStore != null) {
            dStore.flush(incomingTransition);
        }
    }

    @Override
    public double getMin() {
        return Double.NaN;
    }

    @Override
    public double getMax() {
        return Double.NaN;
    }

    @Override
    public boolean isProbabilistic() {
        return true;
    }

}
