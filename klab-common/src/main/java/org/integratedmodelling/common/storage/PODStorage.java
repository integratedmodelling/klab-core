/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.common.storage;

import java.lang.reflect.Array;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.LongBuffer;
import java.nio.ShortBuffer;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;

import org.apache.commons.collections.BidiMap;
import org.apache.commons.collections.bidimap.DualHashBidiMap;
import org.integratedmodelling.api.data.IProbabilityDistribution;
import org.integratedmodelling.collections.Pair;
import org.integratedmodelling.common.data.IndexedCategoricalDistribution;
import org.integratedmodelling.common.utils.CompressionUtils;
import org.integratedmodelling.exceptions.KlabIOException;
import org.integratedmodelling.exceptions.KlabRuntimeException;

// import ucar.ma2.Array;
import ucar.ma2.DataType;

/**
 * Use this instead of an array of Object to hold PODs when size and allocation time are a
 * concern.
 * 
 * Holds POD arrays that are as small as possible to hold a particular type without having
 * to use objects. Sacrifices a little speed (due to a switch at get/set) to small size,
 * and allows indexing any comparable objects using integers and a bidirectional map. All
 * objects (including booleans) allow storage of no-data values, returned as null by
 * get().
 * 
 * @author ferdinando.villa
 *
 */
public class PODStorage {

    public enum Type {
        DOUBLE,
        INT,
        LONG,
        INT_IDX,
        BOOLEAN,
        DISTRIBUTION
    };

    final public static int INT_IDX = 4;
    final public static int BOOLEAN = 5;

    short                            objIdx    = Short.MIN_VALUE + 1;
    Type                             dType     = Type.DOUBLE;
    double[]                         dData     = null;
    long[]                           lData     = null;
    int[]                            iData     = null;
    short[]                          sData     = null;
    IndexedCategoricalDistribution[] pData     = null;
    BidiMap                          dMap      = null;
    BitSet                           bData     = null;
    BitSet                           bNullData = null;
    int                              size      = 0;
    boolean                          dirty     = true;

    public PODStorage(int size, Type type) {
        this.dType = type;
        this.size = size;
        switch (type) {
        case BOOLEAN:
            bData = new BitSet(size);
            bNullData = new BitSet(size);
            break;
        case DOUBLE:
            dData = new double[size];
            Arrays.fill(dData, Double.NaN);
            break;
        case INT:
            iData = new int[size];
            break;
        case INT_IDX:
            sData = new short[size];
            dMap = new DualHashBidiMap();
            break;
        case LONG:
            lData = new long[size];
            break;
        case DISTRIBUTION:
            pData = new IndexedCategoricalDistribution[size];
            break;
        }
    }

    /**
     * Set all data to no-data.
     */
    public void zero() {
        switch (dType) {
        case BOOLEAN:
            bNullData.set(0, bNullData.size());
            break;
        case DOUBLE:
            Arrays.fill(dData, Double.NaN);
            break;
        case INT:
            Arrays.fill(iData, Integer.MIN_VALUE);
            break;
        case INT_IDX:
            Arrays.fill(sData, Short.MIN_VALUE);
            dMap = new DualHashBidiMap();
            break;
        case LONG:
            Arrays.fill(lData, Long.MIN_VALUE);
            break;
        case DISTRIBUTION:
            Arrays.fill(pData, null);
            break;
        }
    }

    /**
     * Get the most immediately usable representation of the object at index, returning
     * null for no data and translating indexed values.
     * 
     * @param index
     * @return object at index
     */
    public Object get(int index) {
        switch (dType) {
        case BOOLEAN:
            return bNullData.get(index) ? null : bData.get(index);
        case DOUBLE:
            return Double.isNaN(dData[index]) ? null : dData[index];
        case INT:
            return iData[index] == Integer.MIN_VALUE ? null : iData[index];
        case INT_IDX:
            return sData[index] == Short.MIN_VALUE ? null : dMap.get(sData[index]);
        case LONG:
            return lData[index] == Long.MIN_VALUE ? null : lData[index];
        case DISTRIBUTION:
            return pData[index];
        }
        return null;
    }

    /**
     * Coerce the return value to be POD, returning the uninterpreted no data values and
     * returning the index for indexed values. Booleans will return false for no data.
     * 
     * @param index
     * @return object at index, coerced to POD
     */
    public Object getPOD(int index) {
        switch (dType) {
        case BOOLEAN:
            return bNullData.get(index) ? false : bData.get(index);
        case DOUBLE:
            return dData[index];
        case INT:
            return iData[index];
        case INT_IDX:
            return sData[index];
        case LONG:
            return lData[index];
        case DISTRIBUTION:
            return pData[index] == null ? Double.NaN : pData[index].getMean();
        }
        return Double.NaN;
    }

    /*
     * use only when type is known without error.
     */
    public double getDouble(int index) {
        return dData[index];
    }

    public int getInt(int index) {
        return iData[index];
    }

    public boolean getBoolean(int index) {
        return bData.get(index);
    }

    public long getLong(int index) {
        return lData[index];
    }

    public boolean booleanize(Object value) {
        if (value instanceof Boolean) {
            return (Boolean) value;
        }
        if (value instanceof Number) {
            return ((Number) value).intValue() != 0;
        }
        return false;
    }

    public void set(int index, Object value) {

        switch (dType) {
        case BOOLEAN:
            if (value == null) {
                bNullData.set(index);
            } else {
                bData.set(index, booleanize(value));
            }
            break;
        case DOUBLE:
            dData[index] = value == null ? Double.NaN : ((Number) value).doubleValue();
            break;
        case INT:
            iData[index] = value == null ? Integer.MIN_VALUE : ((Number) value).intValue();
            break;
        case INT_IDX:
            Short idx = Short.MIN_VALUE;
            if (value != null) {
                idx = (Short) dMap.getKey(value);
                if (idx == null) {
                    idx = ++objIdx;
                    dMap.put(idx, value);
                }
            }
            sData[index] = idx == null ? Short.MIN_VALUE : idx;
            break;
        case LONG:
            lData[index] = value == null ? Long.MIN_VALUE : ((Number) value).longValue();
            break;
        case DISTRIBUTION:
            pData[index] = (IndexedCategoricalDistribution) value;
            break;
        }
    }

    /**
     * Number of objects
     * 
     * @return number of objects
     */
    public int size() {
        return size;
    }

    /**
     * Return the full data content as the shortest possible string value for marshalling
     * over the network.
     * 
     * @return encoded state
     * @throws KlabIOException
     */
    public String getEncodedBytes() throws KlabIOException {
        switch (dType) {
        case BOOLEAN:
            return CompressionUtils.compressToString(bData, bNullData);
        case DOUBLE:
            // FIXME - lose precision and half the weight. For now OK, although obviously
            // we should
            // eventually have a FLOAT type and use that if required.
            float[] f = new float[size];
            for (int i = 0; i < f.length; i++) {
                f[i] = (float) dData[i];
            }
            return CompressionUtils.compressToString(f);
        case INT:
            return CompressionUtils.compressToString(iData);
        case INT_IDX:
            // TODO the key needs to be exactly the same to allow decoding.
            return CompressionUtils.compressToString(sData);
        case LONG:
            return CompressionUtils.compressToString(lData);
        case DISTRIBUTION:
            throw new KlabRuntimeException("unimplemented: encoding of probabilistic data");
        }
        return null;
    }

    public Class<?> getDataClass() {
        switch (dType) {
        case BOOLEAN:
            return Boolean.class;
        case DOUBLE:
            return Double.class;
        case INT:
            return Integer.class;
        case INT_IDX:
            return dMap.size() > 0 ? dMap.values().iterator().next().getClass() : Object.class;
        case LONG:
            return Long.class;
        case DISTRIBUTION:
            return IProbabilityDistribution.class;
        }
        return null;

    }

    /**
     * Set the data from a string encoded by getEncodedBytes().
     * 
     * @param encodedBytes
     */
    public void setBytes(String encodedBytes) {
        try {
            switch (dType) {
            case BOOLEAN:
                Pair<BitSet, BitSet> bb = CompressionUtils.decompressBooleans(encodedBytes);
                bData = bb.getFirst();
                bNullData = bb.getSecond();
                break;
            case DOUBLE:
                FloatBuffer fb = CompressionUtils.decompressFloats(encodedBytes);
                for (int i = 0; i < size; i++) {
                    dData[i] = fb.get(i);
                }
                break;
            case INT:
                IntBuffer ib = CompressionUtils.decompressInts(encodedBytes);
                for (int i = 0; i < size; i++) {
                    iData[i] = ib.get(i);
                }
                break;
            case INT_IDX:
                // TODO must reconstruct keys too
                ShortBuffer sb = CompressionUtils.decompressShorts(encodedBytes);
                for (int i = 0; i < size; i++) {
                    sData[i] = sb.get(i);
                }
                break;
            case LONG:
                LongBuffer lb = CompressionUtils.decompressLongs(encodedBytes);
                for (int i = 0; i < size; i++) {
                    lData[i] = lb.get(i);
                }
                break;
            case DISTRIBUTION:
                throw new KlabRuntimeException("unimplemented: decoding of probabilistic data");
            }
        } catch (Exception e) {
            throw new KlabRuntimeException(e);
        }
    }

    /**
     * Utility to bridge to NetCDF types.
     * 
     * @return NetCDF data type
     */
    public DataType getDataType() {

        DataType ret = null;

        switch (dType) {
        case INT:
            ret = DataType.INT;
            break;
        case INT_IDX:
            ret = DataType.SHORT;
            break;
        case LONG:
            ret = DataType.LONG;
            break;
        case BOOLEAN:
            // NetCDF has boolean but won't accept it
            ret = DataType.SHORT;
            break;
        case DOUBLE:
            ret = DataType.DOUBLE;
            break;
        case DISTRIBUTION:
            // FIXME for now we store means, which means only "live" distribution data are
            // returned.
            ret = DataType.DOUBLE;
            break;
        }

        return ret;
    }

    // bulk set
    public void set(Object data) {
        if (data.getClass().isArray()) {
            for (int i = 0; i < Array.getLength(data); i++) {
                set(i, Array.get(data, i));
            }
        } else if (data instanceof List) {
            List<?> coll = (List<?>) data;
            for (int i = 0; i < coll.size(); i++) {
                set(i, coll.get(i));
            }
        } else {
            throw new KlabRuntimeException("cannot set a state from a "
                    + data.getClass().getCanonicalName());
        }
    }
}
