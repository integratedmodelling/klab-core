# Configuring a Geoserver instance to work with the /get/wcs and /get/wfs endpoints

A k.LAB server can work as a proxy for WCS and WFS calls using URNs managed locally. When a /get/{wcs|wfs}/[urn] call is received, a properly configured server acts as a proxy for both services, talking to a Geoserver instance that can be behind the firewall. Access control is entirely dependent on URN data access rules. 

The first step is to properly configure the geoserver instance. In order to set up geoserver, a service key for the geoserver instance managed by k.LAB needs to be defined. This will be done through the web interface in version 1.0, but at the current stage of development, a record must be provided as a JSON object defined in the services/services.json file under the data directory of the k.LAB server. 

> NOTE: there is redundancy between the k.LAB service config and the Mongo/JSON configuration in the collaboration codebase. The method described here works in any k.LAB server including the collaboration service.

A sample services/services.json looks like this:

```
{
	// key
	"28619a88-cc34-4d28-9bd3-e25ed377b17d":
	{ "name" : "Local Geoserver", " url" : "http://127.0.0.1:8091/geoserver", "type" : " geoserver" }
}
```

The key does not matter but it must be unique; the best way to generate one manually is to use a site that generates UUID strings, like https://www.uuidgenerator.net/. DO NOT copy the key above, and keep it hidden!

Data records linked to URNs should reference the key above (as field 'serverKey') to select the server they should be associated with. If there is only one server and the data record provides no server key, the single server is used by default. This mechanism is general: the specific API for the service depends on the service type (second field in the URL after /get/), which is associated to a Java handler object through a org.integratedmodelling.api.annotations.ResourceService annotation.

## Configuring Geoserver proxy URL.

If only WCS access is desired, no further configuration is necessary. 

If WFS is required, ensure that the configuration file is installed with the correct URL for the local geoserver (which must be reachable by the k.LAB server but can be a local URL or be behind a firewall, secured for public access from outside). Then go to the Geoserver 'Settings->Global' configuration page and define the proxy URL as <k.LAB server URL>/get/wfs/<node name>:service:geoserver:<key> for example if the node name is 'aldo' running on 127.0.0.1:8187/knode and the key is the one above, the URL should be http://127.0.0.1:8187/knode/get/wfs/aldo:service:geoserver:28619a88-cc34-4d28-9bd3-e25ed377b17d. With this done, k.LAB will properly talk to geoserver back and forth, and you can define all of your WCS and WFS services using the correspondent /get services with k.LAB. 

Read below for further WFS requirements. Note that when the proxy URL is established, Geoserver will only work correctly as long as the k.LAB server is available to proxy the requests: if the k.LAB server is down, Geoserver will produce unusable URLs unless the proxy configuration is removed.

## WCS

WCS is a simple protocol and only requires a single trip per request. Indeed if only WCS is desired, the proxy URL configuration can be skipped. All GET/POST WCS requests normally sent to Geoserver can be now sent as {k.LAB}/get/wcs/{urn}?.... and will respond with an access denied response when the URN is not accessible by the authenticated k.LAB user.

## WFS

The getCapabilities response in Geoserver WFS returns absolute URLs, and these are used from within the Geotools WFS client to access namespaces and other resources. For this reason, before calls to geoserver can be transparently proxied through the k.LAB wcs and wfs services, an instance of geoserver needs to be configured so that all URLs returned by getCapabilities are also proxied. If this is not done, the URLs will be wrong, due to assumptions made from within the WFS client that do not consider the possibility of proxies being configured.

> NOTE: The namespace and resource ID in any URN that points to WFS resources must be the exact same as in Geoserver, with the possible exception of using dots where Geoserver (which does not behave well with dot-separated names anywhere) uses dashes. So any dot in the URN namespace or resource ID will be translated into a dash before it's submitted to Geoserver, but otherwise the names must be identical.
