package org.integratedmodelling.knode.controller;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.configuration.IResourceConfiguration.StaticResource;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.network.IComponent;
import org.integratedmodelling.api.network.IServer;
import org.integratedmodelling.api.network.IServer.Authentication;
import org.integratedmodelling.api.network.INodeNetwork;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.api.services.IPrototype;
import org.integratedmodelling.common.auth.IUserRegistry;
import org.integratedmodelling.common.auth.UserAuthorizationProvider;
import org.integratedmodelling.common.beans.DataSource;
import org.integratedmodelling.common.beans.Network;
import org.integratedmodelling.common.beans.Node;
import org.integratedmodelling.common.beans.Service;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.integratedmodelling.common.beans.responses.UserView;
import org.integratedmodelling.common.client.palette.Palette;
import org.integratedmodelling.common.client.palette.PaletteManager;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.UrnResolver;
import org.integratedmodelling.common.network.NodeNetwork;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Endpoints and directories related to the establishment of the IM network.
 * 
 * FIXME these should be secured using permanent server tokens and roles,
 * established at initialization with certificate comparison. This way each
 * server can trust the network session requests coming in through /connect.
 * 
 * Also, server certificates should be specific of the route, not the server,
 * have both endpoint's tokens compared at handshake, and be extensible without
 * regenerating the token. Each server should use its own SSL private key.
 * 
 * @author Ferd
 *
 */
@RestController
public class NetworkController {

	@Autowired
	UserAuthorizationProvider authorizationProvider;

	@Autowired
	IUserRegistry userRegistry;

	@Autowired
	UrnResolver urnResolver;

	private RestTemplate template = new RestTemplate();

	/**
	 * IDENTIFY request handler
	 * 
	 * FIXME should be merely a GET with the node authorization token, passing
	 * around the previously accepted request.
	 * 
	 * @param request
	 * @param servletRequest
	 * @return the network topology
	 * @throws KlabAuthorizationException
	 */
	@RequestMapping(value = API.IDENTIFY, method = RequestMethod.POST)
	public @ResponseBody Network identify(@RequestBody ConnectionAuthorization request,
			HttpServletRequest servletRequest) throws KlabAuthorizationException {

		if (request.getTraversedNodes().contains(KLAB.ENGINE.getName())) {
			return null;
		}

		request.getTraversedNodes().add(KLAB.ENGINE.getName());

		Network ret = new Network();

		if (!KLAB.ENGINE.getResourceConfiguration().isAuthorized(request.getUsername(), request.getUserGroups(),
				request.getRequestingIP())) {
			return ret;
		}

		/*
		 * authorized: memorize privilege set and add self
		 */
		Node self = new Node();

		self.setAuthToken(userRegistry.authorize(request));
		self.setAuthentication(
				authorizationProvider.isAuthorizationDirect() ? Authentication.DIRECT : Authentication.INDIRECT);
		self.setUrl(KLAB.ENGINE.getNetwork().getUrl());
		self.setVersion(Version.CURRENT);
		self.setBuild(Version.VERSION_BUILD);

		for (String pid : KLAB.ENGINE.getResourceConfiguration().getSynchronizedProjectIds()) {
			IProject project = KLAB.PMANAGER.getProject(pid);
			if (project != null) {
				if (KLAB.ENGINE.getResourceConfiguration().isAuthorized(project, request.getUsername(),
						request.getUserGroups(), request.getRequestingIP())) {
					self.getSynchronizedProjectUrns().add(ResourceFactory.getProjectUrn(project));
				}
			}
		}

		/**
		 * Add any toolkit that the user has access to
		 */
		for (Palette palette : PaletteManager.get().getPalettes()) {
			Set<String> pgroups = new HashSet<>();
			pgroups.addAll(palette.getAllowedGroups());
			pgroups.retainAll(request.getUserGroups());
			if (!pgroups.isEmpty()) {
				self.getToolkits().add(palette);
			}
		}

		for (StaticResource sr : StaticResource.values()) {
			if (KLAB.ENGINE.getResourceConfiguration().isAuthorized(sr, request.getUsername(), request.getUserGroups(),
					request.getRequestingIP())) {
				self.getStaticResourceIds().add(sr.name());
			}
		}

		for (IComponent component : KLAB.PMANAGER.getComponents()) {
			if (KLAB.ENGINE.getResourceConfiguration().isAuthorized(component, request.getUsername(),
					request.getUserGroups(), request.getRequestingIP())) {
				self.getComponentUrns().add(ResourceFactory.getComponentUrn(component));
			}
		}

		/**
		 * publish function prototypes from our own components.
		 */
		for (IPrototype p : KLAB.ENGINE.getFunctionPrototypes()) {
			if (p.getComponentId() != null && KLAB.PMANAGER.getComponent(p.getComponentId()) != null) {
				self.getFunctions().add(KLAB.MFACTORY.adapt(p, Service.class));
			}
		}

		ret.getNodes().add(self);

		/*
		 * Build network view: first build our node descriptor for this user and
		 * add it to the response
		 */
		for (IServer node : KLAB.ENGINE.getNetwork().getNodes()) {

			if (request.getTraversedNodes().contains(node.getId())) {
				continue;
			}

			try {
				Network nr = template.postForObject(node.getUrl() + API.IDENTIFY + ".json",
						new ConnectionAuthorization(request), Network.class);
				if (nr != null) {
					ret.merge(nr);
				}
			} catch (Throwable e) {

				KLAB.info("quarantining unresponsive node " + node.getId());
				/*
				 * put the unresponsive node under observation for a while; the
				 * network decides what to do and when to propose it again.
				 */
				((NodeNetwork) KLAB.ENGINE.getNetwork()).quarantine(node);
			}
		}

		/*
		 * this will allow the primary node to be found w/o additional
		 * information.
		 */
		ret.setNodeId(KLAB.ENGINE.getName());

		return ret;
	}

	/**
	 * CONNECT request handler.
	 * 
	 * @param certificate
	 * @param servletRequest
	 * @return the user view with its privileges and the network as seen through
	 *         them.
	 * @throws KlabAuthorizationException
	 */
	@RequestMapping(value = API.CONNECT, method = RequestMethod.POST, consumes = "text/plain")
	public @ResponseBody UserView connect(@RequestBody String certificate, HttpServletRequest servletRequest)
			throws KlabAuthorizationException {

		UserView ret = new UserView();

		AuthorizationResponse authorization = authorizationProvider.authenticateUser(certificate);

		ret.setUserProfile(authorization.getProfile());
		ret.setAuthToken(authorization.getAuthToken());

		ConnectionAuthorization nrequest = new ConnectionAuthorization(authorization.getProfile(),
				servletRequest.getRemoteAddr());
		ret.setNetwork(identify(nrequest, servletRequest));

		return ret;
	}

	/**
	 * Called with a previously issued node authorization token to obtain the
	 * current network status.
	 * 
	 * @param headers
	 * @param servletRequest
	 * @return the current network view for a previously authorized engine
	 * @throws KlabAuthorizationException
	 *             TODO should be integrated in security framework and respond a
	 *             401
	 */
	@RequestMapping(value = API.GET_NETWORK, method = RequestMethod.GET)
	public @ResponseBody Network getNetwork(@RequestHeader HttpHeaders headers, HttpServletRequest servletRequest)
			throws KlabAuthorizationException {
		ConnectionAuthorization authorization = getConnectionAuthorization(headers.get(API.AUTHENTICATION_HEADER));
		if (authorization == null) {
			throw new KlabAuthorizationException("no node authorization for get-network");
		}
		return identify(authorization, servletRequest);
	}

	/*
	 * Painful helper to avoid the ugliness of checking if the array isn't empty
	 * when calling the real function.
	 * 
	 * @param tokens
	 * 
	 * @return the authorization
	 */
	ConnectionAuthorization getConnectionAuthorization(Collection<String> tokens) {
		if (tokens == null || tokens.size() == 0) {
			return null;
		}
		for (String token : tokens) {
			ConnectionAuthorization ret = userRegistry.getConnectionAuthorization(token);
			if (ret != null) {
				return ret;
			}
		}
		return null;
	}

	@RequestMapping(value = { API.GET_RESOURCE_INFO }, method = RequestMethod.GET)
	@ResponseBody
	public Object getResourceInfo(@PathVariable String urn, @RequestHeader HttpHeaders headers,
			HttpServletRequest request, HttpServletResponse response) throws KlabException {

		boolean authenticated = false;
		ConnectionAuthorization authorization = null;
		List<String> auth = headers.get(API.AUTHENTICATION_HEADER);
		String token = null;
		if (auth != null && auth.size() > 0) {
			token = auth.get(0);
		}
		if (token != null) {

			if (KLAB.ENGINE.getNetwork() instanceof INodeNetwork) {
				authenticated = token.equals(((INodeNetwork) KLAB.ENGINE.getNetwork()).getKey());
			}
			if (!authenticated) {
				/*
				 * check if it's previously authorized. Works for nodes and for
				 * engines.
				 */
				authorization = authorizationProvider.getAuthorization(token);
				if (authorization != null) {
					authorization.setRequestingIP(request.getRemoteAddr());
					authenticated = true;
				}
			}
		}

		return KLAB.MFACTORY.adapt(urnResolver.resolveUrn(urn, authorization, KLAB.ENGINE.getResourceConfiguration()),
				DataSource.class);

	}

}
