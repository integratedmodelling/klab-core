package org.integratedmodelling.knode.controller;

import javax.servlet.http.HttpServletRequest;

import org.integratedmodelling.Version;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.beans.Model;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.beans.requests.ModelQuery;
import org.integratedmodelling.common.beans.responses.ModelQueryResponse;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.engine.modelling.resolver.ResolutionScope;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ferdinando.villa
 * Publishes the services using node authentication.
 * 
 * TODO secure with a NetworkSession and just extract the nodes from
 * there.
 * 
 */
@RestController
public class ModelKboxController {

    @Autowired
    NetworkController networkController;

    /**
     * Matching and authorized models in own kbox, with rankings for passed criteria.
     * 
     * @param query 
     * @param headers 
     * @param request 
     * @return the response, containing a list of matching model beans
     * @throws KlabException 
     * 
     */
    @RequestMapping(value = API.QUERY_MODELS, method = RequestMethod.POST)
    public @ResponseBody ModelQueryResponse query(@RequestBody ModelQuery query, @RequestHeader HttpHeaders headers, HttpServletRequest request)
            throws KlabException {

        String version = Version.CURRENT;
        if (headers.get(API.KLAB_VERSION_HEADER) != null && version.equals(headers.get(API.KLAB_VERSION_HEADER))) {
            /*
             * TODO upgrade/downgrade query
             */
        }
        
        ConnectionAuthorization credentials = networkController
                .getConnectionAuthorization(headers.get(API.AUTHENTICATION_HEADER));

        if (credentials == null) {
            throw new KlabAuthorizationException("no node authorization for model query");
        }

        IObservableSemantics observable = KLAB.MFACTORY.adapt(query.getObservable(), ObservableSemantics.class);
        IResolutionScope scope = KLAB.MFACTORY.adapt(query.getScope(), ResolutionScope.class);

        ModelQueryResponse ret = new ModelQueryResponse();
        
        if (observable == null || observable.getType() == null) {
        	/*
        	 * we don't even know the concept. Just return an empty response.
        	 */
        	return ret;
        }
        
        for (Model md : ModelKbox.get().queryModels(observable, scope)) {

            IProject project = KLAB.PMANAGER.getProject(md.getProjectId());
            if (project == null || !KLAB.ENGINE.getResourceConfiguration().isAuthorized(project, credentials
                    .getUsername(), credentials.getUserGroups(), credentials.getRequestingIP())) {
                continue;
            }
            /*
             * publish the URN so that clients can retrieve the project.
             */
            md.setProjectUrn(ResourceFactory.getProjectUrn(project));
//            Model mdl = KLAB.MFACTORY.adapt(md, Model.class);
            md.setServerId( KLAB.ENGINE.getName());
            
            ret.getModels().add(md);
        }
        return ret;
    }

    /**
     * Get a model's descriptor. Again unsecured, used with specific authentication
     * in subclassed controllers.
     * 
     * @param id
     * @param headers
     * @param request
     * @return a bean describing a model and the project it comes from
     */
    @RequestMapping(value = API.RETRIEVE_MODEL, method = RequestMethod.GET)
    public @ResponseBody Model retrieve(@RequestParam String id, @RequestHeader HttpHeaders headers, HttpServletRequest request) {
        return null;
    }
}
