package org.integratedmodelling.knode.controller;

import javax.servlet.http.HttpServletRequest;

import org.integratedmodelling.api.network.API;
import org.integratedmodelling.common.beans.Observation;
import org.integratedmodelling.common.beans.requests.ObservationQuery;
import org.integratedmodelling.common.beans.responses.ObservationQueryResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * The node implementation, only operating on the local kbox and using
 * node authentication.
 *
 * TODO secure with a NetworkSession 
 */
@RestController
public class ObservationKboxController {

    @RequestMapping(value = API.QUERY_OBSERVATIONS, method = RequestMethod.POST)
    public @ResponseBody ObservationQueryResponse query(@RequestBody ObservationQuery query, @RequestHeader HttpHeaders headers, HttpServletRequest request) {
        return null;
    }

    @RequestMapping(value = API.RETRIEVE_OBSERVATION, method = RequestMethod.GET)
    public @ResponseBody Observation retrieve(@PathVariable String id, @RequestHeader HttpHeaders headers, HttpServletRequest request) {
        return null;
    }
}
