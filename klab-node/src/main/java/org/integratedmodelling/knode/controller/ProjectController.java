package org.integratedmodelling.knode.controller;

import org.integratedmodelling.kserver.controller.AbstractProjectController;
import org.springframework.web.bind.annotation.RestController;

/**
 * Simply provides the three basic endpoints - list, reload and deploy a local project.
 * @author ferdinando.villa
 *
 */
@RestController
public class ProjectController extends AbstractProjectController {

}
