/*******************************************************************************
 * Copyright (C) 2007, 2015:
 * 
 * - Ferdinando Villa <ferdinando.villa@bc3research.org> - integratedmodelling.org - any
 * other authors listed in @author annotations
 *
 * All rights reserved. This file is part of the k.LAB software suite, meant to enable
 * modular, collaborative, integrated development of interoperable data and model
 * components. For details, see http://integratedmodelling.org.
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms
 * of the Affero General Public License Version 3 or any later version.
 *
 * This program is distributed in the hope that it will be useful, but without any
 * warranty; without even the implied warranty of merchantability or fitness for a
 * particular purpose. See the Affero General Public License for more details.
 * 
 * You should have received a copy of the Affero General Public License along with this
 * program; if not, write to the Free Software Foundation, Inc., 59 Temple Place - Suite
 * 330, Boston, MA 02111-1307, USA. The license is also available at:
 * https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.knode;

import java.io.IOException;

import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.configuration.KLAB.BootMode;
import org.integratedmodelling.engine.NodeEngine;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.kserver.controller.KServerController;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
        "org.integratedmodelling.knode",
        "org.integratedmodelling.error.kserver",   // link in the error controller
        "org.integratedmodelling.auth.knode",      // links in the auth provider
        "org.integratedmodelling.common.data",     // links in the basic urn resolver
        "org.integratedmodelling.common.configuration.services",    // service conf
        "org.integratedmodelling.kserver" })
public class Application {

    public static void main(String[] args) throws IOException {

        if (!KServerController.setup(BootMode.NODE)) {
            KLAB.error("node server initialization failed. Exiting.");
            System.exit(1);
        }

        final ConfigurableApplicationContext context = SpringApplication.run(Application.class, args);
        final KServerController controller = context.getBean(KServerController.class);

        /*
         * complete the boot phase loading knowledge after all services are up.
         */
        try {
            ((NodeEngine)KLAB.ENGINE).loadKnowledge();
        } catch (KlabException e) {
            KLAB.error("knowledge initialization failed. Exiting.");
            System.exit(1);
        }
        
        /*
         * print a nice visible startup banner - stdout not log
         */
        System.out.println();
        System.out.println("****************************************************************************");
        System.out.println("* k.LAB node " + KLAB.ENGINE.getName() + " ready at " + KLAB.ENGINE.getUrl());
        System.out.println("* Node workspace set to " + KLAB.WORKSPACE.getDefaultFileLocation());
        System.out.println("****************************************************************************");

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // SessionManager sessionManager = context.getBean(SessionManager.class);
                // if (sessionManager != null) {
                // sessionManager.pausePolling();
                // }
                if (controller != null) {
                    KLAB.ENGINE.shutdown(0);
                }
            }
        });
    }

}
