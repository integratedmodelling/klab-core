package org.integratedmodelling.auth.knode;

import org.integratedmodelling.auth.indirect.IndirectAuthorizationProvider;
import org.integratedmodelling.common.auth.IUserRegistry;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * The default authorization provider for a node will check into authorizations coming
 * from secondary node authentication as well as providing regular, indirect
 * authentication.
 * 
 * @author ferdinando.villa
 *
 */
@Component
public class NodeAuthorizationProvider extends IndirectAuthorizationProvider {

    @Autowired
    IUserRegistry userRegistry;

    @Override
    public ConnectionAuthorization getAuthorization(String token) {
        ConnectionAuthorization ret = userRegistry.getConnectionAuthorization(token);
        if (ret != null) {
            return ret;
        }
        return super.getAuthorization(token);
    }

}
