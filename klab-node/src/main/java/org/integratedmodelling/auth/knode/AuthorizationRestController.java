package org.integratedmodelling.auth.knode;

import org.integratedmodelling.api.network.API;
import org.integratedmodelling.common.beans.requests.AuthorizationRequest;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Just decorates the main controller class with the REST mappings. Need to do this as
 * NetworkController will need the component, but can't define the mappings within the
 * org.integratedmodelling.knode package because the derived server for collaboration also
 * does that.
 * 
 * @author ferdinando.villa
 *
 */
@RestController
public class AuthorizationRestController {

    @Autowired
    AuthorizationController authorizationController;

    /**
     * Authorize using username/password. Delegate to authorization provider for the
     * actual authentication.
     * 
     * @param request
     * @return the response bean
     * @throws KlabAuthorizationException
     */
    @RequestMapping(value = API.AUTHENTICATION, method = RequestMethod.POST)
    public @ResponseBody AuthorizationResponse authorizeUsingUsername(@RequestBody AuthorizationRequest request)
            throws KlabAuthorizationException {
        return authorizationController.authorizeFromUsername(request);
    }

    /**
     * Authorize through cert file delegating to authorization provider. If authentication
     * fails, check out any certificates in <data>/authorization subdir and if we have a
     * match, make a user anyway and assign an internally valid token.
     * 
     * @param certificate
     * @return a response bean.
     * @throws KlabAuthorizationException
     */
    @RequestMapping(
            value = API.AUTHENTICATION_CERT_FILE,
            method = RequestMethod.POST,
            consumes = "text/plain")
    public @ResponseBody AuthorizationResponse authorizeUsingCertificate(@RequestBody String certificate)
            throws KlabAuthorizationException {
        return authorizationController.authorizeFromCertificate(certificate);
    }

}
