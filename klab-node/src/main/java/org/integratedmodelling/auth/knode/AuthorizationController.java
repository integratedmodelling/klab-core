package org.integratedmodelling.auth.knode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.integratedmodelling.api.network.IServer.Authentication;
import org.integratedmodelling.auth.indirect.IndirectAuthorizationProvider;
import org.integratedmodelling.common.auth.LicenseManager;
import org.integratedmodelling.common.auth.UserAuthorizationProvider;
import org.integratedmodelling.common.beans.requests.AuthorizationRequest;
import org.integratedmodelling.common.beans.responses.AuthorizationResponse;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.utils.FileUtils;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Simply replicates the authorization endpoints, sending to whatever authorization
 * provider this server uses. For now this allows us to decouple the collaboration server
 * from the certificates; later we can have the collaboration server be a kLAB node and
 * provide direct authentication.
 * 
 * If we have an authorized/ directory in the data dir, the certificates in it will be
 * authorized and a self-generated token will be returned if primary authentication fails.
 * The token may not be good for communication with remote engines. This is provided to
 * enable self-contained operation when offline.
 * 
 * @author ferdinando.villa
 *
 */
@Component
public class AuthorizationController {

    @Autowired
    UserAuthorizationProvider authorizationProvider;

    List<String> localCertificates;

    /**
     * Authorize using username/password. Delegate to authorization provider for the
     * actual authentication.
     * 
     * @param request
     * @return the response bean
     * @throws KlabAuthorizationException
     */
    public AuthorizationResponse authorizeFromUsername(AuthorizationRequest request)
            throws KlabAuthorizationException {
        return authorizationProvider.authenticateUser(request.getUsername(), request.getPassword());
    }

    /**
     * Authorize through cert file delegating to authorization provider. If authentication
     * fails, check out any certificates in <data>/authorization subdir and if we have a
     * match, make a user anyway and assign an internally valid token.
     * 
     * @param certificate
     * @return a response bean.
     * @throws KlabAuthorizationException
     */
    public AuthorizationResponse authorizeFromCertificate(String certificate)
            throws KlabAuthorizationException {
        try {
            return authorizationProvider.authenticateUser(certificate);
        } catch (Throwable e) {
            for (String c : getLocalCertificates()) {
                if (certificate.equals(c)) {
                    KLAB.warn("regular authentication failed: attempting to use local authorization");
                    AuthorizationResponse ret = readLocalCertificate(c);
                    if (ret != null) {
                        KLAB.warn("local authentication succeeded for user " + ret.getUsername()
                                + ": network privileges are not guaranteed");
                        return ret;
                    }
                }
            }
            throw e;
        }
    }

    private AuthorizationResponse readLocalCertificate(String certificate)
            throws KlabAuthorizationException {
        File publicKey = new File(KLAB.CONFIG.getDataPath()
                + File.separator + "ssh" +
                File.separator + "pubring.gpg");
        if (publicKey.exists()) {
            try {
                Properties identity = LicenseManager.readCertificate(certificate, publicKey.toURI().toURL());
                return AuthorizationResponse.newFromCertificateContents(identity);
            } catch (Exception e) {
            }
        }
        throw new KlabAuthorizationException("unable to authorize user from locally provided certificate");
    }

    private Collection<String> getLocalCertificates() {
        if (localCertificates == null) {
            localCertificates = new ArrayList<>();
            File dir = new File(KLAB.CONFIG.getDataPath() + File.separator + "authorized");
            if (dir.exists() && dir.isDirectory()) {
                for (File f : dir.listFiles()) {
                    if (f.isFile() && f.toString().endsWith(".cert")) {
                        try {
                            String cf = FileUtils.readFileToString(f);
                            localCertificates.add(cf);
                        } catch (IOException e) {
                            // ignore it
                        }
                    }
                }
            }
        }

        return localCertificates;
    }

    /**
     * Check if node is authenticating against its own directory or is delegating.
     * 
     * @return Authentication.DIRECT if node has own user directory.
     */
    public Authentication isDirect() {
        return authorizationProvider instanceof IndirectAuthorizationProvider
                ? Authentication.INDIRECT : Authentication.DIRECT;
    }
}
