# --------------------------------------------------------------------------
# Prototype of access properties for k.LAB servers. This file defines
# access privileges and resources shared by this server. Nodes and engines
# can use this file to define who can access the server and do what with it.
#
# This file will be managed directly by the web admin interface of the modeler,
# but for the time being it must be configured manually. The data in it represent
# the default access privileges.
# 
# Essentially the file has two main purposes:
#   1. declare which assets (projects, components) are made available, and
# 	2. provide a simple syntax to blacklist or whitelist access to them for
#      groups, individual users and/or IP patterns. 
#
# All specifications have the same pattern and consist of a prefix identifying
# the asset, a suffix identifying the permission type, and a list of targets.
#
# Global and asset declarations are read first, then the permissions are 
# scanned. 
# --------------------------------------------------------------------------


# GLOBAL ACCESS
# Defines privileges for access to the server as a whole.

# Overall connection to the server. This can be 'all' (everyone can connect), 'local'
# (connection must come from local network), 'self' (connection can only come from
# localhost) or 'none' (nobody). Further specifications can be added to white/blacklist
# specific connections.

server.connect = all

# If needed, the suffixes .users, .groups and .ips can be used to introduce further
# limitations. These define a whitelist - users that would not be allowed but are. If
# a blacklist is desired, use the prefix .no. in front of the suffix. 
# Permissions will be subordinate to global access permissions - if joe.smith cannot connect,
# no use to set permission for him for components, knowledge or services.

# Example: nobody allowed except joe.smith, jane.doe and users in the IM group
# server.connect = none
# server.connect.users = joe.smith, jane.doe
# server.connect.groups = IM

# Example: everyone allowed except joe.smith, jane.doe and users in the IM group
# server.connect = all
# server.connect.no.users = joe.smith, jane.doe
# server.connect.no.groups = IM

# ADMINISTRATION
# node administration is allowed through the web interface only to the users listed 
# below. These are authenticated according to the authentication strategy in the node,
# usually through the central IM directory. Authentication is only on a user or group 
# base.

# server.admin.users = joe.smith
# server.admin.groups = ADMIN

# ASSET DECLARATIONS: in public k.LAB nodes, these specify which assets are shared and how. 
# Assets must be declared here in order to be shared. Stating permissions on assets without
# declaring them is an error that prevents the server from starting.
#
# Assets are introduced by:
#   projects.shared 	    projects whose contents (models, observations) are available to allowed users
#   projects.synchronized   projects that are synchronized at the user end with allowed users
#   components              components containing knowledge and binary code, synchronized with user's
#                           modeling engines so they can run them.
#
# The statements define the names for projects and components that are shared. These must be available
# to the engine - projects in the workspace/ directory, components in the component/ directory. No error
# is generated if these are not available.

# Examples:
#   projects.shared = org.aries, org.aries.eu, org.icimod.es
#   componentd = im.mca
#   projects.synchronized = im, im.aries
# 
# After declaration, these can be restricted or allowed as before:
#
#   org.aries.groups = ARIES, ISU
#   im = all
#   im.no.groups = EXXON
#   im.mca.groups = IM, ISU, ARIES

projects.synchronized = im.aries

# Some asset names are predefined and control overall functionalities when used with permissions:
#  observations.db.submit the database of observations in this server. Allowed users can add 
#                         observations to it.
#  observations.db.query  the database of observations in this server. Allowed users can search
#                         and retrieve observations to it.

observations.db.submit = self
observation.db.query = all
