namespace hydrology
	using im, earth, physical, geography, geology, chemistry, ecology, im.soil
	in domain im:Hydrology	exports IM.WATERSHED, IM.STREAM_OUTLET, IM.PIT_FILLED_GEOGRAPHICAL_ELEVATION,
			IM.INFILTRATED_WATER_VOLUME, IM.EVAPORATED_WATER_VOLUME, 
			IM.LOCALLY_EXCHANGED_WATER_VOLUME, IM.RUNOFF_WATER_VOLUME, IM.WATER_FLOW_DIRECTION, 
			IM.TOTAL_FLOW_CONTRIBUTING_AREA, IM.HYDROLOGIC_SOIL_GROUP;

abstract attribute WaterPerviousness is im:Perviousness
	has children
		Pervious,
		Impervious;

@experimental
//Was in the earth namespace, but that gave errors when it referred back to hydrology:Impervious
quality ImperviousCover
	is im:Proportion of Impervious Landsurface;

@experimental
//likely not the right place for this.
identity Sewage
	is physical:Liquid chemistry:Mixture;

/**
 * basicsWW. Because hydrology is concerned with water movement, what we call Water in
 * hydrology is not the water identity but a volume of water.
 * 
 * Naming rules: if something ends by -ing it's participating in a process. Avoid -ed.
 * 
 * TODO: adopt traits
 */
 volume WaterVolume 
 	"Various types of water amounts of interest in hydrology. Named so that they can not be confused with
	 the processes that generate them."
 	is chemistry:Water im:Volume
 	has children
	 	(SurfaceWaterVolume
	 		has children
	 			(@export("IM.RUNOFF_WATER_VOLUME") RunoffWaterVolume 
	 				"Water that moves horizontally across the landscape."
	 			),
	 			(@export ("IM.LOCALLY_EXCHANGED_WATER_VOLUME") 
	 				LocallyExchangedWaterVolume
	 				"Water that is either infiltrated or evaporated and does not move horizontally. 
					 Curve Number methods produce this quantity; more sophisticated biophysical models
					 observe the child concepts."
	 				has children
	 				 	(
	 				 		@export("IM.INFILTRATED_WATER_VOLUME")
	 				 		InfiltratedWaterVolume
	 				 	), 
	 				 	// ? use -ing form as we are interested to know the water that participates in the process of infiltration
 						(
 							@export("IM.EVAPORATED_WATER_VOLUME")
 							EvaporatedWaterVolume
 								has children
 								(EvapotranspiredWaterVolume
 									""
 									has children
 										(PotentialEvapotranspiredWaterVolume
 											"" 
 											inherits im:Potential
 										),
 										(ActualEvapotranspiredWaterVolume 
 											""
 											inherits im:Actual
 										)
 								)	
 						)
 					)  
	 	),
//	 	Groundwater,
 		FloodWaterVolume,
 		(abstract WeatherMediatedDepositionWaterVolume 
			has children 
//			(PrecipitationVolume
//				has children
//					RainfallVolume,
//					( SnowfallVolume),
//					HailVolume),
			SnowmeltVolume,
			FogInterceptionVolume);
 
/*
 * Hydrological elevation doesn't cause water to pool.
 */
@export("IM.PIT_FILLED_GEOGRAPHICAL_ELEVATION")
length Elevation
	"The elevation concept in hydrology is a specialized elevation that is distributed so that water is not 
	 allowed to pool. That is usually referred to as \"hydrologically corrected\" elevation."
	is geography:Elevation;
 		
/*
 * from here to the next marker, new concepts for revised ontology. Stuff to be fixed after the marker.
 */

/**
 * Processes observed in water-aware subjects such as watersheds.
 */
abstract process HydrologicalProcess
	has children
		(abstract WaterInput
		),
		(abstract WaterOutput
			has children 
				WaterUse,
				Evaporation,  // Why not Evapotranspiration?
				Infiltration,
				Runoff
		),
		(abstract WaterFlow
			has children
    			SurfaceWaterFlow,
				GroundwaterFlow,
				GroundwaterRecharge
		);

process WaterBalance
	has 
		at least 1 WaterInput, 
		at least 1 WaterOutput;

/**
 * Watersheds are regions where water may get in, out, and move around. If they do, a watershed
 * model should be able to use those processes appropriately.
 */
@export("IM.WATERSHED")
thing Watershed 
	"A region where surface water from rain and melting snow or ice converges to a single point at a lower elevation."
	is earth:Terrestrial earth:Region
	has WaterBalance,
		WaterFlow,
		earth:Stream;

process WatershedFormation 
	"Watershed formation is the set of processes that generates the features in a watershed, such
     as elevation, flow direction and accumulation, etc."
	is earth:LandformingProcess
	creates Watershed;

/*
 * hydrologically relevant events
 */
event DayWithPrecipitation is im:Day;

@export("IM.HYDROLOGIC_SOIL_GROUP")
abstract attribute HydrologicSoilGroup
	"A widely used USDA classification categorizing soils with respect to their drainage properties."
    has disjoint children
        (SoilGroupA
        	"Soil with low runoff potential when thoroughly wet."
        ),
        (SoilGroupB
        	"Soils with moderately low runoff potential when thoroughly wet."
        ),
        (SoilGroupC
        	"Soils with moderately high runoff potential when thoroughly wet."
        ),
        (SoilGroupD
        	"Soils with high runoff potential when thoroughly wet."
        );

/*
 * one of these is created by the watershed process per
 * watershed. Its qualities vs time can be used to calibrate the
 * process model.
 */
@export("IM.STREAM_OUTLET")
thing StreamOutlet; // TODO inheritance! Both the core subject and the relationship to the process.

/*
 * Stuff below is old and wrinkled - to be understood, fixed and floated to the noble part of the
 * ontology.
 */

/**
 * TODO this really needs metadata.
 */
//KB: Sure, metadata is at: http://directives.sc.egov.usda.gov/OpenNonWebContent.aspx?content=17757.wba

priority StreamOrder;

@deprecated("use inherent observers with im.geo:Proximity (is Distance)")
length SurfaceWaterProximity;

//thing Stream 
//	has children
//		(River with metadata { dc:comment "A stream of 6th order or higher"}) //That definition will definitely depend on what part of the world you're working in. There are some places where a much lower order stream would still be considered a river. I don't know that there's a way to define a river consistently - I think it's more a local/colloquial term, which isn't great for semantic purposes.
//	has
//		StreamOrder;

abstract thing HydrologicallyRelevantRegion is earth:Region
	has children
		 Floodplain,
		 (RechargeZone
			has children
				 MountainfrontRechargeZone,
				 InstreamRechargeZone);

/*
 * Used to classify patterns and things resulting from events of different magnitude, whose
 * likelihood increases with time span.
 */
attribute EventLikelihood
	has children
		Likelihood100Years, //In the context of flooding, could use any number of other return frequencies (which may be a better term). e.g., annual, ten-year, twenty-year, 50-year.
		Likelihood500Years;

@export("IM.WATER_FLOW_DIRECTION")
quality FlowDirection;

@export("IM.TOTAL_FLOW_CONTRIBUTING_AREA")
area ContributingArea is im:Area;

@issue(who="ferdinando.villa", what="What is this?")
@issue(who="ferdinando.villa", what="KB: quite simple, this is the frequency of surface flow. A perennial stream flows 100% of the time, an ephemeral stream flows only in response to precipitation or snowmelt events, an intermittent stream is somewhere intermediate to those two")
thing SurfaceFlowFrequency
	has children
		PerennialFlow,
		IntermittentFlow,
		EphemeralFlow;


length DepthToWaterTable;

@experimental
volume WaterYield; //FV check: created to annotate aries.data.hydrology - ARIES:q_project 

quality WaterQuality
	has children
		(WaterQualityUSA //These are water quality standards for the U.S. Leaving open the possibility 
			has children         // for different names for standards elsewhere in the world.
		    	MeetsStandards,
    			OfConcern,
    			RequiresTMDL),
    	WaterQualityElsewhere;
   
@origin("SWEET")	 
@todo("Document")
velocity Seepage
	"";
	
@todo("Align & check nature")
@origin("SWEET")
volume Storativity 
	"The volume of water an aquifer releases from or takes into storage per 
	 unit surface area of the aquifer per unit change in head (virtually 
     equal to the specific yield in an unconfined aquifer).";

@todo("complete, align")      
process WaterTransport 
	"Any transport of water that happens within the earth:Crust realm of a 
	 earth:Region. This excludes exchanges with the atmosphere (dealt with in
	 earth) such as Precipitation."
	is earth:Crust chemistry:Water physical:MatterTransport within earth:Region
	has children
		(AquiferRecharge
			""
		),
		(Percolation
			""
		),
		(Infiltration
			""
		);

process SurfaceWaterFlow
	""
	is WaterTransport within earth:Terrestrial earth:Region
	contains
		earth:Precipitation, AquiferRecharge, Percolation, 
		Infiltration, ecology:Evapotranspiration;
		
quality AvailableWaterCapacity is im:Proportion of im.soil:SoilStratum chemistry:Water im:Capacity;

@experimental
("Clearly there are better ways to organize this")
quality WaterPermanence
	has children
		Permanent,
		Seasonal,
		Tidal,
		WaterloggedSoil;