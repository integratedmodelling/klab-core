namespace geography
	using im, earth, physical
	in domain im:Geography
	exports IM.GEOGRAPHICAL_ELEVATION, IM.GEOGRAPHICAL_SLOPE,
			IM.GEOGRAPHICAL_SLOPE_LENGTH, IM.GEOGRAPHICAL_ASPECT;

@export("IM.GEOGRAPHICAL_ELEVATION")
length Elevation
	"Geographical elevation above sea level, as described by a digital
	 elevation model."
	is im:Height of earth:Terrain within earth:Terrestrial earth:Region
	has children 
		(LidarElevation 
			"Elevation at edge of any reflective surface including canopy and buildings"
		 );
		 
length BathymetricDepth
	"Depth of earth's crust below sea level, as described by a digital bathymetry model."
	is im:Height of earth:Bathymetry within earth:Aquatic earth:Region
	has children 
		(LidarElevation 
			"Elevation at edge of any reflective surface including canopy and buildings"
		 );
	
@export("IM.GEOGRAPHICAL_SLOPE")
angle Slope
	"Inclination of the above-water terrain in a geographical region."
	is im:Angle of earth:Terrain within earth:Terrestrial earth:Region;

angle StreamGradient
	is im:Angle of earth:Terrain within earth:Stream;

angle BathymetricSlope
	"Inclination of the below-water terrain in a geographical region."
	is im:Angle of earth:Bathymetry within earth:Aquatic earth:Region;
	
@export("IM.GEOGRAPHICAL_SLOPE_LENGTH")
length SlopeLength 
	""
	// is im:Length
;	
	
@export("IM.GEOGRAPHICAL_ASPECT")
angle Aspect 
	"The compass direction that a slope faces. For example, a slope on the
	 eastern edge of the Rockies toward the Great Plains is described as having 
	an easterly aspect."
	is im:Angle of im:MagneticPlanetaryOrientation within earth:Terrestrial earth:Region;

	
/**
 * Political/Regional geography. Knowledge below was in policy which is obviously
 * inappropriate. Not sure it should have its own namespace.
 */	
 
 abstract identity GeographicalIdentity
 	"Any abstract identity we can use to divide the world up into disjoint entities."
 	has children
 		(	
 			@todo("this should be deniable - the parser doesn't allow it here.")
 			abstract PoliticalIdentity
 			""
 		),
 		(abstract RegionalIdentity
 			""
 		);
 
/*
 * TODO add continents and major areas
 */ 
 
@todo("Some kind soul please start adding ISO country codes as metadata")
abstract identity Country is PoliticalIdentity 
			has disjoint children
				Afghanistan,                 
				Albania,                     
				Antarctica,                  
                Algeria,                     
                AmericanSamoa,               
                Andorra,                     
                Angola,                      
                AntiguaAndBarbuda,           
                Azerbaijan,                  
                Argentina,                   
                Australia,                   
                Austria,                     
                Bahamas,                     
                Bahrain,                     
                Bangladesh ,                 
                Armenia ,                    
                Armenia ,                    
                Barbados,                    
                Belgium,                    
                Bermuda,                     
                Bhutan ,                     
                Bolivia,                     
                BosniaAndHerzegovina ,       
                Botswana  ,                  
                BouvetIsland ,               
                Brazil  ,                    
                Belize,                      
                BritishIndianOceanTerritory ,
                SolomonIslands   ,           
                BritishVirginIslands ,       
                Brunei   ,                   
                Bulgaria ,                   
                Myanmar   ,                  
                Burundi  ,                   
                Belarus   ,                  
                Cambodia ,                  
                Cameroon  ,                  
                Canada    ,                  
                CapeVerde  ,                 
                CaymanIslands ,              
                CentralAfricanRepublic      ,
                SriLanka         ,           
                Chad  ,                      
                Chile ,                      
                China    ,                   
                Taiwan  ,                    
                ChristmasIsland   ,          
                CocosIslands    ,            
                Colombia  ,                  
                Comoros  ,                   
                Mayotte ,                    
                Congo  ,                     
                CongoDemocraticRepublic     ,
                CookIslands        ,         
                CostaRica  ,                 
                Croatia  ,                   
                Cuba    ,                    
                Cyprus  ,                    
                Cyprus  ,                    
                CzechRepublic,               
                Benin          ,             
                Denmark       ,              
                Dominica     ,               
                DominicanRepublic ,          
                Ecuador              ,       
                ElSalvador         ,         
                EquatorialGuinea   ,         
                Ethiopia          ,          
                Eritrea           ,          
                Estonia          ,           
                FaroeIslands      ,          
                FalklandIsland     ,         
                SouthGeorgiaAndSouthSandwichIslands,
                Fiji                        ,
                Finland                     ,
                AlandIslands                ,
                France                      ,
                FrenchGuiana                ,
                FrenchPolynesia             ,
                FrenchSouthernTerritories   ,
                Djibouti                    ,
                Gabon                       ,
                Georgia                     ,
                Gambia                      ,
                PalestinianTerritory        ,
                Germany                     ,
                Ghana                       ,
                Gibraltar                   ,
                Kiribati                    ,
                Greece                      ,
                Greenland                   ,
                Grenada                     ,
                Guadeloupe                  ,
                Guam                        ,
                Guatemala                   ,
                Guinea                      ,
                Guyana                      ,
                Haiti                       ,
                HeardIslandAndMcDonaldIslands,
                Vatican                     ,
                Honduras                    ,
                HongKong                    ,
                Hungary                     ,
                Iceland                     ,
                India                       ,
                Indonesia                   ,
                Iran                        ,
                Iraq                        ,
                Ireland                     ,
                Israel                      ,
                Italy                       ,
                CoteDIvoire                 ,
                Jamaica                     ,
                Japan                       ,
                Kazakhstan                  ,
                Jordan                      ,
                Kenya                       ,
                NorthKorea                  ,
                SouthKorea                  ,
                Kuwait                      ,
                Kyrgyzstan                  ,
                Lao                         ,
                Lebanon                     ,
                Lesotho                     ,
                Latvia                      ,
                Liberia                     ,
                Libya                       ,
                Liechtenstein               ,
                Lithuania                   ,
                Luxembourg                  ,
                Macao                       ,
                Madagascar                  ,
                Malawi                      ,
                Malaysia                    ,
                Maldives                    ,
                Mali                        ,
                Malta                       ,
                Martinique                  ,
                Mauritania                  ,
                Mauritius                   ,
                Mexico                      ,
                Monaco                      ,
                Mongolia                    ,
                Moldova                     ,
                Montenegro                  ,
                Montserrat                  ,
                Morocco                     ,
                Mozambique                  ,
                Oman                        ,
                Namibia                     ,
                Nauru                       ,
                Nepal                       ,
                Netherlands                 ,
                NetherlandsAntilles,
                Curacao                    ,
                Aruba                       ,
                SintMaarten                 ,
                Bonaire                     ,
                NewCaledonia                ,
                Vanuatu                     ,
                NewZealand                  ,
                Nicaragua                   ,
                Niger                       ,
                Nigeria                     ,
                Niue                        ,
                NorfolkIsland               ,
                Norway                      ,
                NorthernMarianaIslands      ,
                UnitedStatesMinorOutlyingIslands,
                Micronesia                  ,
                MarshallIslands             ,
                Palau                       ,
                Pakistan                    ,
                Panama                      ,
                PapuaNewGuinea              ,
                Paraguay                    ,
                Peru                        ,
                Philippines                 ,
                PitcairnIslands             ,
                Poland                      ,
                Portugal                    ,
                GuineaBissau                ,
                TimorLeste                  ,
                PuertoRico                  ,
                Qatar                       ,
                Reunion                     ,
                Romania                     ,
                Russia                      ,
                Rwanda                      ,
                SaintBarthelemy             ,
                SaintHelena                 ,
                SaintKittsAndNevis          ,
                Anguilla                    ,
                SantaLucia                  ,
                SaintMartin                 ,
                SaintPierreAndMiquelon      ,
                SaintVincentAndGrenadines   ,
                SanMarino                   ,
                SaoTomeAndPrincipe          ,
                SaudiArabia                 ,
                Senegal                     ,
                Serbia                      ,
                Seychelles                  ,
                SierraLeone                 ,
                Singapore                   ,
                Slovakia                    ,
                Vietnam                     ,
                Slovenia                    ,
                Somalia                     ,
                SouthAfrica                 ,
                Zimbabwe                    ,
                Spain                       ,
                SouthSudan                  ,
                WesternSahara               ,
                Sudan                       ,
                Suriname                    ,
                SvalbardJanMayenIslands     ,
                Swaziland                   ,
                Sweden                      ,
                Switzerland                 ,
                Syria                       ,
                Tajikistan                  ,
                Thailand                    ,
                Togo                        ,
                Tokelau                     ,
                Tonga                       ,
                TrinidadAndTobago           ,
                UnitedArabEmirates          ,
                Tunisia                     ,
                Turkey                      ,
                Turkey                      ,
                Turkmenistan                ,
                TurksAndCaicosIslands       ,
                Tuvalu                      ,
                Uganda                      ,
                Ukraine                     ,
                Macedonia                   ,
                Egypt                       ,
                UnitedKingdom               ,
                Guernsey                    ,
                Jersey                      ,
                IsleOfMan                   ,
                Tanzania                    ,
                UnitedStates                ,
                UnitedStatesVirginIslands   ,
                BurkinaFaso                 ,
                Uruguay                     ,
                Uzbekistan                  ,
                Venezuela                   ,
                WallisAndFutuna             ,
                Samoa                       ,
                Yemen                       ,
                Zambia                     
;