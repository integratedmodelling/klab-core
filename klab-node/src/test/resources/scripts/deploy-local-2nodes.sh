#!/bin/sh
# ---------------------------------------------------------------------------------------------------------
# Start two nodes and one modeling engine on the local machine, set up so that one of the nodes works as
# primary node for a test user.
#
# Use from a bash shell after changing to the main thinklab directory (where the root pom.xml is):
#
# > sh knode/src/test/resources/scripts/deploy-local-2nodes-1engine.sh
# 
# If everything works, all systems are go when the console window comes up and the modeling engine is
# automatically connected to.
# ---------------------------------------------------------------------------------------------------------

VERSION="0.9.9"

# ensure everything is up to date. The repackaged jars don't get cleaned up unless we run mvn clean, which 
# fails if Eclipse is active and is doing anything, so we just delete them.

rm -f knode/target/knode.jar
mvn -o install package

# start up two nodes unless they're active

# aldo is first as it provides resources that pino checks for at initialization
if [ `curl -Is http://127.0.0.1:8187/knode/status | head -1 | wc -l` == 1 ] ; then
	echo "aldo is already active"
else 	
	java -Xdebug -Xbootclasspath/p:lib/jsr166.jar -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8887 -Dtest.assets=aldo -Dklab.data.directory=${HOME}/opt/klab/aldo -jar klab-node/target/knode.jar --server.port=8187  &
	echo $! > ${HOME}/.aldod
	# sleep long enough to hopefully have the resources available
	sleep 1m
fi	

if [ `curl -Is http://127.0.0.1:8186/knode/status | head -1 | wc -l` == 1 ] ; then
	echo "pino is already active"
else 	
	java -Xdebug -Xbootclasspath/p:lib/jsr166.jar -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8886 -Dtest.assets=pino -Dklab.data.directory=${HOME}/opt/klab/pino -jar klab-node/target/knode.jar --server.port=8186  &
	echo $! > ${HOME}/.pinod
fi

read -p "Press [Enter] to stop servers... "

# when client exits, kill all servers
kill -9 `cat ${HOME}/.pinod`
kill -9 `cat ${HOME}/.aldod`
rm -f ${HOME}/.pinod ${HOME}/.aldod
