#!/bin/sh
# ---------------------------------------------------------------------------------------------------------
# Start two nodes and one modeling engine on the local machine, set up so that one of the nodes works as
# primary node for a test user.
#
# Use from a bash shell after changing to the main thinklab directory (where the root pom.xml is):
#
# > sh knode/src/test/resources/scripts/deploy-local-2nodes-1engine.sh
# 
# If everything works, all systems are go when the console window comes up and the modeling engine is
# automatically connected to.
# ---------------------------------------------------------------------------------------------------------

VERSION="0.9.9"

function usage()
{
    echo "launch two nodes (named pino and aldo) and one modeling engine, then a console client"
    echo ""
    echo "\t-d --debug"
    echo ""
}

DEBUG="false"

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
#    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        -d | --debug)
            DEBUG="true"
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

# ensure everything is up to date. The repackaged jars don't get cleaned up unless we run mvn clean, which 
# fails if Eclipse is active and is doing anything, so we just delete them.


rm -f klab-modeler/target/kmodeler.jar
rm -f klab-node/target/knode.jar
rm -f common/target/kclient.jar
mvn install package
cd common
mvn compile assembly:single
mv target/klab-common-${VERSION}-jar-with-dependencies.jar target/kclient.jar
cd ..

# start up two nodes unless they're active
if [ `curl -Is http://127.0.0.1:8186/knode/status | head -1 | wc -l` == 1 ] ; then
	echo "pino is already active"
else 	
	java -Xdebug -Xbootclasspath/p:lib/jsr166.jar -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8886 -Dtest.assets=pino -Dklab.data.directory=${HOME}/opt/klab/pino -jar knode/target/knode.jar --server.port=8186  &
	echo $! > ${HOME}/.pinod
fi

# start up two nodes unless they're active
if [ `curl -Is http://127.0.0.1:8187/knode/status | head -1 | wc -l` == 1 ] ; then
	echo "aldo is already active"
else 	
	java -Xdebug -Xbootclasspath/p:lib/jsr166.jar -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8887 -Dtest.assets=aldo -Dklab.data.directory=${HOME}/opt/klab/aldo -jar knode/target/knode.jar --server.port=8187  &
	echo $! > ${HOME}/.aldod
fi	

# give servers some time to come up
echo sleeping one minute for the servers to start up...
sleep 1m

# start a modeling engine with a certificate that will authenticate test.user with the local pino as primary node
if [ `curl -Is http://127.0.0.1:8184/knode/status | head -1 | wc -l` == 1 ] ; then
	echo "peppe is already active"
else 	
	java  -Xdebug -Xbootclasspath/p:lib/jsr166.jar -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8897  -Dtest.assets=peppe -Dklab.data.directory=${HOME}/opt/klab/peppe -jar kmodeler/target/kmodeler.jar --server-port=8184 &
	echo $! > ${HOME}/.pepped
fi

# sleep one more minute so the certificate is available for the client
sleep 1m

# start a client with the same test.user certfile that should find the kmodeler running on port 8184 and connect to pino
# for indirect authentication
java -Dklab.certificate=${HOME}/opt/klab/peppe/im.cert -jar common/target/kclient.jar

# when client exits, kill all servers
kill -9 `cat ${HOME}/.pinod`
kill -9 `cat ${HOME}/.aldod`
kill -9 `cat ${HOME}/.pepped`
rm -f ${HOME}/.pinod ${HOME}/.aldod ${HOME}/.pepped