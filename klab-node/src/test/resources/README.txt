Testing for nodes relies on a property 'test.assets = <name>' in the node certificate. If that property is present, the 
node server will extract all assets under the named subdirectory of test.assets as the working directory of each server.

The scripts and certificates in test/resources can be used to launch prebuilt test configurations 
on the local machine. Launch them from the directory where the root pom.xml is located, e.g.:

cd ~/git/thinklab
mvn install package
sh knode/src/test/resources/scripts/deploy-local-2nodes-1engine.sh

NOTE: no Windows scripts are provided; under Windows, use mingw or equivalent shell.

In test.assets, configurations for three engines are available:

  pino  main node with direct authentication, running on port 8186, serving project im
  aldo  secondary node with indirect authentication (defers to pino) running on 8187
  peppe engine node for user "peppe", using pino as primary node, running on 8184

When a server is in test mode (i.e., has the test.assets property) it will only accept connections from the local
network.