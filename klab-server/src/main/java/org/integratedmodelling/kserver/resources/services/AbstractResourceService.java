package org.integratedmodelling.kserver.resources.services;

import org.integratedmodelling.common.interfaces.ResourceService;
import org.springframework.context.ApplicationContext;

public abstract class AbstractResourceService implements ResourceService {

    protected ApplicationContext applicationContext;

    /**
     * Trick to add the application context to components we can't define as such.
     * 
     * @param appContext
     */
    public void setApplicationContext(ApplicationContext appContext) {
        this.applicationContext = appContext;
    }

}
