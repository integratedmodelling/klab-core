package org.integratedmodelling.kserver.resources.services;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.beans.responses.FileResource;
import org.integratedmodelling.common.interfaces.DirectResourceService;
import org.integratedmodelling.common.utils.StringUtils;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabResourceNotFoundException;
import org.integratedmodelling.kserver.controller.GetResourceController;

/**
 * @author ferdinando.villa This enables the /get/file/<urn> endpoints. Any file that has
 *         been added to the publishedResources map will be accessible as long as the URN
 *         is authorized.
 */
@ResourceService(service = "file")
public class FileService extends AbstractResourceService implements DirectResourceService {

    /**
     * Pre-filled at initialization with things that the engine makes available no matter
     * what, such as the knowledge map.
     */
    public static Map<String, FileResource> publishedResources = new HashMap<>();

    @Override
    public Object resolveUrn(String urn, String element, ConnectionAuthorization authorization, IResourceConfiguration configuration, Map<String, String[]> requestParameters)
            throws KlabException {

        if (publishedResources.containsKey(urn)) {
            FileResource res = publishedResources.get(urn);
            return resolveResource(res, authorization);
        }

        Object inp = getFileFromUrn(urn, authorization);
        if (inp != null) {
            return inp;
        }

        return null;
    }

    private Object resolveResource(FileResource res, ConnectionAuthorization authorization)
            throws KlabResourceNotFoundException {
        if (res.isClasspath()) {
            try {
                return applicationContext.getResource("classpath:" + res.getPath()).getURL().openStream();
            } catch (Throwable e) {
                throw new KlabResourceNotFoundException("cannot find resource " + res.getPath()
                        + " on classpath");
            }
        }
        return getFileFromUrn(res.getUrn(), authorization);
    }

    private File getFileFromUrn(String urn, ConnectionAuthorization authorization) {

        String filePrefix = urn.startsWith("klab:") ? "klab:file:"
                : GetResourceController.getUrnPrefix() + "file:";

        if (urn.startsWith(filePrefix)) {
            String fn = urn.substring(filePrefix.length());
            fn = StringUtils.replace(fn, "..", File.separator);
            return new File(fn);
        }
        return null;
    }

    @Override
    public boolean allowsUnauthenticated(String urn) {
        // TODO Auto-generated method stub
        return true;
    }

}
