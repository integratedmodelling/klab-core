package org.integratedmodelling.kserver.resources.services;

import java.util.Map;

import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.interfaces.DirectResourceService;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Looks up an observation based on the URN, checks permissions against namespace and
 * project, and returns the observation bean that allows the caller to reconstruct it.
 * 
 * @author Ferd
 *
 */
public class ObservationService implements DirectResourceService {

    @Override
    public Object resolveUrn(String urn, String element, ConnectionAuthorization authorization, IResourceConfiguration configuration, Map<String, String[]> requestParameters)
            throws KlabException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean allowsUnauthenticated(String urn) {
        // TODO Auto-generated method stub
        return true;
    }

}
