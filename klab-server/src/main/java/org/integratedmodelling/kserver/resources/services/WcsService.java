package org.integratedmodelling.kserver.resources.services;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.data.IDataAsset;
import org.integratedmodelling.api.data.IDataService;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.interfaces.IndirectResourceService;
import org.integratedmodelling.common.interfaces.RequestParameterFilter;
import org.integratedmodelling.exceptions.KlabException;

/**
 * Finds a record associated with a URN, checks permissions, and if everything is OK
 * rewrites a URL to access a WCS service with the same parameters we got.
 * 
 * @author Ferd
 *
 */
@ResourceService(service = "wcs")
public class WcsService implements IndirectResourceService, RequestParameterFilter {

    @Override
    public Object resolveUrn(IDataAsset assets, IDataService service, ConnectionAuthorization authorization, IResourceConfiguration configuration, Map<String, String[]> requestParameters)
            throws KlabException {
        Set<String> request = new HashSet<>(Arrays.asList(requestParameters.containsKey("request")
                ? requestParameters.get("request") : new String[] {}));
        if (request.contains("GetCoverage")) {
            // TODO require authentication only for access to data; leave access to
            // catalog free
        }
        return assets.getResourceUrl("wcs");
    }

    @Override
    public String filter(String key, String value, IDataAsset resource) {
        if (key.equals("coverageId") || key.equals("coverage")) {
            return resource.getResourceNamespace() + ":" + resource.getResourceId();
        }
        return value;
    }

    @Override
    public String filterHeader(String key, String value, IDataAsset resource) {
        return value;
    }

    @Override
    public boolean allowsUnauthenticated(String urn) {
        // TODO Auto-generated method stub
        return false;
    }

}
