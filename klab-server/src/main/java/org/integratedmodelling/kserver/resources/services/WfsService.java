package org.integratedmodelling.kserver.resources.services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.data.IDataAsset;
import org.integratedmodelling.api.data.IDataService;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.IndirectResourceService;
import org.integratedmodelling.common.interfaces.RequestParameterFilter;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabValidationException;

/**
 * Finds a record associated with a URN, checks permissions, and if everything is OK
 * rewrites a URL to access a WCS service with the same parameters we got.
 * 
 * @author Ferd
 *
 */
@ResourceService(service = "wfs")
public class WfsService implements IndirectResourceService, RequestParameterFilter {

    private static String spoof(String url) {
        if (KLAB.CONFIG.getProperties().getProperty(IConfiguration.KLAB_SPOOF_DEV_URL, "false").equals("true")) {
            if (url.endsWith(KLAB.CONFIG.getDeveloperNetworkURLPostfix())) {
                url = url.substring(0, url.length() - KLAB.CONFIG.getDeveloperNetworkURLPostfix().length());
            }
        }
        return url;
    }
    
    @Override
    public Object resolveUrn(IDataAsset assets, IDataService service, ConnectionAuthorization authorization, IResourceConfiguration configuration, Map<String, String[]> requestParameters)
            throws KlabException {

        if (service != null) {
            try {
                return new URL(spoof(service.getUrl()) + "/wfs");
            } catch (MalformedURLException e) {
                throw new KlabValidationException(e);
            }
        }

        return assets.getResourceUrl("wfs");
    }

    // AUTH SNIPPET to convert WFS header into our auth data.
    // final String authorization = httpRequest.getHeader("Authorization");
    // if (authorization != null && authorization.startsWith("Basic")) {
    // // Authorization: Basic base64credentials
    // String base64Credentials = authorization.substring("Basic".length()).trim();
    // String credentials = new String(Base64.getDecoder().decode(base64Credentials),
    // Charset.forName("UTF-8"));
    // // credentials = username:password
    // final String[] values = credentials.split(":",2);
    @Override
    public String filter(String key, String value, IDataAsset resource) {
        if (key.equals("typeNames")) {
            /* return resource.getResourceNamespace() + ":" + */ resource.getResourceId();
        }
        return value;
    }

    @Override
    public String filterHeader(String key, String value, IDataAsset resource) {
        if (key.equals("authorization")) {
            /*
             * TODO either this or substitute with appropriate server authorization from 
             * configuration
             */
            return null;
        }
        return value;
    }

    @Override
    public boolean allowsUnauthenticated(String urn) {
        // TODO Auto-generated method stub
        // TODO becomes false when operational
        return true;
    }

}
