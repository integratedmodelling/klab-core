package org.integratedmodelling.kserver.resources.services;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.modelling.IModelBean;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.interfaces.DirectResourceService;
import org.integratedmodelling.exceptions.KlabException;

/**
 * @author ferdinando.villa
 * 
 *         This enables the /get/directory/<urn> endpoints. Any directory that has been
 *         added to publishedDirectories or predefinedDirectory maps will be accessible as
 *         long as the URN is authorized.
 */
@ResourceService(service = "directory")
public class DirectoryService implements DirectResourceService {

    /**
     * Pre-filled at initialization with things that the engine makes available no matter
     * what, such as the knowledge map.
     * 
     * Otherwise used during project and component retrieval, for which we check session
     * authorization before serving anything.
     */
    public static Map<String, IModelBean> predefinedDirectories = new HashMap<>();

    /**
     * 
     */
    public static Map<String, File> publishedDirectories = new HashMap<>();

    @Override
    public Object resolveUrn(String urn, String element, ConnectionAuthorization authorization, IResourceConfiguration configuration, Map<String, String[]> requestParameters)
            throws KlabException {

        if (element != null && publishedDirectories.containsKey(urn)) {
            return new File(publishedDirectories.get(urn) + File.separator + element);
        }
        IModelBean ret = predefinedDirectories.get(urn);
        if (ret != null) {
            return ret;
        }

        return null;
    }

    @Override
    public boolean allowsUnauthenticated(String urn) {
        // TODO Auto-generated method stub
        return true;
    }

}
