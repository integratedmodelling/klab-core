package org.integratedmodelling.kserver.resources.services;

import java.io.File;
import java.util.Map;

import org.integratedmodelling.api.annotations.ResourceService;
import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.beans.Project;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.interfaces.DirectResourceService;
import org.integratedmodelling.common.resources.ResourceFactory;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabResourceNotFoundException;

/**
 * @author ferdinando.villa
 *
 */
@ResourceService(service = "project")
public class ProjectService implements DirectResourceService {

    @Override
    public Object resolveUrn(String urn, String element, ConnectionAuthorization authorization, IResourceConfiguration configuration, Map<String, String[]> requestParameters)
            throws KlabException {
    	
        String prefix = "project:";
        if (urn.startsWith(prefix)) {
            String projectId = urn.substring(prefix.length());
            IProject project = KLAB.PMANAGER.getProject(projectId);
            if (project == null) {
                throw new KlabResourceNotFoundException("project identified by URN " + urn
                        + " was not loaded on this server");
            }
            if (!configuration.isAuthorized(project, authorization.getUsername(), authorization
                    .getUserGroups(), authorization.getRequestingIP())) {
                throw new KlabAuthorizationException("project identified by URN " + urn
                        + " is not authorized for user " + authorization.getUsername());
            }

            if (element != null) {
                return new File(project.getLoadPath() + File.separator + element);
            }

            return ResourceFactory.populateDirectory(KLAB.ENGINE.getName() + ":" + urn, project
                    .getLoadPath(), new Project(project));
        }
        return null;
    }

    @Override
    public boolean allowsUnauthenticated(String urn) {
        return false;
    }

}
