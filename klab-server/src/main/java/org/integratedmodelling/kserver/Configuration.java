/*******************************************************************************
 *  Copyright (C) 2007, 2015:
 *  
 *    - Ferdinando Villa <ferdinando.villa@bc3research.org>
 *    - integratedmodelling.org
 *    - any other authors listed in @author annotations
 *
 *    All rights reserved. This file is part of the k.LAB software suite,
 *    meant to enable modular, collaborative, integrated 
 *    development of interoperable data and model components. For
 *    details, see http://integratedmodelling.org.
 *    
 *    This program is free software; you can redistribute it and/or
 *    modify it under the terms of the Affero General Public License 
 *    Version 3 or any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but without any warranty; without even the implied warranty of
 *    merchantability or fitness for a particular purpose.  See the
 *    Affero General Public License for more details.
 *  
 *     You should have received a copy of the Affero General Public License
 *     along with this program; if not, write to the Free Software
 *     Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 *     The license is also available at: https://www.gnu.org/licenses/agpl.html
 *******************************************************************************/
package org.integratedmodelling.kserver;

import java.io.File;

import org.integratedmodelling.api.configuration.IConfiguration;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.configuration.KLAB.BootMode;

public class Configuration {

    public static String SERVICE_PORT_SYSTEM_PROPERTY = "klab.port";
    public static String TEST_ASSETS_PROPERTY         = "test.assets";

    private static final String DEFAULT_APPLICATION_PATH = "/opt/klab";
    private static String       applicationPath;

    public static String getApplicationPath(BootMode mode) {

        if (applicationPath == null) {
            applicationPath = System.getProperty(IConfiguration.KLAB_DATA_DIRECTORY);
            if (applicationPath /* still */ == null) {
                applicationPath = DEFAULT_APPLICATION_PATH + File.separator + mode.name().toLowerCase();
            }
        }

        File appath = new File(applicationPath);

        if (!appath.exists()) {
            try {
                appath.mkdirs();
            } catch (Throwable e) {
                KLAB.warn("could not create chosen application path " + appath);
            }
        }

        if (appath.exists() && !appath.canWrite()) {
            KLAB.warn("chosen application path " + appath + " is not writable");
            appath = null;
        }

        if (appath == null || !appath.exists()) {

            /*
             * force to $HOME/.k[modeler|knode]
             */
            appath = new File(System.getProperty("user.home") + File.separator + "."
                    + mode.name().toLowerCase());
            appath.mkdirs();
            applicationPath = appath.toString();

            KLAB.warn("fallback application path set to " + appath);
        }

        return applicationPath;
    }

}
