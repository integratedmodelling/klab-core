package org.integratedmodelling.kserver.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.integratedmodelling.api.knowledge.IConcept;
import org.integratedmodelling.api.modelling.IExtent;
import org.integratedmodelling.api.modelling.IObservableSemantics;
import org.integratedmodelling.api.modelling.resolution.IResolutionScope;
import org.integratedmodelling.common.beans.Model;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.owl.Knowledge;
import org.integratedmodelling.common.vocabulary.NS;
import org.integratedmodelling.common.vocabulary.ObservableSemantics;
import org.integratedmodelling.common.vocabulary.ObservationMetadata;
import org.integratedmodelling.engine.geospace.Geospace;
import org.integratedmodelling.engine.modelling.kbox.ModelKbox;
import org.integratedmodelling.engine.modelling.kbox.ObservationKbox;
import org.integratedmodelling.engine.modelling.resolver.ResolutionScope;
import org.integratedmodelling.engine.modelling.runtime.Scale;
import org.integratedmodelling.exceptions.KlabException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Inspection endpoints. Not advertised in API.
 * 
 * @author Ferd
 *
 */
@RestController
public class IntrospectionController {

    @RequestMapping(value = "/inspect/projection", method = RequestMethod.GET)
    public Object inspectProjection(@RequestParam(value = "id") String id) {
        CoordinateReferenceSystem crs = Geospace.getCRSFromID(id);
        return crs == null ? "" : crs.toWKT();
    }

    @RequestMapping(value = "/inspect/models", method = RequestMethod.GET)
    public List<Model> inspectModels() throws KlabException {
        List<Model> ret = new ArrayList<>();
        for (Model md : ModelKbox.get().retrieveAll()) {
            ret.add(md);
        }
        return ret;
    }
    
    @RequestMapping(value = "/inspect/mbox", method = RequestMethod.GET)
    public List<Map<String, String>> inspectModelKbox() throws KlabException {
        return ModelKbox.get().getDatabase().dump("model");
    }

    @RequestMapping(value = "/inspect/query", method = RequestMethod.POST)
    public List<Object> handleFileUpload(
            @RequestParam("concept") String concept, 
            @RequestParam("context") String context, 
            @RequestParam("querytype") String querytype, 
            @RequestParam("bbox") String boundingbox,
            @RequestParam("daterange") String daterange,
            @RequestParam(value = "abstract", required = false) String generic,
            @RequestParam(value = "instantiator", required = false) String instantiator) throws KlabException {
        List<Object> ret = new ArrayList<>();

        /*
         * TODO allow scenarios and extents
         */
        
        IExtent[] extents = new IExtent[] {};
        
        IConcept c = Knowledge.parse(concept);
        IConcept ctx = null;
        if (context != null && !context.trim().isEmpty()) {
            ctx = Knowledge.parse(context);
        }
        
        if (querytype.equals("models")) {
            
            IConcept obs = NS.isDirect(c) ? KLAB.c(NS.DIRECT_OBSERVATION) : KLAB.c(NS.INDIRECT_OBSERVATION);
            IObservableSemantics observable = new ObservableSemantics(c, obs, "test");
            if (observable == null || observable.getType() == null) {
                return ret;
            }

            /*
             * use abstract observable workflow even if observable is concrete.
             */
            IResolutionScope scope = ResolutionScope.newInstance(observable, instantiator != null, extents);
            if (generic != null) {
                ((ResolutionScope)scope).setGeneric(true);
            }
            
            for (Model md : ModelKbox.get().queryModels(observable, scope)) {
                ret.add(md);
            }
            
        } else {
            
            /*
             * TODO must use context
             */
         
            for(ObservationMetadata res : ObservationKbox.get().queryLocal(Collections.singleton(c), new Scale(extents), new ArrayList<>())) {
                ret.add(res);
            }
        }

        return ret;
    }
}
