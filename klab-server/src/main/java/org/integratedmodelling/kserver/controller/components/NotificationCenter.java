// package org.integratedmodelling.kserver.controller.components;
//
// import javax.annotation.Nullable;
//
// import org.integratedmodelling.api.runtime.ISession;
// import org.integratedmodelling.api.runtime.ITask;
// import org.springframework.stereotype.Component;
//
/// **
// * Component holding all notifications being sent (through monitors), with
// * the API to retrieve, delete and dispatch them.
// *
// * @author ferdinando.villa
// *
// */
// @Component
// public class NotificationCenter extends org.integratedmodelling.common.monitoring.NotificationCenter {
//
// class Notification extends org.integratedmodelling.common.beans.Notification {
//
// }
//
// public void info(String body) {
//
// }
//
// public void info(String body, @Nullable String cls) {
//
// }
//
// public void info(ITask task, String body, @Nullable String cls) {
//
// }
//
// public void info(ISession session, String body, @Nullable String cls) {
//
// }
//
// public void warn(Object body) {
//
// }
//
// public void warn(ITask task, Object body) {
//
// }
//
// public void warn(ISession session, Object body) {
//
// }
//
// public void error(Object body) {
//
// }
//
// public void error(ITask task, Object body) {
//
// }
//
// public void error(ISession session, Object body) {
//
// }
//
// }
