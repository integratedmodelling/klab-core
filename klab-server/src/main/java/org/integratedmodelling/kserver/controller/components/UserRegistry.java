package org.integratedmodelling.kserver.controller.components;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.engine.IModelingEngine;
import org.integratedmodelling.common.auth.IUserRegistry;
import org.integratedmodelling.common.beans.requests.ConnectionAuthorization;
import org.integratedmodelling.common.configuration.KLAB;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

/**
 * Simple, in-memory registry for connected users and authorizations. Not related to the
 * security mechanism as it needs to keep authorization data also for users authenticated
 * by different nodes.
 * 
 * TODO all the maps must become persistent with expiration.
 * 
 * @author ferdinando.villa
 *
 */
@Component("uReg")
public class UserRegistry implements IUserRegistry {

    Map<String, IUser>                           users = new HashMap<>();
    // private BiMap<String, ConnectionAuthorization> authorizations = HashBiMap.create();

    private static DB                            db;
    private Map<String, IUser>                   authorizedUsers;
    private Map<String, ConnectionAuthorization> authorizedNodes;
    private Map<ConnectionAuthorization, String> nodeAuthorization;
    private Map<String, Long>                    timestamps;
    private static UserRegistry                  _this;
    // private Scheduler scheduler;

    static class CleanupJob implements Job {

        @Override
        public void execute(JobExecutionContext arg0) throws JobExecutionException {
            KLAB.info("cleaning up expired node and user tokens");
            if (_this != null) {
                _this.cleanup();
            }
        }

    }

    public void initialize() {

        if (db == null) {

            db = DBMaker
                    .newFileDB(new File(KLAB.CONFIG.getDataPath("authorization") + File.separator
                            + "authcache"))
                    .closeOnJvmShutdown()
                    .make();

            authorizedUsers = db.getTreeMap("authorizedUsers");
            authorizedNodes = db.getTreeMap("authorizedNodes");
            nodeAuthorization = db.getTreeMap("nodeAuthorization");
            timestamps = db.getTreeMap("timestamps");

            _this = this;

        // /*
        // * TODO cleanup on creation? Should schedule regular cycles every day or so.
        // */
        // Trigger trigger =
        // TriggerBuilder.newTrigger().withIdentity("authorizationCleanup", "auth")
        // .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(24).repeatForever())
        // .build();
        // try {
        // this.scheduler = new StdSchedulerFactory().getScheduler();
        // this.scheduler.start();
        // this.scheduler.scheduleJob(JobBuilder.newJob(CleanupJob.class).withIdentity("authReaper",
        // "auth")
        // .build(), trigger);
        // } catch (SchedulerException e) {
        // KLAB.error("could not start authorization cleanup scheduler");
        // }

        }

    }

    @Override
    public void register(IUser user) {

        if (db == null) {
            initialize();
        }

        boolean commitNeeded = false;
        
        if (user.getSecurityKey() == null) {
        	return;
        }
        
        // if (KLAB.ENGINE instanceof IModelingEngine) {
        // users.put(user.getSecurityKey(), user);
        // } else {
        if (timestamps.containsKey(user.getSecurityKey())) {
            IUser reg = authorizedUsers.get(user.getSecurityKey());
            if (reg.getUsername().equals(user.getUsername())) {
                timestamps.put(user.getSecurityKey(), System.currentTimeMillis());
                commitNeeded = true;
            } else {
                KLAB.warn("repeated login with different username: " + user.getUsername() + " was "
                        + reg.getUsername());
            }
        } else {
            authorizedUsers.put(user.getSecurityKey(), user);
            timestamps.put(user.getSecurityKey(), System.currentTimeMillis());
            commitNeeded = true;
        }
        // }

        if (commitNeeded) {
            db.commit();
        }
        
    }

    @Override
    public IUser getUser(List<String> list) {

        if (db == null) {
            initialize();
        }

        if (list == null) {
            return null;
        }

        if (KLAB.ENGINE instanceof IModelingEngine) {
            for (String h : list) {

                IUser user = authorizedUsers.get(h);
                if (user != null) {
                    return user;
                }
            }
        } else {
            // remove
            // user = users.get(h);
            // if (user != null) {
            // return user;
            // }
        }
        return null;
    }

    /**
     * Match a node token to a set of user privileges seen at API.CONNECT. If this returns
     * null, the request is coming from an expired or fraudolent connection.
     * 
     * This is the authentication strategy for nodes that have not authenticated the user
     * directly; based on credentials delivered during identify() based on trust from
     * certified authenticating nodes.
     * 
     * FIXME needs to make tokens persistent
     * 
     * @param token
     * @return a previous connection authorization with user credentials, or null.
     */
    @Override
    public ConnectionAuthorization getConnectionAuthorization(String token) {

        if (db == null) {
            initialize();
        }

        ConnectionAuthorization auth = authorizedNodes.get(token);
        if (auth != null) {
            return auth;
        }

        /*
         * TODO better expiration handling for connections
         */
        return null;

        // remove
        // return authorizations.get(token);
    }

    /**
     * Return a token to match to a set of user authorization, without authenticating a
     * user that was authenticated previously.
     * 
     * @return
     */
    @Override
    public String authorize(ConnectionAuthorization request) {

        if (db == null) {
            initialize();
        }

        String ret = nodeAuthorization.get(request);
        if (ret != null) {
            return ret;
        }

        ret = UUID.randomUUID().toString();
        authorizedNodes.put(ret, request);
        timestamps.put(ret, System.currentTimeMillis());

        db.commit();
        
        return ret;

        // // remove
        // if (authorizations.containsValue(request)) {
        // return authorizations.inverse().get(request);
        // }
        // String ret = UUID.randomUUID().toString();
        // authorizations.put(ret, request);
        // return ret;
    }

    /**
     * Run periodically to clear up expired tokens according to expiration dates set above
     * or through configuration.
     * 
     */
    void cleanup() {

    }

}
