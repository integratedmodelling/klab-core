package org.integratedmodelling.kserver.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.integratedmodelling.api.knowledge.IAuthority;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.common.beans.authority.AuthorityConcept;
import org.integratedmodelling.common.beans.authority.AuthorityQueryResponse;
import org.integratedmodelling.common.vocabulary.authority.AuthorityFactory;
import org.integratedmodelling.common.vocabulary.authority.BaseAuthority;
import org.integratedmodelling.exceptions.KlabException;
import org.integratedmodelling.exceptions.KlabIOException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Services to resolve and query authorities. These are public endpoints.
 * 
 * @author Ferd
 *
 */
@RestController
public class AuthorityController {
    
    @RequestMapping(value = API.QUERY_AUTHORITY, method = RequestMethod.GET)
    public AuthorityQueryResponse query(@PathVariable String authority, @PathVariable String query) throws KlabException {
        
        IAuthority<?> auth = AuthorityFactory.get().getAuthorityView(authority);
        if (auth != null && auth.canSearch()) {
            try {
                return (AuthorityQueryResponse) ((BaseAuthority)auth).search(URLDecoder.decode(query, "UTF-8"), authority);
            } catch (UnsupportedEncodingException e) {
                throw new KlabIOException(e);
            }
        }
        return null;
    }

    @RequestMapping(value = API.RESOLVE_AUTHORITY, method = RequestMethod.GET)
    public AuthorityConcept identify(@PathVariable String authority, @PathVariable String id) throws KlabException {

        IAuthority<?> auth = AuthorityFactory.get().getAuthorityView(authority);
        if (auth != null) {
            try {
                return ((BaseAuthority)auth).getConcept(authority, URLDecoder.decode(id, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                throw new KlabIOException(e);
            }
        }
        return null;
    }

}
