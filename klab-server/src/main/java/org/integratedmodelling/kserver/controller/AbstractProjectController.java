package org.integratedmodelling.kserver.controller;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.integratedmodelling.api.auth.IUser;
import org.integratedmodelling.api.configuration.IResourceConfiguration;
import org.integratedmodelling.api.network.API;
import org.integratedmodelling.api.project.IProject;
import org.integratedmodelling.common.auth.IUserRegistry;
import org.integratedmodelling.common.beans.Project;
import org.integratedmodelling.common.beans.requests.DeployRequest;
import org.integratedmodelling.common.beans.responses.Projects;
import org.integratedmodelling.common.configuration.KLAB;
import org.integratedmodelling.common.project.Workspace;
import org.integratedmodelling.engine.ModelingEngine;
import org.integratedmodelling.exceptions.KlabAuthorizationException;
import org.integratedmodelling.exceptions.KlabException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Base controller for common project capabilities.
 * 
 * @author ferdinando.villa
 *
 */
public class AbstractProjectController {

	@Autowired
	protected KServerController kserver;

	@Autowired
	IUserRegistry userRegistry;

	/**
	 * Reload projects, optionally forcing full reload. Only for admins or local
	 * clients.
	 * 
	 * @param force
	 * @param headers
	 * @param request
	 * @throws KlabException
	 */
	@RequestMapping(value = API.RELOAD, method = RequestMethod.PUT)
	public void reloadProjects(@RequestParam(value = "force", defaultValue = "false", required = false) boolean force,
			@RequestHeader HttpHeaders headers, HttpServletRequest request) throws KlabException {

		if (!(KServerController.isLocalIp(request) || KServerController.isAdmin(headers))) {
			throw new KlabAuthorizationException("no authorization for shutdown");
		}

		KLAB.PMANAGER.load(force, KLAB.MFACTORY.getRootParsingContext());
	}

	@RequestMapping(value = API.LIST_PROJECTS, method = RequestMethod.GET)
	public Projects listProjects(@RequestHeader HttpHeaders headers, HttpServletRequest request) {

		IResourceConfiguration rconf = KLAB.ENGINE.getResourceConfiguration();
		Projects ret = new Projects();
		IUser user = userRegistry.getUser(headers.get(API.AUTHENTICATION_HEADER));

		for (IProject p : KLAB.PMANAGER.getProjects()) {
			if (rconf.isAuthorized(p, user, request.getRemoteAddr())) {
				ret.getProjectUrns().add(GetResourceController.getUrnPrefix() + "project:" + p.getId());
			}
		}

		return ret;
	}

	@RequestMapping(value = API.DEPLOY_LOCAL, method = RequestMethod.POST)
	public Project deployLocalProject(@RequestBody DeployRequest data, @RequestHeader HttpHeaders headers,
			HttpServletRequest request) throws KlabException {

		if (!(KServerController.isLocalIp(request) || KServerController.isAdmin(headers))) {
			throw new KlabAuthorizationException("no authorization for local deployment");
		}

		String id = data.getId();
		File outfile = new File(data.getPath());

		IProject existing = KLAB.PMANAGER.getProject(id);

		if (existing != null) {
			if (((Workspace)KLAB.WORKSPACE).isClientProject(id) || (data.getTimestamp() > 0 && existing.getLastModificationTime() <= data.getTimestamp())) {
					KLAB.info("local deployment of " + id + " unnecessary as same version is already loaded");
					return KLAB.MFACTORY.adapt(existing, Project.class);
			}
		}
		
		boolean reloadOthers = existing != null;

		if (reloadOthers) {
			KLAB.PMANAGER.undeployProject(id);
		}

		KLAB.info("local deploy request received for project " + id);
		IProject project = KLAB.PMANAGER.registerProject(outfile);
		
		if (project != null) {

		    // remember for future batch executions
		    if (KLAB.ENGINE instanceof ModelingEngine) {
		        ((ModelingEngine)KLAB.ENGINE).registerClientProject(outfile);
		    }
		    
			/*
			 * reload the entire thing unless the project is one we had not seen
			 * before. This is because we cannot easily know the
			 * interdependencies, particularly regarding composed concepts with
			 * traits from disparate projects.
			 */
			if (reloadOthers) {
				KLAB.PMANAGER.load(true, KLAB.MFACTORY.getRootParsingContext());
			} else {
				KLAB.PMANAGER.loadProject(project.getId(), KLAB.MFACTORY.getRootParsingContext());
			}
		}

		return KLAB.MFACTORY.adapt(KLAB.PMANAGER.getProject(id), Project.class);

	}

}
